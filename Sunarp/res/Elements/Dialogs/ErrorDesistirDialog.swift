//
//  ConfirmDialog.swift
//  App
//
//  Created by femer on 1/18/21.
//  Copyright © 2021 Raul Quispe. All rights reserved.
//

import UIKit

protocol ErrorDesistirDialogDelegate {
    func primary(action:String)
    func secondary()
} 

class ErrorDesistirDialog: UIViewController {
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var dialogHeaderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var secondaryButton: UIButton!
    @IBOutlet weak var primaryButton: UIButton!
    var delegate:ErrorDesistirDialogDelegate?
    var titleText = ""
    var messageText = ""
    var primaryText = ""
    var secondaryText = ""
    var addColor:UIColor = .clear
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        
        dialogHeaderView.backgroundColor =  addColor
        
       // dialogHeaderView.roundCorners(corners: [.topLeft, .topRight], radius: 5)
        
        let attributedString = NSMutableAttributedString(string: messageText)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 8 // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        /*
        let labelFont = messageLabel.font!
        attributedString.addAttribute(NSAttributedString.Key.font, value: labelFont, range: NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        messageLabel.attributedText = attributedString
        
        */
        
        
        titleLabel.text = titleText
        messageLabel.text = messageText
        
        primaryButton.backgroundColor = SunarpColors.red
        primaryButton.titleLabel?.font = SunarpFont.bold12
        primaryButton.setTitleColor(UIColor .white, for: UIControl.State.normal)
        primaryButton.addShadowViewCustom(cornerRadius: 5)
        
        
        
        secondaryButton.titleLabel?.font = SunarpFont.bold12
        secondaryButton.setTitleColor(UIColor .red, for: UIControl.State.normal)
        secondaryButton.addShadowViewCustom(cornerRadius: 5)
        secondaryButton.borderView()
        secondaryButton.layer.borderColor = SunarpColors.red.cgColor
        
        let controlStates: Array<UIControl.State> = [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved]
        for controlState in controlStates {
            primaryButton.setTitle(NSLocalizedString(primaryText, comment: ""), for: controlState)
            secondaryButton.setTitle(NSLocalizedString(secondaryText, comment: ""), for: controlState)
        }
        
       //  secondaryButton.isHidden = secondaryText.isEmpty
        
    }
    
    @IBAction func secondary(_ sender: Any) {
        delegate?.secondary()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func primary(_ sender: Any) {
        delegate?.primary(action: primaryText)
        self.dismiss(animated: true, completion: nil)
    }
    
}
