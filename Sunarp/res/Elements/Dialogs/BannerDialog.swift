//
//  ConfirmDialog.swift
//  App
//
//  Created by femer on 1/18/21.
//  Copyright © 2021 Raul Quispe. All rights reserved.
//

import UIKit

protocol BannerDialogDelegate {
    func primary(action:String)
    func secondary()
} 

class BannerDialog: UIViewController {
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var dialogHeaderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var secondaryButton: UIButton!
    @IBOutlet weak var primaryButton: UIButton!
    @IBOutlet weak var secondaryButtonView: UIView!
    
    var delegate:BannerDialogDelegate?
    var titleText = ""
    var messageText = ""
    var primaryText = ""
    var secondaryText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        
        dialogHeaderView.backgroundColor =  SunarpColors.greenLight
        
        let attributedString = NSMutableAttributedString(string: messageText)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 8 // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        /*
        let labelFont = messageLabel.font!
        attributedString.addAttribute(NSAttributedString.Key.font, value: labelFont, range: NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        messageLabel.attributedText = attributedString
        
        titleLabel.text = titleText
        */
        
        let controlStates: Array<UIControl.State> = [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved]
        for controlState in controlStates {
          //  primaryButton.setTitle(NSLocalizedString(primaryText, comment: ""), for: controlState)
            secondaryButton.setTitle(NSLocalizedString(secondaryText, comment: ""), for: controlState)
        }
        
         secondaryButton.isHidden = secondaryText.isEmpty
        
        let tapSecondaryGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSecondaryView))
        self.secondaryButtonView.addGestureRecognizer(tapSecondaryGesture)
        
    }
    
    @objc private func onTapSecondaryView() {
        delegate?.secondary()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func secondary(_ sender: Any) {
        delegate?.secondary()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func primary(_ sender: Any) {
        delegate?.primary(action: primaryText)
        self.dismiss(animated: true, completion: nil)
    }
    
}
