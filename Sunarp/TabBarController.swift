//
//  TabBarController.swift
//  Calidda
//
//  Created by Avances on 12/7/20.
//  Copyright © 2020 Calidda. All rights reserved.
//

import UIKit
 
class TabBarController: UITabBarController {
    
    var viewCtrl1: InicioViewController?
    var viewCtrl2: PerfilViewController?
    var viewCtrl3: QrViewController?
    var subViewCtrl:[UIViewController]=[]

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

        // Do any additional setup after loading the view.
    }
    
    func setup() {
        
        let storyboard1 = UIStoryboard(name: "Home", bundle: Bundle.main)
        let storyboard2 = UIStoryboard(name: "Home", bundle: Bundle.main)
        let storyboard3 = UIStoryboard(name: "Home", bundle: Bundle.main)
        
        let inicioViewController:InicioViewController = storyboard1.instantiateViewController(withIdentifier: "InicioViewController") as! InicioViewController
        let contactoFirstViewController:PerfilViewController = storyboard2.instantiateViewController(withIdentifier: "PerfilViewController") as! PerfilViewController
        let qrViewController:QrViewController = storyboard3.instantiateViewController(withIdentifier: "QrViewController") as! QrViewController
       
        
        let navigationController1: UINavigationController = UINavigationController(rootViewController: inicioViewController)
        let navigationController2: UINavigationController = UINavigationController(rootViewController: contactoFirstViewController)
        let navigationController3: UINavigationController = UINavigationController(rootViewController: qrViewController)
      
        
        navigationController1.tabBarItem = UITabBarItem(title:"Inicio", image: #imageLiteral(resourceName: "Home"), selectedImage: #imageLiteral(resourceName: "HomeSelect"))
        navigationController2.tabBarItem = UITabBarItem(title:"Perfil", image: #imageLiteral(resourceName: "Profile"), selectedImage: #imageLiteral(resourceName: "ProfileSelect"))
        navigationController3.tabBarItem = UITabBarItem(title:"Qr", image: #imageLiteral(resourceName: "Qr"), selectedImage: #imageLiteral(resourceName: "QrSelect"))
     
        
        
        UITabBar.appearance().barTintColor = SunarpColors.white
        UITabBar.appearance().tintColor = SunarpColors.greenLight
        
        navigationController1.tabBarItem.tag = 0
        navigationController2.tabBarItem.tag = 1
        navigationController3.tabBarItem.tag = 2
        
        self.view.backgroundColor = .black
        self.setViewControllers([navigationController1,navigationController2,navigationController3], animated: true)
        self.selectedIndex = 0
        self.selectedViewController = navigationController1
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            tabBar.tintColor = SunarpColors.greenLight
        case 1:
            tabBar.tintColor = SunarpColors.greenLight
        case 2:
            tabBar.tintColor = SunarpColors.greenLight
        default:
            print("no tab")
        }
    }
    
    func hidden(){
        self.tabBar.isHidden = true
    }
    
    func visible(){
        self.tabBar.isHidden = false
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
