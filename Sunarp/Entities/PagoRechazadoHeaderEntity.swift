//
//  PagoRechazadoHeaderEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

struct PagoRechazadoHeaderEntity {
    
    public let ecoreTransactionDate: Int
    public let ecoreTransactionUUID: String
    public let millis: Int
    
    init(json: [String: Any]) {
        self.ecoreTransactionDate = json["ecoreTransactionDate"] as? Int ?? 0
        self.ecoreTransactionUUID = json["ecoreTransactionUUID"] as? String ?? ""
        self.millis = json["millis"] as? Int ?? 0
    }
}
