//
//  GrupoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct GrupoEntity {
    
    public let coTipoPersona: String
    public let noTipoPersona: String
    
    init(json: JSON) {
        self.coTipoPersona = json["coTipoPersona"] as? String ?? ""
        self.noTipoPersona = json["noTipoPersona"] as? String ?? ""
    }
}
