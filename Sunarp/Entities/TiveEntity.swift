//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct TiveEntity {
   
    public let codigoZona: String
    public let codigoOficina: String
    public let anioTitulo: String
    public let numeroTitulo: String
    public let numeroPlaca: String
    public let codigoVerificacion: String
    public let oficina: String
    public let tipo: String
    public let codResult: String
    public let msgResult: String

    init(json: JSON) {
        self.codigoZona = json["codigoZona"] as? String ?? ""
        self.codigoOficina = json["codigoOficina"] as? String ?? ""
        self.anioTitulo = json["anioTitulo"] as? String ?? ""
        self.numeroTitulo = json["numeroTitulo"] as? String ?? ""
        self.numeroPlaca = json["numeroPlaca"] as? String ?? ""
        self.codigoVerificacion = json["codigoVerificacion"] as? String ?? ""
        self.oficina = json["oficina"] as? String ?? ""
        self.tipo = json["tipo"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
