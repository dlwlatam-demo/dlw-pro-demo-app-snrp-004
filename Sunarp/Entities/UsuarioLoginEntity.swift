//
//  UsuarioLoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class UsuarioLoginEntity: NSObject {
    
    public let tipo: String
    public let idUser: Int
    public let email: String
    public let sexo: String
    public let tipoDoc: String
    public let nroDoc: String
    public let nombres: String
    public let priApe: String
    public let segApe: String
    public let fecNac: String
    public let nroCelular: String
    public let status: String
    public let password: String
    public let rememberToken: String
    public let createdAt: String
    public let updatedAt: String
    public let deletedAt: String
    public let appVersion: String
    public let ipAddress: String
    public let codValidacion: String
    public let idUsuaCrea: String
    public let idUsuaModi: String
    public let lastConn: String
    public let geoLat: Double
    public let geoLong: Double
    public let userKeyId: String
    public let userPhoto: String
    public let token: String
    
    init(json: [String: Any]) {
        self.tipo = json["tipo"] as? String ?? ""
        self.idUser = json["idUser"] as? Int ?? 0
        self.email = json["email"] as? String ?? ""
        self.sexo = json["sexo"] as? String ?? ""
        self.tipoDoc = json["tipoDoc"] as? String ?? ""
        self.nroDoc = json["nroDoc"] as? String ?? ""
        self.nombres = json["nombres"] as? String ?? ""
        self.priApe = json["priApe"] as? String ?? ""
        self.segApe = json["segApe"] as? String ?? ""
        self.fecNac = json["fecNac"] as? String ?? ""
        self.nroCelular = json["nroCelular"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.password = json["password"] as? String ?? ""
        self.rememberToken = json["rememberToken"] as? String ?? ""
        self.createdAt = json["createdAt"] as? String ?? ""
        self.updatedAt = json["updatedAt"] as? String ?? ""
        self.deletedAt = json["deletedAt"] as? String ?? ""
        self.appVersion = json["appVersion"] as? String ?? ""
        self.ipAddress = json["ipAddress"] as? String ?? ""
        self.codValidacion = json["codValidacion"] as? String ?? ""
        self.idUsuaCrea = json["idUsuaCrea"] as? String ?? ""
        self.idUsuaModi = json["idUsuaModi"] as? String ?? ""
        self.lastConn = json["lastConn"] as? String ?? ""
        self.geoLat = json["geoLat"] as? Double ?? 0.0
        self.geoLong = json["geoLong"] as? Double ?? 0.0
        self.userKeyId = json["userKeyId"] as? String ?? ""
        self.userPhoto = json["userPhoto"] as? String ?? ""
        self.token = json["token"] as? String ?? ""
    }
}
