//
//  AreaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct AreaEntity {
    
    public let codGrupoLibroArea: String
    public let descGrupoLibroArea: String
    public let precOfic: Double
    
    init(json: JSON) {
        self.codGrupoLibroArea = json["codGrupoLibroArea"] as? String ?? ""
        self.descGrupoLibroArea = json["descGrupoLibroArea"] as? String ?? ""
        self.precOfic = json["precOfic"] as? Double ?? 0.0
    }
}
