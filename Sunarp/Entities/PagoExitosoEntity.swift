//
//  PagoExitosoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

struct PagoExitosoEntity {
    
    public let actionCode: String
    public let actionDescription: String
    public let adquirente: String
    public let amount: String
    public let authorizationCode: String
    public let brand: String
    public let card: String
    public let currency: String
    public let eci: String
    public let eciDescription: String
    public let email: String
    public let firstName: String
    public let idUnico: String
    public let lastName: String
    public let merchant: String
    public let processCode: String
    public let purchaseNumber: String
    public let signature: String
    public let status: String
    public let terminal: String
    public let traceNumber: String
    public let transactionDate: String
    public let transactionId: String
    public let quotaAmount: String
    public let idResolutor: String
    public let brandHostId: String
    public let brandName: String
    public let brandHostDateTime: String
    public let yapeId: String
    public let cardType: String
    public let installmentsInfo: String
    public let quotaNumber: String
    public let quotaDeferred: String
    public let brandActionCode: String
    
    init(json: [String: Any]) {
        
        self.actionCode = json["ACTION_CODE"] as? String ?? ""
        self.actionDescription = json["ACTION_DESCRIPTION"] as? String ?? ""
        self.adquirente = json["ADQUIRENTE"] as? String ?? ""
        self.amount = json["AMOUNT"] as? String ?? ""
        self.authorizationCode = json["AUTHORIZATION_CODE"] as? String ?? ""
        self.brand = json["BRAND"] as? String ?? ""
        self.card = json["CARD"] as? String ?? ""
        self.currency = json["CURRENCY"] as? String ?? ""
        self.eci = json["ECI"] as? String ?? ""
        self.eciDescription = json["ECI_DESCRIPTION"] as? String ?? ""
        self.email = json["EMAIL"] as? String ?? ""
        self.firstName = json["FIRST_NAME"] as? String ?? ""
        self.idUnico = json["ID_UNICO"] as? String ?? ""
        self.lastName = json["LAST_NAME"] as? String ?? ""
        self.merchant = json["MERCHANT"] as? String ?? ""
        self.processCode = json["PROCESS_CODE"] as? String ?? ""
        self.purchaseNumber = json["PURCHASE_NUMBER"] as? String ?? ""
        self.signature = json["SIGNATURE"] as? String ?? ""
        self.status = json["STATUS"] as? String ?? ""
        self.terminal = json["TERMINAL"] as? String ?? ""
        self.traceNumber = json["TRACE_NUMBER"] as? String ?? ""
        self.transactionDate = json["TRANSACTION_DATE"] as? String ?? ""
        self.transactionId = json["TRANSACTION_ID"] as? String ?? ""
        self.quotaAmount = json["QUOTA_AMOUNT"] as? String ?? ""
        self.idResolutor = json["ID_RESOLUTOR"] as? String ?? ""
        self.brandHostId = json["BRAND_HOST_ID"] as? String ?? ""
        self.brandName = json["BRAND_NAME"] as? String ?? ""
        self.brandHostDateTime = json["BRAND_HOST_DATE_TIME"] as? String ?? ""
        self.yapeId = json["YAPE_ID"] as? String ?? ""
        self.cardType = json["CARD_TYPE"] as? String ?? ""
        self.installmentsInfo = json["INSTALLMENTS_INFO"] as? String ?? ""
        self.quotaNumber = json["QUOTA_NUMBER"] as? String ?? ""
        self.quotaDeferred = json["QUOTA_DEFERRED"] as? String ?? ""
        self.brandActionCode = json["BRAND_ACTION_CODE"] as? String ?? ""
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["actionCode": actionCode, "actionDescription": "actionDescription", "adquirente": adquirente, "amount": amount, "authorizationCode": authorizationCode, "brand": brand, "card": card, "currency": currency, "eci": eci, "eciDescription": eciDescription, "email": email, "firstName": firstName, "idUnico": idUnico, "lastName": lastName, "merchant": merchant, "processCode": processCode, "purchaseNumber": purchaseNumber, "signature": signature, "status": status, "terminal": terminal, "traceNumber": traceNumber, "transactionDate": transactionDate, "transactionId": transactionId, "quotaAmount": quotaAmount, "idResolutor": idResolutor, "brandHostId": brandHostId, "brandName": brandName, "brandHostDateTime": brandHostDateTime, "yapeId": yapeId, "cardType": cardType, "installmentsInfo": installmentsInfo, "quotaNumber": quotaNumber, "quotaDeferred": quotaDeferred, "brandActionCode": brandActionCode]
        print (bodyDic)
        return bodyDic
    }
}
