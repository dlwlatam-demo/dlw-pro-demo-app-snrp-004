//
//  LoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

struct LoginEntity {
    
    public let accessToken: String
    public let tokenType: String
    public let refreshToken: String
    public let expiresIn: Int
    public let scope: String
    public let userName: String
    public let jti: String
    
    public let code: String
    public let category: String
    public let component: String
    public let description: String
    
    init(json: JSON) {
        self.accessToken = json["access_token"] as? String ?? ""
        self.tokenType = json["token_type"] as? String ?? ""
        self.refreshToken = json["refresh_token"] as? String ?? ""
        self.expiresIn = json["expires_in"] as? Int ?? 0
        self.scope = json["scope"] as? String ?? ""
        self.userName = json["user_name"] as? String ?? ""
        self.jti = json["jti"] as? String ?? ""
        self.code = json["code"] as? String ?? ""
        self.category = json["category"] as? String ?? ""
        self.component = json["component"] as? String ?? ""
        self.description = json["description"] as? String ?? ""
    }
}
