//
//  TransactionData.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 19/10/23.
//

import Foundation

// MARK: - Welcome
struct TransactionData: Codable {
    let fulfillment: Fulfillment
    let order: Order
    var dataMap: DataMap
    let header: Header
}

// MARK: - DataMap
struct DataMap: Codable {
    let signature, lastName, idResolutor, brandActionCode: String
    let brandHostID, merchant, eci, currency: String
    let adquirente, transactionID, actionDescription, actionCode: String
    let cardType, processCode, eciDescription, firstName: String
    let email, brandName, status, idUnico: String
    let authorizationCode, card, transactionDate, brandHostDateTime: String
    let terminal, traceNumber, amount, brand: String
    var purchaseNumber: String

    enum CodingKeys: String, CodingKey {
        case signature = "SIGNATURE"
        case lastName = "LAST_NAME"
        case idResolutor = "ID_RESOLUTOR"
        case brandActionCode = "BRAND_ACTION_CODE"
        case brandHostID = "BRAND_HOST_ID"
        case merchant = "MERCHANT"
        case eci = "ECI"
        case currency = "CURRENCY"
        case adquirente = "ADQUIRENTE"
        case transactionID = "TRANSACTION_ID"
        case actionDescription = "ACTION_DESCRIPTION"
        case actionCode = "ACTION_CODE"
        case cardType = "CARD_TYPE"
        case processCode = "PROCESS_CODE"
        case eciDescription = "ECI_DESCRIPTION"
        case firstName = "FIRST_NAME"
        case email = "EMAIL"
        case brandName = "BRAND_NAME"
        case status = "STATUS"
        case idUnico = "ID_UNICO"
        case authorizationCode = "AUTHORIZATION_CODE"
        case card = "CARD"
        case transactionDate = "TRANSACTION_DATE"
        case brandHostDateTime = "BRAND_HOST_DATE_TIME"
        case terminal = "TERMINAL"
        case traceNumber = "TRACE_NUMBER"
        case amount = "AMOUNT"
        case brand = "BRAND"
        case purchaseNumber = "PURCHASE_NUMBER"
    }
}

// MARK: - Fulfillment
struct Fulfillment: Codable {
    let channel, merchantID, signature, terminalID: String
    let fastPayment, countable: Bool
    let captureType: String

    enum CodingKeys: String, CodingKey {
        case channel
        case merchantID = "merchantId"
        case signature
        case terminalID = "terminalId"
        case fastPayment, countable, captureType
    }
}

// MARK: - Header
struct Header: Codable {
    let ecoreTransactionDate: Int
    let ecoreTransactionUUID: String
    let millis: Int
}

// MARK: - Order
struct Order: Codable {
    let amount: Int
    let tokenID, authorizationCode, currency, actionCode: String
    let purchaseNumber, traceNumber, transactionID, transactionDate: String
    let installment, authorizedAmount: Int

    enum CodingKeys: String, CodingKey {
        case amount
        case tokenID = "tokenId"
        case authorizationCode, currency, actionCode, purchaseNumber, traceNumber
        case transactionID = "transactionId"
        case transactionDate, installment, authorizedAmount
    }
}

