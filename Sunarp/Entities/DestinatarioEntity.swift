//
//  DestinatarioEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct DestinatarioEntity {
    
    public let tpoEnvio: String
    public let tpoEnvioDesc: String
    public let oficina: String
    public let distrito: String
    public let direccion: String
    public let codPostal: String
    public let departamento: String
    public let provincia: String
    public let nombre: String
    
    init(json: [String: Any]) {
        self.tpoEnvio = json["tpoEnvio"] as? String ?? ""
        self.tpoEnvioDesc = json["tpoEnvioDesc"] as? String ?? ""
        self.oficina = json["oficina"] as? String ?? ""
        self.distrito = json["distrito"] as? String ?? ""
        self.direccion = json["direccion"] as? String ?? ""
        self.codPostal = json["codPostal"] as? String ?? ""
        self.departamento = json["departamento"] as? String ?? ""
        self.provincia = json["provincia"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
    }
}
