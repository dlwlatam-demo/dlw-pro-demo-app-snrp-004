//
//  PagoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct PagoEntity {
    
    public let monto: String
    public let fecha: String
    public let tpoPago: String
    public let mayorDerecho: String
    public let codTpoEsquela: String
    
    init(json: [String: Any]) {
        self.monto = json["monto"] as? String ?? ""
        self.fecha = json["fecha"] as? String ?? ""
        self.tpoPago = json["tpoPago"] as? String ?? ""
        self.mayorDerecho = json["mayorDerecho"] as? String ?? ""
        self.codTpoEsquela = json["codTpoEsquela"] as? String ?? ""
    }
}
