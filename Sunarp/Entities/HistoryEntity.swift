//
//  HistoryEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

struct HistoryEntity {
    
    var solicitudes  : [SolicitudEntity]
    
    init(jsonArray: JSONArray) {
        
        var arraySolicitudes = [SolicitudEntity]()
        
        for json in jsonArray {
            arraySolicitudes.append(SolicitudEntity(json: json))
        }
        
        self.solicitudes = arraySolicitudes
    }
}
