//
//  TipoDocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct OficinaRegistralEntity {
    
    public let RegPubId: String
    public let OficRegId: String
    public let CodOficina: String
    public let Nombre: String
    public let JurisId: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.RegPubId = json["regPubId"] as? String ?? ""
        self.OficRegId = json["oficRegId"] as? String ?? ""
        self.CodOficina = json["codOficina"] as? String ?? ""
        self.Nombre = json["nombre"] as? String ?? ""
        self.JurisId = json["jurisId"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
