//
//  TitularidadEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 3/10/22.
//

import Foundation

struct TitularidadEntity {
    
    public let codZona: String
    public let codOficina: String
    public let estado: String
    public let refNumPart: String
    public let numPartida: String
    public let codArea: String
    public let areaRegisDescripcion: String
    public let codLibro: String
    public let libroDescripcion: String
    public let fichaId: String
    public let tomoId: String
    public let fojaId: String
    public let regPubSiglas: String
    public let nombreOfic: String
    public let flagRPU: String
    public let numeroPlaca: String
    public let baja: String
    public let libroDescripcionMigrado: String
    public let numPartidaMigrado: String
    public let numeroPaginas: String
    public let participanteDesc: String
    public let estadoInd: String
    public let numDocumPartic: String
    public let tipoDocumPartic: String
    public let descParticipacion: String
    public let direccionPredio: String
    public let propietarioPredio: String
    public let numeroMatricula: String
    public let nombreEmbPesq: String
    public let tipoPropAeronave: String
    public let propietarioAeronave: String
    public let numMotor: String
    public let numSerie: String
    public let modelo: String
    public let marca: String
    public let mineriaRazonSocial: String
    public let flagMineria: String
    public let flagTitulo: String
    public let flagNavegacion: String
    public let codigoGla: String
    public let flagNavegCopLiteral: String
    public let acction: String
    public let objSolicitudId: String
    public let flagVisuaPubCert: String
    public let coRegi: String
    
    init(json: JSON) {
        self.codZona = json["codZona"] as? String ?? ""
        self.codOficina = json["codOficina"] as? String ?? ""
        self.estado = json["estado"] as? String ?? ""
        self.refNumPart = json["refNumPart"] as? String ?? ""
        self.numPartida = json["numPartida"] as? String ?? ""
        self.codArea = json["codArea"] as? String ?? ""
        self.areaRegisDescripcion = json["areaRegisDescripcion"] as? String ?? ""
        self.codLibro = json["codLibro"] as? String ?? ""
        self.libroDescripcion = json["libroDescripcion"] as? String ?? ""
        self.fichaId = json["fichaId"] as? String ?? ""
        self.tomoId = json["tomoId"] as? String ?? ""
        self.fojaId = json["fojaId"] as? String ?? ""
        self.regPubSiglas = json["regPubSiglas"] as? String ?? ""
        self.nombreOfic = json["nombreOfic"] as? String ?? ""
        self.flagRPU = json["flagRPU"] as? String ?? ""
        self.numeroPlaca = json["numeroPlaca"] as? String ?? ""
        self.baja = json["baja"] as? String ?? ""
        self.libroDescripcionMigrado = json["libroDescripcionMigrado"] as? String ?? ""
        self.numPartidaMigrado = json["numPartidaMigrado"] as? String ?? ""
        self.numeroPaginas = json["numeroPaginas"] as? String ?? ""
        self.participanteDesc = json["participanteDesc"] as? String ?? ""
        self.estadoInd = json["estadoInd"] as? String ?? ""
        self.numDocumPartic = json["numDocumPartic"] as? String ?? ""
        self.tipoDocumPartic = json["tipoDocumPartic"] as? String ?? ""
        self.descParticipacion = json["descParticipacion"] as? String ?? ""
        self.direccionPredio = json["direccionPredio"] as? String ?? ""
        self.propietarioPredio = json["propietarioPredio"] as? String ?? ""
        self.numeroMatricula = json["numeroMatricula"] as? String ?? ""
        self.nombreEmbPesq = json["nombreEmbPesq"] as? String ?? ""
        self.tipoPropAeronave = json["tipoPropAeronave"] as? String ?? ""
        self.propietarioAeronave = json["propietarioAeronave"] as? String ?? ""
        self.numMotor = json["numMotor"] as? String ?? ""
        self.numSerie = json["numSerie"] as? String ?? ""
        self.modelo = json["modelo"] as? String ?? ""
        self.marca = json["marca"] as? String ?? ""
        self.mineriaRazonSocial = json["mineriaRazonSocial"] as? String ?? ""
        self.flagMineria = json["flagMineria"] as? String ?? ""
        self.flagTitulo = json["flagTitulo"] as? String ?? ""
        self.flagNavegacion = json["flagNavegacion"] as? String ?? ""
        self.codigoGla = json["codigoGla"] as? String ?? ""
        self.flagNavegCopLiteral = json["flagNavegCopLiteral"] as? String ?? ""
        self.acction = json["acction"] as? String ?? ""
        self.objSolicitudId = json["objSolicitudId"] as? String ?? ""
        self.flagVisuaPubCert = json["flagVisuaPubCert"] as? String ?? ""
        self.coRegi = json["coRegi"] as? String ?? ""
    }
}
