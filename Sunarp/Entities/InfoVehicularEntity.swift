//
//  PersonaJuridicaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/10/22.
//

import Foundation

struct InfoVehicularEntity : Codable{
    
    public let datosVehiculo: String
    
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.datosVehiculo = json["datosVehiculo"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
