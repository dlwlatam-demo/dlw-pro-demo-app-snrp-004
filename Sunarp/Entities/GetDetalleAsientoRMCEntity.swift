//
//  CertificadoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct GetDetalleAsientoRMCEntity {
    
    
    
    public let codigoSede: String
    public let numPlaca: String
    public let nsAsiento: String
    public let anioTitulo: String
    public let numTitulo: String
    public let nsActo: String
    public let codigoConcepto: String
    public let descripcionConcepto: String
    public let codigoActo: String
    public let descriActo: String
    public let codigoRegi: String
    public let registrador: String
    public let numPartida: String
    public let codigoEmpleado: String
    public let codigoEmpleadoOrig: String
    public let nsEmpleadoOrig: String
    public let codigoRegiOrig: String
    public let codigoSedeOrig: String
    public let numPlacaAnte: String
    public let fgExonPub: String
    public let fgExonPub1: String
    public let fgMandJud: String
    public let idLavaActi: String
    public let idImagAsiento: String
    public let numPagina: String
    public let fgExonPub2: String
    public let in_sele: String
    public let sede: String
    public let fechaInscripcion: String
    public var select: Bool
    public var count: Int
    public var intValCount: Int
    public var todas: Bool
    
    init(json: [String: Any]) {
        self.codigoSede = json["codigoSede"] as? String ?? ""
        self.numPlaca = json["numPlaca"] as? String ?? ""
        self.nsAsiento = json["nsAsiento"] as? String ?? ""
        self.anioTitulo = json["anioTitulo"] as? String ?? ""
        self.numTitulo = json["numTitulo"] as? String ?? ""
        self.nsActo = json["nsActo"] as? String ?? ""
        self.codigoConcepto = json["codigoConcepto"] as? String ?? ""
        self.descripcionConcepto = json["descripcionConcepto"] as? String ?? ""
        self.codigoActo = json["codigoActo"] as? String ?? ""
        self.descriActo = json["descriActo"] as? String ?? ""
        self.codigoRegi = json["codigoRegi"] as? String ?? ""
        self.registrador = json["registrador"] as? String ?? ""
        self.numPartida = json["numPartida"] as? String ?? ""
        self.codigoEmpleado = json["codigoEmpleado"] as? String ?? ""
        self.codigoEmpleadoOrig = json["codigoEmpleadoOrig"] as? String ?? ""
        self.nsEmpleadoOrig = json["nsEmpleadoOrig"] as? String ?? ""
        self.codigoRegiOrig = json["codigoRegiOrig"] as? String ?? ""
        self.codigoSedeOrig = json["codigoSedeOrig"] as? String ?? ""
        self.numPlacaAnte = json["numPlacaAnte"] as? String ?? ""
        self.fgExonPub = json["fgExonPub"] as? String ?? ""
        self.fgExonPub1 = json["fgExonPub1"] as? String ?? ""
        self.fgMandJud = json["fgMandJud"] as? String ?? ""
        self.idLavaActi = json["idLavaActi"] as? String ?? ""
        self.idImagAsiento = json["idImagAsiento"] as? String ?? ""
        self.numPagina = json["numPagina"] as? String ?? ""
        self.fgExonPub2 = json["fgExonPub2"] as? String ?? ""
        self.in_sele = json["in_sele"] as? String ?? ""
        self.sede = json["sede"] as? String ?? ""
        self.fechaInscripcion = json["fechaInscripcion"] as? String ?? ""
        self.select = false
        self.count = 0
        self.intValCount = 0
        self.todas = false
    }
}
