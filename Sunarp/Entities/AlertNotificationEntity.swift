//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct AlertNotificationEntity : Codable{
   
    /*
     [{"sno": "", "feNoti": "", "message": ""}]
     */
    public let sno: String
    public let feNoti: String
    public var message: String
    
    public let codResult: String
    public let msgResult: String

    init(json: [String: Any]) {
        self.sno = json["sno"] as? String ?? ""
        self.feNoti = json["feNoti"] as? String ?? ""
        self.message = json["message"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
