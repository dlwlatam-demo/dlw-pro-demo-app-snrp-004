//
//  LiquidacionEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

struct LiquidacionEntity {
    
    public let solicitudId: Int
    public let descripcion: String
    public let tsCrea: String
    public let total: Double
    public let email: String
    public let solicitante: String
    public let tpoPago: String
    public let pagoSolicitudId: Int
    public let numeroRecibo: String
    public let numeroPublicidad: String
    public let codVerificacion: String
    public let secReciDetaAtenNac: Int
    
    init(json: JSON) {
        self.solicitudId = json["solicitudId"] as? Int ?? 0
        self.descripcion = json["descripcion"] as? String ?? ""
        self.tsCrea = json["tsCrea"] as? String ?? ""
        self.total = json["total"] as? Double ?? 0.0
        self.email = json["email"] as? String ?? ""
        self.solicitante = json["solicitante"] as? String ?? ""
        self.tpoPago = json["tpoPago"] as? String ?? ""
        self.pagoSolicitudId = json["pagoSolicitudId"] as? Int ?? 0
        self.numeroRecibo = json["numeroRecibo"] as? String ?? ""
        self.numeroPublicidad = json["numeroPublicidad"] as? String ?? ""
        self.codVerificacion = json["codVerificacion"] as? String ?? ""
        self.secReciDetaAtenNac = json["secReciDetaAtenNac"] as? Int ?? 0
    }
}
