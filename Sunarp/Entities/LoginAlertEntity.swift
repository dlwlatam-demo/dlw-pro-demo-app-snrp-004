//
//  LoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation


struct LoginAlertEntity {
    
    public let idRgst: String
    public let email: String
    public let tiPers: String
    public let rzSocl: Int
    public let apPate: String
    public let apMate: String
    public let nombre: String
    public let tiDocu: String
    public let noDocu: String
    public let ruc: String
    public let direccion: String
    public let nuTele: String
    public let inNoti: String
    public let coNoti: String
    public let cnumCel: Int
    public let idOperTele: String
    public let flEnvioSms: String
    public let celNumber: String
    public let code: String
    public let description: String
    
    init(json: JSON) {
        self.idRgst = json["idRgst"] as? String ?? ""
        self.email = json["email"] as? String ?? ""
        self.tiPers = json["tiPers"] as? String ?? ""
        self.rzSocl = json["rzSocl"] as? Int ?? 0
        self.apPate = json["apPate"] as? String ?? ""
        self.apMate = json["apMate"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
        self.tiDocu = json["tiDocu"] as? String ?? ""
        self.noDocu = json["noDocu"] as? String ?? ""
        self.ruc = json["ruc"] as? String ?? ""
        self.direccion = json["direccion"] as? String ?? ""
        self.nuTele = json["nuTele"] as? String ?? ""
        self.inNoti = json["inNoti"] as? String ?? ""
        self.coNoti = json["coNoti"] as? String ?? ""
        self.cnumCel = json["cnumCel"] as? Int ?? 0
        self.idOperTele = json["idOperTele"] as? String ?? ""
        self.flEnvioSms = json["flEnvioSms"] as? String ?? ""
        self.celNumber = json["celNumber"] as? String ?? ""
        self.code = json["code"] as? String ?? ""
        self.description = json["description"] as? String ?? ""
    }
}
