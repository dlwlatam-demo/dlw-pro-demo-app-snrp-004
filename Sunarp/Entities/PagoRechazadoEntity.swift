//
//  PagoRechazadoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

struct PagoRechazadoEntity {
    
    public var data: PagoRechazadoDataEntity
    public let errorCode: Int
    public let errorMessage: String
    public let header: PagoRechazadoHeaderEntity
    
    init(json: [String: Any]) {
        self.data = PagoRechazadoDataEntity(json: json["data"] as? [String: Any] ?? [:])
        self.errorCode = json["errorCode"] as? Int ?? 0
        self.errorMessage = json["errorMessage"] as? String ?? ""
        self.header = PagoRechazadoHeaderEntity(json: json["header"] as? [String: Any] ?? [:])
    }
}
