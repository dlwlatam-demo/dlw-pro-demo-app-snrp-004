//
//  NiubizPinHashEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

struct NiubizPinHashEntity {
    
    public let pinHash: String
    public let expireDate: String
    
    init(json: JSON) {
        self.pinHash = json["pinHash"] as? String ?? ""
        self.expireDate = json["expireDate"] as? String ?? ""
    }
}
