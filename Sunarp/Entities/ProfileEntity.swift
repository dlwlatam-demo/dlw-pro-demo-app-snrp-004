//
//  ProfileEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class ProfileEntity: NSObject {
    
    var usuarioLogin: UsuarioLoginEntity
    var deviceLoginResponse: DeviceLoginEntity
    var deviceSessionLoginResponse: DeviceSessionLoginEntity
        
    init(json: [String: Any]) {
        self.usuarioLogin = UsuarioLoginEntity(json: json["usuarioLogin"] as? [String: Any] ?? [:])
        self.deviceLoginResponse = DeviceLoginEntity(json: json["deviceLoginResponse"] as? [String: [String: Any]] ?? [:])
        self.deviceSessionLoginResponse = DeviceSessionLoginEntity(json: json["deviceSessionLoginResponse"] as? [String: [String: Any]] ?? [:])
    }
}
