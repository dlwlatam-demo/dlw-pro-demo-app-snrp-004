//
//  SolicitudEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct ResponseEntity : Codable{
    
    public let codResult: String
    public let outCodTemporal: String
    public var msgResult: String
    public let guid: String
    public let transId: String
    
    init(json: [String: Any]) {
        self.codResult = json["codResult"] as? String ?? ""
        self.outCodTemporal = json["outCodTemporal"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
        self.guid = json["guid"] as? String ?? ""
        self.transId = String(json["transId"] as? Int ?? 0)
        print("json transId = ", self.transId)
        if (self.msgResult.isEmpty) {
            self.msgResult = json["description"] as? String ?? ""
        }
    }
}
