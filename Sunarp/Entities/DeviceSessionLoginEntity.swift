//
//  DeviceSessionLoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class DeviceSessionLoginEntity: NSObject {
    
    public let status: String
    public let createdAt: String
    public let closedAt: String
    
    init(json: [String: Any]) {
        self.status = json["status"] as? String ?? ""
        self.createdAt = json["createdAt"] as? String ?? ""
        self.closedAt = json["closedAt"] as? String ?? ""
    }
    
}
