//
//  DeviceLoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class DeviceLoginEntity: NSObject {
    
    public let idUserDevice: Int
    public let appId: String
    public let deviceToken: String
    public let deviceType: String
    public let createdAt: String
    public let modifiedAt: String
    public let status: String
    public let deviceId: String
    public let appVersion: String
    public let ipAddress: String
    public let deviceLogged: String
    public let receiveNotifications: String
    public let deviceOs: String
    
    init(json: [String: Any]) {
        self.idUserDevice = json["idUserDevice"] as? Int ?? 0
        self.appId = json["appId"] as? String ?? ""
        self.deviceToken = json["deviceToken"] as? String ?? ""
        self.deviceType = json["deviceType"] as? String ?? ""
        self.createdAt = json["createdAt"] as? String ?? ""
        self.modifiedAt = json["modifiedAt"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.deviceId = json["deviceId"] as? String ?? ""
        self.appVersion = json["appVersion"] as? String ?? ""
        self.ipAddress = json["ipAddress"] as? String ?? ""
        self.deviceLogged = json["deviceLogged"] as? String ?? ""
        self.receiveNotifications = json["receiveNotifications"] as? String ?? ""
        self.deviceOs = json["deviceOs"] as? String ?? ""
    }
}
