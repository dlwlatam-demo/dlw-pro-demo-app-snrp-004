//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct AllMandatoFromUserEntity {
   
    public let idRgst: String
    public let aaCont: String
    public let nuCont: String
    public let apPatePersNatu: String
    public let apMatePersNatu: String
    public let noPersNatu: String
    public let tiDocuIden: String
    public let nuDocu: String
    public let feAltaCont: String
    public let inEstd: String
    public let inNoti: String
    public let coNoti: String
    public let deObs: String
    public let feNoti: String
    public let inElim: String
    
    public let codResult: String
    public let msgResult: String

    init(json: JSON) {
        self.idRgst = json["idRgst"] as? String ?? ""
        self.aaCont = json["aaCont"] as? String ?? ""
        self.nuCont = json["nuCont"] as? String ?? ""
        self.apPatePersNatu = json["apPatePersNatu"] as? String ?? ""
        self.apMatePersNatu = json["apMatePersNatu"] as? String ?? ""
        self.noPersNatu = json["noPersNatu"] as? String ?? ""
        self.tiDocuIden = json["tiDocuIden"] as? String ?? ""
        self.nuDocu = json["nuDocu"] as? String ?? ""
        self.feAltaCont = json["feAltaCont"] as? String ?? ""
        self.inEstd = json["inEstd"] as? String ?? ""
        self.inNoti = json["inNoti"] as? String ?? ""
        self.coNoti = json["coNoti"] as? String ?? ""
        self.deObs = json["deObs"] as? String ?? ""
        self.feNoti = json["feNoti"] as? String ?? ""
        self.inElim = json["inElim"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
