//
//  TiveBusqEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/10/22.
//

import Foundation


struct ObtenerLibroEntity {
   
    public let codLibro: String
    public let nomLibro: String
    public let areaRegId: String
    public let grupoLibroArea: String
    
    
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.codLibro = json["codLibro"] as? String ?? ""
        self.nomLibro = json["nomLibro"] as? String ?? ""
        self.areaRegId = json["areaRegId"] as? String ?? ""
        self.grupoLibroArea = json["grupoLibroArea"] as? String ?? ""
     
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
