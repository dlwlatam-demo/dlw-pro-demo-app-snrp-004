//
//  EstadoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct EstadoEntity {
    
    public let estado: String
    public let codEstado: String
    
    init(json: [String: Any]) {
        self.estado = json["estado"] as? String ?? ""
        self.codEstado = json["codEstado"] as? String ?? ""
    }
}
