//
//  SolicitudModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 17/08/22.
//

import Foundation

struct SolicitudEntity {
    
    public let tipo: String
    public let transId: Int
    public let fecHor: String
    public let servicioId: Int
    public let cuentaId: Int
    public let strBusq: String
    public let costo: Int
    public let solicitudKey: Int
    public let aaPubl: String
    public let nuPubl: String
    
    init(json: JSON) {
        self.tipo = json["tipo"] as? String ?? ""
        self.transId = json["transId"] as? Int ?? 0
        self.fecHor = json["fecHor"] as? String ?? ""
        self.servicioId = json["servicioId"] as? Int ?? 0
        self.cuentaId = json["cuentaId"] as? Int ?? 0
        self.strBusq = json["strBusq"] as? String ?? ""
        self.costo = json["costo"] as? Int ?? 0
        self.solicitudKey = json["key"] as? Int ?? 0
        self.aaPubl = json["aaPubl"] as? String ?? ""
        self.nuPubl = json["nuPubl"] as? String ?? ""
    }
}
