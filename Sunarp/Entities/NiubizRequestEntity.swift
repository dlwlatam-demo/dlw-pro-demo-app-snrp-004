//
//  NiubizRequestEntity.swift
//  Sunarp
//
//  Created by Segundo Acosta on 17/01/23.
//

import Foundation

struct NiubizRequestEntity{
    
    public let codAccion: String
    public let codAutoriza: String
    public let codtienda: String
    public let concepto: String
    public let decisionCs: String
    public let dscCodAccion: String
    public let dscEci: String
    public let eci: String
    public let estado: String
    public let eticket: String
    public let fechaYhoraTx: String
    public let idUnico: String
    public let idUser: String
    public let impAutorizado: String
    public let nomEmisor: String
    public let numOrden: String
    public let numReferencia: String
    public let oriTarjeta: String
    public let pan: String
    public let resCvv2: String
    public let respuesta: String
    public let reviewTransaction: String
    public let transId: String
    
    init(json: JSON) {
        self.codAccion = json["codAccion"] as? String ?? ""
        self.codAutoriza = json["codAutoriza"] as? String ?? ""
        self.codtienda = json["codtienda"] as? String ?? ""
        self.concepto = json["concepto"] as? String ?? ""
        self.decisionCs = json["decisionCs"] as? String ?? ""
        self.dscCodAccion = json["dscCodAccion"] as? String ?? ""
        self.dscEci = json["dscEci"] as? String ?? ""
        self.eci = json["eci"] as? String ?? ""
        self.estado = json["estado"] as? String ?? ""
        self.eticket = json["eticket"] as? String ?? ""
        self.fechaYhoraTx = json["fechaYhoraTx"] as? String ?? ""
        self.idUnico = json["idUnico"] as? String ?? ""
        self.idUser = json["idUser"] as? String ?? ""
        self.impAutorizado = json["impAutorizado"] as? String ?? ""
        self.nomEmisor = json["nomEmisor"] as? String ?? ""
        self.numOrden = json["numOrden"] as? String ?? ""
        self.numReferencia = json["numReferencia"] as? String ?? ""
        self.oriTarjeta = json["oriTarjeta"] as? String ?? ""
        self.pan = json["pan"] as? String ?? ""
        self.resCvv2 = json["resCvv2"] as? String ?? ""
        self.respuesta = json["respuesta"] as? String ?? ""
        self.reviewTransaction = json["reviewTransaction"] as? String ?? ""
        self.transId = json["transId"] as? String ?? ""
    }
    
}


