//
//  ValidacionCeEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import Foundation

struct ValidacionCeEntity {
    
    public let strCalidadMigratoria: String
    public let strNombres: String
    public let strNumRespuesta: String
    public let strPrimerApellido: String
    public let strSegundoApellido: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.strCalidadMigratoria = json["strCalidadMigratoria"] as? String ?? ""
        self.strNombres = json["strNombres"] as? String ?? ""
        self.strNumRespuesta = json["strNumRespuesta"] as? String ?? ""
        self.strPrimerApellido = json["strPrimerApellido"] as? String ?? ""
        self.strSegundoApellido = json["strSegundoApellido"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
