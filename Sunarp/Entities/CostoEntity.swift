//
//  CostoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/12/22.
//

import Foundation

struct CostoEntity {
    
    public let monto: Double
    public let codCerti: String
    public let msgResult: String
    
    init(json: JSON) {
        self.monto = json["monto"] as? Double ?? 0.0
        self.codCerti = json["codCerti"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}

