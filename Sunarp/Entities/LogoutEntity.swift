//
//  LogoutEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/08/22.
//

import Foundation

struct LogoutEntity {
    
    public let resultado: String
    
    init(json: JSON) {
        self.resultado = json["resultado"] as? String ?? ""
    }
}
