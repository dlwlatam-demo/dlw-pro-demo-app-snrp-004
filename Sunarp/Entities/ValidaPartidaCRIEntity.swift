//
//  TiveBusqEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/10/22.
//

import Foundation

struct ValidaPartidaCRIEntity {
   
    
  
    
    public let numPlaca: String
    public let numPartida: String
    public let codigoLibro: String
    
    public let codigoGla: String
    public let refNumPartMP: Int
    public let refNumPart: Int
    
    public let estado: Int
    public let msj: String
    
    init(json: JSON) {
        self.numPlaca = json["numPlaca"] as? String ?? ""
        self.numPartida = json["numPartida"] as? String ?? ""
        self.codigoLibro = json["codigoLibro"] as? String ?? ""
        self.codigoGla = json["codigoGla"] as? String ?? ""
        self.refNumPartMP = json["refNumPartMP"] as? Int ?? 0
        self.refNumPart = json["refNumPart"] as? Int ?? 0
     
        self.estado = json["estado"] as? Int ?? -1
        self.msj = json["msj"] as? String ?? "No se encontraron datos."
    }
}
