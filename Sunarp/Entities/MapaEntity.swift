//
//  MapaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation


struct MapaEntity {
    public let codResult: String
    public let msgResult: String
    var dataMap: [MapaOfficeEntity]
    
    init(json: JSON) {
        self.dataMap = [MapaOfficeEntity]()
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
        let jsonArray = json["listOfficeResult"] as? JSONArray ?? [[:]]
        
        for jsonObj in jsonArray {
            self.dataMap.append(MapaOfficeEntity(json: jsonObj))
        }
    }
}

struct MapaOfficeEntity {
    
    public let lat: String
    public let lon: String
    public let head: String
    public let zone: String
    public let type: String
    public let office: String
    public let address: String
  //  var arrayAttention = [MapaAttentionEntity]()
    
    
    var arrayAttention: [MapaAttentionEntity] = []
    var arrayPhone: [MapaPhonesEntity] = []
    
    init(json: JSON) {
        self.lat = json["lat"] as? String ?? ""
        self.lon = json["lon"] as? String ?? ""
        self.head = json["head"] as? String ?? ""
        self.zone = json["zone"] as? String ?? ""
        self.type = json["type"] as? String ?? ""
        self.office = json["office"] as? String ?? ""
        self.address = json["address"] as? String ?? ""
        let arrayJson = json["attentions"]  as? JSONArray ?? [[:]]
        let arrayJsonPhone = json["phones"]  as? JSONArray ?? [[:]]
        
        for jsonObj in arrayJson {
            self.arrayAttention.append(MapaAttentionEntity(json: jsonObj))
        }
        
        for jsonObj in arrayJsonPhone {
            self.arrayPhone.append(MapaPhonesEntity(json: jsonObj))
        }
        
    }
}

struct MapaAttentionEntity {
    
    public let area: String
    public let schedules: Array<Any>
    init(json: JSON) {
        self.area = json["area"] as? String ?? ""
        self.schedules = (json["schedules"] as? Array <Any>)!
    }
}
struct MapaPhonesEntity {
    
    public let phone: String
    public let annex: String
    init(json: JSON) {
        self.phone = json["phone"] as? String ?? ""
        self.annex = json["annex"] as? String ?? ""
    }
}

