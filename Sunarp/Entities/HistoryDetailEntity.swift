//
//  HistoryDetailEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct HistoryDetailEntity {
    
    public let solicitudId: String
    public let userKeyId: String
    public let ncopAdic: String
    public let nsolMonLiq: String
    public let tsDesis: String
    public let tsMaxReing: String
    public let flags: FlagEntity
    public let estado: EstadoEntity
    public let certificado: CertificadoEntity
    public let solicitante: SolicitanteEntity
    public let destinatario: DestinatarioEntity
    public let pago: PagoEntity
    public let sistema: String
        
    init(json: JSON) {
        self.solicitudId = json["solicitudId"] as? String ?? ""
        self.userKeyId = json["userKeyId"] as? String ?? ""
        self.ncopAdic = json["ncopAdic"] as? String ?? ""
        self.nsolMonLiq = json["nsolMonLiq"] as? String ?? ""
        self.tsDesis = json["tsDesis"] as? String ?? ""
        self.tsMaxReing = json["tsMaxReing"] as? String ?? ""
        self.flags = FlagEntity(json: json["flags"] as? [String: Any] ?? [:])
        self.estado = EstadoEntity(json: json["estado"] as? [String: Any] ?? [:])
        self.certificado = CertificadoEntity(json: json["certificado"] as? [String: Any] ?? [:])
        self.solicitante = SolicitanteEntity(json: json["solicitante"] as? [String: Any] ?? [:])
        self.destinatario = DestinatarioEntity(json: json["destinatario"] as? [String: Any] ?? [:])
        self.pago = PagoEntity(json: json["pago"] as? [String: Any] ?? [:])
        self.sistema = json["sistema"] as? String ?? ""
    }
}
