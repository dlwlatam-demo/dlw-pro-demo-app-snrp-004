//
//  TipoDocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct RegistroJuridicoEntity {
    
    public let areaRegId: String
    public let nombre: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.areaRegId = json["areaRegId"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
