//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct NewsTransaccionEntity {
   
    /*
     [{"fecHor":"18/11/2022 16:10:08","strBusq":"APP MOVIL-BUSQUEDA NOMBRE: DE LA CRUZ BELLEZA FANNY JUDITH","estado":"-", "solicitudId":100}]
     */
    public let fecHor: String
    public let strBusq: String
    public let estado: String
    public let solicitudId: Int
    
    public let codResult: String
    public let msgResult: String

    init(json: JSON) {
        self.fecHor = json["fecHor"] as? String ?? ""
        self.strBusq = json["strBusq"] as? String ?? ""
        self.estado = json["estado"] as? String ?? ""
        self.solicitudId = json["solicitudId"] as? Int ?? 0
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
