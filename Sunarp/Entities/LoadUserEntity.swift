//
//  UsuarioLoginEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation


class LoadUserEntity: NSObject {
    
    public let apMate: String
    public let apPate: String
    public let appVersion: String
    public let celNumber: String
    public let cnumCel: String
    public let coChnPwd: String
    public let coDist: String
    public let coDpto: String
    public let coNoti: Int
    public let coPass: String
    public let coProv: String
    public let deObs: String
    public let direccion: String
    public let email: String
    public let emailAlt: String
    public let feNoti: String
    public let feRgst: String
    public let flEnvioSms: String
    public let guid: String
    public let idDevice: String
    public let idOperTele: Int
    public let idRgst: Int
    public let idUser: Int
    public let idUsuaCrea: Int
    public let idUsuaModi: Int
    public let inElim: String
    public let inElimAlrtPub: String
    public let inEstd: String
    public let inNoti: String
    public let ipAddr: String
    public let noDocu: String
    public let nombre: String
    public let nuFormAlrtPub: String
    public let nuTele: String
    public let regAlrtPub: String
    public let ruc: String
    public let rzSocl: String
    public let tiDocu: String
    public let tiPers: String
    public let tiRgst: String
    public let tsUsuaCrea: String
    public let tsUsuaModi: String
    
    public let codResult: String
    public let msgResult: String
    
    //self.apPate = json["apPate"] as? Int ?? 0
    init(json: [String: Any]) {
        self.apMate = json["apMate"] as? String ?? ""
        self.apPate = json["apPate"] as? String ?? ""
        self.appVersion = json["appVersion"] as? String ?? ""
        self.celNumber = json["celNumber"] as? String ?? ""
        self.cnumCel = json["cnumCel"] as? String ?? ""
        self.coChnPwd = json["coChnPwd"] as? String ?? ""
        self.coDist = json["coDist"] as? String ?? ""
        self.coDpto = json["coDpto"] as? String ?? ""
        self.coNoti = json["coNoti"] as? Int ?? 0
        self.coPass = json["coPass"] as? String ?? ""
        self.coProv = json["coProv"] as? String ?? ""
        self.deObs = json["deObs"] as? String ?? ""
        self.direccion = json["direccion"] as? String ?? ""
        self.email = json["email"] as? String ?? ""
        self.emailAlt = json["emailAlt"] as? String ?? ""
        self.feNoti = json["feNoti"] as? String ?? ""
        self.feRgst = json["feRgst"] as? String ?? ""
        self.flEnvioSms = json["flEnvioSms"] as? String ?? ""
        self.guid = json["guid"] as? String ?? ""
        self.idDevice = json["idDevice"] as? String ?? ""
        self.idOperTele = json["idOperTele"] as? Int ?? 0
        self.idRgst = json["idRgst"] as? Int ?? 0
        self.idUser = json["idUser"] as? Int ?? 0
        self.idUsuaCrea = json["idUsuaCrea"] as? Int ?? 0
        self.idUsuaModi = json["idUsuaModi"] as? Int ?? 0
        self.inElim = json["inElim"] as? String ?? ""
        self.inElimAlrtPub = json["inElimAlrtPub"] as? String ?? ""
        self.inEstd = json["inEstd"] as? String ?? ""
        self.inNoti = json["inNoti"] as? String ?? ""
        self.ipAddr = json["ipAddr"] as? String ?? ""
        self.noDocu = json["noDocu"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
        self.nuFormAlrtPub = json["nuFormAlrtPub"] as? String ?? ""
        self.nuTele = json["nuTele"] as? String ?? ""
        self.regAlrtPub = json["regAlrtPub"] as? String ?? ""
        self.ruc = json["ruc"] as? String ?? ""
        self.rzSocl = json["rzSocl"] as? String ?? ""
        self.tiDocu = json["tiDocu"] as? String ?? ""
        self.tiPers = json["tiPers"] as? String ?? ""
        self.tiRgst = json["tiRgst"] as? String ?? ""
        self.tsUsuaCrea = json["tsUsuaCrea"] as? String ?? ""
        self.tsUsuaModi = json["tsUsuaModi"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
        
    }
}
