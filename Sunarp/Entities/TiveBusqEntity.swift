//
//  TiveBusqEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/10/22.
//

import Foundation

struct TiveBusqEntity : Codable {
   
    public let code: String
    public let errorMessage: String
    public let documento: String
    
    public let description: String
    public let category: String
    public let component: String
    
    public let codResult: String
    public let msgResult: String
    
    init(json: [String: Any]) {
        self.code = json["code"] as? String ?? ""
        self.errorMessage = json["errorMessage"] as? String ?? ""
        self.documento = json["documento"] as? String ?? ""
        self.description = json["description"] as? String ?? ""
        self.category = json["category"] as? String ?? ""
        self.component = json["component"] as? String ?? ""
     
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
