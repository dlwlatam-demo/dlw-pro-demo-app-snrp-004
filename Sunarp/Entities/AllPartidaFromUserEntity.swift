//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct AllPartidaFromUserEntity {
   
 
    public let idRgst: Int
    public let region: String
    public let sede: String
    public let area: String
    public let fecReg: String
    public let nuCont: String
    public let nuPart: String
    public let aaCont: String
    public let estado: String
    public let estadoAP: String
    
    public let codResult: String
    public let msgResult: String

    init(json: JSON) {
        self.idRgst = json["idRgst"] as? Int ?? 0
        self.region = json["region"] as? String ?? ""
        self.sede = json["sede"] as? String ?? ""
        self.area = json["area"] as? String ?? ""
        self.fecReg = json["fecReg"] as? String ?? ""
        self.nuCont = json["nuCont"] as? String ?? ""
        self.nuPart = json["nuPart"] as? String ?? ""
        self.aaCont = json["aaCont"] as? String ?? ""
        self.estado = json["estado"] as? String ?? ""
        self.estadoAP = json["estadoAP"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
