//
//  PaymentProcessEntity.swift
//  Sunarp
//
//  Created by Segundo Acosta on 17/01/23.
//

import Foundation


struct PaymentProcessEntity{
    
    public var solicitudId: Int
    public let costoTotal: String
    public let ip: String
    public let usrId: String
    public let codigoGla: String
    public let idUser: String
    public let idUnico: String
    public let pan: String
    public let dscCodAccion: String
    public let codAutoriza: String
    public let codtienda: String
    public let numOrden: String
    public let codAccion: String
    public let fechaYhoraTx: String
    public let nomEmisor: String
    public let oriTarjeta: String
    public let respuesta: String
    public let usrKeyId: String
    public let transId: String
    public let eticket: String
    public let concepto: String
    public let codCerti: String
    public let numeroRecibo: String
    public let numeroPublicidad: String
    public let codVerificacion: String
    public let codLibroOpc: String
    public let cantPaginas: String
    public let cantPaginasExon: String
    public let paginasSolicitadas: String
    public let totalPaginasPartidaFicha: String
    public let nuAsieSelectSARP: String
    public let imPagiSIR: String
    public let nuSecuSIR: String
    public let codLibro: String
    public let coServ: String
    public let coTipoRgst: String
    public let visanetResponse: String
    public let lstOtorgantes: String
    public let lstApoyos: String
    public let lstCuradores: String
    public let lstPoderdantes: String
    public let lstApoderados: String
    public let niubizData: NiubizRequestEntity
    public let tsCrea: String
    
    public var paymentItem: PaymentItem = PaymentItem()
    
    init(json: JSON) {
        self.solicitudId = json["solicitudId"] as? Int ?? 0
        self.costoTotal = json["costoTotal"] as? String ?? ""
        self.ip = json["ip"] as? String ?? ""
        self.usrId = json["usrId"] as? String ?? ""
        self.codigoGla = json["codigoGla"] as? String ?? ""
        self.idUser = json["idUser"] as? String ?? ""
        self.idUnico = json["idUnico"] as? String ?? ""
        self.pan = json["pan"] as? String ?? ""
        self.dscCodAccion = json["dscCodAccion"] as? String ?? ""
        self.codAutoriza = json["codAutoriza"] as? String ?? ""
        self.codtienda = json["codtienda"] as? String ?? ""
        self.numOrden = json["numOrden"] as? String ?? ""
        self.codAccion = json["codAccion"] as? String ?? ""
        self.nomEmisor = json["nomEmisor"] as? String ?? ""
        self.oriTarjeta = json["oriTarjeta"] as? String ?? ""
        self.respuesta = json["respuesta"] as? String ?? ""
        self.usrKeyId = json["usrKeyId"] as? String ?? ""
        self.transId = json["transId"] as? String ?? ""
        self.eticket = json["eticket"] as? String ?? ""
        self.concepto = json["concepto"] as? String ?? ""
        self.codCerti = json["codCerti"] as? String ?? ""
        self.numeroRecibo = json["numeroRecibo"] as? String ?? ""
        self.numeroPublicidad = json["numeroPublicidad"] as? String ?? ""
        self.codVerificacion = json["codVerificacion"] as? String ?? ""
        self.codLibroOpc = json["codLibroOpc"] as? String ?? ""
        self.cantPaginas = json["cantPaginas"] as? String ?? ""
        self.cantPaginasExon = json["cantPaginasExon"] as? String ?? ""
        self.paginasSolicitadas = json["paginasSolicitadas"] as? String ?? ""
        self.totalPaginasPartidaFicha = json["totalPaginasPartidaFicha"] as? String ?? ""
        self.nuAsieSelectSARP = json["nuAsieSelectSARP"] as? String ?? ""
        self.imPagiSIR = json["imPagiSIR"] as? String ?? ""
        self.nuSecuSIR = json["nuSecuSIR"] as? String ?? ""
        self.codLibro = json["codLibro"] as? String ?? ""
        self.coServ = json["coServ"] as? String ?? ""
        self.coTipoRgst = json["coTipoRgst"] as? String ?? ""
        self.visanetResponse = json["visanetResponse"] as? String ?? ""
        self.lstOtorgantes = json["lstOtorgantes"] as? String ?? ""
        self.lstApoyos = json["lstApoyos"] as? String ?? ""
        self.lstCuradores = json["lstCuradores"] as? String ?? ""
        self.lstPoderdantes = json["lstPoderdantes"] as? String ?? ""
        self.lstApoderados = json["lstApoderados"] as? String ?? ""
        self.tsCrea = json["tsCrea"] as? String ?? ""
        if json["fechaYhoraTx"] as? String ?? "" == "" {
            self.fechaYhoraTx = self.tsCrea
        }
        else {
            self.fechaYhoraTx = json["fechaYhoraTx"] as? String ?? ""
        }
        
        self.niubizData = NiubizRequestEntity(json: json["visanetResponse"] as? [String: Any] ?? [:])
        
    }
    
}

