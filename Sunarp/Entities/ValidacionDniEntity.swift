//
//  UsuarioRegisterEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import Foundation

struct ValidacionDniEntity {
    
    public let type: String
    public let dni: String
    public let apellidoPaterno: String
    public let apellidoMaterno: String
    public let nombres: String
    public let fechaNacimiento: String
    public let fechaEmision: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.type = json["type"] as? String ?? ""
        self.dni = json["dni"] as? String ?? ""
        self.apellidoPaterno = json["apellidoPaterno"] as? String ?? ""
        self.apellidoMaterno = json["apellidoMaterno"] as? String ?? ""
        self.nombres = json["nombres"] as? String ?? ""
        self.fechaNacimiento = json["fechaNacimiento"] as? String ?? ""
        self.fechaEmision = json["fechaEmision"] as? String ?? ""
        self.codResult = json["code"] as? String ?? ""
        self.msgResult = json["description"] as? String ?? ""
    }
}
