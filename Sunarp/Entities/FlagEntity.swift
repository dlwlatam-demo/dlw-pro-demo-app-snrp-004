//
//  FlagEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct FlagEntity {
    
    public let flgObs: String
    public let flgEstado: String
    public let flgLiq: String
    public let flgTipoLiq: String
    public let flgDesis: String
    public let flgDeneg: String
    public let flgAban: String
    public let flgAclar: String
    
    init(json: [String: Any]) {
        self.flgObs = json["flgObs"] as? String ?? ""
        self.flgEstado = json["flgEstado"] as? String ?? ""
        self.flgLiq = json["flgLiq"] as? String ?? ""
        self.flgTipoLiq = json["flgTipoLiq"] as? String ?? ""
        self.flgDesis = json["flgDesis"] as? String ?? ""
        self.flgDeneg = json["flgDeneg"] as? String ?? ""
        self.flgAban = json["flgAban"] as? String ?? ""
        self.flgAclar = json["flgAclar"] as? String ?? ""
    }
}
