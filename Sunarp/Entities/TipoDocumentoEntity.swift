//
//  TipoDocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct TipoDocumentoEntity {
    
    public let tipoDocId: String
    public let nombreAbrev: String
    public let tipoPer: String
    public let estado: String
    public let descripcion: String
    
    init(json: JSON) {
        self.tipoDocId = json["tipoDocId"] as? String ?? ""
        self.nombreAbrev = json["nombreAbrev"] as? String ?? ""
        self.tipoPer = json["tipoPer"] as? String ?? ""
        self.estado = json["estado"] as? String ?? ""
        self.descripcion = json["descripcion"] as? String ?? ""
    }
}
