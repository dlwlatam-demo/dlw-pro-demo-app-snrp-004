//
//  RegistroEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct RegistroEntity : Codable {
    
    public let codResult: String
    public let msgResult: String
    
    init(json: [String: Any]) {
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
