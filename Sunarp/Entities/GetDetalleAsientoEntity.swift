//
//  CertificadoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct GetDetalleAsientoEntity {
    
    public let codigoImagen: String
    public let coZonaRegi: String
    public let coOficRegi: String
    public let desOficina: String
    public let registro: String
    public let desRegistro: String
    public let numeroPartida: String
    public let tiAsiento: String
    public let desAsiento: String
    public let detalle: String
    public let desActo: String
    public let nuPaginasCantidad: Int
    public let flagExonerado: String
    public let paginas: String
    public let nuSecu: String
    public var select: Bool
    public var count: Int
    public var intValCount: Int
    public var todas: Bool
    
    init(json: [String: Any]) {
        self.codigoImagen = json["codigoImagen"] as? String ?? ""
        self.coZonaRegi = json["coZonaRegi"] as? String ?? ""
        self.coOficRegi = json["coOficRegi"] as? String ?? ""
        self.desOficina = json["desOficina"] as? String ?? ""
        self.registro = json["registro"] as? String ?? ""
        self.desRegistro = json["desRegistro"] as? String ?? ""
        self.numeroPartida = json["numeroPartida"] as? String ?? ""
        self.tiAsiento = json["tiAsiento"] as? String ?? ""
        self.desAsiento = json["desAsiento"] as? String ?? ""
        self.detalle = json["detalle"] as? String ?? ""
        self.desActo = json["desActo"] as? String ?? ""
        self.nuPaginasCantidad = json["nuPaginasCantidad"] as? Int ?? 0
        self.flagExonerado = json["flagExonerado"] as? String ?? ""
        self.paginas = json["paginas"] as? String ?? ""
        self.nuSecu = json["nuSecu"] as? String ?? ""
        self.select = false
        self.count = 0
        self.intValCount = 0
        self.todas = false
    }
}
