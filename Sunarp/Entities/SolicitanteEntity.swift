//
//  SolicitanteEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct SolicitanteEntity {
    
    public let numDoc: String
    public let tpoDoc: String
    public let nombre: String
    
    init(json: [String: Any]) {
        self.numDoc = json["numDoc"] as? String ?? ""
        self.tpoDoc = json["tpoDoc"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
    }
}
