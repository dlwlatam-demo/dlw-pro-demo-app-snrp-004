//
//  TipoDocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct TipoCertificadoEntity {
    
    public let areaRegId: String
    public let servicioId: String
    public let certificadoId: String
    public let nombre: String
    public let precOfic: String
    public let codGrupoLibroArea: String
    public let tpoCertificado: String
    public let descGrupoLibroArea: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.areaRegId = json["areaRegId"] as? String ?? ""
        self.servicioId = json["servicioId"] as? String ?? ""
        self.certificadoId = json["certificadoId"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
        self.precOfic = json["precOfic"] as? String ?? ""
        self.codGrupoLibroArea = json["codGrupoLibroArea"] as? String ?? ""
        self.tpoCertificado = json["tpoCertificado"] as? String ?? ""
        self.descGrupoLibroArea = json["descGrupoLibroArea"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
