//
//  UsuarioRegisterEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import Foundation

struct ComentarioEntity {
    
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
