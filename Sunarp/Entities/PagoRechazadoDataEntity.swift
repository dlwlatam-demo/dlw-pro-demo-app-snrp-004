//
//  PagoRechazadoDataEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

struct PagoRechazadoDataEntity {
    
    public let actionCode: String
    public let actionDescription: String
    public let amount: String
    public let brand: String
    public let card: String
    public let currency: String
    public let eci: String
    public let merchant: String
    public let signature: String
    public let status: String
    public let traceNumber: String
    public let transactionDate: String
    public let transactionId: String
    public var purchaseNumber: String
    
    init(json: [String: Any]) {
        self.actionCode = json["ACTION_CODE"] as? String ?? ""
        self.actionDescription = json["ACTION_DESCRIPTION"] as? String ?? ""
        self.amount = json["AMOUNT"] as? String ?? ""
        self.brand = json["BRAND"] as? String ?? ""
        self.card = json["CARD"] as? String ?? ""
        self.currency = json["CURRENCY"] as? String ?? ""
        self.eci = json["ECI"] as? String ?? ""
        self.merchant = json["MERCHANT"] as? String ?? ""
        self.signature = json["SIGNATURE"] as? String ?? ""
        self.status = json["STATUS"] as? String ?? ""
        self.traceNumber = json["TRACE_NUMBER"] as? String ?? ""
        self.transactionDate = json["TRANSACTION_DATE"] as? String ?? ""
        self.transactionId = json["TRANSACTION_ID"] as? String ?? ""
        self.purchaseNumber = json["PURCHASE_NUMBER"] as? String ?? ""
    }
}
