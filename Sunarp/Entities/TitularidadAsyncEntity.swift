//
//  TitularidadAsyncEntity.swift
//  Sunarp
//
//  Created by Percy Silva on 15/12/23.
//

import Foundation

struct TitularidadAsyncEntity {
    
    public let guid: String
    public let status: String
    public let response: [TitularidadEntity]
    
    
    init(json: JSON) {
        self.guid = json["guid"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        
        var arrayTitularidad = [TitularidadEntity]()
        let array = json["response"] as? [[String: Any]] ?? [[:]]
        for json1 in array {
            arrayTitularidad.append(TitularidadEntity(json: json1))
        }
        self.response = arrayTitularidad
        
        // self.response = [TitularidadEntity](json: json["response"] as? [[String: Any]] ?? [[:]])
    }
}
