//
//  ZonaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct ZonaEntity {
    
    public let regPubId: String
    public let nombre: String
    public var select: Bool
    
    init(json: JSON) {
        self.regPubId = json["regPubId"] as? String ?? ""
        self.nombre = json["nombre"] as? String ?? ""
        self.select = false
    }
}
