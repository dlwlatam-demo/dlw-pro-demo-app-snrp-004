//
//  DocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

struct DocumentoEntity {
    
    public let documento: String
    public let filename: String
    
    init(json: JSON) {
        self.documento = json["documento"] as? String ?? ""
        self.filename = json["fileName"] as? String ?? ""
    }
}
