//
//  CertificadoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

struct CertificadoEntity {
    
    public let certificadoId: String
    public let tpoCertificado: String
    public let aaPubl: String
    public let nuPubl: String
    public let oficina: String
    public let codOficina: String
    public let codZona: String
    public let numPartida: String
    public let asiento: String
    public let annoTitulo: String
    public let numTitulo: String
    public let numPaginas: String
    public let acto: String
    public let tpoPers: String
    public let nombrePers: String
    public let areaRegistral: String
    
    init(json: [String: Any]) {
        self.certificadoId = json["certificadoId"] as? String ?? ""
        self.tpoCertificado = json["tpoCertificado"] as? String ?? ""
        self.aaPubl = json["aaPubl"] as? String ?? ""
        self.nuPubl = json["nuPubl"] as? String ?? ""
        self.oficina = json["oficina"] as? String ?? ""
        self.codOficina = json["codOficina"] as? String ?? ""
        self.codZona = json["codZona"] as? String ?? ""
        self.numPartida = json["numPartida"] as? String ?? ""
        self.asiento = json["asiento"] as? String ?? ""
        self.annoTitulo = json["annoTitulo"] as? String ?? ""
        self.numTitulo = json["numTitulo"] as? String ?? ""
        self.numPaginas = json["numPaginas"] as? String ?? ""
        self.acto = json["acto"] as? String ?? ""
        self.tpoPers = json["tpoPers"] as? String ?? ""
        self.nombrePers = json["nombrePers"] as? String ?? ""
        self.areaRegistral = json["areaRegistral"] as? String ?? ""
    }
}
