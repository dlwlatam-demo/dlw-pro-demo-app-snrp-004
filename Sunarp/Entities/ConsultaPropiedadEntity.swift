//
//  MapaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation


struct ConsultaPropiedadEntity {
    public let strCalidadMigratoria: String
    public let strNombres: String
    public let strNumRespuesta: String
    public let strPrimerApellido: String
    public let strSegundoApellido: String
    public let codResult: String
    public let msgResult: String
    
    public let estado: String
    public let msj: String
    var dataConsul: [ConsultaPropiedadListEntity]
    
    init(json: JSON) {
        self.dataConsul = [ConsultaPropiedadListEntity]()
        self.strCalidadMigratoria = json["strCalidadMigratoria"] as? String ?? ""
        self.strNombres = json["strNombres"] as? String ?? ""
        self.strNumRespuesta = json["strNumRespuesta"] as? String ?? ""
        self.strPrimerApellido = json["strPrimerApellido"] as? String ?? ""
        self.strSegundoApellido = json["strSegundoApellido"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
        
        self.estado = json["estado"] as? String ?? ""
        self.msj = json["msj"] as? String ?? ""
        let jsonArray = json["lstTitularidad"] as? JSONArray ?? [[:]]
        
        for jsonObj in jsonArray {
            self.dataConsul.append(ConsultaPropiedadListEntity(json: jsonObj))
        }
    }
}

struct ConsultaPropiedadListEntity {
    
    public let titular: String
    public let zona: String
    public let inPred: String
    public let oficina: String
    public let partida: String
    public let nroDocumento: String
    public let coUnidRegi: String
    public let refNumPart: String
    
    public let codResult: String
    public let msgResult: String
    
    
  //  var arrayAttention = [MapaAttentionEntity]()
    
    
 
    init(json: JSON) {
        self.titular = json["titular"] as? String ?? ""
        self.zona = json["zona"] as? String ?? ""
        self.inPred = json["inPred"] as? String ?? ""
        self.nroDocumento = json["nroDocumento"] as? String ?? ""
        self.oficina = json["oficina"] as? String ?? ""
        self.partida = json["partida"] as? String ?? ""
        
        self.coUnidRegi = json["coUnidRegi"] as? String ?? ""
        self.refNumPart = json["refNumPart"] as? String ?? ""
        
            self.codResult = json["codResult"] as? String ?? ""
            self.msgResult = json["msgResult"] as? String ?? ""
    }
}

