//
//  TiveEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/10/22.
//

import Foundation

struct SearchPartidaEntity : Codable {
   
    public let numPartida: String
    public let descripcion: String
    public let codLibro: String
    public let ficha: String
    public let tomo: String
    public let tipo: String
    public let regPubId: String
    public let oficRegId: String
    public let placa: String
    
    public let codResult: String
    public let msgResult: String

    init(json: [String: Any]) {
        self.numPartida = json["numPartida"] as? String ?? ""
        self.descripcion = json["descripcion"] as? String ?? ""
        self.codLibro = json["codLibro"] as? String ?? ""
        self.ficha = json["ficha"] as? String ?? ""
        self.tomo = json["tomo"] as? String ?? ""
        self.tipo = json["tipo"] as? String ?? ""
        self.regPubId = json["regPubId"] as? String ?? ""
        self.oficRegId = json["oficRegId"] as? String ?? ""
        self.placa = json["placa"] as? String ?? ""
        
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
