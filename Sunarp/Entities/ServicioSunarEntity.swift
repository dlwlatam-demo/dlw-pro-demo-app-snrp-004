//
//  TipoDocumentoEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

struct ServicioSunarEntity {

    public let coServ: String
    public let coTipoRgst: String
    public let deServRgst: String
    public let codResult: String
    public let msgResult: String
    
    init(json: JSON) {
        self.coServ = json["coServ"] as? String ?? ""
        self.coTipoRgst = json["coTipoRgst"] as? String ?? ""
        self.deServRgst = json["deServRgst"] as? String ?? ""
        self.codResult = json["codResult"] as? String ?? ""
        self.msgResult = json["msgResult"] as? String ?? ""
    }
}
