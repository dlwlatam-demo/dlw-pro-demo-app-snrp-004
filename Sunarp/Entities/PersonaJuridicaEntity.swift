//
//  PersonaJuridicaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/10/22.
//

import Foundation

struct PersonaJuridicaEntity {
    
    public let razon: String
    public let siglas: String
    public let oficina: String
    public let partida: String
    public let codigoRegi: String
    public let codigoSede: String
        
    init(json: JSON) {
        self.razon = json["razon"] as? String ?? ""
        self.siglas = json["siglas"] as? String ?? ""
        self.oficina = json["oficina"] as? String ?? ""
        self.partida = json["partida"] as? String ?? ""
        self.codigoRegi = json["codigoRegi"] as? String ?? ""
        self.codigoSede = json["codigoSede"] as? String ?? ""
    }
}
