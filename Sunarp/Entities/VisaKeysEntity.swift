//
//  VisaKeysResponse.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation

struct VisaKeysEntity {
    
    public let instancia: String
    public let merchantId: String
    public let accesskeyId: String
    public let secretAccessKey: String
    public let pinHash: String
    public let expireDate: String
    public let urlVisanetToken: String
    public let urlVisanet: String
    public let urlVisanetCounter: String
    public let mdd4: String
    public let mdd21: String
    public let mdd32: String
    public let mdd75: String
    public let mdd77: String
    
    init(json: JSON) {
        self.instancia = json["instancia"] as? String ?? ""
        self.merchantId = json["merchantId"] as? String ?? ""
        self.accesskeyId = json["accesskeyId"] as? String ?? ""
        self.secretAccessKey = json["secretAccessKey"] as? String ?? ""
        self.pinHash = json["pinHash"] as? String ?? ""
        self.expireDate = json["expireDate"] as? String ?? ""
        self.urlVisanetToken = json["urlVisanetToken"] as? String ?? ""
        self.urlVisanet = json["urlVisanet"] as? String ?? ""
        self.urlVisanetCounter = json["urlVisanetCounter"] as? String ?? ""
        self.mdd4 = json["mdd4"] as? String ?? ""
        self.mdd21 = json["mdd21"] as? String ?? ""
        self.mdd32 = json["mdd32"] as? String ?? ""
        self.mdd75 = json["mdd75"] as? String ?? ""
        self.mdd77 = json["mdd77"] as? String ?? ""
    }
}
