//
//  BoletaEntity.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 8/09/22.
//

import Foundation

struct BoletaEntity {
    
    public let boleta: String
    
    init(json: JSON) {
        self.boleta = json["boleta"] as? String ?? ""
    }
}
