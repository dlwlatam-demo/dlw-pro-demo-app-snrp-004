//
//  ViewControllerPresenter.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 20/07/23.
//

import Foundation

class ViewControllerPresenter {
    private weak var controller: ViewController?
    
    lazy private var model: LoginModel = {
        let navigation = controller?.navigationController
       return LoginModel(navigationController: navigation!)
    }()
    
    init(controller: ViewController) {
        self.controller = controller
    }
    
    func validateVersionApp() {
        self.model.validateVersionAppTask { jsonResponse, resultCode in
            guard let code = resultCode else { return }
            
            switch(code){
                case 200:
                    if self.validarVersiones(version: jsonResponse?.resultado) {
                        self.controller?.showAlertMessage()
                    }else{
                        self.controller?.nextView()
                    }
            default:
                self.controller?.showAlertError()
            }
        
        }
    }
    
    func validarVersiones(version: String?) -> Bool{
        var isValor = false
        if let version = version, let appVersion = self.getAppVersion() {
            let versionCodeApp = NSString(string: version)
            let versionAppWS = NSString(string: appVersion)

            if (versionAppWS != versionCodeApp){
                isValor = true
            }
        }
       return isValor
    }
    
    func getAppVersion() -> String? {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return nil
    }

}
