//
//  Router.swift
//  Calidda
//
//  Created by MacAdrian on 10/22/20.
//  Copyright © 2020 Calidda. All rights reserved.
//

import UIKit
import RxSwift
import Then
import RxCocoa

class Router {
    lazy private var mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    lazy private var homeStoryboard = UIStoryboard(name: "Home", bundle: nil)


    lazy private var tabBarCtrl = TabBarController()
    lazy private var windowsFirst = UIApplication.shared.windows.first
    
    //OldPasswordView
    //AttentionManagementView
    
    
    // MARK: - views list
    enum Scene {
        case home
        
    }
    // MARK: - invoke a single view
    func show(view: Scene, sender: UIViewController) {
        switch view {
            
        case .home:
            
            let navCtrl = UINavigationController(rootViewController: tabBarCtrl)
            navCtrl.isNavigationBarHidden = false
        
            windowsFirst?.rootViewController = navCtrl
            windowsFirst?.makeKeyAndVisible()
            windowsFirst?.tintColor = .systemTeal
            
            let homeView:InicioViewController = homeStoryboard.instantiateViewController(withIdentifier:"InicioViewController") as! InicioViewController
            homeView.router = self
            self.show(target: homeView, sender: sender)
            
       
            
            break
      /*  case .homeMenu:
                      let homeView:HomeView = homeStoryboard.instantiateViewController(withIdentifier:"HomeView") as! HomeView
                      homeView.router = self
                      self.showMenu(target: homeView, sender: sender)
                      
                 
                      
                      break
        case .logIn:
            let logInView:LogInView = authStoryboard.instantiateViewController(withIdentifier:"LogInView") as! LogInView
            logInView.router = self
            self.show(target: logInView, sender: sender)
            break
   
        case .splash:
            let splashView:SplashView = mainStoryboard.instantiateViewController(withIdentifier:"SplashView") as! SplashView
            splashView.router = self
            self.show(target: splashView, sender: sender)
            break
            
       */
            
                
            
     /*   case .register:
            let registerView:RegisterView = authStoryboard.instantiateViewController(withIdentifier:"RegisterView") as! RegisterView
            registerView.router = self
            self.show(target: registerView, sender: sender)
            break
*/
       
        }
    }
    private func show(target: UIViewController, sender: UIViewController) {
        
        if let nav = sender as? UINavigationController {
            //push root controller on navigation stack
            nav.pushViewController(target, animated: false)
            nav.setNavigationBarHidden(true, animated: false)
            return
        }
        
        if let nav = sender.navigationController {
            //add controller to navigation stack
            nav.pushViewController(target, animated: true)
            
            
        } else {
            //present modally
            sender.present(target, animated: true, completion: nil)
        }
    }
    
    private func showMenu(target: UIViewController, sender: UIViewController) {
           
        //present modally
            if let nav = sender.navigationController {
                //add controller to navigation stack
                nav.pushViewController(target, animated: false)
                checkTabBarVisible(target: target)
            }
       }
    
    func pop(sender: UIViewController){
        if let nav = sender.navigationController {
            //add controller to navigation stack
            nav.popViewController(animated: true)
            checkTabBarVisible(target: nav.visibleViewController!)
        } else {
            //present modally
            sender.dismiss(animated: true, completion: nil)
        }
    }
    
    func checkTabBarVisible(target: UIViewController){
        (target.tabBarController as! TabBarController).visible()
        
       /* if target is DetailPdfView {
            (target.tabBarController as! TabBarController).hidden()
        }
        */
    
    }
}
