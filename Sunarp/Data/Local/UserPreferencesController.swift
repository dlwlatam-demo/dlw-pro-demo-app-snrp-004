//
//  UserPreferencesController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/07/22.
//

import Foundation

class UserPreferencesController {
 
    private static let preferences = UserDefaults.standard
    
    static func clearPreference() {
        self.clearGuid()
        self.clearAccessToken()
        self.clearRefreshToken()
        self.clearTokenType()
        self.clearJti()
    }
    
    //MARK: - First Launch
    static func isFirstLaunch() -> Bool {
        return preferences.object(forKey: "USER_FIRST_LAUNCH") as? Bool ?? true
    }
    
    static func setFirstLaunch(isFirstLaunch: Bool?) {
        if isFirstLaunch == nil {
            clearFirstLaunch()
        } else {
            preferences.set(isFirstLaunch, forKey: "USER_FIRST_LAUNCH")
            preferences.synchronize()
        }
    }
    
    static func clearFirstLaunch() {
        preferences.set(true, forKey: "USER_FIRST_LAUNCH")
        preferences.synchronize()
    }
    
    //MARK: - GUID
    static func getGuid() -> String {
        return preferences.object(forKey: "USER_GUID") as? String ?? ""
    }
    
    static func setGuid(guid: String?) {
        if guid == nil {
            clearGuid()
        } else {
            preferences.set(guid, forKey: "USER_GUID")
            preferences.synchronize()
        }
    }
    
    static func clearGuid() {
        preferences.set("", forKey: "USER_GUID")
        preferences.synchronize()
    }
    
    //MARK: - ACCESS TOKEN
    static func getAccessToken() -> String {
        return preferences.object(forKey: "USER_ACCESS_TOKEN") as? String ?? ""
    }
    
    static func setAccessToken(accessToken: String?) {
        if accessToken == nil {
            clearAccessToken()
        } else {
            preferences.set(accessToken, forKey: "USER_ACCESS_TOKEN")
            preferences.synchronize()
        }
    }
    
    static func clearAccessToken() {
        preferences.set("", forKey: "USER_ACCESS_TOKEN")
        preferences.synchronize()
    }
    
    
    //MARK: - REFRESH TOKEN
    static func getRefreshToken() -> String {
        return preferences.object(forKey: "USER_REFRESH_TOKEN") as? String ?? ""
    }
    
    static func setRefreshToken(refreshToken: String?) {
        if refreshToken == nil {
            clearRefreshToken()
        } else {
            preferences.set(refreshToken, forKey: "USER_REFRESH_TOKEN")
            preferences.synchronize()
        }
    }
    
    static func clearRefreshToken() {
        preferences.set("", forKey: "USER_REFRESH_TOKEN")
        preferences.synchronize()
    }
    
    //MARK: - TOKEN TYPE
    static func getTokenType() -> String {
        return preferences.object(forKey: "USER_TOKEN_TYPE") as? String ?? ""
    }
    
    static func setTokenType(tokenType: String?) {
        if tokenType == nil {
            clearTokenType()
        } else {
            preferences.set(tokenType, forKey: "USER_TOKEN_TYPE")
            preferences.synchronize()
        }
    }
    
    static func clearTokenType() {
        preferences.set("", forKey: "USER_TOKEN_TYPE")
        preferences.synchronize()
    }
    
    //MARK: - JTI
    static func getJti() -> String {
        return preferences.object(forKey: "USER_JTI") as? String ?? ""
    }
    
    static func setJti(jti: String?) {
        if jti == nil {
            clearJti()
        } else {
            preferences.set(jti, forKey: "USER_JTI")
            preferences.synchronize()
        }
    }
    
    static func clearJti() {
        preferences.set("", forKey: "USER_JTI")
        preferences.synchronize()
    }
    
    //MARK: - DOCUMENTOS
    static func documentosJson() -> JSONArray {
        return preferences.object(forKey: "PARAMETER_TIPO_DOCUMENTO") as? [[String:Any]] ?? [[:]]
    }
    
    static func setDocumentosJson(_ dict: JSONArray) {
        preferences.set(dict, forKey: "PARAMETER_TIPO_DOCUMENTO")
        preferences.synchronize()
    }
    
    static func clearDocumentos() {
        preferences.set("", forKey: "PARAMETER_TIPO_DOCUMENTO")
        preferences.synchronize()
    }
    
    //MARK: - JTI
    static func getReloadData() -> Bool {
        return preferences.object(forKey: "HISTORY_RELOAD_DATA") as? Bool ?? false
    }
    
    static func setReloadData(reloadData: Bool?) {
        if reloadData == nil {
            clearReloadData()
        } else {
            preferences.set(reloadData, forKey: "HISTORY_RELOAD_DATA")
            preferences.synchronize()
        }
    }
    
    static func clearReloadData() {
        preferences.set("", forKey: "HISTORY_RELOAD_DATA")
        preferences.synchronize()
    }
    
    //MARK: - REFRESH TOKEN
    static func getTipoDocDesc() -> String {
        return preferences.object(forKey: "USER_TIPO_DOC_DESC") as? String ?? ""
    }
    
    static func setTipoDocDesc(tipoDocDesc: String?) {
        if tipoDocDesc == nil {
            clearTipoDocDesc()
        } else {
            preferences.set(tipoDocDesc, forKey: "USER_TIPO_DOC_DESC")
            preferences.synchronize()
        }
    }
    
    static func clearTipoDocDesc() {
        preferences.set("", forKey: "USER_TIPO_DOC_DESC")
        preferences.synchronize()
    }
    
    //MARK: - USER
    static func usuario() -> UsuarioLoginEntity {
        var json: [String: Any] = [:]
        
        json["tipo"] = preferences.object(forKey: "DATA_USER_TIPO") as? String ?? ""
        json["idUser"] = preferences.object(forKey: "DATA_USER_ID") as? Int ?? 0
        json["email"] = preferences.object(forKey: "DATA_USER_EMAIL") as? String ?? ""
        json["sexo"] = preferences.object(forKey: "DATA_USER_SEXO") as? String ?? ""
        json["tipoDoc"] = preferences.object(forKey: "DATA_USER_TIPO_DOC") as? String ?? ""
        json["nroDoc"] = preferences.object(forKey: "DATA_USER_NUM_DOC") as? String ?? ""
        json["nombres"] = preferences.object(forKey: "DATA_USER_NOMBRES") as? String ?? ""
        json["priApe"] = preferences.object(forKey: "DATA_USER_PRI_APE") as? String ?? ""
        json["segApe"] = preferences.object(forKey: "DATA_USER_SEG_APE") as? String ?? ""
        json["fecNac"] = preferences.object(forKey: "DATA_USER_FEC_NAC") as? String ?? ""
        json["nroCelular"] = preferences.object(forKey: "DATA_USER_NUM_CEL") as? String ?? ""
        json["status"] = preferences.object(forKey: "DATA_USER_STATUS") as? String ?? ""
        json["userKeyId"] = preferences.object(forKey: "DATA_USER_KEY_ID") as? String ?? ""
        json["userPhoto"] = preferences.object(forKey: "DATA_USER_PHOTO") as? String ?? ""
        json["createdAt"] = preferences.object(forKey: "DATA_CREATED_AT") as? String ?? ""

        let usuario = UsuarioLoginEntity(json: json)
        
        return usuario
    }
    
    static func setUsuario(_ usuario: UsuarioLoginEntity) {
        preferences.set(usuario.tipo, forKey: "DATA_USER_TIPO")
        preferences.synchronize()
        preferences.set(usuario.idUser, forKey: "DATA_USER_ID")
        preferences.synchronize()
        preferences.set(usuario.email, forKey: "DATA_USER_EMAIL")
        preferences.synchronize()
        preferences.set(usuario.sexo, forKey: "DATA_USER_SEXO")
        preferences.synchronize()
        preferences.set(usuario.tipoDoc, forKey: "DATA_USER_TIPO_DOC")
        preferences.synchronize()
        preferences.set(usuario.nroDoc, forKey: "DATA_USER_NUM_DOC")
        preferences.synchronize()
        preferences.set(usuario.nombres, forKey: "DATA_USER_NOMBRES")
        preferences.synchronize()
        preferences.set(usuario.priApe, forKey: "DATA_USER_PRI_APE")
        preferences.synchronize()
        preferences.set(usuario.segApe, forKey: "DATA_USER_SEG_APE")
        preferences.synchronize()
        preferences.set(usuario.fecNac, forKey: "DATA_USER_FEC_NAC")
        preferences.synchronize()
        preferences.set(usuario.nroCelular, forKey: "DATA_USER_NUM_CEL")
        preferences.synchronize()
        preferences.set(usuario.status, forKey: "DATA_USER_STATUS")
        preferences.synchronize()
        preferences.set(usuario.userKeyId, forKey: "DATA_USER_KEY_ID")
        preferences.synchronize()
        preferences.set(usuario.userPhoto, forKey: "DATA_USER_PHOTO")
        preferences.synchronize()
        preferences.set(usuario.createdAt, forKey: "DATA_CREATED_AT")
        preferences.synchronize()
    }
    
}
