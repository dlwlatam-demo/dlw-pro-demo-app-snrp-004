//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class CancelUserRequest: NSObject {
/*
 {"aaCont": "", "nuCont": ""}
    */
    var idRgst: Int = 0
    
    init(idRgst: Int) {
        self.idRgst = idRgst
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["idRgst": idRgst]
        print (bodyDic)
        return bodyDic
    }
    
}
