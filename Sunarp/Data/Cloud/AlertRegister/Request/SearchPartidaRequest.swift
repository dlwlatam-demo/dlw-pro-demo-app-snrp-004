//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class SearchPartidaRequest: NSObject {
/*
    {"region": " regPubId ", "ofic": " oficRegId ", "area": "21000",
    "partida": "numero", "ficha": "", "tomo": "", "foja": "",
    "partiSarp": "", "placa": "", "tipo": "P"}
    */
    var region: String = ""
    var ofic: String = ""
    var area: String = ""
    var partida: String = ""
    var ficha: String = ""
    var tomo: String = ""
    var foja: String = ""
    var partiSarp: String = ""
    var placa: String = ""
    var tipo: String = ""

    init(region: String,ofic: String, area: String, partida: String, ficha: String, tomo: String, foja: String, partiSarp: String, placa: String, tipo: String) {
        self.region = region
        self.ofic = ofic
        self.area = area
        self.partida = partida
        self.ficha = ficha
        self.tomo = tomo
        self.foja = foja
        self.partiSarp = partiSarp
        self.placa = placa
        self.tipo = tipo
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["region": region,"ofic": ofic,"area": area,"partida": partida,"ficha": ficha,"tomo": tomo,"foja": foja,"partiSarp": partiSarp,"placa": placa,"tipo": tipo]
        print (bodyDic)
        return bodyDic
    }
    
}
