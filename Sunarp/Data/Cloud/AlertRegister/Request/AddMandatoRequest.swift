//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class AddMandatoRequest: NSObject {
/*
 {"apPatePersNatu": "", "apMatePersNatu": "", "noPersNatu": "", "tiDocuIden": "", "nuDocu": ""}
    */
    var apPatePersNatu: String = ""
    var apMatePersNatu: String = ""
    var noPersNatu: String = ""
    var tiDocuIden: String = ""
    var nuDocu: String = ""

    init(apPatePersNatu: String,apMatePersNatu: String, noPersNatu: String, tiDocuIden: String, nuDocu: String) {
        self.apPatePersNatu = apPatePersNatu
        self.apMatePersNatu = apMatePersNatu
        self.noPersNatu = noPersNatu
        self.tiDocuIden = tiDocuIden
        self.nuDocu = nuDocu
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["apPatePersNatu": apPatePersNatu,"apMatePersNatu": apMatePersNatu,"noPersNatu": noPersNatu,"tiDocuIden": tiDocuIden,"nuDocu": nuDocu]
        print (bodyDic)
        return bodyDic
    }
    
}
