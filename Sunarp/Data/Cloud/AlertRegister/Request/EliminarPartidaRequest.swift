//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class EliminarPartidaRequest: NSObject {
/*
 {"aaCont": "", "nuCont": ""}
    */
    var aaCont: String = ""
    var nuCont: String = ""

    init(aaCont: String,nuCont: String) {
        self.aaCont = aaCont
        self.nuCont = nuCont
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["aaCont": aaCont,"nuCont": nuCont]
        print (bodyDic)
        return bodyDic
    }
    
}
