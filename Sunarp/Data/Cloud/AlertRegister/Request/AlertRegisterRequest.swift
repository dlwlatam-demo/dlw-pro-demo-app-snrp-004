//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class AlertRegisterRequest: NSObject {

    var idRgst: Int = 0
    var apPate: String = ""
    var apMate: String = ""
    var nombre: String = ""
    var tiDocu: String = ""
    var noDocu: String = ""
    var direccion: String = ""
    var cnumCel: Int = 0
    var idOperTele: String = ""
    var flEnvioSms: String = ""

    init(idRgst: Int,apPate: String, apMate: String, nombre: String, tiDocu: String, noDocu: String, direccion: String, cnumCel: Int, idOperTele: String, flEnvioSms: String) {
        self.idRgst = idRgst
        self.apPate = apPate
        self.apMate = apMate
        self.nombre = nombre
        self.tiDocu = tiDocu
        self.noDocu = noDocu
        self.direccion = direccion
        self.cnumCel = cnumCel
        self.idOperTele = idOperTele
        self.flEnvioSms = flEnvioSms
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["idRgst": idRgst,"apPate": apPate,"apMate": apMate,"nombre": nombre,"tiDocu": tiDocu,"noDocu": noDocu,"direccion": direccion,"cnumCel": cnumCel,"idOperTele": idOperTele,"flEnvioSms": flEnvioSms]
        print (bodyDic)
        return bodyDic
    }
    
}
