//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class AddPartidaRequest: NSObject {
/*
 {"coRegi": "", "coSede": "", "coArea": "", "nuPart": "", "numPlaca": "", "nuPartSarp": "", "coLibr": ""}
    */
    var coRegi: String = ""
    var coSede: String = ""
    var coArea: String = ""
    var nuPart: String = ""
    var numPlaca: String = ""
    var nuPartSarp: String = ""
    var coLibr: String = ""

    init(coRegi: String,coSede: String, coArea: String, nuPart: String, numPlaca: String, nuPartSarp: String, coLibr: String) {
        self.coRegi = coRegi
        self.coSede = coSede
        self.coArea = coArea
        self.nuPart = nuPart
        self.numPlaca = numPlaca
        self.nuPartSarp = nuPartSarp
        self.coLibr = coLibr
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["coRegi": coRegi,"coSede": coSede,"coArea": coArea,"nuPart": nuPart,"numPlaca": numPlaca,"nuPartSarp": nuPartSarp,"coLibr": coLibr]
        print (bodyDic)
        return bodyDic
    }
    
}
