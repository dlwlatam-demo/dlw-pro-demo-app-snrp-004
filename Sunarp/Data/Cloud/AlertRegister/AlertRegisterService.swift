//
//  RecoveryService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class AlertRegisterService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func postSolicitudCodigo(solicitud: SolicitudRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postSolicitudCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: solicitud.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postValidarCodigo(validacion: ValidacionRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postValidacionCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func putActualizarContrasena(contrasena: ActualizarContrasenaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.putActualizaContrasena()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: contrasena.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
   
    /*
    func getListaTive(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Tivet.getBusquedaHistory()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }*/
    
  //JsonResponse
    func getLoadUser(_ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Alert.getLoadUser()
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func putUpdateUser(update: AlertRegisterRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.putUpdateUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: update.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    
    func putDeletePartida(eliminar: EliminarPartidaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.putDeletePartida()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: eliminar.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func putCancelUser(eliminar: CancelUserRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.putCancelUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: eliminar.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func putDeleteMandato(eliminar: EliminarPartidaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.putDeleteMandato()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: eliminar.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func postAlertAddPartida(addPartida: AddPartidaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.postAlertAddPartida()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: addPartida.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func postAddMandatoPartida(mandato: AddMandatoRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Alert.postAddMandatoPartida()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: mandato.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
                          
    /*
    func getSelectAllPartidaFromUser(_ handler: @escaping ArrayJsonResponse) {

        let urlString = SunarpWebService.Alert.getSelectAllPartidaFromUser()
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let arrayResult = jsonResponse as? JSONArray ?? [:]
                handler(arrayResult)
            }
    }*/
    
    func getSelectAllPartidaFromUser(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Alert.getSelectAllPartidaFromUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaNotifications(aaCont: String, nuCont: String, tipo: String, source: String,_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Alert.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: tipo, source: source)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
   
    func postSearchPartida(search: SearchPartidaRequest, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Alert.postSearchPartida()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: search.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSONArray ?? []
            handler(json)
        }
    }
    func getSelectAllMandatoFromUser(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Alert.getSelectAllMandatoFromUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let json = jsonResponse as? JSONArray ?? []
            handler(json)
        }
    }
    /*
     func getSelectAllMandatoFromUser(_ handler: @escaping JsonResponse) {
     
         let urlString = SunarpWebService.Alert.getSelectAllMandatoFromUser()
         
         WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
             
             let json = jsonResponse as? JSON ?? [:]
             handler(json)
         }
     }
     */
    
    
    func getLoadUserByEmailChnPwd(email: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getLoadUserByEmailChnPwd(email: email)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    func postAlertRegValidarCodigo(validacion: RegisterAlertRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postAlertRegCreateUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }

    func getAlertSendValidationCode(guid: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertSendValidationCode(guid: guid)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    //coNoti
    func getAlertActivateUserById(guid: String,coNoti: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertRegInsertValidacionCodigo(guid: guid,coNoti: coNoti)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    
    func getAlertRegActualizarContrasena(password: String,guid: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertRegActualizarContrasena(password: password,guid: guid)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func putAlertRegActualizarContrasena(contrasena: ActualizarContrasenaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.putActualizaContrasena()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: contrasena.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
}
