//
//  RecoveryModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class AlertRegisterModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func postSolicitudCodigo(_ correoDestino: String, handler: @escaping ResponseModel) {
        
        let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postSolicitudCodigo(solicitud: solicitud)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidacionCodigo(codigoValidacion: String, guid: String, handler: @escaping ResponseModel) {
        
        let validacion = ValidacionRecoveryRequest(codigoValidacion: codigoValidacion, guid: guid)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postValidarCodigo(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func putActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
        let actualizarContrasena = ActualizarContrasenaRequest(password: password, guid: guid)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putActualizarContrasena(contrasena: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    
    
    
    
    
    
    
    //loadUser
    func getLoadUser(handler: @escaping ResponseAlertModel) {
        let service = AlertRegisterService(navigationController:navigationController)
        service.getLoadUser()  { (jsonResponse) in
            let objSolicitud = LoadUserEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
   
    
    func putUpdateUser(idRgst: Int, apPate: String, apMate: String, nombre: String, tiDocu: String, noDocu: String, direccion: String, cnumCel: Int, idOperTele: String, flEnvioSms: String, handler: @escaping RegistroModel) {
        
        let actualizarContrasena = AlertRegisterRequest(idRgst: idRgst, apPate: apPate, apMate: apMate, nombre: nombre, tiDocu: tiDocu, noDocu: noDocu, direccion: direccion, cnumCel: cnumCel, idOperTele: idOperTele, flEnvioSms: flEnvioSms)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putUpdateUser(update: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    //selectAllPartidaFromUser
    func getSelectAllPartidaFromUser(handler: @escaping AllPartidaFromUserModel) {
        
      
        let service = AlertRegisterService(navigationController:navigationController)
    service.getSelectAllPartidaFromUser() { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [AllPartidaFromUserEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = AllPartidaFromUserEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
          
            handler(arrayTipoDocumentos)
        }
        
    }
    //getSelectAllMandatoFromUser
    func getSelectAllMandatoFromUser(handler: @escaping AllMandatoFromUserModel) {
        /*
            let service = AlertRegisterService(navigationController:navigationController)
    service.getSelectAllMandatoFromUser()  { (jsonResponse) in
                let objSolicitud = AllMandatoFromUserEntity(json: jsonResponse)
                handler(objSolicitud)
            }*/
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.getSelectAllMandatoFromUser()  { (arrayJsonResponse) in
            var arrayTipoDocumentos = [AllMandatoFromUserEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = AllMandatoFromUserEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
          
            handler(arrayTipoDocumentos)
        }
            
    }
    //getListaNotifications
    
    func getListaNotifications(aaCont: String, nuCont: String, tipo: String, source: String, callback: @escaping ([AlertNotificationEntity]?) -> ()) {
        let service = AlertRegisterService(navigationController:navigationController)
    service.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: tipo, source: source, { arrayJsonResponse in
            var arrayTipoDocumentos: [AlertNotificationEntity] = []
            print(arrayJsonResponse)
            for jsonResponse in arrayJsonResponse {
                
                let objJson = AlertNotificationEntity(json: jsonResponse)
                print(objJson)
                arrayTipoDocumentos.append(objJson)
            }
            print(arrayTipoDocumentos.count)
            callback(arrayTipoDocumentos)
        })
    }

    //SearchPartida
    func postSearchPartida(region: String,ofic: String, area: String, partida: String, ficha: String, tomo: String, foja: String, partiSarp: String, placa: String, tipo: String, handler: @escaping SearchPArtidaModel) {
        
        let validacion = SearchPartidaRequest(region: region,ofic: ofic, area: area, partida: partida, ficha: ficha, tomo: tomo, foja: foja, partiSarp: partiSarp, placa: placa, tipo: tipo)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postSearchPartida(search: validacion)  { (jsonResponse) in
            
            //let objSolicitud = SearchPartidaEntity(json: jsonResponse)
            //handler(objSolicitud)
            
            var arrayTipoDocumentos = [SearchPartidaEntity]()
            for jsonResponse in jsonResponse {
                let objJson = SearchPartidaEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
          
            handler(arrayTipoDocumentos)
        }
        
    }
    
   
    
    func postAlertAddPartida(coRegi: String, coSede: String, coArea: String, nuPart: String, numPlaca: String, nuPartSarp: String, coLibr: String, handler: @escaping RegistroModel) {
        
        let actualizarContrasena = AddPartidaRequest(coRegi: coRegi, coSede: coSede, coArea: coArea, nuPart: nuPart, numPlaca: numPlaca, nuPartSarp: nuPartSarp, coLibr: coLibr)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postAlertAddPartida(addPartida: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
   // postAddMandatoPartida
    func postAddMandatoPartida(apPatePersNatu: String, apMatePersNatu: String, noPersNatu: String, tiDocuIden: String, nuDocu: String, handler: @escaping RegistroModel) {
        
        let actualizarContrasena = AddMandatoRequest(apPatePersNatu: apPatePersNatu, apMatePersNatu: apMatePersNatu, noPersNatu: noPersNatu, tiDocuIden: tiDocuIden, nuDocu: nuDocu)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postAddMandatoPartida(mandato: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    //deletePartida
    func putDeletePartida(aaCont: String, nuCont: String, handler: @escaping RegistroModel) {
        
        let eliminarPartida = EliminarPartidaRequest(aaCont: aaCont, nuCont: nuCont)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putDeletePartida(eliminar: eliminarPartida)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func putCancelUser(idRgst: Int, handler: @escaping RegistroModel) {
        
        let eliminarPartida = CancelUserRequest(idRgst: idRgst)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putCancelUser(eliminar: eliminarPartida)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    //deletePartida
    func putDeleteMandato(aaCont: String, nuCont: String, handler: @escaping RegistroModel) {
        
        let eliminarPartida = EliminarPartidaRequest(aaCont: aaCont, nuCont: nuCont)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putDeleteMandato(eliminar: eliminarPartida)  { (jsonResponse) in
            let objSolicitud = RegistroEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    
  
    //loadUserByEmailChnPwd
    func getLoadUserByEmailChnPwd(_ email: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.getLoadUserByEmailChnPwd(email: email)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
        
    func postAlertRegValidacionCodigo(email: String,appVersion: String, guid: String, handler: @escaping ResponseModel) {
        
        let validacion = RegisterAlertRecoveryRequest(email: email,appVersion: appVersion,  guid: guid)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.postAlertRegValidarCodigo(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertSendValidationCode(_ guid: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.getAlertSendValidationCode(guid: guid)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertActivateUserById(_ guid: String,coNoti: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.getAlertActivateUserById(guid: guid,coNoti:coNoti)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertRegActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.getAlertRegActualizarContrasena(password: password, guid: guid)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func putAlertRegActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
        let actualizarContrasena = ActualizarContrasenaRequest(password: password, guid: guid)
        
        let service = AlertRegisterService(navigationController:navigationController)
    service.putAlertRegActualizarContrasena(contrasena: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
}
