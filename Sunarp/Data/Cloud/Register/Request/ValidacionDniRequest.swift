//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ValidacionDniRequest: NSObject {

    var dni: String = ""
    var fecEmi: String = ""
    var guid: String = ""

    init(dni: String, fecEmi: String, guid: String) {
        self.dni = dni
        self.fecEmi = fecEmi
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["dni": dni, "fecEmi": fecEmi, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}

