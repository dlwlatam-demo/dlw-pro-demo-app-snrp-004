//
//  SolicitudRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import UIKit

class SolicitudRequest: NSObject {

    var numcelular: String = ""

    init(numcelular: String) {
        self.numcelular = numcelular
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["numcelular": numcelular]
        print (bodyDic)
        return bodyDic
    }
    
}
