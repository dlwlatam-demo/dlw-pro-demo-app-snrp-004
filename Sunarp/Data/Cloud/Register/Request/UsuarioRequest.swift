//
//  UsuarioRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import UIKit

class UsuarioRequest: NSObject {

    var email: String = ""
    var sexo: String = ""
    var tipoDoc: String = ""
    var nroDoc: String = ""
    var nombres: String = ""
    var priApe: String = ""
    var segApe: String = ""
    var fecNac: String = ""
    var nroCelular: String = ""
    var password: String = ""
    var geoLat: Double = 0.0
    var geoLong: Double = 0.0
    var appId: String = ""
    var guid: String = ""
    var appVersion: String = ""

    init(email: String, sexo: String, tipoDoc: String, nroDoc: String, nombres: String, priApe: String, segApe: String, fecNac: String, nroCelular: String, password: String, geoLat: Double, geoLong: Double, appId: String, guid: String, appVersion: String) {
        self.email = email
        self.sexo = sexo
        self.tipoDoc = tipoDoc
        self.nroDoc = nroDoc
        self.nombres = nombres
        self.priApe = priApe
        self.segApe = segApe
        self.fecNac = fecNac
        self.nroCelular = nroCelular
        self.password = password
        self.geoLat = geoLat
        self.geoLong = geoLong
        self.appId = appId
        self.guid = guid
        self.appVersion = appVersion
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["email": email, "sexo": sexo, "tipoDoc": tipoDoc, "nroDoc": nroDoc, "nombres": nombres, "priApe": priApe, "segApe": segApe, "fecNac": fecNac, "nroCelular": nroCelular, "password": password, "geoLat": geoLat, "geoLong": geoLong, "appId": appId, "guid": guid, "appVersion": appVersion]
        print (bodyDic)
        return bodyDic
    }
    
}
