//
//  ValidationRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import UIKit

class ValidacionRequest: NSObject {

    var codtemporal: String = ""
    var guid: String = ""

    init(codtemporal: String, guid: String) {
        self.codtemporal = codtemporal
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codtemporal": codtemporal, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}
