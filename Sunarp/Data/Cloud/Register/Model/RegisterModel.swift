//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class RegisterModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }


    func postSolicitudCodigo(_ numcelular: String, handler: @escaping ResponseModel) {
        
        let solicitud = SolicitudRequest(numcelular: numcelular)
        
            let service = RegisterService(navigationController:navigationController)
    service.postSolicitudCodigo(solicitud: solicitud)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidacionCodigo(codeOtp: String, guid: String, handler: @escaping ResponseModel) {
        
        let validacion = ValidacionRequest(codtemporal: codeOtp, guid: guid)
        
            let service = RegisterService(navigationController:navigationController)
    service.postValidarCodigo(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postRegistrarUsuario(email: String, sexo: String, tipoDoc: String, nroDoc: String, nombres: String, priApe: String, segApe: String, fecNac: String, nroCelular: String, password: String, geoLat: Double, geoLong: Double, appId: String, guid: String, appVersion: String, handler: @escaping RegistroModel) {
        
        let usuario = UsuarioRequest(email: email, sexo: sexo, tipoDoc: tipoDoc, nroDoc: nroDoc, nombres: nombres, priApe: priApe, segApe: segApe, fecNac: fecNac, nroCelular: nroCelular, password: password, geoLat: geoLat, geoLong: geoLong, appId: appId, guid: guid, appVersion: appVersion)
                
            let service = RegisterService(navigationController:navigationController)
    service.postRegistrarUsuario(usuario: usuario)  { (jsonResponse) in
            let objRegistro = RegistroEntity(json: jsonResponse)
            handler(objRegistro)
        }
        
    }
    
    func getListaTipoDocumentos(guid: String, handler: @escaping TipoDocumentodModel) {
        
            let service = RegisterService(navigationController:navigationController)
    service.getListaTipoDocumentos(guid: guid) { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [TipoDocumentoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoDocumentoEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoDocumentos.filter { $0.tipoDocId == "03" || $0.tipoDocId == "08" || $0.tipoDocId == "09" || $0.tipoDocId == "50" || $0.tipoDocId == "51"}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaTipoDocumentosInt(guid: String, handler: @escaping TipoDocumentodModel) {
        
            let service = RegisterService(navigationController:navigationController)
    service.getListaTipoDocumentosInt(guid: guid) { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [TipoDocumentoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoDocumentoEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoDocumentos.filter { $0.tipoDocId == "03" || $0.tipoDocId == "08" || $0.tipoDocId == "09" || $0.tipoDocId == "50" || $0.tipoDocId == "51"}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    func getListaTipoDocumentosJur(guid: String, handler: @escaping TipoDocumentodModel) {
        
            let service = RegisterService(navigationController:navigationController)
    service.getListaTipoDocumentosInt(guid: guid) { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [TipoDocumentoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoDocumentoEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoDocumentos.filter { $0.tipoDocId == "05" || $0.tipoDocId == "00" || $0.tipoDocId == "90" || $0.tipoDocId == "91" || $0.tipoDocId == "92"}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    func getListaObtenerLibro(areaRegId: String, handler: @escaping ObtenerLibroModel) {
        
            let service = RegisterService(navigationController:navigationController)
    service.getListaObtenerLibro(areaRegId: areaRegId) { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [ObtenerLibroEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = ObtenerLibroEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
           // let arrayTipoDocumentosFiltered = arrayTipoDocumentos.filter { $0.tipoDocId == "03" || $0.tipoDocId == "08" || $0.tipoDocId == "09" || $0.tipoDocId == "50" || $0.tipoDocId == "51"}
            //handler(arrayTipoDocumentosFiltered)
            handler(arrayTipoDocumentos)
        }
    }
    func postValidarDni(dni: String, fecEmi: String, guid: String, handler: @escaping DniResponseModel) {
        
        let validacion = ValidacionDniRequest(dni: dni, fecEmi: fecEmi, guid: guid)
        
            let service = RegisterService(navigationController:navigationController)
    service.postValidarDni(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionDniEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidarCe(ce: String, guid: String, handler: @escaping CeResponseModel) {
        
        let validacion = ValidacionCeRequest(ce: ce, guid: guid)
        
            let service = RegisterService(navigationController:navigationController)
    service.postValidarCe(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionCeEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func postValidarDniConsul(dni: String, fecEmi: String, guid: String, handler: @escaping DniResponseModel) {
        
        let validacion = ValidacionDniRequest(dni: dni, fecEmi: fecEmi, guid: guid)
        
            let service = RegisterService(navigationController:navigationController)
    service.postValidarDniConsul(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionDniEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidarCeConsul(ce: String, guid: String, handler: @escaping CeResponseModel) {
        
        let validacion = ValidacionCeRequest(ce: ce, guid: guid)
        
            let service = RegisterService(navigationController:navigationController)
    service.postValidarCeConsul(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionCeEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
}
