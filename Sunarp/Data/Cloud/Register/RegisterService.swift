//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class RegisterService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func postSolicitudCodigo(solicitud: SolicitudRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Register.postSolicitudCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: solicitud.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postValidarCodigo(validacion: ValidacionRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Register.postValidacionCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postRegistrarUsuario(usuario: UsuarioRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Register.postRegistrarUsuario()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: usuario.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getListaTipoDocumentos(guid: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Register.getListaTipoDocumentos(guid: guid)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoDocumentosInt(guid: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Register.getListaTipoDocumentosInt(guid: guid)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    func getListaObtenerLibro(areaRegId: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Register.getListaObtenerLibro(areaRegId: areaRegId)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func postValidarDni(validacion: ValidacionDniRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Register.postValidarDni()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postValidarCe(validacion: ValidacionCeRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Register.postValidarCe()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func postValidarDniConsul(validacion: ValidacionDniRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postConsulValidarDni()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postValidarCeConsul(validacion: ValidacionCeRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postConsulValidarCe()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
}
