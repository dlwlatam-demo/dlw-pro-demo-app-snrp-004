//
//  LoginModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class CommentModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    

    
    func postComment(txt: String, img: String,appName: String, handler: @escaping CommentResponseModel) {
        
        let commentRequest = CommentRequest(txt: txt, img: img, appName: appName)
        
        let service = CommentService(navigationController:navigationController)
        service.postComment(comment: commentRequest)  { (jsonResponse) in
            let objComment = ComentarioEntity(json: jsonResponse)
            handler(objComment)
        }
        
    }
    
 
    
}
