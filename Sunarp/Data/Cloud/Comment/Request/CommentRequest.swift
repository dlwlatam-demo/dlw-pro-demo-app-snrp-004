//
//  LoginRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class CommentRequest: NSObject {

    var txt: String = ""
    var img: String = ""
    var appName: String = ""
    

    init(txt: String, img: String, appName: String) {
        self.txt = txt
        self.img = img
        self.appName = appName
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["txt": txt, "img": img, "appName": appName]
        print (bodyDic)
        return bodyDic
    }
    
}
