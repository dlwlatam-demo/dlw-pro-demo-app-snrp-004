//
//  LoginService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class CommentService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func postComment(comment: CommentRequest, _ handler: @escaping JsonResponse) {
        let urlString = SunarpWebService.Comment.postComment()
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: comment.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
}
