//
//  ValidacionCeRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsulValidacionCeRequest: NSObject {

    var ce: String = ""
    var guid: String = ""

    init(ce: String, guid: String) {
        self.ce = ce
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["ce": ce, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}

