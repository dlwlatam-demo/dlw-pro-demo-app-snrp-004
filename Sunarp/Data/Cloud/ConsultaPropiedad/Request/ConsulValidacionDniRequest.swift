//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsulValidacionDniRequest: NSObject {

    var dni: String = ""
    var fecEmi: String = ""

    init(dni: String, fecEmi: String) {
        self.dni = dni
        self.fecEmi = fecEmi
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["dni": dni, "fecEmi": fecEmi]
        print (bodyDic)
        return bodyDic
    }
    
}

