//
//  ConsultaDetailRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

class ConsultaDetailRequest: NSObject {

    var tipo: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombres: String = ""
    var razonSocial: String = ""
    var userApp: String = ""
    var typeDoc: String = ""
    var numbDoc: String = ""
    var devideId: String = ""
    var userCrea: String = ""

    init(tipo: String, apePaterno: String, apeMaterno: String, nombres: String, razonSocial: String, userApp: String, typeDoc: String, numbDoc: String, devideId: String, userCrea: String) {
        self.tipo = tipo
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombres = nombres
        self.razonSocial = razonSocial
        self.userApp = userApp
        self.typeDoc = typeDoc
        self.numbDoc = numbDoc
        self.devideId = devideId
        self.userCrea = userCrea
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["tipo": tipo, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombres": nombres, "razonSocial": razonSocial, "userApp": userApp, "typeDoc": typeDoc, "numbDoc": numbDoc, "devideId": devideId, "userCrea": userCrea]
        print (bodyDic)
        return bodyDic
    }
    
}
