//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class ConsultaPropiedadModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    
    func postListarConsultaPropiedad(tipo: String, apePaterno: String, apeMaterno: String, nombres: String, razonSocial: String, userApp: String, typeDoc: String, numbDoc: String, devideId: String, userCrea: String, handler: @escaping ConsultarPropiedadModel) {
        
        let listaConsultaPropiedad = ConsultaDetailRequest(tipo: tipo, apePaterno: apePaterno, apeMaterno: apeMaterno, nombres: nombres, razonSocial: razonSocial, userApp: userApp, typeDoc: typeDoc, numbDoc: numbDoc, devideId: devideId, userCrea: userCrea)
                
        let service = ConsultaPropiedadService(navigationController:navigationController)
         
      service.postListConsultaPropiedad(listaConsultaPropiedad: listaConsultaPropiedad)  { (jsonResponse) in
            let objConsulta = ConsultaPropiedadEntity(json: jsonResponse)
            handler(objConsulta)
        }
        
    }
    
    func postValidarDni(dni: String, fecEmi: String, guid: String, handler: @escaping DniResponseModel) {
        
        let validacion = ConsulValidacionDniRequest(dni: dni, fecEmi: fecEmi)
        
        let service = ConsultaPropiedadService(navigationController:navigationController)
         
      service.postConsultaValidarDni(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionDniEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidarCe(ce: String, guid: String, handler: @escaping CeResponseModel) {
        
        let validacion = ValidacionCeRequest(ce: ce, guid: guid)
        
        let service = ConsultaPropiedadService(navigationController:navigationController)
         
      service.postConsultaValidarCe(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ValidacionCeEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
}
