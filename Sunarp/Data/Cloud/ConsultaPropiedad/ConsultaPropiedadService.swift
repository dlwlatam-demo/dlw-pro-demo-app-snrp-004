//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class ConsultaPropiedadService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
   
    
    func postListConsultaPropiedad(listaConsultaPropiedad: ConsultaDetailRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postConsulListaPropiedad()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: listaConsultaPropiedad.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
   
    
    func postConsultaValidarDni(validacion: ConsulValidacionDniRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postConsulValidarDni()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postConsultaValidarCe(validacion: ValidacionCeRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postConsulValidarCe()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
}
