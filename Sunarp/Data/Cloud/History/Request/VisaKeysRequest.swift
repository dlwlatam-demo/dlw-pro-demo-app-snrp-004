//
//  VisaKeysRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation

class VisaKeysRequest: NSObject {

    var instancia: String = ""
    var accessAppKey: String = ""

    init(instancia: String, accessAppKey: String) {
        self.instancia = instancia
        self.accessAppKey = accessAppKey
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["instancia": instancia, "accessAppKey": accessAppKey]
        print (bodyDic)
        return bodyDic
    }
    
}
