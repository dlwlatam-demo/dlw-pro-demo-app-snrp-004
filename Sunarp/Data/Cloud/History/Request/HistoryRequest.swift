//
//  HistoryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

class HistoryRequest: NSObject {

    var solicitudId: String = ""
    var detalle: String = ""
    var usuario: String = ""

    init(solicitudId: String, detalle: String, usuario: String) {
        self.solicitudId = solicitudId
        self.detalle = detalle
        self.usuario = usuario
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["solicitudId": solicitudId, "detalle": detalle, "usuario": usuario]
        print (bodyDic)
        return bodyDic
    }
    
}
