//
//  LiquidacionRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

class LiquidacionRequest: NSObject {

    var solicitudId: Int = 0
    var usuario: String = ""
    var detalle: String = ""
    var monto: Double = 0.0
    var niubizData: NiubizRequest

    init(solicitudId: Int, usuario: String, detalle: String, monto: Double, niubizData: NiubizRequest) {
        self.solicitudId = solicitudId
        self.usuario = usuario
        self.detalle = detalle
        self.monto = monto
        self.niubizData = niubizData
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["solicitudId": solicitudId, "usuario": usuario, "detalle": detalle, "monto": monto, "niubizData": niubizData.bodyDictionary()]
        print (bodyDic)
        return bodyDic
    }
    
}
