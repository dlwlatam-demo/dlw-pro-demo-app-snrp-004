//
//  NiubizRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

class NiubizRequest: NSObject {

    var terminal: String = ""
    var traceNumber: String = ""
    var eciDescription: String = ""
    var signature: String = ""
    var card: String = ""
    var merchant: String = ""
    var status: String = ""
    var actionDescription: String = ""
    var idUnico: String = ""
    var amount: String = ""
    var authorizationCode: String = ""
    var transactionDate: String = ""
    var actionCode: String = ""
    var eci: String = ""
    var brand: String = ""
    var adquirente: String = ""
    var processCode: String = ""
    var transactionId: String = ""
    var recurrenceStatus: String = ""
    var recurrenceMaxAmount: String = ""

    init(terminal: String, traceNumber: String, eciDescription: String, signature: String, card: String, merchant: String, status: String, actionDescription: String, idUnico: String, amount: String, authorizationCode: String, transactionDate: String, actionCode: String, eci: String, brand: String, adquirente: String, processCode: String, transactionId: String, recurrenceStatus: String, recurrenceMaxAmount: String) {
        self.terminal = terminal
        self.traceNumber = traceNumber
        self.eciDescription = eciDescription
        self.signature = signature
        self.card = card
        self.merchant = merchant
        self.status = status
        self.actionDescription = actionDescription
        self.idUnico = idUnico
        self.amount = amount
        self.authorizationCode = authorizationCode
        self.transactionDate = transactionDate
        self.actionCode = actionCode
        self.eci = eci
        self.brand = brand
        self.adquirente = adquirente
        self.processCode = processCode
        self.transactionId = transactionId
        self.recurrenceStatus = recurrenceStatus
        self.recurrenceMaxAmount = recurrenceMaxAmount
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["terminal": terminal, "traceNumber": traceNumber, "eciDescription": eciDescription, "signature": signature, "card": card, "merchant": merchant, "status": status, "actionDescription": actionDescription, "idUnico": idUnico, "amount": amount, "authorizationCode": authorizationCode, "transactionDate": transactionDate, "actionCode": actionCode, "eci": eci, "brand": brand, "adquirente": adquirente, "processCode": processCode, "transactionId": transactionId, "recurrenceStatus": recurrenceStatus, "recurrenceMaxAmount": recurrenceMaxAmount]
        print (bodyDic)
        return bodyDic
    }
    
    override convenience init() {
        self.init(terminal : "",traceNumber : "",eciDescription : "",signature : "",card : "",merchant : "",status : "",actionDescription : "",idUnico : "",amount : "",authorizationCode : "",transactionDate : "",actionCode : "",eci : "",brand : "",adquirente : "",processCode : "",transactionId : "",recurrenceStatus : "",recurrenceMaxAmount : "")
    }
    
}
