//
//  HistoryService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation
import UIKit

class HistoryService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func getHistorial(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Solicitud.getHistorial()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (arrayJsonResponse) in
            
            let arrayJson = arrayJsonResponse as? JSONArray ?? [[:]]
            handler(arrayJson)
        }
    }
        
    func getHistorialDetalle(id: String, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.getHistorialDetalle().replacingOccurrences(of: "{id}", with: id)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getHistorialDetalleAnio(anio:String,id: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Solicitud.getHistorialDetalleAnio(anio: anio,id: id)
      //  urlString = urlString.replacingOccurrences(of: "{anio}", with: anio)
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        let json = jsonResponse as? JSON ?? [:]
        handler(json)
        }
    }
    
    func postAclaramiento(aclaramiento: HistoryRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.postAclaramiento()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: aclaramiento.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postSubsanacion(subsanacion: HistoryRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.postSubsanacion()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: subsanacion.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postDesestimiento(desestimiento: HistoryRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.postDesestimiento()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: desestimiento.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getDocumento(solicitudId: String, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.getDocumento().replacingOccurrences(of: "{id}", with: solicitudId)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postLiquidacion(liquidacion: LiquidacionRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.postLiquidacion()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: liquidacion.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getBoleta(transId: String, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Solicitud.getBoleta().replacingOccurrences(of: "{transId}", with: transId)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postNiubiz(loginRequest: LoginRequest, urlVisa: String, _ handler: @escaping StringResponse) {
    
        var urlString = urlVisa
        
        if (urlVisa.isEmpty) {
            urlString = SunarpWebService.Niubiz.postNiubiz()
        }
        
        WebServiceManager(navigationController:navigationController).niubizDataTask(urlString: urlString, params: loginRequest) { (stringResponse) in
            
            let token = stringResponse as? String ?? ""
            handler(token)
        }
    }
    
    func postNiubizPinHash(token: String, merchant: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Niubiz.postNiubizPinHash().replacingOccurrences(of: "{merchant}", with: merchant)
        
        WebServiceManager(navigationController:navigationController).niubizPinHashDataTask(urlString: urlString, token: token) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postVisaKeys(visaKeysRequest: VisaKeysRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Niubiz.postVisaKeys()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: visaKeysRequest.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getTransactionId(_ handler: @escaping StringResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getTransactionId()
        
        WebServiceManager(navigationController:navigationController).niubizTransactionDataTask(urlString: urlString) { (stringResponse) in
            
            let transactionId = stringResponse as? String ?? ""
            handler(transactionId)
        }
    }
}
