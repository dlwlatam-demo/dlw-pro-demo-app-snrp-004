//
//  HistoryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation
import UIKit

class HistoryModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    func getHistorial(handler: @escaping HistoryResponseModel) {
        let service = HistoryService(navigationController:navigationController)

        service.getHistorial { (arrayJsonResponse) in
            let arraySolicitud = HistoryEntity(jsonArray: arrayJsonResponse)
            handler(arraySolicitud)
        }
        
    }
    
    func getHistorialDetalle(id: String, handler: @escaping HistoryDetailResponseModel) {
        let service = HistoryService(navigationController:navigationController)
        service.getHistorialDetalle(id: id) { (jsonResponse) in
            let solicitudDetalle = HistoryDetailEntity(json: jsonResponse)
            handler(solicitudDetalle)
        }
    }
    
    func getHistorialDetalleAnio(anio:String,id: String, handler: @escaping HistoryDetailResponseModel) {
        let service = HistoryService(navigationController:navigationController)
        service.getHistorialDetalleAnio(anio: anio,id: id) { (jsonResponse) in
            let solicitudDetalle = HistoryDetailEntity(json: jsonResponse)
            handler(solicitudDetalle)
        }
    }
    
    func postAclaramiento(solicitudId: String, detalle: String, usuario: String, handler: @escaping ResponseModel) {
        let aclaramiento = HistoryRequest(solicitudId: solicitudId, detalle: detalle, usuario: usuario)
        let service = HistoryService(navigationController:navigationController)
        service.postAclaramiento(aclaramiento: aclaramiento) { (jsonResponse) in
            let aclaramientoResponse = ResponseEntity(json: jsonResponse)
            handler(aclaramientoResponse)
        }
    }
    
    func postSubsanacion(solicitudId: String, detalle: String, usuario: String, handler: @escaping ResponseModel) {
        let subsanacion = HistoryRequest(solicitudId: solicitudId, detalle: detalle, usuario: usuario)
        let service = HistoryService(navigationController:navigationController)
        service.postSubsanacion(subsanacion: subsanacion) { (jsonResponse) in
            let subsanacionResponse = ResponseEntity(json: jsonResponse)
            handler(subsanacionResponse)
        }
    }
    
    func postDesestimiento(solicitudId: String, detalle: String, usuario: String, handler: @escaping ResponseModel) {
        let desestimiento = HistoryRequest(solicitudId: solicitudId, detalle: detalle, usuario: usuario)
        let service = HistoryService(navigationController:navigationController)
        service.postDesestimiento(desestimiento: desestimiento) { (jsonResponse) in
            let desestimientoResponse = ResponseEntity(json: jsonResponse)
            handler(desestimientoResponse)
        }
    }
    
    func getDocumento(solicitudId: String, handler: @escaping DocumentoResponseModel) {
        let service = HistoryService(navigationController:navigationController)
        service.getDocumento(solicitudId: solicitudId) { (jsonResponse) in
            let documentoResponse = DocumentoEntity(json: jsonResponse)
            handler(documentoResponse)
        }
    }
    
    func postLiquidacion(solicitudId: Int, usuario: String, detalle: String, monto: Double, terminal: String, traceNumber: String, eciDescription: String, signature: String, card: String, merchant: String, status: String, actionDescription: String, idUnico: String, amount: String, authorizationCode: String, transactionDate: String, actionCode: String, eci: String, brand: String, adquirente: String, processCode: String, transactionId: String, recurrenceStatus: String, recurrenceMaxAmount: String, handler: @escaping LiquidacionResponseModel) {
        let niubiz = NiubizRequest(terminal: terminal, traceNumber: traceNumber, eciDescription: eciDescription, signature: signature, card: card, merchant: merchant, status: status, actionDescription: actionDescription, idUnico: idUnico, amount: amount, authorizationCode: authorizationCode, transactionDate: transactionDate, actionCode: actionCode, eci: eci, brand: brand, adquirente: adquirente, processCode: processCode, transactionId: transactionId, recurrenceStatus: recurrenceStatus, recurrenceMaxAmount: recurrenceMaxAmount)
        let liquidacion = LiquidacionRequest(solicitudId: solicitudId, usuario: usuario, detalle: detalle, monto: monto, niubizData: niubiz)
        let service = HistoryService(navigationController:navigationController)
        service.postLiquidacion(liquidacion: liquidacion) { (jsonResponse) in
            let liquidacionResponse = LiquidacionEntity(json: jsonResponse)
            handler(liquidacionResponse)
        }
        
    }
    
    func getBoleta(transId: String, handler: @escaping BoletaResponseModel) {
        let service = HistoryService(navigationController:navigationController)
        service.getBoleta(transId: transId) { (jsonResponse) in
            let boletaResponse = BoletaEntity(json: jsonResponse)
            handler(boletaResponse)
        }
        
    }
    
    func postNiubiz(userName: String, password: String, urlVisa: String, handler: @escaping NiubizResponse) {
        let loginRequest = LoginRequest(username: userName, password: password)
        let service = HistoryService(navigationController:navigationController)
        service.postNiubiz(loginRequest: loginRequest, urlVisa: urlVisa) { (stringResponse) in
            handler(stringResponse)
        }
        
    }
    
    func postNiubizPinHash(token: String, merchant: String, handler: @escaping NiubizPinHashResponse) {
        let service = HistoryService(navigationController:navigationController)
        service.postNiubizPinHash(token: token, merchant: merchant) { (jsonResponse) in
            let pinHashResponse = NiubizPinHashEntity(json: jsonResponse)
            handler(pinHashResponse)
        }
        
    }
    
    func postVisaKeys(instancia: String, accessAppKey: String, handler: @escaping VisaKeysResponse) {
        let visaKeysRequest = VisaKeysRequest(instancia: instancia, accessAppKey: accessAppKey)
        let service = HistoryService(navigationController:navigationController)
        service.postVisaKeys(visaKeysRequest: visaKeysRequest) { (jsonResponse) in
            let visaKeysResponse = VisaKeysEntity(json: jsonResponse)
            handler(visaKeysResponse)
        }
        
    }
    
    func getTransactionId(handler: @escaping NiubizResponse) {
        let service = HistoryService(navigationController:navigationController)
        service.getTransactionId() { (stringResponse) in
            handler(stringResponse)
        }
        
    }
    
}
