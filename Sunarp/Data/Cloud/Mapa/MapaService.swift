//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class MapaService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
   
    
    func getListaMapa(guid: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Mapa.getMapa()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    
  
    
    
    
}
