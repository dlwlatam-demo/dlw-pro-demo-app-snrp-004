//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class MapaModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    
    func getListaMapa(guid: String, handler: @escaping MapaResponseModel) {
        
        let service = MapaService(navigationController:navigationController)
        service.getListaMapa(guid: guid) { (jsonResponse) in
            
           // var arrayMap = [MapaEntity]()
            //for jsonResponse in arrayJsonResponse {
                let objJson = MapaEntity(json: jsonResponse)
                handler(objJson)
             //   arrayMap.append(objJson)
          //  }
           // let arrayTipoDocumentosFiltered = arrayTipoDocumentos.filter { $0.tipoDocId == "09" || $0.tipoDocId == "03" }
           // handler(arrayTipoDocumentosFiltered)
           // handler(arrayMap)
            
        }
    }
    
}
