//
//  ConsultaPublicCertiVigeDARequest.swift
//  Sunarp
//

import UIKit

class ConsultaPublicCertiVigeDARequest: NSObject {
    var codCerti: String = ""
    var codArea: String = ""
    var codLibro: String = ""
    var oficinaOrigen: String = ""
    var refNumPart: String = ""
    var refNumPartMP: String = ""
    var partida: String = ""
    var tpoPersona: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombre: String = ""
    var razSoc: String = ""
    var tpoDoc: String = ""
    var numDoc: String = ""
    var email: String = ""
    var asiento: String = ""
    var costoServicio: String = ""
    var usrId: String = ""
    var formaEnvio: String = "V"
    var numPartidaMP: String = ""
    var numAsientoMP: String = ""
    var tomo: String = ""
    var folio: String = ""
    
    init(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String, numPartidaMP: String, numAsientoMP: String,  costoServicio: String, costoTotal: String, usrId: String, tomo: String, folio: String) {
        
        self.codCerti = codCerti
        self.codArea = codArea
        self.oficinaOrigen = oficinaOrigen
        self.refNumPart = refNumPart
        self.refNumPartMP = refNumPartMP
        self.partida = partida
        self.tpoPersona = tpoPersona
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombre = nombre
        self.razSoc = razSoc
        self.tpoDoc = tpoDoc
        self.numDoc = numDoc
        self.email = email
        self.asiento = asiento
        self.numPartidaMP = numPartidaMP
        self.numAsientoMP = numAsientoMP
        self.costoServicio = costoServicio
        self.usrId = usrId
        self.formaEnvio = "V"
        self.tomo = tomo
        self.folio = folio
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codCerti": codCerti, "codArea": codArea, "codLibro": codLibro, "oficinaOrigen": oficinaOrigen, "refNumPart": refNumPart,  "refNumPartMP": refNumPartMP,"partida": partida, "tpoPersona": tpoPersona, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombre": nombre, "razSoc": razSoc, "tpoDoc": tpoDoc, "numDoc": numDoc, "email": email, "asiento": asiento,"numPartidaMP": numPartidaMP,"numAsientoMP": numAsientoMP,"costoServicio": costoServicio,  "usrId": usrId, "formaEnvio":formaEnvio, "tomo":tomo, "folio":folio]
        print (bodyDic)
        return bodyDic
    }
    
}
