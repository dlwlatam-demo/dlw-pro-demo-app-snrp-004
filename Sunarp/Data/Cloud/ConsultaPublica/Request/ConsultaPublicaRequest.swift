//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsultaPublicaRequest: NSObject {
    //  "usrId": "APPSNRPANDRO"}
    var codCerti: String = ""
    var codArea: String = ""
    var oficinaOrigen: String = ""
    var tpoPersona: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombre: String = ""
    var razSoc: String = ""
    var tpoDoc: String = ""
    var numDoc: String = ""
    var email: String = ""
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var usrId: String = ""
    var costoServicio: String = ""
    var costoTotal: String = ""
    var formaEnvio: String = "V"
    

    init(codCerti: String, codArea: String, oficinaOrigen: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, tipPerPN: String, apePatPN: String, apeMatPN: String, nombPN: String, razSocPN: String, tipoDocPN: String, numDocPN: String, costoServicio: String, costoTotal: String, usrId: String) {
        self.codCerti = codCerti
        self.codArea = codArea
        self.oficinaOrigen = oficinaOrigen
        self.tpoPersona = tpoPersona
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombre = nombre
        self.razSoc = razSoc
        self.tpoDoc = tpoDoc
        self.numDoc = numDoc
        self.email = email
        self.tipPerPN = tipPerPN
        self.apePatPN = apePatPN
        self.apeMatPN = apeMatPN
        self.nombPN = nombPN
        self.razSocPN = razSocPN
        self.tipoDocPN = tipoDocPN
        self.numDocPN = numDocPN
        self.costoServicio = costoServicio
        self.costoTotal = costoTotal
        self.usrId = usrId
        self.formaEnvio = "V"
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codCerti": codCerti, "codArea": codArea, "oficinaOrigen": oficinaOrigen, "tpoPersona": tpoPersona, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombre": nombre, "razSoc": razSoc, "tpoDoc": tpoDoc, "numDoc": numDoc, "email": email, "tipPerPN": tipPerPN, "apePatPN": apePatPN, "apeMatPN": apeMatPN, "nombPN": nombPN, "razSocPN": razSocPN, "tipoDocPN": tipoDocPN, "numDocPN": numDocPN, "costoServicio": costoServicio, "costoTotal": costoTotal, "usrId": usrId,"formaEnvio":"V"]
        print (bodyDic)
        return bodyDic
    }
    
}

