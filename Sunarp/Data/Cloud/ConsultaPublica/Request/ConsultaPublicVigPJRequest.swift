//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsultaPublicVigPJRequest: NSObject {
    //  "usrId": "APPSNRPANDRO"}
    var codCerti: String = ""
    var codArea: String = ""
    var codLibro: String = ""
    var oficinaOrigen: String = ""
    var refNumPart: String = ""
    var refNumPartMP: String = ""
    var partida: String = ""
    var ficha: String = ""
    var tomo: String = ""
    var folio: String = ""
    var placa: String = ""
    var matricula: String = ""
    var nomEmbarcacion: String = ""
    var expediente: String = ""
    var tpoPersona: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombre: String = ""
    var razSoc: String = ""
    var tpoDoc: String = ""
    var numDoc: String = ""
    var email: String = ""
    var asiento: String = ""
    var tipPerVP: String = ""
    var apePateVP: String = ""
    var apeMateVP: String = ""
    var nombVP: String = ""
    var razSocVP: String = ""
    var cargoApoderado: String = ""
    var datoAdic: String = ""
    var costoServicio: String = ""
    var usrId: String = ""
    var formaEnvio: String = "V"

    init(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tomo: String, folio: String, placa: String, matricula: String, nomEmbarcacion: String, expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String,tipPerVP: String,apePateVP: String,apeMateVP: String,nombVP: String,razSocVP: String,cargoApoderado: String,datoAdic: String,costoServicio: String, usrId: String) {
        
        self.codCerti = codCerti
        self.codArea = codArea
        self.codLibro = codLibro
        self.oficinaOrigen = oficinaOrigen
        self.refNumPart = refNumPart
        self.refNumPartMP = refNumPartMP
        self.partida = partida
        self.ficha = ficha
        self.tomo = tomo
        self.folio = folio
        self.placa = placa
        self.matricula = matricula
        self.nomEmbarcacion = nomEmbarcacion
        self.expediente = expediente
        self.tpoPersona = tpoPersona
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombre = nombre
        self.razSoc = razSoc
        self.tpoDoc = tpoDoc
        self.numDoc = numDoc
        self.email = email
        self.asiento = asiento
        self.tipPerVP = tipPerVP
        self.apePateVP = apePateVP
        self.apeMateVP = apeMateVP
        self.nombVP = nombVP
        self.razSocVP = razSocVP
        self.cargoApoderado = cargoApoderado
        self.datoAdic = datoAdic
        self.costoServicio = costoServicio
        self.usrId = usrId
        self.formaEnvio = "V"
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codCerti": codCerti, "codArea": codArea, "codLibro": codLibro, "oficinaOrigen": oficinaOrigen, "refNumPart": refNumPart,  "refNumPartMP": refNumPartMP,"partida": partida, "ficha": ficha, "tomo": tomo,"folio": folio,"placa": placa,"matricula": matricula,"nomEmbarcacion": nomEmbarcacion,"expediente": expediente,"tpoPersona": tpoPersona, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombre": nombre, "razSoc": razSoc, "tpoDoc": tpoDoc, "numDoc": numDoc, "email": email, "asiento": asiento,"tipPerVP": tipPerVP,"apePateVP": apePateVP,"apeMateVP": apeMateVP,"nombVP": nombVP,"razSocVP": razSocVP,"cargoApoderado": cargoApoderado,"datoAdic": datoAdic,"costoServicio": costoServicio,  "usrId": usrId, "formaEnvio":formaEnvio]
        print (bodyDic)
        return bodyDic
    }
    
}

