//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit


class ConsulPublicaRequest: NSObject {
    //  "usrId": "APPSNRPANDRO"}
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var numPart: String = ""
    var codGrupo: String = ""
    var codigoLibro: String = ""
    var tipoBusqueda: String = ""
    var formaEnvio: String = "V"
    

    init(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, codGrupo: String, codigoLibro: String, tipoBusqueda: String) {
        self.regPubId = regPubId
        self.oficRegId = oficRegId
        self.areaRegId = areaRegId
        self.numPart = numPart
        self.codGrupo = codGrupo
        self.codigoLibro = codigoLibro
        self.tipoBusqueda = tipoBusqueda
        
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["regPubId": regPubId, "oficRegId": oficRegId, "areaRegId": areaRegId, "numPart": numPart, "codGrupo": codGrupo, "codigoLibro": codigoLibro, "tipoBusqueda": tipoBusqueda]
        print (bodyDic)
        return bodyDic
    }
    
}

