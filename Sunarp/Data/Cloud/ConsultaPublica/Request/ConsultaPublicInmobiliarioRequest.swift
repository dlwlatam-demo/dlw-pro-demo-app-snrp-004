//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit


class ConsultaPublicInmobiliarioRequest: NSObject {
    //  "usrId": "APPSNRPANDRO"}
    var codCerti: String = ""
    var codArea: String = ""
    var codLibro: String = ""
    var oficinaOrigen: String = ""
    var refNumPart: String = ""
    var refNumPartMP: String = ""
    var partida: String = ""
    var ficha: String = ""
    var tpoPersona: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombre: String = ""
    var razSoc: String = ""
    var tpoDoc: String = ""
    var numDoc: String = ""
    var email: String = ""
    var costoServicio: String = ""
    var ipRemote: String = ""
    var sessionId: String = ""
    var usrId: String = ""
    var formaEnvio: String = "V"

    init(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String) {
        self.codCerti = codCerti
        self.codArea = codArea
        self.codLibro = codLibro
        self.oficinaOrigen = oficinaOrigen
        self.refNumPart = refNumPart
        self.refNumPartMP = refNumPartMP
        self.partida = partida
        self.ficha = ficha
        self.tpoPersona = tpoPersona
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombre = nombre
        self.razSoc = razSoc
        self.tpoDoc = tpoDoc
        self.numDoc = numDoc
        self.email = email
        self.costoServicio = costoServicio
        self.ipRemote = ipRemote
        self.sessionId = sessionId
        self.usrId = usrId
        self.formaEnvio = "V"
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codCerti": codCerti, "codArea": codArea, "codLibro": codLibro, "oficinaOrigen": oficinaOrigen, "refNumPart": refNumPart,  "refNumPartMP": refNumPartMP,"partida": partida, "ficha": ficha, "tpoPersona": tpoPersona, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombre": nombre, "razSoc": razSoc, "tpoDoc": tpoDoc, "numDoc": numDoc, "email": email, "costoServicio": costoServicio, "ipRemote": ipRemote, "sessionId": sessionId, "usrId": usrId,"formaEnvio":"V"]
        print (bodyDic)
        return bodyDic
    }
    
}

