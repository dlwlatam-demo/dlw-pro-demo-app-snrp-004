//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class ConsultaPublicaService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
   
    func getListaJuridicos(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaJuridico()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoCertificado(tipoCertificado: String, _ handler: @escaping ArrayJsonResponse) {
            
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaTipoCerti().replacingOccurrences(of: "{tipoCertificado}", with: tipoCertificado)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaOficinaRegistral(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaOficinaRegistral()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoServicioSunarp(areaRegId: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaServicioSunarp(areaRegId: areaRegId)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
    
    func getListaValidaPartidaLiteral(regPubId: String,oficRegId: String,areaRegId: String,codGrupo: String,tipoPartidaFicha: String,numPart: String,coServ: String,coTipoRgst: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getConsulValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getDetalleAsientosLiteral(regPubId: String,oficRegId: String,codLibro: String,numPartida: String,fichaId: String,tomoId: String,fojaId: String,ofiSARP: String,coServicio: String,coTipoRegis: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosLiteral(regPubId: regPubId,oficRegId: oficRegId,codLibro: codLibro,numPartida: numPartida,fichaId: fichaId,tomoId: tomoId,fojaId: fojaId,ofiSARP: ofiSARP,coServicio: coServicio,coTipoRegis: coTipoRegis)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
        
    func getDetalleAsientosLiteralRMCTrue(codZona: String,codOficina: String,numPartida: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosRMCLiteral(codZona: codZona,codOficina: codOficina,numPartida: numPartida)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
    func getValidaPartidaCRI(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, tipoCRI: String, codGrupo: String, tipoPartidaFicha: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCRI(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart, tipoCRI: tipoCRI, codGrupo: codGrupo, tipoPartidaFicha: tipoPartidaFicha)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func getValidaPartidaCargaG(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, codGrupo: String, codigoLibro: String, tipoBusqueda: String, _ handler: @escaping JsonResponse) {
        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCargaG(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart, codGrupo: codGrupo, codigoLibro: codigoLibro, tipoBusqueda: tipoBusqueda)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
     
    
    /*
     
     func getValidaPartidaCargaG(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, codGrupo: String, codigoLibro: String, tipoBusqueda: String, _ handler: @escaping ArrayJsonResponse) {

         let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCargaG(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart, codGrupo: codGrupo, codigoLibro: codigoLibro, tipoBusqueda: tipoBusqueda)
     
             WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
         
                 let arrayResult = jsonResponse as? JSONArray ?? []
         handler(arrayResult)
             }
     }
     
    func getValidaPartidaCargaG(profileRequest: ConsulPublicaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCargaG()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
  */
        
        
    func getValidaPartidaCGA(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCGA(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    func getValidaPartidaCGEP(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCGEP(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    func getValidaPartidaCGV(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaCGV(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    
    
    func getObtenerNumPartida(tipo: String,numero: String,regPubId: String, oficRegId: String, areaRegId: String, _ handler: @escaping ArrayStringResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getObtenerNumPartida(tipo: tipo,numero: numero,regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
                dump(jsonResponse)
                let arrayResult = jsonResponse as? [String] ?? []
                handler(arrayResult)
            }
    }
    
    func validatePartidaApoyo(regPubId: String, oficRegId: String, areaRegId: String,libroArea: String, numPart: String,numPartMP: String , _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.validatePartidaApoyo(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, numPartMP: numPartMP)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func validaPartidaCurador(regPubId: String, oficRegId: String, areaRegId: String,libroArea: String, numPart: String,tipoPartidaFicha: String , _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.validaPartidaCurador(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, tipoPartidaFicha: tipoPartidaFicha)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    
    
    func getValidaPartidaxRefNumPart(regPubId: String, oficRegId: String, areaRegId: String,  numPart: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.ConsultaPublicidadJuridico.getValidaPartidaxRefNumPart(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart)
    
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func getDetalleAsientosLiteralRMCFalse(codZona: String,codOficina: String,numPartida: String,numPlaca: String, _ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosVehLiteral(codZona: codZona,codOficina: codOficina,numPartida: numPartida,numPlaca: numPlaca)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
       
    func getDetalleAsientosLiteralCalculo(coServicio: String,cantPaginas: String, _ handler: @escaping StringResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getCalcularPagoLiteral(coServicio: coServicio,cantPaginas: cantPaginas)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (intResponse) in
                
                print("intResponse::>>",intResponse.self!)
                let arrayResult = String(intResponse.self as? Int ?? 0)
            handler(arrayResult)
                
                print("arrayResult1::>>",arrayResult)
        }
    }
    

    
    func postDetalleAsientosLiteralSaveSolicitud(profileRequest: ConsultaLiteralRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    func postDetalleAsientosPublicaSaveSolicitud(profileRequest: ConsultaPublicaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    
    func postPaymentRequest(paymentInformation: PaymentProcessRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.ConsultaPropiedad.postPagarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: paymentInformation.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func postDetalleAsientosPublicaCargaSaveSolicitud(profileRequest: ConsultaPublicCargaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    func postDetalleAsientosPublicaCGASaveSolicitud(profileRequest: ConsultaPublicCarGraAeroRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    func postDetalleAsientosPublicaCGEPSaveSolicitud(profileRequest: ConsultaPublicCarGraEmbaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    func postDetalleAsientosPublicaVehiSaveSolicitud(profileRequest: ConsultaPublicVehiRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    
    func postCertiVigeDesigApoyoSaveSolicitud(profileRequest: ConsultaPublicCertiVigeDARequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    

    func postDetalleAsientosPublicaCertiVigPJSaveSolicitud(profileRequest: ConsultaPublicVigPJRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    
    func postDetalleAsientosPublicaInmobiliarioSaveSolicitud(profileRequest: ConsultaPublicInmobiliarioRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
    
}
