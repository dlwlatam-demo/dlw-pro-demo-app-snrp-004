//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class ConsultaPublicaModel {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
   
    func getListaJuridicos(handler: @escaping RegistroJuridicoModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaJuridicos()  { (arrayJsonResponse) in
            var arrayRegistroJuridico = [RegistroJuridicoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = RegistroJuridicoEntity(json: jsonResponse)
                arrayRegistroJuridico.append(objJson)
            }
            handler(arrayRegistroJuridico)
        }
    }
    func getListaTipoCertificado(tipoCertificado: String, handler: @escaping TipoCertificadoModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaTipoCertificado(tipoCertificado: "L") { (arrayJsonResponse) in
            
            var arrayTipoCertificado = [TipoCertificadoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoCertificadoEntity(json: jsonResponse)
                arrayTipoCertificado.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoCertificado.filter { $0.areaRegId == tipoCertificado}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaTipoCertificadoC(tipoCertificado: String, handler: @escaping TipoCertificadoModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaTipoCertificado(tipoCertificado: "C") { (arrayJsonResponse) in
            
            var arrayTipoCertificado = [TipoCertificadoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoCertificadoEntity(json: jsonResponse)
                arrayTipoCertificado.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoCertificado.filter { $0.areaRegId == tipoCertificado}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaOficinaRegistral(handler: @escaping OficinaRegistralModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaOficinaRegistral()  { (arrayJsonResponse) in
            var arrayOficinaRegistral = [OficinaRegistralEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = OficinaRegistralEntity(json: jsonResponse)
                arrayOficinaRegistral.append(objJson)
            }
            handler(arrayOficinaRegistral)
        }
    }
    func getListaTipoServicioSunarp(areaRegId: String, handler: @escaping ServicioSunarModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaTipoServicioSunarp(areaRegId: areaRegId) { (arrayJsonResponse) in
            
            var arrayServicioSunar = [ServicioSunarEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = ServicioSunarEntity(json: jsonResponse)
                arrayServicioSunar.append(objJson)
            }
            handler(arrayServicioSunar)
        }
    }
     
    
    func getListaValidaPartidaLiteral(regPubId: String,oficRegId: String,areaRegId: String,codGrupo: String,tipoPartidaFicha: String,numPart: String,coServ: String,coTipoRgst: String, handler: @escaping ValidaPartidaLiteralModel) {
            
            let service = ConsultaPublicaService(navigationController: navigationController)
    service.getListaValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [validaPartidaLiteralEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = validaPartidaLiteralEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
        
        
    func getDetalleAsientosLiteral(regPubId: String,oficRegId: String,codLibro: String,numPartida: String,fichaId: String,tomoId: String,fojaId: String,ofiSARP: String,coServicio: String,coTipoRegis: String, handler: @escaping getDetalleAsientoLiteralModel) {
            
            let service = ConsultaPublicaService(navigationController: navigationController)
    service.getDetalleAsientosLiteral(regPubId: regPubId,oficRegId: oficRegId,codLibro: codLibro,numPartida: numPartida,fichaId: fichaId,tomoId: tomoId,fojaId: fojaId,ofiSARP: ofiSARP,coServicio: coServicio,coTipoRegis: coTipoRegis) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [GetDetalleAsientoEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = GetDetalleAsientoEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
    
    
  
        func getDetalleAsientosLiteralRMCTrue(codZona: String,codOficina: String,numPartida: String, handler: @escaping getDetalleAsientoLiteralRMCModel) {
                
                let service = ConsultaPublicaService(navigationController: navigationController)
    service.getDetalleAsientosLiteralRMCTrue(codZona: codZona,codOficina: codOficina,numPartida: numPartida) { (arrayJsonResponse) in
                    
                    var arrayServicioSunar = [GetDetalleAsientoRMCEntity]()
                    
                    for jsonResponse in arrayJsonResponse {
                        let objJson = GetDetalleAsientoRMCEntity(json: jsonResponse)
                        arrayServicioSunar.append(objJson)
                    }
                    handler(arrayServicioSunar)
                }
            }
    
    
   
      func getDetalleAsientosLiteralRMCFalse(codZona: String,codOficina: String,numPartida: String,numPlaca: String, handler: @escaping getDetalleAsientoLiteralRMCModel) {
            
            let service = ConsultaPublicaService(navigationController: navigationController)
    service.getDetalleAsientosLiteralRMCFalse(codZona: codZona,codOficina: codOficina,numPartida: numPartida,numPlaca: numPlaca) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [GetDetalleAsientoRMCEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = GetDetalleAsientoRMCEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
    
        
       func getDetalleAsientosLiteralCalculo(coServicio: String,cantPaginas: String, handler: @escaping CalculoMontoResponse) {
             
             let service = ConsultaPublicaService(navigationController: navigationController)
    service.getDetalleAsientosLiteralCalculo(coServicio: coServicio,cantPaginas: cantPaginas) { (stringResponse) in
                 
                 handler(stringResponse)
                 
                 
                 print("arrayJsonResponse::>>",stringResponse)
             }
         }
    
    
    
        func getValidaPartidaCRI(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, tipoCRI: String, codGrupo: String, tipoPartidaFicha: String, handler: @escaping ValidaPartidaCRIModel) {
            
            let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCRI(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart, tipoCRI: tipoCRI, codGrupo: codGrupo, tipoPartidaFicha: tipoPartidaFicha)  { (jsonResponse) in
                let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
                //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
                handler(objProfile)
            }
                  
        }
    
        func getValidaPartidaCargaG(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, codGrupo: String, codigoLibro: String, tipoBusqueda: String, handler: @escaping ValidaPartidaCRIModel) {
               
            let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCargaG(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart, codGrupo: codGrupo, codigoLibro: codigoLibro, tipoBusqueda: tipoBusqueda)  { (jsonResponse) in
                let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
                //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
                handler(objProfile)
            }
    }
    
    
    func postDetalleAsientosPublicaCargaSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, tomo: String, folio: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, costoTotal: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicCargaRequest(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, tomo: tomo,folio: folio, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, costoTotal: costoTotal, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaCargaSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    func postDetalleAsientosPublicaCGASaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, matricula: String,expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicCarGraAeroRequest(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, matricula: matricula,expediente: expediente, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio,  ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaCGASaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    func postDetalleAsientosPublicaCGEPSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, matricula: String,nomEmbarcacion: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicCarGraEmbaRequest(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, matricula: matricula,nomEmbarcacion: nomEmbarcacion, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio,  ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaCGEPSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    func postDetalleAsientosPublicaVehiSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicVehiRequest(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio,  ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaVehiSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    func postCertiVigeDesigApoyoSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String, numPartidaMP: String, numAsientoMP: String,  costoServicio: String, costoTotal: String, usrId: String, tomo: String, folio: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicCertiVigeDARequest(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, asiento: asiento, numPartidaMP: numPartidaMP, numAsientoMP: numAsientoMP,  costoServicio: costoServicio, costoTotal: costoTotal, usrId: usrId, tomo: tomo, folio: folio)

        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postCertiVigeDesigApoyoSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    func postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tomo: String, folio: String, placa: String, matricula: String, nomEmbarcacion: String, expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String,tipPerVP: String,apePateVP: String,apeMateVP: String,nombVP: String,razSocVP: String,cargoApoderado: String,datoAdic: String,costoServicio: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicVigPJRequest(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, ficha: ficha, tomo: tomo, folio: folio, placa: placa, matricula: matricula, nomEmbarcacion: nomEmbarcacion, expediente: expediente, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, asiento: asiento,tipPerVP: tipPerVP,apePateVP: apePateVP,apeMateVP: apeMateVP,nombVP: nombVP,razSocVP: razSocVP,cargoApoderado: cargoApoderado,datoAdic: datoAdic,costoServicio: costoServicio, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaCertiVigPJSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    
    
    func postDetalleAsientosPublicaInmobiliarioSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicInmobiliarioRequest(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, ficha: ficha, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaInmobiliarioSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    func getValidaPartidaCGA(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, handler: @escaping ValidaPartidaCRIModel) {
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCGA(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)  { (jsonResponse) in
            let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    func getValidaPartidaCGEP(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, handler: @escaping ValidaPartidaCRIModel) {
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCGEP(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)  { (jsonResponse) in
            let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    func getValidaPartidaCGV(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String, handler: @escaping ValidaPartidaCRIModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCGV(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida)  { (jsonResponse) in
            let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
        
              /*
              let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaCGV(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, tipoNumero: tipoNumero, numPartida: numPartida) { (arrayJsonResponse) in
                  
                  var arrayServicioSunar = [ValidaPartidaCRIEntity]()
                  
                  for jsonResponse in arrayJsonResponse {
                      let objJson = ValidaPartidaCRIEntity(json: jsonResponse)
                      arrayServicioSunar.append(objJson)
                  }
                  handler(arrayServicioSunar)
              }
              */
    }
    
    
    func getValidaPartidaxRefNumPart(regPubId: String, oficRegId: String, areaRegId: String,  numPart: String, handler: @escaping ValidaPartidaCRIModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getValidaPartidaxRefNumPart(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart)  { (jsonResponse) in
            let objProfile = ValidaPartidaCRIEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    
    func getObtenerNumPartida(tipo: String,numero: String,regPubId: String, oficRegId: String, areaRegId: String, handler: @escaping PartidaTFModel) {
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.getObtenerNumPartida(tipo: tipo,numero: numero,regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId)  { (jsonResponse) in
            print("count = ", jsonResponse.count)
            if jsonResponse.count > 0 {
                let respuesta : PartidaTFEntity = PartidaTFEntity()
                respuesta.partida = jsonResponse.first?.description ?? ""
                handler(respuesta)
            }
            else {
                let respuesta : PartidaTFEntity = PartidaTFEntity()
                
                handler(respuesta)
            }
        }
    }
    
    
    func validaPartidaCurador(regPubId: String, oficRegId: String, areaRegId: String,libroArea: String, numPart: String, tipoPartidaFicha: String ,handler: @escaping (PartidaModel) -> ()) {
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.validaPartidaCurador(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, tipoPartidaFicha: tipoPartidaFicha) { objResponse in
            if let itemResult = self.convertDictionaryToJson(dictionary: objResponse) {
                let jsonData = itemResult.data(using: .utf8)!
                if let decodedData = try? JSONDecoder().decode(PartidaModel.self, from: jsonData){
                   handler(decodedData)
                }
            }
        }
    }
    
    
    func validatePartidaApoyos(regPubId: String, oficRegId: String, areaRegId: String, libroArea: String, numPart: String, numPartMP: String, handler: @escaping (PartidaModel?) -> ()) {
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.validatePartidaApoyo(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, numPartMP: numPartMP) { objResponse in
            if let itemResult = self.convertDictionaryToJson(dictionary: objResponse) {
                let jsonData = itemResult.data(using: .utf8)!
                
                do {
                    let decodedData = try JSONDecoder().decode(PartidaModel.self, from: jsonData)
                    handler(decodedData)
                } catch {
                    print("Error during decoding: \(error)")
                    handler(nil) // Manejar el caso de error, pasando nil al handler
                }
            } else {
                handler(nil) // Manejar el caso en el que itemResult sea nulo
            }
        }
    }

   
    func postDetalleAsientosPublicaSaveSolicitud(codCerti: String, codArea: String,  oficinaOrigen: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, tipPerPN: String , apePatPN: String , apeMatPN: String , nombPN: String , razSocPN: String , tipoDocPN: String , numDocPN: String , costoServicio: String , costoTotal: String , usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaPublicaRequest(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, tipPerPN: tipPerPN, apePatPN: apePatPN, apeMatPN: apeMatPN, nombPN: nombPN, razSocPN: razSocPN, tipoDocPN: tipoDocPN, numDocPN: numDocPN, costoServicio: costoServicio, costoTotal: costoTotal, usrId: usrId)
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        dump(deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosPublicaSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
    
    
    func postDetalleAsientosLiteralSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaLiteralRequest(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
        
       
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postDetalleAsientosLiteralSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
        
        
        
        
    }
    
    func postPaymentRequest(itemProcess: PaymentProcessRequest, handler: @escaping PaymentProcessResult) {
        
        
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        let service = ConsultaPublicaService(navigationController: navigationController)
    service.postPaymentRequest(paymentInformation: itemProcess)  { (jsonResponse) in
            print("postPaymentRequest: ",jsonResponse)
            let objProfile = PaymentProcessEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
    }
  
    func convertDictionaryToJson(dictionary: [String: Any]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        } catch {
            print("Error converting dictionary to JSON: \(error)")
        }
        return nil
    }
}
