//
//  ClosuresModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

typealias ResponseModel = (_ objSolicitud: ResponseEntity) -> Void
typealias TipoDocumentodModel = (_ arrayTipoDocumento: [TipoDocumentoEntity]) -> Void
typealias RegistroModel = (_ objRegistro: RegistroEntity) -> Void
typealias LoginResponseModel = (_ objLogin: LoginEntity) -> Void
typealias LoginAlertResponseModel = (_ objLogin: LoginAlertEntity) -> Void
typealias LogoutResponseModel = (_ objLogin: LogoutEntity) -> Void
typealias DniResponseModel = (_ objLogin: ValidacionDniEntity) -> Void
typealias CeResponseModel = (_ objLogin: ValidacionCeEntity) -> Void
typealias CommentResponseModel = (_ objComment: ComentarioEntity) -> Void
typealias MapaResponseModel = (_ objComment: MapaEntity) -> Void
typealias ProfileResponseModel = (_ objProfile: ProfileEntity) -> Void
typealias HistoryResponseModel = (_ arrayHistory: HistoryEntity) -> Void
typealias HistoryDetailResponseModel = (_ objHistory: HistoryDetailEntity) -> Void
typealias DocumentoResponseModel = (_ objDocumento: DocumentoEntity) -> Void
typealias BoletaResponseModel = (_ objBoleta: BoletaEntity) -> Void
typealias LiquidacionResponseModel = (_ objLiquidacion: LiquidacionEntity) -> Void
typealias NiubizResponse = (_ stringToken: String) -> Void
typealias NiubizPinHashResponse = (_ objNiubiz: NiubizPinHashEntity) -> Void
typealias VisaKeysResponse = (_ objVisaKeys: VisaKeysEntity) -> Void
typealias ConsultarPropiedadModel = (_ objConsulta: ConsultaPropiedadEntity) -> Void
typealias RegistroJuridicoModel = (_ arrayTipoDocumento: [RegistroJuridicoEntity]) -> Void
typealias TipoCertificadoModel = (_ arrayTipoDocumento: [TipoCertificadoEntity]) -> Void
typealias OficinaRegistralModel = (_ arrayTipoDocumento: [OficinaRegistralEntity]) -> Void
typealias ServicioSunarModel = (_ arrayTipoDocumento: [ServicioSunarEntity]) -> Void
typealias ZonaResponseModel = (_ arrayZona: ZonaResponse) -> Void
typealias AreaResponseModel = (_ arrayArea: AreaResponse) -> Void
typealias GrupoResponseModel = (_ arrayGrupo: GrupoResponse) -> Void
typealias TitularidadModel = (_ arrayTitularidad: [TitularidadEntity]) -> Void
typealias TitularidadAsyncModel = (_ asyncTitularidad: TitularidadAsyncEntity) -> Void
typealias BusquedadModel = (_ arrayBusqueda: [PersonaJuridicaEntity]) -> Void
typealias ConsultarVehicularPlacaModel = (_ objConsultaPlaca: InfoVehicularEntity) -> Void
typealias ListaTiveModel = (_ arrayTive: [TiveEntity]) -> Void
typealias ListaTiveHiperlinkModel = (_ arrayTive: TiveBusqEntity) -> Void
typealias ValidaPartidaLiteralModel = (_ arrayValidaPartidaLiteral: [validaPartidaLiteralEntity]) -> Void
typealias getDetalleAsientoLiteralModel = (_ arrayDetalleAsientoLiteral: [GetDetalleAsientoEntity]) -> Void
typealias getDetalleAsientoLiteralRMCModel = (_ arrayDetalleAsientoLiteral: [GetDetalleAsientoRMCEntity]) -> Void
typealias CalculoMontoResponse = (_ stringMonto: String) -> Void
typealias SaveLiteralResponseModel = (_ objProfile: GuardarSolicitudEntity) -> Void
typealias ObtenerLibroModel = (_ arrayTipoDocumento: [ObtenerLibroEntity]) -> Void
typealias CostoModel = (_ objCosto: CostoEntity) -> Void
typealias ValidaPartidaCRIModel = (_ arrayTipoDocumento: ValidaPartidaCRIEntity) -> Void
typealias ResponseAlertModel = (_ objSolicitud: LoadUserEntity) -> Void
typealias AllPartidaFromUserModel = (_ objSolicitud: [AllPartidaFromUserEntity]) -> Void
typealias SearchPArtidaModel = (_ objSolicitud: [SearchPartidaEntity]) -> Void
typealias alertNotificationModel = (_ objSolicitud: [AlertNotificationEntity]) -> Void
typealias AllMandatoFromUserModel = (_ objSolicitud: [AllMandatoFromUserEntity]) -> Void
typealias NewsTransaccionModel = (_ objSolicitud: [NewsTransaccionEntity]) -> Void
typealias PaymentProcessResult = (_ arrayTipoDocumento: PaymentProcessEntity) -> Void
typealias PartidaTFModel = (_ arrayPartidaTF: PartidaTFEntity) -> Void
