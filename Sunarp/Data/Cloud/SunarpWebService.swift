//
//  SunarpService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation

struct SunarpWebService {
    
    static let userURL = ConfigurationEnvironment.environment.info.userURL
    static let parameterURL = ConfigurationEnvironment.environment.info.parameterURL
    static let securityURL = ConfigurationEnvironment.environment.info.securityURL
    static let profileURL = ConfigurationEnvironment.environment.info.profileURL
    static let solicitudURL = ConfigurationEnvironment.environment.info.solicitudURL
    static let niubizURL = ConfigurationEnvironment.environment.info.niubizURL
    static let niubizPinURL = ConfigurationEnvironment.environment.info.niubizPinURL
    static let consultaPropiedadURL = ConfigurationEnvironment.environment.info.consultaPropiedadURL
    static let consultaSunarpURL = ConfigurationEnvironment.environment.info.consultaSunarpURL
    static let consultaPropiedadPJ = ConfigurationEnvironment.environment.info.consultaPropiedadPJ
    static let consultaVehicularURL = ConfigurationEnvironment.environment.info.consultaVehicularURL
    static let consultaTivetURL = ConfigurationEnvironment.environment.info.consultaTiveURL
    static let userAlertRegURL = ConfigurationEnvironment.environment.info.userAlertRegURL
    static let userAlertPartidaRegURL = ConfigurationEnvironment.environment.info.userAlertPartidaRegURL
    static let userAlertMandatoRegURL = ConfigurationEnvironment.environment.info.userAlertMandatoRegURL
    
    static let contextoURL = "sunarp-desarrollo/app-sunarpp/appsunarp_servicios"
    static let contextoSeguURL = "sunarp-desarrollo/app-sunarpp/appsunarp_servicios"
    // Sin API Gateway:
    //static let contextoURL = "appsunarp/api/v1"
    //tatic let contextoSeguURL = ""

    struct Register {
        
        static func postSolicitudCodigo() -> String {
            return userURL + contextoURL + "/registro/usuario/solicitud"
        }
        
        static func postValidacionCodigo() -> String {
            return userURL + contextoURL + "/registro/usuario/solicitud/valida"
        }
        
        static func postRegistrarUsuario() -> String {
            return userURL + contextoURL + "/registro/usuario"
        }
        
        static func getListaTipoDocumentos(guid: String) -> String {
            return parameterURL + contextoURL + "/parametros/registro/tipoDocumentos/\(guid)"
        }
        
        static func getListaTipoDocumentosInt(guid: String) -> String {
            return parameterURL + contextoURL + "/parametros/tipoDocumentos?tipoPer=\(guid)"
        }
        static func getListaObtenerLibro(areaRegId: String) -> String {
            return parameterURL + contextoURL + "/parametros/obtenerLibros/\(areaRegId)"
        }
        static func postValidarDni() -> String {
            return userURL + contextoURL + "/registro/usuario/valida/dni"
        }
        
        static func postValidarCe() -> String {
            return userURL + contextoURL + "/registro/usuario/valida/ce"
        }
    }
    struct Alert {
        static func getLoadUser() -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/loadUser"
        }
        static func putUpdateUser() -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/updateUser"
        }
        static func putCancelUser() -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/cancelUser"
        }
        static func getSelectAllPartidaFromUser() -> String {
            return userAlertPartidaRegURL + contextoURL + "/alerta-partida/selectAllPartidasFromUser"
        }
        static func postSearchPartida() -> String {
            return userAlertPartidaRegURL + contextoURL + "/alerta-partida/searchPartida"
        }
        static func postAlertAddPartida() -> String {
            return userAlertPartidaRegURL + contextoURL + "/alerta-partida/addPartida"
        }
        static func putDeletePartida() -> String {
            return userAlertPartidaRegURL + contextoURL + "/alerta-partida/deletePartida"
        }
        static func putDeleteMandato() -> String {
            return userAlertMandatoRegURL + contextoURL + "/alerta-mandato/deleteMandato"
        }
        static func getListaNotifications(aaCont: String, nuCont: String, tipo: String, source: String) -> String {
            return userAlertPartidaRegURL + contextoURL + "/alerta-partida/getAlertasPorContrato?aaCont=\(aaCont)&nuCont=\(nuCont)&tipo=\(tipo)&source=\(source)"
        }
        static func getSelectAllMandatoFromUser() -> String {
            return userAlertMandatoRegURL + contextoURL + "/alerta-mandato/selectAllMandatosFromUser"
        }
        static func postAddMandatoPartida() -> String {
            return userAlertMandatoRegURL + contextoURL + "/alerta-mandato/addMandato"
        }
        
    }
    struct Recovery {
        
        static func postSolicitudCodigo() -> String {
            return userURL + contextoURL + "/seguridad/recupera"
        }
        
        static func postValidacionCodigo() -> String {
            return userURL + contextoURL + "/seguridad/recupera/valida"
        }
        
        static func putActualizaContrasena() -> String {
            return userURL + contextoURL + "/seguridad/cambio/clave"
        }
        static func getAlertRegSolicitudCodigo(email: String) -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/loadUserByEmail/\(email)"
        }
        static func getLoadUserByEmailChnPwd(email: String) -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/loadUserByEmailChnPwd/\(email)"
        }
        static func postAlertRegCreateUser() -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/createUser"
        }
        
        static func getAlertSendValidationCode(guid: String) -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/sendValidationCodeByMail/\(guid)"
        }
        static func getAlertRegInsertValidacionCodigo(guid: String,coNoti: String) -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/activateUserById/\(guid)?coNoti=\(coNoti)"
        }
        static func getAlertRegActualizarContrasena(password: String,guid: String) -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/setPassword/\(guid)?passw=\(password)"
        }
    }
    
    struct Authorization {
        static func postLogin() -> String {
            return securityURL + contextoSeguURL + "/oauth/token"
        }
        static func deleteLogout() -> String {
            return securityURL + contextoSeguURL + "/invalidarToken/{jti}"
        }
        static func postAlertLogin() -> String {
            return userAlertRegURL + contextoURL + "/alerta/registro/loadUserByEmailAndPass"
        }
        static func deleteAlertLogout() -> String {
            return userAlertRegURL + contextoSeguURL + "invalidarToken/{jti}"
        }
        
        static func validateVersionAppURL() -> String {
            return securityURL + contextoSeguURL + "/getLastVersion/techiapp"
        }
    }
    
    struct Comment {
        static func postComment() -> String {
            return parameterURL + contextoURL + "/servicios/contacto/comentario"
        }
    }
    struct Mapa {
        static func getMapa() -> String {
            return parameterURL + contextoURL + "/parametros/oficinas/mapa"
        }
    }
    
    struct Profile {
        
        static func postLogin() -> String {
            return profileURL + contextoURL + "/seguridad/login"
        }
        
        static func putRegistro() -> String {
            return profileURL + contextoURL + "/seguridad/registro/usuario"
        }
        
        static func putRegistroFoto() -> String {
            return profileURL + contextoURL + "/seguridad/registro/usuario/foto"
        }
        
        static func getDocumentos() -> String {
            return parameterURL + contextoURL + "/parametros/tipoDocumentos"
        }
    }
    
    struct Solicitud {
        
        static func getHistorial() -> String {
            return solicitudURL + contextoURL + "/solicitud/transaccion"
        }
        
        static func getHistorialDetalle() -> String {
            return solicitudURL + contextoURL + "/solicitud/{id}"
        }
        static func getHistorialDetalleAnio(anio: String,id: String) -> String {
            return solicitudURL + contextoURL + "/solicitud/publicidad/\(anio)/\(id)"
        }
        static func postAclaramiento() -> String {
            return solicitudURL + contextoURL + "/solicitud/aclaramiento"
        }
        
        static func postSubsanacion() -> String {
            return solicitudURL + contextoURL + "/solicitud/subsanacion"
        }
        
        static func postDesestimiento() -> String {
            return solicitudURL + contextoURL + "/solicitud/desistimiento"
        }
        
        static func getDocumento() -> String {
            return solicitudURL + contextoURL + "/solicitud/documento/{id}"
        }
        
        static func postLiquidacion() -> String {
            return solicitudURL + contextoURL + "/solicitud/pagar/liquidacion"
        }
        
        static func getBoleta() -> String {
            return solicitudURL + contextoURL + "/solicitud/boleta/informativa/descarga/{transId}"
        }
        static func getNotification() -> String {
            return solicitudURL + contextoURL + "/solicitud/news/transaccion"
        }
        
    }
    
    struct Niubiz {
        
        static func postVisaKeys() -> String {
            return parameterURL + contextoURL + "/parametros/visaKeys"
        }
        
        static func postNiubiz() -> String {
            return niubizURL + "api.security/v1/security"
        }
        
        static func postNiubizPinHash() -> String {
            return niubizPinURL + "api.certificate/v1/query/{merchant}"
        }
        
    }  
    struct ConsultaPropiedad {
        
        static func postConsulListaPropiedad() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/consultaPropiedad"
        }
        
        static func postConsulValidarDni() -> String {
            return consultaSunarpURL + contextoURL + "/consultas/valida/dni"
        }
        
        static func postConsulValidarCe() -> String {
            return consultaSunarpURL + contextoURL + "/consultas/valida/ce"
        }
        static func getConsulValidaPartidaLiteral(regPubId: String,oficRegId: String,areaRegId: String,codGrupo: String,tipoPartidaFicha: String,numPart: String,coServ: String,coTipoRgst: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/validaPartidaLiteral/\(regPubId)/\(oficRegId)/\(areaRegId)/\(codGrupo)/\(tipoPartidaFicha)/\(numPart)/\(coServ)/\(coTipoRgst)"
        }
        
        
        static func getDetalleAsientosLiteral(regPubId: String,oficRegId: String,codLibro: String,numPartida: String,fichaId: String,tomoId: String,fojaId: String,ofiSARP: String,coServicio: String,coTipoRegis: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/getDetalleAsientos/\(regPubId)/\(oficRegId)/\(codLibro)/\(numPartida)/\(fichaId)/\(tomoId)/\(fojaId)/\(ofiSARP)/\(coServicio)/\(coTipoRegis)"
        }
        
        
        static func getDetalleAsientosRMCLiteral(codZona: String,codOficina: String,numPartida: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/getDetalleAsientosRMC/\(codZona)/\(codOficina)/\(numPartida)"
        }
        static func getDetalleAsientosVehLiteral(codZona: String,codOficina: String,numPartida: String,numPlaca: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/getDetalleAsientosVeh/\(codZona)/\(codOficina)/\(numPartida)/\(numPlaca)"
        }
        
        static func getCalcularPagoLiteral(coServicio: String,cantPaginas: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/calcularPago/\(coServicio)/\(cantPaginas)"
        }
        static func postGuardarSolicitud() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/guardarSolicitud"
        }
        
        static func postPagarSolicitud() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/pagarSolicitud"
        }
        
        static func getTransactionId() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/solicitud/nextCount"
        }
    }
    
    struct ConsultaVehicular {
        
        static func postConsulVehicular() -> String {
            return consultaVehicularURL + contextoURL + "/consulta-vehicular/vehiculo/data"
        }
        
        static func postValidarBoleta() -> String {
            return consultaVehicularURL + contextoURL + "/consulta-vehicular/boleta/validar"
        }
        
        static func postGenerarBoleta() -> String {
            return consultaVehicularURL + contextoURL + "/consulta-vehicular/boleta/generar"
        }
        
        static func getDescargaBoleta() -> String {
            return solicitudURL + contextoURL + "/solicitud/boleta/informativa/descarga/{transId}"
        }
        
        static func postCosto() -> String {
            return parameterURL + contextoURL + "/parametros/tarifa"
        }
    }
    struct ConsultaPublicidadJuridico {
   
        static func validaPartidaCurador(regPubId: String, oficRegId: String, areaRegId: String, libroArea: String, numPart: String, tipoPartidaFicha: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCurador/\(regPubId)/\(oficRegId)/\(areaRegId)/\(libroArea)/\(numPart)/\(tipoPartidaFicha)"
        }
        
        static func validatePartidaApoyo(regPubId: String, oficRegId: String, areaRegId: String, libroArea: String, numPart: String, numPartMP: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaApoyo/\(regPubId)/\(oficRegId)/\(areaRegId)/\(libroArea)/\(numPart)/\(numPartMP)"
        }
        
        static func getConsultaListaJuridico() -> String {
            return parameterURL + contextoURL + "/parametros/registros/juridicos"
        }
        
        static func getConsultaListaTipoCertiC(areaRegId: String) -> String {
            return parameterURL + contextoURL + "/parametros/tipoCertificado/C?/areaRegId=\(areaRegId)" //contextoURL + "/parametros/tipoCertificado/\(tipoCertificado)"
        }


        static func getConsultaOficinaRegistral() -> String {
            return parameterURL + contextoURL + "/parametros/oficinaRegistral"
        }
        
        static func getValidaPartidaCRI(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, tipoCRI: String, codGrupo: String, tipoPartidaFicha: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCRI/\(regPubId)/\(oficRegId)/\(areaRegId)/\(numPart)/\(tipoCRI)/\(codGrupo)/\(tipoPartidaFicha)"
        }
        
        static func getValidaPartidaCGA(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCGA/\(regPubId)/\(oficRegId)/\(areaRegId)/\(tipoNumero)/\(numPartida)"
        }
        static func getValidaPartidaCGEP(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCGEP/\(regPubId)/\(oficRegId)/\(areaRegId)/\(tipoNumero)/\(numPartida)"
        }
        static func getValidaPartidaCGV(regPubId: String, oficRegId: String, areaRegId: String, tipoNumero: String, numPartida: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCGV/\(regPubId)/\(oficRegId)/\(areaRegId)/\(tipoNumero)/\(numPartida)"
        }
        
        
        static func getObtenerNumPartida(tipo: String,numero: String,regPubId: String, oficRegId: String, areaRegId: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/obtenerNumPartida/\(tipo)/\(numero)/\(regPubId)/\(oficRegId)/\(areaRegId)"
        }
        static func getValidaPartidaxRefNumPart(regPubId: String, oficRegId: String, areaRegId: String, numPart: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaxRefNumPart/\(regPubId)/\(oficRegId)/\(areaRegId)/\(numPart)"
        }
        static func getValidaPartidaCargaG(regPubId: String, oficRegId: String, areaRegId: String, numPart: String, codGrupo: String, codigoLibro: String, tipoBusqueda: String) -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCargaGravamen/\(regPubId)/\(oficRegId)/\(areaRegId)/\(numPart)/\(codGrupo)/\(codigoLibro)/\(tipoBusqueda)"
        }
        
        /*
        static func getValidaPartidaCargaG() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/publicidad/validaPartidaCargaGravamen"
        }*/
        
    }
    
    struct ConsultaLiteralJuridico {
        
        static func getConsultaListaJuridico() -> String {
            return parameterURL + contextoURL + "/parametros/registros/juridicos"
        }
        
        static func getConsultaListaTipoCerti() -> String {
            return parameterURL + contextoURL + "/parametros/tipoCertificado/{tipoCertificado}" //contextoURL + "/parametros/tipoCertificado/\(tipoCertificado)"
        }
        
        
        static func getConsultaListaTipoCertiL(areaRegId: String) -> String {
            return parameterURL + contextoURL + "/parametros/tipoCertificado/{tipoCertificado}?areaRegId=\(areaRegId)"
        }
        
        static func getConsultaOficinaRegistral() -> String {
            return parameterURL + contextoURL + "/parametros/oficinaRegistral"
        }
        
        static func getConsultaServicioSunarp(areaRegId: String) -> String {
            return parameterURL + contextoURL + "/parametros/servicioScunac/\(areaRegId)"
        }
        
    }
    
    struct Busqueda {
        
        static func getZonas() -> String {
            return parameterURL + contextoURL + "/parametros/zonas"
        }
        
        static func getAreasRegistrales() -> String {
            return parameterURL + contextoURL + "/parametros/areasRegistrales"
        }
        
        static func getGrupoLibro() -> String {
            return parameterURL + contextoURL + "/parametros/tipoPersonas/{codGrupoLibroArea}"
        }
        
        static func postConsultaTitularidad() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/consultaTitularidad"
        }

        static func postConsultaTitularidadAsync() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/consultaTitularidadAsync"
        }

        static func getStatusConsultaTitularidadAsync() -> String {
            return consultaPropiedadURL + contextoURL + "/consultas/async/status/consultaTitularidad/{guid}"
        }

        static func getBusquedaJuridica() -> String {
            return consultaPropiedadPJ + contextoURL + "/consultas/personaJuridica/busqueda?nombre={nombre}&oficina={nombreOficinaRegistral}&siglas={siglas}"
        }
        
    }
    
    struct Tivet {
        
        static func postBusqueda() -> String {
            return consultaTivetURL + contextoURL + "/consulta-tive/busqueda"
        }
        static func postHiperlinkInsert() -> String {
            return consultaTivetURL + contextoURL + "/consulta-tive/historial/insertar"
        }
        static func getBusquedaHistory() -> String {
            return consultaTivetURL + contextoURL + "/consulta-tive/historial/busqueda"
        }
        
    }
}
