//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class ConsultaLiteralService {
    
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func getListaJuridicos(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaJuridico()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoCertificado(tipoCertificado: String, _ handler: @escaping ArrayJsonResponse) {
            
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaTipoCerti().replacingOccurrences(of: "{tipoCertificado}", with: tipoCertificado)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }

    
    func getListaTipoCertificadoL(tipoCertificado: String, areaRegId: String, _ handler: @escaping ArrayJsonResponse) {
            
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaTipoCertiL(areaRegId: areaRegId).replacingOccurrences(of: "{tipoCertificado}", with: tipoCertificado)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    

    func getListaOficinaRegistral(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaOficinaRegistral()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoServicioSunarp(areaRegId: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaServicioSunarp(areaRegId: areaRegId)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
    
    func getListaValidaPartidaLiteral(regPubId: String,oficRegId: String,areaRegId: String,codGrupo: String,tipoPartidaFicha: String,numPart: String,coServ: String,coTipoRgst: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getConsulValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getDetalleAsientosLiteral(regPubId: String,oficRegId: String,codLibro: String,numPartida: String,fichaId: String,tomoId: String,fojaId: String,ofiSARP: String,coServicio: String,coTipoRegis: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosLiteral(regPubId: regPubId,oficRegId: oficRegId,codLibro: codLibro,numPartida: numPartida,fichaId: fichaId,tomoId: tomoId,fojaId: fojaId,ofiSARP: ofiSARP,coServicio: coServicio,coTipoRegis: coTipoRegis)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
        
    func getDetalleAsientosLiteralRMCTrue(codZona: String,codOficina: String,numPartida: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosRMCLiteral(codZona: codZona,codOficina: codOficina,numPartida: numPartida)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
 
    
    func getDetalleAsientosLiteralRMCFalse(codZona: String,codOficina: String,numPartida: String,numPlaca: String, _ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.ConsultaPropiedad.getDetalleAsientosVehLiteral(codZona: codZona,codOficina: codOficina,numPartida: numPartida,numPlaca: numPlaca)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
       
    func getDetalleAsientosLiteralCalculo(coServicio: String,cantPaginas: String, _ handler: @escaping StringResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.getCalcularPagoLiteral(coServicio: coServicio,cantPaginas: cantPaginas)
        
            WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (intResponse) in
            
                let arrayResult = String(intResponse.self as? Int ?? 0)
            handler(arrayResult)
                
                print("arrayResult2::>>",arrayResult)
        }
    }
    

    
    func postDetalleAsientosLiteralSaveSolicitud(profileRequest: ConsultaLiteralRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaPropiedad.postGuardarSolicitud()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
        
    }
 
}
