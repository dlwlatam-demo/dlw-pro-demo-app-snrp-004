//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsultaLiteralRequest: NSObject {
    //  "usrId": "APPSNRPANDRO"}
    var codCerti: String = ""
    var codArea: String = ""
    var codLibro: String = ""
    var oficinaOrigen: String = ""
    var refNumPart: String = ""
    var partida: String = ""
    var placa: String = ""
    var tpoPersona: String = ""
    var apePaterno: String = ""
    var apeMaterno: String = ""
    var nombre: String = ""
    var razSoc: String = ""
    var tpoDoc: String = ""
    var numDoc: String = ""
    var email: String = ""
    var costoServicio: String = ""
    var cantPaginas: String = ""
    var cantPaginasExon: String = ""
    var paginasSolicitadas: String = ""
    var nuAsieSelectSARP: String = ""
    var imPagiSIR: String = ""
    var nuSecuSIR: String = ""
    var ipRemote: String = ""
    var sessionId: String = ""
    var usrId: String = ""
    var formaEnvio: String = "V"

    init(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, ipRemote: String, sessionId: String, usrId: String) {
        self.codCerti = codCerti
        self.codArea = codArea
        self.codLibro = codLibro
        self.oficinaOrigen = oficinaOrigen
        self.refNumPart = refNumPart
        self.partida = partida
        self.placa = placa
        self.tpoPersona = tpoPersona
        self.apePaterno = apePaterno
        self.apeMaterno = apeMaterno
        self.nombre = nombre
        self.razSoc = razSoc
        self.tpoDoc = tpoDoc
        self.numDoc = numDoc
        self.email = email
        self.costoServicio = costoServicio
        self.cantPaginas = cantPaginas
        self.cantPaginasExon = cantPaginasExon
        self.paginasSolicitadas = paginasSolicitadas
        self.nuAsieSelectSARP = nuAsieSelectSARP
        self.imPagiSIR = imPagiSIR
        self.ipRemote = ipRemote
        self.sessionId = sessionId
        self.usrId = usrId
        self.formaEnvio = "V"
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codCerti": codCerti, "codArea": codArea, "codLibro": codLibro, "oficinaOrigen": oficinaOrigen, "refNumPart": refNumPart, "partida": partida, "placa": placa, "tpoPersona": tpoPersona, "apePaterno": apePaterno, "apeMaterno": apeMaterno, "nombre": nombre, "razSoc": razSoc, "tpoDoc": tpoDoc, "numDoc": numDoc, "email": email, "costoServicio": costoServicio, "cantPaginas": cantPaginas, "cantPaginasExon": cantPaginasExon, "paginasSolicitadas": paginasSolicitadas, "nuAsieSelectSARP": nuAsieSelectSARP, "imPagiSIR": imPagiSIR, "formaEnvio": formaEnvio,"ipRemote": ipRemote, "sessionId": sessionId, "usrId": usrId]
        print (bodyDic)
        return bodyDic
    }
    
}

