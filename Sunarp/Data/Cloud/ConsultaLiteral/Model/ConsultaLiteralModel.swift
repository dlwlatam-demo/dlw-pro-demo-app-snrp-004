//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class ConsultaLiteralModel {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func getListaJuridicos(handler: @escaping RegistroJuridicoModel) {
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaJuridicos()  { (arrayJsonResponse) in
            var arrayRegistroJuridico = [RegistroJuridicoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = RegistroJuridicoEntity(json: jsonResponse)
                arrayRegistroJuridico.append(objJson)
            }
            handler(arrayRegistroJuridico)
        }
    }
    func getListaTipoCertificado(tipoCertificado: String, handler: @escaping TipoCertificadoModel) {
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaTipoCertificadoL(tipoCertificado: "L", areaRegId: tipoCertificado) { (arrayJsonResponse) in
            
            var arrayTipoCertificado = [TipoCertificadoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoCertificadoEntity(json: jsonResponse)
                arrayTipoCertificado.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoCertificado.filter { $0.areaRegId == tipoCertificado}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaTipoCertificadoC(tipoCertificado: String, handler: @escaping TipoCertificadoModel) {
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaTipoCertificado(tipoCertificado: "C") { (arrayJsonResponse) in
            
            var arrayTipoCertificado = [TipoCertificadoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoCertificadoEntity(json: jsonResponse)
                arrayTipoCertificado.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoCertificado.filter { $0.areaRegId == tipoCertificado}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaOficinaRegistral(handler: @escaping OficinaRegistralModel) {
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaOficinaRegistral()  { (arrayJsonResponse) in
            var arrayOficinaRegistral = [OficinaRegistralEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = OficinaRegistralEntity(json: jsonResponse)
                arrayOficinaRegistral.append(objJson)
            }
            handler(arrayOficinaRegistral)
        }
    }
    func getListaTipoServicioSunarp(areaRegId: String, handler: @escaping ServicioSunarModel) {
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaTipoServicioSunarp(areaRegId: areaRegId) { (arrayJsonResponse) in
            
            var arrayServicioSunar = [ServicioSunarEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = ServicioSunarEntity(json: jsonResponse)
                arrayServicioSunar.append(objJson)
            }
            handler(arrayServicioSunar)
        }
    }
     
    
    func getListaValidaPartidaLiteral(regPubId: String,oficRegId: String,areaRegId: String,codGrupo: String,tipoPartidaFicha: String,numPart: String,coServ: String,coTipoRgst: String, handler: @escaping ValidaPartidaLiteralModel) {
            
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getListaValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [validaPartidaLiteralEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = validaPartidaLiteralEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
        
        
    func getDetalleAsientosLiteral(regPubId: String,oficRegId: String,codLibro: String,numPartida: String,fichaId: String,tomoId: String,fojaId: String,ofiSARP: String,coServicio: String,coTipoRegis: String, handler: @escaping getDetalleAsientoLiteralModel) {
            
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.getDetalleAsientosLiteral(regPubId: regPubId,oficRegId: oficRegId,codLibro: codLibro,numPartida: numPartida,fichaId: fichaId,tomoId: tomoId,fojaId: fojaId,ofiSARP: ofiSARP,coServicio: coServicio,coTipoRegis: coTipoRegis) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [GetDetalleAsientoEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = GetDetalleAsientoEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
    
    
  
        func getDetalleAsientosLiteralRMCTrue(codZona: String,codOficina: String,numPartida: String, handler: @escaping getDetalleAsientoLiteralRMCModel) {
                
            let service = ConsultaLiteralService(navigationController: navigationController)
            service.getDetalleAsientosLiteralRMCTrue(codZona: codZona,codOficina: codOficina,numPartida: numPartida) { (arrayJsonResponse) in
                    
                    var arrayServicioSunar = [GetDetalleAsientoRMCEntity]()
                    
                    for jsonResponse in arrayJsonResponse {
                        let objJson = GetDetalleAsientoRMCEntity(json: jsonResponse)
                        arrayServicioSunar.append(objJson)
                    }
                    handler(arrayServicioSunar)
                }
            }
    
    
   
      func getDetalleAsientosLiteralRMCFalse(codZona: String,codOficina: String,numPartida: String,numPlaca: String, handler: @escaping getDetalleAsientoLiteralRMCModel) {
            
          let service = ConsultaLiteralService(navigationController: navigationController)
          service.getDetalleAsientosLiteralRMCFalse(codZona: codZona,codOficina: codOficina,numPartida: numPartida,numPlaca: numPlaca) { (arrayJsonResponse) in
                
                var arrayServicioSunar = [GetDetalleAsientoRMCEntity]()
                
                for jsonResponse in arrayJsonResponse {
                    let objJson = GetDetalleAsientoRMCEntity(json: jsonResponse)
                    arrayServicioSunar.append(objJson)
                }
                handler(arrayServicioSunar)
            }
        }
    
        
       func getDetalleAsientosLiteralCalculo(coServicio: String,cantPaginas: String, handler: @escaping CalculoMontoResponse) {
             
           let service = ConsultaLiteralService(navigationController: navigationController)
           service.getDetalleAsientosLiteralCalculo(coServicio: coServicio,cantPaginas: cantPaginas) { (stringResponse) in
                 
                 handler(stringResponse)
                 
                 
                 print("arrayJsonResponse1::>>",stringResponse)
             }
         }
    
    func postDetalleAsientosLiteralSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, ipRemote: String, sessionId: String, usrId: String, handler: @escaping SaveLiteralResponseModel) {
        
        let deviceLoginRequest = ConsultaLiteralRequest(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId)
        
       
       // let profileRequest = ConsultaLiteralRequest(deviceLoginRequest: deviceLoginRequest)
        
        let service = ConsultaLiteralService(navigationController: navigationController)
        service.postDetalleAsientosLiteralSaveSolicitud(profileRequest: deviceLoginRequest)  { (jsonResponse) in
            let objProfile = GuardarSolicitudEntity(json: jsonResponse)
            //UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
        
        
        
        
    }
    
  
}
