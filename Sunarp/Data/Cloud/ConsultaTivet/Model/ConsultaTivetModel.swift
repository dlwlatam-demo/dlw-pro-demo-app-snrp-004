//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class ConsultaTivetModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func getListaTive(handler: @escaping ListaTiveModel) {
        let service = ConsultaTivetService(navigationController:navigationController)
        service.getListaTive()  { (arrayJsonResponse) in
            var arrayTive = [TiveEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = TiveEntity(json: jsonResponse)
                arrayTive.append(objJson)
            }
            handler(arrayTive)
        }
    }
 
  
    func postListConsultaTive(codigoZona: String, codigoOficina: String, anioTitulo: String, numeroTitulo: String, numeroPlaca: String, codigoVerificacion: String, tipo: String, handler: @escaping ListaTiveHiperlinkModel) {
        
        
        let listaConsultaTive = ConsultaTivetRequest(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo)
                
        let service = ConsultaTivetService(navigationController:navigationController)
        service.postListConsultaTive(busquedaTive: listaConsultaTive)  { (jsonResponse) in
            print("*** response", jsonResponse)
            let objConsulta = TiveBusqEntity(json: jsonResponse)
            handler(objConsulta)
        }
        
    }
    func postListConsultaHiperLinkTive(codigoZona: String, codigoOficina: String, anioTitulo: String, numeroTitulo: String, numeroPlaca: String, codigoVerificacion: String, tipo: String, handler: @escaping ListaTiveHiperlinkModel) {
        
        
        let listaConsultaTive = ConsultaTivetRequest(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo)
                
        let service = ConsultaTivetService(navigationController:navigationController)
        service.postListConsultaHiperLinkTive(busquedaTive: listaConsultaTive)  { (jsonResponse) in
            let objConsulta = TiveBusqEntity(json: jsonResponse)
            handler(objConsulta)
        }
        
    }
}
