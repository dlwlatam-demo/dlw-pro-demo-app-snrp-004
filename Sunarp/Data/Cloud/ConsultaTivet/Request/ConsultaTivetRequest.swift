//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsultaTivetRequest: NSObject {

    var codigoZona: String = ""
    var codigoOficina: String = ""
    var anioTitulo: String = ""
    var numeroTitulo: String = ""
    var numeroPlaca: String = ""
    var codigoVerificacion: String = ""
    var tipo: String = ""

    init(codigoZona: String, codigoOficina: String, anioTitulo: String, numeroTitulo: String, numeroPlaca: String, codigoVerificacion: String, tipo: String) {
        self.codigoZona = codigoZona
        self.codigoOficina = codigoOficina
        self.anioTitulo = anioTitulo
        self.numeroTitulo = numeroTitulo
        self.numeroPlaca = numeroPlaca
        self.codigoVerificacion = codigoVerificacion
        self.tipo = tipo
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codigoZona": codigoZona, "codigoOficina": codigoOficina, "anioTitulo": anioTitulo, "numeroTitulo": numeroTitulo, "numeroPlaca": numeroPlaca, "codigoVerificacion": codigoVerificacion, "tipo": tipo]
        print (bodyDic)
        return bodyDic
    }
    
}



