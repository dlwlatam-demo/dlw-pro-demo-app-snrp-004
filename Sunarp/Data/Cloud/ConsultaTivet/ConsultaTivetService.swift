//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class ConsultaTivetService {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

   
    func getListaTive(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Tivet.getBusquedaHistory()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    
    
    
    func postListConsultaTive(busquedaTive: ConsultaTivetRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Tivet.postBusqueda()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: busquedaTive.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postListConsultaHiperLinkTive(busquedaTive: ConsultaTivetRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Tivet.postHiperlinkInsert()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: busquedaTive.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
   
    
}
