//
//  RecoveryModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class RecoveryModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    
    func postSolicitudCodigo(_ correoDestino: String, handler: @escaping ResponseModel) {
        
        let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.postSolicitudCodigo(solicitud: solicitud)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func postValidacionCodigo(codigoValidacion: String, guid: String, handler: @escaping ResponseModel) {
        
        let validacion = ValidacionRecoveryRequest(codigoValidacion: codigoValidacion, guid: guid)
        
        let service = RecoveryService(navigationController:navigationController)
    service.postValidarCodigo(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func putActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
        let actualizarContrasena = ActualizarContrasenaRequest(password: password, guid: guid)
        
        let service = RecoveryService(navigationController:navigationController)
    service.putActualizarContrasena(contrasena: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    //postAlertRegSolicitudCodigo
    func getAlertRegSolicitudCodigo(_ email: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.getAlertRegSolicitudCodigo(email: email)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    //loadUserByEmailChnPwd
    func getLoadUserByEmailChnPwd(_ email: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.getLoadUserByEmailChnPwd(email: email)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
        
    func postAlertRegValidacionCodigo(email: String,appVersion: String, guid: String, handler: @escaping ResponseModel) {
        
        let validacion = RegisterAlertRecoveryRequest(email: email,appVersion: appVersion,  guid: guid)
        
        let service = RecoveryService(navigationController:navigationController)
    service.postAlertRegValidarCodigo(validacion: validacion)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertSendValidationCode(_ guid: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.getAlertSendValidationCode(guid: guid)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertActivateUserById(_ guid: String,coNoti: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.getAlertActivateUserById(guid: guid,coNoti:coNoti)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func getAlertRegActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
       // let solicitud = SolicitudRecoveryRequest(correoDestino: correoDestino)
        
        let service = RecoveryService(navigationController:navigationController)
    service.getAlertRegActualizarContrasena(password: password, guid: guid)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func putAlertRegActualizarContrasena(password: String, guid: String, handler: @escaping ResponseModel) {
        
        let actualizarContrasena = ActualizarContrasenaRequest(password: password, guid: guid)
        
        let service = RecoveryService(navigationController:navigationController)
    service.putAlertRegActualizarContrasena(contrasena: actualizarContrasena)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
}
