//
//  ValidacionRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class ValidacionRecoveryRequest: NSObject {

    var codigoValidacion: String = ""
    var guid: String = ""

    init(codigoValidacion: String, guid: String) {
        self.codigoValidacion = codigoValidacion
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codigoValidacion": codigoValidacion, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}
