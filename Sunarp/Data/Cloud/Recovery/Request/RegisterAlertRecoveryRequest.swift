//
//  ValidacionRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class RegisterAlertRecoveryRequest: NSObject {

    var email: String = ""
    var appVersion: String = ""
    var guid: String = ""

    init(email: String, appVersion: String, guid: String) {
        self.email = email
        self.appVersion = appVersion
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["email": email,"appVersion": appVersion, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}
