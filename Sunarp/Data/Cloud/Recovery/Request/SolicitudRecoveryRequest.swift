//
//  SolicitudRecoveryRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import UIKit

class SolicitudRecoveryRequest: NSObject {

    var correoDestino: String = ""

    init(correoDestino: String) {
        self.correoDestino = correoDestino
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["correoDestino": correoDestino]
        print (bodyDic)
        return bodyDic
    }
    
}
