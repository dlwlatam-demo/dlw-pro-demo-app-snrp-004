//
//  ActualizarContrasenaRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class ActualizarContrasenaRequest: NSObject {

    var password: String = ""
    var guid: String = ""

    init(password: String, guid: String) {
        self.password = password
        self.guid = guid
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["password": password, "guid": guid]
        print (bodyDic)
        return bodyDic
    }
    
}
