//
//  RecoveryService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class RecoveryService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func postSolicitudCodigo(solicitud: SolicitudRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postSolicitudCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: solicitud.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postValidarCodigo(validacion: ValidacionRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postValidacionCodigo()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func putActualizarContrasena(contrasena: ActualizarContrasenaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.putActualizaContrasena()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: contrasena.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
   
    /*
    func getListaTive(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Tivet.getBusquedaHistory()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }*/
    
  //JsonResponse
    func getAlertRegSolicitudCodigo(email: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertRegSolicitudCodigo(email: email)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    func getLoadUserByEmailChnPwd(email: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getLoadUserByEmailChnPwd(email: email)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    func postAlertRegValidarCodigo(validacion: RegisterAlertRecoveryRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.postAlertRegCreateUser()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validacion.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }

    func getAlertSendValidationCode(guid: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertSendValidationCode(guid: guid)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    //coNoti
    func getAlertActivateUserById(guid: String,coNoti: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertRegInsertValidacionCodigo(guid: guid,coNoti: coNoti)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    
    func getAlertRegActualizarContrasena(password: String,guid: String, _ handler: @escaping JsonResponse) {

        let urlString = SunarpWebService.Recovery.getAlertRegActualizarContrasena(password: password,guid: guid)
        print("urlString::>>",urlString)
            WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
        
                //let arrayResult = jsonResponse as? JSONArray ?? []
                //handler(arrayResult)
                let json = jsonResponse as? JSON ?? [:]
                handler(json)
            }
    }
    
    func putAlertRegActualizarContrasena(contrasena: ActualizarContrasenaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Recovery.putActualizaContrasena()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: contrasena.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
}
