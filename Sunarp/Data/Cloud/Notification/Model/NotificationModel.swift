//
//  RecoveryModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class NotificationModel {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    //selectAllPartidaFromUser
    func getNewsTransaccion(handler: @escaping NewsTransaccionModel) {
        
        let service = NotificationService(navigationController: navigationController)
        service.getNewsTransaccion() { (arrayJsonResponse) in
            
            var arrayTipoDocumentos = [NewsTransaccionEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = NewsTransaccionEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
          
            handler(arrayTipoDocumentos)
        }
        
    }
   
}
