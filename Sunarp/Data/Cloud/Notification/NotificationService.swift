//
//  RecoveryService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class NotificationService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func getNewsTransaccion(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Solicitud.getNotification()
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
}
