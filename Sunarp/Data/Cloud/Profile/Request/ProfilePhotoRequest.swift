//
//  ProfilePhotoRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class ProfilePhotoRequest: NSObject {

    var userPhoto: String

    init(userPhoto: String) {
        self.userPhoto = userPhoto
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["userPhoto": userPhoto]
        print (bodyDic)
        return bodyDic
    }
    
}
