//
//  ProfileUpdateRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class ProfileUpdateRequest: NSObject {

    var sexo: String = ""
    var tipoDoc: String = ""
    var nroDoc: String = ""
    var nombres: String = ""
    var priApe: String = ""
    var segApe: String = ""
    var fecNac: String = ""
    var nroCelular: String = ""
    var rememberToken: String = ""
    var appVersion: String = ""
    var ipAddress: String = ""
    var lastConn: String = ""
    var geoLat: Double = 0.0
    var geoLong: Double = 0.0

    init(sexo: String, tipoDoc: String, nroDoc: String, nombres: String, priApe: String, segApe: String, fecNac: String, nroCelular: String, rememberToken: String, appVersion: String, ipAddress: String, lastConn: String, geoLat: Double, geoLong: Double) {
        self.sexo = sexo
        self.tipoDoc = tipoDoc
        self.nroDoc = nroDoc
        self.nombres = nombres
        self.priApe = priApe
        self.segApe = segApe
        self.fecNac = fecNac
        self.nroCelular = nroCelular
        self.rememberToken = rememberToken
        self.appVersion = appVersion
        self.ipAddress = ipAddress
        self.lastConn = lastConn
        self.geoLat = geoLat
        self.geoLong = geoLong
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["sexo": sexo, "tipoDoc": tipoDoc, "nroDoc": nroDoc, "nombres": nombres, "priApe": priApe, "segApe": segApe, "fecNac": fecNac, "nroCelular": nroCelular, "rememberToken": rememberToken, "appVersion": appVersion, "ipAddress": ipAddress, "lastConn": lastConn, "geoLat": geoLat, "geoLong": geoLong]
        print (bodyDic)
        return bodyDic
    }
    
}
