//
//  ProfileRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class ProfileRequest: NSObject {

    var deviceLoginRequest: ProfileLoginRequest

    init(deviceLoginRequest: ProfileLoginRequest) {
        self.deviceLoginRequest = deviceLoginRequest
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["deviceLoginRequest": deviceLoginRequest.bodyDictionary()]
        print (bodyDic)
        return bodyDic
    }
    
}
