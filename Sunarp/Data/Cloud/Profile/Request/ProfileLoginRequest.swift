//
//  ProfileLoginRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation

class ProfileLoginRequest: NSObject {

    var appId: String = ""
    var deviceToken: String = ""
    var deviceType: String = ""
    var status: String = ""
    var deviceId: String = ""
    var appVersion: String = ""
    var ipAddress: String = ""
    var deviceLogged: String = ""
    var receiveNotifications: String = ""
    var deviceOs: String = ""

    init(appId: String, deviceToken: String, deviceType: String, status: String, deviceId: String, appVersion: String, ipAddress: String, deviceLogged: String, receiveNotifications: String, deviceOs: String) {
        self.appId = appId
        self.deviceToken = deviceToken
        self.deviceType = deviceType
        self.status = status
        self.deviceId = deviceId
        self.appVersion = appVersion
        self.ipAddress = ipAddress
        self.deviceLogged = deviceLogged
        self.receiveNotifications = receiveNotifications
        self.deviceOs = deviceOs
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["appId": appId, "deviceToken": deviceToken, "deviceType": deviceType, "status": status, "deviceId": deviceId, "appVersion": appVersion, "ipAddress": ipAddress, "deviceLogged": deviceLogged, "receiveNotifications": receiveNotifications, "deviceOs": deviceOs]
        print (bodyDic)
        return bodyDic
    }
    
}
