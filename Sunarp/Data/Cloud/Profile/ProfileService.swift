//
//  ProfileService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/08/22.
//

import Foundation
import UIKit

class ProfileService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func postLogin(profileRequest: ProfileRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Profile.postLogin()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: profileRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func putRegistro(profileUpdateRequest: ProfileUpdateRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Profile.putRegistro()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: profileUpdateRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func putRegistroFoto(profilePhotoRequest: ProfilePhotoRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Profile.putRegistroFoto()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.put, urlString: urlString, params: profilePhotoRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getListaTipoDocumentos(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.Profile.getDocumentos()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
            
}
