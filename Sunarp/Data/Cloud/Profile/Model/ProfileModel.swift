//
//  ProfileModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 17/08/22.
//

import Foundation
import UIKit

class ProfileModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    

    
    func postLogin(appId: String, deviceToken: String, deviceType: String, status: String, deviceId: String, appVersion: String, ipAddress: String, deviceLogged: String, receiveNotifications: String, deviceOs: String, handler: @escaping ProfileResponseModel) {
        
        let deviceLoginRequest = ProfileLoginRequest(appId: appVersion, deviceToken: deviceToken, deviceType: deviceType, status: status, deviceId: deviceId, appVersion: appVersion, ipAddress: ipAddress, deviceLogged: deviceLogged, receiveNotifications: receiveNotifications, deviceOs: deviceOs)
        let profileRequest = ProfileRequest(deviceLoginRequest: deviceLoginRequest)
        
        let service = ProfileService(navigationController:navigationController)
        service.postLogin(profileRequest: profileRequest)  { (jsonResponse) in
            let objProfile = ProfileEntity(json: jsonResponse)
            UserPreferencesController.setUsuario(objProfile.usuarioLogin)
            handler(objProfile)
        }
        
    }
    
    func putRegistro(sexo: String, tipoDoc: String, nroDoc: String, nombres: String, priApe: String, segApe: String, fecNac: String, nroCelular: String, rememberToken: String, appVersion: String, ipAddress: String, lastConn: String, geoLat: Double, geoLong: Double, handler: @escaping ResponseModel) {
        
        let profileUpdateRequest = ProfileUpdateRequest(sexo: sexo, tipoDoc: tipoDoc, nroDoc: nroDoc, nombres: nombres, priApe: priApe, segApe: segApe, fecNac: fecNac, nroCelular: nroCelular, rememberToken: rememberToken, appVersion: appVersion, ipAddress: ipAddress, lastConn: lastConn, geoLat: geoLat, geoLong: geoLong)
        
        let service = ProfileService(navigationController:navigationController)
        service.putRegistro(profileUpdateRequest: profileUpdateRequest)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func putRegistroFoto(userPhoto: String, handler: @escaping ResponseModel) {
        
        let profilePhotoRequest = ProfilePhotoRequest(userPhoto: userPhoto)
        
        let service = ProfileService(navigationController:navigationController)
        service.putRegistroFoto(profilePhotoRequest: profilePhotoRequest)  { (jsonResponse) in
            let objSolicitud = ResponseEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func getListaTipoDocumentos(handler: @escaping TipoDocumentodModel) {
        
        let service = ProfileService(navigationController:navigationController)
        service.getListaTipoDocumentos { (arrayJsonResponse) in
            var arrayTipoDocumentos = [TipoDocumentoEntity]()
            print(arrayJsonResponse)
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoDocumentoEntity(json: jsonResponse)
                arrayTipoDocumentos.append(objJson)
            }
            handler(arrayTipoDocumentos)
        }
    }
}
