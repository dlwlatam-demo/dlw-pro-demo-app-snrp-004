//
//  RegisterModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class ConsultaVehicularModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    
   
    func getListaJuridicos(handler: @escaping RegistroJuridicoModel) {
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.getListaJuridicos()  { (arrayJsonResponse) in
            var arrayRegistroJuridico = [RegistroJuridicoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = RegistroJuridicoEntity(json: jsonResponse)
                arrayRegistroJuridico.append(objJson)
            }
            handler(arrayRegistroJuridico)
        }
    }
    func getListaTipoCertificado(tipoCertificado: String, handler: @escaping TipoCertificadoModel) {
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.getListaTipoCertificado(tipoCertificado: "L") { (arrayJsonResponse) in
            
            var arrayTipoCertificado = [TipoCertificadoEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TipoCertificadoEntity(json: jsonResponse)
                arrayTipoCertificado.append(objJson)
            }
            let arrayTipoDocumentosFiltered = arrayTipoCertificado.filter { $0.areaRegId == tipoCertificado}
            handler(arrayTipoDocumentosFiltered)
        }
    }
    
    func getListaOficinaRegistral(handler: @escaping OficinaRegistralModel) {
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.getListaOficinaRegistral()  { (arrayJsonResponse) in
            var arrayOficinaRegistral = [OficinaRegistralEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = OficinaRegistralEntity(json: jsonResponse)
                arrayOficinaRegistral.append(objJson)
            }
            handler(arrayOficinaRegistral)
        }
    }
    
    func getListaTipoServicioSunarp(areaRegId: String, handler: @escaping ServicioSunarModel) {
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.getListaTipoServicioSunarp(areaRegId: areaRegId) { (arrayJsonResponse) in
            
            var arrayServicioSunar = [ServicioSunarEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = ServicioSunarEntity(json: jsonResponse)
                arrayServicioSunar.append(objJson)
            }
            handler(arrayServicioSunar)
        }
    }
        
    func postListarConsultaVehicular(nuPlac: String, codigoRegi: String, codigoSede: String, ipAddress: String, idUser: String, appVersion: String, handler: @escaping ConsultarVehicularPlacaModel) {
        
        
        let listaConsultaVehicular = ConsultaVehiculoPlacaRequest(nuPlac: nuPlac, codigoRegi: codigoRegi, codigoSede: codigoSede, ipAddress: ipAddress, idUser: idUser, appVersion: appVersion)
                
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.postListConsultaVehicular(listaConsultaVehicular: listaConsultaVehicular)  { (jsonResponse) in
            let objConsulta = InfoVehicularEntity(json: jsonResponse)
            handler(objConsulta)
        }
        
    }
    
    func postValidarBoleta(placa: String, codZona: String, codOficina: String, ipAddress: String, idUser: String, appVersion: String, handler: @escaping ResponseModel) {
        
        let validarBoletaRequest = ValidarBoletaRequest(placa: placa, codZona: codZona, codOficina: codOficina, ipAddress: ipAddress, idUser: idUser, appVersion: appVersion)
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.postValidarBoleta(validarBoletaRequest: validarBoletaRequest) { (jsonResponse) in
            
            let objJson = ResponseEntity(json: jsonResponse)
            
            handler(objJson)
        }
    }
    
    func postGenerarBoleta(usuario: String, costo: String, ip: String, codZona: String, codOficina: String, placa: String, codAccion: String, codAutoriza: String, codtienda: String, concepto: String, decisionCs: String, dscCodAccion: String, dscEci: String, eci: String, estado: String, eticket: String, fechaYhoraTx: String, idUnico: String, idUser: String, impAutorizado: String, nomEmisor: String, numOrden: String, numReferencia: String, oriTarjeta: String, pan: String, resCvv2: String, respuesta: String, reviewTransaction: String, transId: String, handler: @escaping ResponseModel) {
        
        let visanetResponse = VisaNetRequest(codAccion: codAccion, codAutoriza: codAutoriza, codtienda: codtienda, concepto: concepto, decisionCs: decisionCs, dscCodAccion: dscCodAccion, dscEci: dscEci, eci: eci, estado: estado, eticket: eticket, fechaYhoraTx: fechaYhoraTx, idUnico: idUnico, idUser: idUser, impAutorizado: impAutorizado, nomEmisor: nomEmisor, numOrden: numOrden, numReferencia: numReferencia, oriTarjeta: oriTarjeta, pan: pan, resCvv2: resCvv2, respuesta: respuesta, reviewTransaction: reviewTransaction, transId: transId)
        
        let generarBoletaRequest = GenerarBoletaRequest(usuario: usuario, costo: costo, ip: ip, codZona: codZona, codOficina: codOficina, placa: placa, visanetResponse: visanetResponse)
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.postGenerarBoleta(generarBoletaRequest: generarBoletaRequest) { (jsonResponse) in
            print("jsonResponse: ", jsonResponse)
            dump(jsonResponse)
            let objJson = ResponseEntity(json: jsonResponse)
            
            dump(objJson)
            handler(objJson)
        }
    }
    
    func getDescargaBoleta(transId: String, handler: @escaping BoletaResponseModel) {
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.getDescargaBoleta(transId: transId) { (jsonResponse) in
            
            let boletaResponse = BoletaEntity(json: jsonResponse)
            
            handler(boletaResponse)
        }
    }
    
    func postCosto(flg: String, zona: String, oficina: String, codLibroArea: Int, partida: String, handler: @escaping CostoModel) {
                
        let costoRequest = CostoRequest(flg: flg, zona: zona, oficina: oficina, codLibroArea: codLibroArea, partida: partida)
        
        let service = ConsultaVehicularService(navigationController:navigationController)
    service.postCostoServicioSunarp(costoRequest: costoRequest) { (jsonResponse) in
            
            let objJson = CostoEntity(json: jsonResponse)
            
            handler(objJson)
        }
    }
    
}
