//
//  RegisterService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

class ConsultaVehicularService {
    
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
  
    func getListaJuridicos(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaJuridico()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaTipoCertificado(tipoCertificado: String, _ handler: @escaping ArrayJsonResponse) {
    
        //let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaTipoCerti(tipoCertificado: tipoCertificado)
        
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaListaTipoCerti().replacingOccurrences(of: "{tipoCertificado}", with: tipoCertificado)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func getListaOficinaRegistral(_ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaOficinaRegistral()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func postListConsultaVehicular(listaConsultaVehicular: ConsultaVehiculoPlacaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaVehicular.postConsulVehicular()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: listaConsultaVehicular.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
        
    func getListaTipoServicioSunarp(areaRegId: String, _ handler: @escaping ArrayJsonResponse) {
    
        let urlString = SunarpWebService.ConsultaLiteralJuridico.getConsultaServicioSunarp(areaRegId: areaRegId)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
    func postValidarBoleta(validarBoletaRequest: ValidarBoletaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaVehicular.postValidarBoleta()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: validarBoletaRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
        
    func postGenerarBoleta(generarBoletaRequest: GenerarBoletaRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaVehicular.postGenerarBoleta()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: generarBoletaRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
        
    func getDescargaBoleta(transId: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.ConsultaVehicular.getDescargaBoleta().replacingOccurrences(of: "{transId}", with: transId)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func postCostoServicioSunarp(costoRequest: CostoRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.ConsultaVehicular.postCosto()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.post, urlString: urlString, params: costoRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
}
