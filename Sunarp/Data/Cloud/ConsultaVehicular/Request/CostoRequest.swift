//
//  CostoRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/12/22.
//

import UIKit

class CostoRequest: NSObject {

  
    var flg: String = ""
    var zona: String = ""
    var oficina: String = ""
    var codLibroArea: Int = 0
    var partida: String = ""
    

    init(flg: String, zona: String, oficina: String, codLibroArea: Int, partida: String) {
        self.flg = flg
        self.zona = zona
        self.oficina = oficina
        self.codLibroArea = codLibroArea
        self.partida = partida
      
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["flg": flg, "zona ": zona, "oficina ": oficina, "codLibroArea": codLibroArea, "partida": partida]
        print (bodyDic)
        return bodyDic
    }
    
}

