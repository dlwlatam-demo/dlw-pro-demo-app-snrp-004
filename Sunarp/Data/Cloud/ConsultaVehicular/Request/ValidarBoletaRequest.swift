//
//  ValidarBoletaRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/10/22.
//

import Foundation

class ValidarBoletaRequest: NSObject {
  
    var placa: String = ""
    var codZona: String = ""
    var codOficina: String = ""
    var ipAddress: String = ""
    var idUser: String = ""
    var appVersion: String = ""

    init(placa: String, codZona: String, codOficina: String, ipAddress: String, idUser: String, appVersion: String) {
        self.placa = placa
        self.codZona = codZona
        self.codOficina = codOficina
        self.ipAddress = ipAddress
        self.idUser = idUser
        self.appVersion = appVersion
      
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["placa": placa, "codZona": codZona, "codOficina": codOficina, "ipAddress": ipAddress, "idUser": idUser, "appVersion": appVersion]
        print (bodyDic)
        return bodyDic
    }
    
}
