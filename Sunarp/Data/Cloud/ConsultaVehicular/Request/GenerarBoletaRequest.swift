//
//  GenerarBoletaRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/10/22.
//

import Foundation

class GenerarBoletaRequest: NSObject {
      
    var usuario: String = ""
    var costo: String = ""
    var ip: String = ""
    var codZona: String = ""
    var codOficina: String = ""
    var placa: String = ""
    var visanetResponse: VisaNetRequest = VisaNetRequest.init()
    

    init(usuario: String, costo: String, ip: String, codZona: String, codOficina: String, placa: String, visanetResponse: VisaNetRequest) {
        self.usuario = usuario
        self.costo = costo
        self.ip = ip
        self.codZona = codZona
        self.codOficina = codOficina
        self.placa = placa
        self.visanetResponse = visanetResponse
      
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["usuario": usuario, "costo": costo, "ip": ip, "codZona": codZona, "codOficina": codOficina, "placa": placa, "visanetResponse": visanetResponse.bodyDictionary()]
        print (bodyDic)
        return bodyDic
    }
    
}
