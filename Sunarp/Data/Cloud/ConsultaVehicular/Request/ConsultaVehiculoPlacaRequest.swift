//
//  ValidacionDniRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import UIKit

class ConsultaVehiculoPlacaRequest: NSObject {

  
    var nuPlac: String = ""
    var codigoRegi: String = ""
    var codigoSede: String = ""
    var ipAddress: String = ""
    var idUser: String = ""
    var appVersion: String = ""
    

    init(nuPlac: String, codigoRegi: String, codigoSede: String, ipAddress: String, idUser: String, appVersion: String) {
        self.nuPlac = nuPlac
        self.codigoRegi = codigoRegi
        self.codigoSede = codigoSede
        self.ipAddress = ipAddress
        self.idUser = idUser
        self.appVersion = appVersion
      
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["nuPlac": nuPlac, "codigoRegi": codigoRegi, "codigoSede": codigoSede, "ipAddress": ipAddress, "idUser": idUser, "appVersion": appVersion]
        print (bodyDic)
        return bodyDic
    }
    
}

