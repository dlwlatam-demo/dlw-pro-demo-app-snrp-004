//
//  VisaNetRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/10/22.
//

import Foundation

class VisaNetRequest: NSObject {
  
    var codAccion: String = ""
    var codAutoriza: String = ""
    var codtienda: String = ""
    var concepto: String = ""
    var decisionCs: String = ""
    var dscCodAccion: String = ""
    var dscEci: String = ""
    var eci: String = ""
    var estado: String = ""
    var eticket: String = ""
    var fechaYhoraTx: String = ""
    var idUnico: String = ""
    var idUser: String = ""
    var impAutorizado: String = ""
    var nomEmisor: String = ""
    var numOrden: String = ""
    var numReferencia: String = ""
    var oriTarjeta: String = ""
    var pan: String = ""
    var resCvv2: String = ""
    var respuesta: String = ""
    var reviewTransaction: String = ""
    var transId: String = ""
    

    init(codAccion: String, codAutoriza: String, codtienda: String, concepto: String, decisionCs: String, dscCodAccion: String, dscEci: String, eci: String, estado: String, eticket: String, fechaYhoraTx: String, idUnico: String, idUser: String, impAutorizado: String, nomEmisor: String, numOrden: String, numReferencia: String, oriTarjeta: String, pan: String, resCvv2: String, respuesta: String, reviewTransaction: String, transId: String) {
        self.codAccion = codAccion
        self.codAutoriza = codAutoriza
        self.codtienda = codtienda
        self.concepto = concepto
        self.decisionCs = decisionCs
        self.dscCodAccion = dscCodAccion
        self.dscEci = dscEci
        self.eci = eci
        self.estado = estado
        self.eticket = eticket
        self.fechaYhoraTx = fechaYhoraTx
        self.idUnico = idUnico
        self.idUser = idUser
        self.impAutorizado = impAutorizado
        self.nomEmisor = nomEmisor
        self.numOrden = numOrden
        self.numReferencia = numReferencia
        self.oriTarjeta = oriTarjeta
        self.pan = pan
        self.resCvv2 = resCvv2
        self.respuesta = respuesta
        self.reviewTransaction = reviewTransaction
        self.transId = transId
      
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codAccion": codAccion, "codAutoriza": codAutoriza, "codtienda": codtienda, "concepto": concepto, "decisionCs": decisionCs, "dscCodAccion": dscCodAccion, "dscEci": dscEci, "eci": eci, "estado": estado, "eticket": eticket, "fechaYhoraTx": fechaYhoraTx, "idUnico": idUnico, "idUser": idUser, "impAutorizado": impAutorizado, "nomEmisor": nomEmisor, "numOrden": numOrden, "numReferencia": numReferencia, "oriTarjeta": oriTarjeta, "pan": pan, "resCvv2": resCvv2, "respuesta": respuesta, "reviewTransaction": reviewTransaction, "transId": transId]
        print (bodyDic)
        return bodyDic
    }
    
    override convenience init() {
        self.init(codAccion: "", codAutoriza: "", codtienda: "", concepto: "", decisionCs: "", dscCodAccion: "", dscEci: "", eci: "", estado: "", eticket: "", fechaYhoraTx: "", idUnico: "", idUser: "", impAutorizado: "", nomEmisor: "", numOrden: "", numReferencia: "", oriTarjeta: "", pan: "", resCvv2: "", respuesta: "", reviewTransaction: "", transId: "")
    }
    
}
