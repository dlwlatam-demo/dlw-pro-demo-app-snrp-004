//
//  PaymentProcessRequest.swift
//  Sunarp
//
//  Created by Segundo Acosta on 18/01/23.
//

import Foundation

class PaymentProcessRequest: NSObject{
    
    var solicitudId: Int = 0
    var costoTotal: String = ""
    var ip: String = ""
    var usrId: String = ""
    var codigoGla: String = ""
    var idUser: String = ""
    var idUnico: String = ""
    var pan: String = ""
    var dscCodAccion: String = ""
    var codAutoriza: String = ""
    var codtienda: String = ""
    var numOrden: String = ""
    var codAccion: String = ""
    var fechaYhoraTx: String = ""
    var nomEmisor: String = ""
    var oriTarjeta: String = ""
    var respuesta: String = ""
    var usrKeyId: String = ""
    var transId: String = ""
    var eticket: String = ""
    var concepto: String = ""
    var codCerti: String = ""
    var numeroRecibo: String = ""
    var numeroPublicidad: String = ""
    var codVerificacion: String = ""
    var codLibroOpc: String = ""
    var cantPaginas: String = ""
    var cantPaginasExon: String = ""
    var paginasSolicitadas: String = ""
    var totalPaginasPartidaFicha: String = ""
    var nuAsieSelectSARP: String = ""
    var imPagiSIR: String = ""
    var nuSecuSIR: String = ""
    var codLibro: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    var visanetResponse: VisaNetRequest = VisaNetRequest.init()
    var lstOtorgantes: String = ""
    var lstApoyos: String = ""
    var lstCuradores: String = ""
    var lstPoderdantes: String = ""
    var lstApoderados: String = ""
    var niubizData: NiubizRequest = NiubizRequest.init()
    var idUsuario: String = ""
    var email: String = ""
    var formaEnvio: String = "V"
    
    override init() {}
    
    init(solicitudId: Int, costoTotal: String, ip: String, usrId: String, codigoGla: String, idUser: String, idUnico: String, pan: String, dscCodAccion: String, codAutoriza: String, codtienda: String, numOrden: String, codAccion: String, fechaYhoraTx: String, nomEmisor: String, oriTarjeta: String, respuesta: String, usrKeyId: String, transId: String, eticket: String, concepto: String, codCerti: String, numeroRecibo: String, numeroPublicidad: String, codVerificacion: String, codLibroOpc: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, totalPaginasPartidaFicha: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, codLibro: String, coServ: String, coTipoRgst: String, visanetResponse: VisaNetRequest, lstOtorgantes: String, lstApoyos: String, lstCuradores: String, lstPoderdantes: String, lstApoderados: String, niubizData : NiubizRequest){
        
        self.solicitudId = solicitudId
        self.costoTotal = costoTotal
        self.ip = ip
        self.usrId = usrId
        self.codigoGla = codigoGla
        self.idUser = idUser
        self.idUnico = idUnico
        self.pan = pan
        self.dscCodAccion = dscCodAccion
        self.codAutoriza = codAutoriza
        self.codtienda = codtienda
        self.numOrden = numOrden
        self.codAccion = codAccion
        self.fechaYhoraTx = fechaYhoraTx
        self.nomEmisor = nomEmisor
        self.oriTarjeta = oriTarjeta
        self.respuesta = respuesta
        self.usrKeyId = usrKeyId
        self.transId = transId
        self.eticket = eticket
        self.concepto = concepto
        self.codCerti = codCerti
        self.numeroRecibo = numeroRecibo
        self.numeroPublicidad = numeroPublicidad
        self.codVerificacion = codVerificacion
        self.codLibroOpc = codLibroOpc
        self.cantPaginas = cantPaginas
        self.cantPaginasExon = cantPaginasExon
        self.paginasSolicitadas = paginasSolicitadas
        self.totalPaginasPartidaFicha = totalPaginasPartidaFicha
        self.nuAsieSelectSARP = nuAsieSelectSARP
        self.imPagiSIR = imPagiSIR
        self.nuSecuSIR = nuSecuSIR
        self.codLibro = codLibro
        self.coServ = coServ
        self.coTipoRgst = coTipoRgst
        self.visanetResponse = visanetResponse
        self.lstOtorgantes = lstOtorgantes
        self.lstApoyos = lstApoyos
        self.lstCuradores = lstCuradores
        self.lstPoderdantes = lstPoderdantes
        self.lstApoderados = lstApoderados
        self.niubizData = niubizData
        self.formaEnvio = "V"
    
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["solicitudId":solicitudId,"costoTotal":costoTotal,"ip":ip,"usrId":usrId,"codigoGla":codigoGla,"idUser":idUser,"idUnico":idUnico,"pan":pan,"dscCodAccion":dscCodAccion,"codAutoriza":codAutoriza,"codtienda":codtienda,"numOrden":numOrden,"codAccion":codAccion,"fechaYhoraTx":fechaYhoraTx,"nomEmisor":nomEmisor,"oriTarjeta":oriTarjeta,"respuesta":respuesta,"usrKeyId":usrKeyId,"transId":transId,"eticket":eticket,"concepto":concepto,"codCerti":codCerti,"numeroRecibo":numeroRecibo,"numeroPublicidad":numeroPublicidad,"codVerificacion":codVerificacion,"codLibroOpc":codLibroOpc,"cantPaginas":cantPaginas,"cantPaginasExon":cantPaginasExon,"paginasSolicitadas":paginasSolicitadas,"totalPaginasPartidaFicha":totalPaginasPartidaFicha,"nuAsieSelectSARP":nuAsieSelectSARP,"imPagiSIR":imPagiSIR,"nuSecuSIR":nuSecuSIR,"codLibro":codLibro,"coServ":coServ,"coTipoRgst":coTipoRgst,"formaEnvio":formaEnvio, "visanetResponse":visanetResponse.bodyDictionary(),"lstOtorgantes":lstOtorgantes,"lstApoyos":lstApoyos,"lstCuradores":lstCuradores,"lstPoderdantes":lstPoderdantes,"lstApoderados":lstApoderados]
        print("========================================================================================")
        print ("PaymentProcessRequest", bodyDic)
        print("========================================================================================")
        return bodyDic
    }
    
}
