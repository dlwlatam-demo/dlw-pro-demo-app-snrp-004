//
//  WebServiceManager.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 6/08/22.
//

import Foundation
import UIKit

typealias JSON = [String: Any]
typealias JSONArray = [JSON]

extension WebServiceManager   {
    
    typealias ServiceHandler = (_ jsonResponse: Any?) -> Void
    typealias ServiceHandlerWithError = (_ jsonResponse: Any?,_ error: Int?) -> Void
    
    enum HTTPMethod: String {
        case get    = "GET"
        case post   = "POST"
        case put    = "PUT"
        case delete = "DELETE"
        case patch  = "PATCH"
    }
    
    enum HostHttp: String {
        case user    = "user"
        case parameter   = "parameter"
        case security    = "security"
        case profile    = "profile"
        case solicitud    = "solicitud"
        case consultaDoc    = "consultaDoc"
        case consultaPro    = "consultaPro"
        case consultaPJ    = "consultaPJ"
        case consultaVehi    = "consultaVehi"
        case consultaTive    = "consultaTive"
        case consultaAlert    = "consultaAlert"
        case consultaPartidaAlert    = "consultaPartidaAlert"
        case consultaMandatoAlert    = "consultaMandatoAlert"
        
    }
    
    
}

class WebServiceManager {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func doResquestToMethod(_ method: HTTPMethod, urlString: String, params: AnyObject? = nil, aditionalHeaders: [String: Any] = [:], handler: @escaping ServiceHandler) {
        
        var newUrl = urlString
        var host: HostHttp? = HostHttp.user
        print("urlString -> ", urlString)
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            if (urlString.contains(Constant.hostUser)) {
                host = HostHttp.user
                newUrl = urlString.replacingOccurrences(of: Constant.hostUser, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostParameter)) {
                host = HostHttp.parameter
                newUrl = urlString.replacingOccurrences(of: Constant.hostParameter, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSecurity)) {
                host = HostHttp.security
                newUrl = urlString.replacingOccurrences(of: Constant.hostSecurity, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostProfile)) {
                host = HostHttp.profile
                newUrl = urlString.replacingOccurrences(of: Constant.hostProfile, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSolicitud)) {
                host = HostHttp.solicitud
                newUrl = urlString.replacingOccurrences(of: Constant.hostSolicitud, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostConsulDoc)) {
                host = HostHttp.consultaDoc
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsulDoc, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultPropiedad)) {
                host = HostHttp.consultaPro
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultPropiedad, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultPJ)) {
                host = HostHttp.consultaPJ
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultPJ, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultVehicular)) {
                host = HostHttp.consultaVehi
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultVehicular, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultTive)) {
                host = HostHttp.consultaTive
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultTive, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostAlert)) {
                host = HostHttp.consultaAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostPartidaAlert)) {
                host = HostHttp.consultaPartidaAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostPartidaAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostMandatoAlert)) {
                host = HostHttp.consultaMandatoAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostMandatoAlert, with: Constant.ipHost)
            }
            
        }
        */
        print("newUrl -> ", newUrl)
        
        guard let urlService = URL(string: newUrl) else {
            print("La url es inválida")
            return
        }
        

        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = method.rawValue
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = self.createAditionalHeader(aditionalHeaders, host: host!)

        let urlSession = URLSession(configuration: sessionConfiguration)
        print(newUrl)
        method == .get ?
        downloadDataTask(urlSession, urlRequest: urlRequest, handler: handler) :
        uploadDataTask(urlSession, urlRequest: urlRequest, params: params, handler: handler)
    }
    
    func doResquestToMethodParameter(_ method: HTTPMethod, urlString: String, params: AnyObject? = nil, aditionalHeaders: [String: Any] = [:], handler: @escaping ServiceHandler) {
        
        var newUrl = urlString
        var host: HostHttp? = HostHttp.user
        
        
        print("urlString -> ", urlString)
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            if (urlString.contains(Constant.hostUser)) {
                host = HostHttp.user
                newUrl = urlString.replacingOccurrences(of: Constant.hostUser, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostParameter)) {
                host = HostHttp.parameter
                newUrl = urlString.replacingOccurrences(of: Constant.hostParameter, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSecurity)) {
                host = HostHttp.security
                newUrl = urlString.replacingOccurrences(of: Constant.hostSecurity, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostProfile)) {
                host = HostHttp.profile
                newUrl = urlString.replacingOccurrences(of: Constant.hostProfile, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSolicitud)) {
                host = HostHttp.solicitud
                newUrl = urlString.replacingOccurrences(of: Constant.hostSolicitud, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostConsulDoc)) {
                host = HostHttp.consultaDoc
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsulDoc, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultPropiedad)) {
                host = HostHttp.consultaPro
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultPropiedad, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultPJ)) {
                host = HostHttp.consultaPJ
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultPJ, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostConsultVehicular)) {
                host = HostHttp.consultaVehi
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultVehicular, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostConsultTive)) {
                host = HostHttp.consultaTive
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultTive, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostAlert)) {
                host = HostHttp.consultaAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostPartidaAlert)) {
                host = HostHttp.consultaPartidaAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostPartidaAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostMandatoAlert)) {
                host = HostHttp.consultaMandatoAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostMandatoAlert, with: Constant.ipHost)
            }
        }
         */
        print("newUrl -> ", newUrl)
        
        guard let urlService = URL(string: newUrl) else {
            print("La url es inválida")
            return
        }
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = method.rawValue
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = self.createAditionalHeaderCerti(aditionalHeaders, host: host!)
 
        let urlSession = URLSession(configuration: sessionConfiguration)
        print(newUrl)
        method == .get ?
        downloadDataTask(urlSession, urlRequest: urlRequest, handler: handler) :
        uploadDataTask(urlSession, urlRequest: urlRequest, params: params, handler: handler)
        
    }
    func getJSONResponseFromData(_ data: Data?) -> Any? {
        
        guard let data = data else {
            print("No existe respuesta del servicio")
            return  nil
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        print(jsonResponse ?? "Error en la lectura del servicio")
        
        return jsonResponse
    }
    
    func uploadDataTask(_ urlSession: URLSession, urlRequest: URLRequest, params: AnyObject?, handler: @escaping ServiceHandler) {
        
        var dataPrams: Data? = nil
        
        if let params = params {
            dataPrams = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        
        let task = urlSession.uploadTask(with: urlRequest, from: dataPrams ?? Data()) { (data, urlResponse, error) in
            if let error = error {
                print("Error: \(error)")
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                print("Invalid response")
                return
            }
            
            let statusCode = httpResponse.statusCode
            print("Status Code: \(statusCode)")
            if statusCode == 401 {
                
                self.goToLogin()
                /*
                let jsonResponse = "{}"
                DispatchQueue.main.async {
                    handler(jsonResponse)
                }
                */
                
            }
            
            if let requestURL = httpResponse.url {
                print("Request URL: \(requestURL)")
            }
            
            let jsonResponse = self.getJSONResponseFromData(data)
            DispatchQueue.main.async {
                handler(jsonResponse)
            }
        }
        
        task.resume()
    }
    
    func goToLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
    func downloadDataTask(_ urlSession: URLSession, urlRequest: URLRequest, handler: @escaping ServiceHandler) {
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
        
            if let error = error {
                print("Error: \(error)")
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                print("Invalid response")
                return
            }
            
            let statusCode = httpResponse.statusCode
            print("Status Code: \(statusCode)")
            if statusCode == 401 {
                self.goToLogin()
                /*
                let jsonResponse = "{}"
                DispatchQueue.main.async {
                    handler(jsonResponse)
                }
                */
            }
            
            if let requestURL = httpResponse.url {
                print("Request URL: \(requestURL)")
            }
            
            let jsonResponse = self.getJSONResponseFromData(data)
            DispatchQueue.main.async {
                handler(jsonResponse)
            }
            
            /*
            if let httpResponse = urlResponse as? HTTPURLResponse {
                if (httpResponse.statusCode == 401) {
                    UserPreferencesController.clearPreference()
                } else {
                    DispatchQueue.main.async {
                        handler(jsonResponse)
                    }
                }
            }*/
        }
        task.resume()
    }
    
    func loginDataTask(urlString: String, params: LoginRequest, handler: @escaping ServiceHandler) {
        
        var newUrl = urlString
        
        let credentials = String(format: "%@:%@", Constant.usernameSunarp, Constant.passwordSunarp)
        let encodedCredentials = credentials.data(using: String.Encoding.utf8)!.base64EncodedString()
        
        let dataPrams : Data = "username=\(params.username)&password=\(params.password)&grant_type=password".data(using: .utf8)!
        
        var header: [String: Any] = ["Content-Type": "application/x-www-form-urlencoded", "Authorization": "Basic " + encodedCredentials, "X-IBM-Client-Id": "658b4813f125bcff05778ac0db25face"]
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            if (urlString.contains(Constant.hostUser)) {
                header["Host"] = Constant.hostUser
                newUrl = urlString.replacingOccurrences(of: Constant.hostUser, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostParameter)) {
                header["Host"] = Constant.hostParameter
                newUrl = urlString.replacingOccurrences(of: Constant.hostParameter, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSecurity)) {
                header["Host"] = Constant.hostSecurity
                newUrl = urlString.replacingOccurrences(of: Constant.hostSecurity, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostProfile)) {
                header["Host"] = Constant.hostProfile
                newUrl = urlString.replacingOccurrences(of: Constant.hostProfile, with: Constant.ipHost)
            } else if (urlString.contains(Constant.hostSolicitud)) {
                header["Host"] = Constant.hostSolicitud
                newUrl = urlString.replacingOccurrences(of: Constant.hostSolicitud, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostConsultTive)) {
                header["Host"] = Constant.hostConsultTive
                newUrl = urlString.replacingOccurrences(of: Constant.hostConsultTive, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostAlert)) {
                header["Host"] = Constant.hostAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostPartidaAlert)) {
                header["Host"] = Constant.hostPartidaAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostPartidaAlert, with: Constant.ipHost)
            }else if (urlString.contains(Constant.hostMandatoAlert)) {
                header["Host"] = Constant.hostMandatoAlert
                newUrl = urlString.replacingOccurrences(of: Constant.hostMandatoAlert, with: Constant.ipHost)
            }
        }
        */
        
        guard let urlService = URL(string: newUrl) else {
            print("La url es inválida")
            return
        }
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = header
        
        let urlSession = URLSession(configuration: sessionConfiguration)
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = dataPrams
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let error = error {
                // Handle the error
                print("Error: \(error)")
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                print("Invalid response")
                return
            }
            
            let statusCode = httpResponse.statusCode
            print("Status Code: \(statusCode)")
            
            if let requestURL = httpResponse.url {
                print("Request URL: \(requestURL)")
            }
            // Now you can handle the response data if needed
            if let data = data {
                let jsonResponse = self.getJSONResponseFromData(data)
                DispatchQueue.main.async {
                    handler(jsonResponse)
                }
            }
        }
        
        task.resume()
    }
    
    func niubizDataTask(urlString: String, params: LoginRequest, handler: @escaping ServiceHandler) {

        let credentials = String(format: "%@:%@", params.username, params.password)
        let encodedCredentials = credentials.data(using: String.Encoding.utf8)!.base64EncodedString()
        let header: NSDictionary = [ "Authorization": "Basic " + encodedCredentials ]
        
        guard let urlService = URL(string: urlString) else {
            print("La url es inválida")
            return
        }
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        sessionConfiguration.httpAdditionalHeaders = header as? [AnyHashable: Any]
        
        let urlSession = URLSession(configuration: sessionConfiguration)
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = "POST"
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            let token =  String(data: data!, encoding: String.Encoding.utf8) ?? ""
            DispatchQueue.main.async {
                handler(token)
            }
        }
        
        task.resume()
    }
    
    func validateVersionAppTask(urlString: String, callback: @escaping (GenericEntity?, Int?) -> ()) {
        var  newUrl = urlString
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            newUrl = urlString.replacingOccurrences(of: Constant.hostSecurity, with: Constant.ipHost)
        }*/
        print(newUrl)
        guard let urlService = URL(string: newUrl) else { return }
        
        print("validateVersionAppTask")
        let sessionConfiguration = URLSessionConfiguration.default
        var header: [String: Any] = ["Content-Type": "application/json","systemName": "APPSNRPIOS", "X-IBM-Client-Id": "658b4813f125bcff05778ac0db25face"]
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            header["Host"] = Constant.hostSecurity
        }*/

        sessionConfiguration.httpAdditionalHeaders = header
        
        //let session = URLSession(configuration: sessionConfiguration, //delegate: self, delegateQueue:OperationQueue.main)
        let urlSession = URLSession(configuration: sessionConfiguration)
     
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                print("Invalid response")
                return
            }
            
            let statusCode = httpResponse.statusCode
            print("Status Code: \(statusCode)")
            
            
            do {
                if let data = data {
                    let decoder = JSONDecoder()
                    let dataResponse = try decoder.decode(GenericEntity.self, from: data)
                    DispatchQueue.main.async {
                        callback(dataResponse,httpResponse.statusCode)
                    }
                }
            }catch {
                print(error)
            }
        }
        task.resume()
        
    }
    /*
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            let trust = challenge.protectionSpace.serverTrust!
            NSLog("is self-signed: %@", trust.isSelfSigned.flatMap { "\($0)" } ?? "unknown" )
        }
        completionHandler(.performDefaultHandling, nil)
    }
    */
    
    func niubizPinHashDataTask(urlString: String, token: String, handler: @escaping ServiceHandler) {
        let header: NSDictionary = [ "Authorization": token ]
        
        guard let urlService = URL(string: urlString) else {
            print("La url es inválida")
            return
        }
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        sessionConfiguration.httpAdditionalHeaders = header as? [AnyHashable: Any]
        
        let urlSession = URLSession(configuration: sessionConfiguration)
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = "POST"
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            let jsonResponse = self.getJSONResponseFromData(data)
            DispatchQueue.main.async {
                handler(jsonResponse)
            }
        }
        
        task.resume()
    }
    
    func niubizTransactionDataTask(urlString: String, handler: @escaping ServiceHandler) {
        
        print("urlString -> ", urlString)
        let host = HostHttp.consultaPro
        // let newUrl = urlString.replacingOccurrences(of: Constant.hostConsultPropiedad, with: Constant.ipHost)
        
        let header = createAditionalHeader([:], host: host)
        
        guard let urlService = URL(string: urlString) else {
            print("La url es inválida")
            return
        }
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        sessionConfiguration.httpAdditionalHeaders = header as? [AnyHashable: Any]
        
        let urlSession = URLSession(configuration: sessionConfiguration)
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            let transactionId =  String(data: data!, encoding: String.Encoding.utf8) ?? ""
            DispatchQueue.main.async {
                print("transactionId -> ", transactionId)
                handler(transactionId)
            }
        }
        
        task.resume()
    }
    
    func createAditionalHeader(_ aditionalHeader: [String: Any], host: HostHttp) -> [String: Any] {
        
        var header: [String: Any] = ["Content-Type": "application/json", "systemName": "APPSNRPIOS", "X-IBM-Client-Id": "658b4813f125bcff05778ac0db25face"]
        
        for element in aditionalHeader {
            header[element.key] = element.value
        }
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            switch(host) {
            case .user:
                header["Host"] = Constant.hostUser
            case .parameter:
                header["Host"] = Constant.hostParameter
            case .security:
                header["Host"] = Constant.hostSecurity
            case .profile:
                header["Host"] = Constant.hostProfile
            case .solicitud:
                header["Host"] = Constant.hostSolicitud
            case .consultaDoc:
                header["Host"] = Constant.hostConsulDoc
            case .consultaPro:
                header["Host"] = Constant.hostConsultPropiedad
            case .consultaPJ:
                header["Host"] = Constant.hostConsultPJ
            case .consultaVehi:
                header["Host"] = Constant.hostConsultVehicular
            case .consultaTive:
                header["Host"] = Constant.hostConsultTive
            case .consultaAlert:
                header["Host"] = Constant.hostAlert
            case .consultaPartidaAlert:
                header["Host"] = Constant.hostPartidaAlert
            case .consultaMandatoAlert:
                header["Host"] = Constant.hostMandatoAlert
            }
        }
        */
        
        if (!UserPreferencesController.getAccessToken().isEmpty) {
            let accessToken = "\(UserPreferencesController.getTokenType().capitalized) \(UserPreferencesController.getAccessToken())"
            print(accessToken)
            header["Authorization"] = accessToken
        }
        
        return header
    }
    func createAditionalHeaderCerti(_ aditionalHeader: [String: Any], host: HostHttp) -> [String: Any] {
        
        var header: [String: Any] = ["Content-Type": "application/json","systemName": "APPSNRPIOS", "X-IBM-Client-Id": "658b4813f125bcff05778ac0db25face"]
        
        for element in aditionalHeader {
            header[element.key] = element.value
        }
        print("host 1: ", host)
        /*
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.develop.rawValue) {
            
            switch(host) {
            case .user:
                header["Host"] = Constant.hostUser
            case .parameter:
                header["Host"] = Constant.hostParameter
            case .security:
                header["Host"] = Constant.hostSecurity
            case .profile:
                header["Host"] = Constant.hostProfile
            case .solicitud:
                header["Host"] = Constant.hostSolicitud
            case .consultaDoc:
                header["Host"] = Constant.hostConsulDoc
            case .consultaPro:
                header["Host"] = Constant.hostConsultPropiedad
            case .consultaPJ:
                header["Host"] = Constant.hostConsultPJ
            case .consultaVehi:
                header["Host"] = Constant.hostConsultVehicular
            case .consultaTive:
                header["Host"] = Constant.hostConsultTive
            case .consultaAlert:
                header["Host"] = Constant.hostAlert
            case .consultaPartidaAlert:
                header["Host"] = Constant.hostPartidaAlert
            case .consultaMandatoAlert:
                header["Host"] = Constant.hostMandatoAlert
            }
        }
        */
        //print("header: ", header["Host"]!)
        if (!UserPreferencesController.getAccessToken().isEmpty) {
            let accessToken = "\(UserPreferencesController.getTokenType().capitalized) \(UserPreferencesController.getAccessToken())"
            print(accessToken)
            header["Authorization"] = accessToken
        }
        
        return header
    }
}
       /*
extension SecTrust {

    var isSelfSigned: Bool? {
        guard SecTrustGetCertificateCount(self) == 1 else {
            return false
        }
        guard let cert = SecTrustGetCertificateAtIndex(self, 0) else {
            return nil
        }
        return cert.isSelfSigned
    }
}

extension SecCertificate {

    var isSelfSigned: Bool? {
        guard
            let subject = SecCertificateCopyNormalizedSubjectSequence(self),
            let issuer = SecCertificateCopyNormalizedIssuerSequence(self)
        else {
            return nil
        }
        return subject == issuer
    }
}
*/
