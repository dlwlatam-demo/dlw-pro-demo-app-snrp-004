//
//  ServiceModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation
import UIKit

class ServiceModel {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    
    func getZonas(handler: @escaping ZonaResponseModel) {
        
        let service = ServiceService(navigationController:navigationController)
    service.getZonas { (arrayJsonResponse) in
            let arrayZona = ZonaResponse(jsonArray: arrayJsonResponse)
            handler(arrayZona)
        }
        
    }
    
    func getAreasRegistrales(handler: @escaping AreaResponseModel) {
        
        let service = ServiceService(navigationController:navigationController)
    service.getAreasRegistrales { (arrayJsonResponse) in
            let arrayArea = AreaResponse(jsonArray: arrayJsonResponse)
            handler(arrayArea)
        }
        
    }
    
    func getGrupoLibro(codGrupoLibroArea: String, handler: @escaping GrupoResponseModel) {
        
        let service = ServiceService(navigationController:navigationController)
    service.getGrupoLibro(codGrupoLibroArea: codGrupoLibroArea) { (arrayJsonResponse) in
            let arrayGrupo = GrupoResponse(jsonArray: arrayJsonResponse)
            handler(arrayGrupo)
        }
        
    }
    
    func postConsultaTitularidad(codZonas: [String], areaRegistral: String, apPaterno: String, apMaterno: String, nombre: String, razonSocial: String, derecho: String, sociedad: String, tipoTitular: String, userId: String, ipDispositivo: String, idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, eticket: String, codtienda: String, oriTarjeta: String, codAccion: String, dscCodAccion: String, respuesta: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, visanetResponse: PagoExitosoEntity, deviceId: String, userCrea: String, handler: @escaping TitularidadModel) {
        
        let consultaTitularidadRequest = ConsultaTitularidadRequest(codZonas: codZonas, areaRegistral: areaRegistral, apPaterno: apPaterno, apMaterno: apMaterno, nombre: nombre, razonSocial: razonSocial, derecho: derecho, sociedad: sociedad, tipoTitular: tipoTitular, userId: userId, ipDispositivo: ipDispositivo, idUnico: idUnico, numOrden: numOrden, pan: pan, codAutoriza: codAutoriza, nomEmisor: nomEmisor, eticket: eticket, codtienda: codtienda, oriTarjeta: oriTarjeta, codAccion: codAccion, dscCodAccion: dscCodAccion, respuesta: respuesta, fechaYhoraTx: fechaYhoraTx, concepto: concepto, idUser: idUser, transId: transId, visanetResponse: visanetResponse, deviceId: deviceId, userCrea: userCrea)
        
        let service = ServiceService(navigationController:navigationController)
        
        service.postConsultaTitularidad(consultaTitularidadRequest: consultaTitularidadRequest) { (arrayJsonResponse) in
            
            var arrayTitularidad = [TitularidadEntity]()
            
            for jsonResponse in arrayJsonResponse {
                let objJson = TitularidadEntity(json: jsonResponse)
                arrayTitularidad.append(objJson)
            }
            handler(arrayTitularidad)
        }
        
    }
    
    func postConsultaTitularidadAsync(codZonas: [String], areaRegistral: String, apPaterno: String, apMaterno: String, nombre: String, razonSocial: String, derecho: String, sociedad: String, tipoTitular: String, userId: String, ipDispositivo: String, idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, eticket: String, codtienda: String, oriTarjeta: String, codAccion: String, dscCodAccion: String, respuesta: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, visanetResponse: PagoExitosoEntity, deviceId: String, userCrea: String, handler: @escaping TitularidadAsyncModel) {
        
        let consultaTitularidadRequest = ConsultaTitularidadRequest(codZonas: codZonas, areaRegistral: areaRegistral, apPaterno: apPaterno, apMaterno: apMaterno, nombre: nombre, razonSocial: razonSocial, derecho: derecho, sociedad: sociedad, tipoTitular: tipoTitular, userId: userId, ipDispositivo: ipDispositivo, idUnico: idUnico, numOrden: numOrden, pan: pan, codAutoriza: codAutoriza, nomEmisor: nomEmisor, eticket: eticket, codtienda: codtienda, oriTarjeta: oriTarjeta, codAccion: codAccion, dscCodAccion: dscCodAccion, respuesta: respuesta, fechaYhoraTx: fechaYhoraTx, concepto: concepto, idUser: idUser, transId: transId, visanetResponse: visanetResponse, deviceId: deviceId, userCrea: userCrea)
        
        let service = ServiceService(navigationController:navigationController)
        
        service.postConsultaTitularidadAsync(consultaTitularidadRequest: consultaTitularidadRequest) { (jsonResponse) in
            
            let objJson = TitularidadAsyncEntity(json: jsonResponse)
            
            handler(objJson)
            
        }
        
    }

    
    func getStatusConsultaTitularidadAsync(guid: String, handler: @escaping TitularidadAsyncModel) {
        
        let service = ServiceService(navigationController:navigationController)
        service.getStatusConsultaTitularidadAsync(guid: guid) { (jsonResponse) in
            let objJson = TitularidadAsyncEntity(json: jsonResponse)
            handler(objJson)
            
        }
        
        
    }

    
    
    func getBusquedaJuridica(nombre: String, nombreOficinaRegistral: String, siglas: String, handler: @escaping BusquedadModel) {
        
        let service = ServiceService(navigationController:navigationController)
    service.getBusquedaJuridica(nombre: nombre, nombreOficinaRegistral: nombreOficinaRegistral, siglas: siglas) { (arrayJsonResponse) in
            
            var arrayBusqueda = [PersonaJuridicaEntity]()
            for jsonResponse in arrayJsonResponse {
                let objJson = PersonaJuridicaEntity(json: jsonResponse)
                arrayBusqueda.append(objJson)
            }
            handler(arrayBusqueda)
        }
    }
}
