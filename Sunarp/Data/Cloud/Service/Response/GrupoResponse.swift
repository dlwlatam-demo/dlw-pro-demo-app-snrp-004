//
//  GrupoResponse.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct GrupoResponse {
    
    var grupos  : [GrupoEntity]
    
    init(jsonArray: JSONArray) {
        
        var arrayGrupos = [GrupoEntity]()
        
        for json in jsonArray {
            arrayGrupos.append(GrupoEntity(json: json))
        }
        
        self.grupos = arrayGrupos
    }
}
