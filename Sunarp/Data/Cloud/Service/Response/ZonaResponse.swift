//
//  ZonaResponse.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct ZonaResponse {
    
    var zonas  : [ZonaEntity]
    
    init(jsonArray: JSONArray) {
        
        var arrayZonas = [ZonaEntity]()
        
        for json in jsonArray {
            arrayZonas.append(ZonaEntity(json: json))
        }
        
        self.zonas = arrayZonas
    }
}
