//
//  AreaResponse.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation

struct AreaResponse {
    
    var areas  : [AreaEntity]
    
    init(jsonArray: JSONArray) {
        
        var arrayAreas = [AreaEntity]()
        
        for json in jsonArray {
            arrayAreas.append(AreaEntity(json: json))
        }
        
        self.areas = arrayAreas
    }
}
