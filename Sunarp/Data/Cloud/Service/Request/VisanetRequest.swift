//
//  VisanetRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 1/10/22.
//

import Foundation

class VisanetRequest: NSObject {

    var idUnico: String = ""
    var numOrden: String = ""
    var pan: String = ""
    var codAutoriza: String = ""
    var nomEmisor: String = ""
    var estado: String = ""
    var eticket: String = ""
    var codtienda: String = ""
    var numReferencia: String = ""
    var eci: String = ""
    var oriTarjeta: String = ""
    var codAccion: String = ""
    var resCvv2: String = ""
    var reviewTransaction: String = ""
    var dscEci: String = ""
    var dscCodAccion: String = ""
    var respuesta: String = ""
    var impAutorizado: String = ""
    var decisionCs: String = ""
    var fechaYhoraTx: String = ""
    var concepto: String = ""
    var idUser: String = ""
    var transId: String = ""
    var usrKeyId: String = ""

    init(idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, estado: String, eticket: String, codtienda: String, numReferencia: String, eci: String, oriTarjeta: String, codAccion: String, resCvv2: String, reviewTransaction: String, dscEci: String, dscCodAccion: String, respuesta: String, impAutorizado: String, decisionCs: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, usrKeyId: String) {
        self.idUnico = idUnico
        self.numOrden = numOrden
        self.pan = pan
        self.codAutoriza = codAutoriza
        self.nomEmisor = nomEmisor
        self.estado = estado
        self.eticket = eticket
        self.codtienda = codtienda
        self.numReferencia = numReferencia
        self.eci = eci
        self.oriTarjeta = oriTarjeta
        self.codAccion = codAccion
        self.resCvv2 = resCvv2
        self.reviewTransaction = reviewTransaction
        self.dscEci = dscEci
        self.dscCodAccion = dscCodAccion
        self.respuesta = respuesta
        self.impAutorizado = impAutorizado
        self.decisionCs = decisionCs
        self.fechaYhoraTx = fechaYhoraTx
        self.concepto = concepto
        self.idUser = idUser
        self.transId = transId
        self.usrKeyId = usrKeyId
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["idUnico": idUnico, "numOrden": numOrden, "pan": pan, "codAutoriza": codAutoriza, "nomEmisor": nomEmisor, "estado": estado, "eticket": eticket, "codtienda": codtienda, "numReferencia": numReferencia, "eci": eci, "oriTarjeta": oriTarjeta, "codAccion": codAccion, "resCvv2": resCvv2, "reviewTransaction": reviewTransaction, "dscEci": dscEci, "dscCodAccion": dscCodAccion, "respuesta": respuesta, "impAutorizado": impAutorizado, "decisionCs": decisionCs, "fechaYhoraTx": fechaYhoraTx, "concepto": concepto, "idUser": idUser, "transId": transId, "usrKeyId": usrKeyId]
        print (bodyDic)
        return bodyDic
    }
    
}
