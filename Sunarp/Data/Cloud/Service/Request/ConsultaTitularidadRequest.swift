//
//  ConsultaTitularidadRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 1/10/22.
//

import Foundation

class ConsultaTitularidadRequest: NSObject {

    var codZonas: [String] = []
    var areaRegistral: String = ""
    var apPaterno: String = ""
    var apMaterno: String = ""
    var nombre: String = ""
    var razonSocial: String = ""
    var derecho: String = ""
    var sociedad: String = ""
    var tipoTitular: String = ""
    var userId: String = ""
    var ipDispositivo: String = ""
    var idUnico: String = ""
    var numOrden: String = ""
    var pan: String = ""
    var codAutoriza: String = ""
    var nomEmisor: String = ""
    var eticket: String = ""
    var codtienda: String = ""
    var oriTarjeta: String = ""
    var codAccion: String = ""
    var dscCodAccion: String = ""
    var respuesta: String = ""
    var fechaYhoraTx: String = ""
    var concepto: String = ""
    var idUser: String = ""
    var transId: String = ""
    var visanetResponse: PagoExitosoEntity = PagoExitosoEntity.init(json: [:])
    var deviceId: String = ""
    var userCrea: String = ""

    init(codZonas: [String], areaRegistral: String, apPaterno: String, apMaterno: String, nombre: String, razonSocial: String, derecho: String, sociedad: String, tipoTitular: String, userId: String, ipDispositivo: String, idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, eticket: String, codtienda: String, oriTarjeta: String, codAccion: String, dscCodAccion: String, respuesta: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, visanetResponse: PagoExitosoEntity, deviceId: String, userCrea: String) {
        self.codZonas = codZonas
        self.areaRegistral = areaRegistral
        self.apPaterno = apPaterno
        self.apMaterno = apMaterno
        self.nombre = nombre
        self.razonSocial = razonSocial
        self.derecho = derecho
        self.sociedad = sociedad
        self.tipoTitular = tipoTitular
        self.userId = userId
        self.ipDispositivo = ipDispositivo
        self.idUnico = idUnico
        self.numOrden = numOrden
        self.pan = pan
        self.codAutoriza = codAutoriza
        self.nomEmisor = nomEmisor
        self.eticket = eticket
        self.codtienda = codtienda
        self.oriTarjeta = oriTarjeta
        self.codAccion = codAccion
        self.dscCodAccion = dscCodAccion
        self.respuesta = respuesta
        self.fechaYhoraTx = fechaYhoraTx
        self.concepto = concepto
        self.idUser = idUser
        self.transId = transId
        self.visanetResponse = visanetResponse
        self.deviceId = deviceId
        self.userCrea = userCrea
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["codZonas": codZonas, "areaRegistral": areaRegistral, "apPaterno": apPaterno, "apMaterno": apMaterno, "nombre": nombre, "razonSocial": razonSocial, "derecho": derecho, "sociedad": sociedad, "tipoTitular": tipoTitular, "userId": userId, "ipDispositivo": ipDispositivo, "idUnico": idUnico, "numOrden": numOrden, "pan": pan, "codAutoriza": codAutoriza, "nomEmisor": nomEmisor, "eticket": eticket, "codtienda": codtienda, "oriTarjeta": oriTarjeta, "codAccion": codAccion, "dscCodAccion": dscCodAccion, "respuesta": respuesta, "fechaYhoraTx": fechaYhoraTx, "concepto": concepto, "idUser": idUser, "transId": transId, "visanetResponse": visanetResponse.bodyDictionary(), "deviceId": deviceId, "userCrea": userCrea]
        print (bodyDic)
        return bodyDic
    }
    
}
