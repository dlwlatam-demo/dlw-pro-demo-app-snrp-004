//
//  ServiceService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import Foundation
import UIKit

class ServiceService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func getZonas(_ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.getZonas()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (arrayJsonResponse) in
            
            let arrayJson = arrayJsonResponse as? JSONArray ?? [[:]]
            handler(arrayJson)
        }
    }
    
    func getAreasRegistrales(_ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.getAreasRegistrales()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (arrayJsonResponse) in
            
            let arrayJson = arrayJsonResponse as? JSONArray ?? [[:]]
            handler(arrayJson)
        }
    }
    
    func getGrupoLibro(codGrupoLibroArea: String, _ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.getGrupoLibro().replacingOccurrences(of: "{codGrupoLibroArea}", with: codGrupoLibroArea)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethod(.get, urlString: urlString) { (arrayJsonResponse) in
            
            let arrayJson = arrayJsonResponse as? JSONArray ?? [[:]]
            handler(arrayJson)
        }
    }
    
    func postConsultaTitularidad(consultaTitularidadRequest: ConsultaTitularidadRequest, _ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.postConsultaTitularidad()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: consultaTitularidadRequest.bodyDictionary()) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }

    func postConsultaTitularidadAsync(consultaTitularidadRequest: ConsultaTitularidadRequest, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.postConsultaTitularidadAsync()
        
        WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.post, urlString: urlString, params: consultaTitularidadRequest.bodyDictionary()) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func getStatusConsultaTitularidadAsync(guid: String, _ handler: @escaping JsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.getStatusConsultaTitularidadAsync().replacingOccurrences(of: "{guid}", with: guid)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }

    func getBusquedaJuridica(nombre: String, nombreOficinaRegistral: String, siglas: String, _ handler: @escaping ArrayJsonResponse) {
        
        let urlString = SunarpWebService.Busqueda.getBusquedaJuridica().replacingOccurrences(of: "{nombre}", with: nombre).replacingOccurrences(of: "{nombreOficinaRegistral}", with: nombreOficinaRegistral).replacingOccurrences(of: "{siglas}", with: siglas)
        
        WebServiceManager(navigationController:navigationController).doResquestToMethodParameter(.get, urlString: urlString) { (jsonResponse) in
            
            let arrayResult = jsonResponse as? JSONArray ?? []
            handler(arrayResult)
        }
    }
    
}
