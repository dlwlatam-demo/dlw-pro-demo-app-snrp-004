//
//  LoginService.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class LoginService {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
   
    func validateVersionAppTask(callback: @escaping (GenericEntity?, Int?) -> ()) {
        let urlString = SunarpWebService.Authorization.validateVersionAppURL()
        print(urlString)
        WebServiceManager(navigationController: navigationController).validateVersionAppTask(urlString: urlString) { jsonResponse, code in
            callback(jsonResponse, code)
        }
    }
    
    func postLogin(loginRequest: LoginRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Authorization.postLogin()
         
        WebServiceManager(navigationController: navigationController).loginDataTask(urlString: urlString, params: loginRequest) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
    func deleteLogout(jti: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Authorization.deleteLogout().replacingOccurrences(of: "{jti}", with: jti)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.delete, urlString: urlString) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    func postAlertLogin(loginAlertRequest: LoginAlertRequest, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Authorization.postAlertLogin()

        WebServiceManager(navigationController: navigationController).doResquestToMethod(.post, urlString: urlString, params: loginAlertRequest.bodyDictionary()) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
    
  
    
    func deleteAlertLogout(jti: String, _ handler: @escaping JsonResponse) {
    
        let urlString = SunarpWebService.Authorization.deleteLogout().replacingOccurrences(of: "{jti}", with: jti)
        
        WebServiceManager(navigationController: navigationController).doResquestToMethod(.delete, urlString: urlString) { (jsonResponse) in
            
            let json = jsonResponse as? JSON ?? [:]
            handler(json)
        }
    }
}
