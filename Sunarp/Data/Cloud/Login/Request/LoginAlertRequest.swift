//
//  LoginRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class LoginAlertRequest: NSObject {

    var email: String = ""
    var password: String = ""

    init(email: String, password: String) {
        self.email = email
        self.password = password
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["email": email, "password": password]
        print (bodyDic)
        return bodyDic
    }
    
}
