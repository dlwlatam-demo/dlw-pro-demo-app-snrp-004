//
//  LoginRequest.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class LoginRequest: NSObject {

    var username: String = ""
    var password: String = ""

    init(username: String, password: String) {
        self.username = username
        self.password = password
        super.init()
    }
    
    func bodyDictionary() -> NSDictionary {
        let bodyDic : NSDictionary = ["username": username, "password": password]
        print (bodyDic)
        return bodyDic
    }
    
}
