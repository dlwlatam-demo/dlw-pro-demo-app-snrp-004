//
//  LoginModel.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation
import UIKit

class LoginModel {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    func validateVersionAppTask(callback: @escaping (GenericEntity?, Int?) -> ()) {
        let service = LoginService(navigationController: navigationController)
    service.validateVersionAppTask() { jsonResponse, resultCode in
            callback(jsonResponse,resultCode)
        }
    }
    
    func postLogin(userName: String, password: String, handler: @escaping LoginResponseModel) {
        
        let loginRequest = LoginRequest(username: userName, password: password)
        
        let service = LoginService(navigationController: navigationController)
    service.postLogin(loginRequest: loginRequest)  { (jsonResponse) in
            let objLogin = LoginEntity(json: jsonResponse)
            handler(objLogin)
        }
        
    }
    
    func deleteLogout(jti: String, handler: @escaping LogoutResponseModel) {
                
        let service = LoginService(navigationController: navigationController)
    service.deleteLogout(jti: jti)  { (jsonResponse) in
            let objSolicitud = LogoutEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    func postAlertLogin(email: String, password: String, handler: @escaping LoginAlertResponseModel) {
        
        let loginRequest = LoginAlertRequest(email: email, password: password)
      
        let service = LoginService(navigationController: navigationController)
        print("postAlertLogin")
    service.postAlertLogin(loginAlertRequest: loginRequest)  { (jsonResponse) in
        dump(jsonResponse)
            let objSolicitud = LoginAlertEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
    
    func deleteAlertLogout(jti: String, handler: @escaping LogoutResponseModel) {
                
        let service = LoginService(navigationController: navigationController)
    service.deleteLogout(jti: jti)  { (jsonResponse) in
            let objSolicitud = LogoutEntity(json: jsonResponse)
            handler(objSolicitud)
        }
        
    }
}
