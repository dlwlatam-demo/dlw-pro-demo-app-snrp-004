//
//  SDCTextField.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 2/09/23.
//

import Foundation
import UIKit

enum ValueType: Int {
    case none
    case onlyLetters
    case onlyNumbers
    case phoneNumber   // Allowed "+0123456789"
    case alphaNumeric
    case fullName
    case alphaNumericWithSpace// Allowed letters and space
    case fullNameWithSpace
    case alphaNumericCML
    case razonSocial
    case placaAuto
}

class SDCTextField: UITextField {
    
    @IBInspectable var maxLengths: Int = 0 // Max character length
    var valueType: ValueType = ValueType.none // Allowed characters

    @IBInspectable var allowedCharInString: String = ""
    
    func getMaxLengths() -> Int {
        return maxLengths
    }

    func verifyFields(shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch valueType {
        case .none:
            break // Do nothing
            
        case .onlyLetters:
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            
        case .onlyNumbers:
            let numberSet = CharacterSet.decimalDigits
            if string.rangeOfCharacter(from: numberSet.inverted) != nil {
                return false
            }
            
        case .phoneNumber:
            let phoneNumberSet = CharacterSet(charactersIn: "+0123456789")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
            }
        case .alphaNumericWithSpace:
            let phoneNumberSet = CharacterSet(charactersIn: "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
            }
        case .fullNameWithSpace:
                let phoneNumberSet = CharacterSet(charactersIn: "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz¨-.' ")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
            }
        case .alphaNumericCML:
                let phoneNumberSet = CharacterSet(charactersIn: "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
        }
        case .placaAuto:
                let phoneNumberSet = CharacterSet(charactersIn: "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
        }
            
        case .razonSocial:
            let phoneNumberSet = CharacterSet(charactersIn: "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz¨-.' ")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
        }
            
        case .alphaNumeric:
            let alphaNumericSet = CharacterSet.alphanumerics
            if string.rangeOfCharacter(from: alphaNumericSet.inverted) != nil {
                return false
            }
            
        case .fullName:
            var characterSet = CharacterSet.letters
            print(characterSet)
            characterSet = characterSet.union(CharacterSet(charactersIn: " "))
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
        }
        
        if let text = self.text, let textRange = Range(range, in: text) {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if maxLengths > 0, maxLengths < finalText.utf8.count {
                return false
            }
        }

        // Check supported custom characters
        if !self.allowedCharInString.isEmpty {
            let customSet = CharacterSet(charactersIn: self.allowedCharInString)
            if string.rangeOfCharacter(from: customSet.inverted) != nil {
                return false
            }
        }
        
        return true
    }
}

extension SDCTextField {
    func clearText() {
        self.text = "" // Establece el texto del campo como una cadena vacía
    }
    
    var isEmpty: Bool {
            if let text = self.text {
                let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
                return trimmedText.isEmpty
            }
            return true
        }
    
    func characterCount() -> Int {
            if let text = self.text {
                return text.count
            }
            return 0
        }
}
