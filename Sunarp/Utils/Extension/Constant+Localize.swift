//
//  Constant+Localize.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 22/07/23.
//

import Foundation

extension Constant {
    
    enum Localize {
        static var aceptar = "Aceptar"
        static var cancelar = "Cancelar"
        static var allPoints = "Todos"
        static var información = "Información"
        static var apiKeyGooleMaps = "AIzaSyDBv17vk9ZDkPqIbWan3iD3xfQS4Fzxam0"
        static var urlGoogleDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    }
    
}
