//
//  Constants.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 24/07/22.
//

import Foundation

struct Constant {
    static var backgroundColorTop = "#323c37"
    static var backgroundColorBottom = "#252c29"
    static var labelLinkActiveColor = "#00A5A5"
    static var labelLinkInactiveColor = "#B3B3B3"
    
    static var usernameSunarp = "app_sunarp"
    static var passwordSunarp = "Sunarp2022!"
    
//    static var hostUser: String = "app-mto-user-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostUser: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostParameter: String = "app-parametros-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostParameter: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostSecurity: String = "app-oauth-seguridad-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostSecurity: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostProfile: String = "app-seguridad-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostProfile: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostSolicitud: String = "app-solicitud-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostSolicitud: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostConsulDoc: String = "app-consultas-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostConsulDoc: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostConsultPropiedad: String = "app-consulta-propiedad-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostConsultPropiedad: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostConsultPJ: String = "app-consulta-propiedad-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostConsultPJ: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostConsultVehicular: String = "app-consulta-vehicular-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostConsultVehicular: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostConsultTive: String = "app-consulta-vehicular-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostConsultTive: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostAlert: String = "app-alerta-mto-user-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostAlert: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostPartidaAlert: String = "app-alerta-partida-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostPartidaAlert: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var hostMandatoAlert: String = "app-alerta-mandato-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe"
    static var hostMandatoAlert: String = "api-gateway-nonprod.sunarp.gob.pe:9443"
//sunarp-desarrollo/app-sunarpp/appsunarp_servicios
    static var ipHost: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443"
    //    static var ipHost: String = "172.18.1.205"

    static var TYPE_DOCUMENT_CODE_DNI = "09"
    static var TYPE_DOCUMENT_CODE_CE = "03"
    static var TYPE_DOCUMENT_CODE_RUC = "05"
    
    static var APP_ID = "APP SUNARP"
    static var DEVICE_OS = "IOS"
    
    static var COD_COPIA_LITERAL = "6970717273"
    
    static var VISA_KEYS_INSTANCIA_DEBUG = "2"
    static var VISA_KEYS_ACCESS_DEBUG = "p0MlvYUI7EmgZPj"
    
    static var VISA_KEYS_INSTANCIA_RELEASE = "1"
    static var VISA_KEYS_ACCESS_RELEASE = "iMp1ws4Yp0exw1o"
    
    static var API_USRID = "APPSNRPIOS"
    static var API_TERMS_CONDITIONS = "https://www.sunarp.gob.pe/politicas-privacidad.aspx"
    
    
    //Constant services
    static var REGISTRO_DE_PROPIEDAD = "21000"
    static var CERTIFICADO_NEGATIVO_PREDIOS = "61"
    static var CERTIFICADO_NEGATIVO_PREDIOS_B = "93"
    static var CERTIFICADO_POSITIVO_PREDIOS = "60"
    static var CERTIFICADO_POSITIVO_PREDIOS_B = "92"
    
    
    
    static var CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_A = "74"
    static var CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B = "75"
    static var CERTIFICADO_DE_CARGAS_Y_GRAVAMENES = "85"
    
    
    static var REGISTRO_DE_PERSONAS_JURIDICAS = "22000"
    static var CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA = "101"
    static var CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA = "100"
    static var CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA_B = "104"
    
    static var REGISTRO_DE_PERSONAS_NATURALES = "23000"
    static var CERTIFICADO_VIGENCIA_DESIGNACION_APOYO = "77"
    static var CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR = "78"
    static var CERTIFICADO_VIGENCIA_PODER = "50"
    static var CERTIFICADO_VIGENCIA_PODER_B = "91"
    static var CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA = "57"
    static var CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B = "83"
    static var CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA = "56"
    static var CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B = "89"
    static var CERTIFICADO_NEGATIVO_UNION_DE_HECHO = "86"
    static var CERTIFICADO_POSITIVO_UNION_DE_HECHO = "87"
    static var CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL = "66"
    static var CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B = "99"
    static var CERTIFICADO_NEGATIVO_DE_TESTAMENTOS = "63"
    static var CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B = "103"
    static var CERTIFICADO_POSITIVO_DE_TESTAMENTOS = "62"
    static var CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B = "102"
    static var CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA = "76"
    static var CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ = "84"
    static var CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO = "88"
    
    static var REGISTRO_DE_BIENES_INMUEBLES = "24000"
    static var CERTIFICADO_REGISTRAL_VEHICULAR = "45"
    static var CERTIFICADO_REGISTRAL_VEHICULAR_B = "98"
    
    static var CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR = "58"
    static var CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B = "94"
    static var CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR = "59"
    static var CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B = "95"
    
    static var CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES = "46"
    static var CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B = "96"
    static var CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES = "47"
    static var CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B = "97"
    
    
    static var CERTIFICADO_COPIA_LITERAL_PI = "69"
    static var CERTIFICADO_COPIA_LITERAL_PJ = "70"
    static var CERTIFICADO_COPIA_LITERAL_PN = "71"
    static var CERTIFICADO_COPIA_LITERAL_BM = "72"

    enum Identifier {
        static let pagoRechazadoViewController = "PagoRechazadoViewController"
        static let pagoExitosoViewController = "PagoExitosoViewController"
        static let paymentsViewController = "PaymentsViewController"
        static let pagoExitosoCertificadoLiteralvewController = "PagoExitosoCertificadoLiteralvewController"
        static let publicidadOnlyCertiPJViewController = "PublicidadOnlyCertiPJViewController"
        static let publicidadRegistroPerNatViewController =  "PublicidadRegistroPerNatViewController"
    }
    
    enum StoryBoardName {
     
        static let publicidadCertificada = "PublicidadCertificada"
        static let payments = "Payments"
    }
        
}

struct App {
    static var bundle = Bundle(identifier: "pe.gob.sunarp.app-ios")
    static var version = "3.0"
    static var user = "APPSNRPIOS"
}
