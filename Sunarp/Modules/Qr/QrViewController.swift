//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import WebKit

class QrViewController: UIViewController {
        
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var toScanerView: UIView!
    @IBOutlet weak var viewWebDetail: UIView!
    
    @IBOutlet weak var webViewCons: WKWebView!
    
    var loading: UIAlertController!
    var urlQr:String = ""
    
    private lazy var presenter: QrPresenter = {
        return QrPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        let defaults = UserDefaults.standard
        urlQr = defaults.string(forKey: "urlNameKey")!
        
        if (urlQr.count > 1) {
             
            viewWebDetail.backgroundColor = SunarpColors.dark
            viewWebDetail.isHidden = false
            let myURL = URL(string: urlQr)
               let myRequest = URLRequest(url: myURL!)
               webViewCons.load(myRequest)
            
            
            let defaults = UserDefaults.standard
            defaults.set("", forKey: "urlNameKey")
        }
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        self.formView.backgroundCard()
  
        toScanerView.primaryButton()
        viewWebDetail.isHidden = true
    }
    
    private func addGestureView(){
      
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toScanerView.addGestureRecognizer(tapConfirmGesture)
    
    }
    
    @objc private func onTapToConfirmView() {
      
        if self.storyboard != nil {
            //if let qrReaderViewController = storyboardValue.instantiateViewController(withIdentifier: "QrReaderViewController") as? QrReaderViewController {
            let qrReaderViewController = QrReaderViewController()
            //navigationController?.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(qrReaderViewController, animated: true)
            //self.present(vc, animated: true, completion: nil)
            //}
        }
    }
    
    @IBAction func btnClose() {
        
        viewWebDetail.isHidden = true
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @IBAction func onTapMenu(_ sender: Any) {
        let drawerController = MenuDrawerViewController(delegate: self)
        present(drawerController, animated: true)
    }
    
}


extension QrViewController: QrReaderViewControllerDelegate{
    func capture(action:String) {
      
        viewWebDetail.isHidden = false
        let myURL = URL(string: action)
           let myRequest = URLRequest(url: myURL!)
           webViewCons.load(myRequest)
    }
    
    
}


extension QrViewController: MenuDrawerDelegate {
    
    func menuDrawerLogoutSelected() {
        self.presenter.logout()
    }
    
    func goToLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
    
}
