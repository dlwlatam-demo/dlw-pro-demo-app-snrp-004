//
//  QrPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 19/09/22.
//

import Foundation

class QrPresenter {
    
    private weak var controller: QrViewController?
    
    lazy private var loginModel: LoginModel = {
        let navigation = controller?.navigationController
       return LoginModel(navigationController: navigation!)
    }()
    
    init(controller: QrViewController) {
        self.controller = controller
    }
    
}

extension QrPresenter: GenericPresenter {
    
    func logout() {
        self.controller?.loaderView(isVisible: true)
        self.loginModel.deleteLogout(jti: UserPreferencesController.getJti()) { (logoutResponse) in
            let state = logoutResponse.resultado == "0" || logoutResponse.resultado == ""
            if (state) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.controller?.loaderView(isVisible: false)
                    UserPreferencesController.clearPreference()
                    self.controller?.goToLogin()
                }
            }
        }

    }
    
}
