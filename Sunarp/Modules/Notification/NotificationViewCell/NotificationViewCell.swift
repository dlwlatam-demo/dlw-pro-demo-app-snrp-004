//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class NotificationViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "NotificationViewCell"
  //  @IBOutlet var profileImage:UIImageView!
    @IBOutlet var titleLabel:UILabel!
    
    @IBOutlet var strBusqLabel:UILabel!
    @IBOutlet var fecHorLabel:UILabel!
    @IBOutlet var estadoLabel:UILabel!
    @IBOutlet var SolicitudIdLabel:UILabel!
    
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
     @IBOutlet var viewPoint:UIView!
    
    @IBOutlet weak var addButtonView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    var subjectList: [String] = [] {
        didSet {
          //  titleNameLabel?.text = subjectList.joined(separator: ", ")
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.layer.cornerRadius = 10
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
      
        
        //titleLabel.textColor = 
        
    }
    func setup(with solicitud: NewsTransaccionEntity) {
        
      //  print("itttemm:", solicitud.titular)
        strBusqLabel?.text = solicitud.strBusq
        fecHorLabel?.text = "Fecha: \(solicitud.fecHor)"
        estadoLabel?.text =  "Estado: \(solicitud.estado)"
       // SolicitudIdLabel?.text = solicitud.strBusq
        
        
           let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
           self.addButtonView.addGestureRecognizer(onTapVerSolicitud)
        
    }
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /*
    func loadWithData(info:DoctorProperties){
        
        let fileName = info.photo!
        let fileArray = fileName.components(separatedBy: "/")
        let finalFileName = fileArray.last
        self.profileImage.sd_setImage(with: URL(string: info.photo!), placeholderImage: UIImage(named: String(finalFileName!)))
        self.titleNameLabel.text = "\(String(info.first_name!)) \n \(String(info.last_name!))"
        //self.titleEmail.text = "\(String(info.bank_email!))"
    }
    
    func loadWithDataFicha(info:MedicalAttentionSDProCompleted){
        
        let fileName = info.patient_photo!
        let fileArray = fileName.components(separatedBy: "/")
        let finalFileName = fileArray.last
        self.profileImage.sd_setImage(with: URL(string: info.patient_photo!), placeholderImage: UIImage(named: String(finalFileName!)))
        self.titleNameLabel.text = "\(String(info.patient_name!))"
        self.titleEmail.text = ""
        //self.titleEmail.text = "\(String(info.patient_name!))"
    }
     */
}
