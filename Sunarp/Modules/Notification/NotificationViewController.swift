//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift

class NotificationViewController: UIViewController {
    
    
    var style: Style = Style.myApp
    let disposebag = DisposeBag()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    
    var isValidDocument: Bool = false
    
    var numDocumentSol: Int = 0
    // var numDocumentSol: Int = 0
    
    var notifications: [NewsTransaccionEntity] = []
    
    @IBOutlet var tableSearchView:UITableView!
    
    private lazy var presenter: NotificationPresenter = {
        return NotificationPresenter(controller: self)
    }()
    
    
    var loading: UIAlertController!
    
    let subjectsDict = ["Spanish": ["Lesson 1", "Lesson 2", "Lesson 2", "Lesson 2", "Lesson 2", "Lesson 2", "Lesson 2"], "Math":["Problem set 1", "Problem set 2", "Problem set 2", "Problem set 2", "Problem set 2", "Problem set 2", "Problem set 2", "Problem set 2"], "Science": ["Lab"]]
    let subjectArray = ["Spanish", "Math", "Science"]
    
    let array = ["GAFDGSG","VSBFFSB","BFBFB"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        //addGestureView()
        
        self.tableSearchView.reloadData()
        setupNavigationBar()
        self.presenter.getNewsTransaccion()
        registerCell()
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Notificaciones")
        
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
        // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        //  self.headerView.backgroundColorGradientHeader()
        //   self.formView.backgroundCard()
        
        self.tableSearchView.delegate = self
        self.tableSearchView.dataSource = self
        
    }
    
    func registerCell() {
        //Celda Foto perfil
        let profileCellNib = UINib(nibName: NotificationViewCell.reuseIdentifier, bundle: nil)
        self.tableSearchView.register(profileCellNib, forCellReuseIdentifier: NotificationViewCell.reuseIdentifier)
        
    }
    func loadPartidas(allPartidaResponse: [NewsTransaccionEntity]) {
        self.notifications = allPartidaResponse
        //self.loaderView(isVisible: false)
        
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            //self.insertNumTextField.resignFirstResponder()
            
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.notifications.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
                
            }
        }
        
        self.tableSearchView.reloadData()
        //self.insertNumTextField.resignFirstResponder()
        self.loaderView(isVisible: false)
        
    }
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            //    self.stopLoader(loader: self.loading)
        }
    }
    func loadDataSolicitud(solicitud: HistoryDetailEntity) {
        if (!solicitud.solicitudId.isEmpty) {
            isValidDocument = true
            let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EstadoSolicitudViewController") as! EstadoSolicitudViewController
            vc.isValidDocument = isValidDocument
            vc.solicitudDetail = solicitud
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

// MARK: - Table view datasource
extension NotificationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        // return self.dateDataScheduleModel?.rows.count ?? 0
        return notifications.count
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rawCell = tableView.dequeueReusableCell(withIdentifier: "NotificationViewCell", for: indexPath)
        guard let cell = rawCell as? NotificationViewCell else {
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        
        let listaPro = notifications[indexPath.row]
        
        cell.setup(with: listaPro)
        cell.onTapVerSolicitud = { [weak self] in
            self?.presenter.didLoadSolicitud(numDocumentSol: listaPro.solicitudId)
        }
        return cell
    }
    
}

// MARK: - Table view delegate
extension NotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*  guard let delegate = delegate, let categories = self.doctorPropertiesModel?.services[indexPath.row] else {
         
         DispatchQueue.main.async {
         self.router.show(view: .scheduled, sender: self)
         }
         return
         }*/
        // let categories = self.dateDataScheduleModel?.rows[indexPath.row]
    
    }
}
