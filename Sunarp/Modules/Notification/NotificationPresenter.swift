//
//  LoginPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/08/22.
//

import Foundation

class NotificationPresenter {
    
    private weak var controller: NotificationViewController?
    
    lazy private var model: NotificationModel = {
        let navigation = controller?.navigationController
        return NotificationModel(navigationController: navigation!)
    }()
    lazy private var modelHis: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    init(controller: NotificationViewController) {
        self.controller = controller
    }
    
}


extension NotificationPresenter: GenericPresenter {
    
    func getNewsTransaccion() {
       
     //   self.controller?.loaderView(isVisible: true)
        self.model.getNewsTransaccion{ (arrayPartidas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadPartidas(allPartidaResponse: arrayPartidas)
        }
        
    }
    
    func didLoadSolicitud(numDocumentSol: Int?) {
        if let solicitudId = numDocumentSol {
            self.modelHis.getHistorialDetalle(id: String(solicitudId)) { (objHistory) in
                self.controller?.loadDataSolicitud(solicitud: objHistory)
            }
        }
    }
}
