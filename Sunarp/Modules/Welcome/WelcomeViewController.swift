//
//  WelcomeViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/07/22.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var loginView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupDesigner() {
        self.view.backgroundColorGradient()
        self.loginView.primaryButton()
        self.registerView.secondaryButton()
        self.addGestureView()
    }
    
    private func addGestureView(){
        let onTapLogin = UITapGestureRecognizer(target: self, action: #selector(onTapLoginButton))
        self.loginView.addGestureRecognizer(onTapLogin)
        
        let onTapRegister = UITapGestureRecognizer(target: self, action: #selector(onTapRegisterButton))
        self.registerView.addGestureRecognizer(onTapRegister)
    }
    
    @objc private func onTapLoginButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapRegisterButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PhoneRegisterViewController") as! PhoneRegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
