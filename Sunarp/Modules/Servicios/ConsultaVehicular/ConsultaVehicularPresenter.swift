//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class ConsultaVehicularPresenter {
    private weak var controller: ConsultaVehicularViewController?
    lazy private var model: ConsultaVehicularModel = {
        let navigation = controller?.navigationController
       return ConsultaVehicularModel(navigationController: navigation!)
    }()
    init(controller: ConsultaVehicularViewController) {
        self.controller = controller
    }
}

extension ConsultaVehicularPresenter: GenericPresenter {
    
    func didLoad() {
        self.controller?.loaderView(isVisible: true)
        let guid = UserPreferencesController.getGuid()
        self.model.getListaOficinaRegistral() { (arrayTipoOficina) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadListaOficina(arrayOficinaRegistral: arrayTipoOficina)
        }
    }
    
    func validateData() {
       
        let numDocument = self.controller?.numPlacaTextField.text ?? ""
     
        if (numDocument.isEmpty) {
            self.controller?.goToConfirmDatos(false, message: "Los campos no pueden estar vacio.")
        } else {
            self.controller?.goToConfirmDatos(true, message: "")
        }
    }
    
    
    func searchVehiculo() {
        self.controller?.loaderView(isVisible: true)
          
        let codigoRegi = self.controller?.codigoRegi ?? ""
        let codigoSede = self.controller?.codigoSede ?? ""
        let newStr = self.controller?.nuPlac.replacingOccurrences(of: "-", with: "") ?? .empty
        let nuPlac = newStr
            
        self.model.postListarConsultaVehicular(nuPlac: nuPlac.uppercased(), codigoRegi: codigoRegi, codigoSede: codigoSede, ipAddress: "", idUser: App.user, appVersion: App.version) { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            let state = !objConsulta.datosVehiculo.isEmpty
            if (state) {
                UserPreferencesController.clearGuid()
            }
            self.controller?.loadDatosConsultaVehicular(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
        }
    }
    
}
