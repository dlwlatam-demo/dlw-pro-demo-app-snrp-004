//
//  DetailConsultaVehicularPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 16/12/22.
//

import Foundation

class DetailConsultaVehicularPresenter {
    private weak var controller: DetailConsultaVehicularViewController?
    lazy private var model: ConsultaVehicularModel = {
        let navigation = controller?.navigationController
       return ConsultaVehicularModel(navigationController: navigation!)
    }()
    init(controller: DetailConsultaVehicularViewController) {
        self.controller = controller
    }
}

extension DetailConsultaVehicularPresenter: GenericPresenter {
    
    func obtenerCosto() {
        self.controller?.loaderView(isVisible: true)
          
        let codZona = self.controller?.codZona ?? ""
        let codOficina = self.controller?.codOficina ?? ""
            
        self.model.postCosto(flg: "1", zona: codZona, oficina: codOficina, codLibroArea: 0, partida: "") { objCosto in
            
            let state = objCosto.msgResult.isEmpty
            self.controller?.loadFormularioPago(state, message: objCosto.msgResult, costoEntity: objCosto)
        }
    }
    
}

