//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

class DetailConsultaVehicularViewController: UIViewController {
        
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    var loading: UIAlertController!
    
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var addPhotoView: UIView!
    @IBOutlet weak var addAdjuntoView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet weak var formViewEditar: UIView!
    @IBOutlet weak var formViewHist: UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    
    
    @IBOutlet weak var photoImage: UIImageView!
    
    var valImage: String = ""
    var codZona: String = ""
    var codOficina: String = ""
    var placa: String = ""
    
    private lazy var presenter: DetailConsultaVehicularPresenter = {
       return DetailConsultaVehicularPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        
    }
        
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Información del Vehículo")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        
        self.formView.backgroundCard()
        
        
      //  self.formViewEditar.backgroundCard()
        
        
        let str = valImage
        self.photoImage.image = convertBase64StringToImage(imageBase64String: str)
       
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image
    }
    
    private func addGestureView(){
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @objc private func onTapToConfirmView() {
        presenter.obtenerCosto()
    }
    
    
    func loadFormularioPago(_ state: Bool, message: String, costoEntity: CostoEntity) {
        if (state) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.loaderView(isVisible: false)
                let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LiquidacionSolicitudViewController") as! LiquidacionSolicitudViewController
                vc.nsolMonLiq = String(costoEntity.monto)
                vc.codZona = self.codZona
                vc.codOficina = self.codOficina
                vc.placa = self.placa
                vc.typeController = .consultaVehicular
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.loaderView(isVisible: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
}
