//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class ConsultaVehicularViewController: UIViewController, UITextFieldDelegate {
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var addPhotoView: UIView!
    @IBOutlet weak var addAdjuntoView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet weak var formViewEditar: UIView!
    @IBOutlet weak var formViewHist: UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    
    
    @IBOutlet weak var numPlacaTextField: UITextField!
    @IBOutlet weak var selecCiudadTextField: UITextField!
    
    var ciudad: String = ""
    var nuPlac: String = ""
    var select_placa: String = ""
    
    var codigoRegi: String = ""
    var codigoSede: String = ""
    var ipAddress: String = ""
    var idUser: String = ""
    var appVersion: String = ""
    
    
    var indexSelect = 0
    var valDone: Bool = true
    
    var valImage: String = ""
    
    var ciudades: [String] = ["Lima", "Arequipa", "Ica"]
    
    var ciudadPickerView = UIPickerView()
    
    var listaOficinaRegistralEntities: [OficinaRegistralEntity] = []
    
    var loading: UIAlertController!
    
    var textNumberPlaca:String = ""
    
    private lazy var presenter: ConsultaVehicularPresenter = {
        return ConsultaVehicularPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupDesigner()
        addGestureView()
        
        setupNavigationBar()
        
        self.presenter.didLoad()
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Consulta Vehicular/Boleta Informativa")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiciosViewController") as! ServiciosViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
        // router.pop(sender: self)
        // self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func loadListaOficina(arrayOficinaRegistral: [OficinaRegistralEntity]) {
        self.listaOficinaRegistralEntities = arrayOficinaRegistral
        self.ciudadPickerView.delegate = self
        self.ciudadPickerView.dataSource = self
        
        self.selecCiudadTextField.inputView = self.ciudadPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    private func setupDesigner() {

        numPlacaTextField.border()
        numPlacaTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        selecCiudadTextField.border()
        selecCiudadTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        self.selecCiudadTextField.inputAccessoryView = self.createToolbar()
        self.selecCiudadTextField.isHidden = true
        self.formView.backgroundCard()
        
        
        self.ciudadPickerView.tag = 1
        self.numPlacaTextField.delegate = self
        self.ciudadPickerView.delegate = self
        self.ciudadPickerView.dataSource = self
        self.selecCiudadTextField.inputView = self.ciudadPickerView
        
        //  self.formViewEditar.backgroundCard()
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func addGestureView(){
        
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: self, action: #selector(donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                          target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    @objc func donePressed() {
        if valDone == true {
            // self.selecCiudadTextField.text = listaOficinaRegistralEntities[indexSelect].Nombre
            // self.selecCiudadTextField.resignFirstResponder()
            self.selecCiudadTextField.text = ""
            self.codigoRegi = "00" //self.listaOficinaRegistralEntities[indexSelect].RegPubId
            self.codigoSede = "00" // self.listaOficinaRegistralEntities[indexSelect].OficRegId
        }
    }
    
    func goToConfirmDatos(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                validateTypePlaca()
                /*
                 let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
                 // vc.array = users
                 self.navigationController?.pushViewController(vc, animated: true)
                 */
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
    func loadDatosConsultaVehicular(_ state: Bool, message: String, jsonValidacion: InfoVehicularEntity) {
        
        if (state) {
            if (state) {
                valImage = jsonValidacion.datosVehiculo
                // validateTypeDocument()
                
                
                let storyboard = UIStoryboard(name: "Service", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaVehicularViewController") as! DetailConsultaVehicularViewController
                vc.valImage = valImage
                vc.placa = nuPlac
                vc.codZona = codigoRegi
                vc.codOficina = codigoSede
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    
    
    func validateTypePlaca() {
        self.nuPlac = self.numPlacaTextField.text ?? ""
        self.select_placa = self.selecCiudadTextField.text ?? ""
        
        if (!nuPlac.isEmpty && !select_placa.isEmpty) {
            self.presenter.searchVehiculo()
        }
        
    }
    
    @objc private func onTapToConfirmView() {
        
        //  validateTypeDocument()
        self.presenter.validateData()
        
        
        
    }
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func changeTypeCiudad(index: Int) {
        
        valDone = true
        self.indexSelect = index
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let regex = "^[a-zA-Z0-9]*$"
        let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
        
        if(updatedText.count > 7){
            return false
        }
        
        let newStr = updatedText.replacingOccurrences(of: "-", with: "")
        if (!textTest.evaluate(with: newStr)) {
            return false
        }
        
         var text = textNumberPlaca
         if range.length == Int.zero {
             text.append(contentsOf: string)
         } else {
             text = String(text.dropLast())
         }
       
        textNumberPlaca = self.formatoPlaca(placa: text)
        textField.text = textNumberPlaca
        return false
    }
    
    func formatoPlaca(placa: String) -> String {
        var textPlaca = placa.replacingOccurrences(of: "-", with: "")
        if(textPlaca.count > 3){
            let prefixText = textPlaca.prefix(3).uppercased()
            let suffixText = textPlaca.suffix(textPlaca.count - 3).uppercased()
            return "\(prefixText)-\(suffixText)"
        }
        return textPlaca.uppercased()
    }
    
}


extension ConsultaVehicularViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.listaOficinaRegistralEntities.count
            
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.listaOficinaRegistralEntities[row].Nombre
            
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            if (!listaOficinaRegistralEntities.isEmpty) {
                self.changeTypeCiudad(index: row)
                //  self.selecCiudadTextField.text = listaOficinaRegistralEntities[row].Nombre
                //   self.selecCiudadTextField.resignFirstResponder()
            }
            
        default:
            return
        }
        
    }
}

