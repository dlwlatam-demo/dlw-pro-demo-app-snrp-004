//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class DetailConsultaTivetPresenter {
    
    private weak var controller: DetailConsultaTivetViewController?
    
    lazy private var model: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()

    init(controller: DetailConsultaTivetViewController) {
        self.controller = controller
    }
    
}

extension DetailConsultaTivetPresenter: GenericPresenter {
    
    func didLoad() {
       // self.controller?.loaderView(isVisible: true)
        let guid = UserPreferencesController.getGuid()
     
    }
    
    func listaTive() {
        self.controller?.loaderView(isVisible: true)
        self.model.getListaTive(){ (arrayBusqTive) in
            self.controller?.loadResultados(busquedaResponse: arrayBusqTive)
        }
    }
    
    func busquedaTive() {
            self.controller?.loaderView(isVisible: true)
        
            let codigoZona = self.controller?.codigoZona ?? ""
            let codigoOficina = self.controller?.codigoOficina ?? ""
            let anioTitulo = self.controller?.anioTitulo ?? ""
            let numeroTitulo = self.controller?.numeroTitulo ?? ""
            let numeroPlaca = self.controller?.numeroPlaca ?? ""
            let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
            let tipo = self.controller?.tipo ?? ""
        
        self.model.postListConsultaTive(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo) { objConsulta in
            
            let state = !objConsulta.documento.isEmpty
            
            /*let state = objConsulta.codResult == "1"
            if (state) {
                UserPreferencesController.clearGuid()
            }*/
           // self.controller?.loadDatosConsultaPropiedad(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
        }
    }
    
    func busquedaTiveHiperlink() {
            self.controller?.loaderView(isVisible: true)
        
            let codigoZona = self.controller?.codigoZona ?? ""
            let codigoOficina = self.controller?.codigoOficina ?? ""
            let anioTitulo = self.controller?.anioTitulo ?? ""
            let numeroTitulo = self.controller?.numeroTitulo ?? ""
            let numeroPlaca = self.controller?.numeroPlaca ?? ""
            let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
            let tipo = self.controller?.tipo ?? ""
        
        self.model.postListConsultaHiperLinkTive(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo) { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            let state = objConsulta.codResult == "1"
            if (state) {
                UserPreferencesController.clearGuid()
            }
           // self.controller?.loadDatosConsultaPropiedad(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
        }
    }
 
}
