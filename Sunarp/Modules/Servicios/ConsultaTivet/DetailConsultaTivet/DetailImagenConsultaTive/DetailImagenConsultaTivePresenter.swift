//
//  BusquedaPersonaJuridicaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/10/22.
//

import Foundation

class DetailImagenConsultaTivePresenter {
    
    private weak var controller: DetailImagenConsultaTiveViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var serviceModel: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelTive: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()
    init(controller: DetailImagenConsultaTiveViewController) {
        self.controller = controller
    }
    
}

extension DetailImagenConsultaTivePresenter: GenericPresenter {
    
  
    func busquedaTive() {
            // self.controller?.loaderView(isVisible: true)
        
            let codigoZona = self.controller?.codigoZona ?? ""
            let codigoOficina = self.controller?.codigoOficina ?? ""
            let anioTitulo = self.controller?.anioTitulo ?? ""
           let numeroTitulo = self.controller?.numeroTitulo.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            let numeroPlaca = self.controller?.numeroPlaca.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
            let tipo = self.controller?.tipo ?? ""
        
        self.modelTive.postListConsultaTive(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo) { objConsulta in
            
            // self.controller?.loaderView(isVisible: false)
            let state = (objConsulta.documento == "")
            print("obj2:>>>",objConsulta.codResult)
            dump(objConsulta)
            if (state) {
                print("sin datos 1")
                self.controller?.goBack()
            }
            else {
                print("con datos 1")
                self.controller?.loadDatosConsultaTivet(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
            }
        
        }
    }
    
            
}
