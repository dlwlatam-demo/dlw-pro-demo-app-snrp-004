//
//  BusquedaPersonaJuridicaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation
import UIKit
import WebKit

class DetailImagenConsultaTiveViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    
    @IBOutlet weak var yourWebView: WKWebView!
    
    
    var loading: UIAlertController!
    
    var isChecked: Bool = false
    var oficinas: [OficinaRegistralEntity] = []
    var resultado: [PersonaJuridicaEntity] = []
    var style: Style = Style.myApp
    
    private lazy var presenter: DetailImagenConsultaTivePresenter = {
       return DetailImagenConsultaTivePresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    var anioTitulo: String = ""
    var numeroTitulo: String = ""
    var numeroPlaca: String = ""
    var codigoVerificacion: String = ""
    var tipo: String = ""
    
    
    
    var document: String = ""
    
    
    
    @IBOutlet weak var imageDocument: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        //self.presenter.willAppear()
        
        print("tipopp:>>",tipo)
        
        self.presenter.busquedaTive()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Documento TIVe")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaTivetViewController") as! DetailConsultaTivetViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    //   self.presenter.willAppear()
    }
    
    
    private func setupDesigner() {
       // self.formView.backgroundCard()
        
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    private func setupGestures() {
  
     
        
    }
    
  
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {

            self.stopLoader(loader: self.loading)
        }
    }
    

    func loadDatosConsultaTivet(_ state: Bool, message: String, jsonValidacion: TiveBusqEntity) {
        
        document = jsonValidacion.documento
        
        
        /*
            if (document.isEmpty){
            }else{
                self.presenter.busquedaTiveHiperlink()
            }
        */
       
       
        /*
        if (state) {
           // self.isValidDocument = true
            //self.namesTextField.text = jsonValidacion.nombres
            print(jsonValidacion)
        } else {
            //self.isValidDocument = false
            showMessageAlert(message: message)
        }*/
        //let str = usuarioLogin.userPhoto
       // self.imageDocument.image = convertBase64StringToImage(imageBase64String: document)
       /* self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            *self.loaderView(isVisible: false)
        }*/
        
        //convertBase64StringToImage(imageBase64String: document)
        print("archivo: ", document.count)
        if document.count > 10 {
            if let decodeData = Data(base64Encoded: document, options: .ignoreUnknownCharacters) {
                yourWebView.load(decodeData, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
            }
        }
        else {
            yourWebView = nil
            print("sin datos 2")
            // showMessageAlert(message: "No se cuenta con documento para visualizar")
            self.goBack()
        }
        
        //saveBase64StringToPDF(document)
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
      /*  let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image*/
        if let imageData = Data(base64Encoded: imageBase64String) {
                        let image = UIImage(data: imageData)
                        return image
                    }
        return UIImage()
    }
    
    func saveBase64StringToPDF(_ base64String: String) {
        guard
            var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
            let convertedData = Data(base64Encoded: base64String)
        else {
            return
        }
        
        documentsURL.appendPathComponent("Constancia de transferencia.pdf")
        
        do {
            try convertedData.write(to: documentsURL)
        } catch {
            //handle write error here
        }
        
        downloadFileDocumentoContable(urlDocument: documentsURL, nameDocument: "Constancia de transferencia.pdf")
    }
    
    func downloadFileDocumentoContable(urlDocument:URL,nameDocument:String){
        var documentInteractionController: UIDocumentInteractionController!
        documentInteractionController = UIDocumentInteractionController.init(url: urlDocument)
        documentInteractionController?.delegate = self
        documentInteractionController?.presentPreview(animated: true)
    }
   
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    

    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
        self.loaderView(isVisible: false)
    }
    

    
   
}

extension DetailImagenConsultaTiveViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}
/*
extension ConsultaTivetViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.oficinas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.oficinas[row].Nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (!oficinas.isEmpty) {
            self.oficinaRegistralText.text = oficinas[row].Nombre
            self.oficinaRegistralText.resignFirstResponder()
        }
        
    }
}
*/
