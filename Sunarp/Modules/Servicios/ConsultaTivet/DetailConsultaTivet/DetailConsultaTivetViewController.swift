//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import CoreLocation
import SearchTextField
import HSSearchable

class DetailConsultaTivetViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
        
 
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    @IBOutlet weak var formTableView: UIView!

  
    //@IBOutlet weak var busquedaTxtFiel: SearchTextField!
    @IBOutlet weak var busquedaTxtFiel: UITextField!
    
    let locationManager = CLLocationManager()
    var latValue = CLLocationDegrees()
    var longValue = CLLocationDegrees()
    var markerArray = Array<Any>()
    var i : Int = 0
    var origin = String()
    var destination = String()
    var previousLat = CLLocationDegrees()
    var previousLong = CLLocationDegrees()
    var listaEntity: [TiveEntity] = []
    
    var loading: UIAlertController!
    
    private lazy var presenter: DetailConsultaTivetPresenter = {
       return DetailConsultaTivetPresenter(controller: self)
    }()
    
    
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var contrasena: String = ""
    var contrasenaRepeat: String = ""
    
    
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    var anioTitulo: String = ""
    var numeroTitulo: String = ""
    var numeroPlaca: String = ""
    var codigoVerificacion: String = ""
    var tipo: String = ""
    
    
    
    @IBOutlet var tableSearchView:UITableView!
    
    //let array = ["Oficina Registral","Oficina Receptora","Oficina Central","Tribunal Registral","Zona Registral","Oficina Otras"]
    
    //MARK: iVars

    //MARK: IBOutlet
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        
        
       // formTableView.isHidden = true
        
        
        //mapiperu2019
        registerCell()
        
       
        // 1 - Configure a simple search text field
       
        
        //set anyone delegate of two based on your requiredments

        //self.usersData.customDelegate = self;
      
    
       // self.loadDummyData()
      
        self.presenter.listaTive()
        
        
       // loaderView(isVisible: false)
        
        
        self.tableSearchView.reloadData()
        
    }
    
  
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Últimas búsquedas TIVe")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConsultaTivetViewController") as! ConsultaTivetViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - search
    // 1 - Configure a simple search text view
   /* fileprivate func configureSimpleSearchTextField() {
        // Start visible even without user's interaction as soon as created - Default: false
        busquedaTxtFiel.startVisibleWithoutInteraction = false
        
        // Set data source
        let countries = localPlace()
        busquedaTxtFiel.filterStrings(countries)
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
       // self.loaderView(isVisible: false)
    }
    // Hide keyboard when touching the screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
   
    private func setupDesigner() {
      //  self.formView.backgroundCard()
      //  self.formViewEditar.backgroundCard()
        tableSearchView.delegate = self
        tableSearchView.dataSource = self
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: DetailConsultaTivetViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: DetailConsultaTivetViewCell.reuseIdentifier)
    }
    private func addGestureView(){
      
    }

    @objc private func onTapToBusqView() {
        
        
    }

    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func loadResultados(busquedaResponse: [TiveEntity]) {
        self.listaEntity = busquedaResponse
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        self.loaderView(isVisible: false)
    }
    
    

}



// MARK: - Table view datasource
extension DetailConsultaTivetViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        print("couunt:>>",self.listaEntity.count)
            return self.listaEntity.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 265
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        
        guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: DetailConsultaTivetViewCell.reuseIdentifier, for: indexPath) as? DetailConsultaTivetViewCell else {
            return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            //cell.item = self.listaPropiedadEntities[indexPath.row]
        
            let listaPro = listaEntity[indexPath.row]
            print("listaPro:>>",listaPro)
            cell.setup(with: listaPro)
            cell.onTapVerSolicitud = {[weak self] in
            
                let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailImagenConsultaTiveViewController") as! DetailImagenConsultaTiveViewController
                  vc.tipo = listaPro.tipo
                  vc.anioTitulo = listaPro.anioTitulo
                  vc.numeroTitulo = listaPro.numeroTitulo
                  vc.numeroPlaca = listaPro.numeroPlaca
                  vc.codigoVerificacion = listaPro.codigoVerificacion
                  vc.codigoZona = listaPro.codigoZona
                  vc.codigoOficina = listaPro.codigoOficina
                self?.navigationController?.pushViewController(vc, animated: true)
            
            
             }
            return cell
    }
}

// MARK: - Table view delegate
extension DetailConsultaTivetViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}

extension DetailConsultaTivetViewController: MarketWindowDelegate{
    func showRouteMap(item: MapaOfficeEntity) {
      
    }
    
    func showShareSheet(item: MapaOfficeEntity) {
    }
    
    func primary(action:[MapaOfficeEntity],index:Int) {
        //URL
    
    }
    
    func secondary() {
    }
}
