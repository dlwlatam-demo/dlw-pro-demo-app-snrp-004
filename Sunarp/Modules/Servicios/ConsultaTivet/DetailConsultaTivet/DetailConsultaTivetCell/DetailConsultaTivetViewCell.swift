//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class DetailConsultaTivetViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "DetailConsultaTivetViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    @IBOutlet var oficinaLabel:UILabel!
    @IBOutlet var anioTituloLabel:UILabel!
    @IBOutlet var numTituloLabel:UILabel!
    @IBOutlet var placaVehLabel:UILabel!
    @IBOutlet var codigoVeriLabel:UILabel!
    
    
    @IBOutlet weak var searchButtonView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: TiveEntity) {
        
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
      print("ofiiicina:>>",solicitud.oficina)
        
        oficinaLabel?.text = solicitud.oficina
        anioTituloLabel?.text = solicitud.anioTitulo
        numTituloLabel?.text = solicitud.numeroTitulo
        placaVehLabel?.text = solicitud.numeroPlaca
        codigoVeriLabel?.text = solicitud.codigoVerificacion
       
     
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.searchButtonView.addGestureRecognizer(onTapVerSolicitud)
        
    }
    
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        self.searchButtonView.primaryButton()
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
