//
//  BusquedaPersonaJuridicaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation
import UIKit

class ConsultaTivetViewController: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var searchButtonView: UIView!
    @IBOutlet weak var searchLastButtonView: UIView!
    @IBOutlet weak var oficinaRegistralText: UITextField!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    
    @IBOutlet weak var anioTituloText: SDCTextField!
    @IBOutlet weak var numTituloText: SDCTextField!
    @IBOutlet weak var numPlacaText: SDCTextField!
    @IBOutlet weak var codigoText: SDCTextField!
    
    var oficinaRegistralPickerView = UIPickerView()
    var loading: UIAlertController!
    
    var isChecked: Bool = false
    var oficinas: [OficinaRegistralEntity] = []
    var resultado: [PersonaJuridicaEntity] = []
    var style: Style = Style.myApp
    
    private lazy var presenter: ConsultaTivetPresenter = {
       return ConsultaTivetPresenter(controller: self)
    }()
    
    var indexSelect = 0
    var valDone: Bool = true
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    var anioTitulo: String = ""
    var numeroTitulo: String = ""
    var numeroPlaca: String = ""
    var codigoVerificacion: String = ""
    var tipo: String = ""
    var textNumberPlaca:String = ""
    var document: String = ""
    
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        self.presenter.willAppear()
        
        numPlacaText.delegate = self
        numPlacaText.maxLengths = 7
        numPlacaText.valueType = .alphaNumeric
        
        anioTituloText.delegate = self
        anioTituloText.maxLengths = 4
        anioTituloText.valueType = .onlyNumbers
        
        numTituloText.delegate = self
        numTituloText.maxLengths = 8
        numTituloText.valueType = .onlyNumbers
        
        codigoText.delegate = self
        codigoText.maxLengths = 12
        codigoText.valueType = .onlyNumbers
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Consulta TIVe")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiciosViewController") as! ServiciosViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
        // self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    //   self.presenter.willAppear()
    }
    
    private func setupDesigner() {
        self.numPlacaText.delegate = self
        self.formView.backgroundCard()
        self.oficinaRegistralText.borderAndPaddingLeftAndRight()
        
        self.anioTituloText.borderAndPaddingLeftAndRight()
        self.numTituloText.borderAndPaddingLeftAndRight()
        self.numPlacaText.borderAndPaddingLeftAndRight()
        self.codigoText.borderAndPaddingLeftAndRight()
        
        self.searchButtonView.primaryButton()
        
        self.searchLastButtonView.secondaryButton()
        
        self.isChecked = false
        
        self.oficinaRegistralPickerView.delegate = self
        self.oficinaRegistralPickerView.dataSource = self
        self.oficinaRegistralText.inputView = self.oficinaRegistralPickerView
        self.oficinaRegistralText.inputAccessoryView = self.createToolbar()
        
        self.anioTituloText.placeholder = "Año del Título"
        self.numTituloText.placeholder = "Nº de Título"
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func donePressed() {
        if valDone == true {
            self.oficinaRegistralText.text = oficinas[indexSelect].Nombre
            self.oficinaRegistralText.resignFirstResponder()
            self.codigoZona = self.oficinas[indexSelect].RegPubId
            self.codigoOficina = self.oficinas[indexSelect].OficRegId
        }
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
  
        
        let tapSearchGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchView))
        self.searchButtonView.addGestureRecognizer(tapSearchGesture)
        
        let tapSearchGestureLast = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchLastView))
        self.searchLastButtonView.addGestureRecognizer(tapSearchGestureLast)
      
        
        
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        self.tipo = "T"
        
    }
    
    @objc private func onTapToConfirmViewAnio() {
      //  self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        
        
        self.anioTituloText.placeholder = "Año del Título"
        self.numTituloText.placeholder = "Nº de Título"
        self.tipo = "T"
        self.anioTituloText.text = ""
        self.numTituloText.text = ""
        self.numPlacaText.text = ""
        self.codigoText.text = ""
    }
  
    @objc private func onTapToConfirmViewNum() {
     //   self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        
        self.anioTituloText.placeholder = "Año de Publicidad"
        self.numTituloText.placeholder = "Nº de Publicidad"
        self.tipo = "P"
        self.anioTituloText.text = ""
        self.numTituloText.text = ""
        self.numPlacaText.text = ""
        self.codigoText.text = ""
    }
  
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {

            self.stopLoader(loader: self.loading)
        }
    }
    
    
    
    @objc private func onTapSearchLastView(){
      
        let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaTivetViewController") as! DetailConsultaTivetViewController
        //vc.listaPropiedadEntities = listaPropiedadEntities
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc private func onTapSearchView(){
        let oficinaSelect = self.oficinaRegistralText.text ?? ""
       
        self.anioTitulo = self.anioTituloText.text ?? ""
        self.numeroTitulo = self.numTituloText.text ?? ""
        self.numeroPlaca = self.numPlacaText.text ?? ""
        self.codigoVerificacion = self.codigoText.text ?? ""
       
        
        self.presenter.validateDataDoc()
    }
    
    func goToConfirmDoc(_ state: Bool, message: String) {
      //  loaderView(isVisible: false)
          
        if (state) {
            if (state) {
                  self.anioTitulo = self.anioTituloText.text ?? ""
                    if (!anioTitulo.isEmpty) {
                        self.presenter.busquedaTive()
                    }
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
    func loadDatosConsultaTivet(_ state: Bool, message: String, jsonValidacion: TiveBusqEntity) {
        
        document = jsonValidacion.documento
        
        if(jsonValidacion.documento != "") {
            self.presenter.busquedaTiveHiperlink()
        }
        /*
            if (document.isEmpty){
            }else{
                self.presenter.busquedaTiveHiperlink()
            }
        */
       
        loaderView(isVisible: false)
        
        /*
        if (state) {
           // self.isValidDocument = true
            //self.namesTextField.text = jsonValidacion.nombres
            print(jsonValidacion)
        } else {
            //self.isValidDocument = false
            showMessageAlert(message: message)
        }*/
    }
    func loadDatosConsultaHiperlinkTivet(_ state: Bool, message: String, jsonValidacion: TiveBusqEntity) {
        
     
        if (state) {
            loaderView(isVisible: false)
            
            let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaTivetViewController") as! DetailConsultaTivetViewController
            //vc.listaPropiedadEntities = listaPropiedadEntities
            self.navigationController?.pushViewController(vc, animated: true)
            
            self.loaderView(isVisible: false)
            print(jsonValidacion)
           // self.isValidDocument = true
            //self.namesTextField.text = jsonValidacion.nombres
        } else {
            //self.isValidDocument = false
            let code = jsonValidacion.code
            print("code::>>",code)
            if (code == "005"){
                loaderView(isVisible: false)
                let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaTivetViewController") as! DetailConsultaTivetViewController
                //vc.listaPropiedadEntities = listaPropiedadEntities
                self.navigationController?.pushViewController(vc, animated: true)
                self.loaderView(isVisible: false)
            }else{
                showMessageAlert(message: message)
            }
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.oficinaRegistralText.resignFirstResponder()
       
        
        
        self.anioTituloText.resignFirstResponder()
        self.numTituloText.resignFirstResponder()
        self.numPlacaText.resignFirstResponder()
        self.codigoText.resignFirstResponder()
        
        
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadResultados(busquedaResponse: [PersonaJuridicaEntity]) {
        self.resultado = busquedaResponse
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            if (self.resultado.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "BusquedaPersonaJuridica", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ResultadoBusquedaViewController") as! ResultadoBusquedaViewController
                vc.setResultado(resultado: self.resultado)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func changeTypeDocument(index: Int) {
        
        valDone = true
        self.indexSelect = index
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.oficinas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.oficinas[row].Nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (!oficinas.isEmpty) {
            self.changeTypeDocument(index: row)
         //   self.oficinaRegistralText.text = oficinas[row].Nombre
         //   self.oficinaRegistralText.resignFirstResponder()
        }
        
    }
    

 
}
/*
extension ConsultaTivetViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.oficinas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.oficinas[row].Nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (!oficinas.isEmpty) {
            self.oficinaRegistralText.text = oficinas[row].Nombre
            self.oficinaRegistralText.resignFirstResponder()
        }
        
    }
}
*/

extension ConsultaTivetViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
    
}
