//
//  BusquedaPersonaJuridicaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/10/22.
//

import Foundation

class ConsultaTivetPresenter {
    
    private weak var controller: ConsultaTivetViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var serviceModel: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelTive: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()
    init(controller: ConsultaTivetViewController) {
        self.controller = controller
    }
    
}

extension ConsultaTivetPresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getListaOficinaRegistral{ (arrayOficinas) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    func buscarOficinas(oficina: String, razon: String, siglas: String) {
        self.controller?.loaderView(isVisible: true)
        self.serviceModel.getBusquedaJuridica(nombre: razon.uppercased(), nombreOficinaRegistral: oficina, siglas: siglas){ (arrayOficinas) in
            self.controller?.loadResultados(busquedaResponse: arrayOficinas)
        }
    }
    /*
    func listaTive() {
        self.controller?.loaderView(isVisible: true)
        self.modelTive.getListaTive(){ (arrayOficinas) in
            self.controller?.loadResultados(busquedaResponse: arrayOficinas)
        }
    }
    */
    func busquedaTive() {
            self.controller?.loaderView(isVisible: true)
        
            let codigoZona = self.controller?.codigoZona ?? ""
            let codigoOficina = self.controller?.codigoOficina ?? ""
            let anioTitulo = self.controller?.anioTitulo ?? ""
            let numeroTitulo = self.controller?.numeroTitulo ?? ""
            let numeroPlaca = self.controller?.numeroPlaca ?? ""
            let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
            let tipo = self.controller?.tipo ?? ""
        
        self.modelTive.postListConsultaTive(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo) { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            print("obj1:>>>",objConsulta.msgResult)
            let state = (objConsulta.documento == "")
            if state {
                print("sin datos")
                UserPreferencesController.clearGuid()
                var mensaje = objConsulta.description
                if mensaje == "" {
                    mensaje = "No se encontraron registros con los datos ingresados."
                }
                self.controller?.showMessageAlert(message: mensaje)
            }
            self.controller?.loadDatosConsultaTivet(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
        }
    }
    
    func busquedaTiveHiperlink() {
            self.controller?.loaderView(isVisible: true)
        
            let codigoZona = self.controller?.codigoZona ?? ""
            let codigoOficina = self.controller?.codigoOficina ?? ""
            let anioTitulo = self.controller?.anioTitulo ?? ""
            let numeroTitulo = self.controller?.numeroTitulo ?? ""
            let numeroPlaca = self.controller?.numeroPlaca ?? ""
            let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
            let tipo = self.controller?.tipo ?? ""
        
        self.modelTive.postListConsultaHiperLinkTive(codigoZona: codigoZona, codigoOficina: codigoOficina, anioTitulo: anioTitulo, numeroTitulo: numeroTitulo, numeroPlaca: numeroPlaca, codigoVerificacion: codigoVerificacion, tipo: tipo) { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            let state = objConsulta.codResult == ""
            if (state) {
                UserPreferencesController.clearGuid()
                self.controller?.showMessageAlert(message: objConsulta.description)
            }
            self.controller?.loadDatosConsultaHiperlinkTivet(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
        }
    }
    
    func validateDataDoc() {
       
        let codigoZona = self.controller?.codigoZona ?? ""
        let codigoOficina = self.controller?.codigoOficina ?? ""
        let anioTitulo = self.controller?.anioTitulo ?? ""
        let numeroTitulo = self.controller?.numeroTitulo ?? ""
        let numeroPlaca = self.controller?.numeroPlaca.replacingOccurrences(of: "-", with: "") ?? .empty
        let codigoVerificacion = self.controller?.codigoVerificacion ?? ""
        let tipo = self.controller?.tipo ?? ""
       print("numDocument::>>",anioTitulo)
      
        if (numeroPlaca.isEmpty || numeroTitulo.isEmpty || anioTitulo.isEmpty || codigoVerificacion.isEmpty || codigoZona.isEmpty || codigoOficina.isEmpty || tipo.isEmpty) {
            self.controller?.goToConfirmDoc(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmDoc(true, message: "")
        }
        
    }
            
}
