//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class ConsultaPropiedadViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var numberDocumentTextField: SDCTextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    
    var listaPropiedadEntities: [ConsultaPropiedadListEntity] = []
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var generoPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    @IBOutlet weak var formBackView: UIView!
    @IBOutlet weak var lblTxtInforma: UILabel!
    @IBOutlet weak var firstApeTextField: UITextField!
    @IBOutlet weak var secondApeTextField: UITextField!
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var toConfirmBusquedaView: UIView!
    
    @IBOutlet weak var uiScrollView: UIScrollView!
    var indexTypeDocument = 0
    var dateFormatForWS:String = ""
    
    private lazy var presenter: ConsultaPropiedadPresenter = {
       return ConsultaPropiedadPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       // self.presenter.didLoad()
      //  presenter.didLoad()
        setupNavigationBar()
        formBackView.isHidden = true
        lblTxtInforma.isHidden = false
        
        typeDocumentTextField.text = self.tipoDocumentos[0]
        self.typeDocument = self.tipoDocumentos[0]
        self.maxLength = 8
        changeTypeDocument(index: 0)
        self.numberDocumentTextField.keyboardType = .numberPad
        self.dateOfIssueTextField.placeholder = "Fecha Emisión"
        numberDocumentTextField.delegate = self
        self.numberDocumentTextField.valueType = .onlyNumbers
        self.numberDocumentTextField.maxLengths = 8
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Consulta de Propiedad")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.reiniciaFormulario()
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        let tapConfirmBusGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmBusqView))
        self.toConfirmBusquedaView.addGestureRecognizer(tapConfirmBusGesture)
        
        
    }
    
    private func setupDesigner() {
       
        formView.backgroundCard()
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        numberDocumentTextField.border()
        numberDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        dateOfIssueTextField.border()
        dateOfIssueTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
       
     
        toConfirmView.primaryButton()
        toConfirmBusquedaView.primaryButton()
        
        self.numberDocumentTextField.delegate = self
        self.numberDocumentTextField.inputAccessoryView = createToolbarGender()
        self.tipoDocumentoPickerView.tag = 1
        
        
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        self.typeDocumentTextField.inputAccessoryView = self.createToolbarTypeDocument()
        
        self.generoPickerView.tag = 2
        
        self.generoPickerView.delegate = self
        self.generoPickerView.dataSource = self
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numberDocumentTextField.resignFirstResponder()
    }
    
    
    
    @objc private func onTapToConfirmView() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.dateFormatForWS
        self.presenter.validateData()
    }
    
    //second Butoon
    @objc private func onTapToConfirmBusqView() {
        self.names = self.NameTextField.text ?? ""
        self.lastName = self.firstApeTextField.text ?? ""
        self.middleName = self.secondApeTextField.text ?? ""
        self.presenter.validateDataSearch()
    }
    
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        
        if (state) {
            if (state) {
                validateTypeDocument()
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func goToConfirmSearch(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                self.presenter.searchPropiedad()
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func loadDatosConsultaPropiedad(_ state: Bool, message: String, jsonValidacion: ConsultaPropiedadEntity) {
        
        
        if jsonValidacion.estado == "0000"  {
            self.listaPropiedadEntities = jsonValidacion.dataConsul
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
                vc.listaPropiedadEntities = self.listaPropiedadEntities
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            dump(jsonValidacion)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            self.reiniciaFormulario()
        }
    }
    
    func reiniciaFormulario() {
        self.isValidDocument = false
        
        self.formBackView.isHidden = true
        self.lblTxtInforma.isHidden = false
        self.typeDocumentTextField.isEnabled = true
        self.dateOfIssueTextField.isEnabled = true
        self.numberDocumentTextField.isEnabled = true
    }
    
    func changeTypeDocument(index: Int) {
        self.typeDocument = self.tipoDocumentos[index]
        indexTypeDocument = index
        self.isValidDocument = false
        if (self.typeDocument == "DNI") {
            self.maxLength = 8
            self.numberDocumentTextField.maxLengths = 8
            self.numberDocumentTextField.valueType = .onlyNumbers
            self.numberDocumentTextField.keyboardType = .numberPad
            self.dateOfIssueTextField.placeholder = "Fecha Emisión"
        } else if (self.typeDocument == "CE") {
            self.maxLength = 9
            self.numberDocumentTextField.valueType = .alphaNumeric
            self.numberDocumentTextField.maxLengths = 9
            self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numberDocumentTextField.keyboardType = .alphabet
        } else {
            self.maxLength = 20
            self.numberDocumentTextField.maxLengths = 20
            self.numberDocumentTextField.valueType = .alphaNumeric
            self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numberDocumentTextField.keyboardType = .alphabet
     
            self.isValidDocument = true
        }
        self.numberDocumentTextField.text = ""
        self.dateOfIssueTextField.text = ""

        createDatePicker()
    }
    
    func changeTypeGender(index: Int) {
        if (index == 0) {
            self.gender = "M"
        } else if (index == 1) {
            self.gender = "F"
        } else {
            self.gender = "-"
        }
    }
    
    func validateTypeDocument() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.dateFormatForWS
        print(typeDocument)
        if (self.typeDocument == "DNI" || self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty && !dateOfIssue.isEmpty) {
                self.typeDocument = Constant.TYPE_DOCUMENT_CODE_DNI
                self.presenter.validarDni()
            }
        } else if (self.typeDocument == "CE" || self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.typeDocument = Constant.TYPE_DOCUMENT_CODE_CE
                self.presenter.validarCe()
            }
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            formBackView.isHidden = false
            lblTxtInforma.isHidden = true
            
            self.NameTextField.text = jsonValidacionDni.nombres
            self.firstApeTextField.text = jsonValidacionDni.apellidoPaterno
            self.secondApeTextField.text = jsonValidacionDni.apellidoMaterno
            
            
            self.typeDocumentTextField.isEnabled = false
            self.dateOfIssueTextField.isEnabled = false
            self.numberDocumentTextField.isEnabled = false
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            formBackView.isHidden = false
            lblTxtInforma.isHidden = true
            
            self.NameTextField.text = jsonValidacionCe.strNombres
            self.firstApeTextField.text = jsonValidacionCe.strPrimerApellido
            self.secondApeTextField.text = jsonValidacionCe.strSegundoApellido
            
            self.numberDocumentTextField.resignFirstResponder()
            
            self.typeDocumentTextField.isEnabled = false
            self.dateOfIssueTextField.isEnabled = false
            self.numberDocumentTextField.isEnabled = false

        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarTypeDocument() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: self, action: #selector(donePressedType))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                          target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
    }
    
    @objc func donePressedType() {
        self.changeTypeDocument(index: indexTypeDocument)
        self.typeDocumentTextField.text = tipoDocumentos[indexTypeDocument]
        self.typeDocumentTextField.resignFirstResponder()
    }
    
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            comps.year = -10
            let minDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = .now
            datePicker.minimumDate = minDate
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            comps.year = -100
            let minDate = calender.date(byAdding: comps, to: .now)
            comps.year = -18
            let maxDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
        }
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let fechaPrevia = dateOfIssueTextField.text ?? ""
        dateOfIssueTextField.text = dateFormatter.string(from: datePicker.date)
        if fechaPrevia == dateOfIssueTextField.text {
            self.view.endEditing(true)
            validateTypeDocument()
        }
        else {
            self.numberDocumentTextField.text = ""
        }
        
        dateFormatWS(dateFormatter: dateFormatter)
    }
    
    func dateFormatWS(dateFormatter: DateFormatter) {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatForWS = dateFormatter.string(from: datePicker.date)
    }
        
}

extension ConsultaPropiedadViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentos.count
       // case 2:
       //     return self.generos.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentos[row]
       // case 2:
       //     return self.generos[row]
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeDocument(index: row)
            self.typeDocumentTextField.text = tipoDocumentos[row]
            self.typeDocumentTextField.resignFirstResponder()
        //case 2:
          //  self.changeTypeGender(index: row)
        default:
            return
        }
        
    }
}

extension ConsultaPropiedadViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    
    /*
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == numberDocumentTextField) {
            if (count == maxLength) {
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                    self.validateTypeDocument()
                    return false
                }
            } else {
                self.isValidDocument = false
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }*/
}

/*
private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}
*/
