//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import SearchTextField
import HSSearchable

class DetailConsultaPropiedadViewController: UIViewController {
        
 
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    @IBOutlet weak var formTableView: UIView!

  
    //@IBOutlet weak var busquedaTxtFiel: SearchTextField!
    @IBOutlet weak var busquedaTxtFiel: UITextField!
    
    var markerArray = Array<Any>()
    var i : Int = 0
    var origin = String()
    var destination = String()
    var listaPropiedadEntities: [ConsultaPropiedadListEntity] = []
    
    var loading: UIAlertController!
    
    private lazy var presenter: DetailConsultaPropiedadPresenter = {
       return DetailConsultaPropiedadPresenter(controller: self)
    }()
    
    
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var contrasena: String = ""
    var contrasenaRepeat: String = ""
    
    @IBOutlet var tableSearchView:UITableView!
    
    //let array = ["Oficina Registral","Oficina Receptora","Oficina Central","Tribunal Registral","Zona Registral","Oficina Otras"]
    
    //MARK: iVars
    var usersData = SearchableWrapper()
    var users: [UserDM] {
        return self.usersData.dataArray as! [UserDM]
    }
    
    //MARK: IBOutlet
    
    var array : Array<UserDM> = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
       // formTableView.isHidden = true
        
        
        //mapiperu2019
        registerCell()
        
       
        // 1 - Configure a simple search text field
       
        print("..AArray.>>",array)
        self.usersData.serverArray = array;
        self.tableSearchView.reloadData()
        
        
    }
    
  
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Consulta de Propiedad")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    // Hide keyboard when touching the screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
   
    private func setupDesigner() {
        tableSearchView.delegate = self
        tableSearchView.dataSource = self
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: DetailConsultaPropiedadViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: DetailConsultaPropiedadViewCell.reuseIdentifier)
    }
    private func addGestureView(){
      
    }

    @objc private func onTapToBusqView() {
        
        
    }

    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func loadDatosConsultaPropiedad(_ state: Bool, message: String, jsonValidacion: ConsultaPropiedadEntity) {
        
        
        listaPropiedadEntities = jsonValidacion.dataConsul
        self.tableSearchView.reloadData()
        
        loaderView(isVisible: false)
        if (state) {
            print(jsonValidacion)
        } 
    }
    


}



// MARK: - Table view datasource
extension DetailConsultaPropiedadViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
            return self.listaPropiedadEntities.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 265
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        
        guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: DetailConsultaPropiedadViewCell.reuseIdentifier, for: indexPath) as? DetailConsultaPropiedadViewCell else {
            return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            //cell.item = self.listaPropiedadEntities[indexPath.row]
        
            let listaPro = listaPropiedadEntities[indexPath.row]
            cell.setup(with: listaPro)
        
            return cell
    }
}

// MARK: - Table view delegate
extension DetailConsultaPropiedadViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}

extension DetailConsultaPropiedadViewController: MarketWindowDelegate {
    func showRouteMap(item: MapaOfficeEntity) {
        
    }
    
    func showShareSheet(item: MapaOfficeEntity) {
  
    }
    
    func primary(action: [MapaOfficeEntity],index:Int) {
        
    }
    
    
    func secondary() {
    }
}
