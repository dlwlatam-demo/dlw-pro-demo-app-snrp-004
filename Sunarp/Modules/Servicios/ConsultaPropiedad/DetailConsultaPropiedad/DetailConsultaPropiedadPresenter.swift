//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class DetailConsultaPropiedadPresenter {
    
    private weak var controller: DetailConsultaPropiedadViewController?
    
    lazy private var model: ConsultaPropiedadModel = {
        let navigation = controller?.navigationController
       return ConsultaPropiedadModel(navigationController: navigation!)
    }()

    init(controller: DetailConsultaPropiedadViewController) {
        self.controller = controller
    }
    
}

extension DetailConsultaPropiedadPresenter: GenericPresenter {
    
    func didLoad() {
       // self.controller?.loaderView(isVisible: true)
        let guid = UserPreferencesController.getGuid()
     
    }
    
    func registrarUsuario() {
        
        guard let contrasena = self.controller?.contrasena else { return }
        guard let contrasenaRepeat = self.controller?.contrasenaRepeat else { return }
        
       
            self.controller?.loaderView(isVisible: true)
            let email = self.controller?.email ?? ""
            let sexo = self.controller?.gender ?? ""
            let tipoDoc = self.controller?.typeDocument ?? ""
            let nroDoc = self.controller?.numDocument ?? ""
            let nombres = self.controller?.names ?? ""
            let priApe = self.controller?.lastName ?? ""
            let segApe = self.controller?.middleName ?? ""
            let fecNac = self.controller?.dateOfIssue ?? ""
            let nroCelular = self.controller?.numcelular ?? ""
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
       
        
        self.model.postListarConsultaPropiedad(tipo: "N", apePaterno: priApe, apeMaterno: segApe, nombres: nombres, razonSocial: "", userApp: "APPSNRPIOS", typeDoc: tipoDoc, numbDoc: nroDoc, devideId: "1", userCrea: "55555") { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            let state = objConsulta.codResult == "1"
            if (state) {
                UserPreferencesController.clearGuid()
            }
         
            self.controller?.loadDatosConsultaPropiedad(state, message: objConsulta.msgResult, jsonValidacion: objConsulta)
       
        }
        
          
        
    }
    
 
}
