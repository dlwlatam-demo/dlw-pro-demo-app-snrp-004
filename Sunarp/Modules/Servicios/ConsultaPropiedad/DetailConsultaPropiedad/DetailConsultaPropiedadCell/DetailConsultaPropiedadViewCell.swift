//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class DetailConsultaPropiedadViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "DetailConsultaPropiedadViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    @IBOutlet var nroDocumentoLabel:UILabel!
    @IBOutlet var oficinaLabel:UILabel!
    @IBOutlet var partidaLabel:UILabel!
    @IBOutlet var refNumPartLabel:UILabel!
    @IBOutlet var titularLabel:UILabel!
    @IBOutlet var zonaLabel:UILabel!
    
    
    
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: ConsultaPropiedadListEntity) {
        
        print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
        
        nroDocumentoLabel?.text = solicitud.nroDocumento
        oficinaLabel?.text = solicitud.oficina
        partidaLabel?.text = solicitud.partida
        titularLabel?.text = solicitud.titular
        zonaLabel?.text = solicitud.zona
        
     
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
