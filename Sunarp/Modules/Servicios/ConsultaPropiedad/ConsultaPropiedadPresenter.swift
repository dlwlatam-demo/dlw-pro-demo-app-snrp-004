//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class ConsultaPropiedadPresenter {
    
    private weak var controller: ConsultaPropiedadViewController?
    
    lazy private var model: ConsultaPropiedadModel = {
        let navigation = controller?.navigationController
        return ConsultaPropiedadModel(navigationController: navigation!)
    }()
    
    lazy private var modelProfil: ProfileModel = {
        let navigation = controller?.navigationController
        return ProfileModel(navigationController: navigation!)
    }()
    init(controller: ConsultaPropiedadViewController) {
        self.controller = controller
    }
    
}

extension ConsultaPropiedadPresenter: GenericPresenter {
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
        //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
        //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        // }
        
        self.modelProfil.getListaTipoDocumentos{ (arrayTipoDocumentos) in
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.controller?.loaderView(isVisible: false)
            self.modelProfil.postLogin(appId: Constant.APP_ID, deviceToken: "", deviceType: "", status: "A", deviceId: "1", appVersion: appVersion, ipAddress: UtilHelper.getIPAddress(), deviceLogged: "1", receiveNotifications: "N", deviceOs: Constant.DEVICE_OS) { (profileEntity) in
                //  self.controller?.loadDataPerfil(profileEntity: profileEntity, arrayTipoDocumentos: arrayTipoDocumentos)
            }
        }
    }
    
    func searchPropiedad() {
        self.controller?.loaderView(isVisible: true)
        let email = self.controller?.email ?? ""
        let sexo = self.controller?.gender ?? ""
        let tipoDoc = self.controller?.typeDocument ?? ""
        let nroDoc = self.controller?.numDocument ?? ""
        let nombres = self.controller?.names ?? ""
        let priApe = self.controller?.lastName ?? ""
        let segApe = self.controller?.middleName ?? ""
        let fecNac = self.controller?.dateOfIssue ?? ""
        let nroCelular = self.controller?.numcelular ?? ""
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
        self.model.postListarConsultaPropiedad(tipo: "N", apePaterno: priApe, apeMaterno: segApe, nombres: nombres, razonSocial: "", userApp: "APPSNRPIOS", typeDoc: tipoDoc, numbDoc: nroDoc, devideId: "", userCrea: "55555") { objConsulta in
            
            self.controller?.loaderView(isVisible: false)
            let state = objConsulta.codResult == "1"
            if (state) {
                UserPreferencesController.clearGuid()
            }
            self.controller?.loadDatosConsultaPropiedad(state, message: objConsulta.msj, jsonValidacion: objConsulta)
        }
    }
    
    
    func validarDni() {
        self.controller?.loaderView(isVisible: true)
        let dni = controller?.numDocument ?? ""
        let fecEmi = controller?.dateOfIssue ?? ""
        let guid = UserPreferencesController.getGuid()
        self.model.postValidarDni(dni: dni, fecEmi: fecEmi, guid: guid) { (jsonValidacionDni) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionDni.msgResult.isEmpty
            self.controller?.loadDatosDni(state, message: jsonValidacionDni.msgResult, jsonValidacionDni: jsonValidacionDni)
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
        self.model.postValidarCe(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }
    }
    
    func validateData() {
        
        let numDocument = self.controller?.numDocument ?? ""
        let dateOfIssue = self.controller?.dateOfIssue ?? ""
        let typeDocument = self.controller?.typeDocument ?? ""
        
        var isValidDocument = self.controller?.isValidDocument ?? false
        if (typeDocument != "DNI" && typeDocument != "CE") {
            isValidDocument = true
        }
        
        if (numDocument.isEmpty || dateOfIssue.isEmpty || typeDocument.isEmpty ) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
    
    func validateDataSearch() {
        
        let names = self.controller?.names ?? ""
        let lastName = self.controller?.lastName ?? ""
        let middleName = self.controller?.middleName ?? ""
        print("numDocument::>>",names)
        print("dateOfIssue::>>",lastName)
        print("typeDocument::>>",middleName)
        
        var isValidDocument = self.controller?.isValidDocument ?? false
        
        if (names.isEmpty || lastName.isEmpty || middleName.isEmpty ) {
            self.controller?.goToConfirmSearch(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmSearch(true, message: "")
        }
        
    }
}
