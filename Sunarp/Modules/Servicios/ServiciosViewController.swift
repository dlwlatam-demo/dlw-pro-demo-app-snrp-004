//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift

class ServiciosViewController: UIViewController {
    
    
    var style: Style = Style.myApp
    let disposebag = DisposeBag()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet weak var serviceOneView: UIView!
    @IBOutlet weak var serviceTwoView: UIView!
    @IBOutlet weak var serviceThreeView: UIView!
    @IBOutlet weak var serviceFourView: UIView!
    @IBOutlet weak var serviceFiveView: UIView!
    @IBOutlet weak var serviceSixView: UIView!
    @IBOutlet weak var serviceSevenView: UIView!
    @IBOutlet weak var serviceEightView: UIView!
    @IBOutlet weak var serviceNineView: UIView!
    @IBOutlet weak var serviceTenView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        setupNavigationBar()
        addGestureView()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Servicios")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        self.serviceOneView.backgroundCard()
        self.serviceTwoView.backgroundCard()
        self.serviceThreeView.backgroundCard()
        self.serviceFourView.backgroundCard()
        self.serviceFiveView.backgroundCard()
        self.serviceSixView.backgroundCard()
        self.serviceSevenView.backgroundCard()
        self.serviceEightView.backgroundCard()
        self.serviceNineView.backgroundCard()
    }
    
    private func addGestureView(){
        
        let tapServiceOne = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceOne))
        self.serviceOneView.addGestureRecognizer(tapServiceOne)
        
        let tapServiceTwo = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceTwo))
        self.serviceTwoView.addGestureRecognizer(tapServiceTwo)
        
        let tapServiceThree = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceThree))
        self.serviceThreeView.addGestureRecognizer(tapServiceThree)
        
        let tapServiceFour = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceFour))
        self.serviceFourView.addGestureRecognizer(tapServiceFour)
        
        let tapServiceFive = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceFive))
        self.serviceFiveView.addGestureRecognizer(tapServiceFive)
        
        let tapServiceSix = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceSix))
        self.serviceSixView.addGestureRecognizer(tapServiceSix)
        
        let tapServiceSeven = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceSeven))
        self.serviceSevenView.addGestureRecognizer(tapServiceSeven)
        
        let tapServiceEight = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceEight))
        self.serviceEightView.addGestureRecognizer(tapServiceEight)
        
        let tapServiceNine = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceNine))
        self.serviceNineView.addGestureRecognizer(tapServiceNine)
        
        let tapServiceTen = UITapGestureRecognizer(target: self, action: #selector(self.onTapServiceTen))
        self.serviceTenView.addGestureRecognizer(tapServiceTen)
        
    }
    
    @objc private func onTapServiceOne() {
        let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CertificadoLiteralPartidaViewController") as! CertificadoLiteralPartidaViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc private func onTapServiceTwo() {
        let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertificadaViewController") as! PublicidadCertificadaViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceThree() {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConsultaVehicularViewController") as! ConsultaVehicularViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceFour() {
        let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConsultaPropiedadViewController") as! ConsultaPropiedadViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceFive() {
        /* let storyboard = UIStoryboard(name: "Siguelo", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "SigueloViewController") as! SigueloViewController
         self.navigationController?.pushViewController(vc, animated: true)
         */
        guard let url = URL(string: "https://siguelo.sunarp.gob.pe/siguelo/?ref=appandr") else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc private func onTapServiceSix() {
        let storyboard = UIStoryboard(name: "ConsultaSolicitudPublica", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConsultaSolicitudPublicaViewController") as! ConsultaSolicitudPublicaViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceSeven() {
        let storyboard = UIStoryboard(name: "BusquedaPersonaJuridica", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BusquedaPersonaJuridicaViewController") as! BusquedaPersonaJuridicaViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceEight() {
        let storyboard = UIStoryboard(name: "BusquedaNombre", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BusquedaNombreViewController") as! BusquedaNombreViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapServiceNine() {
        let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ConsultaTivetViewController") as! ConsultaTivetViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func onTapServiceTen() {
        /* let storyboard = UIStoryboard(name: "Siguelo", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "SigueloViewController") as! SigueloViewController
         self.navigationController?.pushViewController(vc, animated: true)
         */
        guard let url = URL(string: "https://scr.sunarp.gob.pe/alerta-normativa-registral/") else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
}
