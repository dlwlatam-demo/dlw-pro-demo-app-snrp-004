//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import WebKit

class SigueloViewController: UIViewController {
        
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var toScanerView: UIView!
    @IBOutlet weak var viewWebDetail: UIView!
    
    @IBOutlet weak var webViewCons: WKWebView!
    
    var loading: UIAlertController!
    var urlQr:String = "https://siguelo.sunarp.gob.pe/siguelo/?ref=appandr"
    
   // private lazy var presenter: QrPresenter = {
   //     return QrPresenter(controller: self)
   // }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
      //  addGestureView()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Síguelo")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        viewWebDetail.isHidden = false
        let myURL = URL(string: urlQr)
           let myRequest = URLRequest(url: myURL!)
           webViewCons.load(myRequest)
    }
    
    private func setupDesigner() {
    
  
       // toScanerView.primaryButton()
       // viewWebDetail.isHidden = true
        
    }
    
    private func addGestureView(){
      
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toScanerView.addGestureRecognizer(tapConfirmGesture)
    
    }
    
    @objc private func onTapToConfirmView() {
      
        if self.storyboard != nil {
            //if let qrReaderViewController = storyboardValue.instantiateViewController(withIdentifier: "QrReaderViewController") as? QrReaderViewController {
            let qrReaderViewController = QrReaderViewController()
            //navigationController?.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(qrReaderViewController, animated: true)
            //self.present(vc, animated: true, completion: nil)
            //}
        }
    }
    
    @IBAction func btnClose() {
        
        viewWebDetail.isHidden = true
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }

    
}


extension SigueloViewController: QrReaderViewControllerDelegate{
    func capture(action:String) {
      
        viewWebDetail.isHidden = false
        let myURL = URL(string: action)
           let myRequest = URLRequest(url: myURL!)
           webViewCons.load(myRequest)
    }
    
    
}


