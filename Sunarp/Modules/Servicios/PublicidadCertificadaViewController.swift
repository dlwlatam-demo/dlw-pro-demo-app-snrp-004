//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class PublicidadCertificadaViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var tipoCertiTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var maxLength = 0
    
    
    var certificadoId: String = ""
    
    var precOfic: String = ""
    var titleCerti: String = ""
    
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    var tipoCertificadoEntity:TipoCertificadoEntity?
    
    var codGrupoLibroArea: String = ""
    
    
    var indexRegistroSelect = 0
    var indexSelect = 0
    var indexCertificadoSelect = 0
    var tagSelect = 1 // por defecto Registro Jurídico
    var valDone: Bool = true
    
    var titleBar: String = ""
    
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var loading: UIAlertController!
    
    private lazy var presenter: PublicidadCertificadaPresenter = {
        return PublicidadCertificadaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Publicidad Certificada")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.formView.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
    }
    
    private func setupDesigner() {
        
        formView.backgroundCard()
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        tipoCertiTextField.border()
        tipoCertiTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        //obtiene lista registro juridico
        self.presenter.validarRegistroJuridico()
        
        toConfirmView.primaryButton()
        
        self.tipoCertiTextField.delegate = self
        self.tipoCertiTextField.inputAccessoryView = createToolbar()
        self.tipoDocumentoPickerView.tag = 1
        
        
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        self.typeDocumentTextField.inputAccessoryView = createToolbar()
        
        self.tipoCertiPickerView.tag = 2
        
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.tipoCertiTextField.inputView = self.tipoCertiPickerView
    
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.tipoCertiTextField.resignFirstResponder()
        self.typeDocumentTextField.resignFirstResponder()
    }
    
    
    @objc private func onTapToConfirmView() {
        self.presenter.validateData()
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        
        
        if (state) {
            print("certificadoId", certificadoId)
                switch (certificadoId){
                case "60", "61", "92","93","94","58","59","95":
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiDatosViewController") as! PublicidadCertiDatosViewController
                    vc.areaRegId = typeDocument
                    vc.certificadoId = certificadoId
                    vc.precOfic = precOfic
                    vc.titleBar = titleBar
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case "74", "75", "44": // implementacionde payment
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiRegistralViewController") as! PublicidadCertiRegistralViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.precOfic = precOfic
                    if let certificado = self.tipoCertificadoEntity {
                        vc.tipoCertificadoEntity = certificado
                    } else {
                        vc.tipoCertificadoEntity = self.tipoCertificadoEntities[0]
                    }
                  
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case "48", "85": // desarrollo 85
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiCargaGravamenesViewController") as! PublicidadCertiCargaGravamenesViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.precOfic = precOfic
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    self.navigationController?.pushViewController(vc, animated: true)
                
                case  "54", "55", "56", "57", "62", "63", "66", "79", "80", "82", "86", "87", "102", "103", "83", "89", "99":  let storyboard = UIStoryboard(name: Constant.StoryBoardName.publicidadCertificada, bundle: nil)
                    let nameViewController = Constant.Identifier.publicidadRegistroPerNatViewController
                    if let vc = storyboard.instantiateViewController(withIdentifier: nameViewController) as? PublicidadRegistroPerNatViewController {
                        vc.certificadoId = certificadoId
                        vc.areaRegId = typeDocument
                        vc.precOfic = precOfic
                        vc.titleBar = titleBar
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                case "100", "101", "4":
                    let storyboard = UIStoryboard(name: Constant.StoryBoardName.publicidadCertificada, bundle: nil)
                    let nameViewController = Constant.Identifier.publicidadOnlyCertiPJViewController
                    if let vc = storyboard.instantiateViewController(withIdentifier: nameViewController) as? PublicidadOnlyCertiPJViewController {
                        vc.certificadoId = certificadoId
                        vc.areaRegId = typeDocument
                        vc.precOfic = precOfic
                        vc.titleBar = titleBar
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                case "45", "98": //desarrpllo
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiRegisVehicularViewController") as! PublicidadCertiRegisVehicularViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case "76":// 76
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiVigPoderJuridicaViewController") as! PublicidadCertiVigPoderJuridicaViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case "84", "88":// 84 terminado
                    print("Punto de control 1")
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiVigPJViewController") as! PublicidadCertiVigPJViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case "46", "47", "96", "97"://desarrollo  ok
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertiCargaGraAeronavesViewController") as! PublicidadCertiCargaGraAeronavesViewController
                    vc.certificadoId = certificadoId
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                case "50","91":
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PCertificadoVigenciaPoderPNViewController") as! PCertificadoVigenciaPoderPNViewController
                    vc.areaRegId = typeDocument
                    vc.titleBar = titleBar
                    vc.certificadoId = certificadoId
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                case "77": // terminado
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PublicidadCertificadoVigenciaDAViewController") as! PublicidadCertificadoVigenciaDAViewController
                    vc.areaRegId = typeDocument
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.titleBar = titleBar
                    vc.certificadoId = certificadoId
                    vc.precOfic = precOfic
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                case "78":  // terminado
                    let storyboard = UIStoryboard(name: "PublicidadCertificada", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "PCVNombramientoCuradorViewController") as! PCVNombramientoCuradorViewController
                    vc.areaRegId = typeDocument
                    vc.codGrupoLibroArea = codGrupoLibroArea
                    vc.titleBar = titleBar
                    vc.certificadoId = certificadoId
                    vc.precOfic = precOfic
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    print("no es encontro el typeDocument ",typeDocument)
                    print("no es encontro el certificadoId ",certificadoId)
                }
        } else {
          let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func changeTypeRegistro(index: Int) {
        
        
        valDone = true
        self.indexRegistroSelect = index
        
        
    }
    
    func changeTypeCertificado(index: Int) {
        valDone = false
        self.indexSelect = index
        
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
        
        
        for tipoDocumento in arrayRegistroJuridico {
            self.tipoRegistroJuridico.append(tipoDocumento.nombre)
        }
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
    
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
        // certificadoId = arrayTipoCerficado.certificadoId
        for tipoDocumento in arrayTipoCerficado {
            self.tipoCertificado.append(tipoDocumento.nombre)
        }
        
        if self.tipoCertificadoEntities.count > 0 {
            self.tipoCertiTextField.text = tipoCertificadoEntities[0].nombre
            self.codGrupoLibroArea = self.tipoCertificadoEntities[0].codGrupoLibroArea
            self.titleCerti = self.tipoCertificadoEntities[0].nombre
            self.precOfic = self.tipoCertificadoEntities[0].precOfic
            titleBar = self.tipoCertificadoEntities[0].nombre
            self.certificadoId = self.tipoCertificadoEntities[0].certificadoId
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            
            self.typeDocumentTextField.resignFirstResponder()
            self.tipoCertiTextField.resignFirstResponder()
        }
        
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
            // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            
            
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
            // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    
    
    @objc func donePressed() {
        
        if tagSelect == 1 {
        //if valDone == true {
            indexRegistroSelect = indexSelect
            self.typeDocumentTextField.text = registroJuridicoEntities[indexRegistroSelect].nombre
            self.tipoCertiTextField.text = ""
            self.typeDocument = self.registroJuridicoEntities[indexRegistroSelect].areaRegId
            self.presenter.validarTipoCertificadoC(tipoCertificado:self.typeDocument)
            self.typeDocumentTextField.resignFirstResponder()
            
        }else{
            indexCertificadoSelect = indexSelect
            self.tipoCertificadoEntity = tipoCertificadoEntities[indexCertificadoSelect]
            self.tipoCertiTextField.text = tipoCertificadoEntities[indexCertificadoSelect].nombre
            
            self.codGrupoLibroArea = self.tipoCertificadoEntities[indexCertificadoSelect].codGrupoLibroArea
            self.titleCerti = self.tipoCertificadoEntities[indexCertificadoSelect].nombre
            self.precOfic = self.tipoCertificadoEntities[indexCertificadoSelect].precOfic
            titleBar = self.tipoCertificadoEntities[indexCertificadoSelect].nombre
            self.certificadoId = self.tipoCertificadoEntities[indexCertificadoSelect].certificadoId
            
            self.tipoCertiTextField.resignFirstResponder()
            
        }
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func goToLogin() {
        self.loaderView(isVisible: false)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

        self.navigationController?.pushViewController(vc, animated: true)
    
    }
}

extension PublicidadCertificadaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel

        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }

        switch pickerView.tag {
        case 1:
            label.text = self.registroJuridicoEntities[row].nombre
        case 2:
            label.text = self.tipoCertificadoEntities[row].nombre
        default:
            label.text = "Sin datos"
        }
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()

        return label
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.registroJuridicoEntities.count
        case 2:
            return self.tipoCertificadoEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.registroJuridicoEntities[row].nombre
        case 2:
            return self.tipoCertificadoEntities[row].nombre
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tagSelect = pickerView.tag
        switch pickerView.tag {
        case 1:
            self.indexSelect = row
            
        case 2:
            self.indexSelect = row
            
        default:
            return
        }
        
    }
}

extension PublicidadCertificadaViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == tipoCertiTextField) {
            if (count == maxLength) {
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                    return false
                }
            } else {
                self.isValidDocument = false
                
                self.tipoCertiTextField.resignFirstResponder()
                self.typeDocumentTextField.resignFirstResponder()
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
