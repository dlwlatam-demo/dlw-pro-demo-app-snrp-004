//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class CertificadoLiteralPartidaPresenter {
    
    private weak var controller: CertificadoLiteralPartidaViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    init(controller: CertificadoLiteralPartidaViewController) {
        self.controller = controller
    }
    
}

extension CertificadoLiteralPartidaPresenter: GenericPresenter {
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
      //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
       //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
       // }
    }
    
    func validarRegistroJuridico() {
        self.controller?.loaderView(isVisible: true)
     
        self.model.getListaJuridicos() { (arrayRegistroJuridico) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoRegistroJuridico(arrayRegistroJuridico: arrayRegistroJuridico)
           // let state = arrayTipoDocumentos.msgResult.isEmpty
           // self.controller?.loadDatosDni(state, message: arrayTipoDocumentos.msgResult, jsonValidacionDni: arrayTipoDocumentos)
        }
    }
    
    func validarTipoCertificado(tipoCertificado:String) {
        self.controller?.loaderView(isVisible: true)
        
        self.model.getListaTipoCertificado(tipoCertificado:tipoCertificado) { (arrayTipoCertificado) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoCerficado(arrayTipoCerficado: arrayTipoCertificado)
          
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
     /*   self.model.postValidarCe(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }*/
    }
    
    func validateData() {
       
        let numDocument = self.controller?.typeDocumentTextField.text ?? ""
        let typeDocument = self.controller?.typeDocument ?? ""
       
        let isChecked = self.controller?.isChecked ?? false
        var isValidDocument = self.controller?.isValidDocument ?? false
        
        if (numDocument.isEmpty ||  typeDocument.isEmpty ) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        } else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
}
