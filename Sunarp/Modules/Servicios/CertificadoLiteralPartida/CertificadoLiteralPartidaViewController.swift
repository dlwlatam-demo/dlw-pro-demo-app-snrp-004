//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class CertificadoLiteralPartidaViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var tipoCertiTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var maxLength = 0
    
    var indexSelect = 0
    var valDone: Bool = true
    
    var certificadoId: String = ""
    
    var titleCerti: String = ""
    var titleBar: String = ""
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    
    var codGrupoLibroArea: String = ""
    
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    private lazy var presenter: CertificadoLiteralPartidaPresenter = {
       return CertificadoLiteralPartidaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
      //  presenter.didLoad()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Certificado Literal de Partida")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
    }
    
    private func setupDesigner() {
       
        formView.backgroundCard()
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        tipoCertiTextField.border()
        tipoCertiTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
       // dateOfIssueTextField.border()
       // dateOfIssueTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
       
        //obtiene lista registro juridico
        self.presenter.validarRegistroJuridico()
     
        toConfirmView.primaryButton()
        
        self.tipoCertiTextField.delegate = self
        self.tipoCertiTextField.inputAccessoryView = createToolbar()
        self.tipoDocumentoPickerView.tag = 1
        
        
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputAccessoryView = createToolbar()
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        
        self.tipoCertiPickerView.tag = 2
        
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.tipoCertiTextField.inputView = self.tipoCertiPickerView
        
        
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.tipoCertiTextField.resignFirstResponder()
    }
    

    @objc private func onTapToConfirmView() {
       // self.numDocument = self.tipoCertiTextField.text ?? ""
       // self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        self.presenter.validateData()
        
        
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                //validateTypeDocument()
                
                
                let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CertificadoLiteralPartidaDetailViewController") as! CertificadoLiteralPartidaDetailViewController
               // vc.array = users
                vc.areaRegId = typeDocument
                vc.codGrupoLibroArea = codGrupoLibroArea
                vc.certificadoId = certificadoId
                
                vc.titleCerti = titleCerti
                vc.titleBar = titleCerti
                print("titleCerti = ", titleCerti)
                self.navigationController?.pushViewController(vc, animated: true)
                /*
                
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPasswordRegisterViewController") as! ConfirmPasswordRegisterViewController
                vc.numcelular = self.numcelular
                vc.numDocument = self.numDocument
                vc.dateOfIssue = self.dateOfIssue
                vc.typeDocument = self.typeDocument
                vc.names = self.names
                vc.lastName = self.lastName
                vc.middleName = self.middleName
                vc.gender = self.gender
                vc.email = self.email
                self.navigationController?.pushViewController(vc, animated: true)
                 */
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func changeTypeDocument(index: Int) {
        
        valDone = true
        self.indexSelect = index
        /*
        self.tipoCertiTextField.text = ""
        self.typeDocument = self.registroJuridicoEntities[index].areaRegId
        self.presenter.validarTipoCertificado(tipoCertificado:self.typeDocument)
         */
    }
    
    func changeTypeGender(index: Int) {
        
        valDone = false
        self.indexSelect = index
        
    }
    
    func validateTypeDocument() {
        self.numDocument = self.tipoCertiTextField.text ?? ""
        self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == "DNI") {
            if (!numDocument.isEmpty && !dateOfIssue.isEmpty) {
               // self.presenter.validarDni()
            }
        } else if (self.typeDocument == "CE") {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
            }
        }
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
        
        
        for tipoDocumento in arrayRegistroJuridico {
            self.tipoRegistroJuridico.append(tipoDocumento.nombre)
        }
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
  
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
        for tipoDocumento in arrayTipoCerficado {
            self.tipoCertificado.append(tipoDocumento.nombre)
        }
     
        if self.tipoCertificadoEntities.count > 0 {
            self.tipoCertiTextField.text = tipoCertificadoEntities[0].nombre
            self.codGrupoLibroArea = self.tipoCertificadoEntities[0].codGrupoLibroArea
            self.titleCerti = self.tipoCertificadoEntities[0].nombre
            
            self.certificadoId = self.tipoCertificadoEntities[0].certificadoId
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            
            self.typeDocumentTextField.resignFirstResponder()
            self.tipoCertiTextField.resignFirstResponder()
        }
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            
           
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            comps.year = -10
            let minDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = .now
            datePicker.minimumDate = minDate
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            comps.year = -100
            let minDate = calender.date(byAdding: comps, to: .now)
            comps.year = -18
            let maxDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
        }
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressed() {
        
        if valDone == true {
            self.typeDocumentTextField.text = registroJuridicoEntities[indexSelect].nombre
        
            self.tipoCertiTextField.text = ""
            self.typeDocument = self.registroJuridicoEntities[indexSelect].areaRegId
            self.presenter.validarTipoCertificado(tipoCertificado:self.typeDocument)
            //self.typeDocumentTextField.resignFirstResponder()
            self.view.endEditing(true)
        }else{
            
              self.tipoCertiTextField.text = tipoCertificadoEntities[indexSelect].nombre
            
            self.codGrupoLibroArea = self.tipoCertificadoEntities[indexSelect].codGrupoLibroArea
            self.titleCerti = self.tipoCertificadoEntities[indexSelect].nombre
            
            self.certificadoId = self.tipoCertificadoEntities[indexSelect].certificadoId
            
            if (indexSelect == 0) {
                self.gender = "M"
            } else if (indexSelect == 1) {
                self.gender = "F"
            } else {
                self.gender = "-"
            }
            //self.tipoCertiTextField.resignFirstResponder()
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
}

extension CertificadoLiteralPartidaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel

        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }

        switch pickerView.tag {
        case 1:
            label.text = self.registroJuridicoEntities[row].nombre
        case 2:
            label.text = self.tipoCertificadoEntities[row].nombre
        default:
            label.text = "Sin datos"
        }
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()

        return label
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.registroJuridicoEntities.count
        case 2:
            return self.tipoCertificadoEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.registroJuridicoEntities[row].nombre
        case 2:
            return self.tipoCertificadoEntities[row].nombre
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeDocument(index: row)
            //self.typeDocumentTextField.text = registroJuridicoEntities[row].nombre
          //  self.typeDocumentTextField.resignFirstResponder()
        case 2:
            self.changeTypeGender(index: row)
            
          //  self.tipoCertiTextField.text = tipoCertificadoEntities[row].nombre
            //self.tipoCertiTextField.resignFirstResponder()
            
        default:
            return
        }
        
    }
}

extension CertificadoLiteralPartidaViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == tipoCertiTextField) {
            if (count == maxLength) {
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                    self.validateTypeDocument()
                    return false
                }
            } else {
                self.isValidDocument = false
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
}

/*
private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}
*/
