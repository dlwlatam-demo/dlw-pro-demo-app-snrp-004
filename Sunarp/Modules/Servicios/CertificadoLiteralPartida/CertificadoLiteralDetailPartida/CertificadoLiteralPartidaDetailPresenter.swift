//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class CertificadoLiteralPartidaDetailPresenter {
    
    private weak var controller: CertificadoLiteralPartidaDetailViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
 
    lazy private var modelTive: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()

    
    init(controller: CertificadoLiteralPartidaDetailViewController) {
        self.controller = controller
    }
    
}

extension CertificadoLiteralPartidaDetailPresenter: GenericPresenter {
    
    func willAppear() {
     //   self.controller?.loaderView(isVisible: true)
        self.model.getListaOficinaRegistral{ (arrayOficinas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
  
    
    func listaValidaPartidaLiteral() {
        
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let areaRegId = controller?.areaRegId ?? ""
        let codGrupo = controller?.codGrupoLibroArea ?? ""
        let tipoPartidaFicha = controller?.tipoPartidaFicha ?? ""
        let numPart = controller?.numPart ?? ""
        let coServ = controller?.coServ ?? ""
        let coTipoRgst = controller?.coTipoRgst ?? ""
        
        self.controller?.loaderView(isVisible: true)
        self.model.getListaValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultados(busquedaResponse: arrayBusqTive)
        }
    }
        
        
    
    
    func listaService() {
        
        let areaRegId = controller?.areaRegId ?? ""
       // self.controller?.loaderView(isVisible: true)
        print("areaRegId::>>",areaRegId)
        self.model.getListaTipoServicioSunarp(areaRegId: areaRegId){ (arrayService) in
           // self.controller?.loaderView(isVisible: false)
            self.controller?.loadService(serviceResponse: arrayService)
        }
    }
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
      //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
       //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
       // }
    }
    
    func validarRegistroJuridico() {
        self.controller?.loaderView(isVisible: true)
     
        self.model.getListaJuridicos() { (arrayRegistroJuridico) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoRegistroJuridico(arrayRegistroJuridico: arrayRegistroJuridico)
           // let state = arrayTipoDocumentos.msgResult.isEmpty
           // self.controller?.loadDatosDni(state, message: arrayTipoDocumentos.msgResult, jsonValidacionDni: arrayTipoDocumentos)
        }
    }
    
    func validarTipoCertificado(tipoCertificado:String) {
        self.controller?.loaderView(isVisible: true)
        
        self.model.getListaTipoCertificado(tipoCertificado:tipoCertificado) { (arrayTipoCertificado) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoCerficado(arrayTipoCerficado: arrayTipoCertificado)
          
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
        
            self.controller?.loaderView(isVisible: false)
     /*   self.model.postValidarCe(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }*/
    }
    
    func validateData() {
       
        let numPart = self.controller?.numPart ?? ""
        let regPubId = self.controller?.regPubId ?? ""
        let coServ = self.controller?.coServ ?? ""
       
        
        
        if (numPart.isEmpty || regPubId.isEmpty || coServ.isEmpty ) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
}
