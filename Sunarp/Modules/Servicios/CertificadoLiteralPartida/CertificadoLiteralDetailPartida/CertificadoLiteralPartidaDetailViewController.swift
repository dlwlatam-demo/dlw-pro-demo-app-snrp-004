//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit

class CertificadoLiteralPartidaDetailViewController: UIViewController,UITableViewDataSource {
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typeSolicitudTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    @IBOutlet weak var insertNumTextField: UITextField!
    @IBOutlet weak var labelMensaje:UILabel!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    
    var numDocument: String = ""
    
    var certificadoId: String = ""
    
    
    var valoficinaOrigen: String = ""
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    var maxLength = 0
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    var indexSelect = 0
    var valDone: Bool = true
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    var listaValidaPartidaLiteralEntity: [validaPartidaLiteralEntity] = []
    
    private lazy var presenter: CertificadoLiteralPartidaDetailPresenter = {
       return CertificadoLiteralPartidaDetailPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var titleCerti: String = ""
    var titleBar: String = ""
    
    var codGrupoLibroArea: String = ""
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var tipo: String = ""
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
      //  presenter.didLoad()
        setupNavigationBar()
        
      
       // self.presenter.listaTive()
        registerCell()
        self.presenter.willAppear()
        self.presenter.listaService()
        
        self.tableSearchView.reloadData()
        tipoPartidaFicha = "1"
        print("areaRegId = ", areaRegId)
        if areaRegId == "21000"{
            self.labelMensaje.isHidden = false
        }
        else {
            self.labelMensaje.isHidden = true
        }
        
        self.toConfirmViewNum.isHidden = false
        titleLabel.text = titleCerti
        
        self.maxLength = 9
        numDocument = ""
        if areaRegId == "24000"{
            typeRadioButtonLabel.text = "Placa"
            self.onTapToConfirmViewNum()
        }else{
            typeRadioButtonLabel.text = "Ficha"
            self.onTapToConfirmViewAnio()
        }
        
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Certificado Literal de Partida")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        self.tipo = "T"
    }
    
    
    @objc private func onTapToConfirmViewAnio() {
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        numDocument = ""
        self.tipoPartidaFicha = "1"
        self.insertNumTextField.text = ""

        if areaRegId == "21000"{
            self.labelMensaje.isHidden = false
        }
        else {
            self.labelMensaje.isHidden = true
        }
        print(self.coServ)
        if (self.coServ == "181") { // CERTI. LITERAL - SARP
            self.insertNumTextField.keyboardType = .alphabet
            self.insertNumTextField.autocapitalizationType = .allCharacters
        }
        else {
            self.insertNumTextField.keyboardType = .decimalPad
        }
        
        self.insertNumTextField.maxLength = 10
        if let t: String = insertNumTextField.text {
            insertNumTextField.text = String(t.prefix(10))
        }
        
        self.tipo = "T"
        view.endEditing(true)
    }
  
    @objc private func onTapToConfirmViewNum() {
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        
        //self.anioTituloText.placeholder = "Año de Publicidad"
        //self.numTituloText.placeholder = "Nº de Publicidad"
        self.tipo = "P"
        print(self.coServ)
        self.insertNumTextField.text = ""
        print(typeRadioButtonLabel.text)
        if typeRadioButtonLabel.text == "Ficha"{
            self.tipoPartidaFicha = "2"
            if areaRegId == "21000"{
                self.labelMensaje.isHidden = false
            }
            else {
                self.labelMensaje.isHidden = true
            }
            self.insertNumTextField.keyboardType = .decimalPad
            
            print("areaRegId2 = ", areaRegId)
            // areaRegId = "21000"
            
            numDocument = ""
        }else if typeRadioButtonLabel.text == "Placa"{
            self.tipoPartidaFicha = "4"
            self.insertNumTextField.keyboardType = .alphabet
            self.insertNumTextField.maxLength = 7
            self.labelMensaje.isHidden = true
            
            print("areaRegId3 = ", areaRegId)
            // areaRegId = "24000"
            
            numDocument = ""
        }else{
            self.tipoPartidaFicha = "3"
            self.insertNumTextField.maxLength = 10
            if areaRegId == "21000"{
                self.labelMensaje.isHidden = false
            }
            else {
                self.labelMensaje.isHidden = true
            }
            
            numDocument = ""
        }
        view.endEditing(true)
    }
  
    
    private func setupDesigner() {
       
        formView.backgroundCard()
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        typeSolicitudTextField.border()
        typeSolicitudTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        self.insertNumTextField.borderAndPadding()
        self.insertNumTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
       
        //obtiene lista registro juridico
       // self.presenter.validarRegistroJuridico()
     
        toConfirmView.primaryButton()
        
        
        self.tipoDocumentoPickerView.tag = 1
        
        
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        
        self.typeOficinaTextField.delegate = self
        self.typeOficinaTextField.inputAccessoryView = createToolbar()
        self.tipoCertiPickerView.tag = 2
        
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.delegate = self
        self.typeSolicitudTextField.inputAccessoryView = createToolbar()
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        
          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
        self.insertNumTextField.delegate = self
        
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier)
        
        let profileCellSearchNibPlaca = UINib(nibName: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNibPlaca, forCellReuseIdentifier: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier)
    }
    
    func loadResultados(busquedaResponse: [validaPartidaLiteralEntity]) {
        self.listaValidaPartidaLiteralEntity = busquedaResponse
        
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            self.insertNumTextField.resignFirstResponder()
            
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaValidaPartidaLiteralEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        //self.insertNumTextField.resignFirstResponder()
        self.loaderView(isVisible: false)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.insertNumTextField.resignFirstResponder()
        self.textFieldShouldReturn(insertNumTextField)
        
    }
    
    func padZeroOnLeft(s: String, length: Int) -> String {
       let padZeroSize = max(0, length - s.count)
       let newStr = String(repeating: "0", count: padZeroSize) + s
       return newStr
    }
    
    @objc private func onTapToConfirmView() {
        
        self.numPart = self.insertNumTextField.text ?? ""
        if self.tipoPartidaFicha != "4" {
            if self.numPart.count < 8 && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self.numPart)) {
                self.numPart = padZeroOnLeft(s: self.numPart, length: 8)
                self.insertNumTextField.text = self.numPart
            }
        }
        self.presenter.validateData()
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                self.presenter.listaValidaPartidaLiteral()
                
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func changeTypeDocument(index: Int) {
        valDone = true
        self.indexSelect = index
        
    }
    
    func changeTypeGender(index: Int) {
        valDone = false
        self.indexSelect = index
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    func loadService(serviceResponse: [ServicioSunarEntity]) {
        self.servicesLista = serviceResponse
        //self.loaderView(isVisible: false)
       
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
        for tipoDocumento in arrayRegistroJuridico {
            self.tipoRegistroJuridico.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
  
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
        for tipoDocumento in arrayTipoCerficado {
            self.tipoCertificado.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    @objc func donePressed() {
        
        if valDone == true {
            self.regPubId = self.oficinas[indexSelect].RegPubId
            self.oficRegId = self.oficinas[indexSelect].OficRegId
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            self.view.endEditing(true)
        }else{
            
            self.typeSolicitudTextField.text = servicesLista[indexSelect].deServRgst
            self.typeSolicitudTextField.resignFirstResponder()
            
            self.coServ = self.servicesLista[indexSelect].coServ
            self.coTipoRgst = self.servicesLista[indexSelect].coTipoRgst
            print("coServ:>>",coServ)
            if (self.coServ == "37") {
                self.toConfirmViewNum.isHidden = true
                imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
            }else{
                self.toConfirmViewNum.isHidden = false
                imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
                imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
                
            }
        }
        if self.areaRegId == "24000" {
            self.onTapToConfirmViewNum()
        }
        else {
            self.onTapToConfirmViewAnio()
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("couunt:>>",self.listaValidaPartidaLiteralEntity.count)
            return self.listaValidaPartidaLiteralEntity.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("areaRegId__::>>",areaRegId)
        if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaPlacaViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
    
             let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
             print("listaValidaPartidaLiteralEntity:>>",listaPro)
             cell.setup(with: listaPro)
              cell.onTapVerSolicitud = {[weak self] in
             let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                  
                  vc.codLibro = listaPro.codLibro
                  vc.numPartida = listaPro.numPartida
                  vc.fichaId = listaPro.fichaId
                  vc.tomoId = listaPro.tomoId
                  vc.fojaId = listaPro.fojaId
                  vc.refNumPart = listaPro.refNumPart
                  vc.codArea = listaPro.codArea
                 // vc.ofiSARP = listaPro.ofiSARP
                  vc.ofiSARP = "-"
                  vc.coServicio = self!.coServ
                  vc.coTipoRegis = self!.coTipoRgst
                  vc.codigoGla = listaPro.codigoGla
                  
                  vc.areaRegId = self!.areaRegId
                  print("self!.areaRegId::-->>>",self!.areaRegId)
                  print("self!.regPubId::-->>>",self!.regPubId)
                  
                  vc.regPubId = self!.regPubId
                  vc.oficRegId = self!.oficRegId
                  
                  
                  vc.codZona = listaPro.codZona
                  vc.codOficina = listaPro.codOficina
                  vc.direccionPredio = listaPro.direccionPredio
                  vc.areaRegisDescripcion = listaPro.areaRegisDescripcion
                  vc.nombreOfic = listaPro.nombreOfic
                  vc.libroDescripcion = listaPro.libroDescripcion
                  vc.numeroPlaca = listaPro.numeroPlaca
                  vc.baja = listaPro.baja
                  vc.regPubSiglas = listaPro.regPubSiglas
                  
                  vc.certificadoId = self!.certificadoId
                  vc.titleCerti = self!.titleCerti
                  vc.titleBar = self!.titleBar
                  print("titleCerti1 = ", self!.titleCerti)
                  print("titleBar1 = ", self!.titleBar)
                  vc.valoficinaOrigen = self!.valoficinaOrigen
                  
              self?.navigationController?.pushViewController(vc, animated: true)
             }
             return cell
            
        }else{
          
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
    
            let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
            print("listaValidaPartidaLiteralEntity:>>",listaPro)
            cell.setup(with: listaPro)
            
             cell.onTapVerSolicitud = {[weak self] in
            let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                 
                 vc.codLibro = listaPro.codLibro
                 vc.numPartida = listaPro.numPartida
                 vc.refNumPart = listaPro.refNumPart
                 vc.codArea = listaPro.codArea
                 
                 vc.areaRegId = self!.areaRegId
                 print("self!.areaRegId::-->>>",self!.areaRegId)
                 print("self!.regPubId::-->>>",self!.regPubId)
                 vc.regPubId = self!.regPubId
                 vc.oficRegId = self!.oficRegId
                 
                 vc.fichaId = listaPro.fichaId
                 vc.tomoId = listaPro.tomoId
                 vc.fojaId = listaPro.fojaId
                // vc.ofiSARP = listaPro.ofiSARP
                 vc.ofiSARP = "-"
                 vc.coServicio = self!.coServ
                 vc.coTipoRegis = self!.coTipoRgst
                 vc.codZona = listaPro.codZona
                 vc.codOficina = listaPro.codOficina
                 vc.direccionPredio = listaPro.direccionPredio
                 vc.areaRegisDescripcion = listaPro.areaRegisDescripcion
                 vc.nombreOfic = listaPro.nombreOfic
                 vc.libroDescripcion = listaPro.libroDescripcion
                 vc.numeroPlaca = listaPro.numeroPlaca
                 vc.baja = listaPro.baja
                 vc.regPubSiglas = listaPro.regPubSiglas
                 vc.codigoGla = listaPro.codigoGla
                 
                 vc.certificadoId = self!.certificadoId
                 vc.titleCerti = self!.titleCerti
                 vc.titleBar = self!.titleBar
                 print("titleCerti1 = ", self!.titleCerti)
                 print("titleBar1 = ", self!.titleBar)
                 vc.valoficinaOrigen = self!.valoficinaOrigen
                 
             self?.navigationController?.pushViewController(vc, animated: true)
            }
    
            return cell
        }
    }
}

extension CertificadoLiteralPartidaDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel

        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }

        switch pickerView.tag {
        case 1:
            label.text = self.oficinas[row].Nombre
        case 2:
            label.text = self.servicesLista[row].deServRgst
        default:
            label.text = "Sin datos"
        }
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()

        return label
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            //return self.tipoCertificado.count
            return self.servicesLista.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            //return self.tipoCertificado[row]
            return self.servicesLista[row].deServRgst
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeDocument(index: row)
            numDocument = ""
        case 2:
            self.changeTypeGender(index: row)
            numDocument = ""
        default:
            return
        }
        
    }
}

extension CertificadoLiteralPartidaDetailViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if (textField == typeSolicitudTextField) {
            valDone = false
            self.indexSelect = 0
        }
     
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let regex = "^[a-zA-Z0-9]*$"
        let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
        
        if(updatedText.count > 10){
            return false
        }
        
        let newStr = updatedText
        if (!textTest.evaluate(with: newStr)) {
            return false
        }
        
        var text = numDocument
        if range.length == Int.zero {
            text.append(contentsOf: string)
        } else {
            text = String(text.dropLast())
        }
        
        numDocument = text.uppercased()
        textField.text = numDocument
        return false
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
   
        if(numDocument.count > 1){
            guard let text = textField.text, !text.isEmpty else {
                // Si no hay texto, llenar con ceros y actualizar el campo de texto
                textField.text = ""
                return true
            }
            // Verificar si el texto es numérico
            if self.tipoPartidaFicha != "4" {
                if let intValue = Int(text), let formattedValue = String(format: "%08d", intValue) as NSString? {
                    textField.text = formattedValue as String
                } else {
                    // Si no es numérico, convertir a mayúsculas
                    textField.text = text.uppercased()
                }
            } else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
            // Ocultar el teclado
            textField.resignFirstResponder()
            
            return true
        }
        return false
    }
}


// MARK: - Table view delegate
extension CertificadoLiteralPartidaDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}

