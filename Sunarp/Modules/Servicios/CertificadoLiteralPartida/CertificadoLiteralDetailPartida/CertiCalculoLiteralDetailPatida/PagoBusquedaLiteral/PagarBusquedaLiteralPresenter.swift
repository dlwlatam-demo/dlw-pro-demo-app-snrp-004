//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class PagarBusquedaLiteralPresenter {
    
    private weak var controller: PagarBusquedaLiteralViewController?
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    init(controller: PagarBusquedaLiteralViewController) {
        self.controller = controller
    }
    
}

extension PagarBusquedaLiteralPresenter: GenericPresenter {
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        //let guid = UserPreferencesController.getGuid()
        self.modelDoc.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }

    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getAreasRegistrales{ (arrayArea) in
            self.controller?.loadAreas(areaResponse: arrayArea)
        }
    }
    
    func loadGrupos(codGrupoLibroArea: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.getGrupoLibro(codGrupoLibroArea: codGrupoLibroArea){ (arrayGrupo) in
            self.controller?.loadGrupos(grupoResponse: arrayGrupo)
        }
    }
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId() {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }
        
    func postDetalleAsientosLiteralSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaLiteralModel.postDetalleAsientosLiteralSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (niubizResponse) in
            
             self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
    
        
}
