//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PagarBusquedaLiteralViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var nombreText: UITextField!
    @IBOutlet weak var apellidoPaternoText: UITextField!
    @IBOutlet weak var apellidoMaternoText: UITextField!
    @IBOutlet weak var costoTotalLabel: UILabel!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var nameRazonSocialText: UITextField!
    @IBOutlet weak var numDocText: UITextField!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    let normalAttrs1: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: SunarpFont.regular10]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var numDocument: String = ""
    
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var valoficinaOrigen: String = ""
    var titleText: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    var codigoGla: String = ""
    
    @IBOutlet weak var opeDetail: UILabel!
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    private lazy var presenter: PagarBusquedaLiteralPresenter = {
       return PagarBusquedaLiteralPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        presenter.didLoad()
        self.typeNameDocument = "09"
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Detalle de la solicitud")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
      //  self.scrollview.contentInset = contentInsets
     //   self.scrollview.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
      //  self.scrollview.contentInset = contentInsets
       // self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.nombreText.borderAndPaddingLeftAndRight()
        self.apellidoPaternoText.borderAndPaddingLeftAndRight()
        self.apellidoMaternoText.borderAndPaddingLeftAndRight()
        self.pagarView.primaryButton()
        self.checkView.borderCheckView()
        self.pagarView.primaryDisabledButton()
        
        self.nameRazonSocialText.borderAndPaddingLeftAndRight()
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.areaRegistralPickerView.tag = 1
        self.participantePickerView.tag = 2
        
                
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
        
        self.nombreText.delegate = self
        self.apellidoPaternoText.delegate = self
        self.apellidoMaternoText.delegate = self
        self.numDocText.delegate = self
        
        
        
        let textColor = UIColor.lightGray
        let amount = String(format: "S/ %.2f", (montoCalc as NSString).doubleValue)
                        
        let costoTitle = NSMutableAttributedString(string: "", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: normalAttrs1)
        costoValue.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSRange(location: 0, length: costoValue.length))
        let opeDetailAttributes = NSMutableAttributedString(string: titleText, attributes: normalAttrs1)
        opeDetailAttributes.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSRange(location: 0, length: opeDetailAttributes.length))
        
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
        self.opeDetail.attributedText = opeDetailAttributes
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkView.addGestureRecognizer(tapChecked)
        
        let tapTerminoCondicionesGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapTerminoCondicionesLabel))
        self.terminosLabel.addGestureRecognizer(tapTerminoCondicionesGesture)
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        //self.tipo = "T"
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
        //self.tipoDocumentoPickerView.delegate = self
        //self.tipoDocumentoPickerView.dataSource = self
        
       // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
    }
    
    
    @objc private func onTapToConfirmViewAnio() {
      //  self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        toPersonNatuView.isHidden = false
        toPersonJuriView.isHidden = true
    }
  
    @objc private func onTapToConfirmViewNum() {
     //   self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toPersonNatuView.isHidden = true
        toPersonJuriView.isHidden = false
    }
    
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
        self.nombreText.text          = usuario.nombres
        self.apellidoPaternoText.text = usuario.priApe
        self.apellidoMaternoText.text = usuario.segApe
        self.nameRazonSocialText.text = usuario.nombres
        self.numDocText.text          = usuario.nroDoc
        
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            self.areaRegistralText.text = "DNI"
            typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
            self.areaRegistralText.text = "CE"
            typeDocument = "03"
        } else {
        }
        
        /*
        var numPartida: String = ""
        var fichaId: String = ""
        var tomoId: String = ""
        var fojaId: String = ""
        var ofiSARP: String = ""
        var coServicio: String = ""
        var coTipoRegis: String = ""
        */
        
        //save Solictud
        
        var numero1 = Int(countCantPag) ?? 0
        var numero2 = Int(countCantPagExo) ?? 0
        var numPagina = numero1 + numero2
        
        var nuAsieSelectSARPvalid = nuAsieSelectSARP.take(1)
        
        if nuAsieSelectSARPvalid == "P"{
        }else{
            nuAsieSelectSARP = ""
        }
        
        print("certificadoId::",certificadoId)
        print("codArea::",codArea)
        print("codLibro::",codLibro)
        print("valoficinaOrigen::",valoficinaOrigen)
        print("refNumPart::",refNumPart)
        print("numPartida::",numPartida)
        print("numeroPlaca::",numeroPlaca)
        print("usuario.tipo::",usuario.tipo)
        print(" usuario.priApe::", usuario.priApe)
        print("usuario.segApe::",usuario.segApe)
        print("usuario.nombres::",usuario.nombres)
        print("usuario.tipoDoc::",usuario.tipoDoc)
        print("usuario.nroDoc::",usuario.nroDoc)
        print("usuario.email::",usuario.email)
        print("montoCalc::",montoCalc)
        print("countCantPag::",countCantPag)
        print("countCantPagExo::",countCantPagExo)
        print("numPagina::",String(numPagina))
        print("nuAsieSelectSARP::",nuAsieSelectSARP)
        print("imPagiSIR::",imPagiSIR)
        print("nuSecu::",nuSecu)
        print("codigoGla::", codigoGla)
        
        presenter.postDetalleAsientosLiteralSaveSolicitud(codCerti: certificadoId, codArea: codArea, codLibro: codLibro, oficinaOrigen: valoficinaOrigen, refNumPart: refNumPart, partida: numPartida, placa: numeroPlaca, tpoPersona: usuario.tipo, apePaterno: usuario.priApe, apeMaterno: usuario.segApe, nombre: usuario.nombres, razSoc: "", tpoDoc: usuario.tipoDoc, numDoc: usuario.nroDoc, email: usuario.email, costoServicio: montoCalc, cantPaginas: countCantPag, cantPaginasExon: countCantPagExo, paginasSolicitadas: String(numPagina), nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecu, ipRemote: "", sessionId: "", usrId: "APPSNRPIOS")
        
        print("tipoDoc:>>",usuario.tipoDoc)
       // self.emailText.text = usuario.email
        
       // public let tipoDoc: String
       // public let nroDoc: String
        
        /*
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: "S/ \(nsolMonLiq).00", attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
        */
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
        self.numDocText.resignFirstResponder()
        
    }
    
    @objc private func onTapTerminoCondicionesLabel(){
       /* if let url = URL(string: "https://www.sunarp.gob.pe/politicas-privacidad.aspx") {
            UIApplication.shared.open(url)
        }
        */
        self.alertTermyConPopupDialog(title: "Estimado(a) usuario(a):",message: "¿Está seguro que desea remover el mandato?",primaryButton: "No,regresar",secondaryButton: "Si,remover", addColor: SunarpColors.red, delegate: self)
    }
    
    @objc private func onTapPagarView(){
        if (self.isChecked) {
            loaderView(isVisible: true)
            presenter.getVisaKeys()
        }
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            self.pagarView.primaryDisabledButton()
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            self.pagarView.primaryButton()
           
        }
        self.isChecked = !self.isChecked
        print(tipoDocumentosEntities)
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("tpoPago::-->>",solicitud.tpoPago)
        print("solicitante::-->>",solicitud.solicitante)
        print("solicitudId::-->>",solicitud.solicitudId)
        print("email::-->>",solicitud.email)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz // "12548"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func changeTypeDocument(index: Int) {
        self.typeNameDocument = self.tipoDocumentosEntities[index].tipoDocId
        
        print("self.typeNameDocument:---:>>>",self.typeNameDocument)
       /* if (self.typeNameDocument == "DNI") {
              typeDocument = "09"
        } else if (self.typeNameDocument == "CE") {
              typeDocument = "03"
        } else {
        }*/
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PagarBusquedaLiteralViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        let regex = "^[a-zA-Z0-9]*$"
        let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
        
    
        if self.typeNameDocument == "09" {
            if(updatedText.count > 8) {
                return false
            }
        } else {
            if(updatedText.count > 20) {
                return false
            }
            
        }
        
        let newStr = updatedText
        if (!textTest.evaluate(with: newStr)) {
            return false
        }
        
        var text = textFieldText
        if range.length == Int.zero {
            text.append(contentsOf: string)
        } else {
            text = String(text.dropLast())
        }
       
        textField.text = text.uppercased()
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
}

extension PagarBusquedaLiteralViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentosEntities[row].descripcion
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            
            self.changeTypeDocument(index: row)
            self.areaRegistralText.text = tipoDocumentosEntities[row].descripcion
            self.areaRegistralText.resignFirstResponder()
            
            
        default:
            return
        }
        
    }
}

//MARK: - VISANET DELEGATE SECTION

extension PagarBusquedaLiteralViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoExitosoViewController") as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.apPaterno = self.apellidoPaternoText.text ?? ""
                vc.apMaterno = self.apellidoMaternoText.text ?? ""
                vc.nombre = self.nombreText.text ?? ""
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension String {
    func take(_ n: Int) -> String {
        guard n >= 0 else {
            fatalError("n should never negative")
        }
        let index = self.index(self.startIndex, offsetBy: min(n, self.count))
        return String(self[..<index])
    }
}

extension PagarBusquedaLiteralViewController: TermyCondiDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
          //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
       // print("self.val_aaCont::>>",self.val_aaCont)
        //self.presenter.putDeleteMandato(aaCont: self.val_aaCont, nuCont: self.val_nuCount)
        
        
    }
}
