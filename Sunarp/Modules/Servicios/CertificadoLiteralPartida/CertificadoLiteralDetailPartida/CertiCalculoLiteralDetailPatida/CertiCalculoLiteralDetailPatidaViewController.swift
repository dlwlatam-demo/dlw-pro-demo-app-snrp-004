//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit
import VisaNetSDK

class CertiCalculoLiteralDetailPatidaViewController: UIViewController {
    
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var formView: UIView!
    
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countExoLabel: UILabel!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    
    var certificadoId: String = ""
    
    var isButtonCalcular: Bool = false
    
    var countPagina: String = ""
    var countPaginaExon: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var valoficinaOrigen: String = ""
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var loading: UIAlertController!
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var totalPaginasPartidaFicha: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    
    var listaValidaPartidaLiteralEntity: [GetDetalleAsientoEntity] = []
    var listaValidaPartidaLiteralRMCEntity: [GetDetalleAsientoRMCEntity] = []
    
    
    
    private lazy var presenter: CertiCalculoLiteralDetailPatidaPresenter = {
       return CertiCalculoLiteralDetailPatidaPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    var codZona: String = ""
    var codOficina: String = ""
    var numeroPlaca: String = ""
    var direccionPredio: String = ""
    var areaRegisDescripcion: String = ""
    var nombreOfic: String = ""
    var libroDescripcion: String = ""
    var baja: String = ""
    var codGrupoLibroArea: String = ""
    var regPubSiglas: String = ""
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    var titleCerti: String = ""
    var titleBar: String = ""
    var arrNuSecu = [String]()
    var arrImPagiSIR = [String]()
    var arrNuAsieSelectSARP = [String]()
    var codigoGla: String = ""

    var tipo: String = ""
    @IBOutlet weak var selectorAllPages: UIImageView!
    @IBOutlet weak var selectorCleanSelection: UIImageView!
    
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cantiMontoLabel: UILabel!
    @IBOutlet weak var titleMontoLabel: UILabel!
    
    
    @IBOutlet weak var registroPublicoLabel: UILabel!
    @IBOutlet weak var oficinaRegistralLabel: UILabel!
    @IBOutlet weak var partidaLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    @IBOutlet weak var restadoVehiculoLabel: UILabel!
    
    
    @IBOutlet weak var valor_registroPublicoLabel: UILabel!
    @IBOutlet weak var valor_oficinaRegistralLabel: UILabel!
    @IBOutlet weak var valor_partidaLabel: UILabel!
    @IBOutlet weak var valor_placaLabel: UILabel!
    @IBOutlet weak var valor_restadoVehiculoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
      //  presenter.didLoad()
        setupNavigationBar()
        
       // self.presenter.listaTive()
        registerCell()
        
        self.tableSearchView.reloadData()
        tipoPartidaFicha = "1"
        
        self.toConfirmViewNum.isHidden = false
        
        print("areaRegId:--:>>",areaRegId)
        print("coServicio:--:>>",coServicio)
        print("coTipoRegis:--:>>",coTipoRegis)
        if areaRegId == "24000"{
            
            self.registroPublicoLabel.text = self.regPubSiglas
            self.oficinaRegistralLabel.text = self.nombreOfic
            self.partidaLabel.text = self.numPartida
            self.numeroPlaca = self.numeroPlaca.removeWhitespace()
            print("self.numeroPlaca:--:>>",self.numeroPlaca)
            self.placaLabel.text = self.numeroPlaca
            self.restadoVehiculoLabel.text = self.baja
            
            self.valor_registroPublicoLabel.text = "Registro Público:"
            self.valor_oficinaRegistralLabel.text = "Oficina Registral:"
            self.valor_partidaLabel.text = "Partida:"
            self.valor_placaLabel.text = "Placa:"
            self.valor_restadoVehiculoLabel.text = "Estado de Vehículo:"
            
            
            if coServicio == "37" && coTipoRegis == "008"{
                self.presenter.listaValidaPartidaLiteralRMCTrue()
            }else{
                self.presenter.listaValidaPartidaLiteralRMCFalse()
            }
        }else{
            
                self.registroPublicoLabel.text = self.nombreOfic
                self.oficinaRegistralLabel.text = self.numPartida
                self.partidaLabel.text = self.direccionPredio
                self.placaLabel.text = self.areaRegisDescripcion
                self.restadoVehiculoLabel.text = self.libroDescripcion
                
                self.valor_registroPublicoLabel.text = "Oficina registral:"
                self.valor_oficinaRegistralLabel.text = "Partida:"
                self.valor_partidaLabel.text = "Dirección:"
                self.valor_placaLabel.text = "Área registral:"
                self.valor_restadoVehiculoLabel.text = "Registro de:"
            self.presenter.listaValidaPartidaLiteral()
        }
        
        
        titleLabel.text = titleCerti
        self.titleMontoLabel.text = "Calcular"
        isButtonCalcular = false
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Certificado Literal de Partida")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        selectorAllPages.image = SunarpImage.getImage(named: . iconRadioButtonGray)
        selectorCleanSelection.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        self.tipo = "T"
    }
    
    
    @objc private func onTapToConfirmViewAnio() {
        //  self.numDocument = self.numberDocumentTextField.text ?? ""
        // self.presenter.validateData()
        arrNuSecu = [String]()
        arrImPagiSIR = [String]()
        arrNuAsieSelectSARP = [String]()
        nuAsieSelectSARP = "TODAS"
        imPagiSIR = "TODAS"
        print("todas")
        
        var selectTotal = 0
        var selectExoTotal = 0
        var totalPaginas = 0
        selectorAllPages.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        selectorCleanSelection.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        if areaRegId == "24000"{
            let count = self.listaValidaPartidaLiteralRMCEntity.count
            
            if (count > 0) {
                for i in 0...count - 1 {
                    self.listaValidaPartidaLiteralRMCEntity[i].select = true
                    self.listaValidaPartidaLiteralRMCEntity[i].todas = true
                    let numPagina: Int = Int(self.listaValidaPartidaLiteralRMCEntity[i].numPagina)!
                    if self.listaValidaPartidaLiteralRMCEntity[i].fgExonPub1 == "0"{
                        selectTotal += numPagina
                    }else{
                        selectExoTotal += numPagina
                    }
                    totalPaginas += numPagina
                    arrNuAsieSelectSARP.append(self.listaValidaPartidaLiteralRMCEntity[i].nsAsiento)
                }
            }
            self.countLabel.text = String(selectTotal)
        }else{
            let count = self.listaValidaPartidaLiteralEntity.count
            if (count > 0) {
                for i in 0...count - 1 {
                    self.listaValidaPartidaLiteralEntity[i].select = true
                    self.listaValidaPartidaLiteralEntity[i].todas = true
                     if self.listaValidaPartidaLiteralEntity[i].flagExonerado == "0"{
                         selectTotal += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                     }else{
                         selectExoTotal += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                     }
                    totalPaginas += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                    
                    if self.listaValidaPartidaLiteralEntity[i].numeroPartida.starts(with:"P") {
                        arrNuAsieSelectSARP.append(self.listaValidaPartidaLiteralEntity[i].detalle)
                    }
                    else {
                        nuSecu = self.listaValidaPartidaLiteralEntity[i].nuSecu
                        arrNuSecu.append(nuSecu)
                        arrImPagiSIR.append(self.listaValidaPartidaLiteralEntity[i].paginas)
                    }
                }
            }
            self.countLabel.text = String(count)
        }
        
        self.titleMontoLabel.text = "Calcular"
        self.cantiMontoLabel.text = ""
        
        
        self.countLabel.text = String(selectTotal)
        self.countExoLabel.text = String(selectExoTotal)
        self.totalPaginasPartidaFicha = String(totalPaginas)
        
        isButtonCalcular = false
        //self.countView.isHidden = true
        self.tableSearchView.reloadData()
    }
    
  
    @objc private func onTapToConfirmViewNum() {
     //   self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        arrNuSecu = [String]()
        arrImPagiSIR = [String]()
        arrNuAsieSelectSARP = [String]()
        nuAsieSelectSARP = ""
        imPagiSIR = ""
        var selectTotal = 0
        var selectExoTotal = 0
        var totalPaginas = 0
        
        selectorAllPages.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        selectorCleanSelection.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        if areaRegId == "24000"{
            let count = self.listaValidaPartidaLiteralRMCEntity.count
            let selectTotal = 0
            if (count > 0) {
                for i in 0...count - 1 {
                    self.listaValidaPartidaLiteralRMCEntity[i].select = false
                    self.listaValidaPartidaLiteralRMCEntity[i].todas = false
                    let numPagina: Int = Int(self.listaValidaPartidaLiteralRMCEntity[i].numPagina)!
                    totalPaginas += numPagina
                }
            }
            self.countLabel.text = String(selectTotal)
        }else{
            let count = self.listaValidaPartidaLiteralEntity.count
            if (count > 0) {
                for i in 0...count - 1 {
                    self.listaValidaPartidaLiteralEntity[i].select = false
                    self.listaValidaPartidaLiteralEntity[i].todas = false
                    totalPaginas += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                }
            }
            self.countLabel.text = String(selectTotal)
        }
        //self.countView.isHidden = true
        
        self.titleMontoLabel.text = "Calcular"
        
        self.cantiMontoLabel.text = ""
        
        self.countLabel.text = String(selectTotal)
        self.countExoLabel.text = String(selectExoTotal)
        self.totalPaginasPartidaFicha = String(totalPaginas)
        isButtonCalcular = false
        self.tableSearchView.reloadData()

    }
    
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }

    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    private func setupDesigner() {
       
        formView.backgroundCard()
       
        //obtiene lista registro juridico
       // self.presenter.validarRegistroJuridico()
     
        toConfirmView.primaryButton()

          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: CertiCalculoLiteralDetailPartidaViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: CertiCalculoLiteralDetailPartidaViewCell.reuseIdentifier)
        
        
        let profileCellSearchNibPlaca = UINib(nibName: CertiCalculoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNibPlaca, forCellReuseIdentifier: CertiCalculoLiteralDetailPartidaPlacaViewCell.reuseIdentifier)
                
    }
    
    func loadResultados(busquedaResponse: [GetDetalleAsientoEntity]) {
        self.listaValidaPartidaLiteralEntity = busquedaResponse
        self.loaderView(isVisible: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaValidaPartidaLiteralEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        self.loaderView(isVisible: false)
    }
    
    func loadResultadosRMC(busquedaResponse: [GetDetalleAsientoRMCEntity]) {
        self.listaValidaPartidaLiteralRMCEntity = busquedaResponse
        self.loaderView(isVisible: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaValidaPartidaLiteralRMCEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        self.loaderView(isVisible: false)
    }
    
    func loadResultadosCalculo(busquedaResponse: String) {
       // self.listaValidaPartidaLiteralRMCEntity = busquedaResponse
        
        print("CCALCULO::>>>",busquedaResponse)
        var monto = Double("0")
        if busquedaResponse != "" {
            monto = Double(busquedaResponse)
        }
        var strMonto = ""
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        strMonto = formatter.string(from: monto! as NSNumber) ?? ""


        self.cantiMontoLabel.text = strMonto
        
        self.titleMontoLabel.text = "Solicitar"
        isButtonCalcular = true
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
    }
    

    @objc private func onTapToConfirmView() {
        
        if self.countLabel.text == "0" && self.countExoLabel.text == "0" {
            countPagina = "0"
            self.presenter.validateRealizarCheck()
        }else{
            countPagina = "1"
            
            if isButtonCalcular == false{
                self.presenter.calculoCantPartidaLiteral()
            }else{
                print("TTrue paso solicitar")
                let storyboard = UIStoryboard(name: "Payments", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
                
                let usuario = UserPreferencesController.usuario()
                
                // let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
                //let vc = storyboard.instantiateViewController(withIdentifier: "PagarBusquedaLiteralViewController") as! PagarBusquedaLiteralViewController
                // vc.array = users
                // let usuario = UserPreferencesController.usuario()
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.ipRemote = ""
                vc.paymentItem.sessionId = ""
                vc.paymentItem.usrId = "\(usuario.idUser)"
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.partida =  numPartida
                
                
                var costoServicio = self.cantiMontoLabel.text ?? ""
                costoServicio = costoServicio.replacingOccurrences(of: ",", with: "")
                print("costoServicio = ", costoServicio)
                vc.paymentItem.costoTotal = costoServicio
                vc.paymentItem.costoServicio = costoServicio
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.titleBar = titleBar
                
                print("titleCerti2 = ", titleCerti)
                print("titleBar2 = ", titleBar)
                vc.montoCalc =  costoServicio
                vc.countCantPag =  self.countLabel.text ?? ""
                vc.countCantPagExo =  self.countExoLabel.text ?? ""
                vc.codLibro =  codLibro
                vc.numPartida =  numPartida
                vc.fichaId =  fichaId
                vc.tomoId =  tomoId
                vc.fojaId =  fojaId
                vc.ofiSARP =  ofiSARP
                vc.coServicio =  coServicio
                vc.coTipoRegis =  coTipoRegis
                if imPagiSIR == "TODAS" {
                    vc.nuSecu = ""
                    vc.imPagiSIR = imPagiSIR
                }
                else {
                    vc.nuSecu =  arrNuSecu.joined(separator: ",")
                    vc.imPagiSIR =  arrImPagiSIR.joined(separator: ",")
                }
                if nuAsieSelectSARP == "TODAS" {
                    vc.nuAsieSelectSARP = nuAsieSelectSARP
                }
                else {
                    vc.nuAsieSelectSARP =  arrNuAsieSelectSARP.joined(separator: ",")
                }
                vc.paymentItem.totalPaginasPartidaFicha = self.totalPaginasPartidaFicha
                vc.paymentItem.cantPaginas = vc.countCantPag
                vc.paymentItem.cantPaginasExon = vc.countCantPagExo
                
                vc.paymentItem.codLibro = vc.codLibro
                vc.paymentItem.nuAsieSelectSARP = vc.nuAsieSelectSARP
                vc.paymentItem.imPagiSIR = vc.imPagiSIR
                vc.paymentItem.nuSecuSIR = vc.nuSecu
                
                vc.valoficinaOrigen = valoficinaOrigen
                vc.certificadoId = certificadoId
                print("titleBar = ", titleBar)
                vc.titleBar = titleBar
                vc.refNumPart = refNumPart
                vc.codArea = codArea
                vc.numeroPlaca = numeroPlaca
                vc.codigoGla = codigoGla
                print("codigoGla = ", codigoGla)
                vc.titleText = titleLabel.text ?? "Sin definir"
              //  vc.tipoDocumPartic =  tipoDocumPartic
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz // "\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
        
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
        var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente
        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
            VisaNet.shared.delegate = self
        }
    }
    
    
    
    func goToConfirmPasswordpag(_ state: Bool, message: String) {
        if (state) {
        }else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                self.presenter.listaValidaPartidaLiteral()
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
 
    func changeTypeGender(index: Int) {
        
        
        self.coServ = self.servicesLista[index].coServ
        self.coTipoRgst = self.servicesLista[index].coTipoRgst
        
        print("coServ:>>",coServ)
        if (self.coServ == "37") {
            self.toConfirmViewNum.isHidden = true
        }else{
            self.toConfirmViewNum.isHidden = false
        }
    }
    
    
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    func loadService(serviceResponse: [ServicioSunarEntity]) {
        self.servicesLista = serviceResponse
        //self.loaderView(isVisible: false)
       
     
        
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
    }
    
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    

        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
   
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
}




// MARK: - Table view datasource
extension CertiCalculoLiteralDetailPatidaViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
       // print("couunt:>>",self.listaValidaPartidaLiteralEntity.count)
        
        if areaRegId == "24000"{
            return self.listaValidaPartidaLiteralRMCEntity.count;
        }else{
            return self.listaValidaPartidaLiteralEntity.count;
        }
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 265
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if areaRegId == "24000"{
            
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertiCalculoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, for: indexPath) as? CertiCalculoLiteralDetailPartidaPlacaViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            var listaPro = listaValidaPartidaLiteralRMCEntity[indexPath.row]
            
            listaPro.intValCount = indexPath.row
            listaPro.count = self.listaValidaPartidaLiteralRMCEntity.count
            print("listaValidaPartidaLiteralEntity:>>",listaPro)
            cell.setup(with: &listaPro)
            cell.intVal = self.listaValidaPartidaLiteralRMCEntity.count
            if nuAsieSelectSARP == "TODAS" {
                cell.checkView.isUserInteractionEnabled = false
            }
            else {
                cell.checkView.isUserInteractionEnabled = true
            }

            cell.delegate = self

            return cell
        }else{
        
          /*  if indexPath.section == 0 {
                print("section1")
            } else {
             */
                guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertiCalculoLiteralDetailPartidaViewCell.reuseIdentifier, for: indexPath) as? CertiCalculoLiteralDetailPartidaViewCell else {
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                cell.separatorInset = .zero
                //cell.item = self.listaPropiedadEntities[indexPath.row]
        
                var asientoUnit = listaValidaPartidaLiteralEntity[indexPath.row]
                print("listaValidaPartidaLiteralEntity:>>",asientoUnit)
                asientoUnit.intValCount = indexPath.row
                asientoUnit.count = self.listaValidaPartidaLiteralEntity.count
                cell.setup(with: &asientoUnit)
                cell.intVal = self.listaValidaPartidaLiteralEntity.count
                print("CELL.INTVAL>>",cell.intVal)
            
                
                print("INTVAL COUNT>>",asientoUnit.intValCount)
                cell.delegate = self

                return cell
           // }
        }
    }
}

// MARK: - Table view delegate
extension CertiCalculoLiteralDetailPatidaViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}



extension CertiCalculoLiteralDetailPatidaViewController: CertiCalculoLiteralDetailPartidaCellDelegate {
    
    func onTapSelect(_ zonaCell: CertiCalculoLiteralDetailPartidaViewCell, item: GetDetalleAsientoEntity) {
        print("CertiCalculoLiteralDetailPartidaCellDelegate")
        let count = self.listaValidaPartidaLiteralEntity.count
        var selectTotal = 0
        var selectExoTotal = 0
        var totalPaginas = 0
        arrNuSecu = [String]()
        arrImPagiSIR = [String]()
        arrNuAsieSelectSARP = [String]()
        nuAsieSelectSARP = ""
        imPagiSIR = ""
        if (count > 0) {
            for i in 0...count - 1 {
                
                print("coZonaRegi::>",self.listaValidaPartidaLiteralEntity[i].coZonaRegi)
                print("item.coZonaRegi::>",item.coZonaRegi)
                if (self.listaValidaPartidaLiteralEntity[i].detalle == item.detalle) {
                    self.listaValidaPartidaLiteralEntity[i].select = item.select
                }
                let select = self.listaValidaPartidaLiteralEntity[i].select
                 if (select) {
                     if self.listaValidaPartidaLiteralEntity[i].flagExonerado == "0"{
                         selectTotal += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                     }else{
                         selectExoTotal += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
                     }
                     if self.listaValidaPartidaLiteralEntity[i].numeroPartida.starts(with:"P") {
                         arrNuAsieSelectSARP.append(self.listaValidaPartidaLiteralEntity[i].detalle)
                     }
                     else {
                         nuSecu = self.listaValidaPartidaLiteralEntity[i].nuSecu
                         arrNuSecu.append(nuSecu)
                         arrImPagiSIR.append(self.listaValidaPartidaLiteralEntity[i].paginas)
                     }
                     
                 }
                totalPaginas += self.listaValidaPartidaLiteralEntity[i].nuPaginasCantidad
            }
        }

        print("selectTotal::>",selectTotal)
        print("selectExoTotal::>",selectExoTotal)
        self.countLabel.text = String(selectTotal)
        self.countExoLabel.text = String(selectExoTotal)
        self.totalPaginasPartidaFicha = String(totalPaginas)
        if (selectTotal > 0) {
           // self.countView.isHidden = false
        } else {
           // self.countView.isHidden = true
        }
        self.titleMontoLabel.text = "Calcular"
        
        self.cantiMontoLabel.text = ""
        isButtonCalcular = false
        
    }
    
}



extension CertiCalculoLiteralDetailPatidaViewController: CertiCalculoLiteralDetailPartidaPlacaCellDelegate {
    
    func onTapSelect(_ zonaCell: CertiCalculoLiteralDetailPartidaPlacaViewCell, item: GetDetalleAsientoRMCEntity) {
        print("CertiCalculoLiteralDetailPartidaPlacaCellDelegate")
        let count = self.listaValidaPartidaLiteralRMCEntity.count
        var selectTotal = 0
        var selectExoTotal = 0
        var totalPaginas = 0
        arrNuSecu = [String]()
        arrImPagiSIR = [String]()
        arrNuAsieSelectSARP = [String]()
        nuAsieSelectSARP = ""
        imPagiSIR = ""
        if (count > 0) {
            for i in 0...count - 1 {
                if (self.listaValidaPartidaLiteralRMCEntity[i].nsAsiento == item.nsAsiento) {
                    self.listaValidaPartidaLiteralRMCEntity[i].select = item.select
                }
                let select = self.listaValidaPartidaLiteralRMCEntity[i].select
                let numPagina: Int = Int(self.listaValidaPartidaLiteralRMCEntity[i].numPagina)!
                if (select) {
                    if item.fgExonPub1 == "0"{
                        selectTotal += numPagina
                    }else{
                        selectExoTotal += numPagina
                    }
                    arrNuAsieSelectSARP.append(self.listaValidaPartidaLiteralRMCEntity[i].nsAsiento)
                }
                totalPaginas += numPagina
            }
        }
        
        self.countLabel.text = String(selectTotal)
        self.countExoLabel.text = String((selectExoTotal))
        self.totalPaginasPartidaFicha = String(totalPaginas)
        if (selectTotal > 0) {
           // self.countView.isHidden = false
        } else {
           // self.countView.isHidden = true
        }
        self.titleMontoLabel.text = "Calcular"
        
        self.cantiMontoLabel.text = ""
        isButtonCalcular = false
    }
    
}


extension String {
    func replace(_ string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }

    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
}

//MARK: - VISANET DELEGATE SECTION

extension CertiCalculoLiteralDetailPatidaViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
