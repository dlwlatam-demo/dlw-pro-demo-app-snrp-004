//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class CertiCalculoLiteralDetailPartidaPlacaViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "CertiCalculoLiteralDetailPartidaPlacaViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    
    @IBOutlet weak var checkView: UIView!
    
    weak var delegate : CertiCalculoLiteralDetailPartidaPlacaCellDelegate?
    
    @IBOutlet weak var checkImage: UIImageView!
    
    
    @IBOutlet var cantCheckLabel:UILabel!
    
    @IBOutlet var tituloLabel:UILabel!
    @IBOutlet var conceptoLabel:UILabel!
    @IBOutlet var nrAsientoLabel:UILabel!
    @IBOutlet var exoneradoLabel:UILabel!
    @IBOutlet var cantPagiLabel:UILabel!
    
    
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var intVal = Int()
    var item: [ConsultaPropiedadListEntity] = []
    
    
    private var zonaItem: GetDetalleAsientoRMCEntity!
 
    func setup(with solicitud: inout GetDetalleAsientoRMCEntity) {
        
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
      print("ofiiicina:>>",solicitud.codigoSede)
        print("solicitud.select:>>",solicitud.select)
        
        conceptoLabel?.text = solicitud.descripcionConcepto
        tituloLabel?.text = solicitud.anioTitulo + " - \(solicitud.numTitulo)"
        nrAsientoLabel?.text = solicitud.nsAsiento
        if solicitud.fgExonPub1 == "0" {
            exoneradoLabel?.text = "NO"
        }else{
            exoneradoLabel?.text = "SI"
        }
        cantPagiLabel?.text = solicitud.numPagina
        solicitud.intValCount += 1
        cantCheckLabel?.text = "\(String(solicitud.intValCount))/\(String(solicitud.count))"
        
        self.zonaItem = solicitud
        if (solicitud.select) {
            self.checkImage.image = UIImage(named: "Checked")
        } else {
            self.checkImage.image = UIImage(named: "Check")
        }
        let onTapCell = UITapGestureRecognizer(target: self, action: #selector(onTapItemCell))
        self.checkView.addGestureRecognizer(onTapCell)
    }
    
    @objc func onTapItemCell(){
        if !zonaItem.todas {
            if (zonaItem.select) {
                self.checkImage.image = UIImage(named: "Check")
            } else {
                self.checkImage.image = UIImage(named: "Checked")
            }
            self.zonaItem.select = !self.zonaItem.select
            self.delegate?.onTapSelect(self, item: self.zonaItem)
        }
    }
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
      
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

protocol CertiCalculoLiteralDetailPartidaPlacaCellDelegate: AnyObject {
    
  func onTapSelect(_ zonaCell: CertiCalculoLiteralDetailPartidaPlacaViewCell, item: GetDetalleAsientoRMCEntity)
    
}
