//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class CertiCalculoLiteralDetailPartidaViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "CertiCalculoLiteralDetailPartidaViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    
    @IBOutlet weak var checkView: UIView!
    
    weak var delegate : CertiCalculoLiteralDetailPartidaCellDelegate?
    
    @IBOutlet weak var checkImage: UIImageView!
    
    
    @IBOutlet var cantCheckLabel:UILabel!
    
    @IBOutlet var actoLabel:UILabel!
    @IBOutlet var tipoAsientoLabel:UILabel!
    @IBOutlet var nrAsientoLabel:UILabel!
    @IBOutlet var exoneradoLabel:UILabel!
    @IBOutlet var cantPagiLabel:UILabel!
    @IBOutlet var pagPortadasLabel:UILabel!
    
    
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var intVal = Int()
    var varApoyo = Int()
    
    
    
    private var zonaItem: GetDetalleAsientoEntity!
 
    func setup(with asientoToShow: inout GetDetalleAsientoEntity) {
        
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
        print("ofiiicina:>>",asientoToShow.desOficina)
        print("solicitud:>>",asientoToShow.select)
        print("intValCount---:>>",asientoToShow.intValCount+1)
        
        
        actoLabel?.text = asientoToShow.desActo
        tipoAsientoLabel?.text = asientoToShow.desAsiento
        nrAsientoLabel?.text = asientoToShow.detalle
        if asientoToShow.flagExonerado == "0"{
            exoneradoLabel?.text = "NO"
        }else{
            exoneradoLabel?.text = "SI"
        }
        asientoToShow.intValCount += 1
        
        cantPagiLabel?.text = String(asientoToShow.nuPaginasCantidad)
        pagPortadasLabel?.text = asientoToShow.paginas
        cantCheckLabel?.text = "\(String(asientoToShow.intValCount))/\(String(asientoToShow.count))"
        
        
        self.zonaItem = asientoToShow
        if (asientoToShow.select) {
            self.checkImage.image = UIImage(named: "Checked")
        } else {
            self.checkImage.image = UIImage(named: "Check")
        }
        let onTapCell = UITapGestureRecognizer(target: self, action: #selector(onTapItemCell))
        self.checkView.addGestureRecognizer(onTapCell)
    }
    
    @objc func onTapItemCell(){
        if !zonaItem.todas {
            if (zonaItem.select) {
                self.checkImage.image = UIImage(named: "Check")
            } else {
                self.checkImage.image = UIImage(named: "Checked")
            }
            self.zonaItem.select = !self.zonaItem.select
            
            self.delegate?.onTapSelect(self, item: self.zonaItem)
        }
    }
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        //viewCell.backgroundColor = UIColor.white
        //viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor =
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

protocol CertiCalculoLiteralDetailPartidaCellDelegate: AnyObject {
    
  func onTapSelect(_ zonaCell: CertiCalculoLiteralDetailPartidaViewCell, item: GetDetalleAsientoEntity)
    
}
