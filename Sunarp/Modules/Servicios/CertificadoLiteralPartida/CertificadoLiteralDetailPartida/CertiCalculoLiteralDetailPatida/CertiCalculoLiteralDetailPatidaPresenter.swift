//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class CertiCalculoLiteralDetailPatidaPresenter {
    
    private weak var controller: CertiCalculoLiteralDetailPatidaViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var modelTive: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()

    
    init(controller: CertiCalculoLiteralDetailPatidaViewController) {
        self.controller = controller
    }
    
}

extension CertiCalculoLiteralDetailPatidaPresenter: GenericPresenter {
    
    func willAppear() {
     //   self.controller?.loaderView(isVisible: true)
        self.model.getListaOficinaRegistral{ (arrayOficinas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
  
    
    func listaValidaPartidaLiteral() {
        /*
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let codLibro = controller?.codLibro ?? ""
        let numPartida = controller?.numPartida ?? ""
        let fichaId = controller?.fichaId ?? ""
        let tomoId = controller?.tomoId ?? ""
        let fojaId = controller?.fojaId ?? ""
        let ofiSARP = controller?.ofiSARP ?? ""
        let coServicio = controller?.coServicio ?? ""
        let coTipoRegis = controller?.coTipoRegis ?? ""
        */
        
        
         /*
         let regPubId =  "01"
         let oficRegId =  "01"
         let codLibro =  "001"
         let numPartida =  "55012368"
         var fichaId = controller?.fichaId ?? ""
         if (fichaId == ""){
             fichaId = "-"
         }
         var tomoId = controller?.tomoId ?? ""
         if (tomoId == ""){
             tomoId = "-"
         }
         var fojaId = controller?.fojaId ?? ""
         if (fojaId == ""){
             fojaId = "-"
         }
         var ofiSARP = controller?.ofiSARP ?? ""
         if (ofiSARP == ""){
             ofiSARP = "-"
         }
         let coServicio =  "2"
         let coTipoRegis =  "001"
         */
        
        var regPubId = controller?.regPubId ?? ""
        if (regPubId == ""){
            regPubId = "-"
        }
        var oficRegId = controller?.oficRegId ?? ""
        if (oficRegId == ""){
            oficRegId = "-"
        }
        var codLibro = controller?.codLibro ?? ""
        if (codLibro == ""){
            codLibro = "-"
        }
        var numPartida = controller?.numPartida ?? ""
        if (numPartida == ""){
            numPartida = "-"
        }
        var fichaId = controller?.fichaId ?? ""
        if (fichaId == ""){
            fichaId = "-"
        }
        var tomoId = controller?.tomoId ?? ""
        if (tomoId == ""){
            tomoId = "-"
        }
        var fojaId = controller?.fojaId ?? ""
        if (fojaId == ""){
            fojaId = "-"
        }
        var ofiSARP = controller?.ofiSARP ?? ""
        if (ofiSARP == ""){
            ofiSARP = "-"
        }
        let coServicio = controller?.coServicio ?? ""
        let coTipoRegis = controller?.coTipoRegis ?? ""
        
        self.controller?.loaderView(isVisible: true)
        self.model.getDetalleAsientosLiteral(regPubId: regPubId,oficRegId: oficRegId,codLibro: codLibro,numPartida: numPartida,fichaId: fichaId,tomoId: tomoId,fojaId: fojaId,ofiSARP: ofiSARP,coServicio: coServicio,coTipoRegis: coTipoRegis){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultados(busquedaResponse: arrayBusqTive)
        }
    }
        
    func listaValidaPartidaLiteralRMCTrue() {
        /*
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let codLibro = controller?.codLibro ?? ""
        let numPartida = controller?.numPartida ?? ""
        let fichaId = controller?.fichaId ?? ""
        let tomoId = controller?.tomoId ?? ""
        let fojaId = controller?.fojaId ?? ""
        let ofiSARP = controller?.ofiSARP ?? ""
        let coServicio = controller?.coServicio ?? ""
        let coTipoRegis = controller?.coTipoRegis ?? ""
        */
        
    
        
        let codZona = controller?.codZona ?? ""
        let codOficina = controller?.codOficina ?? ""
        let numPartida = controller?.numPartida ?? ""
        
        //let codZona =  "01"
       // let codOficina =  "01"
       // let numPartida =  "51524167"
        
            
        self.controller?.loaderView(isVisible: true)
        self.model.getDetalleAsientosLiteralRMCTrue(codZona: codZona,codOficina: codOficina,numPartida: numPartida){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultadosRMC(busquedaResponse: arrayBusqTive)
        }
    }
    func listaValidaPartidaLiteralRMCFalse() {
        /*
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let codLibro = controller?.codLibro ?? ""
        let numPartida = controller?.numPartida ?? ""
        let fichaId = controller?.fichaId ?? ""
        let tomoId = controller?.tomoId ?? ""
        let fojaId = controller?.fojaId ?? ""
        let ofiSARP = controller?.ofiSARP ?? ""
        let coServicio = controller?.coServicio ?? ""
        let coTipoRegis = controller?.coTipoRegis ?? ""
        */
        
        
       // vc.codZona = listaPro.codZona
       // vc.codOficina = listaPro.codOficina
      //  vc.numeroPlaca = listaPro.numeroPlaca
        
        
        let codZona = controller?.codZona ?? ""
        let codOficina = controller?.codOficina ?? ""
        let numPartida = controller?.numPartida ?? ""
        let numPlaca = controller?.numeroPlaca ?? ""
        //numPlaca = Trim(numPlaca)
       // let codZona =  "01"
       // let codOficina =  "01"
       // let numPartida =  "51524167"
       // let numPlaca =  "CIM057"
        
        self.controller?.loaderView(isVisible: true)
        self.model.getDetalleAsientosLiteralRMCFalse(codZona: codZona,codOficina: codOficina,numPartida: numPartida,numPlaca:numPlaca ){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultadosRMC(busquedaResponse: arrayBusqTive)
        }
    }
    func calculoCantPartidaLiteral() {
        /*
         
         
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let codLibro = controller?.codLibro ?? ""
        let numPartida = controller?.numPartida ?? ""
        let fichaId = controller?.fichaId ?? ""
        let tomoId = controller?.tomoId ?? ""
        let fojaId = controller?.fojaId ?? ""
        let ofiSARP = controller?.ofiSARP ?? ""
        let coServicio = controller?.coServicio ?? ""
        let coTipoRegis = controller?.coTipoRegis ?? ""
         
         
        */
        print("Cal_coServicio::>>",controller?.coServicio ?? "")
        let coServicio =  controller?.coServicio ?? ""
        let cantPaginas =  controller?.countLabel.text ?? ""
          
        self.controller?.loaderView(isVisible: true)
        self.model.getDetalleAsientosLiteralCalculo(coServicio: coServicio,cantPaginas: cantPaginas){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultadosCalculo(busquedaResponse: arrayBusqTive )
            
            print(":>>>-arrayBusqTive",arrayBusqTive)
        }
    }
    
    func listaService() {
        
        let areaRegId = controller?.areaRegId ?? ""
       // self.controller?.loaderView(isVisible: true)
        print("areaRegId::>>",areaRegId)
        self.model.getListaTipoServicioSunarp(areaRegId: areaRegId){ (arrayService) in
           // self.controller?.loaderView(isVisible: false)
            self.controller?.loadService(serviceResponse: arrayService)
        }
    }
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
      //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
       //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
       // }
    }
    
    func validarRegistroJuridico() {
        self.controller?.loaderView(isVisible: true)
     
        self.model.getListaJuridicos() { (arrayRegistroJuridico) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoRegistroJuridico(arrayRegistroJuridico: arrayRegistroJuridico)
           // let state = arrayTipoDocumentos.msgResult.isEmpty
           // self.controller?.loadDatosDni(state, message: arrayTipoDocumentos.msgResult, jsonValidacionDni: arrayTipoDocumentos)
        }
    }
    
    func validarTipoCertificado(tipoCertificado:String) {
        self.controller?.loaderView(isVisible: true)
        
        self.model.getListaTipoCertificado(tipoCertificado:tipoCertificado) { (arrayTipoCertificado) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoCerficado(arrayTipoCerficado: arrayTipoCertificado)
          
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
        
            self.controller?.loaderView(isVisible: false)
     /*   self.model.postValidarCe(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }*/
    }
    
    func validateData() {
       
        let numPart = self.controller?.numPart ?? ""
        let regPubId = self.controller?.regPubId ?? ""
        let coServ = self.controller?.coServ ?? ""
       
        
        
        if (numPart.isEmpty || regPubId.isEmpty || coServ.isEmpty ) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
    func validateRealizarCheck() {
       
        let countPagina = self.controller?.countPagina ?? ""
        let countPaginaExon = self.controller?.countPaginaExon ?? ""
      
        if (countPagina == "0" && countPaginaExon == "0") {
            self.controller?.goToConfirmPasswordpag(false, message: "Seleccione un asiento de la lista.")
        }
        
    }
    
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        print("getVisaKeys")
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        print("getTokenNiubiz")
        print(urlVisa)
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        print("getNiubizPinHash")
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId() {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }
}
