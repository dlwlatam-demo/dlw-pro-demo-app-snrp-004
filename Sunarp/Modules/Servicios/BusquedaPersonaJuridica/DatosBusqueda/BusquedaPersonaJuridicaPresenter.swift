//
//  BusquedaPersonaJuridicaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/10/22.
//

import Foundation

class BusquedaPersonaJuridicaPresenter {
    
    private weak var controller: BusquedaPersonaJuridicaViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var serviceModel: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    init(controller: BusquedaPersonaJuridicaViewController) {
        self.controller = controller
    }
    
}

extension BusquedaPersonaJuridicaPresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getListaOficinaRegistral{ (arrayOficinas) in
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    func buscarOficinas(oficina: String, razon: String, siglas: String) {
        // self.controller?.loaderView(isVisible: true)
        self.serviceModel.getBusquedaJuridica(nombre: razon.uppercased(), nombreOficinaRegistral: oficina, siglas: siglas){ (arrayOficinas) in
            self.controller?.loadResultados(busquedaResponse: arrayOficinas)
        }
    }
            
}
