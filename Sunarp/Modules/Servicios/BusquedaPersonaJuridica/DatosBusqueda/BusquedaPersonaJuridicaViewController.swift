//
//  BusquedaPersonaJuridicaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation
import UIKit

class BusquedaPersonaJuridicaViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var searchButtonView: UIView!
    @IBOutlet weak var oficinaRegistralText: UITextField!
    @IBOutlet weak var razonDenominacionText: UITextField!
    @IBOutlet weak var siglasText: UITextField!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var todasLabel: UILabel!
    var oficinaRegistralPickerView = UIPickerView()
    var loading: UIAlertController!
    
    var indexSelect = 0
    var valDone: Bool = true
    
    var isChecked: Bool = false
    var oficinas: [OficinaRegistralEntity] = []
    var resultado: [PersonaJuridicaEntity] = []
    var style: Style = Style.myApp
    
    private lazy var presenter: BusquedaPersonaJuridicaPresenter = {
       return BusquedaPersonaJuridicaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Personas Jurídicas")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.oficinaRegistralText.borderAndPaddingLeftAndRight()
        self.razonDenominacionText.borderAndPaddingLeftAndRight()
        self.razonDenominacionText.delegate = self
        self.razonDenominacionText.maxLength = 100
        
        self.siglasText.borderAndPaddingLeftAndRight()
        self.siglasText.delegate = self
        self.siglasText.maxLength = 50
        self.checkView.borderCheckView()
        self.searchButtonView.primaryButton()
        
        self.isChecked = false
        
        self.oficinaRegistralPickerView.delegate = self
        self.oficinaRegistralPickerView.dataSource = self
        self.oficinaRegistralText.inputView = self.oficinaRegistralPickerView
        self.oficinaRegistralText.inputAccessoryView = self.createToolbar()
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    @objc func donePressed() {
        if valDone == true {
            
            self.oficinaRegistralText.text = oficinas[indexSelect].Nombre
            self.oficinaRegistralText.resignFirstResponder()
        }
    }
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkView.addGestureRecognizer(tapChecked)
        
        let tapTerminoCondicionesGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.todasLabel.addGestureRecognizer(tapTerminoCondicionesGesture)
        
        let tapSearchGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchView))
        self.searchButtonView.addGestureRecognizer(tapSearchGesture)
        
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @objc private func onTapSearchView(){
        let oficinaSelect = self.oficinaRegistralText.text ?? .empty
        let razon = self.razonDenominacionText.text ?? .empty
        let siglas = self.siglasText.text ?? .empty
        
        if (!isChecked && oficinaSelect.isEmpty) {
            showAlertSearch(message: "Seleccione una oficina")
            return
        }
        
        if(razon.isEmpty){
            showAlertSearch(message: "Debe de ingresar una Razón o Denominación")
            return
        }
        
        presenter.buscarOficinas(oficina: oficinaSelect, razon: razon, siglas: siglas)
    }
    
    func showAlertSearch(message: String) {
        let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc private func onTapChecked() {
        self.isChecked = !self.isChecked
        if (isChecked) {
            self.checkImage.image = UIImage(systemName: "checkmark")
            self.oficinaRegistralText.text = .empty
            self.oficinaRegistralText.placeholder = "seleccione"
            self.oficinaRegistralText.isEnabled = false
        } else {
            self.oficinaRegistralText.isEnabled = true
            self.checkImage.image = nil
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.oficinaRegistralText.resignFirstResponder()
        self.razonDenominacionText.resignFirstResponder()
        self.siglasText.resignFirstResponder()
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadResultados(busquedaResponse: [PersonaJuridicaEntity]) {
        self.resultado = busquedaResponse
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // self.loaderView(isVisible: false)
            if (self.resultado.count == 0 || self.resultado.isEmpty) {
                self.showAlertSearch(message: "No se encontraron coincidencias")
                
            } else {
                let storyboard = UIStoryboard(name: "BusquedaPersonaJuridica", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ResultadoBusquedaViewController") as! ResultadoBusquedaViewController
                vc.setResultado(resultado: self.resultado)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension BusquedaPersonaJuridicaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.oficinas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.oficinas[row].Nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (!oficinas.isEmpty) {
            
            
            valDone = true
            self.indexSelect = row
            
        }
        
    }
}
 

extension BusquedaPersonaJuridicaViewController: UITextFieldDelegate {
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
