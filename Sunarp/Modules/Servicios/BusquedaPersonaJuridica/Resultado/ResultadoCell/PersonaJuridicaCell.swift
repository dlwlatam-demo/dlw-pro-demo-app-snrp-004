//
//  PersonaJuridicaCell.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/10/22.
//

import UIKit

class PersonaJuridicaCell: UITableViewCell {
    
    @IBOutlet weak var partidaLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var oficinaLabel: UILabel!
    @IBOutlet weak var siglasLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    private var personaItem: PersonaJuridicaEntity!
    
    static let identifier = "PersonaJuridicaCell"
    
    func setup(with item: PersonaJuridicaEntity) {
        self.personaItem = item
        self.partidaLabel.text = item.partida
        self.nombreLabel.text = item.razon
        self.oficinaLabel.text = item.oficina
        self.siglasLabel.text = item.siglas
    }
    
}
