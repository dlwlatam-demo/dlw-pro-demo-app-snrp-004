//
//  ResultadoBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/10/22.
//

import Foundation
import UIKit

class ResultadoBusquedaViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var filterText: UITextField!
    @IBOutlet weak var resultadoTable: UITableView!
    @IBOutlet weak var totalResult: UILabel!
    
    var loading: UIAlertController!
        
    var resultado: [PersonaJuridicaEntity] = []
    var resultadoFiltered: [PersonaJuridicaEntity] = []
    var style: Style = Style.myApp
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Personas Jurídicas")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.filterText.borderAndPaddingLeftAndRight()
        
        self.resultadoTable.register(UINib(nibName: PersonaJuridicaCell.identifier, bundle: App.bundle), forCellReuseIdentifier: PersonaJuridicaCell.identifier)
        self.resultadoTable.delegate = self
        self.resultadoTable.dataSource = self
    }
    
    func setResultado(resultado: [PersonaJuridicaEntity]) {
        self.resultado = resultado
        self.resultadoFiltered = resultado
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        filterText.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.filterText.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let filter = textField.text ?? ""
        if (filter.isEmpty) {
            self.resultadoFiltered = self.resultado
            self.totalResult.text = "Total de registros: \(resultadoFiltered.count)"
        } else {
            self.resultadoFiltered = self.resultado.filter { $0.razon.uppercased().contains(filter.uppercased())}
            self.totalResult.text = "Total de registros: \(resultadoFiltered.count)"
        }
        self.resultadoTable.reloadData()
    }
}

extension ResultadoBusquedaViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.totalResult.text = "Total de registros: \(resultadoFiltered.count)"
        return self.resultadoFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = resultadoFiltered[indexPath.row]
        let cell = self.resultadoTable.dequeueReusableCell(withIdentifier: PersonaJuridicaCell.identifier, for: indexPath) as! PersonaJuridicaCell
        cell.setup(with: item)
        return cell
    }
}
