//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadCertiRegistralViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var numDocText: SDCTextField!
    @IBOutlet weak var textoLabel: UILabel!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var maxLength = 0
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var indexSelectOffice = 0
    var indexSelect = 0
    var valDone: Bool = true
    
    var validaPartida: [ValidaPartidaCRIEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var titleBar: String = ""
    
    var areaRegId: String = ""
    var refNumPartMP: String = ""
    var ficha: String = ""
    var precOfic: String = ""
    
    var partida: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var tipoPartidaFicha: String = ""
    var tipoCRI: String = ""
    
    var codGrupoLibroArea: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
   
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    //partida ficha
    @IBOutlet weak var toConfirmViewPartida: UIView!
    @IBOutlet weak var toConfirmViewFicha: UIView!
    @IBOutlet weak var imgRadioButtonPartida: UIImageView!
    @IBOutlet weak var imgRadioButtonFicha: UIImageView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    
    /*Marin*/
    var tipoCertificadoEntity:TipoCertificadoEntity?
    
    private lazy var presenter: PublicidadCertiRegistralPresenter = {
        return PublicidadCertiRegistralPresenter(controller: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        presenter.didLoad()
        hideKeyboardWhenTappedAround()
        self.presenter.didiListadoOficina()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        self.tipoDocumentoPickerView.tag = 1
        self.areaRegistralPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbar()
        
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.participantePickerView.tag = 2
        
        self.numDocText.delegate = self
        
    }
    
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        
    
        let tapConfirmGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewPartida))
        self.toConfirmViewPartida.addGestureRecognizer(tapConfirmGesture3)
        
        let tapConfirmGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewFicha))
        self.toConfirmViewFicha.addGestureRecognizer(tapConfirmGesture4)
        
        
        imgRadioButtonPartida.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonFicha.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        tipoPartidaFicha = "1"
        tipoCRI = "S"
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    @objc func donePressed() {
        if valDone == true  && indexSelectOffice >= 0{
            
            self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            
            self.regPubId = self.oficinas[indexSelect].RegPubId
            self.oficRegId = self.oficinas[indexSelect].OficRegId
            
            let numero = self.numDocText.text ?? ""
            if numero.count > 0 && self.indexSelectOffice >= 0 {
                self.pagarView.primaryButton()
            }
            else {
                self.pagarView.primaryDisabledButton()
            }
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            
            self.numDocText.keyboardType = .alphabet
            self.numDocText.maxLengths = 10
            self.numDocText.valueType = .alphaNumeric
        }else{
            
            self.typeNameDocument = self.tipoDocumentosEntities[indexSelect].tipoDocId
            self.isValidDocument = false
            if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
                self.numDocText.keyboardType = .numberPad
                maxLength = 8
                self.numDocText.maxLengths = 8
                self.numDocText.valueType = .onlyNumbers
            } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
                self.numDocText.keyboardType = .alphabet
                maxLength = 9
                self.numDocText.maxLengths = 9
                self.numDocText.valueType = .alphaNumeric
            } else {
                self.numDocText.maxLengths = 20
                maxLength = 20
                self.numDocText.keyboardType = .alphabet
                self.numDocText.valueType = .alphaNumeric
                self.isValidDocument = true
            }
            self.numDocText.text = ""
            let numero = self.numDocText.text ?? ""
            if numero.count > 0 && self.indexSelectOffice >= 0 {
                self.pagarView.primaryButton()
            }
            else {
                self.pagarView.primaryDisabledButton()
            }
        }
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
    }
    
    
    @objc private func onTapToConfirmViewAnio() {
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        toConfirmViewFicha.isHidden = false
        tipoCRI = "S"
        self.numDocText.text = ""
    }
    
    @objc private func onTapToConfirmViewNum() {
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toConfirmViewFicha.isHidden = true
        
        imgRadioButtonPartida.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonFicha.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        tipoCRI = "P"
        self.numDocText.text = ""
        
    }
    @objc private func onTapToConfirmViewPartida() {
        
        imgRadioButtonPartida.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonFicha.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        tipoPartidaFicha = "1"
        self.numDocText.text = ""
        
    }
    
    @objc private func onTapToConfirmViewFicha() {
        
        imgRadioButtonPartida.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonFicha.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        tipoPartidaFicha = "2"
        self.numDocText.text = ""
        
    }
    
    
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
            typeDocument = "03"
        } else {
        }
        
        var nuAsieSelectSARPvalid = nuAsieSelectSARP.take(1)
        
        if nuAsieSelectSARPvalid == "P"{
        }else{
            nuAsieSelectSARP = ""
        }
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        let numero = self.numDocText.text ?? ""
        print("numero = ", numero)
        if numero.count > 0 && self.indexSelectOffice >= 0 {
            self.pagarView.primaryButton()
        }
        else {
            self.pagarView.primaryDisabledButton()
        }
        self.numDocText.resignFirstResponder()
    }
    
    
    @objc private func onTapPagarView(){
        let numero = self.numDocText.text ?? ""
        
        if (numero.count > 0 && self.indexSelectOffice >= 0) {
            
             // loaderView(isVisible: true)
            
            let usuario = UserPreferencesController.usuario()
            
            self.tipoPer = "N"
            if (usuario.tipoDoc == "09") {
                typeDocument = "09"
            } else if (usuario.tipoDoc == "03") {
                typeDocument = "03"
            }
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            var numDoc = self.numDocText.text ?? ""
            numDoc = numDoc.uppercased()
            self.numDocText.text = numDoc
            self.pagarView.primaryDisabledButton()
            presenter.getValidaPartidaCRI(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numDoc, tipoCRI: tipoCRI, codGrupo: codGrupoLibroArea, tipoPartidaFicha: tipoPartidaFicha)
        }
        else {
            self.pagarView.primaryDisabledButton()
        }
    }
    
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
        // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    func loadPartidaCRI(solicitud: ValidaPartidaCRIEntity) {
        // self.validaPartida = solicitud
        print("estadoOOO::-->>",solicitud.estado)
        //loaderView(isVisible: false)
        if solicitud.estado == 0 {
            
            
            let usuario = UserPreferencesController.usuario()
            
            print("certificadoId::",certificadoId)
            print("codArea::",areaRegId)
            print("valoficinaOrigen::",valoficinaOrigen)
            print("usuario.tipo::","N")
            print(" usuario.priApe::", usuario.priApe)
            print("usuario.segApe::",usuario.segApe)
            print("usuario.nombres::",usuario.nombres)
            print("usuario.tipoDoc::",usuario.tipoDoc)
            print("usuario.nroDoc::",usuario.nroDoc)
            print("usuario.email::",usuario.email)
            print("costoServicio::",precOfic)
            print("usrId::","APPSNRPIOS")
            
            showControllerPayment()
          
            
        }else{
            self.pagarView.primaryButton()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let alert = UIAlertController(title: "SUNARP", message: solicitud.msj, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    func showControllerPayment(){
        let storyboard = UIStoryboard(name: "Payments", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        
        let usuario = UserPreferencesController.usuario()
        vc.paymentItem.tpoPersona = "N"
        vc.paymentItem.apePaterno = usuario.priApe
        vc.paymentItem.apeMaterno = usuario.segApe
        vc.paymentItem.nombre = usuario.nombres
        vc.paymentItem.razSoc = ""
        vc.paymentItem.tpoDoc = usuario.tipoDoc
        vc.paymentItem.numDoc = usuario.nroDoc
        vc.paymentItem.email = usuario.email
        vc.paymentItem.ipRemote = ""
        vc.paymentItem.sessionId = ""
        vc.paymentItem.usrId = "\(usuario.idUser)"
        vc.paymentItem.codArea = areaRegId
        
        let costoServicio = precOfic.replacingOccurrences(of: " ", with: "")
        vc.paymentItem.costoTotal = costoServicio
        vc.paymentItem.costoServicio = costoServicio
        vc.paymentItem.oficinaOrigen = valoficinaOrigen
        vc.titleBar = titleBar
        vc.certificadoId = certificadoId
        vc.paymentItem.partida = self.numDocText.text ?? .empty
        
        
        vc.tipoCertificadoEntity = self.tipoCertificadoEntity
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz // "\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
        
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
        var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
            VisaNet.shared.delegate = self
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        valDone = true
        self.indexSelectOffice = index
        self.indexSelect = index
        
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func changeTypeDocument(index: Int) {
        
        valDone = false
        self.indexSelect = index
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
        
    }
    
    
    func validateTypeDocument() {
        self.numDocument = self.numDocText.text ?? ""
        //   self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty) {
                self.presenter.validarDni()
                self.numDocText.resignFirstResponder()
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
                self.numDocText.resignFirstResponder()
            }
        }
    }
    
    func isValidSolicitar() {
        var partida = self.numDocText.text ?? ""
        
        if indexSelectOffice >= 0 && partida != ""  {
            self.pagarView.primaryButton()
        }
        else {
            self.pagarView.primaryDisabledButton()
        }

    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            self.pagarView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            self.pagarView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
}

extension PublicidadCertiRegistralViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            // Si no hay texto, llenar con ceros y actualizar el campo de texto
            textField.text = ""
            self.pagarView.primaryDisabledButton()
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text), let formattedValue = String(format: "%08d", intValue) as NSString? {
            textField.text = formattedValue as String
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        if text.count > 0 && self.indexSelectOffice >= 0 {
            self.pagarView.primaryButton()
        }
        else {
            self.pagarView.primaryDisabledButton()
        }

        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        isValidSolicitar()
    }
}

extension PublicidadCertiRegistralViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            //return self.tipoCertificado[row]
            
            return self.tipoDocumentosEntities[row].descripcion
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            //self.typeOficinaTextField.text = oficinas[row].Nombre
            //self.typeOficinaTextField.resignFirstResponder()
            
        case 2:
            
            self.changeTypeDocument(index: row)
            //  self.areaRegistralText.text = tipoDocumentosEntities[row].descripcion
            //  self.areaRegistralText.resignFirstResponder()
            
        default:
            return
        }
        
    }
    
    
}

//MARK: - VISANET DELEGATE SECTION

extension PublicidadCertiRegistralViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}
