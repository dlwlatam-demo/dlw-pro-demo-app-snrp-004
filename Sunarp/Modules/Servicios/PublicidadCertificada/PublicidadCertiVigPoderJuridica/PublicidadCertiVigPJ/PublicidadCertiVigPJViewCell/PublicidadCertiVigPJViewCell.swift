//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

protocol DeleteItemPartidaPJProtocol: AnyObject {
    func deleteItemPartida(at indexPath: IndexPath)
}

final class PublicidadCertiVigPJViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "PublicidadCertiVigPJViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    var delegate: DeleteItemPartidaPJProtocol?
    var indexPath: IndexPath?
    
    
    
    @IBOutlet var oficinaRegistralLabel:UILabel!
    
    
    @IBOutlet weak var searchButtonView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: validaPartidaLiteralEntity) {
        
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
        
      print("ofiiicina:>>",solicitud.codZona)
        
       // oficinaRegistralLabel?.text = solicitud.nombreOfic
       
     
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.searchButtonView.addGestureRecognizer(onTapVerSolicitud)
        
    }
    
    @objc private func onTapVerSolicitudView() {
            self.onTapVerSolicitud?()
            if let indexPath = indexPath {
                delegate?.deleteItemPartida(at: indexPath)
            }
        }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        //viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.searchButtonView.addGestureRecognizer(onTapVerSolicitud)
        
       // self.searchButtonView.primaryButton()
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
