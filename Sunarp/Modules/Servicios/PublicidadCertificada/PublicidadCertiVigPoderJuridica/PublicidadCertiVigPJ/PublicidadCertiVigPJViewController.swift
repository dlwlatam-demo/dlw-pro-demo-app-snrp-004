//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit

class PublicidadCertiVigPJViewController: UIViewController,UITableViewDataSource {
    
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typeSolicitudTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    @IBOutlet weak var pagarView: UIView!
    
    @IBOutlet weak var cargoApoderadoTextField: UITextField!
    
    @IBOutlet weak var informationText: UILabel!
    
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var areaRegistralText: UITextField!
    
    @IBOutlet weak var insertNumTextField: UITextField!
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    @IBOutlet weak var numDocText: UITextField!
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    
    @IBOutlet weak var registerLibroView: UIView!
    @IBOutlet weak var contentButonAddView: UIView!
    
    
  
    @IBOutlet weak var CargoRazonSocialTextField: SDCTextField!
    
    
    var indexSelect = 0
    var indexTipoDocumento = 0
    var indexAreaRegistal = 0
    
    var valDone: String = ""
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    
    var certificadoId: String = ""
    
    var tipoNumero: String = ""
    
    
    var refNumPart: String = ""
    var refNumPartMP: String = ""
    
    
    
    var asiento: String = ""
    var cargoApoderado: String = ""
    var tipPerVP: String = ""
    
    
    var titleBar: String = ""
    
    var precOfic: String = ""
    
    var valoficinaOrigen: String = ""
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    var tipoDocumentos: [String] = ["Partida", "Ficha", "Tomo/Folio"]
    
    
    var locationsAll: [String] = []
    
    var areaRegistralPickerView = UIPickerView()
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    
    @IBOutlet weak var toConfirmViewNum: UIView!
    
    
    
    @IBOutlet weak var contentViewJuridica: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    var listaValidaPartidaLiteralEntity: [validaPartidaLiteralEntity] = []
    
    private lazy var presenter: PublicidadCertiVigPJPresenter = {
       return PublicidadCertiVigPJPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var titleCerti: String = ""
    var codLibro: String = ""
    
    
    var codGrupoLibroArea: String = ""
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var tipo: String = ""
    
    @IBOutlet weak var numInsertTextField: SDCTextField!
    @IBOutlet weak var folioText: SDCTextField!
    @IBOutlet weak var tomoText: SDCTextField!
    @IBOutlet weak var razonSocialTextField: SDCTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        hideKeyboardWhenTappedAround()
        setupNavigationBar()
        configTextField()
      
       // self.presenter.listaTive()
        registerCell()
        print("punto de control 7", titleBar)
        self.presenter.willAppear()
        self.presenter.listaService()
        print("Certificado prueba", certificadoId)
        self.tableSearchView.reloadData()
        tipoPartidaFicha = "1"
        
        tipPerVP = "J"
        
        titleLabel.text = titleBar
        presenter.didLoadLibroRegistral()
        

        // Modificamos el valor de la constraint que define la altura del Header
        headerHeightConstraint.constant = 55
        

        if (certificadoId == "88"){
            razonSocialTextField.placeholder = "Ingrese Denominación del órgano"
            CargoRazonSocialTextField.placeholder = "Datos Adicionales"
            informationText.text = "Información que permita identificar al órgano de la persona jurídica"
        }
    }
    
    func configTextField() {
        CargoRazonSocialTextField.delegate = self
        numInsertTextField.delegate = self
        folioText.delegate = self
        tomoText.delegate = self
        razonSocialTextField.delegate = self
        
        razonSocialTextField.valueType = .alphaNumericWithSpace
        razonSocialTextField.maxLength = 50
        
        CargoRazonSocialTextField.valueType = .alphaNumericWithSpace
        CargoRazonSocialTextField.maxLength = 200
        
        numInsertTextField.valueType = .alphaNumeric
        numInsertTextField.maxLength = 10
        
        folioText.valueType = .onlyNumbers
        folioText.maxLength = 6
        
        tomoText.valueType = .onlyNumbers
        tomoText.maxLength = 6
        
        self.numDocText.keyboardType = .numberPad
        numDocText.maxLength = 5
        
    }
    
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
       
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        self.tipo = "T"
        
        
        contentViewJuridica.isHidden = false
    }
    
  
    
  
    @objc private func onTapPagarView(){
        
        if self.typeDocument == "Tomo/Folio" {
            
          var numPart = "-"
           if  let nroFolio = folioText.text, !nroFolio.isEmpty,
               let nroTomo = tomoText.text, !nroTomo.isEmpty {
                let formato = "%06d"
                var nroFolio1 = nroFolio
                var nroTomo1 = nroTomo
                if nroFolio.count < 6 {
                    if let intValue = Int(nroFolio), let formattedValue = String(format: formato, intValue) as NSString? {
                        folioText.text = formattedValue as String
                        nroFolio1 = formattedValue as String
                    }
                }
                if nroTomo.count < 6 {
                    if let intValue = Int(nroTomo), let formattedValue = String(format: formato, intValue) as NSString? {
                        tomoText.text = formattedValue as String
                        nroTomo1 = formattedValue as String
                    }
                }
                
               numPart = "\(nroTomo1)-\(nroFolio1)"
           }
            if numPart == "-" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                
                presenter.getObtenerNumPartida(tipo: "2", numero: numPart, regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: self.areaRegId)
            }
        }
        else if self.typeDocument == "Ficha" {
            numPart = ""
            if  let nroFicha =  numInsertTextField.text, !nroFicha.isEmpty {
                 let formato = "%08d"
                 var nroFicha1 = nroFicha
                 if nroFicha.count < 8 {
                     if let intValue = Int(nroFicha), let formattedValue = String(format: formato, intValue) as NSString? {
                         numInsertTextField.text = formattedValue as String
                         nroFicha1 = formattedValue as String
                     }
                 }
                numPart = nroFicha1
            }
            if numPart == "" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                presenter.getObtenerNumPartida(tipo: "1", numero: numPart, regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: self.areaRegId)
            }
        }
        else {
            
            numPart = ""
            if  let nroPart =  numInsertTextField.text, !nroPart.isEmpty {
                 let formato = "%08d"
                 var nroPart1 = nroPart
                 if nroPart.count < 8 {
                     if let intValue = Int(nroPart), let formattedValue = String(format: formato, intValue) as NSString? {
                         numInsertTextField.text = formattedValue as String
                         nroPart1 = formattedValue as String
                     }
                 }
                numPart = nroPart1
            }
            if numPart == "" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                presenter.getValidaPartidaxRefNumPart(regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: areaRegId, numPart: numPart)
                
            }
            
        }
    }
    
    
    private func setupDesigner() {
       
        formView.backgroundCard()
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        typeSolicitudTextField.border()
        typeSolicitudTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
       
        
        //self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.folioText.borderAndPaddingLeftAndRight()
        self.tomoText.borderAndPaddingLeftAndRight()
        
        
        self.razonSocialTextField.borderAndPaddingLeftAndRight()
        self.CargoRazonSocialTextField.borderAndPaddingLeftAndRight()
        
        
        self.numDocText.borderAndPadding()
        self.numDocText.setPadding(left: CGFloat(8), right: CGFloat(24))
       
        
        
        self.numInsertTextField.borderAndPadding()
        self.numInsertTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        //obtiene lista registro juridico
       // self.presenter.validarRegistroJuridico()
     
        toConfirmView.primaryButton()
        self.toConfirmView.primaryDisabledButton()
        
        
        
        self.typeSolicitudTextField.delegate = self
        self.folioText.delegate = self
        self.tomoText.delegate = self
        self.numInsertTextField.delegate = self
      //  self.typeSolicitudTextField.inputAccessoryView = createToolbarGender()
        
        self.tipoDocumentoPickerView.tag = 1
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbarOficina()
        
        self.tipoCertiPickerView.tag = 2
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        self.typeSolicitudTextField.inputAccessoryView = self.createToolbarTipoDocument()
        
        self.areaRegistralPickerView.tag = 3
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbarAreaRegistral()
        
          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
        registerLibroView.isHidden = true
        
    }
    
    func createToolbarOficina() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedOficina))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarTipoDocument() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedTipoCerti))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarAreaRegistral() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedAreaRegistal))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func registerCell() {
        //Celda Foto perfil
      
        
        let profileCellSearchNibPlaca = UINib(nibName: PublicidadCertiVigPJViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNibPlaca, forCellReuseIdentifier: PublicidadCertiVigPJViewCell.reuseIdentifier)
    }
    
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
          self.obtenerLibroEntities = arrayTipoDocumentos
        //  for tipoDocumento in arrayTipoDocumentos {
       //       self.tipoDocumentos.append(tipoDocumento.descripcion)
       //   }
                          // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
     }
    
    func loadResultados(busquedaResponse: [validaPartidaLiteralEntity]) {
        self.listaValidaPartidaLiteralEntity = busquedaResponse
        
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            self.insertNumTextField.resignFirstResponder()
            
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaValidaPartidaLiteralEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        //self.insertNumTextField.resignFirstResponder()
        self.loaderView(isVisible: false)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.folioText.resignFirstResponder()
        self.tomoText.resignFirstResponder()
        self.CargoRazonSocialTextField.resignFirstResponder()
        if !(numInsertTextField.text?.isEmpty ?? true) {
            self.textFieldShouldReturn(numInsertTextField)
        }
        cargoApoderado = self.CargoRazonSocialTextField.text ?? ""
        self.CargoRazonSocialTextField.text = cargoApoderado.uppercased()
    }
    

    @objc private func onTapToConfirmView() {
       // self.selectOficina = self.typeSolicitudTextField.text ?? ""
       // self.selectSolicitud = self.dateOfIssueTextField.text ?? ""
        self.numPart = self.numDocText.text ?? ""
        self.presenter.validateData()
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                //validateTypeDocument()
                
                print("Adddd")
                
                if asiento == ""{
                    asiento = "\(self.numDocText.text ?? "")"
                }else{
                    asiento = "\(self.numDocText.text ?? ""),\(self.asiento)"
                }
                
                print("asiento::>>>",asiento)
                locationsAll.append(self.numDocText.text ?? "")
                
                self.numDocText.text = ""
                self.tableSearchView.reloadData()
               // self.presenter.listaValidaPartidaLiteral()
                
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        valDone = "1"
        self.indexSelect = index
        
     
    }
    
    
    func changeTypeDocument(index: Int) {
        valDone = "2"
        print("2")
        self.indexSelect = index
        
    }
    
    func changeTypeRegisterLibro(index: Int) {
        valDone = "3"
        self.indexSelect = index
        
        
    }
    
  
    func validateTypeDocument() {
        self.numDocument = self.typeSolicitudTextField.text ?? ""
        self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == "DNI") {
            if (!numDocument.isEmpty && !dateOfIssue.isEmpty) {
               // self.presenter.validarDni()
            }
        } else if (self.typeDocument == "CE") {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
            }
        }
    }
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    func loadService(serviceResponse: [ServicioSunarEntity]) {
        self.servicesLista = serviceResponse
        //self.loaderView(isVisible: false)
       
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
        for tipoDocumento in arrayRegistroJuridico {
            self.tipoRegistroJuridico.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
  
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
        for tipoDocumento in arrayTipoCerficado {
            self.tipoCertificado.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            
           
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    
    func loadPartidaRefNumPart(solicitud: ValidaPartidaCRIEntity) {
       // self.validaPartida = solicitud
        print("estadoOOO::-->>",solicitud.estado)
        if solicitud.estado == 0 {
            
            refNumPart = String(solicitud.refNumPart)
            refNumPartMP = String(solicitud.refNumPartMP)
            asiento = locationsAll.joined(separator: ",")
            print("tipo::",tipoNumero)
            print("numero::",self.numInsertTextField.text ?? "")
            print("regPubId::",regPubId)
            print("oficRegId::",oficRegId)
            print("areaRegId::",areaRegId)
          
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.codLibro = codLibro
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.refNumPart = refNumPart
                vc.paymentItem.refNumPartMP = refNumPartMP
                vc.paymentItem.partida = self.numInsertTextField.text ?? ""
                vc.paymentItem.ficha = ""
                vc.paymentItem.tomo = ""
                vc.paymentItem.folio = ""
                vc.paymentItem.placa = ""
                vc.paymentItem.matricula = ""
                vc.paymentItem.nomEmbarcacion = ""
                vc.paymentItem.expediente = ""
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.asiento = asiento
                vc.paymentItem.tipPerVP = tipPerVP
                vc.paymentItem.razSocVP = self.razonSocialTextField.text ?? ""
                vc.paymentItem.datoAdic = self.CargoRazonSocialTextField.text ?? ""
                vc.paymentItem.usrId = Constant.API_USRID
                
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                
                //Others data information
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.tipoNumero = tipoNumero
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
        
            let alert = UIAlertController(title: "SUNARP", message: solicitud.msj, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
   }
    
    func loadObtenerNumPartida(solicitud: PartidaTFEntity) {
        if solicitud.partida == "" {
            showMessageAlert(message: "Verifique sus datos")
        } else {
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.codLibro = codLibro
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.refNumPart = ""
                vc.paymentItem.refNumPartMP = ""
                vc.paymentItem.partida = solicitud.partida
                if (self.typeDocument == "Ficha") {
                    vc.paymentItem.ficha = self.numInsertTextField.text ?? ""
                }
                else {
                    vc.paymentItem.ficha = ""
                }
                vc.paymentItem.tomo = self.tomoText.text ?? ""
                vc.paymentItem.folio = self.folioText.text ?? ""
                vc.paymentItem.placa = ""
                vc.paymentItem.matricula = ""
                vc.paymentItem.nomEmbarcacion = ""
                vc.paymentItem.expediente = ""
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.asiento = asiento
                vc.paymentItem.tipPerVP = tipPerVP
                vc.paymentItem.razSocVP = self.razonSocialTextField.text ?? ""
                vc.paymentItem.datoAdic = self.CargoRazonSocialTextField.text ?? ""
                vc.paymentItem.usrId = Constant.API_USRID
                
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                
                //Others data information
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.tipoNumero = tipoNumero
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            comps.year = -10
            let minDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = .now
            datePicker.minimumDate = minDate
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            comps.year = -100
            let minDate = calender.date(byAdding: comps, to: .now)
            comps.year = -18
            let maxDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
        }
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressedOficina() {
        self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
        self.typeOficinaTextField.resignFirstResponder()
        self.regPubId = self.oficinas[indexSelect].RegPubId
        self.oficRegId = self.oficinas[indexSelect].OficRegId
        valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
    }
    
    @objc func donePressedTipoCerti() {
        print(indexTipoDocumento)
        self.typeSolicitudTextField.text = tipoDocumentos[indexTipoDocumento]
        self.typeSolicitudTextField.resignFirstResponder()
        
        self.toConfirmView.primaryButton()
        self.pagarView.primaryButton()
        
        self.typeDocument = self.tipoDocumentos[indexTipoDocumento]
    
        self.isValidDocument = false
        if (self.typeDocument == "Partida") {
            self.maxLength = 18
            tipoNumero = "0"
            //self.numberDocumentTextField.keyboardType = .numberPad
            self.numInsertTextField.placeholder = "Nro. de Partida"
            registerLibroView.isHidden = true
            headerHeightConstraint.constant = 40
        } else if (self.typeDocument == "Ficha") {
            tipoNumero = "1"
            self.maxLength = 19
            self.numInsertTextField.placeholder = "Nro. de Ficha"
           // self.numberDocumentTextField.keyboardType = .alphabet
            registerLibroView.isHidden = true
            headerHeightConstraint.constant = 40
        } else {
            tipoNumero = "2"
            self.maxLength = 20
            self.numInsertTextField.placeholder = "Tomo"
          //  self.numberDocumentTextField.keyboardType = .alphabet
     
            self.isValidDocument = true
            
            registerLibroView.isHidden = false
            headerHeightConstraint.constant = 200
        }
        
        self.isChecked = true
        
        self.toConfirmView.primaryButton()
    }
    
    @objc func donePressedAreaRegistal() {
        self.areaRegistralText.text = obtenerLibroEntities[indexAreaRegistal].nomLibro
        self.areaRegistralText.resignFirstResponder()
        
        codLibro = self.obtenerLibroEntities[indexAreaRegistal].codLibro
        areaRegId = self.obtenerLibroEntities[indexAreaRegistal].areaRegId
        codGrupo = self.obtenerLibroEntities[indexAreaRegistal].grupoLibroArea
    }
    
    @objc func donePressed() {
        if valDone == "1" {
            
            self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            self.regPubId = self.oficinas[indexSelect].RegPubId
            self.oficRegId = self.oficinas[indexSelect].OficRegId
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            
        }else  if valDone == "2" {
            
           
            self.typeSolicitudTextField.text = tipoDocumentos[indexTipoDocumento]
            self.typeSolicitudTextField.resignFirstResponder()
            
            self.toConfirmView.primaryButton()
            self.pagarView.primaryButton()
            
            self.typeDocument = self.tipoDocumentos[indexTipoDocumento]
        
            self.isValidDocument = false
            if (self.typeDocument == "Partida") {
                self.maxLength = 18
                tipoNumero = "0"
                //self.numberDocumentTextField.keyboardType = .numberPad
                self.numInsertTextField.placeholder = "Nro. de Partida"
                registerLibroView.isHidden = true
                headerHeightConstraint.constant = 40
            } else if (self.typeDocument == "Ficha") {
                tipoNumero = "1"
                self.maxLength = 19
                self.numInsertTextField.placeholder = "Nro. de Ficha"
               // self.numberDocumentTextField.keyboardType = .alphabet
                registerLibroView.isHidden = true
                headerHeightConstraint.constant = 40
            } else {
                tipoNumero = "2"
                self.maxLength = 20
                self.numInsertTextField.placeholder = "Tomo"
              //  self.numberDocumentTextField.keyboardType = .alphabet
         
                self.isValidDocument = true
                
                registerLibroView.isHidden = false
                headerHeightConstraint.constant = 200
            }
            
            self.isChecked = true
            
            self.toConfirmView.primaryButton()
            
        }else{
            
            self.areaRegistralText.text = obtenerLibroEntities[indexAreaRegistal].nomLibro
            self.areaRegistralText.resignFirstResponder()
            
            codLibro = self.obtenerLibroEntities[indexAreaRegistal].codLibro
            areaRegId = self.obtenerLibroEntities[indexAreaRegistal].areaRegId
            codGrupo = self.obtenerLibroEntities[indexAreaRegistal].grupoLibroArea
            
            
            print("self.codLibro:---:>>>",codLibro)
            print("self.areaRegId:---:>>>",areaRegId)
        }
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            //print("couunt:>>",self.locationsAll.count)
            // return self.listaValidaPartidaLiteralEntity.count;
        return self.locationsAll.count;
        
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          /*  if indexPath.section == 0 {
                print("section1")
            } else {
             */
        print("areaRegId__::>>",areaRegId)
       
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: PublicidadCertiVigPJViewCell.reuseIdentifier, for: indexPath) as? PublicidadCertiVigPJViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
           
            //let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
            //print("listaValidaPartidaLiteralEntity:>>",listaPro)
        
            let location = locationsAll[indexPath.row]
            print("location.title:>>",location)
            cell.oficinaRegistralLabel?.text = location
        
            cell.indexPath = indexPath
            cell.delegate = self
        
           // cell.setup(with: listaPro)
            /*
             cell.onTapVerSolicitud = {[weak self] in
            let storyboard = UIStoryboard(name: "CertificadoLiteral", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                 
               
                 vc.areaRegId = self!.areaRegId
                 print("self!.areaRegId::-->>>",self!.areaRegId)
                 print("self!.regPubId::-->>>",self!.regPubId)
                 vc.regPubId = self!.regPubId
                 vc.oficRegId = self!.oficRegId
                 
                // vc.ofiSARP = listaPro.ofiSARP
                 vc.ofiSARP = "-"
                 vc.coServicio = self!.coServ
                 vc.coTipoRegis = self!.coTipoRgst
                 
                 vc.certificadoId = self!.certificadoId
                 vc.titleCerti = self!.titleCerti
                 vc.valoficinaOrigen = self!.valoficinaOrigen
                 
             self?.navigationController?.pushViewController(vc, animated: true)
             }
             */
            return cell
        
    }
}

extension PublicidadCertiVigPJViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            //return self.tipoCertificado.count
            return  self.tipoDocumentos.count
        case 3:
            return self.obtenerLibroEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            //return self.tipoCertificado[row]
            //return self.servicesLista[row].deServRgst
            return self.tipoDocumentos[row]
            
        case 3:
            return self.obtenerLibroEntities[row].nomLibro
            
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            indexSelect = row
        case 2:
            self.changeTypeDocument(index: row)
            indexTipoDocumento = row
        case 3:
            self.changeTypeRegisterLibro(index: row)
            indexAreaRegistal = row
        default:
            return
        }
        
    }
}

extension PublicidadCertiVigPJViewController: DeleteItemPartidaPJProtocol {
    
    func deleteItemPartida(at indexPath: IndexPath) {
            // Elimina la celda en el índice dado de tu lista de datos
            // Actualiza tu fuente de datos y recarga la tabla si es necesario
            locationsAll.remove(at: indexPath.row)
            tableSearchView.deleteRows(at: [indexPath], with: .fade)
            tableSearchView.reloadData()
    }
    

    
}

extension PublicidadCertiVigPJViewController: UITextFieldDelegate {

    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        // self.validarBotonSolicitar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text) {
            var formato = "%08d"
            if (self.typeDocument == "Tomo/Folio") {
                 formato = "%06d"
            }
            if let formattedValue = String(format: formato, intValue) as NSString? {
                textField.text = formattedValue as String
            }
            else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
}



/*
// MARK: - Table view datasource
extension CertificadoLiteralPartidaDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        print("couunt:>>",self.listaValidaPartidaLiteralEntity.count)
            return self.listaValidaPartidaLiteralEntity.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          /*  if indexPath.section == 0 {
                print("section1")
            } else {
             */
        print("areaRegId__::>>",areaRegId)
        if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaPlacaViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
             //cell.item = self.listaPropiedadEntities[indexPath.row]
    
             let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
             print("listaValidaPartidaLiteralEntity:>>",listaPro)
             cell.setup(with: listaPro)
              cell.onTapVerSolicitud = {[weak self] in
             let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                  
                  vc.codLibro = listaPro.codLibro
                  vc.numPartida = listaPro.numPartida
                  vc.fichaId = listaPro.fichaId
                  vc.tomoId = listaPro.tomoId
                  vc.fojaId = listaPro.fojaId
                 // vc.ofiSARP = listaPro.ofiSARP
                  vc.ofiSARP = "-"
                  vc.coServicio = self?.coServ
                  vc.coTipoRegis = self!.coTipoRgst
                  vc.codZona = listaPro.codZona
                  vc.codOficina = listaPro.codOficina
                  
              self?.navigationController?.pushViewController(vc, animated: true)
             }
             return cell
            
        }else{
          
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            //cell.item = self.listaPropiedadEntities[indexPath.row]
    
            let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
            print("listaValidaPartidaLiteralEntity:>>",listaPro)
            cell.setup(with: listaPro)
            
             cell.onTapVerSolicitud = {[weak self] in
            let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailImagenConsultaTiveViewController") as! DetailImagenConsultaTiveViewController
             vc.tipo = listaPro.tipo
             vc.anioTitulo = listaPro.anioTitulo
             vc.numeroTitulo = listaPro.numeroTitulo
             vc.numeroPlaca = listaPro.numeroPlaca
             vc.codigoVerificacion = listaPro.codigoVerificacion
             vc.codigoZona = listaPro.codigoZona
             vc.codigoOficina = listaPro.codigoOficina
             self?.navigationController?.pushViewController(vc, animated: true)
            }
    
            return cell
        }
    }
}
*/
// MARK: - Table view delegate
extension PublicidadCertiVigPJViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}



/*
private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}


struct Location {
    let title: String
    let rating: String
    let description: String
    let latitude: Double
    let longitude: Double

}
 */
