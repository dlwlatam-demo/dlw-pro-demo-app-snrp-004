//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class PublicidadCertiVigPoderJuridicaPresenter {
    
    private weak var controller: PublicidadCertiVigPoderJuridicaViewController?
    
    lazy private var model: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var modelTive: ConsultaTivetModel = {
        let navigation = controller?.navigationController
       return ConsultaTivetModel(navigationController: navigation!)
    }()

    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    init(controller: PublicidadCertiVigPoderJuridicaViewController) {
        self.controller = controller
    }
    
}



extension PublicidadCertiVigPoderJuridicaPresenter: GenericPresenter {
    
    func willAppear() {
     //   self.controller?.loaderView(isVisible: true)
        self.model.getListaOficinaRegistral{ (arrayOficinas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
  
    func didLoadLibroRegistral() {
        let areaRegId = controller?.areaRegId ?? ""
        print("areaRegId::->",areaRegId)
        //let guid = UserPreferencesController.getGuid()
        self.modelDoc.getListaObtenerLibro(areaRegId: areaRegId) { (arrayTipoDocumentos) in
            self.controller?.loadObtenerLibro(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    
    func getValidaPartidaxRefNumPart(regPubId: String, oficRegId: String, areaRegId: String, numPart: String) {
        self.consultaPublicaModel.getValidaPartidaxRefNumPart(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart) { (arrayResponse) in
          
             self.controller?.loadPartidaRefNumPart(solicitud:arrayResponse)
        }
    }
    
    func getObtenerNumPartida(tipo: String,numero: String,regPubId: String, oficRegId: String, areaRegId: String) {
        self.consultaPublicaModel.getObtenerNumPartida(tipo: tipo,numero: numero,regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId) { (arrayResponse) in
          
             self.controller?.loadObtenerNumPartida(solicitud:arrayResponse)
        }
    }
    func listaValidaPartidaLiteral() {
        
        let regPubId = controller?.regPubId ?? ""
        let oficRegId = controller?.oficRegId ?? ""
        let areaRegId = controller?.areaRegId ?? ""
        let codGrupo = controller?.codGrupoLibroArea ?? ""
        let tipoPartidaFicha = controller?.tipoPartidaFicha ?? ""
        let numPart = controller?.numPart ?? ""
        let coServ = controller?.coServ ?? ""
        let coTipoRgst = controller?.coTipoRgst ?? ""
        
        self.controller?.loaderView(isVisible: true)
        self.model.getListaValidaPartidaLiteral(regPubId: regPubId,oficRegId: oficRegId,areaRegId: areaRegId,codGrupo: codGrupo,tipoPartidaFicha: tipoPartidaFicha,numPart: numPart,coServ: coServ,coTipoRgst: coTipoRgst){ (arrayBusqTive) in
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadResultados(busquedaResponse: arrayBusqTive)
        }
    }
        
        
    func postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tomo: String, folio: String, placa: String, matricula: String, nomEmbarcacion: String, expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String,tipPerVP: String,apePateVP: String,apeMateVP: String,nombVP: String,razSocVP: String,cargoApoderado: String,datoAdic: String,costoServicio: String, usrId: String) {
           
        self.consultaPublicaModel.postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, ficha: ficha, tomo: tomo, folio: folio, placa: placa, matricula: matricula, nomEmbarcacion: nomEmbarcacion, expediente: expediente, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, asiento: asiento,tipPerVP: tipPerVP,apePateVP: apePateVP,apeMateVP: apeMateVP,nombVP: nombVP,razSocVP: razSocVP,cargoApoderado: cargoApoderado,datoAdic: datoAdic,costoServicio: costoServicio, usrId: usrId) { (niubizResponse) in
                
                 self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            }
    }
    
    
    func listaService() {
        
        let areaRegId = controller?.areaRegId ?? ""
        print("areaRegId::>>",areaRegId)
        self.model.getListaTipoServicioSunarp(areaRegId: areaRegId){ (arrayService) in
            self.controller?.loadService(serviceResponse: arrayService)
        }
    }
    
    func didLoad() {
        //let guid = UserPreferencesController.getGuid()
      //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
       //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
       // }
    }
    
    func validarRegistroJuridico() {
        self.controller?.loaderView(isVisible: true)
     
        self.model.getListaJuridicos() { (arrayRegistroJuridico) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoRegistroJuridico(arrayRegistroJuridico: arrayRegistroJuridico)
           // let state = arrayTipoDocumentos.msgResult.isEmpty
           // self.controller?.loadDatosDni(state, message: arrayTipoDocumentos.msgResult, jsonValidacionDni: arrayTipoDocumentos)
        }
    }
    
    func validarTipoCertificado(tipoCertificado:String) {
        self.controller?.loaderView(isVisible: true)
        
        self.model.getListaTipoCertificado(tipoCertificado:tipoCertificado) { (arrayTipoCertificado) in
            self.controller?.loaderView(isVisible: false)
            
            self.controller?.loadTipoCerficado(arrayTipoCerficado: arrayTipoCertificado)
          
        }
    }
    
    
    func validateData() {
       
        let numPart = self.controller?.numPart ?? ""
        
        if (numPart.isEmpty) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId() {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }

    
}
