//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

protocol DeleteItemPartidaProtocol: AnyObject {
    func deleteItemPartida(at indexPath: IndexPath)
}
final class PublicidadCertiVigPoderJuridicaViewCellViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "PublicidadCertiVigPoderJuridicaViewCellViewCell"
     @IBOutlet var viewCell:UIView!
    
    var delegate: DeleteItemPartidaProtocol?
    var indexPath: IndexPath?
    
    
    
    @IBOutlet var oficinaRegistralLabel:UILabel!
    
    
    @IBOutlet weak var searchButtonView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: validaPartidaLiteralEntity) {
        
      print("ofiiicina:>>",solicitud.codZona)
        
     
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.searchButtonView.addGestureRecognizer(onTapVerSolicitud)
        
    }
    
    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
       
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCell.backgroundColor = UIColor.white
        
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.searchButtonView.addGestureRecognizer(onTapVerSolicitud)
        
        
    }
    
    @objc private func onTapVerSolicitudView() {
            self.onTapVerSolicitud?()
            if let indexPath = indexPath {
                delegate?.deleteItemPartida(at: indexPath)
            }
        }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
