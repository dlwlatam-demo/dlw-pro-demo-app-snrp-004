//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadCertiVigPoderJuridicaViewController: UIViewController,UITableViewDataSource {
    
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typeSolicitudTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    @IBOutlet weak var pagarView: UIView!
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var areaRegistralText: UITextField!
    
    @IBOutlet weak var insertNumTextField: UITextField!
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
   
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    
    @IBOutlet weak var registerLibroView: UIView!
    @IBOutlet weak var contentButonAddView: UIView!
    
    
    @IBOutlet weak var razonSocialTextField: SDCTextField!
    @IBOutlet weak var CargoRazonSocialTextField: SDCTextField!
    @IBOutlet weak var CargoNaturalTextField: SDCTextField!
    @IBOutlet weak var CargoApoderadoTextField: SDCTextField!
    
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"

    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    
    var certificadoId: String = ""
    
    var tipoNumero: String = ""
    
    var valoficinaOrigen: String = ""
    
    var titleBar: String = ""
    
    var precOfic: String = ""
    
    var refNumPart: String = ""
    var refNumPartMP: String = ""
    
    
    
    var asiento: String = ""
    var cargoApoderado: String = ""
    var datoAdicional: String = ""
    var tipPerVP: String = ""
    
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    var indexSelect = 0
    var indexTipoDocumento = 0
    var indexAreaRegistal = 0
    
    var valDone: String = ""
    
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    var tipoDocumentos: [String] = ["Partida", "Ficha", "Tomo/Folio"]

    
    var locationsAll: [String] = []
    
    var areaRegistralPickerView = UIPickerView()
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    
    
    
    @IBOutlet weak var contentViewNatural: UIView!
    @IBOutlet weak var contentViewJuridica: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    var listaValidaPartidaLiteralEntity: [validaPartidaLiteralEntity] = []
    
    private lazy var presenter: PublicidadCertiVigPoderJuridicaPresenter = {
       return PublicidadCertiVigPoderJuridicaPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var titleCerti: String = ""
    var codLibro: String = ""
    
    
    var codGrupoLibroArea: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var tipo: String = ""
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
 
    @IBOutlet weak var nombreTextField: SDCTextField!
    @IBOutlet weak var apellidoPatTextField: SDCTextField!
    @IBOutlet weak var apellidoMatTextField: SDCTextField!
    @IBOutlet weak var folioText: SDCTextField!
    @IBOutlet weak var tomoText: SDCTextField!
    @IBOutlet weak var numInsertTextField: SDCTextField!
    @IBOutlet weak var numDocText: SDCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        addGestureView()
        hideKeyboardWhenTappedAround()
      //  presenter.didLoad()
        configTextField()
      
       // self.presenter.listaTive()
        registerCell()
        self.presenter.willAppear()
        self.presenter.listaService()
        
        self.tableSearchView.reloadData()
        tipoPartidaFicha = "1"
        tipPerVP = "N"
        /*
        if areaRegId == "24000"{
            typeRadioButtonLabel.text = "Placa"
        }else{
            typeRadioButtonLabel.text = "Ficha"
        }
         */
        self.toConfirmViewNum.isHidden = false
        titleLabel.text = titleBar
        presenter.didLoadLibroRegistral()
        

        // Modificamos el valor de la constraint que define la altura del Header
        headerHeightConstraint.constant = 55
        
        self.numDocText.keyboardType = .numberPad
    }
    
    func configTextField() {
        numDocText.delegate = self
        nombreTextField.delegate = self
        apellidoPatTextField.delegate = self
        apellidoMatTextField.delegate = self
        folioText.delegate = self
        tomoText.delegate = self
        numInsertTextField.delegate = self
        
        //CargoNaturalTextField.valueType = .alphaNumericWithSpace
        
        numInsertTextField.valueType = .alphaNumeric
        numInsertTextField.maxLength = 10
        
        folioText.valueType = .alphaNumeric
        folioText.maxLength = 6
        folioText.valueType = .onlyNumbers
        tomoText.valueType = .alphaNumeric
        tomoText.maxLength = 6
        tomoText.valueType = .onlyNumbers

        nombreTextField.valueType = .alphaNumericWithSpace
        nombreTextField.maxLength = 50
        
        apellidoPatTextField.valueType = .alphaNumericWithSpace
        apellidoPatTextField.maxLength = 50
        
        apellidoMatTextField.valueType = .alphaNumericWithSpace
        apellidoMatTextField.maxLengths = 50
        
        numDocText.keyboardType = .numberPad
        numDocText.valueType = .alphaNumeric
        numDocText.maxLength = 5
        numDocText.delegate = self
        // CargoApoderadoTextField.valueType = .alphaNumericWithSpace
        // CargoApoderadoTextField.maxLengths = 50
        CargoNaturalTextField.maxLength = 200
        CargoNaturalTextField.keyboardType = .alphabet
        CargoNaturalTextField.delegate = self
        CargoRazonSocialTextField.maxLength = 200
        CargoRazonSocialTextField.keyboardType = .alphabet
        CargoRazonSocialTextField.delegate = self
        
        CargoApoderadoTextField.placeholder = "Cargo o Apoderado"
        CargoApoderadoTextField.maxLength = 200
        CargoApoderadoTextField.keyboardType = .alphabet
        CargoApoderadoTextField.delegate = self
        CargoNaturalTextField.placeholder = "Dato Adicional"
        CargoRazonSocialTextField.placeholder = "Dato Adicional"
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkView.addGestureRecognizer(tapChecked)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        self.tipo = "T"
        
        
        contentViewNatural.isHidden = false
        contentViewJuridica.isHidden = true
        
        datoAdicional = self.CargoNaturalTextField.text ?? ""
        cargoApoderado = self.CargoApoderadoTextField.text ?? ""
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            self.pagarView.primaryDisabledButton()
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            self.pagarView.primaryButton()
        }
        self.isChecked = !self.isChecked
    }
    
    @objc private func onTapToConfirmViewAnio() {
      //  self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        
        self.tipoPartidaFicha = "1"
        
        //self.anioTituloText.placeholder = "Año del Título"
        //self.numTituloText.placeholder = "Nº de Título"
        self.tipo = "T"
        tipPerVP = "N"
        contentViewNatural.isHidden = false
        contentViewJuridica.isHidden = true
        
        datoAdicional = self.CargoNaturalTextField.text ?? ""
        print(datoAdicional)
        cargoApoderado = self.CargoApoderadoTextField.text ?? ""
        
    }
    @objc private func onTapToConfirmViewNum() {
     //   self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        
        //self.anioTituloText.placeholder = "Año de Publicidad"
        //self.numTituloText.placeholder = "Nº de Publicidad"
        self.tipo = "P"
        
        tipPerVP = "J"
        contentViewNatural.isHidden = true
        contentViewJuridica.isHidden = false
        
        datoAdicional = self.CargoRazonSocialTextField.text ?? ""
        cargoApoderado = self.CargoApoderadoTextField.text ?? ""
        
        
    }
    @objc private func onTapPagarView(){
        
        if self.typeDocument == "Tomo/Folio" {
            
          var numPart = "-"
           if  let nroFolio = folioText.text, !nroFolio.isEmpty,
               let nroTomo = tomoText.text, !nroTomo.isEmpty {
                let formato = "%06d"
                var nroFolio1 = nroFolio
                var nroTomo1 = nroTomo
                if nroFolio.count < 6 {
                    if let intValue = Int(nroFolio), let formattedValue = String(format: formato, intValue) as NSString? {
                        folioText.text = formattedValue as String
                        nroFolio1 = formattedValue as String
                    }
                }
                if nroTomo.count < 6 {
                    if let intValue = Int(nroTomo), let formattedValue = String(format: formato, intValue) as NSString? {
                        tomoText.text = formattedValue as String
                        nroTomo1 = formattedValue as String
                    }
                }
                
               numPart = "\(nroTomo1)-\(nroFolio1)"
           }
            if numPart == "-" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                
                datoAdicional = self.CargoRazonSocialTextField.text ?? ""
                cargoApoderado = self.CargoApoderadoTextField.text ?? ""
                presenter.getObtenerNumPartida(tipo: "2", numero: numPart, regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: self.areaRegId)
            }
        }
        else if self.typeDocument == "Ficha" {
            numPart = ""
            if  let nroFicha =  numInsertTextField.text, !nroFicha.isEmpty {
                 let formato = "%08d"
                 var nroFicha1 = nroFicha
                 if nroFicha.count < 8 {
                     if let intValue = Int(nroFicha), let formattedValue = String(format: formato, intValue) as NSString? {
                         numInsertTextField.text = formattedValue as String
                         nroFicha1 = formattedValue as String
                     }
                 }
                numPart = nroFicha1
            }
            if numPart == "" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                
                datoAdicional = self.CargoRazonSocialTextField.text ?? ""
                cargoApoderado = self.CargoApoderadoTextField.text ?? ""
                presenter.getObtenerNumPartida(tipo: "1", numero: numPart, regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: self.areaRegId)
            }
        }
        else {
            
            numPart = ""
            if  let nroPart =  numInsertTextField.text, !nroPart.isEmpty {
                 let formato = "%08d"
                 var nroPart1 = nroPart
                 if nroPart.count < 8 {
                     if let intValue = Int(nroPart), let formattedValue = String(format: formato, intValue) as NSString? {
                         numInsertTextField.text = formattedValue as String
                         nroPart1 = formattedValue as String
                     }
                 }
                numPart = nroPart1
            }
            if numPart == "" {
                showMessageAlert(message: "Por favor verifique sus datos")
            }
            else {
                
                datoAdicional = self.CargoRazonSocialTextField.text ?? ""
                cargoApoderado = self.CargoApoderadoTextField.text ?? ""
                presenter.getValidaPartidaxRefNumPart(regPubId: self.regPubId, oficRegId: self.oficRegId, areaRegId: areaRegId, numPart: numPart)
                
            }
            
        }
    }
    
   
  
    
    private func setupDesigner() {
       
        formView.backgroundCard()
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        typeSolicitudTextField.border()
        typeSolicitudTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
       
        
        //self.pagarView.primaryButton()
        self.checkView.borderCheckView()
        self.pagarView.primaryDisabledButton()
        
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.folioText.borderAndPaddingLeftAndRight()
        self.tomoText.borderAndPaddingLeftAndRight()
        
        
        
        self.razonSocialTextField.borderAndPaddingLeftAndRight()
        self.CargoRazonSocialTextField.borderAndPaddingLeftAndRight()
        self.CargoNaturalTextField.borderAndPaddingLeftAndRight()
        self.nombreTextField.borderAndPaddingLeftAndRight()
        self.apellidoPatTextField.borderAndPaddingLeftAndRight()
        self.apellidoMatTextField.borderAndPaddingLeftAndRight()
        self.CargoApoderadoTextField.borderAndPaddingLeftAndRight()
        
        
        
        
        self.numDocText.borderAndPadding()
        self.numDocText.setPadding(left: CGFloat(8), right: CGFloat(24))
       
        
        
        self.numInsertTextField.borderAndPadding()
        self.numInsertTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        //obtiene lista registro juridico
       // self.presenter.validarRegistroJuridico()
     
        toConfirmView.primaryButton()
        self.toConfirmView.primaryDisabledButton()
        
        
        
        self.typeSolicitudTextField.delegate = self
        self.folioText.delegate = self
        self.tomoText.delegate = self
        self.numInsertTextField.delegate = self
      //  self.typeSolicitudTextField.inputAccessoryView = createToolbarGender()
        
        self.tipoDocumentoPickerView.tag = 1
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbarOficina()
        
        self.tipoCertiPickerView.tag = 2
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        self.typeSolicitudTextField.inputAccessoryView = self.createToolbarTipoDocument()
        
        self.areaRegistralPickerView.tag = 3
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbarAreaRegistral()
        
          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
        registerLibroView.isHidden = true
        
    }
    func registerCell() {
        //Celda Foto perfil
      
        
        let profileCellSearchNibPlaca = UINib(nibName: PublicidadCertiVigPoderJuridicaViewCellViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNibPlaca, forCellReuseIdentifier: PublicidadCertiVigPoderJuridicaViewCellViewCell.reuseIdentifier)
    }
    
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
          self.obtenerLibroEntities = arrayTipoDocumentos
        //  for tipoDocumento in arrayTipoDocumentos {
       //       self.tipoDocumentos.append(tipoDocumento.descripcion)
       //   }
                          // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
     }
    
    func loadResultados(busquedaResponse: [validaPartidaLiteralEntity]) {
        self.listaValidaPartidaLiteralEntity = busquedaResponse
        
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            self.insertNumTextField.resignFirstResponder()
            
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
            if (self.listaValidaPartidaLiteralEntity.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
        }
        
        self.tableSearchView.reloadData()
        //self.insertNumTextField.resignFirstResponder()
        self.loaderView(isVisible: false)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.folioText.resignFirstResponder()
        self.tomoText.resignFirstResponder()
        self.CargoNaturalTextField.resignFirstResponder()
        self.CargoRazonSocialTextField.resignFirstResponder()
        self.CargoApoderadoTextField.resignFirstResponder()
        if !(numInsertTextField.text?.isEmpty ?? true) {
            self.textFieldShouldReturn(numInsertTextField)
        }
        cargoApoderado = self.CargoApoderadoTextField.text ?? ""
        self.CargoApoderadoTextField.text = cargoApoderado.uppercased()
        
        
    }
    

    @objc private func onTapToConfirmView() {
       // self.selectOficina = self.typeSolicitudTextField.text ?? ""
       // self.selectSolicitud = self.dateOfIssueTextField.text ?? ""
        self.numPart = self.numDocText.text ?? ""
        self.presenter.validateData()
        
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {

                if asiento == ""{
                    asiento = "\(self.numDocText.text ?? "")"
                }else{
                    asiento = "\(self.numDocText.text ?? ""),\(self.asiento)"
                }
                
                print("asiento::>>>",asiento)
                locationsAll.append(self.numDocText.text ?? "")
                
                self.numDocText.text = ""
                self.tableSearchView.reloadData()
                
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
   
    
    func loadPartidaRefNumPart(solicitud: ValidaPartidaCRIEntity) {
       // self.validaPartida = solicitud
        print("estadoOOO::-->>",solicitud.estado)
        if solicitud.estado == 0 {
            
            refNumPart = String(solicitud.refNumPart)
            refNumPartMP = String(solicitud.refNumPartMP)
            asiento = locationsAll.joined(separator: ",")
            print("tipo::",tipoNumero)
            print("numero::",self.numInsertTextField.text ?? "")
            print("regPubId::",regPubId)
            print("oficRegId::",oficRegId)
            print("areaRegId::",areaRegId)
          
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.codLibro = codLibro
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.refNumPart = refNumPart
                vc.paymentItem.refNumPartMP = refNumPartMP
                vc.paymentItem.partida = self.numInsertTextField.text ?? ""
                vc.paymentItem.ficha = ""
                vc.paymentItem.tomo = ""
                vc.paymentItem.folio = ""
                vc.paymentItem.placa = ""
                vc.paymentItem.matricula = ""
                vc.paymentItem.nomEmbarcacion = ""
                vc.paymentItem.expediente = ""
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.asiento = asiento
                vc.paymentItem.tipPerVP = tipPerVP
                vc.paymentItem.apePateVP = self.apellidoPatTextField.text ?? ""
                vc.paymentItem.apeMateVP = self.apellidoMatTextField.text ?? ""
                vc.paymentItem.nombVP = self.nombreTextField.text ?? ""
                vc.paymentItem.razSocVP = self.razonSocialTextField.text ?? ""
                vc.paymentItem.cargoApoderado = cargoApoderado
                if(tipPerVP == "J"){
                    vc.paymentItem.datoAdic = self.CargoRazonSocialTextField.text ?? ""
                }
                else {
                    vc.paymentItem.datoAdic = self.CargoNaturalTextField.text ?? ""
                }
                vc.paymentItem.usrId = Constant.API_USRID
                
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                
                //Others data information
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.tipoNumero = tipoNumero
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
        
            let alert = UIAlertController(title: "SUNARP", message: solicitud.msj, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
   }
    
    func loadObtenerNumPartida(solicitud: PartidaTFEntity) {
        if solicitud.partida == "" {
            showMessageAlert(message: "Verifique sus datos")
        } else {
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.codLibro = codLibro
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.refNumPart = ""
                vc.paymentItem.refNumPartMP = ""
                vc.paymentItem.partida = solicitud.partida
                if (self.typeDocument == "Ficha") {
                    vc.paymentItem.ficha = self.numInsertTextField.text ?? ""
                }
                else {
                    vc.paymentItem.ficha = ""
                }
                vc.paymentItem.tomo = self.tomoText.text ?? ""
                vc.paymentItem.folio = self.folioText.text ?? ""
                vc.paymentItem.placa = ""
                vc.paymentItem.matricula = ""
                vc.paymentItem.nomEmbarcacion = ""
                vc.paymentItem.expediente = ""
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.asiento = asiento
                vc.paymentItem.tipPerVP = tipPerVP
                vc.paymentItem.apePateVP = self.apellidoPatTextField.text ?? ""
                vc.paymentItem.apeMateVP = self.apellidoMatTextField.text ?? ""
                vc.paymentItem.nombVP = self.nombreTextField.text ?? ""
                vc.paymentItem.razSocVP = self.razonSocialTextField.text ?? ""
                vc.paymentItem.cargoApoderado = cargoApoderado
                if(tipPerVP == "J"){
                    vc.paymentItem.datoAdic = self.CargoRazonSocialTextField.text ?? ""
                }
                else {
                    vc.paymentItem.datoAdic = self.CargoNaturalTextField.text ?? ""
                }
                vc.paymentItem.usrId = Constant.API_USRID
                
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                
                //Others data information
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.tipoNumero = tipoNumero
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    func changeTypeDocument(index: Int) {
        valDone = "1"
        self.indexSelect = index
    }
    
    
    func changeTypeGender(index: Int) {
        valDone = "2"
        self.indexSelect = index
        
        
    }
    
    func changeTypeRegisterLibro(index: Int) {
        valDone = "3"
        self.indexSelect = index
        
        
    }
    
  
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    func loadService(serviceResponse: [ServicioSunarEntity]) {
        self.servicesLista = serviceResponse
        //self.loaderView(isVisible: false)
       
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        
    }
    
    func loadTipoRegistroJuridico(arrayRegistroJuridico: [RegistroJuridicoEntity]) {
        self.registroJuridicoEntities = arrayRegistroJuridico
        for tipoDocumento in arrayRegistroJuridico {
            self.tipoRegistroJuridico.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
  
    func loadTipoCerficado(arrayTipoCerficado: [TipoCertificadoEntity]) {
        self.tipoCertificadoEntities = arrayTipoCerficado
        for tipoDocumento in arrayTipoCerficado {
            self.tipoCertificado.append(tipoDocumento.nombre)
        }
        self.loaderView(isVisible: false)
        self.tipoCertiPickerView.delegate = self
        self.tipoCertiPickerView.dataSource = self
        
        self.typeSolicitudTextField.inputView = self.tipoCertiPickerView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            
           
            
            let storyboard = UIStoryboard(name: "ConsultaPropiedad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaPropiedadViewController") as! DetailConsultaPropiedadViewController
           // vc.array = users
            self.navigationController?.pushViewController(vc, animated: true)
            //Segunda pAntalla
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //"\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarOficina() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedOficina))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarTipoDocument() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedTipoCerti))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarAreaRegistral() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedAreaRegistal))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            comps.year = -10
            let minDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = .now
            datePicker.minimumDate = minDate
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            comps.year = -100
            let minDate = calender.date(byAdding: comps, to: .now)
            comps.year = -18
            let maxDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
        }
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressedOficina() {
        self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
        self.typeOficinaTextField.resignFirstResponder()
        
        self.regPubId = self.oficinas[indexSelect].RegPubId
        self.oficRegId = self.oficinas[indexSelect].OficRegId
        
        valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
    }
    
    @objc func donePressedTipoCerti() {
        self.typeSolicitudTextField.text = tipoDocumentos[indexTipoDocumento]
        self.typeSolicitudTextField.resignFirstResponder()
        
        self.toConfirmView.primaryButton()
        
        self.typeDocument = self.tipoDocumentos[indexTipoDocumento]
    
        self.isValidDocument = false
        if (self.typeDocument == "Partida") {
            self.maxLength = 18
            tipoNumero = "0"
            //self.numberDocumentTextField.keyboardType = .numberPad
            self.numInsertTextField.placeholder = "Nro. de Partida"
            registerLibroView.isHidden = true
            headerHeightConstraint.constant = 40
        } else if (self.typeDocument == "Ficha") {
            tipoNumero = "1"
            self.maxLength = 19
            self.numInsertTextField.placeholder = "Nro. de Ficha"
           // self.numberDocumentTextField.keyboardType = .alphabet
            registerLibroView.isHidden = true
            headerHeightConstraint.constant = 40
        } else {
            tipoNumero = "2"
            self.maxLength = 20
            self.numInsertTextField.placeholder = "Número"
          //  self.numberDocumentTextField.keyboardType = .alphabet
     
            self.isValidDocument = true
            
            registerLibroView.isHidden = false
            headerHeightConstraint.constant = 200
        }
        
        self.isChecked = true
        
        self.toConfirmView.primaryButton()
    }
    
    
    @objc func donePressedAreaRegistal() {
        self.areaRegistralText.text = obtenerLibroEntities[indexAreaRegistal].nomLibro
        self.areaRegistralText.resignFirstResponder()
        
        codLibro = self.obtenerLibroEntities[indexAreaRegistal].codLibro
        areaRegId = self.obtenerLibroEntities[indexAreaRegistal].areaRegId
        codGrupo = self.obtenerLibroEntities[indexAreaRegistal].grupoLibroArea
    }
    
    @objc func donePressed() {
        if valDone == "1" {
            
            self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            
            self.regPubId = self.oficinas[indexSelect].RegPubId
            self.oficRegId = self.oficinas[indexSelect].OficRegId
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
        }else  if valDone == "2" {
            
            self.typeSolicitudTextField.text = tipoDocumentos[indexSelect]
            self.typeSolicitudTextField.resignFirstResponder()
            
            self.toConfirmView.primaryButton()
            
            self.typeDocument = self.tipoDocumentos[indexSelect]
        
            self.isValidDocument = false
            if (self.typeDocument == "Partida") {
                self.maxLength = 18
                tipoNumero = "0"
                //self.numberDocumentTextField.keyboardType = .numberPad
                self.numInsertTextField.placeholder = "Nro. de Partida"
                registerLibroView.isHidden = true
                headerHeightConstraint.constant = 40
            } else if (self.typeDocument == "Ficha") {
                tipoNumero = "1"
                self.maxLength = 19
                self.numInsertTextField.placeholder = "Nro. de Ficha"
               // self.numberDocumentTextField.keyboardType = .alphabet
                registerLibroView.isHidden = true
                headerHeightConstraint.constant = 40
            } else {
                tipoNumero = "2"
                self.maxLength = 20
                self.numInsertTextField.placeholder = "Número"
              //  self.numberDocumentTextField.keyboardType = .alphabet
         
                self.isValidDocument = true
                
                registerLibroView.isHidden = false
                headerHeightConstraint.constant = 200
            }
            
            self.isChecked = true
            
            self.toConfirmView.primaryButton()
        }else{
            
            self.areaRegistralText.text = obtenerLibroEntities[indexSelect].nomLibro
            self.areaRegistralText.resignFirstResponder()
            
            codLibro = self.obtenerLibroEntities[indexSelect].codLibro
            areaRegId = self.obtenerLibroEntities[indexSelect].areaRegId
            codGrupo = self.obtenerLibroEntities[indexSelect].grupoLibroArea
            
            
            print("areaRegistralText.text ",areaRegistralText.text)
            print("self.codLibro:---:>>>",codLibro)
            print("self.areaRegId:---:>>>",areaRegId)
        }
      
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
  
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.locationsAll.count;
        
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("areaRegId__::>>",areaRegId)
       
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: PublicidadCertiVigPoderJuridicaViewCellViewCell.reuseIdentifier, for: indexPath) as? PublicidadCertiVigPoderJuridicaViewCellViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
           
            let location = locationsAll[indexPath.row]
            print("location.title:>>",location)
            cell.oficinaRegistralLabel?.text = location
            cell.indexPath = indexPath
            cell.delegate = self
            
        
        
            return cell
        
    }
}

extension PublicidadCertiVigPoderJuridicaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            //return self.tipoCertificado.count
            return  self.tipoDocumentos.count
        case 3:
            return self.obtenerLibroEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            indexTipoDocumento = row
            return self.tipoDocumentos[row]
        case 3:
            indexAreaRegistal = row
            return self.obtenerLibroEntities[row].nomLibro
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            indexSelect = row
        case 2:
            indexTipoDocumento = row
        case 3:
            indexAreaRegistal = row
        default:
            return
        }
        
    }
}
//MARK: - VISANET DELEGATE SECTION

extension PublicidadCertiVigPoderJuridicaViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension PublicidadCertiVigPoderJuridicaViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        // self.validarBotonSolicitar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text) {
            var formato = "%08d"
            if (self.typeDocument == "Tomo/Folio") {
                 formato = "%06d"
            }
            if let formattedValue = String(format: formato, intValue) as NSString? {
                textField.text = formattedValue as String
            }
            else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
}

extension PublicidadCertiVigPoderJuridicaViewController: DeleteItemPartidaProtocol {
    
    func deleteItemPartida(at indexPath: IndexPath) {
            // Elimina la celda en el índice dado de tu lista de datos
            // Actualiza tu fuente de datos y recarga la tabla si es necesario
            locationsAll.remove(at: indexPath.row)
            tableSearchView.deleteRows(at: [indexPath], with: .fade)
        tableSearchView.reloadData()
    }
    

    
}



// MARK: - Table view delegate
extension PublicidadCertiVigPoderJuridicaViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}


struct Location {
    let title: String
    let rating: String
    let description: String
    let latitude: Double
    let longitude: Double

}
