//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadCertiCargaGraAeronavesViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var numDocText: SDCTextField!
    @IBOutlet weak var numExpepText: SDCTextField!
    
    @IBOutlet weak var solicitarView: UIView!
    @IBOutlet weak var numExpepLabel: UILabel!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var validaPartida: [ValidaPartidaCRIEntity] = []
    
    var maxLength = 0
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    
    
    var codGrupoLibroArea: String = ""
    
    var titleBar: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["Partida", "Matrícula"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var indexSelect = 0
    var indexTypeOficina = 0
    var valDone: Bool = true
    
    var tipoNumero: String = ""
    
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var areaRegId: String = ""
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var precOfic: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    private lazy var presenter: PublicidadCertiCargaGraAeronavesPresenter = {
       return PublicidadCertiCargaGraAeronavesPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        hideKeyboardWhenTappedAround()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        //presenter.didLoad()
        
        self.presenter.didiListadoOficina()
        configTextField()
    }
    
    func configTextField() {
        numDocText.delegate = self

        numExpepText.delegate = self
        numExpepText.maxLengths = 20
        numExpepText.valueType = .alphaNumeric
        setupStyle(myView: solicitarView)
    }
    
    func setupStyle(myView: UIView) {
        let grayColor = UIColor.gray.withAlphaComponent(0.5)
        myView.layer.borderColor = grayColor.cgColor
        myView.layer.borderWidth = 1.0
        myView.layer.cornerRadius = 4.0
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
      //  self.scrollview.contentInset = contentInsets
     //   self.scrollview.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
      //  self.scrollview.contentInset = contentInsets
       // self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        //self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoDocumentoPickerView.tag = 1
        self.areaRegistralPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbarTypeOficina()

        self.numDocText.borderAndPaddingLeftAndRight()
        self.numExpepText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.participantePickerView.tag = 2
                
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
        
        
        /*
        let amount = String(format: "S/ %.2f", (montoCalc as NSString).doubleValue)
                        
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
         */
        self.numDocText.delegate = self
       // self.numDocText.inputAccessoryView = createToolbarGender()
        
        //00166251
        self.numDocText.placeholder = "Nro. de Partida"
        
        if (certificadoId == "46"  || certificadoId == "96") {
            self.numExpepLabel.text = "Expediente"
            self.numExpepText.placeholder = "Ingresa Nº de Expediente"
            self.numExpepText.valueType = .onlyNumbers
            self.numExpepText.maxLength = 20
        
        } else  if (certificadoId == "47"  || certificadoId == "97") {
            self.numExpepLabel.text = "Embarcación"
            self.numExpepText.placeholder = "Ingresa Nombre de Embarcación"
            self.numExpepText.valueType = .alphaNumericWithSpace
            self.numExpepText.maxLength = 100
        }
        
        
    }
    
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        
        
        
        //self.tipo = "T"
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
      //  self.tipoDocumentosEntities = arrayTipoDocumentos
     //   for tipoDocumento in arrayTipoDocumentos {
     //       self.tipoDocumentos.append(tipoDocumento.descripcion)
    //    }
        
    }
    
    
    func createToolbarTypeOficina() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedTypeOficina))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func donePressedTypeOficina() {
        self.typeOficinaTextField.text = oficinas[indexTypeOficina].Nombre
        self.typeOficinaTextField.resignFirstResponder()
        
        self.regPubId = self.oficinas[indexTypeOficina].RegPubId
        self.oficRegId = self.oficinas[indexTypeOficina].OficRegId
        
        valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
        print("valoficinaOrigen:---:>>>",valoficinaOrigen)
    }
    
    
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func donePressed() {
        self.areaRegistralText.text = tipoDocumentos[indexSelect]
        self.areaRegistralText.resignFirstResponder()
        self.typeDocument = self.tipoDocumentos[indexSelect]
    
        self.isValidDocument = false
        if (self.typeDocument == "Matrícula") {
            self.maxLength = 7
            tipoNumero = "M"
            self.numDocText.maxLengths = 7
            //5
            //self.numberDocumentTextField.keyboardType = .numberPad
            self.numDocText.placeholder = "Nro. de Matrícula"
        } else if (self.typeDocument == "Partida") {
            tipoNumero = "P"
            //1
            self.maxLength = 10
            self.numDocText.maxLengths = 10
            self.numDocText.placeholder = "Ingresa Nº de Partida"
           // self.numberDocumentTextField.keyboardType = .alphabet
        } else {
            self.maxLength = 20
            self.numDocText.maxLengths = 20
            self.numDocText.placeholder = "Fecha Nacimiento"
          //  self.numberDocumentTextField.keyboardType = .alphabet
     
            self.isValidDocument = true
        }
        
        self.isChecked = true
        self.numDocText.text = .empty
        self.numExpepText.text = .empty
        self.pagarView.primaryButton()
    }
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
     //   self.nombreText.text          = usuario.nombres
     //   self.apellidoPaternoText.text = usuario.priApe
     //   self.apellidoMaternoText.text = usuario.segApe
     //   self.nameRazonSocialText.text = usuario.nombres
     //   self.numDocText.text          = usuario.nroDoc
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            //self.areaRegistralText.text = "DNI"
              typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
           // self.areaRegistralText.text = "CE"
              typeDocument = "03"
        } else {
        }
        
        /*
        var numPartida: String = ""
        var fichaId: String = ""
        var tomoId: String = ""
        var fojaId: String = ""
        var ofiSARP: String = ""
        var coServicio: String = ""
        var coTipoRegis: String = ""
        */
        
        //save Solictud
        
        var numero1 = Int(countCantPag) ?? 0
        var numero2 = Int(countCantPagExo) ?? 0
        var numPagina = numero1 + numero2
        
        var nuAsieSelectSARPvalid = nuAsieSelectSARP.take(1)
        
        if nuAsieSelectSARPvalid == "P"{
        }else{
            nuAsieSelectSARP = ""
        }
        
        print("certificadoId::",certificadoId)
        print("codArea::",codArea)
        print("codLibro::",codLibro)
        print("valoficinaOrigen::",valoficinaOrigen)
        print("refNumPart::",refNumPart)
        print("numPartida::",numPartida)
        print("numeroPlaca::",numeroPlaca)
        print("usuario.tipo::",usuario.tipo)
        print(" usuario.priApe::", usuario.priApe)
        print("usuario.segApe::",usuario.segApe)
        print("usuario.nombres::",usuario.nombres)
        print("usuario.tipoDoc::",usuario.tipoDoc)
        print("usuario.nroDoc::",usuario.nroDoc)
        print("usuario.email::",usuario.email)
        print("montoCalc::",montoCalc)
        print("countCantPag::",countCantPag)
        print("countCantPagExo::",countCantPagExo)
        print("numPagina::",String(numPagina))
        print("nuAsieSelectSARP::",nuAsieSelectSARP)
        print("imPagiSIR::",imPagiSIR)
        print("nuSecu::",nuSecu)
        
       // presenter.postDetalleAsientosLiteralSaveSolicitud(codCerti: certificadoId, codArea: codArea, codLibro: codLibro, oficinaOrigen: valoficinaOrigen, refNumPart: refNumPart, partida: numPartida, placa: numeroPlaca, tpoPersona: usuario.tipo, apePaterno: usuario.priApe, apeMaterno: usuario.segApe, nombre: usuario.nombres, razSoc: "", tpoDoc: usuario.tipoDoc, numDoc: usuario.nroDoc, email: usuario.email, costoServicio: montoCalc, cantPaginas: countCantPag, cantPaginasExon: countCantPagExo, paginasSolicitadas: String(numPagina), nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecu, ipRemote: "", sessionId: "", usrId: "APPSNRPIOS")
        
        print("tipoDoc:>>",usuario.tipoDoc)
       // self.emailText.text = usuario.email
        
       // public let tipoDoc: String
       // public let nroDoc: String
        
        /*
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: "S/ \(nsolMonLiq).00", attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
        */
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.numExpepText.resignFirstResponder()
        if !(numDocText.text?.isEmpty ?? true) {
            self.textFieldShouldReturn(numDocText)
        }
        
    }
    
    @objc private func onTapTerminoCondicionesLabel(){
        if let url = URL(string: "https://www.sunarp.gob.pe/politicas-privacidad.aspx") {
            UIApplication.shared.open(url)
        }
    }
    
    @objc private func onTapPagarView(){
        if (self.isChecked) {
            //loaderView(isVisible: true)
            //presenter.getVisaKeys()
            
            let usuario = UserPreferencesController.usuario()
            
            
            print("regPubId::",regPubId)
            print("oficRegId::",oficRegId)
            
            print("certificadoId::",certificadoId)
            print("codArea::",codArea)
            print("codLibro::",codLibro)
            print("valoficinaOrigen::",valoficinaOrigen)
            print("refNumPart::",refNumPart)
            print("numPartida::",numPartida)
            print("numeroPlaca::",numeroPlaca)
            print("usuario.tipo::",usuario.tipo)
            print(" usuario.priApe::", usuario.priApe)
            print("usuario.segApe::",usuario.segApe)
            print("usuario.nombres::",usuario.nombres)
            print("usuario.tipoDoc::",usuario.tipoDoc)
            print("usuario.nroDoc::",usuario.nroDoc)
            print("usuario.email::",usuario.email)
            print("montoCalc::",montoCalc)
           // print("countCantPag::",countCantPag)
            print("nuAsieSelectSARP::",nuAsieSelectSARP)
            print("imPagiSIR::",imPagiSIR)
            print("nuSecu::",nuSecu)
            print("tipoNumero::",tipoNumero)
            print("numDocText::",self.numDocText.text ?? "")
            print("areaRegId::",codGrupoLibroArea)
            
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            
            if (certificadoId == "46"  || certificadoId == "96")  {
                //00166251
                //areaRegId = codGrupoLibroArea
                 presenter.getValidaPartidaCGA(regPubId: regPubId, oficRegId: oficRegId, areaRegId: codGrupoLibroArea, tipoNumero: tipoNumero, numPartida: self.numDocText.text ?? "")
            } else if (certificadoId == "47"  || certificadoId == "97")  {
                //00166251
                //areaRegId = codGrupoLibroArea
                 presenter.getValidaPartidaCGEP(regPubId: regPubId, oficRegId: oficRegId, areaRegId: codGrupoLibroArea, tipoNumero: tipoNumero, numPartida: self.numDocText.text ?? "")
            }

        }
    }
    

    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //"\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        valDone = true
        self.indexSelect = index
    
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func changeTypeDocument(index: Int) {
        
        valDone = false
        self.indexSelect = index
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    
    func loadPartidaCRI(solicitud: ValidaPartidaCRIEntity) {
       // self.validaPartida = solicitud
        print("estadoOOO::-->>",solicitud.estado)
        if solicitud.estado == 0 {
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
            
            let usuario = UserPreferencesController.usuario()
            
            if (certificadoId == "46"  || certificadoId == "96")  {
                
                if tipoNumero == "M" {
                   
                    vc.paymentItem.codCerti = certificadoId
                    vc.paymentItem.codArea = areaRegId
                    vc.paymentItem.oficinaOrigen = valoficinaOrigen
                    vc.paymentItem.refNumPart = String(solicitud.refNumPart )
                    vc.paymentItem.partida = solicitud.numPartida
                    vc.paymentItem.matricula = self.numDocText.text ?? ""
                    vc.paymentItem.expediente = self.numExpepText.text ?? ""
                    vc.paymentItem.tpoPersona = "N"
                    vc.paymentItem.apePaterno = usuario.priApe
                    vc.paymentItem.apeMaterno = usuario.segApe
                    vc.paymentItem.nombre = usuario.nombres
                    vc.paymentItem.razSoc = ""
                    vc.paymentItem.tpoDoc = usuario.tipoDoc
                    vc.paymentItem.numDoc = usuario.nroDoc
                    vc.paymentItem.email = usuario.email
                    vc.paymentItem.costoServicio = precOfic
                    vc.paymentItem.costoTotal = precOfic
                    vc.paymentItem.ipRemote = ""
                    vc.paymentItem.sessionId = ""
                    vc.paymentItem.usrId = Constant.API_USRID
                    
                    
                }else if tipoNumero == "P" {
                    
                    vc.paymentItem.codCerti = certificadoId
                    vc.paymentItem.codArea = areaRegId
                    vc.paymentItem.oficinaOrigen = valoficinaOrigen
                    vc.paymentItem.refNumPart = String(solicitud.refNumPart)
                    vc.paymentItem.partida = self.numDocText.text ?? ""
                    vc.paymentItem.matricula = ""
                    vc.paymentItem.expediente = self.numExpepText.text ?? ""
                    vc.paymentItem.tpoPersona = "N"
                    vc.paymentItem.apePaterno = usuario.priApe
                    vc.paymentItem.apeMaterno = usuario.segApe
                    vc.paymentItem.nombre = usuario.nombres
                    vc.paymentItem.razSoc = ""
                    vc.paymentItem.tpoDoc = usuario.tipoDoc
                    vc.paymentItem.numDoc = usuario.nroDoc
                    vc.paymentItem.email = usuario.email
                    vc.paymentItem.costoServicio = precOfic
                    vc.paymentItem.costoTotal = precOfic
                    vc.paymentItem.ipRemote = ""
                    vc.paymentItem.sessionId = ""
                    vc.paymentItem.usrId = Constant.API_USRID
                    
                }
                
                
            } else if (certificadoId == "47"  || certificadoId == "97")  {
                if tipoNumero == "M" {
                  
                    vc.paymentItem.codCerti = certificadoId
                    vc.paymentItem.codArea = areaRegId
                    vc.paymentItem.oficinaOrigen = valoficinaOrigen
                    vc.paymentItem.refNumPart = String(solicitud.refNumPart)
                    vc.paymentItem.partida = solicitud.numPartida
                    vc.paymentItem.matricula = self.numDocText.text ?? ""
                    vc.paymentItem.nomEmbarcacion = self.numExpepText.text ?? ""
                    vc.paymentItem.tpoPersona = "N"
                    vc.paymentItem.apePaterno = usuario.priApe
                    vc.paymentItem.apeMaterno = usuario.segApe
                    vc.paymentItem.nombre = usuario.nombres
                    vc.paymentItem.razSoc = ""
                    vc.paymentItem.tpoDoc = usuario.tipoDoc
                    vc.paymentItem.numDoc = usuario.nroDoc
                    vc.paymentItem.email = usuario.email
                    vc.paymentItem.costoServicio = precOfic
                    vc.paymentItem.costoTotal = precOfic
                    vc.paymentItem.ipRemote = ""
                    vc.paymentItem.sessionId = ""
                    vc.paymentItem.usrId = Constant.API_USRID
                    
                }else if tipoNumero == "P" {
                   
                    vc.paymentItem.codCerti = certificadoId
                    vc.paymentItem.codArea = areaRegId
                    vc.paymentItem.oficinaOrigen = valoficinaOrigen
                    vc.paymentItem.refNumPart = String(solicitud.refNumPart)
                    vc.paymentItem.partida = self.numDocText.text ?? ""
                    vc.paymentItem.matricula = ""
                    vc.paymentItem.nomEmbarcacion = self.numExpepText.text ?? ""
                    vc.paymentItem.tpoPersona = "N"
                    vc.paymentItem.apePaterno = usuario.priApe
                    vc.paymentItem.apeMaterno = usuario.segApe
                    vc.paymentItem.nombre = usuario.nombres
                    vc.paymentItem.razSoc = ""
                    vc.paymentItem.tpoDoc = usuario.tipoDoc
                    vc.paymentItem.numDoc = usuario.nroDoc
                    vc.paymentItem.email = usuario.email
                    vc.paymentItem.costoServicio = precOfic
                    vc.paymentItem.costoTotal = precOfic
                    vc.paymentItem.ipRemote = ""
                    vc.paymentItem.sessionId = ""
                    vc.paymentItem.usrId = Constant.API_USRID
                    
                }
            }
            
            //vc.paymentItem = paymentItem
            dump(vc.paymentItem)
            //Others data information
            vc.areaRegId = areaRegId
            vc.certificadoId = certificadoId
            vc.precOfic = precOfic
            vc.titleBar = titleBar
            vc.tipoNumero = tipoNumero
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
        
            let alert = UIAlertController(title: "SUNARP", message: solicitud.msj, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
   }
    
    func validateTypeDocument() {
        self.numDocument = self.numDocText.text ?? ""
     //   self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty) {
                self.presenter.validarDni()
                self.numDocText.resignFirstResponder()
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
                self.numDocText.resignFirstResponder()
            }
        }
    }
    
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            self.pagarView.primaryButton()
            
            
             self.apePatPN = jsonValidacionDni.apellidoPaterno
             self.apeMatPN = jsonValidacionDni.apellidoMaterno
             self.nombPN = jsonValidacionDni.nombres
            // self.razSocPN = jsonValidacionDni.type
           //  self.tipoDocPN = jsonValidacionDni.type
             self.numDocPN = jsonValidacionDni.dni
            
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            
            
            self.apePatPN = jsonValidacionCe.strPrimerApellido
            self.apeMatPN = jsonValidacionCe.strSegundoApellido
            self.nombPN = jsonValidacionCe.strNombres
            
            self.pagarView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PublicidadCertiCargaGraAeronavesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
          //  return  self.tipoDocumentosEntities.count
            return  self.tipoDocumentos.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            
            //return self.tipoDocumentosEntities[row].descripcion
            return self.tipoDocumentos[row]
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            //self.changeTypeOficina(index: row)
            indexTypeOficina = row
           // self.typeOficinaTextField.text = oficinas[row].Nombre
           // self.typeOficinaTextField.resignFirstResponder()
            
        case 2:
            indexSelect = row
            // self.changeTypeDocument(index: row)
           // self.areaRegistralText.text = tipoDocumentos[row]
           // self.areaRegistralText.resignFirstResponder()
            
        default:
            return
        }
        
    }
    

}

//MARK: - VISANET DELEGATE SECTION

extension PublicidadCertiCargaGraAeronavesViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}







extension PublicidadCertiCargaGraAeronavesViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (numDocText ==  textField) {
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            let regex = "^[a-zA-Z0-9-]*$"
            let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
            let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
            
            if(updatedText.count > maxLength){
                return false
            }
            
            let newStr = updatedText
            if (!textTest.evaluate(with: newStr)) {
                return false
            }
            
            var text = textFieldText
            if range.length == Int.zero {
                text.append(contentsOf: string)
            } else {
                text = String(text.dropLast())
            }
           
            textField.text = text.uppercased()
            if (count == maxLength) {
                self.validateTypeDocument()
                textField.resignFirstResponder()
            }
        }else {
            if let sdcTextField = textField as? SDCTextField {
                return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
            }
        }

        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            // Si no hay texto, llenar con ceros y actualizar el campo de texto
            textField.text = "00000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text), let formattedValue = String(format: "%08d", intValue) as NSString? {
            textField.text = formattedValue as String
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
}


/*
private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}


*/
