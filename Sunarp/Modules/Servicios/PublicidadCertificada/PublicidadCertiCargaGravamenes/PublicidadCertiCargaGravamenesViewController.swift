//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadCertiCargaGravamenesViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    
    @IBOutlet weak var folioText: SDCTextField!
    @IBOutlet weak var tomoText: SDCTextField!
    @IBOutlet weak var numDocText: SDCTextField!
    
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var maxLength = 10
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    
    var codGrupoLibroArea: String = ""
    
    var codLibro: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    var validaPartida: [ValidaPartidaCRIEntity] = []
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var indexOficinaSelect = 0
    var indexSelect = 0
    var valDone: Bool = true
    
    var precOfic: String = ""
    
    var valorNumPartida: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var areaRegId: String = ""
    
    var montoCalc: String = "0.0"
    
    var titleBar: String = ""
    
    var numeroPlaca: String = ""
    
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    
    var codGrupo: String = ""
    
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var tipoBusqueda: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    private lazy var presenter: PublicidadCertiCargaGravamenesPresenter = {
       return PublicidadCertiCargaGravamenesPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        hideKeyboardWhenTappedAround()
        setValues()
        self.presenter.didLoad()
        self.presenter.didLoadLibroRegistral()
        self.presenter.didiListadoOficina()
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
      //  self.scrollview.contentInset = contentInsets
     //   self.scrollview.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
      //  self.scrollview.contentInset = contentInsets
       // self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.folioText.borderAndPaddingLeftAndRight()
        self.tomoText.borderAndPaddingLeftAndRight()
        self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoDocumentoPickerView.tag = 1
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbar()
        
        self.areaRegistralPickerView.tag = 2
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbarArea()
        
        
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.participantePickerView.tag = 2
        
        self.folioText.delegate = self
        self.tomoText.delegate = self
        self.numDocText.delegate = self
        
        self.folioText.keyboardType = .numberPad
        self.folioText.valueType = .onlyNumbers
        self.folioText.maxLengths = 6
        
        self.tomoText.keyboardType = .numberPad
        self.tomoText.valueType = .onlyNumbers
        self.tomoText.maxLengths = 6
        
        self.numDocText.keyboardType = .alphabet
        self.numDocText.valueType = .alphaNumeric
        self.numDocText.maxLengths = 10

        tipoBusqueda = "01"
    }
    
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        //self.tipo = "T"
        
        
        toPersonNatuView.isHidden = true
        toPersonJuriView.isHidden = false
    }
    
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
          self.obtenerLibroEntities = arrayTipoDocumentos
     }

                     
    
    @objc private func onTapToConfirmViewAnio() {

        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        toPersonNatuView.isHidden = true
        toPersonJuriView.isHidden = false
        
        tipoBusqueda = "01"
    }
  
    @objc private func onTapToConfirmViewNum() {

        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toPersonNatuView.isHidden = false
        toPersonJuriView.isHidden = true
        
        tipoBusqueda = "02"
    }
    
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    
    func createToolbarArea() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneAreaPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func donePressed() {
        self.typeOficinaTextField.text = oficinas[indexOficinaSelect].Nombre
        self.typeOficinaTextField.resignFirstResponder()
        self.regPubId = self.oficinas[indexOficinaSelect].RegPubId
        self.oficRegId = self.oficinas[indexOficinaSelect].OficRegId
        valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
        print("valoficinaOrigen:---:>>>",valoficinaOrigen)
        self.isValidSolicitar()
        self.isChecked = true
        
    }
    
    
    @objc func doneAreaPressed() {
        print("doneAreaPressed")
        self.areaRegistralText.text = obtenerLibroEntities[indexSelect].nomLibro
        self.areaRegistralText.resignFirstResponder()
        self.typeNameDocument = self.obtenerLibroEntities[indexSelect].nomLibro
        codLibro = self.obtenerLibroEntities[indexSelect].codLibro
        areaRegId = self.obtenerLibroEntities[indexSelect].areaRegId
        codGrupo = self.obtenerLibroEntities[indexSelect].grupoLibroArea
       
    }
    
    
    func setValues() {
        let usuario = UserPreferencesController.usuario()
        
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
        //    self.areaRegistralText.text = "DNI"
              typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
        //    self.areaRegistralText.text = "CE"
              typeDocument = "03"
        } else {
        }
        
        print("tipoDoc:>>",usuario.tipoDoc)
       
    }
    
    func isValidSolicitar() {
        var partida = self.numDocText.text ?? ""
        var tomo = self.tomoText.text ?? ""
        var folio = self.folioText.text ?? ""
        
        if indexOficinaSelect >= 0 && (partida != "" || (tomo != "" && folio != "")) {
            self.pagarView.primaryButton()
        }
        else {
            self.pagarView.primaryDisabledButton()
        }

    }
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.folioText.resignFirstResponder()
        self.tomoText.resignFirstResponder()
        isValidSolicitar()
    }
    
    @objc private func onTapPagarView(){
        if (self.isChecked) {

            let usuario = UserPreferencesController.usuario()
            
            if tipoBusqueda == "01"{
                codLibro = "-"
                valorNumPartida = self.numDocText.text ?? ""
                var formato = "%08d"
                if let intValue = Int(valorNumPartida), let formattedValue = String(format: formato, intValue) as NSString? {
                    valorNumPartida = formattedValue as String
                } else {
                    // Si no es numérico, convertir a mayúsculas
                    valorNumPartida = valorNumPartida.uppercased()
                }
                self.numDocText.text = valorNumPartida
                
            }else{
                var formato = "%06d"
                var tomo = self.tomoText.text ?? ""
                var folio = self.folioText.text ?? ""
                if let intValue = Int(tomo), let formattedValue = String(format: formato, intValue) as NSString? {
                    tomo = formattedValue as String
                }
                else {
                    tomo = ""
                }
                self.tomoText.text = tomo
                if let intValue = Int(folio), let formattedValue = String(format: formato, intValue) as NSString? {
                    folio = formattedValue as String
                }
                else {
                    folio = ""
                }
                self.folioText.text = folio
                valorNumPartida = "\(self.tomoText.text ?? "")-\(self.folioText.text ?? "")"
            }
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            self.loaderView(isVisible: true)
            presenter.getValidaPartidaCargaG(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: valorNumPartida, codGrupo: codGrupoLibroArea, codigoLibro: codLibro, tipoBusqueda: tipoBusqueda)

            
            
        }
    }
    

    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz // "\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        
        valDone = true
        self.indexOficinaSelect = index
        
    
    }
    
    func loadPartidaCargaG(solicitud: ValidaPartidaCRIEntity) {
        self.loaderView(isVisible: false)
        if solicitud.estado == 0 {
            
           
            print("self.tipoPer ::>>",self.tipoPer)
            self.tipoPer = "N"
           
            //save Solictud
            
            let tomo = self.tomoText.text ?? ""
            let folio = self.folioText.text ?? ""
            
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.partida = solicitud.numPartida
                vc.paymentItem.refNumPart = "\(solicitud.refNumPart)"
                vc.paymentItem.codLibro = solicitud.codigoLibro // codLibro
                vc.paymentItem.tomo = tomo
                vc.paymentItem.folio = folio
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                vc.paymentItem.usrId = Constant.API_USRID // "\(usuario.idUser)"
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.titleText = titleBar.text ?? "Sin definir"
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }

        }else{
            print("no encontrado: ", solicitud.msj)
            self.showMessageAlert(message: solicitud.msj)
        }
        
   }
    
    func changeTypeDocument(index: Int) {
        
        valDone = false
        print("changeTypeDocument")
        self.indexSelect = index
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    
    
    func validateTypeDocument() {
        self.numDocument = self.numDocText.text ?? ""
     //   self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty) {
                self.presenter.validarDni()
                self.numDocText.resignFirstResponder()
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
                self.numDocText.resignFirstResponder()
            }
        }
    }
    
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
           // self.apellidoPaternoText.text = jsonValidacionDni.apellidoPaterno
           // self.apellidoMaternoText.text = jsonValidacionDni.apellidoMaterno
            self.pagarView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
          //  self.apellidoPaternoText.text = jsonValidacionCe.strPrimerApellido
          //  self.apellidoMaternoText.text = jsonValidacionCe.strSegundoApellido
            self.pagarView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PublicidadCertiCargaGravamenesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            return  self.obtenerLibroEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            return self.obtenerLibroEntities[row].nomLibro
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            
        case 2:
            
            self.changeTypeDocument(index: row)
            
        default:
            return
        }
        
    }
    

}

//MARK: - VISANET DELEGATE SECTION

extension PublicidadCertiCargaGravamenesViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension PublicidadCertiCargaGravamenesViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if let sdcTextField = textField as? SDCTextField {
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            textField.text = textFieldText.uppercased()
        
            if (count == maxLength) {
//                self.validateTypeDocument()
                textField.resignFirstResponder()
            }
            
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            isValidSolicitar()
            return true
        }
        
        // Verificar si el texto es numérico
        var formato = "%08d"
        if tipoBusqueda == "02"{
            formato = "%06d"
        }
        if let intValue = Int(text), let formattedValue = String(format: formato, intValue) as NSString? {
            textField.text = formattedValue as String
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        isValidSolicitar()
        return true
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        isValidSolicitar()
    }
}




