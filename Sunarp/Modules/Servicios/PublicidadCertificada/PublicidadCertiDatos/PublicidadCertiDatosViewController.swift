//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit

class PublicidadCertiDatosViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var nombreText: SDCTextField!
    @IBOutlet weak var apellidoPaternoText: SDCTextField!
    @IBOutlet weak var apellidoMaternoText: SDCTextField!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var nameRazonSocialText: SDCTextField!
    @IBOutlet weak var numDocText: SDCTextField!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var nsolMonLiq: String = "0.0"
    
    
    var maxLength = 8
    
    var indexOficinaSelect = 0
    var indexSelect = 0
    var valDone: Bool = true
    var valJuri: Bool = true
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var areaRegId: String = ""
    
    var precOfic: String = ""
    
    var titleBar: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    
    
    
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    @IBOutlet weak var nombreApellidosLabel: UILabel!
    @IBOutlet weak var ViewTipoDocument: UIView!
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    private lazy var presenter: PublicidadCertiDatosPresenter = {
        return PublicidadCertiDatosPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        hideKeyboardWhenTappedAround()
        setValues()
        presenter.didLoad()
        setupStyle(myView: ViewTipoDocument)
        self.presenter.didiListadoOficina()
    }
    
    func setupStyle(myView: UIView) {
        let grayColor = UIColor.gray.withAlphaComponent(0.5)
        myView.layer.borderColor = grayColor.cgColor
        myView.layer.borderWidth = 1.0
        myView.layer.cornerRadius = 4.0
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        // self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.nombreText.borderAndPaddingLeftAndRight()
        self.apellidoPaternoText.borderAndPaddingLeftAndRight()
        self.apellidoMaternoText.borderAndPaddingLeftAndRight()
        //self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoDocumentoPickerView.tag = 1
        self.areaRegistralPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbar()
        
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        
        self.areaRegistralText.delegate = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
    
        self.nameRazonSocialText.borderAndPaddingLeftAndRight()
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.participantePickerView.tag = 2
    
        configureTextField()
    }
    
    func configureTextField(){
        self.numDocText.delegate = self
        self.numDocText.maxLengths = 8
        self.nameRazonSocialText.delegate = self
        
        self.nombreText.delegate = self
        self.apellidoPaternoText.delegate = self
        self.apellidoMaternoText.delegate = self
        
        self.nombreText.keyboardType = .alphabet
        self.apellidoPaternoText.keyboardType = .alphabet
        self.apellidoMaternoText.keyboardType = .alphabet
        
        self.nombreText.valueType = .alphaNumericWithSpace
        self.apellidoPaternoText.valueType = .alphaNumericWithSpace
        self.apellidoMaternoText.valueType = .alphaNumericWithSpace
        self.nameRazonSocialText.valueType = .razonSocial
        
        self.nombreText.maxLengths = 50
        self.apellidoPaternoText.maxLengths = 50
        self.apellidoMaternoText.maxLengths = 50
        self.nameRazonSocialText.maxLengths = 50
        
        self.numDocText.isEnabled = true
        self.nameRazonSocialText.isEnabled = true
        self.nombreText.isEnabled = true
        self.apellidoPaternoText.isEnabled = true
        self.apellidoMaternoText.isEnabled = true
        
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        //self.tipo = "T"
        
        valJuri = false
        tipoPer = "N"
        tipPerPN = "N"
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.nombreAbrev)
        }
        for (indice, valor) in tipoDocumentosEntities.enumerated() {
            
            if tipoPer == "N" && valor.nombreAbrev == "DNI" {
                self.areaRegistralText.text = tipoDocumentosEntities[indice].nombreAbrev
                self.typeDocument = tipoDocumentosEntities[indice].tipoDocId
            }
            
            if tipoPer == "J" && valor.nombreAbrev == "R.U.C." {
                self.areaRegistralText.text = tipoDocumentosEntities[indice].nombreAbrev
                self.typeDocument = tipoDocumentosEntities[indice].tipoDocId
            }
        }
        
        // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        self.areaRegistralText.resignFirstResponder()
    }
    
    
    @objc private func onTapToConfirmViewAnio() {
        self.isChecked = false
        self.pagarView.primaryDisabledButton()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        toPersonNatuView.isHidden = false
        toPersonJuriView.isHidden = true
        
        tipPerPN = "N"
        tipoPer = "N"
        self.areaRegistralText.text = ""
        self.numDocText.text = ""
        self.nameRazonSocialText.text = ""
        self.nombreText.text = ""
        self.apellidoPaternoText.text = ""
        self.apellidoMaternoText.text = ""
        presenter.didLoad()
        nombreApellidosLabel.isHidden = false
        valJuri = false
        
        self.maxLength = 8
        self.numDocText.maxLengths = 8
        self.numDocText.valueType = .onlyNumbers
        self.numDocText.keyboardType = .numberPad
        clearVariables()
    }
    
    @objc private func onTapToConfirmViewNum() {
        self.isChecked = false
        self.pagarView.primaryDisabledButton()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toPersonNatuView.isHidden = true
        toPersonJuriView.isHidden = false
        tipPerPN = "J"
        tipoPer = "J"
        self.areaRegistralText.text = ""
        self.areaRegistralText.text = ""
        self.numDocText.text = ""
        self.nameRazonSocialText.text = ""
        self.nombreText.text = ""
        self.apellidoPaternoText.text = ""
        self.apellidoMaternoText.text = ""
        nombreApellidosLabel.isHidden = true
        presenter.didLoadJur()
        valJuri = true
        
        self.numDocText.keyboardType = .numberPad
        self.maxLength = 11
        self.numDocText.maxLengths = 11
        self.numDocText.valueType = .onlyNumbers
        clearVariables()
    }
    
    func clearVariables(){
        self.apePatPN = .empty
        self.apeMatPN = .empty
        self.nombPN = .empty
        self.razSocPN = .empty
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func donePressed() {
        desblockTextField()
        if valDone == true {
            
            self.typeOficinaTextField.text = oficinas[indexOficinaSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            print("typeOficina = ", self.typeOficinaTextField.text ?? "")
            
            self.regPubId = self.oficinas[indexOficinaSelect].RegPubId
            self.oficRegId = self.oficinas[indexOficinaSelect].OficRegId
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            
            self.areaRegistralText.isUserInteractionEnabled = true
            
            print("valoficinaOrigen:---:>>>",valoficinaOrigen)
            
        }else{
            
            self.areaRegistralText.text = tipoDocumentosEntities[indexSelect].nombreAbrev
            self.areaRegistralText.resignFirstResponder()
            
            self.typeNameDocument = self.tipoDocumentosEntities[indexSelect].tipoDocId
            self.typeDocument =  self.tipoDocumentosEntities[indexSelect].tipoDocId
            print("self.typeNameDocument:---:>>>",self.typeNameDocument)
        
            
            self.isValidDocument = false
            if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
                self.maxLength = 8
                self.numDocText.keyboardType = .numberPad
                self.numDocText.maxLengths = 8
                self.numDocText.valueType = .onlyNumbers
            } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
                self.maxLength = 9
                self.numDocText.maxLengths = 9
                self.numDocText.valueType = .alphaNumeric
                self.numDocText.keyboardType = .alphabet
            } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_RUC) {
                self.maxLength = 11
                self.numDocText.maxLengths = 11
                self.numDocText.valueType = .onlyNumbers
                self.numDocText.keyboardType = .numberPad
            } else if (self.typeNameDocument == "00" || self.typeNameDocument == "90" || self.typeNameDocument == "91") || self.typeNameDocument == "92" {
                self.numDocText.maxLengths = 10
                self.numDocText.valueType = .alphaNumeric
                self.numDocText.keyboardType = .alphabet
            } else {
                if valJuri == true {
                    self.maxLength = 10
                    self.numDocText.maxLengths = 10
                } else {
                    self.maxLength = 20
                    self.numDocText.maxLengths = 20
                    self.numDocText.keyboardType = .alphabet
                }
            }
            self.areaRegistralText.resignFirstResponder()
            
            cleanTextField()
        }
        self.validarBotonSolicitar()
    }
    
    func setValues() {
        let usuario = UserPreferencesController.usuario()
        
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            // self.areaRegistralText.text = "DNI"
            typeDocument = "09"
            tipoDocPN = "09"
        } else if (usuario.tipoDoc == "03") {
            // self.areaRegistralText.text = "CE"
            typeDocument = "03"
            tipoDocPN = "03"
        } else {
        }
        
        
        //save Solictud
        

        
        print("tipoDoc:>>",usuario.tipoDoc)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
        self.nameRazonSocialText.resignFirstResponder()
        if !(numDocText.text?.isEmpty ?? true) {
            self.textFieldShouldReturn(numDocText)
        }
    }
    
    func isValidarDatos() -> Bool{
        if(tipoPer == "N"){
            guard let document = numDocText.text, !document.isEmpty ,
                  let name = nombreText.text, !name.isEmpty ,
                  let apePatPN = apellidoPaternoText.text,!apePatPN.isEmpty,
                  let oficina = typeOficinaTextField.text,!oficina.isEmpty  else {
                return false
            }
        }else if (tipoPer == "J"){
            guard let document = numDocText.text, !document.isEmpty ,
                  let name = nameRazonSocialText.text, !name.isEmpty ,
                  let oficina = typeOficinaTextField.text,!oficina.isEmpty else {
                return false
            }
        }
        return true
    }
    
    @objc private func onTapPagarView(){
        
        if (self.isValidarDatos()) {
            
            let storyboard = UIStoryboard(name: "Payments", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = ""
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.tipPerPN = tipPerPN
                vc.paymentItem.apePatPN = apePatPN
                vc.paymentItem.apeMatPN = apeMatPN
                
                vc.paymentItem.nombPN = nombPN
                vc.paymentItem.numDocPN = numDocText.text ?? numDocPN
                vc.paymentItem.tipoDocPN = typeDocument
                vc.paymentItem.razSocPN = nameRazonSocialText.text ?? .empty
                
              
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                vc.paymentItem.usrId = "\(usuario.idUser)"
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                vc.titleText = titleBar.text ?? "Sin definir"
                
                dump(vc.paymentItem)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
    }
    
    
    func changeTypeOficina(index: Int) {
        valDone = true
        self.indexOficinaSelect = index
    }
    
    func changeTypeDocument(index: Int) {
        valDone = false
        self.indexSelect = index
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
    func cleanTextField() {
        [apellidoPaternoText,apellidoMaternoText,nombreText,numDocText,nameRazonSocialText].forEach { item in
            item?.text = .empty
        }
        self.numDocText.resignFirstResponder()
        self.pagarView.primaryDisabledButton()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        validarBotonSolicitar()
    }
    
    func validarBotonSolicitar() {

            
        if(tipoPer == "N"){
            if (numDocText.text ?? "" != "" && nombreText.text ?? "" != "" && apellidoPaternoText.text ?? "" != "" && typeOficinaTextField.text ?? "" != "" ){
                self.pagarView.primaryButton()
            } else {
                self.pagarView.primaryDisabledButton()
            }
        }else if (tipoPer == "J"){
            if (numDocText.text ?? "" != ""  && nameRazonSocialText.text ?? "" != "" && typeOficinaTextField.text ?? "" != "" ){
                self.pagarView.primaryButton()
            } else {
                self.pagarView.primaryDisabledButton()
            }
        }
       
    }
    
    
    func validateTypeDocument() {
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty) {
                self.presenter.validarDni()
                
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
                
            }
        }
        else {
            validarBotonSolicitar()
        }
        self.numDocText.resignFirstResponder()
        self.numDocText.text  = numDocument
    }
    
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            //type
            self.nombreText.text = jsonValidacionDni.nombres
            self.apellidoPaternoText.text = jsonValidacionDni.apellidoPaterno
            self.apellidoMaternoText.text = jsonValidacionDni.apellidoMaterno
            
            
            // self.tipPerPN = jsonValidacionDni.type
            self.apePatPN = jsonValidacionDni.apellidoPaterno
            self.apeMatPN = jsonValidacionDni.apellidoMaterno
            self.nombPN = jsonValidacionDni.nombres
            // self.razSocPN = jsonValidacionDni.type
            //  self.tipoDocPN = jsonValidacionDni.type
            self.numDocPN = jsonValidacionDni.dni
            
            
            blockTextField()
            if typeOficinaTextField.text ?? "" != "" {
                self.pagarView.primaryButton()
                self.isChecked = true
            }
            else {
                self.pagarView.primaryDisabledButton()
                self.isChecked = false
            }
            typeOficinaTextField.resignFirstResponder()
        } else {
            self.isValidDocument = false
            cleanTextField()
            showMessageAlert(message: message)
        }
    }
    
    func blockTextField() {
        self.apellidoPaternoText.isEnabled = false
        self.apellidoMaternoText.isEnabled = false
        self.nombreText.isEnabled = false
        self.numDocText.resignFirstResponder()
    }
    
    func desblockTextField() {
        self.apellidoPaternoText.isEnabled = true
        self.apellidoMaternoText.isEnabled = true
        self.nombreText.isEnabled = true
        self.nameRazonSocialText.isEnabled = true
        self.numDocText.resignFirstResponder()
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            self.nombreText.text = jsonValidacionCe.strNombres
            self.apellidoPaternoText.text = jsonValidacionCe.strPrimerApellido
            self.apellidoMaternoText.text = jsonValidacionCe.strSegundoApellido
            
            
            //self.tipPerPN = jsonValidacionCe.type
            self.apePatPN = jsonValidacionCe.strPrimerApellido
            self.apeMatPN = jsonValidacionCe.strSegundoApellido
            self.nombPN = jsonValidacionCe.strNombres

            blockTextField()
            print(typeOficinaTextField.text)
            if typeOficinaTextField.text ?? "" != "" {
                self.pagarView.primaryButton()
                self.isChecked = true
            }
            else {
                self.pagarView.primaryDisabledButton()
                self.isChecked = false
            }
        } else {
            cleanTextField()
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func validarCampos(){
        self.isChecked = false
        self.pagarView.primaryDisabledButton()
        
        guard let oficina = typeOficinaTextField.text, !oficina.isEmpty else {
            return
        }
        
        guard let numDoc = numDocText.text, !numDoc.isEmpty else {
            return
        }
        
        if tipoPer == "N" {
            guard let nombre = nombreText.text, !nombre.isEmpty else {
                return
            }
            
            guard let apellidoPaterno = apellidoPaternoText.text, !apellidoPaterno.isEmpty else {
                return
            }
        }
        
        if(tipoPer == "J") {
            guard let nameRazonSocial = nameRazonSocialText.text, !nameRazonSocial.isEmpty else {
                return
            }
        }
        
        self.isChecked = true
        self.pagarView.primaryButton()
    }
    
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension PublicidadCertiDatosViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            //return self.tipoCertificado[row]
            
            return self.tipoDocumentosEntities[row].nombreAbrev
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            // self.typeOficinaTextField.text = oficinas[row].Nombre
            // self.typeOficinaTextField.resignFirstResponder()
            
        case 2:
            
            self.changeTypeDocument(index: row)
            // self.areaRegistralText.text = tipoDocumentosEntities[row].descripcion
            // self.areaRegistralText.resignFirstResponder()
            
        default:
            return
        }
        
    }

}

extension PublicidadCertiDatosViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == areaRegistralText) {
            valDone = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        validarCampos()
        if let sdcTextField = textField as? SDCTextField {
            if (sdcTextField == numDocText) {
                guard let textFieldText = textField.text,
                      let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
                }
                let substringToReplace = textFieldText[rangeOfTextToReplace]
                let count = textFieldText.count - substringToReplace.count + string.count
                let textoConCambio = (textFieldText as NSString).replacingCharacters(in: range, with: string)
                print(textoConCambio)
                print(count)
                print(maxLength)
                numDocument = textoConCambio
                if (count == maxLength) {
                    self.validateTypeDocument()
                }
            }
        
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
  
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (numDocText == textField) {
            guard let text = textField.text, !text.isEmpty else {
                // Si no hay texto, llenar con ceros y actualizar el campo de texto
                textField.text = ""
                return true
            }
            
            // Verificar si el texto es numérico
            if let intValue = Int(text), let formattedValue = String(format: "%08d", intValue) as NSString? {
                textField.text = formattedValue as String
            } else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
           
        }
      
        textField.resignFirstResponder()
        return true
    }
}
