//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadOnlyCertiPJViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var numDocText: UITextField!
    @IBOutlet weak var nameRazonSocialText: SDCTextField!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var maxLength = 0
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var codArea: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var areaRegId: String = ""
    
    var montoCalc: String = "0.0"
    
    var codLibro: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    var titleBar: String = ""
    
    var indexSelect = 0
    var valDone: Bool = false
    
    var razSocPN: String = ""
    var precOfic: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    private lazy var presenter: PublicidadOnlyCertiPJPresenter = {
       return PublicidadOnlyCertiPJPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        hideKeyboardWhenTappedAround()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        self.presenter.didiListadoOficina()
        toConfirmViewAnio.isHidden = true
        
        nameRazonSocialText.delegate = self
        nameRazonSocialText.valueType = .razonSocial
        nameRazonSocialText.maxLengths = 50
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)

    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)

    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()
        
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoDocumentoPickerView.tag = 1
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbar()
        
        
        self.nameRazonSocialText.borderAndPaddingLeftAndRight()
        
        
        
        self.isChecked = false
        
    }
    
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toPersonJuriView.isHidden = false
    }
    
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
    }
    
    @objc private func onTapToConfirmViewAnio() {

        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
      
        toPersonJuriView.isHidden = false
    }
  
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func donePressed() {
        if valDone == true {
            
            self.typeOficinaTextField.text = oficinas[indexSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            
            self.regPubId = self.oficinas[indexSelect].RegPubId
            self.oficRegId = self.oficinas[indexSelect].OficRegId
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            print("valoficinaOrigen:---:>>>",valoficinaOrigen)
            self.validarCampo(text: self.nameRazonSocialText.text ?? "")
            //self.pagarView.primaryButton()
          //  self.isChecked = true
        }
    }
    
    
    func setValues() {

    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.nameRazonSocialText.resignFirstResponder()
    }
    
    
    @objc private func onTapPagarView(){
        let storyboard = UIStoryboard(name: Constant.StoryBoardName.payments, bundle: nil)
        let nameIdentifier = Constant.Identifier.paymentsViewController
        if let vc = storyboard.instantiateViewController(withIdentifier: nameIdentifier) as? PaymentsViewController {
            let usuario = UserPreferencesController.usuario()
            
            vc.paymentItem.codCerti = certificadoId
            //vc.paymentItem.codArea = codArea
            vc.paymentItem.oficinaOrigen = valoficinaOrigen
            vc.paymentItem.tpoPersona = "J"
            vc.paymentItem.apePaterno = usuario.priApe
            vc.paymentItem.apeMaterno = usuario.segApe
            vc.paymentItem.nombre = usuario.nombres
            vc.paymentItem.razSoc = .empty
            vc.paymentItem.tpoDoc = usuario.tipoDoc
            vc.paymentItem.numDoc = usuario.nroDoc
            vc.paymentItem.email = usuario.email
            vc.paymentItem.tipPerPN = "J"
            vc.paymentItem.apePatPN = ""
            vc.paymentItem.apeMatPN = ""
            vc.paymentItem.nombPN = ""
            vc.paymentItem.razSocPN = self.nameRazonSocialText.text ?? ""
            vc.paymentItem.tipoDocPN = ""
            vc.paymentItem.numDocPN = ""
            vc.paymentItem.costoServicio = precOfic
            vc.paymentItem.costoTotal = precOfic
            vc.paymentItem.usrId = "\(usuario.idUser)"
           
            vc.paymentItem.codArea = areaRegId
            vc.areaRegId = areaRegId
            vc.certificadoId = certificadoId
            vc.precOfic = precOfic
            vc.titleBar = titleBar
            dump(vc.paymentItem)
            self.navigationController?.pushViewController(vc, animated: true)
     }
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity) {
        
        print("solicitudId::-->>",solicitud.solicitudId)
       // self.tokenNiubiz = token
        //self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz 
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        valDone = true
        self.indexSelect = index
    }
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
    func validarCampo(text:String){
        (!text.isEmpty && valDone) ? self.pagarView.primaryButton() :self.pagarView.primaryDisabledButton()
    }
}

extension PublicidadOnlyCertiPJViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            
        default:
            return
        }
        
    }
    

}

//MARK: - VISANET DELEGATE SECTION

extension PublicidadOnlyCertiPJViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoExitosoViewController") as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension PublicidadOnlyCertiPJViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let sdcTextField = textField as? SDCTextField {
            let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? .empty
       
            self.pagarView.isUserInteractionEnabled = currentText.count > 1
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }

        return false
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        self.validarCampo(text: textField.text ?? .empty)
    }
    
}
