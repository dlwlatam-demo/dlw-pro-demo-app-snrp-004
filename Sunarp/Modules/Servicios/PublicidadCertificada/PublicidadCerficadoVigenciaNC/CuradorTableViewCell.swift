//
//  CuradorTableViewCell.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 20/08/23.
//

import UIKit

protocol DeleteItemCuradorProtocol {
    func deleteItemCurador(item:String, index: Int)
}

class CuradorTableViewCell: UITableViewCell {
    // deleteView

    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static var identifier = String(describing: CuradorTableViewCell.self)
    var delegate: DeleteItemCuradorProtocol?
    var indexCell = 0
    var itemCell = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        let tapDeleteView = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        deleteView.isUserInteractionEnabled = true
        deleteView.addGestureRecognizer(tapDeleteView)
    }
    
    func setup(item: String, index: Int){
        itemCell = item
        nameLabel.text = item
        indexCell = index
    }

    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        delegate?.deleteItemCurador(item: itemCell, index: indexCell)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
