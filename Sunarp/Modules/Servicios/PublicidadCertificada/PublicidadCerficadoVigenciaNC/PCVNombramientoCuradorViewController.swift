//
//  PCVNombramientoCuradorViewController.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 20/08/23.
//

import Foundation
import UIKit
import VisaNetSDK

class PCVNombramientoCuradorViewController: UIViewController {
    
    @IBOutlet weak var OficinaRegistralView: UIView!
    @IBOutlet weak var oficinaRegistralText: UITextField!
    
    @IBOutlet weak var solicitarView: UIView!
    @IBOutlet weak var solicitarText: UITextField!

    @IBOutlet weak var numeroSolicitarText: SDCTextField!
    @IBOutlet weak var numeroAsientoText: SDCTextField!
    
    @IBOutlet weak var libroStack: UIStackView!
    @IBOutlet weak var libroView: UIView!
    @IBOutlet weak var libroText: UITextField!
    @IBOutlet weak var libroFolioText: SDCTextField!
    @IBOutlet weak var libroTomoText: SDCTextField!
    
    @IBOutlet weak var interdictoAPText: SDCTextField!
    @IBOutlet weak var interdictoAMText: SDCTextField!
    @IBOutlet weak var interdictoNombreText: SDCTextField!
    
    @IBOutlet weak var curadorAPText: SDCTextField!
    @IBOutlet weak var curadorAMText: SDCTextField!
    @IBOutlet weak var curadorNombreText: SDCTextField!
    @IBOutlet weak var curadorTable: UITableView!
    @IBOutlet weak var viewCuradorTable: UIView!
    @IBOutlet weak var solicitarButton: UIButton!
    
    
    var loading: UIAlertController!
    
    private lazy var presenter: PCVNombramientoCuradorPresenter = {
        return PCVNombramientoCuradorPresenter(controller: self)
    }()
    
    var arrayInterdicto = [String]()
    var arrayCurador = [String]()
    
    var lstCurador = [Interviniente]()
    
    var oficinas: [OficinaRegistralEntity] = []
    var itemOficina:OficinaRegistralEntity?
    var indexItemOficina = 0
    
    var solicitarArray: [String] = ["Número de Partida", "Ficha", "Tomo/Folio"]
    var indexSolicitar = 0
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    var indexItemLibros = 0
    
    var oficionaRegistralPickerView = UIPickerView()
    var solicitarPickerView = UIPickerView()
    var librosPickerView = UIPickerView()
    
    var areaRegId:String?
    var titleBar:String = ""
    var codGrupoLibroArea:String = ""
    
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var certificadoId: String = ""
    var precOfic: String = ""
    var valoficinaOrigen: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        configureView()
        setupNavigationBar()
        self.presenter.getListaOficinaRegistral()
        if let data = areaRegId {
            self.presenter.getListaObtenerLibro(areaRegId: data)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func configureView(){
        libroStack.isHidden =  true
        
        curadorTable.delegate = self
        curadorTable.dataSource = self
        
        libroFolioText.delegate = self
        libroTomoText.delegate = self
        numeroSolicitarText.delegate = self
        numeroAsientoText.delegate = self
       
        
        setupStyle(myView: viewCuradorTable)
        setupStyle(myView: libroView)
        setupStyle(myView: solicitarView)
        setupStyle(myView: OficinaRegistralView)
        
        oficionaRegistralPickerView.delegate = self
        oficionaRegistralPickerView.dataSource = self
        oficinaRegistralText.inputView = self.oficionaRegistralPickerView
        oficinaRegistralText.inputAccessoryView = self.createToolbarOficinaRegistral()
        
        solicitarPickerView.delegate = self
        solicitarPickerView.dataSource = self
        solicitarText.inputView = self.solicitarPickerView
        solicitarText.inputAccessoryView = self.createToolbarSolicitar()
        
        librosPickerView.delegate = self
        librosPickerView.dataSource = self
        libroText.inputView = self.librosPickerView
        libroText.inputAccessoryView = self.createToolbarLibros()
        configtexfield()
    }
    
    func configtexfield() {
        numeroSolicitarText.delegate = self
        numeroSolicitarText.maxLength = 10
        numeroSolicitarText.valueType = .alphaNumeric
        
        numeroAsientoText.delegate = self
        numeroAsientoText.maxLength = 5
        numeroAsientoText.valueType = .onlyNumbers
        
        libroFolioText.delegate = self
        libroFolioText.maxLength = 6
        libroFolioText.valueType = .alphaNumeric
        
        libroTomoText.delegate = self
        libroTomoText.maxLength = 6
        libroTomoText.valueType = .alphaNumeric
        
        interdictoAPText.delegate = self
        interdictoAPText.maxLength = 50
        interdictoAPText.valueType = .alphaNumericWithSpace
        
        interdictoAMText.delegate = self
        interdictoAMText.maxLength = 50
        interdictoAMText.valueType = .alphaNumericWithSpace
        
        interdictoNombreText.delegate = self
        interdictoNombreText.maxLength = 50
        interdictoNombreText.valueType = .alphaNumericWithSpace
        
        curadorAPText.delegate = self
        curadorAPText.maxLength = 50
        curadorAPText.valueType = .alphaNumericWithSpace
        
        curadorAMText.delegate = self
        curadorAMText.maxLength = 50
        curadorAMText.valueType = .alphaNumericWithSpace
        
        curadorNombreText.delegate = self
        curadorNombreText.maxLength = 50
        curadorNombreText.valueType = .alphaNumericWithSpace
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func createToolbarSolicitar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneSolicitar))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    func createToolbarLibros() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneLibros))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func doneLibros() {
        self.libroText.text = obtenerLibroEntities[indexItemLibros].nomLibro
        self.libroText.resignFirstResponder()
    }
    
    @objc func doneSolicitar() {
        libroStack.isHidden = (indexSolicitar == 2) ? false  : true
        self.solicitarText.text = solicitarArray[indexSolicitar]
        self.solicitarText.resignFirstResponder()
    }
    
    func createToolbarOficinaRegistral() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneOficinaRegistral))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    @objc func doneOficinaRegistral() {
        itemOficina = oficinas[indexItemOficina]
        self.oficinaRegistralText.text = oficinas[indexItemOficina].Nombre
        self.oficinaRegistralText.resignFirstResponder()
        
        
        let regPubId = self.oficinas[indexItemOficina].RegPubId
        let oficRegId = self.oficinas[indexItemOficina].OficRegId
        self.valoficinaOrigen = "\(regPubId)\(oficRegId)"
    }
    
    @objc func cancelOficinaRegistral() {
        self.view.endEditing(true)
    }
    
    func setupStyle(myView: UIView) {
        let grayColor = UIColor.gray.withAlphaComponent(0.5)
        myView.layer.borderColor = grayColor.cgColor
        myView.layer.borderWidth = 1.0
        myView.layer.cornerRadius = 4.0
    }

    @IBAction func agregarInterdicto(_ sender: Any) {
        guard let interdictoAP =  interdictoAPText.text, !interdictoAP.isEmpty,
              let interdictoNombre =  interdictoNombreText.text, !interdictoNombre.isEmpty
        else { return }
        
        let interdictoAM =  interdictoAMText.text ?? .empty
        let fullName = String(format: "%@ %@ %@, Natural", interdictoAP,interdictoAM, interdictoNombre)
       
        print(fullName)
        //validarBotonSolicitar()
    }
    
    @IBAction func agregarCurador(_ sender: Any) {
        guard let curadorAP =  curadorAPText.text, !curadorAP.isEmpty,
              let curadorNombre =  curadorNombreText.text, !curadorNombre.isEmpty
        else { return }
        
        let curadorAM =  curadorAMText.text ?? .empty
        let fullName = String(format: "%@ %@ %@, Natural", curadorAP,curadorAM, curadorNombre)
       
        arrayCurador.append(fullName)
        
        let interviniente: Interviniente = Interviniente()
        interviniente.tipoInter = 4
        interviniente.tipoPart = 1
        interviniente.apMaterno = curadorAM
        interviniente.apPaterno = curadorAP
        interviniente.nombres = curadorNombre
        interviniente.razSocial = ""
        
        lstCurador.append(interviniente)
        curadorTable.reloadData()
        
        curadorAPText.text = .empty
        curadorAMText.text = .empty
        curadorNombreText.text = .empty
        validarBotonSolicitar()
    }
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
        self.obtenerLibroEntities = arrayTipoDocumentos
    }
    
    func validarBotonSolicitar() {
        
        if indexSolicitar == 2 {
            guard !arrayCurador.isEmpty, //, !arrayInterdicto.isEmpty,
                  let folio = libroFolioText.text, !folio.isEmpty,
                  let nroTomo = libroTomoText.text, !nroTomo.isEmpty
            else {
                solicitarButton.isEnabled = false
                solicitarButton.tintColor = .white
                solicitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
            solicitarButton.isEnabled = true
            solicitarButton.tintColor = .white
            solicitarButton.backgroundColor = UIColor(named: "ButtonColor")
        } else {
            guard !arrayCurador.isEmpty, //, !arrayInterdicto.isEmpty,
                  let numPart = numeroSolicitarText.text, !numPart.isEmpty
            else {
                solicitarButton.isEnabled = false
                solicitarButton.tintColor = .white
                solicitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
            solicitarButton.isEnabled = true
            solicitarButton.tintColor = .white
            solicitarButton.backgroundColor = UIColor(named: "ButtonColor")
        }
    }
    
    @IBAction func solicitarAction(_ sender: Any) {
        if indexSolicitar == 2 {
            
          var numPart = "-"
            if  let nroFolio = libroFolioText.text, !nroFolio.isEmpty,
               let nroTomo = libroTomoText.text, !nroTomo.isEmpty {
                let formato = "%06d"
                var nroFolio1 = nroFolio
                var nroTomo1 = nroTomo
                if nroFolio.count < 6 {
                    if let intValue = Int(nroFolio), let formattedValue = String(format: formato, intValue) as NSString? {
                        libroFolioText.text = formattedValue as String
                        nroFolio1 = formattedValue as String
                    }
                }
                if nroTomo.count < 6 {
                    if let intValue = Int(nroTomo), let formattedValue = String(format: formato, intValue) as NSString? {
                        libroTomoText.text = formattedValue as String
                        nroTomo1 = formattedValue as String
                    }
                }
                
               numPart = "\(nroTomo1)-\(nroFolio1)"
           }
          
            let itemOfice = oficinas[indexItemOficina]
            let areaRegId = obtenerLibroEntities[indexItemLibros].areaRegId
            
            presenter.getObtenerNumPartida(tipo: String(indexSolicitar), numero: numPart, regPubId: itemOfice.RegPubId, oficRegId: itemOfice.OficRegId, areaRegId: areaRegId)
        } else {
            let numPart = numeroSolicitarText.text ?? .empty
            let itemOfice = oficinas[indexItemOficina]
            let areaRegId = obtenerLibroEntities[indexItemLibros].areaRegId
            let grupoLibroArea = obtenerLibroEntities[indexItemLibros].grupoLibroArea
            let tipoPartidaFicha = (indexSolicitar + 1)
            presenter.validaPartidaCurador(regPubId: itemOfice.RegPubId, oficRegId: itemOfice.OficRegId, areaRegId: areaRegId, libroArea: grupoLibroArea, numPart: numPart, tipoPartidaFicha: String(tipoPartidaFicha))
        }
    }
    
    
    func loadObtenerNumPartida(solicitud: PartidaTFEntity) {
        if solicitud.partida == "" {
            showAlert(title: "Información", message: "Verifique sus datos" , btnMessage: "Aceptar")
        } else {
            showPayment(refNumPart: 0, partida: solicitud.partida)
        }
    }
    
    func validaPartidaCurador(item: PartidaModel){
    
        if item.estado == 1 {
            showAlert(title: "Información", message: item.msj ?? .empty, btnMessage: "Aceptar")
        } else {
            let refNumPart = item.refNumPart ?? 0
            let numPart = numeroSolicitarText.text ?? .empty
            print("partida = ", numPart)
            showPayment(refNumPart: refNumPart, partida: numPart)
        }
    }
    
    func showPayment(refNumPart: Int, partida: String){
        
        let storyboard = UIStoryboard(name: "Payments", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
            let usuario = UserPreferencesController.usuario()
 
            vc.paymentItem.tpoPersona = "N"
            vc.paymentItem.apePaterno = usuario.priApe
            vc.paymentItem.apeMaterno = usuario.segApe
            vc.paymentItem.nombre = usuario.nombres
            vc.paymentItem.razSoc = ""
            vc.paymentItem.tpoDoc = usuario.tipoDoc
            vc.paymentItem.numDoc = usuario.nroDoc
            vc.paymentItem.email = usuario.email
            vc.paymentItem.ipRemote = ""
            vc.paymentItem.sessionId = ""
            vc.paymentItem.usrId = Constant.API_USRID
            vc.titleBar = titleBar
            
            if(refNumPart == 0) {
                vc.paymentItem.refNumPart = ""
            }
            else {
                vc.paymentItem.refNumPart = String(refNumPart )
            }
            print("indexSolicitar = ", indexSolicitar)
            if indexSolicitar == 0 {
                vc.paymentItem.partida = partida
            }
            else if indexSolicitar == 1 {
                vc.paymentItem.ficha = partida
            }
            else {
                vc.paymentItem.partida = partida
                vc.paymentItem.tomo = libroTomoText.text ?? .empty
                vc.paymentItem.folio = libroFolioText.text ?? .empty
            }
            vc.paymentItem.asiento = numeroAsientoText.text ?? .empty
            vc.paymentItem.costoServicio = precOfic
            vc.paymentItem.costoTotal = precOfic
            //Others data information
            vc.certificadoId = certificadoId
            vc.paymentItem.codCerti = certificadoId
            vc.precOfic = precOfic
            vc.paymentItem.oficinaOrigen = valoficinaOrigen

            let interdictoAP =  interdictoAPText.text ?? .empty
            let interdictoNombre =  interdictoNombreText.text ?? .empty
            let interdictoAM =  interdictoAMText.text ?? .empty

            let interviniente: Interviniente = Interviniente()
            interviniente.tipoInter = 3
            interviniente.tipoPart = 1
            interviniente.apMaterno = interdictoAM
            interviniente.apPaterno = interdictoAP
            interviniente.nombres = interdictoNombre
            interviniente.razSocial = ""
            
            lstCurador.insert(interviniente, at: 0)
            var strLstCurador: String = ""
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(lstCurador)
                strLstCurador = String(data: data, encoding: .utf8)!
            } catch {
                // Handle error
                print(error.localizedDescription)
            }
            vc.paymentItem.lstCuradores = strLstCurador

            dump(vc.paymentItem)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //"\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        print("presentVisaPaymentForm")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    
    func showAlert(title: String, message: String, btnMessage: String) {
       let alert = UIAlertController(title: title,
                                     message: message,
                                     preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: btnMessage,
                                     style: UIAlertAction.Style.default) { _ in
           // self.navigationController?.popViewController(animated: true)
            self.view.endEditing(true)
       })
       self.present(alert, animated: true, completion: nil)
   }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
}

extension PCVNombramientoCuradorViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCurador.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellRow = curadorTable.dequeueReusableCell(withIdentifier: CuradorTableViewCell.identifier, for: indexPath)
        guard let cell = cellRow as?  CuradorTableViewCell else { return UITableViewCell()}
        let item = arrayCurador[indexPath.row]
        cell.setup(item: item, index: indexPath.row)
        cell.delegate = self
        return cell
    }
    
}


extension PCVNombramientoCuradorViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if oficionaRegistralPickerView == pickerView {
           return oficinas.count
        } else if (solicitarPickerView == pickerView) {
            return solicitarArray.count
        } else {
            return obtenerLibroEntities.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if oficionaRegistralPickerView == pickerView {
            return oficinas[row].Nombre
        } else if solicitarPickerView == pickerView {
            return solicitarArray[row]
        } else {
            return obtenerLibroEntities[row].nomLibro
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.numeroSolicitarText.text = ""
        self.libroTomoText.text = ""
        self.libroFolioText.text = ""
        if oficionaRegistralPickerView == pickerView {
            indexItemOficina = row
            itemOficina = oficinas[row]
        } else if (solicitarPickerView == pickerView) {
            indexSolicitar = row
        } else {
            indexItemLibros = row
        }
    }
}

//MARK: - VISANET DELEGATE SECTION

extension PCVNombramientoCuradorViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                 vc.monto = self.nsolMonLiq
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
extension PCVNombramientoCuradorViewController: DeleteItemCuradorProtocol {
    func deleteItemCurador(item: String, index: Int) {
        arrayCurador.remove(at: index)
        curadorTable.reloadData()
        validarBotonSolicitar()
    }
    
    
    
}


extension PCVNombramientoCuradorViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        self.validarBotonSolicitar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text) {
            var formato = "%08d"
            if self.indexSolicitar == 2 {
                formato = "%06d"
            }
            if let formattedValue = String(format: formato, intValue) as NSString? {
                textField.text = formattedValue as String
            }
            else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
}

