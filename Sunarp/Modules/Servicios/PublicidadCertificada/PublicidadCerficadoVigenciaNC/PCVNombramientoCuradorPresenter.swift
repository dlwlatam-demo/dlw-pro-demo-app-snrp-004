//
//  PCVNombramientoCuradorPresenter.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 20/08/23.
//

import Foundation

class PCVNombramientoCuradorPresenter {
    
    
    private weak var controller: PCVNombramientoCuradorViewController?
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    init(controller: PCVNombramientoCuradorViewController) {
        self.controller = controller
    }
}

extension PCVNombramientoCuradorPresenter {
    
    func getListaOficinaRegistral () {
        self.consultaLiteralModel.getListaOficinaRegistral{ (arrayOficinas) in
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    func getListaObtenerLibro(areaRegId:String) {
        self.modelDoc.getListaObtenerLibro(areaRegId: areaRegId) { (arrayTipoDocumentos) in
            self.controller?.loadObtenerLibro(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func getValidaPartidaxRefNumPart(regPubId: String, oficRegId: String, areaRegId: String, numPart: String) {
        self.consultaPublicaModel.getValidaPartidaxRefNumPart(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, numPart: numPart) { (arrayResponse) in
          
          // self.controller?.loadPartidaRefNumPart(solicitud:arrayResponse)
        }
    }
    
   
    func validaPartidaCurador(regPubId: String, oficRegId: String, areaRegId: String,libroArea: String, numPart: String, tipoPartidaFicha: String) {
        self.consultaPublicaModel.validaPartidaCurador(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, tipoPartidaFicha: tipoPartidaFicha) { (arrayResponse) in
            self.controller?.validaPartidaCurador(item: arrayResponse)
        }
    }
    
    func getObtenerNumPartida(tipo: String,numero: String,regPubId: String, oficRegId: String, areaRegId: String) {
        self.consultaPublicaModel.getObtenerNumPartida(tipo: tipo,numero: numero,regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId) { (arrayResponse) in
          self.controller?.loadObtenerNumPartida(solicitud:arrayResponse)
        }
    }
    
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        print("getVisaKeys")
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        print("getTokenNiubiz")
        print(urlVisa)
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        print("getNiubizPinHash")
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId() {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }
}
