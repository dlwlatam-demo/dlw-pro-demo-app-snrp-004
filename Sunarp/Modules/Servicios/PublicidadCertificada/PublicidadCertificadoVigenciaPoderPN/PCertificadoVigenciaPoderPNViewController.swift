//
//  PCertificadoVigenciaPoderPNViewController.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 11/08/23.
//


import Foundation
import UIKit
import VisaNetSDK

class PCertificadoVigenciaPoderPNViewController: UIViewController {

    @IBOutlet weak var viewOficina: UIView!
    @IBOutlet weak var oficionaRegistralText: UITextField!
   
    @IBOutlet weak var viewSolitar: UIView!
    @IBOutlet weak var oficinaRegistralDetailText: UITextField!
    @IBOutlet weak var numeroText: SDCTextField!
    @IBOutlet weak var asientoText: SDCTextField!
    
    @IBOutlet weak var viewLibro: UIView!
    @IBOutlet weak var libroStack: UIStackView!
    @IBOutlet weak var libroSelectText: UITextField!
    @IBOutlet weak var folioText: SDCTextField!
    @IBOutlet weak var tomoText: SDCTextField!
    
    @IBOutlet weak var contenedorOneView: UIView!
    @IBOutlet weak var podernanteAPText: UITextField!
    @IBOutlet weak var podernanteAMText: UITextField!
    @IBOutlet weak var podernanteNombreText: UITextField!
    @IBOutlet weak var tablePodernante: UITableView!
    @IBOutlet weak var agregarPoderanteButton: UIButton!
    
    @IBOutlet weak var radiusNatural: UIView!
    @IBOutlet weak var radiusJuridica: UIView!

    @IBOutlet weak var apoderadoTwoView: UIView!
    @IBOutlet weak var apoderadoAPText: UITextField!
    @IBOutlet weak var apoderadoAMText: UITextField!
    @IBOutlet weak var apoderadoNombreText: UITextField!
    @IBOutlet weak var agregarApoderadoButton: UIButton!
    @IBOutlet weak var tableApoderado: UITableView!
    
    @IBOutlet weak var imgRadiusNatural: UIImageView!
    @IBOutlet weak var imgRadiusJuridica: UIImageView!
    
    @IBOutlet weak var razonSocialText: UITextField!
    @IBOutlet weak var stackPNatural: UIStackView!
    
    @IBOutlet weak var solitarButton: UIButton!
    
    var loading: UIAlertController!
    
    private lazy var presenter: PCertificadoVigenciaPoderPNPresenter = {
        return PCertificadoVigenciaPoderPNPresenter(controller: self)
    }()
    
    var arrayPodernante = [String]()
    var arrayApoderado = [String]()
    
    var lstPoderdante = [Interviniente]()
    var lstApoderado = [Interviniente]()
    var oficinas: [OficinaRegistralEntity] = []
    var itemOficina:OficinaRegistralEntity?
    var indexItemOficina = 0
    
    var solicitarArray: [String] = ["Número de Partida", "Ficha", "Tomo/Folio"]
    var indexSolicitar = 0
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    var indexItemLibros = 0
    
    var oficionaRegistralPickerView = UIPickerView()
    var solicitarPickerView = UIPickerView()
    var librosPickerView = UIPickerView()
    var isPersonaNatural = 1
    
    var areaRegId:String?
    var titleBar:String = ""
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var certificadoId: String = ""
    var precOfic: String = ""
    var valoficinaOrigen: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("1")
        hideKeyboardWhenTappedAround()
        print("2")
        configureView()
        print("3")
        setupNavigationBar()
        print ("4")
        self.presenter.getListaOficinaRegistral()
        if let data = areaRegId {
            self.presenter.getListaObtenerLibro(areaRegId: data)
        }
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func configureView() {
        razonSocialText.isHidden = true
        libroStack.isHidden =  true
        
        tableApoderado.delegate = self
        tableApoderado.dataSource = self
        tablePodernante.delegate = self
        tablePodernante.dataSource = self
        
        folioText.delegate = self
        tomoText.delegate = self
        numeroText.delegate = self
        asientoText.delegate = self
        setupStyle(myView: apoderadoTwoView)
        setupStyle(myView: contenedorOneView)
        setupStyle(myView: viewLibro)
        setupStyle(myView: viewSolitar)
        setupStyle(myView: viewOficina)
        
        
        let tapRadiusNatural = UITapGestureRecognizer(target: self, action: #selector(radiusNaturalTapped(tapGestureRecognizer:)))
        radiusNatural.isUserInteractionEnabled = true
        radiusNatural.addGestureRecognizer(tapRadiusNatural)
        
        let tapRadiusJuridica = UITapGestureRecognizer(target: self, action: #selector(radiusJuridicaTapped(tapGestureRecognizer:)))
        radiusJuridica.isUserInteractionEnabled = true
        radiusJuridica.addGestureRecognizer(tapRadiusJuridica)
        
        oficionaRegistralPickerView.delegate = self
        oficionaRegistralPickerView.dataSource = self
        oficionaRegistralText.inputView = self.oficionaRegistralPickerView
        oficionaRegistralText.inputAccessoryView = self.createToolbarOficinaRegistral()
        
        solicitarPickerView.delegate = self
        solicitarPickerView.dataSource = self
        oficinaRegistralDetailText.inputView = self.solicitarPickerView
        oficinaRegistralDetailText.inputAccessoryView = self.createToolbarSolicitar()
        
        librosPickerView.delegate = self
        librosPickerView.dataSource = self
        libroSelectText.inputView = self.librosPickerView
        libroSelectText.inputAccessoryView = self.createToolbarLibros()
        
        
        podernanteAPText.delegate = self
        podernanteAMText.delegate = self
        podernanteNombreText.delegate = self
        
        apoderadoAPText.delegate = self
        apoderadoAMText.delegate = self
        apoderadoNombreText.delegate = self
        razonSocialText.delegate = self
        
        configTextField()
    }
    
    
    func configTextField(){
        
        numeroText.maxLength = 10
        numeroText.valueType = .alphaNumeric
        numeroText.resignFirstResponder()
        
        folioText.maxLength = 6
        folioText.valueType = .onlyNumbers
        folioText.resignFirstResponder()
        tomoText.maxLength = 6
        tomoText.valueType = .onlyNumbers
        tomoText.resignFirstResponder()
       
        asientoText.maxLength = 5
        asientoText.valueType = .onlyNumbers
        asientoText.resignFirstResponder()
        
        
        podernanteAPText.maxLength = 50
        podernanteAPText.resignFirstResponder()
        podernanteAMText.maxLength = 50
        podernanteAMText.resignFirstResponder()
        podernanteNombreText.maxLength = 50
        podernanteNombreText.resignFirstResponder()
        
        
        apoderadoAPText.maxLength = 50
        apoderadoAPText.resignFirstResponder()
        apoderadoAMText.maxLength = 50
        apoderadoAMText.resignFirstResponder()
        apoderadoNombreText.maxLength = 50
        apoderadoNombreText.resignFirstResponder()
        razonSocialText.maxLength = 50
        razonSocialText.resignFirstResponder()
        
    }
    
    @objc func radiusNaturalTapped(tapGestureRecognizer: UITapGestureRecognizer){
        isPersonaNatural = 1
        imgRadiusNatural.image = UIImage(named: "radioLiteral_4")
        imgRadiusJuridica.image = UIImage(named: "radioLiteral_2")
        stackPNatural.isHidden = false
        razonSocialText.text = .empty
        razonSocialText.isHidden = true
        self.view.endEditing(true)
    }
    
    @objc func radiusJuridicaTapped(tapGestureRecognizer: UITapGestureRecognizer){
        isPersonaNatural = 2
        imgRadiusJuridica.image = UIImage(named: "radioLiteral_4")
        imgRadiusNatural.image = UIImage(named: "radioLiteral_2")
        apoderadoAMText.text = .empty
        apoderadoAPText.text = .empty
        apoderadoNombreText.text = .empty
        stackPNatural.isHidden = true
        razonSocialText.isHidden = false
        self.view.endEditing(true)
    }
    
    
    func createToolbarLibros() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneLibros))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @objc func doneLibros() {
        self.libroSelectText.text = obtenerLibroEntities[indexItemLibros].nomLibro
        self.libroSelectText.resignFirstResponder()
    }
    
    func createToolbarSolicitar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneSolicitar))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func doneSolicitar() {
        libroStack.isHidden = (indexSolicitar == 2) ? false  : true
        self.folioText.valueType = .onlyNumbers
        self.oficinaRegistralDetailText.text = solicitarArray[indexSolicitar]
        self.oficinaRegistralDetailText.resignFirstResponder()
    }
    
  
    func createToolbarOficinaRegistral() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneOficinaRegistral))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    @objc func doneOficinaRegistral() {
        itemOficina = oficinas[indexItemOficina]
        self.oficionaRegistralText.text = oficinas[indexItemOficina].Nombre
        self.oficionaRegistralText.resignFirstResponder()
        
        
        let regPubId = self.oficinas[indexItemOficina].RegPubId
        let oficRegId = self.oficinas[indexItemOficina].OficRegId
        self.valoficinaOrigen = "\(regPubId)\(oficRegId)"
    }
    
    @objc func cancelOficinaRegistral() {
        self.view.endEditing(true)
    }
    
    func setupStyle(myView: UIView) {
        let grayColor = UIColor.gray.withAlphaComponent(0.5)
        myView.layer.borderColor = grayColor.cgColor
        myView.layer.borderWidth = 1.0
        myView.layer.cornerRadius = 4.0
    }
    
    @IBAction func actionAddPoderante(_ sender: Any) {
        guard let podernanteAP =  podernanteAPText.text, !podernanteAP.isEmpty,
              let podernanteNombre =  podernanteNombreText.text, !podernanteNombre.isEmpty
        else { return }
        
        let podernanteAM =  podernanteAMText.text ?? .empty
        let fullName = String(format: "%@ %@ %@, Natural", podernanteAP,podernanteAM, podernanteNombre)
       
        arrayPodernante.append(fullName)
        let interviniente: Interviniente = Interviniente()
        interviniente.tipoInter = 5
        interviniente.tipoPart = 1
        interviniente.apMaterno = podernanteAM
        interviniente.apPaterno = podernanteAP
        interviniente.nombres = podernanteNombre
        interviniente.razSocial = ""
        
        lstPoderdante.append(interviniente)
        tablePodernante.reloadData()
        
        podernanteAPText.text = .empty
        podernanteAMText.text = .empty
        podernanteNombreText.text = .empty
    }
    
    @IBAction func actionAddApoderado(_ sender: Any) {
        if isPersonaNatural == 1 {
            guard let apoderadoAP =  apoderadoAPText.text, !apoderadoAP.isEmpty,
                  let apoderadoNombre =  apoderadoNombreText.text, !apoderadoNombre.isEmpty
            else { return }
            let apoderadoAM =  apoderadoAMText.text ?? .empty
            let fullName = String(format: "%@ %@ %@, Natural", apoderadoAP,apoderadoAM, apoderadoNombre)
        
            arrayApoderado.append(fullName)
            let interviniente: Interviniente = Interviniente()
            interviniente.tipoInter = 6
            interviniente.tipoPart = 1
            interviniente.apMaterno = apoderadoAM
            interviniente.apPaterno = apoderadoAP
            interviniente.nombres = apoderadoNombre
            interviniente.razSocial = ""
            
            lstApoderado.append(interviniente)
            tableApoderado.reloadData()
            apoderadoAPText.text = .empty
            apoderadoAMText.text = .empty
            apoderadoNombreText.text = .empty
            validarBotonSolicitar()
            
        } else {
            guard let razonSocial =  razonSocialText.text, !razonSocial.isEmpty
            else { return }
            
            let fullName = String(format: "%@, Jurídica", razonSocial)
        
            arrayApoderado.append(fullName)
            let interviniente: Interviniente = Interviniente()
            interviniente.tipoInter = 6
            interviniente.tipoPart = 2
            interviniente.razSocial = razonSocial
            
            lstApoderado.append(interviniente)
            tableApoderado.reloadData()
            razonSocialText.text = .empty
            validarBotonSolicitar()
        }
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
        dump(arrayTipoDocumentos)
        self.obtenerLibroEntities = arrayTipoDocumentos
    }
    
    @IBAction func actionSolicitar(_ sender: UIButton) {
        if indexSolicitar == 0 {
            validaPartidaxRefNumPart()
        } else {
            obtenerNumPartida()
        }
    }
    
    func validarBotonSolicitar() {
        
        if indexSolicitar == 2 {
            guard !arrayApoderado.isEmpty, !arrayPodernante.isEmpty,
                  let folio = folioText.text, !folio.isEmpty,
                  let nroTomo = tomoText.text, !nroTomo.isEmpty
            else {
                solitarButton.isEnabled = false
                solitarButton.tintColor = .white
                solitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
            solitarButton.isEnabled = true
            solitarButton.tintColor = .white
            solitarButton.backgroundColor = UIColor(named: "ButtonColor")
        } else {
            guard !arrayApoderado.isEmpty, !arrayPodernante.isEmpty,
                  let numPart = numeroText.text, !numPart.isEmpty
            else {
                solitarButton.isEnabled = false
                solitarButton.tintColor = .white
                solitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
            solitarButton.isEnabled = true
            solitarButton.tintColor = .white
            solitarButton.backgroundColor = UIColor(named: "ButtonColor")
        }
    }
    
    func validaPartidaxRefNumPart() {
        let numPart = numeroText.text ?? .empty
        let areaRegId = obtenerLibroEntities[indexItemLibros].areaRegId
        let itemOfice = oficinas[indexItemOficina]
        presenter.getValidaPartidaxRefNumPart(regPubId: itemOfice.RegPubId,
                                              oficRegId: itemOfice.OficRegId,
                                              areaRegId: areaRegId,
                                              numPart: numPart)
    }

    func obtenerNumPartida() {
        if indexSolicitar == 1 {
            let numPart = numeroText.text ?? .empty
            let itemOfice = oficinas[indexItemOficina]
            let areaRegId = obtenerLibroEntities[indexItemLibros].areaRegId
            presenter.getObtenerNumPartida(tipo: String(indexSolicitar), numero: numPart, regPubId: itemOfice.RegPubId, oficRegId: itemOfice.OficRegId, areaRegId: areaRegId)
        } else {
            let nroFolio = folioText.text ?? .empty
            let nroTomo = tomoText.text ?? .empty
            let formato = "%06d"
            var nroFolio1 = nroFolio
            var nroTomo1 = nroTomo
            if nroFolio.count < 6 {
                if let intValue = Int(nroFolio), let formattedValue = String(format: formato, intValue) as NSString? {
                    folioText.text = formattedValue as String
                    nroFolio1 = formattedValue as String
                }
            }
            if nroTomo.count < 6 {
                if let intValue = Int(nroTomo), let formattedValue = String(format: formato, intValue) as NSString? {
                    tomoText.text = formattedValue as String
                    nroTomo1 = formattedValue as String
                }
            }
            
            let numPart = "\(nroTomo1)-\(nroFolio1)"
            
            // let numPart = String(format: "%@-%@", nroTomo,nroFolio)
            let itemOfice = oficinas[indexItemOficina]
            let areaRegId = obtenerLibroEntities[indexItemLibros].areaRegId
            
            presenter.getObtenerNumPartida(tipo: String(indexSolicitar), numero: numPart, regPubId: itemOfice.RegPubId, oficRegId: itemOfice.OficRegId, areaRegId: areaRegId)
        }
    }
    
    func loadPartidaRefNumPart(solicitud: ValidaPartidaCRIEntity) {
        if solicitud.estado != 0 {
            showAlert(title: "Información", message: solicitud.msj , btnMessage: "Aceptar")
        } else {
            let refNumPart = solicitud.refNumPart
            let numPart = numeroText.text ?? .empty
            showPayment(refNumPart: refNumPart, partida: numPart)
        }
    }
    
    func loadObtenerNumPartida(solicitud: PartidaTFEntity) {

        if solicitud.partida == "" {
            showAlert(title: "Información", message: "Verifique sus datos" , btnMessage: "Aceptar")
        } else {
            let refNumPart = 0 // solicitud.refNumPart
            showPayment(refNumPart: refNumPart, partida: solicitud.partida)
        }
    }
    
     func showAlert(title: String, message: String, btnMessage: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: btnMessage,
                                      style: UIAlertAction.Style.default) { _ in
            // self.navigationController?.popViewController(animated: true)
            self.view.endEditing(true)
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPayment(refNumPart: Int, partida: String){
        
        let storyboard = UIStoryboard(name: "Payments", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.paymentsViewController) as? PaymentsViewController {
            let usuario = UserPreferencesController.usuario()
            
            vc.paymentItem.tpoPersona = "N"
            vc.paymentItem.apePaterno = usuario.priApe
            vc.paymentItem.apeMaterno = usuario.segApe
            vc.paymentItem.nombre = usuario.nombres
            vc.paymentItem.razSoc = ""
            vc.paymentItem.tpoDoc = usuario.tipoDoc
            vc.paymentItem.numDoc = usuario.nroDoc
            vc.paymentItem.email = usuario.email
            vc.paymentItem.ipRemote = ""
            vc.paymentItem.sessionId = ""
            vc.paymentItem.usrId = Constant.API_USRID
            vc.titleBar = titleBar
            
            if(refNumPart == 0) {
                vc.paymentItem.refNumPart = ""
            }
            else {
                vc.paymentItem.refNumPart = String(refNumPart )
            }
            
            if indexSolicitar == 0 {
                vc.paymentItem.partida = partida
            }
            else if indexSolicitar == 1 {
                vc.paymentItem.ficha = partida
            }
            else {
                vc.paymentItem.tomo = tomoText.text ?? .empty
                vc.paymentItem.folio = folioText.text ?? .empty
            }
            vc.paymentItem.asiento = asientoText.text ?? .empty
            vc.paymentItem.costoServicio = precOfic
            vc.paymentItem.costoTotal = precOfic
            //Others data information
            vc.certificadoId = certificadoId
            vc.paymentItem.codCerti = certificadoId
            vc.precOfic = precOfic
            vc.paymentItem.oficinaOrigen = valoficinaOrigen
            var strLstPoderdante: String = ""
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(lstPoderdante)
                strLstPoderdante = String(data: data, encoding: .utf8)!
            } catch {
                // Handle error
                print(error.localizedDescription)
            }
            vc.paymentItem.lstPoderdantes = strLstPoderdante
            var strLstApoderado: String = ""
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(lstApoderado)
                strLstApoderado = String(data: data, encoding: .utf8)!
            } catch {
                // Handle error
                print(error.localizedDescription)
            }
            vc.paymentItem.lstApoderados = strLstApoderado
        
            
            dump(vc.paymentItem)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //"\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        print("presentVisaPaymentForm")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PCertificadoVigenciaPoderPNViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tablePodernante {
            return arrayPodernante.count
        } else {
            return arrayApoderado.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tablePodernante {
            let cellRow = tablePodernante.dequeueReusableCell(withIdentifier: "PodernanteTableViewCell", for: indexPath)
            guard let cell = cellRow as?  PodernanteTableViewCell else { return UITableViewCell()}
            let item = arrayPodernante[indexPath.row]
            cell.setup(item: item, index: indexPath.row)
            cell.delegate = self
            return cell
        } else {
            let cellRow = tableApoderado.dequeueReusableCell(withIdentifier: "ApoderadoTableViewCell", for: indexPath)
            guard let cell = cellRow as?  ApoderadoTableViewCell else { return UITableViewCell()}
            let item = arrayApoderado[indexPath.row]
            cell.setup(item: item, index: indexPath.row)
            cell.delegate = self
            return cell
        }
    }
}

extension PCertificadoVigenciaPoderPNViewController: deleteApoderadoProtocol, DeletePodernanteProtocol {
    
    func deleteItemApoderado(item: String, index: Int) {
        arrayApoderado.remove(at: index)
        tableApoderado.reloadData()
        validarBotonSolicitar()
    }
    
    func deleteItem(item: String,index: Int) {
        arrayPodernante.remove(at: index)
        tablePodernante.reloadData()
        validarBotonSolicitar()
    }
    
}

extension PCertificadoVigenciaPoderPNViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if oficionaRegistralPickerView == pickerView {
           return oficinas.count
        } else if (solicitarPickerView == pickerView) {
            return solicitarArray.count
        } else {
            return obtenerLibroEntities.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if oficionaRegistralPickerView == pickerView {
            return oficinas[row].Nombre
        } else if solicitarPickerView == pickerView {
            return solicitarArray[row]
        } else {
            return obtenerLibroEntities[row].nomLibro
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if oficionaRegistralPickerView == pickerView {
            indexItemOficina = row
            itemOficina = oficinas[row]
           /* oficionaRegistralText.text = oficinas[row].Nombre
            oficionaRegistralText.resignFirstResponder()*/
        } else if (solicitarPickerView == pickerView) {
            indexSolicitar = row
          /*  oficinaRegistralDetailText.text = solicitarArray[row]
            oficinaRegistralDetailText.resignFirstResponder()*/
           
        } else {
            indexItemLibros = row
            /*libroSelectText.text = obtenerLibroEntities[row].nomLibro
            libroSelectText.resignFirstResponder()*/
        }
    }
}


//MARK: - VISANET DELEGATE SECTION

extension PCertificadoVigenciaPoderPNViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}



extension PCertificadoVigenciaPoderPNViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        self.validarBotonSolicitar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text) {
            var formato = "%08d"
            if self.indexSolicitar == 2 {
                formato = "%06d"
            }
            if let formattedValue = String(format: formato, intValue) as NSString? {
                textField.text = formattedValue as String
            }
            else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
}
