//
//  PodernanteTableViewCell.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 13/08/23.
//

import UIKit

protocol DeletePodernanteProtocol {
    func deleteItem(item:String, index: Int)
}

class PodernanteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var delegate: DeletePodernanteProtocol?
    var indexCell = 0
    var itemCell = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapDeleteView = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        deleteView.isUserInteractionEnabled = true
        deleteView.addGestureRecognizer(tapDeleteView)
    }
    
    
    func setup(item: String, index: Int){
        itemCell = item
        nameLabel.text = item
        indexCell = index
        print(indexCell)
    }
    
    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        print(itemCell)
        print(indexCell)
        if let delegate = delegate {
            delegate.deleteItem(item: itemCell, index: indexCell)
        } else {
            print("self")
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
