//
//  ApoderadoTableViewCell.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 13/08/23.
//

import UIKit

protocol deleteApoderadoProtocol {
    func deleteItemApoderado(item:String, index: Int)
}

class ApoderadoTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var delegate:deleteApoderadoProtocol?
    var indexCell = 0
    var itemCell = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
        let tapDeleteView = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        deleteView.isUserInteractionEnabled = true
        deleteView.addGestureRecognizer(tapDeleteView)
        
    }
    
    func setup(item: String, index: Int){
        itemCell = item
        nameLabel.text = item
        indexCell = index
    }

    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        delegate?.deleteItemApoderado(item: itemCell, index: indexCell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
