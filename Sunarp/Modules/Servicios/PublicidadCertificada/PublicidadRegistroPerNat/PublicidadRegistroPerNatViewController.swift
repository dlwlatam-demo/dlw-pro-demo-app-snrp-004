//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadRegistroPerNatViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var nombreText: SDCTextField!
    @IBOutlet weak var apellidoPaternoText: SDCTextField!
    @IBOutlet weak var apellidoMaternoText: SDCTextField!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var numDocText: SDCTextField!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"

    var maxLength = 8
    var minLength = 0
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    var titleBar: String = ""
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    var countCantPag: String = ""
    var countCantPagExo: String = .empty
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []

    var regPubId: String = ""
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var areaRegId: String = ""
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var precOfic: String = ""
    
    
    var indexSelect = 0
    var indexOficinaSelect = -1
    var valDone: Bool = true
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    
    private lazy var presenter: PublicidadRegistroPerNatPresenter = {
       return PublicidadRegistroPerNatPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        hideKeyboardWhenTappedAround()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        presenter.didLoad()
        
        self.presenter.didiListadoOficina()
        configTextField()
    }
    
    func configTextField() {
        numDocText.delegate = self
        numDocText.maxLengths = 8
        nombreText.delegate = self
        nombreText.maxLengths = 50
        nombreText.valueType = .alphaNumericWithSpace
        apellidoPaternoText.delegate = self
        apellidoPaternoText.maxLengths = 50
        apellidoPaternoText.valueType = .alphaNumericWithSpace
        apellidoMaternoText.delegate = self
        apellidoMaternoText.maxLengths = 50
        apellidoMaternoText.valueType = .alphaNumericWithSpace
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
      //  self.scrollview.contentInset = contentInsets
     //   self.scrollview.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
      //  self.scrollview.contentInset = contentInsets
       // self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.nombreText.borderAndPaddingLeftAndRight()
        self.apellidoPaternoText.borderAndPaddingLeftAndRight()
        self.apellidoMaternoText.borderAndPaddingLeftAndRight()
       // self.pagarView.primaryButton()
        self.pagarView.primaryDisabledButton()

        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        self.tipoDocumentoPickerView.tag = 1
        self.areaRegistralPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeOficinaTextField.inputView = self.tipoDocumentoPickerView
        self.typeOficinaTextField.inputAccessoryView = self.createToolbar()
        
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
        
        
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.participantePickerView.tag = 2
        
                
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
    
        self.numDocText.delegate = self
    }
    
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.nombreAbrev)
        }
        
        for (indice, valor) in tipoDocumentosEntities.enumerated() {
            if valor.nombreAbrev == "DNI" {
                self.areaRegistralText.text = tipoDocumentosEntities[indice].nombreAbrev
            }
        }
    }
    
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
    
    func validarBotonSolicitar() {
        print(indexOficinaSelect)
        print(numDocText.text ?? "")
        print(nombreText.text ?? "")
        print(apellidoPaternoText.text ?? "")
        print(typeOficinaTextField.text ?? "")
        if (indexOficinaSelect >= 0 && numDocText.text ?? "" != "" && nombreText.text ?? "" != "" && apellidoPaternoText.text ?? "" != "" && typeOficinaTextField.text ?? "" != ""){
            self.pagarView.primaryButton()
        } else {
            self.pagarView.primaryDisabledButton()
        }
    }
    
    
    @objc func donePressed() {
        desblockTextField()
        if valDone == true  && indexOficinaSelect >= 0{
            self.typeOficinaTextField.text = oficinas[indexOficinaSelect].Nombre
            self.typeOficinaTextField.resignFirstResponder()
            print("typeOficina = ", self.typeOficinaTextField.text ?? "")
            self.regPubId = self.oficinas[indexOficinaSelect].RegPubId
            self.oficRegId = self.oficinas[indexOficinaSelect].OficRegId
            
            valoficinaOrigen = "\(self.regPubId)\(self.oficRegId)"
            print("valoficinaOrigen:---:>>>",valoficinaOrigen)
        }else{
          
            self.areaRegistralText.text = tipoDocumentosEntities[indexSelect].nombreAbrev
            self.areaRegistralText.resignFirstResponder()
            self.typeNameDocument = self.tipoDocumentosEntities[indexSelect].tipoDocId
            self.typeDocument = self.tipoDocumentosEntities[indexSelect].tipoDocId
            
            print("self.typeNameDocument:---:>>>",self.typeNameDocument)
            
            self.nombreText.isEnabled = false
            self.apellidoPaternoText.isEnabled = false
            self.apellidoMaternoText.isEnabled = false
            
//            self.pagarView.primaryButton()
            self.isChecked = true
            
            
            self.isValidDocument = false
            if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
                self.maxLength = 8
                self.minLength = 8
                
                self.numDocText.keyboardType = .numberPad
            } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
                self.maxLength = 9
                self.minLength = 9
              //  self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
                self.numDocText.keyboardType = .alphabet
            } else {
                self.maxLength = 20
                self.minLength = 8
                //self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
                self.numDocText.keyboardType = .alphabet
                self.nombreText.isEnabled = true
                self.apellidoPaternoText.isEnabled = true
                self.apellidoMaternoText.isEnabled = true
                self.isValidDocument = true
            }
            self.numDocText.text = ""
            self.nombreText.text = ""
            self.apellidoPaternoText.text = ""
            self.apellidoMaternoText.text = ""
            self.cleanTextField()
        }
        print("validar: ")
        self.validarBotonSolicitar()
    }
    
    func desblockTextField() {
        self.apellidoPaternoText.isEnabled = true
        self.apellidoMaternoText.isEnabled = true
        self.nombreText.isEnabled = true
        self.numDocText.resignFirstResponder()
    }
    
    func setValues() {
        let usuario = UserPreferencesController.usuario()
       
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
              typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
              typeDocument = "03"
        }
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numDocText.resignFirstResponder()
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
        if !(numDocText.text?.isEmpty ?? true) {
            self.textFieldShouldReturn(numDocText)
        }
    }
    
    func isValidarDatos() -> Bool{
        guard let document = numDocText.text, !document.isEmpty ,
              let name = nombreText.text, !name.isEmpty ,
              let apePatPN = apellidoPaternoText.text,!apePatPN.isEmpty else {
            return false
        }
        return true
    }
    
    @objc private func onTapPagarView(){
        if (isValidarDatos()) {
            let storyboard = UIStoryboard(name: Constant.StoryBoardName.payments, bundle: nil)
            let nameIdentifier = Constant.Identifier.paymentsViewController
            if let vc = storyboard.instantiateViewController(withIdentifier: nameIdentifier ) as? PaymentsViewController {
                let usuario = UserPreferencesController.usuario()
                vc.paymentItem.codCerti = certificadoId
                vc.paymentItem.codArea = areaRegId
                vc.paymentItem.oficinaOrigen = valoficinaOrigen
                vc.paymentItem.tpoPersona = "N"
                vc.paymentItem.apePaterno = usuario.priApe
                vc.paymentItem.apeMaterno = usuario.segApe
                vc.paymentItem.nombre = usuario.nombres
                vc.paymentItem.razSoc = .empty
                vc.paymentItem.tpoDoc = usuario.tipoDoc
                vc.paymentItem.numDoc = usuario.nroDoc
                vc.paymentItem.email = usuario.email
                vc.paymentItem.tipPerPN = "N"
                vc.paymentItem.apePatPN = apePatPN
                vc.paymentItem.apeMatPN = apeMatPN
                vc.paymentItem.nombPN = nombPN
                vc.paymentItem.razSocPN = ""
                vc.paymentItem.tipoDocPN = self.typeDocument
                vc.paymentItem.numDocPN = numDocPN
                vc.paymentItem.costoServicio = precOfic
                vc.paymentItem.costoTotal = precOfic
                vc.paymentItem.usrId = "\(usuario.idUser)"
                vc.paymentItem.codArea = areaRegId
                vc.areaRegId = areaRegId
                vc.certificadoId = certificadoId
                vc.precOfic = precOfic
                vc.titleBar = titleBar
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            showMessageAlert(message: "Debe de ingresar el número de documento")
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
    }
    
    
    func changeTypeOficina(index: Int) {
        valDone = true
        self.indexOficinaSelect = index
        
    }
    
    func changeTypeDocument(index: Int) {
        
        valDone = false
        self.indexSelect = index
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
    func validateTypeDocument() {
        self.numDocument = self.numDocText.text ??  .empty
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty) {
                self.presenter.validarDni()
                self.numDocText.resignFirstResponder()
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
                self.numDocText.resignFirstResponder()
            }
        }
        else {
            validarBotonSolicitar()
        }
    }
    
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            self.nombreText.text = jsonValidacionDni.nombres
            self.apellidoPaternoText.text = jsonValidacionDni.apellidoPaterno
            self.apellidoMaternoText.text = jsonValidacionDni.apellidoMaterno
            
            self.apePatPN = jsonValidacionDni.apellidoPaterno
            self.apeMatPN = jsonValidacionDni.apellidoMaterno
            self.nombPN = jsonValidacionDni.nombres
            // self.razSocPN = jsonValidacionDni.type
           //  self.tipoDocPN = jsonValidacionDni.type
            self.numDocPN = jsonValidacionDni.dni
            blockTextField()
            validarBotonSolicitar()
            
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
            nombreText.clearText()
            apellidoMaternoText.clearText()
            apellidoPaternoText.clearText()
        }
       
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            self.nombreText.text = jsonValidacionCe.strNombres
            self.apellidoPaternoText.text = jsonValidacionCe.strPrimerApellido
            self.apellidoMaternoText.text = jsonValidacionCe.strSegundoApellido
            
            
            self.apePatPN = jsonValidacionCe.strPrimerApellido
            self.apeMatPN = jsonValidacionCe.strSegundoApellido
            self.nombPN = jsonValidacionCe.strNombres
            validarBotonSolicitar()
            
        } else {
            self.isValidDocument = false
            let mensaje = message.isEmpty ? "El documento no se ha encontrado" : message
            showMessageAlert(message: mensaje)
            nombreText.clearText()
            apellidoMaternoText.clearText()
            apellidoPaternoText.clearText()
        }
        blockTextField()
    }
    
    func blockTextField() {
        self.apellidoPaternoText.isEnabled = false
        self.apellidoMaternoText.isEnabled = false
        self.nombreText.isEnabled = false
        self.numDocText.resignFirstResponder()
    }
    
    func cleanTextField() {
        [apellidoPaternoText,apellidoMaternoText,numDocText, nombreText].forEach { item in
            item?.text = .empty
        }
        self.numDocText.resignFirstResponder()
        self.pagarView.primaryDisabledButton()
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PublicidadRegistroPerNatViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            //return self.tipoCertificado[row]
            
            return self.tipoDocumentosEntities[row].nombreAbrev
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
        case 2:
            self.changeTypeDocument(index: row)
        default:
            return
        }
        
    }
}

extension PublicidadRegistroPerNatViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         if numDocText == textField {
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            let regex = "^[a-zA-Z0-9]*$"
            let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
            let textTest = NSPredicate(format: "SELF MATCHES %@", regex)
            
            if(updatedText.count > 20){
                return false
            }
            
            let newStr = updatedText
            if (!textTest.evaluate(with: newStr)) {
                return false
            }
            
            var text = textFieldText
            if range.length == Int.zero {
                text.append(contentsOf: string)
            } else {
                text = String(text.dropLast())
            }
           
            textField.text = text.uppercased()
            if (count == maxLength) {
                self.validateTypeDocument()
                textField.resignFirstResponder()
            }
        }else {
            if let sdcTextField = textField as? SDCTextField {
                let result = sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
                if numDocText == sdcTextField && sdcTextField.getMaxLengths() == 8 {
                    
                }
                self.validateTypeDocument()

                return result
            }
        }
        return false
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        validarBotonSolicitar()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.validateTypeDocument()
        textField.resignFirstResponder()
        return true
    }
    
}
