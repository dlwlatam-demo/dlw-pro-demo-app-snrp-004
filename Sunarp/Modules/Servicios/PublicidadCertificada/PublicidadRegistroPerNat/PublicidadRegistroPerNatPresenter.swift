//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class PublicidadRegistroPerNatPresenter {
    
    private weak var controller: PublicidadRegistroPerNatViewController?
    
    lazy private var modelCons: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
 
    init(controller: PublicidadRegistroPerNatViewController) {
        self.controller = controller
    }
    
}

extension PublicidadRegistroPerNatPresenter: GenericPresenter {
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        //let guid = UserPreferencesController.getGuid()
        self.modelDoc.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }

    func didiListadoOficina() {
     //   self.controller?.loaderView(isVisible: true)
        self.consultaLiteralModel.getListaOficinaRegistral{ (arrayOficinas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    func validarDni() {
        self.controller?.loaderView(isVisible: true)
        let dni = controller?.numDocument ?? ""
        let fecEmi = controller?.dateOfIssue ?? ""
        let guid = UserPreferencesController.getGuid()
        self.modelCons.postValidarDniConsul(dni: dni, fecEmi: fecEmi, guid: guid) { (jsonValidacionDni) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionDni.msgResult.isEmpty
            self.controller?.loadDatosDni(state, message: jsonValidacionDni.msgResult, jsonValidacionDni: jsonValidacionDni)
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
        self.modelCons.postValidarCeConsul(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }
    }
    
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getAreasRegistrales{ (arrayArea) in
            self.controller?.loadAreas(areaResponse: arrayArea)
        }
    }
    
    func loadGrupos(codGrupoLibroArea: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.getGrupoLibro(codGrupoLibroArea: codGrupoLibroArea){ (arrayGrupo) in
            self.controller?.loadGrupos(grupoResponse: arrayGrupo)
        }
    }
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
           // self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
          //  self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            //self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    func postDetalleAsientosPublicaSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, tipPerPN: String, apePatPN: String, apeMatPN: String, nombPN: String, razSocPN: String, tipoDocPN: String, numDocPN: String, costoServicio: String, costoTotal: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosPublicaSaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, tipPerPN: tipPerPN, apePatPN: apePatPN, apeMatPN: apeMatPN, nombPN: nombPN, razSocPN: razSocPN, tipoDocPN: tipoDocPN, numDocPN: numDocPN, costoServicio: costoServicio, costoTotal: costoTotal, usrId: usrId) { (niubizResponse) in
            
            // self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
    /*
    func postDetalleAsientosLiteralSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaLiteralModel.postDetalleAsientosLiteralSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (niubizResponse) in
            
             self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
     */
    
        
}
