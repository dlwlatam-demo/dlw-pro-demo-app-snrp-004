//
//  OtorganteTableViewCell.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 19/08/23.
//

import UIKit

protocol DeleteOtorganteProtocol {
    func deleteItemOtorgante(item:String, index: Int)
}

class OtorganteTableViewCell: UITableViewCell {

    static var identifier = String(describing: OtorganteTableViewCell.self)
    @IBOutlet weak var deleteView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var delegate:DeleteOtorganteProtocol?
    var indexCell = 0
    var itemCell = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapDeleteView = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        deleteView.isUserInteractionEnabled = true
        deleteView.addGestureRecognizer(tapDeleteView)
    }

    func setup(item: String, index: Int){
        itemCell = item
        nameLabel.text = item
        indexCell = index
    }

    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        print(itemCell)
        print(indexCell)
        delegate?.deleteItemOtorgante(item: itemCell, index: indexCell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
