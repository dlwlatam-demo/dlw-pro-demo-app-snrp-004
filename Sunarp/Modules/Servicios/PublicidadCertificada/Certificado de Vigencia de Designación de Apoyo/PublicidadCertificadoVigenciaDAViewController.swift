//
//  PublicidadCertificadoVigenciaDAViewController.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 17/08/23.
//

import Foundation
import UIKit
import VisaNetSDK

class PublicidadCertificadoVigenciaDAViewController: UIViewController {
    
    @IBOutlet weak var oficinaView: UIView!
    @IBOutlet weak var oficinaRegistalSelect: UITextField!
    
    @IBOutlet weak var numeroPartidaText: SDCTextField!
    @IBOutlet weak var numeroAsientoText: SDCTextField!
    
    @IBOutlet weak var checkImg: UIImageView!
    @IBOutlet weak var stackDatosCheck: UIStackView!
    @IBOutlet weak var partidaRMP: SDCTextField!
    @IBOutlet weak var asientoRMP: SDCTextField!
    
    @IBOutlet weak var apellidoPOtorganteText: SDCTextField!
    @IBOutlet weak var apellidoMOtorganteText: SDCTextField!
    @IBOutlet weak var nombreOtorganteText: SDCTextField!
    @IBOutlet weak var agregarOButton: UIButton!
    @IBOutlet weak var otorganteTable: UITableView!
    @IBOutlet weak var otorganteTableView: UIView!
    
    @IBOutlet weak var apellidoPApoyoText: SDCTextField!
    @IBOutlet weak var apellidoMApoyoText: SDCTextField!
    @IBOutlet weak var nombreApoyoText: SDCTextField!
    @IBOutlet weak var agregarAButton: UIButton!
    @IBOutlet weak var apoyoTable: UITableView!
    @IBOutlet weak var apoyoTableView: UIView!
    
    var loading: UIAlertController!
    @IBOutlet weak var solicitarButton: UIButton!
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var certificadoId: String = ""
    var precOfic: String = ""
    var valoficinaOrigen: String = ""
    
    private lazy var presenter: PublicidadCertificadoVigenciaDAPresenter = {
        return PublicidadCertificadoVigenciaDAPresenter(controller: self)
    }()
    
    var oficinas: [OficinaRegistralEntity] = []
    var itemOficina:OficinaRegistralEntity?
    var oficionaRegistralPickerView = UIPickerView()
    var indexItemOficina = 0
    
    var arrayOtorgante = [String]()
    var arrayApoyo = [String]()
    var isCheck: Bool = false
    var titleBar:String = ""
    var lstOtorgante = [Interviniente]()
    var lstApoyo = [Interviniente]()

    var areaRegId:String?
    var codGrupoLibroArea:String?
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var pagoExitoso:PagoExitosoEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBar()
        configureTables()
        configureAction()
        setup()
        configTextField()
    }
    
    func configTextField(){
        numeroPartidaText.delegate = self
        numeroPartidaText.maxLength = 10
        numeroPartidaText.valueType = .alphaNumeric
        numeroPartidaText.resignFirstResponder()
       
        numeroAsientoText.delegate = self
        numeroAsientoText.maxLength = 5
        numeroAsientoText.valueType = .onlyNumbers
        numeroAsientoText.resignFirstResponder()
        
        partidaRMP.delegate = self
        partidaRMP.maxLength = 10
        partidaRMP.valueType = .alphaNumeric
        partidaRMP.resignFirstResponder()
        
        asientoRMP.delegate = self
        asientoRMP.maxLength = 5
        asientoRMP.valueType = .onlyNumbers
        asientoRMP.resignFirstResponder()
        
        apellidoPOtorganteText.delegate = self
        apellidoPOtorganteText.maxLength = 50
        apellidoPOtorganteText.valueType = .alphaNumericWithSpace
        apellidoPOtorganteText.resignFirstResponder()
        
        apellidoMOtorganteText.delegate = self
        apellidoMOtorganteText.maxLength = 50
        apellidoMOtorganteText.valueType = .alphaNumericWithSpace
        apellidoMOtorganteText.resignFirstResponder()
        
        nombreOtorganteText.delegate = self
        nombreOtorganteText.maxLength = 50
        nombreOtorganteText.valueType = .alphaNumericWithSpace
        nombreOtorganteText.resignFirstResponder()
        
        apellidoPApoyoText.delegate = self
        apellidoPApoyoText.maxLength = 50
        apellidoPApoyoText.valueType = .alphaNumericWithSpace
        apellidoPApoyoText.resignFirstResponder()
        
        apellidoMApoyoText.delegate = self
        apellidoMApoyoText.maxLength = 50
        apellidoMApoyoText.valueType = .alphaNumericWithSpace
        apellidoMApoyoText.resignFirstResponder()
        
        nombreApoyoText.delegate = self
        nombreApoyoText.maxLength = 50
        nombreApoyoText.valueType = .alphaNumericWithSpace
        nombreApoyoText.resignFirstResponder()
        
    
    }
    
    
    func configureTables() {
        apoyoTable.delegate = self
        apoyoTable.dataSource = self
        otorganteTable.delegate = self
        otorganteTable.dataSource = self
        
        numeroAsientoText.delegate = self
        numeroPartidaText.delegate = self
        partidaRMP.delegate = self
        asientoRMP.delegate = self
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: titleBar)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    
    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupStyle(myView: UIView) {
        let grayColor = UIColor.gray.withAlphaComponent(0.5)
        myView.layer.borderColor = grayColor.cgColor
        myView.layer.borderWidth = 1.0
        myView.layer.cornerRadius = 4.0
    }
    
    func setup() {
        oficionaRegistralPickerView.delegate = self
        oficionaRegistralPickerView.dataSource = self
        oficinaRegistalSelect.inputView = self.oficionaRegistralPickerView
        oficinaRegistalSelect.inputAccessoryView = self.createToolbarOficinaRegistral()
        stackDatosCheck.isHidden = true
        
        setupStyle(myView: apoyoTableView)
        setupStyle(myView: otorganteTableView)
        setupStyle(myView: oficinaView)
        
        self.presenter.getListaOficinaRegistral()
    }
    
    func createToolbarOficinaRegistral() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneOficinaRegistral))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelOficinaRegistral))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    @objc func doneOficinaRegistral() {
        self.itemOficina = oficinas[indexItemOficina]
        self.oficinaRegistalSelect.text = oficinas[indexItemOficina].Nombre
        self.oficinaRegistalSelect.resignFirstResponder()
        
        let regPubId = self.oficinas[indexItemOficina].RegPubId
        let oficRegId = self.oficinas[indexItemOficina].OficRegId
        self.valoficinaOrigen = "\(regPubId)\(oficRegId)"

        
    }
    
    
    @objc func cancelOficinaRegistral() {
        self.view.endEditing(true)
    }
    
    func configureAction() {
    
        let tapCheck = UITapGestureRecognizer(target: self, action: #selector(checkTapped(tapGestureRecognizer:)))
        checkImg.isUserInteractionEnabled = true
        checkImg.addGestureRecognizer(tapCheck)
    }
    
    @objc func checkTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let itemCheck = !isCheck
        isCheck = itemCheck
        checkImg.image = (itemCheck == true) ? UIImage(named: "Checked") :  UIImage(named: "Check")
        stackDatosCheck.isHidden = (itemCheck) ? false : true
        partidaRMP.text = .empty
        asientoRMP.text = .empty
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func actionAddOtorgante() {
        guard let otorganteAP =  apellidoPOtorganteText.text, !otorganteAP.isEmpty,
              let otorganteNombre =  nombreOtorganteText.text, !otorganteNombre.isEmpty
        else { return }
        let otorganteAM =  apellidoMOtorganteText.text ?? .empty
        let fullName = String(format: "%@ %@ %@, Natural", otorganteAP,otorganteAM, otorganteNombre)
        arrayOtorgante.append(fullName)
        let interviniente: Interviniente = Interviniente()
        interviniente.tipoInter = 1
        interviniente.tipoPart = 1
        interviniente.apMaterno = otorganteAM
        interviniente.apPaterno = otorganteAP
        interviniente.nombres = otorganteNombre
        interviniente.razSocial = ""
        
        lstOtorgante.append(interviniente)
        print(arrayOtorgante.count)
        
        otorganteTable.reloadData()
        
        nombreOtorganteText.text = .empty
        apellidoPOtorganteText.text = .empty
        apellidoMOtorganteText.text = .empty
        
        validarBotonSolicitar()
    }
    
    func actionAddApoyo() {
        guard let apoyoAP = apellidoPApoyoText.text, !apoyoAP.isEmpty,
              let apoyoNombre = nombreApoyoText.text, !apoyoNombre.isEmpty
        else { return }
        let apoyoAM = apellidoMApoyoText.text ?? .empty
        let fullName = String(format: "%@ %@ %@, Natural", apoyoAP,apoyoAM, apoyoNombre)
        arrayApoyo.append(fullName)
        apoyoTable.reloadData()
        let interviniente: Interviniente = Interviniente()
        interviniente.tipoInter = 2
        interviniente.tipoPart = 1
        interviniente.apMaterno = apoyoAM
        interviniente.apPaterno = apoyoAP
        interviniente.nombres = apoyoNombre
        interviniente.razSocial = ""
        
        lstApoyo.append(interviniente)

        
        apellidoPApoyoText.text = .empty
        nombreApoyoText.text = .empty
        apellidoMApoyoText.text = .empty
        
        validarBotonSolicitar()
    }
    
    func validarBotonSolicitar() {
        if isCheck {
            guard let partidaRMP = partidaRMP.text, !partidaRMP.isEmpty,
                  let asientoRMP = asientoRMP.text, !asientoRMP.isEmpty,
                  !arrayOtorgante.isEmpty, !arrayApoyo.isEmpty
            else {
                solicitarButton.isEnabled = false
                solicitarButton.tintColor = .white
                solicitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
            
        } else {
            guard !arrayOtorgante.isEmpty, !arrayApoyo.isEmpty,
                  let numPart = numeroPartidaText.text, !numPart.isEmpty
            else {
                solicitarButton.isEnabled = false
                solicitarButton.tintColor = .white
                solicitarButton.backgroundColor = UIColor(named: "IconColor")
                return
            }
        }
        
        solicitarButton.isEnabled = true
        solicitarButton.tintColor = .white
        solicitarButton.backgroundColor = UIColor(named: "ButtonColor")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    @IBAction func addOtorganteAction(_ sender: UIButton) {
        actionAddOtorgante()
    }
    
    @IBAction func addApoyoAction(_ sender: UIButton) {
        actionAddApoyo()
    }
    
    @IBAction func solitiarAction(_ sender: UIButton) {
        validatePartidaApoyos()
        
    }
    
    func validatePartidaApoyos() {
        if let oficina = itemOficina {
            
            let regPubId = oficina.RegPubId
            let oficRegId = oficina.OficRegId
            let numPart = numeroPartidaText.text ?? .empty
            
            var numPartMP = "-"
            if let numeroPartida = partidaRMP.text, !numeroPartida.isEmpty,
               let asiento = asientoRMP.text, !asiento.isEmpty {
                numPartMP =  "\(numeroPartida)" // -\(asiento)
            }
            
            print(numPartMP)
            
            presenter.validatePartidaApoyos(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId ?? .empty, libroArea: codGrupoLibroArea ?? .empty, numPart: numPart, numPartMP: numPartMP)
        }
     
    }
    
    func validatePartidaApoyos(item: PartidaModel){
    
        if item.estado != 1 {
            print("Punto de control 1")
            showPayment(item: item)
        } else {
            print("Punto de control 2")
            showAlert(title: "Información", message: item.msj ?? .empty, btnMessage: "Aceptar")
        }
    }
    
    func showPayment(item: PartidaModel){
        print("Punto de control 3")
        //loaderView(isVisible: false
        
        
        
        let storyboard = UIStoryboard(name: "Payments", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        
        let usuario = UserPreferencesController.usuario()
            
            vc.paymentItem.tpoPersona = "N"
            vc.paymentItem.apePaterno = usuario.priApe
            vc.paymentItem.apeMaterno = usuario.segApe
            vc.paymentItem.nombre = usuario.nombres
            vc.paymentItem.razSoc = ""
            vc.paymentItem.tpoDoc = usuario.tipoDoc
            vc.paymentItem.numDoc = usuario.nroDoc
            vc.paymentItem.email = usuario.email
            vc.paymentItem.ipRemote = ""
            vc.paymentItem.sessionId = ""
            if(item.refNumPart == nil) {
                vc.paymentItem.refNumPart = ""
            }
            else {
                vc.paymentItem.refNumPart = String(item.refNumPart! )
            }
            let numPart = numeroPartidaText.text ?? .empty
        
            let partidaRMP = partidaRMP.text ?? .empty
            let asientoRMP = asientoRMP.text ?? .empty
            
            vc.paymentItem.partida = numPart
            vc.paymentItem.asiento = numeroAsientoText.text ?? .empty
            vc.paymentItem.numPartidaMP = partidaRMP
            vc.paymentItem.numAsientoMP = asientoRMP
                
            vc.paymentItem.usrId = Constant.API_USRID
            vc.titleBar = titleBar
            vc.paymentItem.costoServicio = precOfic
            vc.paymentItem.costoTotal = precOfic
            //Others data information
            vc.certificadoId = certificadoId
            vc.paymentItem.codCerti = certificadoId
            vc.precOfic = precOfic
            vc.paymentItem.oficinaOrigen = valoficinaOrigen
            var strLstApoyo: String = ""
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(lstApoyo)
                strLstApoyo = String(data: data, encoding: .utf8)!
            } catch {
                // Handle error
                print(error.localizedDescription)
            }
            vc.paymentItem.lstApoyos = strLstApoyo
            var strLstOtorgante: String = ""
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(lstOtorgante)
                strLstOtorgante = String(data: data, encoding: .utf8)!
            } catch {
                // Handle error
                print(error.localizedDescription)
            }
            vc.paymentItem.lstOtorgantes = strLstOtorgante
        
            dump(vc.paymentItem)
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func showAlert(title: String, message: String, btnMessage: String) {
       let alert = UIAlertController(title: title,
                                     message: message,
                                     preferredStyle: UIAlertController.Style.alert)
       alert.addAction(UIAlertAction(title: btnMessage,
                                     style: UIAlertAction.Style.default) { _ in
//           self.navigationController?.popViewController(animated: true)
       })
       self.present(alert, animated: true, completion: nil)
   }
    
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("pinhash")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
 
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz = " + self.transactionIdNiubiz)
        self.loadNiubizScreen(pinHash: self.pinHash)
    }
    
    func loadNiubizScreen(pinHash: String) {
        let usuario = UserPreferencesController.usuario()
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.endPointProdURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .prod
        } else {
            Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
            Config.merchantID = self.visaKeys.merchantId
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //"\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente

        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        print("presentVisaPaymentForm")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension PublicidadCertificadoVigenciaDAViewController: UIPickerViewDelegate, UIPickerViewDataSource  {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return oficinas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return oficinas[row].Nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        indexItemOficina = row
        itemOficina = oficinas[row]
    }
}

extension PublicidadCertificadoVigenciaDAViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == otorganteTable {
            return arrayOtorgante.count
        } else {
            return arrayApoyo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == otorganteTable {
            let cellRow = otorganteTable.dequeueReusableCell(withIdentifier: OtorganteTableViewCell.identifier, for: indexPath)
            guard let cell = cellRow as?  OtorganteTableViewCell else { return UITableViewCell()}
            let item = arrayOtorgante[indexPath.row]
            cell.delegate = self
            cell.setup(item: item, index: indexPath.row)
            
            return cell
        } else {
            let cellRow = apoyoTable.dequeueReusableCell(withIdentifier: ApoyoTableViewCell.identifier, for: indexPath)
            guard let cell = cellRow as?  ApoyoTableViewCell else { return UITableViewCell()}
            let item = arrayApoyo[indexPath.row]
            cell.delegate = self
            cell.setup(item: item, index: indexPath.row)
            
            return cell
        }
    }
    
}


//MARK: - VISANET DELEGATE SECTION

extension PublicidadCertificadoVigenciaDAViewController : VisaNetDelegate{
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
            showMessageAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoViewController) as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                vc.monto = self.nsolMonLiq
                vc.razonSocial = ""
                vc.derecho = ""
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}





extension PublicidadCertificadoVigenciaDAViewController: DeleteOtorganteProtocol, DeleteApoyoProtocol {
    
    func deleteItemApoyo(item: String, index: Int) {
        arrayApoyo.remove(at: index)
        lstApoyo.remove(at: index)
        apoyoTable.reloadData()
        validarBotonSolicitar()
    }
    
    func deleteItemOtorgante(item: String, index: Int) {
        arrayOtorgante.remove(at: index)
        lstOtorgante.remove(at: index)
        otorganteTable.reloadData()
        validarBotonSolicitar()
    }
}


extension PublicidadCertificadoVigenciaDAViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        self.validarBotonSolicitar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            //Si no hay texto, llenar con ceros y actualizar el campo de texto
            //textField.text = "000000"
            return true
        }
        
        // Verificar si el texto es numérico
        if let intValue = Int(text) {
            var count = textField.maxLength
            if count > 5 {
                count = 8
            }
            let formato = "%0\(count)d"
            
            if let formattedValue = String(format: formato, intValue) as NSString? {
                textField.text = formattedValue as String
            }
            else {
                // Si no es numérico, convertir a mayúsculas
                textField.text = text.uppercased()
            }
            
        } else {
            // Si no es numérico, convertir a mayúsculas
            textField.text = text.uppercased()
        }
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
}
