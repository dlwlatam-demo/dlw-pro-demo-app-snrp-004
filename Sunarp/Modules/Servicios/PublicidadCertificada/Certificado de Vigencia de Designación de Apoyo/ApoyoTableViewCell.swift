//
//  ApoyoTableViewCell.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 19/08/23.
//

import UIKit

protocol DeleteApoyoProtocol {
    func deleteItemApoyo(item:String, index: Int)
}

class ApoyoTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteView: UIView!
    static var identifier = String(describing: ApoyoTableViewCell.self)
    var delegate:DeleteApoyoProtocol?
    var indexCell = 0
    var itemCell = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapDeleteView = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        deleteView.isUserInteractionEnabled = true
        deleteView.addGestureRecognizer(tapDeleteView)
    }

    func setup(item: String, index: Int){
        itemCell = item
        nameLabel.text = item
        indexCell = index
    }

    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        delegate?.deleteItemApoyo(item: itemCell, index: indexCell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
