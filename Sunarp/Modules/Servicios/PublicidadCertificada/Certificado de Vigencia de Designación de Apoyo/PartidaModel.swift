//
//  PartidaModel.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 19/08/23.
//

import Foundation

struct PartidaModel: Codable {
    
    public let estado: Int?
    public let msj: String?
    
    public let refNumPart: Int?
    public let refNumPartMP: Int?
    public let codigoGla: String?
    public let codigoLibro: String?
    public let numPartida: String?
    public let numPlaca: String?
    
}
