//
//  PublicidadCertificadoVigenciaDAPresenter.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 19/08/23.
//

import Foundation

class PublicidadCertificadoVigenciaDAPresenter {
    
    private weak var controller: PublicidadCertificadoVigenciaDAViewController?
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: PublicidadCertificadoVigenciaDAViewController) {
        self.controller = controller
    }
}

extension PublicidadCertificadoVigenciaDAPresenter: GenericPresenter {
    
    func getListaOficinaRegistral () {
        self.consultaLiteralModel.getListaOficinaRegistral{ (arrayOficinas) in
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    func validatePartidaApoyos(regPubId: String, oficRegId: String, areaRegId: String, libroArea: String, numPart: String, numPartMP: String)  {
        self.consultaPublicaModel.validatePartidaApoyos(regPubId: regPubId, oficRegId: oficRegId, areaRegId: areaRegId, libroArea: libroArea, numPart: numPart, numPartMP: numPartMP) { itemModel in
            print("Punto de control 6")
            self.controller?.validatePartidaApoyos(item: itemModel!)
        }
    }
    
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        print("getVisaKeys")
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        print("getTokenNiubiz")
        print(urlVisa)
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        print("getNiubizPinHash")
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId() {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }
    
    
}
