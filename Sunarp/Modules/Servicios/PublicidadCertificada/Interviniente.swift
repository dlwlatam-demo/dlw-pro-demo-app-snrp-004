//
//  Interviniente.swift
//  Sunarp
//
//  Created by Percy Silva on 12/11/23.
//

import Foundation


class Interviniente : Codable{
    var tipoInter:Int = 0
    var tipoPart:Int = 0
    var apPaterno:String = ""
    var apMaterno:String = ""
    var nombres:String = ""
    var razSocial:String = ""
}
