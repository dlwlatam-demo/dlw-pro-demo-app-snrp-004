//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class PagarBusquedaPresenter {
    
    private weak var controller: PagarBusquedaViewController?
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: PagarBusquedaViewController) {
        self.controller = controller
    }
    
}

extension PagarBusquedaPresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getAreasRegistrales{ (arrayArea) in
            self.controller?.loadAreas(areaResponse: arrayArea)
        }
    }
    
    func loadGrupos(codGrupoLibroArea: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.getGrupoLibro(codGrupoLibroArea: codGrupoLibroArea){ (arrayGrupo) in
            self.controller?.loadGrupos(grupoResponse: arrayGrupo)
        }
    }
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
            
    func getTransactionId(userName: String, password: String, urlString: String) {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
    }
}
