//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class PagarBusquedaViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var zonasTable: UITableView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var participanteText: UITextField!
    @IBOutlet weak var nombreText: UITextField!
    @IBOutlet weak var apellidoPaternoText: UITextField!
    @IBOutlet weak var apellidoMaternoText: UITextField!
    @IBOutlet weak var costoTotalLabel: UILabel!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var selectAreaRegistral: AreaEntity!
    var selectParticipante: GrupoEntity!
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    var transactionIdNiubiz: String = ""
    var pinHash: String = ""
    var areaRow = 0
    var participanteRow = 0
    var typeController: PagoExitosoViewController.Controller = .busquedaNombre
    var pagoExitoso:PagoExitosoEntity?
    
    
    private lazy var presenter: PagarBusquedaPresenter = {
       return PagarBusquedaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        setupKeyboard()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Búsqueda por Nombre")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.participanteText.borderAndPaddingLeftAndRight()
        self.nombreText.borderAndPaddingLeftAndRight()
        self.apellidoPaternoText.borderAndPaddingLeftAndRight()
        self.apellidoMaternoText.borderAndPaddingLeftAndRight()
        self.pagarView.primaryButton()
        self.checkView.borderCheckView()
        self.pagarView.primaryDisabledButton()
        
        self.nombreText.delegate = self
        self.apellidoPaternoText.delegate = self
        self.apellidoMaternoText.delegate = self
        
        self.nombreText.maxLength = 50
        self.apellidoPaternoText.maxLength = 50
        self.apellidoMaternoText.maxLength = 50

        self.isChecked = false
        self.areaRegistralPickerView.tag = 1
        self.participantePickerView.tag = 2
        
        self.zonasTable.register(UINib(nibName: ZonaSelectCell.identifier, bundle: App.bundle), forCellReuseIdentifier: ZonaSelectCell.identifier)
        self.zonasTable.delegate = self
        self.zonasTable.dataSource = self        
        self.zonasTable.reloadData()
                
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbarArea()
        
        self.participantePickerView.delegate = self
        self.participantePickerView.dataSource = self
        self.participanteText.inputView = self.participantePickerView
        self.participanteText.inputAccessoryView = self.createToolbarParticipante()
        
        let amount = String(format: "S/ %.2f", 0.0)
                        
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkView.addGestureRecognizer(tapChecked)
        
        let tapTerminoCondicionesGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapTerminoCondicionesLabel))
        self.terminosLabel.addGestureRecognizer(tapTerminoCondicionesGesture)
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func createToolbarArea() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedArea))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    func createToolbarParticipante() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedParticipante))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func donePressedArea() {
        self.selectAreaRegistral = areas[areaRow]
        self.areaRegistralText.text = areas[areaRow].descGrupoLibroArea
        self.areaRegistralText.resignFirstResponder()
        self.presenter.loadGrupos(codGrupoLibroArea: areas[areaRow].codGrupoLibroArea)
        
        var precOfic = self.selectAreaRegistral.precOfic
        
        if (areas[areaRow].codGrupoLibroArea != "21") {
            precOfic = self.selectAreaRegistral.precOfic * Double(zonas.count)
        }
        
        self.nsolMonLiq = String(format: "%.2f", precOfic)
        let amount = String(format: "S/ %.2f", precOfic)
                        
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
        
        cleanTextFields()
    }
    
    func cleanTextFields() {
        nombreText.text = ""
        apellidoPaternoText.text = ""
        apellidoMaternoText.text = ""
        participanteText.text = ""
    }
    
    @objc func donePressedParticipante() {
        
        self.selectParticipante = grupos[participanteRow]
        self.participanteText.text = grupos[participanteRow].noTipoPersona
        self.participanteText.resignFirstResponder()
        switch participanteText.text {
        case "Persona Jurídica":
            nombreText.placeholder = "Razón Social"
            apellidoMaternoText.isHidden = true
            apellidoPaternoText.isHidden = true
        case "Derechos Mineros":
            nombreText.placeholder = "Ingresar el Derecho Minero"
            apellidoMaternoText.isHidden = true
            apellidoPaternoText.isHidden = true
        case "Sociedad Minera":
            nombreText.placeholder = "Ingresar Sociedad Minera"
            apellidoMaternoText.isHidden = true
            apellidoPaternoText.isHidden = true
        default:
            nombreText.placeholder = "Nombre"
            apellidoMaternoText.isHidden = false
            apellidoPaternoText.isHidden = false
        }
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
    }
    
    @objc private func onTapTerminoCondicionesLabel(){
       /* if let url = URL(string: "https://www.sunarp.gob.pe/politicas-privacidad.aspx") {
            UIApplication.shared.open(url)
        }*/
        
        self.alertTermyConPopupDialog(title: "Estimado(a) usuario(a):",message: "¿?",primaryButton: "No,regresar",secondaryButton: "Si,remover", addColor: SunarpColors.red, delegate: self)
    }
    
    @objc private func onTapPagarView(){
        var isValid = false
        if participanteText.text == "Persona Jurídica" ||  participanteText.text == "Derechos Mineros" || participanteText.text == "Sociedad Minera" {
            isValid = self.nombreText.text != ""
        }
        else {
            isValid = self.nombreText.text != "" && self.apellidoPaternoText.text != ""
        }
        
        if (self.isChecked && isValid) {
            loaderView(isVisible: true)
            presenter.getVisaKeys()
        }
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            self.pagarView.primaryDisabledButton()
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            self.pagarView.primaryButton()
        }
        self.isChecked = !self.isChecked
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        //presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: SunarpWebService.Niubiz.postNiubiz())
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        //self.pinHash = self.visaKeys.pinHash
        //let urlString = "\(String(visaKeys.urlVisanetCounter))\(String(visaKeys.merchantId))/nextCounter"
        //self.presenter.getTransactionId(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlString: urlString)
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
        
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        let urlString = "\(String(visaKeys.urlVisanetCounter))\(String(visaKeys.merchantId))/nextCounter"
        self.presenter.getTransactionId(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlString: urlString)
    }
    
    func loadNiubizTransactionId(transactionId: String) {
        print("transactionIdNiubiz = " + transactionIdNiubiz)
        self.transactionIdNiubiz = transactionId
        self.loadNiubizScreen()
    }
    
    func loadNiubizScreen() {
        let usuario = UserPreferencesController.usuario()
                
        Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
        Config.merchantID = self.visaKeys.merchantId
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.type = .prod
        } else {
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz // "\(Int.random(in:99999...9999999999))"
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.countable = true
        Config.CE.showAmount = true
        Config.CE.initialAmount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        Config.CE.LastNameField.defaultText = usuario.priApe
        Config.CE.FirstNameField.defaultText = usuario.nombres
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente
        
        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: Constant.Localize.información, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension PagarBusquedaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.areas.count
        case 2:
            return self.grupos.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.areas[row].descGrupoLibroArea
        case 2:
            return self.grupos[row].noTipoPersona
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            if (!areas.isEmpty) {
                areaRow = row
                print("caso1")
                print(pickerView.tag)
                print(component)
                print(row)
            }
        case 2:
            if (!grupos.isEmpty) {
                
                participanteRow = row
                print("caso2")
                print(pickerView.tag)
               
            }
        default:
            return
        }
        
    }
}

extension PagarBusquedaViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.zonas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = zonas[indexPath.row]
        let cell = self.zonasTable.dequeueReusableCell(withIdentifier: ZonaSelectCell.identifier, for: indexPath) as! ZonaSelectCell
        cell.setup(with: item)
        cell.onTapDelete = {[weak self] in
//            self?.zonas.remove(at: indexPath.row)
            self?.zonasTable.reloadData()
        }
        return cell
    }
}

//MARK: - VISANET DELEGATE SECTION

extension PagarBusquedaViewController : VisaNetDelegate{
    
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
           showAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA BXN: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoExitosoViewController") as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                print("----------------------")
                vc.monto = self.nsolMonLiq
                vc.codZonas = obtainCodesOfZone()
                vc.usuario = "APPSNRPIOS"
                
                if participanteText.text == "Persona Jurídica" ||  participanteText.text == "Derechos Mineros" || participanteText.text == "Sociedad Minera" {
                    switch participanteText.text {
                    case "Persona Jurídica":
                        vc.razonSocial = self.nombreText.text ?? ""
                    case "Derechos Mineros":
                        vc.derecho = self.nombreText.text ?? ""
                    case "Sociedad Minera":
                        vc.sociedad = self.nombreText.text ?? ""
                    default:
                        vc.razonSocial = self.nombreText.text ?? ""
                    }
                }
                else {
                    vc.apPaterno = self.apellidoPaternoText.text ?? ""
                    vc.apMaterno = self.apellidoMaternoText.text ?? ""
                    vc.nombre = self.nombreText.text ?? ""
                }
                

                
                vc.controller = typeController
                vc.tipoTitular = self.selectParticipante.coTipoPersona
                vc.codAreaRegistral = self.selectAreaRegistral.codGrupoLibroArea
                vc.desAreaRegistral = self.selectAreaRegistral.descGrupoLibroArea
                vc.usuario = UserPreferencesController.usuario().userKeyId
                vc.controller = .busquedaNombre
                vc.costo = self.nsolMonLiq
                vc.ip = "0.0.0.0"
                vc.codAccion = transactionData.dataMap.actionCode
                vc.codAutoriza = transactionData.dataMap.authorizationCode
                vc.codtienda = transactionData.dataMap.merchant
                vc.concepto = "Boleta Informativa"
                vc.decisionCs = "Accept"
                vc.dscCodAccion = transactionData.dataMap.actionDescription
                vc.dscEci = transactionData.dataMap.eciDescription
                vc.eci = transactionData.dataMap.eci
                vc.estado = transactionData.dataMap.status
                vc.eticket = transactionData.dataMap.idUnico
                vc.fechaYhoraTx = getCurrentDateAndTime()
                vc.idUnico = transactionData.dataMap.idUnico
                vc.idUser = UserPreferencesController.usuario().nroDoc
                vc.impAutorizado = transactionData.dataMap.amount
                vc.nomEmisor = transactionData.dataMap.brandName
                vc.numOrden = transactionData.dataMap.purchaseNumber
                vc.numReferencia = transactionData.dataMap.traceNumber
                vc.oriTarjeta = "D"
                vc.pan = transactionData.dataMap.card
                vc.resCvv2 = transactionData.dataMap.brandHostID
                vc.respuesta = "1"
                vc.reviewTransaction = "false"
                vc.transId = transactionData.dataMap.transactionID
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension PagarBusquedaViewController: TermyCondiDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
          //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
       // print("self.val_aaCont::>>",self.val_aaCont)
        //self.presenter.putDeleteMandato(aaCont: self.val_aaCont, nuCont: self.val_nuCount)
        
        
    }
}


extension PagarBusquedaViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
    
}
