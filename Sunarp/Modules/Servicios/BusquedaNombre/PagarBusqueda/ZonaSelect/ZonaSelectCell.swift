//
//  ZonaSelectCell.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import UIKit

class ZonaSelectCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteView: UIView!
    
    private var zonaItem: ZonaEntity!
    
    var onTapDelete: (() -> Void)?
    
    static let identifier = "ZonaSelectCell"
    
    func setup(with item: ZonaEntity) {
        self.zonaItem = item
        self.nameLabel.text = item.nombre
        
        let onTapCell = UITapGestureRecognizer(target: self, action: #selector(onTapDeleteCell))
        self.deleteView.addGestureRecognizer(onTapCell)
    }
    
    @objc func onTapDeleteCell(){
        self.onTapDelete?()
    }
}
