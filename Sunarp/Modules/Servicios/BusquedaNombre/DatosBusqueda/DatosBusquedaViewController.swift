//
//  DatosBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 24/09/22.
//

import Foundation
import UIKit

class DatosBusquedaViewController: UIViewController {
    
    @IBOutlet weak var formHeaderView: UIView!
    @IBOutlet weak var formBodyView: UIView!
    @IBOutlet weak var titulosTable: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var areaRegistralLabel: UILabel!
    @IBOutlet weak var datosIngresadosLabel: UILabel!
    
    var loading: UIAlertController!
    
    var pagoExitosoEntity: PagoExitosoEntity!
    var codZonas: [String] = []
    var apPaterno: String = ""
    var apMaterno: String = ""
    var nombre: String = ""
    var razonSocial: String = ""
    var tipoTitular: String = ""
    var derecho: String = ""
    var codAreaRegistral: String = ""
    var desAreaRegistral: String = ""
    var sociedad: String = ""
    var userId: String = "APPSNRPIOS"
    var ipDispositivo: String = ""
    var idUnico: String = ""
    var numOrden: String = ""
    var pan: String = ""
    var codAutoriza: String = ""
    var nomEmisor: String = ""
    var eticket: String = ""
    var codtienda: String = ""
    var oriTarjeta: String = ""
    var codAccion: String = ""
    var dscCodAccion: String = ""
    var respuesta: String = ""
    var fechaYhoraTx: String = ""
    var concepto: String = ""
    var idUser: String = ""
    var transId: String = ""
    var visanetResponse: String = ""
    var titulos: [TitularidadEntity] = []
    
    private lazy var presenter: DatosBusquedaPresenter = {
       return DatosBusquedaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Búsqueda por Nombre")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        if let firstStoryboardViewController = navigationController?.viewControllers.first(where: { $0 is ServiciosViewController }) {
            navigationController?.popToViewController(firstStoryboardViewController, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.postConsultaTitularidadAsync(codZonas: self.codZonas, areaRegistral: self.codAreaRegistral, apPaterno: self.apPaterno, apMaterno: self.apMaterno, nombre: self.nombre, razonSocial: self.razonSocial, derecho: self.derecho, sociedad: self.sociedad, tipoTitular: self.tipoTitular, userId: self.userId, ipDispositivo: self.ipDispositivo, idUnico: self.idUnico, numOrden: self.numOrden, pan: self.pan, codAutoriza: self.codAutoriza, nomEmisor: self.nomEmisor, eticket: self.eticket, codtienda: self.codtienda, oriTarjeta: self.oriTarjeta, codAccion: self.codAccion, dscCodAccion: self.dscCodAccion, respuesta: self.respuesta, fechaYhoraTx: self.fechaYhoraTx, concepto: self.concepto, idUser: self.idUser, transId: self.transId, visanetResponse: self.pagoExitosoEntity, deviceId: "iOS", userCrea: "55555")
    }
    
    private func setupDesigner() {
        self.formHeaderView.backgroundCard()
        self.formBodyView.backgroundCard()
        
        self.titulosTable.register(UINib(nibName: ResultadoCell.identifier, bundle: App.bundle), forCellReuseIdentifier: ResultadoCell.identifier)
        self.titulosTable.delegate = self
        self.titulosTable.dataSource = self
    }
        
    @objc private func onTapPagarView(){
        
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadTitulos(titulos: [TitularidadEntity]) {
        let total = titulos.count
        self.titulos = titulos
        self.totalLabel.text = String(describing: total)
        self.areaRegistralLabel.text = self.desAreaRegistral
        if !derecho.isEmpty {
            self.datosIngresadosLabel.text = "\(self.derecho)"
        }
        else if !sociedad.isEmpty {
            self.datosIngresadosLabel.text = "\(self.sociedad)"
        }
        else if !razonSocial.isEmpty {
            self.datosIngresadosLabel.text = "\(self.razonSocial)"
        }
        else {
            self.datosIngresadosLabel.text = "\(self.nombre) \(self.apPaterno) \(self.apMaterno)"
        }
        
        self.titulosTable.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadTitulosAsync(titulosAsync: TitularidadAsyncEntity) {
        let guid = titulosAsync.guid
        sleep(5)
        print("espera 5 seg.")
        
        self.presenter.getStatusConsultaTitularidadAsync(guid: guid)
    }
    
    func loadStatusTitulosAsync(titulosAsync: TitularidadAsyncEntity) {
        let guid = titulosAsync.guid
        let status = (titulosAsync.status == "1")
        if (!status) {
            sleep(10)
            print("espera 10 seg.")
            // self.loaderView(isVisible: false)
            self.presenter.getStatusConsultaTitularidadAsync(guid: guid)
        }
        else {
            self.loadTitulos(titulos: titulosAsync.response)
        }
            
    }
}


extension DatosBusquedaViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titulos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = titulos[indexPath.row]
        let cell = self.titulosTable.dequeueReusableCell(withIdentifier: ResultadoCell.identifier, for: indexPath) as! ResultadoCell
        cell.setup(with: item)
        return cell
    }
}
