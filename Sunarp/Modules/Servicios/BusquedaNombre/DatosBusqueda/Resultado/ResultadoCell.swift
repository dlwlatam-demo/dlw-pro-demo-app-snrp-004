//
//  ResultadoCell.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 24/09/22.
//

import Foundation
import UIKit

class ResultadoCell: UITableViewCell {
    
    @IBOutlet weak var partidaLabel: UILabel!
    @IBOutlet weak var registroPublicoLabel: UILabel!
    @IBOutlet weak var oficinaLabel: UILabel!
    @IBOutlet weak var fichaLabel: UILabel!
    @IBOutlet weak var tomoLabel: UILabel!
    @IBOutlet weak var folioLabel: UILabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var registroLabel: UILabel!
    @IBOutlet weak var participanteLabel: UILabel!
    @IBOutlet weak var documentoIdentidadLabel: UILabel!
    @IBOutlet weak var numeroDocumentoLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var estadoLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
        
    static let identifier = "ResultadoCell"
    
    func setup(with item: TitularidadEntity) {
        
        self.partidaLabel.text = item.numPartida
        self.registroPublicoLabel.text = item.regPubSiglas
        self.oficinaLabel.text = item.nombreOfic
        self.fichaLabel.text = item.fichaId
        self.tomoLabel.text = item.tomoId
        self.folioLabel.text = item.fojaId
        self.areaLabel.text = item.areaRegisDescripcion
        self.registroLabel.text = item.libroDescripcion
        self.participanteLabel.text = item.participanteDesc
        self.documentoIdentidadLabel.text = item.tipoDocumPartic
        self.numeroDocumentoLabel.text = item.numDocumPartic
        self.direccionLabel.text = item.direccionPredio
        if (item.estado == "0") {
            self.estadoLabel.text = "Inactivo"
        } else {
            self.estadoLabel.text = "Activo"
        }
        
    }
    
}
