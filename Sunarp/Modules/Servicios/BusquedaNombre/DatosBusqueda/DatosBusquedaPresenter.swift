//
//  DatosBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 24/09/22.
//

import Foundation


class DatosBusquedaPresenter {
    
    private weak var controller: DatosBusquedaViewController?
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
        
    init(controller: DatosBusquedaViewController) {
        self.controller = controller
    }
    
}

extension DatosBusquedaPresenter: GenericPresenter {
    
    func postConsultaTitularidad(codZonas: [String], areaRegistral: String, apPaterno: String, apMaterno: String, nombre: String, razonSocial: String, derecho: String, sociedad: String, tipoTitular: String, userId: String, ipDispositivo: String, idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, eticket: String, codtienda: String, oriTarjeta: String, codAccion: String, dscCodAccion: String, respuesta: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, visanetResponse: PagoExitosoEntity, deviceId: String, userCrea: String)  {
        self.controller?.loaderView(isVisible: true)
        self.model.postConsultaTitularidad(codZonas: codZonas, areaRegistral: areaRegistral, apPaterno: apPaterno, apMaterno: apMaterno, nombre: nombre, razonSocial: razonSocial, derecho: derecho, sociedad: sociedad, tipoTitular: tipoTitular, userId: userId, ipDispositivo: ipDispositivo, idUnico: idUnico, numOrden: numOrden, pan: pan, codAutoriza: codAutoriza, nomEmisor: nomEmisor, eticket: eticket, codtienda: codtienda, oriTarjeta: oriTarjeta, codAccion: codAccion, dscCodAccion: dscCodAccion, respuesta: respuesta, fechaYhoraTx: fechaYhoraTx, concepto: concepto, idUser: idUser, transId: transId, visanetResponse: visanetResponse, deviceId: deviceId, userCrea: userCrea) { (arrayTitularidad) in
            self.controller?.loadTitulos(titulos: arrayTitularidad)
        }
    }
    
    
    func postConsultaTitularidadAsync(codZonas: [String], areaRegistral: String, apPaterno: String, apMaterno: String, nombre: String, razonSocial: String, derecho: String, sociedad: String, tipoTitular: String, userId: String, ipDispositivo: String, idUnico: String, numOrden: String, pan: String, codAutoriza: String, nomEmisor: String, eticket: String, codtienda: String, oriTarjeta: String, codAccion: String, dscCodAccion: String, respuesta: String, fechaYhoraTx: String, concepto: String, idUser: String, transId: String, visanetResponse: PagoExitosoEntity, deviceId: String, userCrea: String)  {
        self.controller?.loaderView(isVisible: true)
        self.model.postConsultaTitularidadAsync(codZonas: codZonas, areaRegistral: areaRegistral, apPaterno: apPaterno, apMaterno: apMaterno, nombre: nombre, razonSocial: razonSocial, derecho: derecho, sociedad: sociedad, tipoTitular: tipoTitular, userId: userId, ipDispositivo: ipDispositivo, idUnico: idUnico, numOrden: numOrden, pan: pan, codAutoriza: codAutoriza, nomEmisor: nomEmisor, eticket: eticket, codtienda: codtienda, oriTarjeta: oriTarjeta, codAccion: codAccion, dscCodAccion: dscCodAccion, respuesta: respuesta, fechaYhoraTx: fechaYhoraTx, concepto: concepto, idUser: idUser, transId: transId, visanetResponse: visanetResponse, deviceId: deviceId, userCrea: userCrea) { (objTitularidad) in
            
            self.controller?.loadTitulosAsync(titulosAsync: objTitularidad)
            
        }
    }
    
    
    func getStatusConsultaTitularidadAsync(guid: String)  {
        // self.controller?.loaderView(isVisible: true)
        self.model.getStatusConsultaTitularidadAsync(guid: guid) { (objTitularidad) in
            
            self.controller?.loadStatusTitulosAsync(titulosAsync: objTitularidad)
            
        }
    }
    
    
}
