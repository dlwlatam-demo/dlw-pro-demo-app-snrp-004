//
//  ZonaCell.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/09/22.
//

import UIKit

class ZonaCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
    
    private var zonaItem: ZonaEntity!
    
    weak var delegate : ZonaCellDelegate?
        
    static let identifier = "ZonaCell"
    
    func setup(with item: ZonaEntity) {
        self.zonaItem = item
        self.nameLabel.text = item.nombre
        if (item.select) {
            self.checkImage.image = UIImage(named: "Checked")
        } else {
            self.checkImage.image = UIImage(named: "Check")
        }
        
        let onTapCell = UITapGestureRecognizer(target: self, action: #selector(onTapItemCell))
        self.cellView.addGestureRecognizer(onTapCell)
    }
    
    @objc func onTapItemCell(){
        if (zonaItem.select) {
            self.checkImage.image = UIImage(named: "Check")
        } else {
            self.checkImage.image = UIImage(named: "Checked")
        }
        self.zonaItem.select = !self.zonaItem.select
        self.delegate?.onTapSelect(self, item: self.zonaItem)
    }
}

protocol ZonaCellDelegate: AnyObject {
    
  func onTapSelect(_ zonaCell: ZonaCell, item: ZonaEntity)
    
}
