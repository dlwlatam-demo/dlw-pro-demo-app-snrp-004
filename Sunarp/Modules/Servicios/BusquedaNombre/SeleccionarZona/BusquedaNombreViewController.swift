//
//  BusquedaNombreViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/09/22.
//

import Foundation
import UIKit

class BusquedaNombreViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var zonasTable: UITableView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var searchView: UIView!
        
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var loading: UIAlertController!
    
    private lazy var presenter: BusquedaNombrePresenter = {
       return BusquedaNombrePresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Búsqueda por Nombre")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.searchView.primaryButton()
        self.countView.borderCard()
        
        self.zonasTable.register(UINib(nibName: ZonaCell.identifier, bundle: App.bundle), forCellReuseIdentifier: ZonaCell.identifier)
        self.zonasTable.delegate = self
        self.zonasTable.dataSource = self
    }
    
    private func setupGestures() {
        let onTapClearSelectView = UITapGestureRecognizer(target: self, action: #selector(onTapClearSelect))
        self.closeView.addGestureRecognizer(onTapClearSelectView)
        
        let onTapSearchView = UITapGestureRecognizer(target: self, action: #selector(onTapSearch))
        self.searchView.addGestureRecognizer(onTapSearchView)
    }
    
    @objc func onTapClearSelect(){
        let count = self.zonas.count
        let selectTotal = 0
        if (count > 0) {
            for i in 0...count - 1 {
                self.zonas[i].select = false
            }
        }
        self.countLabel.text = String(selectTotal)
        self.countView.isHidden = true
        self.zonasTable.reloadData()
    }
    
    @objc func onTapSearch(){
        let storyboard = UIStoryboard(name: "BusquedaNombre", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PagarBusquedaViewController") as! PagarBusquedaViewController
        vc.setZonaList(zonaList: self.zonas)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadZonas(zonaResponse: ZonaResponse) {
        self.zonas = zonaResponse.zonas
        self.zonasTable.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
}

extension BusquedaNombreViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.zonas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = zonas[indexPath.row]
        let cell = self.zonasTable.dequeueReusableCell(withIdentifier: ZonaCell.identifier, for: indexPath) as! ZonaCell
        cell.setup(with: item)
        cell.delegate = self
        return cell
    }
}

extension BusquedaNombreViewController: ZonaCellDelegate {
    
    func onTapSelect(_ zonaCell: ZonaCell, item: ZonaEntity) {
        let count = self.zonas.count
        var selectTotal = 0
        if (count > 0) {
            for i in 0...count - 1 {
                print("item.regPubId::>",item.regPubId)
                print("item.zonas::>",self.zonas[i].regPubId )
                print("item.select::>",item.select)
                if (self.zonas[i].regPubId == item.regPubId) {
                    self.zonas[i].select = item.select
                }
                let select = self.zonas[i].select
                if (select) {
                    selectTotal += 1
                }
            }
        }
        self.countLabel.text = String(selectTotal)
        if (selectTotal > 0) {
            self.countView.isHidden = false
        } else {
            self.countView.isHidden = true
        }
    }
    
}
