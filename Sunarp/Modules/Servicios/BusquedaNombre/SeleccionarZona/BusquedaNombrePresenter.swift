//
//  BusquedaNombrePresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class BusquedaNombrePresenter {
    
    private weak var controller: BusquedaNombreViewController?
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    init(controller: BusquedaNombreViewController) {
        self.controller = controller
    }
    
}

extension BusquedaNombrePresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getZonas{ (arrayZonas) in
            self.controller?.loadZonas(zonaResponse: arrayZonas)
        }
    }
        
}
