//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class ConsultaSolicitudPublicaViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var anioTextField: UITextField!
    @IBOutlet weak var numberDocumentTextField: UITextField!

    
    @IBOutlet weak var formSecondView: UIView!
    @IBOutlet weak var toConfirmSolView: UIView!
    @IBOutlet weak var numberDocumentSolicitudTextField: UITextField!
    
    var listaPropiedadEntities: [ConsultaPropiedadListEntity] = []
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var numDocumentSol: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var maxLength = 0
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var tipoDocumentoPickerView = UIPickerView()
    var generoPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    @IBOutlet weak var formBackView: UIView!
    @IBOutlet weak var secondApeTextField: UITextField!
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var toConfirmBusquedaView: UIView!
    
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    
    
    private lazy var presenter: ConsultaSolicitudPublicaPresenter = {
       return ConsultaSolicitudPublicaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       // self.presenter.didLoad()
      //  presenter.didLoad()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Estado de Solicitud de Publicidad")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        let tapConfirmGestureSol = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewDoc))
        self.toConfirmSolView.addGestureRecognizer(tapConfirmGestureSol)
        
        
        
              
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
    }
    @objc private func onTapToConfirmViewAnio() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        
        
        toConfirmView.isHidden = false
        anioTextField.isHidden = false
        numberDocumentTextField.isHidden = false
        formSecondView.isHidden = true
    }
  
    @objc private func onTapToConfirmViewNum() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
       // self.presenter.validateData()
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        
        toConfirmView.isHidden = true
        anioTextField.isHidden = true
        numberDocumentTextField.isHidden = true
        formSecondView.isHidden = false
    }
  
    private func setupDesigner() {
       
        formView.backgroundCard()
        anioTextField.border()
        anioTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        numberDocumentTextField.border()
        numberDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        numberDocumentSolicitudTextField.border()
        numberDocumentSolicitudTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
       
     
        toConfirmView.primaryButton()
        toConfirmSolView.primaryButton()
        
        
        
        toConfirmView.isHidden = true
        anioTextField.isHidden = true
        numberDocumentTextField.isHidden = true
      
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numberDocumentTextField.resignFirstResponder()
    }
    
    @objc private func onTapToConfirmViewDoc() {
        self.numDocumentSol = self.numberDocumentSolicitudTextField.text ?? ""
        self.presenter.validateDataDoc()
    }
    
    @objc private func onTapToConfirmView() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.anioTextField.text ?? ""
        self.presenter.validateData()
    }
  
    func goToConfirmDocAnio(_ state: Bool, message: String) {
      //  loaderView(isVisible: false)
          
        if (state) {
            if (state) {
                validateTypeDocumentSolAnio()
                
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    func goToConfirmDoc(_ state: Bool, message: String) {
      //  loaderView(isVisible: false)
          
        if (state) {
            if (state) {
                validateTypeDocumentSol()
                
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
    func validateTypeDocumentSol() {
        self.numDocumentSol = self.numberDocumentSolicitudTextField.text ?? ""
      //  if (self.typeDocument == "DNI") {
            if (!numDocumentSol.isEmpty) {
                self.presenter.didLoadSolicitud()
            }
       // }
    }
    func validateTypeDocumentSolAnio() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.anioTextField.text ?? ""
      //  if (self.typeDocument == "DNI") {
            if (!numDocument.isEmpty && !dateOfIssue.isEmpty) {
                self.presenter.didLoadSolicitudAnio()
            }
       // }
    }
    
    
    func loadDataSolicitud(solicitud: HistoryDetailEntity) {
        isValidDocument = true
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EstadoSolicitudViewController") as! EstadoSolicitudViewController
        vc.isValidDocument = isValidDocument
        vc.solicitudDetail = solicitud
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
        
 
}
