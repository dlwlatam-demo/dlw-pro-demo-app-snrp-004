//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class ConsultaSolicitudPublicaPresenter {
    
    private weak var controller: ConsultaSolicitudPublicaViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var modelProfil: ProfileModel = {
        let navigation = controller?.navigationController
       return ProfileModel(navigationController: navigation!)
    }()
    init(controller: ConsultaSolicitudPublicaViewController) {
        self.controller = controller
    }
    
}

extension ConsultaSolicitudPublicaPresenter: GenericPresenter {
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
      //  self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
       //     self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
       // }
        
        self.modelProfil.getListaTipoDocumentos{ (arrayTipoDocumentos) in
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.controller?.loaderView(isVisible: false)
            self.modelProfil.postLogin(appId: Constant.APP_ID, deviceToken: "", deviceType: "", status: "A", deviceId: "1", appVersion: appVersion, ipAddress: UtilHelper.getIPAddress(), deviceLogged: "1", receiveNotifications: "N", deviceOs: Constant.DEVICE_OS) { (profileEntity) in
              //  self.controller?.loadDataPerfil(profileEntity: profileEntity, arrayTipoDocumentos: arrayTipoDocumentos)
            }
        }
    }
    
    
       func didLoadSolicitud() {
           let numDocument: String = self.controller?.numDocumentSol ?? ""
           self.model.getHistorialDetalle(id: numDocument) { (objHistory) in
               self.controller?.loadDataSolicitud(solicitud: objHistory)
           }
       }
    
       func didLoadSolicitudAnio() {
           let id: String = self.controller?.numDocument ?? ""
           let anio: String = self.controller?.dateOfIssue ?? ""
           self.model.getHistorialDetalleAnio(anio: anio,id: id) { (objHistory) in
               self.controller?.loadDataSolicitud(solicitud: objHistory)
           }
       }
   
    
    
   
    
    func validateData() {
       
        let numDocument = self.controller?.numDocument ?? ""
        let dateOfIssue = self.controller?.dateOfIssue ?? ""
       print("numDocument::>>",numDocument)
        print("dateOfIssue::>>",dateOfIssue)
      
        var isValidDocument = self.controller?.isValidDocument ?? false
        
      
        if (numDocument.isEmpty || dateOfIssue.isEmpty) {
            self.controller?.goToConfirmDocAnio(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmDocAnio(true, message: "")
        }
        
    }
    
    
    func validateDataDoc() {
       
        let numDocument = self.controller?.numDocumentSol ?? ""
       print("numDocument::>>",numDocument)
      
        if (numDocument.isEmpty) {
            self.controller?.goToConfirmDoc(false, message: "Los campos no pueden estar vacio.")
        }  else {
            self.controller?.goToConfirmDoc(true, message: "")
        }
        
    }
    
}
