//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class ContactoViewPresenter {
    
    private weak var controller: ContactoViewController?
    
    lazy private var model: CommentModel = {
        let navigation = controller?.navigationController
       return CommentModel(navigationController: navigation!)
    }()
    
    init(controller: ContactoViewController) {
        self.controller = controller
    }
    
}

extension ContactoViewPresenter: GenericPresenter {
        
    func sendComment() {
        self.controller?.loaderView(isVisible: true)
        let txt = controller?.txt ?? ""
        let img = controller?.img ?? ""
        let appName = UserPreferencesController.getGuid()
        
        self.model.postComment(txt: txt, img: img, appName: appName) { (jsonValidacionComment) in
            let state = jsonValidacionComment.codResult == "1"
            self.controller?.loadCommemnt(state, message: jsonValidacionComment.msgResult, jsonValidacionComment: jsonValidacionComment)
        }
    }

  
    
}
