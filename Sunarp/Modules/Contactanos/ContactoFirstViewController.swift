//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class ContactoFirstViewController: UIViewController {
        
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet weak var formViewContacto: UIView!
    @IBOutlet weak var formViewContactOption: UIView!
    @IBOutlet weak var formViewConsulta: UIView!
    @IBOutlet weak var formViewVisit: UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    
    @IBOutlet weak var toFaceView: UIView!
    @IBOutlet weak var toYoutubeView: UIView!
    @IBOutlet weak var toTwitterView: UIView!
    @IBOutlet weak var toLinkedingView: UIView!
    @IBOutlet weak var toInstagramView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Contáctenos")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        
        self.formView.backgroundCard()
        self.formViewContacto.backgroundCard()
        self.formViewContactOption.backgroundCard()
        self.formViewConsulta.backgroundCard()
        self.formViewVisit.backgroundCard()
        
        
        
      //  self.formViewEditar.backgroundCard()
       
    }
    
    private func addGestureView(){
        
     
        let tapadContactGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToContacView))
        self.formViewContacto.addGestureRecognizer(tapadContactGesture)
        
        let tapadContacOptGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToContactOptionView))
        self.formViewContactOption.addGestureRecognizer(tapadContacOptGesture)
        
        
        let tapConConsulGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConsultafirmView))
        self.formViewConsulta.addGestureRecognizer(tapConConsulGesture)
        
        
        let tapVisitLinkGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.formViewVisit.addGestureRecognizer(tapVisitLinkGesture)
        
        
        let tapVisitLinkGestureFace = UITapGestureRecognizer(target: self, action: #selector(self.onTapToFaceView))
        self.toFaceView.addGestureRecognizer(tapVisitLinkGestureFace)

        let tapVisitYouGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToYouView))
        self.toYoutubeView.addGestureRecognizer(tapVisitYouGesture)
        
        let tapVisitTwittterGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToTwitterView))
        self.toTwitterView.addGestureRecognizer(tapVisitTwittterGesture)
        
        let tapVisitLinkedingGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToLinkedingView))
        self.toLinkedingView.addGestureRecognizer(tapVisitLinkedingGesture)
        
        let tapVisitInstagramGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToInstagramView))
        self.toInstagramView.addGestureRecognizer(tapVisitInstagramGesture)
        
        
    }
    @objc private func onTapToFaceView() {

        guard let url = URL(string: "https://www.facebook.com/SunarpOficial") else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @objc private func onTapToYouView() {
        guard let url = URL(string: "https://www.youtube.com/user/SunarpOficial") else {
             return
        }
        if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @objc private func onTapToTwitterView() {
        guard let url = URL(string: "https://twitter.com/sunarpoficial") else {
             return
        }
        if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @objc private func onTapToLinkedingView() {
        guard let url = URL(string: "https://www.linkedin.com/company/sunarp") else {
             return
        }
        if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @objc private func onTapToInstagramView() {
        guard let url = URL(string: "https://www.instagram.com/sunarpoficial") else {
             return
        }
        guard let url1 = URL(string: "instagram://user?username=sunarpoficial") else {
             return
        }

        if UIApplication.shared.canOpenURL(url1) {
            UIApplication.shared.open(url1)
         } else if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }
    //dialNumber(number: "+921111111222")
    @objc private func onTapToConsultafirmView() {
      
        
        let storyboard = UIStoryboard(name: "Contacto", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContactoViewController") as! ContactoViewController
        self.navigationController?.pushViewController(vc, animated: true)
         
        //dialogMessage()
       
    }
    @objc private func onTapToConfirmView() {
        
        guard let url = URL(string: "https://www.sunarp.gob.pe/") else {
             return
        }
        if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
        
    @objc private func onTapToContacView() {
      
        
       dialNumber(number: "+012083100")
    }
    
    @objc private func onTapToContactOptionView() {
        
       dialNumber(number: "+080027164")
    }
    
    
    
  

}


