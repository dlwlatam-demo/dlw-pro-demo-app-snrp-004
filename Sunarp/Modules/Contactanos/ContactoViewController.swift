//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import MobileCoreServices

class ContactoViewController: UIViewController, UITextViewDelegate  {
        
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var addPhotoView: UIView!
    @IBOutlet weak var addAdjuntoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var previewImage: UIImageView!
    
    @IBOutlet weak var formViewEditar: UIView!
    @IBOutlet weak var formViewHist: UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var imageSend: UIImageView!
    
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var iconPhotoView: UIView!
    @IBOutlet weak var labelCount: UILabel!
    
    var loading: UIAlertController!
    
    
    private lazy var presenter: ContactoViewPresenter = {
       return ContactoViewPresenter(controller: self)
    }()
    
    var txt: String = ""
    var img: String = ""
    var appName: String = ""
    
    var flagAdjunto: Bool = false
    var flagPhoto: Bool = false
    var tipoMensaje: Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        self.iconView.isHidden = true
        self.iconView.layer.cornerRadius = 5
        
        self.iconPhotoView.isHidden = true
        self.iconPhotoView.layer.cornerRadius = 5
        
        self.txtView.delegate = self
        
    }
    
    
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Enviar comentario")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        print("goBack")
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        
        self.formView.backgroundCard()
        self.noteView.backgroundTextFiel()
        
        
      //  self.formViewEditar.backgroundCard()
       
    }
    
    private func addGestureView(){
        
     
        let tapadjuntarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToAdjunView))
        self.addAdjuntoView.addGestureRecognizer(tapadjuntarGesture)
        
        let tapadPhotGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToPhotoView))
        self.addPhotoView.addGestureRecognizer(tapadPhotGesture)
        
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        self.txt = self.txtView.text
        
    }
    
    
    @objc private func onTapToConfirmView() {
      
        self.txt = self.txtView.text ?? ""
        txtView.text = subString( texto: self.txt, limit: 500)
        self.txt = self.txtView.text
        self.labelCount.text = "\(String(txtView.text.count))/500"
     
        if (!txt.isEmpty) {
            self.presenter.sendComment()
        }else{
            tipoMensaje = 0
            self.confirmPopupDialog(title: "Alerta del Sistema",message: "Ingrese mensaje a enviar",primaryButton: "",secondaryButton: "",addColor: SunarpColors.red,delegate: self)
        }
       
    }
    func dialogMessage(){
        tipoMensaje = 1
        self.confirmPopupDialog(title: "Envío satisfactorio",message: "Tu comentario ha sido enviado",primaryButton: "",secondaryButton: "",addColor: SunarpColors.greenLight,delegate: self)
        
    }
    func dialogMessageError(){
        tipoMensaje = 0
        self.confirmPopupDialog(title: "Envío fallido",message: "Error al momento del envío de tu comentario, inténtalo nuevamente",primaryButton: "",secondaryButton: "",addColor: SunarpColors.red,delegate: self)
    }
    
    @objc private func onTapToPhotoView() {
       
        let myPickerControllerCamera = UIImagePickerController()
            myPickerControllerCamera.delegate = self
            myPickerControllerCamera.sourceType = UIImagePickerController.SourceType.camera
            myPickerControllerCamera.allowsEditing = true
            self.present(myPickerControllerCamera, animated: true, completion: nil)
        
        flagAdjunto = false
        flagPhoto = true
    }
    
    func subString(texto: String, limit: Int) -> String {
        if(texto.count >= limit){
            let start = texto.index(texto.startIndex, offsetBy: 0)
            let end = texto.index(texto.startIndex, offsetBy: limit - 2)
            let range = start...end

            let firstN = String(texto[range])
            return firstN
        }
        return texto
    }
    
    func textViewDidChange(_ textView: UITextView) {
        var mensaje = txtView.text!
        
        if(mensaje.count >= 500){
            let start = mensaje.index(mensaje.startIndex, offsetBy: 0)
            let end = mensaje.index(mensaje.startIndex, offsetBy: 499)
            let range = start...end

            let firstN = String(mensaje[range])
            mensaje = firstN
        }
        
        txtView.text = subString( texto: mensaje, limit: 500)
        
        self.labelCount.text = "\(String(txtView.text.count))/500"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // Comprueba si se presionó la tecla "Return"
            if text == "\n" {
                // Oculta el teclado
                textView.resignFirstResponder()
                return false
            }

            // Resto del código para limitar la longitud del texto a 500 caracteres
            guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
                return false
            }
            let substringToReplace = textView.text[rangeOfTextToReplace]
            let count = textView.text.count - substringToReplace.count + text.count
            self.labelCount.text = "\(String(count))/500"
            
            // Resto del código para limitar la longitud del texto a 500 caracteres

            return count <= 500
    }
    
    @objc private func onTapToAdjunView() {
      
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
            
            flagAdjunto = true
            flagPhoto = false
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func validateCooment() {
        self.txt = self.txtView.text ?? ""
        txtView.text = subString( texto: self.txt, limit: 500)
        
        self.labelCount.text = "\(String(txtView.text.count))/500"
        self.txt = self.txtView.text
        
            if (!txt.isEmpty && !img.isEmpty) {
                self.presenter.sendComment()
            }
        
    }
    
    func loadCommemnt(_ state: Bool, message: String, jsonValidacionComment: ComentarioEntity) {
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if (state) {
                //self.isValidDocument = true
                self.dialogMessage()
                self.txtView.text = ""
            } else {
               // self.isValidDocument = false
                self.dialogMessageError()
            }
        }
    }
    
    

}

extension UIImage {
    func scaleImage(toSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
            return newImage
        }
        return self
    }
}


extension ContactoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           print(info)
           let data = convertFromUIimageTODict(info)
           
           if let editingImage = data[convertInfoKey((UIImagePickerController.InfoKey.editedImage))] as? UIImage {
               print(editingImage)
               
               // Escalar la imagen a 100x80
               let scaledImage = editingImage.scaleImage(toSize: CGSize(width: 100, height: 80))
               
               // Mostrar la imagen en la vista de previsualización
               previewImage.image = scaledImage
               
               // Convertir la imagen a base64
               let imageData: Data = scaledImage.pngData()!
               let base64String = imageData.base64EncodedString()
               print(base64String)
               
               // Guardar el base64 en la variable img
               self.img = base64String
           }
           
           if flagPhoto == true {
               self.iconView.isHidden = true
               self.iconPhotoView.isHidden = false
           }
           if flagAdjunto == true {
               self.iconView.isHidden = false
               self.iconPhotoView.isHidden = true
           }
           picker.dismiss(animated: true, completion: nil)
       }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
  
    func convertFromUIimageTODict( _ input :[UIImagePickerController.InfoKey : Any]) -> [String:Any]{
        
        return Dictionary(uniqueKeysWithValues: input.map({key, value in (key.rawValue, value)}))
    }
    func convertInfoKey(_ input : UIImagePickerController.InfoKey) -> String{
        return input.rawValue
    }

    
}

extension ContactoViewController: SendOkDialogDelegate{
    func primary(action:String) {
        print("action = ", action)
        if action == "CERRAR " {
            
        } else if action == "REINTENTAR" {
            
        }
    }
    
    func secondary() {
        if tipoMensaje == 1 {
            self.goBack()
        }
    }
}
