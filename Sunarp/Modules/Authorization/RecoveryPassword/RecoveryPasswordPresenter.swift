//
//  RecoveryPasswordPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class RecoveryPasswordPresenter {
    
    private weak var controller: RecoveryPasswordViewController?
    
    lazy private var model: RecoveryModel = {
        let navigation = controller?.navigationController
       return RecoveryModel(navigationController: navigation!)
    }()
    
    init(controller: RecoveryPasswordViewController) {
        self.controller = controller
    }
    
    func codeRequest() {
        
        guard let email = self.controller?.email else { return }
        
        if (email.isEmpty) {
            self.controller?.goToCodeValidationRegister(false, message: "El campo no puede estar vacio.")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToCodeValidationRegister(false, message: "Ingrese un email válido.")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.postSolicitudCodigo(email) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationRegister(state, message: objSolicitud.msgResult)
            }
        }
    }
    
}
