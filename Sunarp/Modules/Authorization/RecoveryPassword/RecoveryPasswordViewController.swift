//
//  RecoveryPasswordViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 23/07/22.
//

import UIKit

class RecoveryPasswordViewController: UIViewController {

    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    var email: String = ""
    var loading: UIAlertController!
    
    private lazy var presenter: RecoveryPasswordPresenter = {
       return RecoveryPasswordPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
        
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()        
        formView.backgroundCard()
        
        emailTextField.borderAndPadding()
        
        sendView.primaryButton()
    }
    
    private func addGestureView(){        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapSendGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSendView))
        self.sendView.addGestureRecognizer(tapSendGesture)
    }
    
    func goToCodeValidationRegister(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CodeValidationViewController") as! CodeValidationViewController
            vc.email = self.email
            self.navigationController?.pushViewController(vc, animated: true)
            
            /*
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                  //
                }
            }))
            self.present(alert, animated: true)
             */
        }
    }
        
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.emailTextField.resignFirstResponder()
    }
    
    @objc private func onTapSendView() {
        self.email = self.emailTextField.text ?? ""
        self.presenter.codeRequest()
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
}
