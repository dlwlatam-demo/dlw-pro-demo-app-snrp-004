//
//  CodeValidationRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class CodeValidationRegisterPresenter {
    
    private weak var controller: CodeValidationRegisterViewController?
    
    lazy private var model: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    init(controller: CodeValidationRegisterViewController) {
        self.controller = controller
    }
    
    func validationCodeRequest() {
        guard let codeOtp = self.controller?.codeOtp else { return }
        
        if (codeOtp.isEmpty) {
            self.controller?.goToDataRegister(false, message: "El campo no puede estar vacío.")
        } else {
            //self.controller?.loaderView(isVisible: true)
            let guid = UserPreferencesController.getGuid()
            self.model.postValidacionCodigo(codeOtp: codeOtp, guid: guid) { (objSolicitud) in
                //self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                    self.controller?.goToDataRegister(state, message: objSolicitud.msgResult)
                } else {
                    self.controller?.goToDataRegister(false, message: "Código Erróneo - Volver a intentar.")
                }
                
            }
        }
    }
    
    func resendCodeRequest() {
        guard let numcelular = self.controller?.numcelular else { return }
        
        if (!numcelular.isEmpty) {
            self.controller?.loaderView(isVisible: true)
            self.model.postSolicitudCodigo(numcelular) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.showMessageFromResendCode(state, message: objSolicitud.msgResult)
            }
        }
    }
    
}
