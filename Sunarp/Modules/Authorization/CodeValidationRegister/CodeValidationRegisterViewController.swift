//
//  CodeValidationRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 29/07/22.
//

import UIKit

class CodeValidationRegisterViewController: UIViewController {

    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var toBackView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var numberOneTextField: UITextField!
    @IBOutlet weak var numberTwoTextField: UITextField!
    @IBOutlet weak var numberThreeTextField: UITextField!
    @IBOutlet weak var numberFourTextField: UITextField!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendView: UIView!
    
    var timer: Timer?
    var timeLeft = 5 * 60
    var isEnableResendLabel = false
    var codeOtp: String = ""
    var numcelular: String = ""
    var loading: UIAlertController!
    
    private lazy var presenter: CodeValidationRegisterPresenter = {
       return CodeValidationRegisterPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupTimer()
        setupKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        let tapBackGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToBackView))
        self.toBackView.addGestureRecognizer(tapBackGesture)
        
        let tapResendGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapResendCodeValidation))
        self.resendLabel.addGestureRecognizer(tapResendGesture)
        self.resendView.addGestureRecognizer(tapResendGesture)
    }
    
    private func setupTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    
    private func setupKeyboard() {
        /*NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIResponder.keyboardWillHideNotification, object: nil)*/
    }
    
    @objc func onTimerFires() {
        self.timeLeft -= 1
        self.isEnableResendLabel = false
        if (self.timeLeft < 10) {
            self.countLabel.text = "00:0\(timeLeft)"
        } else if timeLeft < 60 {
            self.countLabel.text = "00:\(timeLeft)"
        } else {
            let minute = 60
            let minutesLeft = timeLeft / minute
            let secondsLeft = timeLeft % minute
            if secondsLeft < 10 {
                self.countLabel.text = "0\(minutesLeft):0\(secondsLeft)"
            } else {
                self.countLabel.text = "0\(minutesLeft):\(secondsLeft)"
            }
        }
        if (self.timeLeft <= 0) {
            self.isEnableResendLabel = true
            self.timer?.invalidate()
            self.timer = nil
        }
        self.changeColorResendLabel()
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numberOneTextField.resignFirstResponder()
    }
    
    /*@objc func keyboardWillShowNotification(notification: Notification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: self.scrollview.contentSize.height + height)
        }
    }
    
    @objc func keyboardWillHideNotification(notification: Notification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: self.scrollview.contentSize.height - height)
        }
    }*/
        
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        
        numberOneTextField.border()
        numberTwoTextField.border()
        numberThreeTextField.border()
        numberFourTextField.border()
        
        toConfirmView.primaryButton()
        toBackView.secondaryButton()
        
        self.numberOneTextField.delegate = self
        self.numberTwoTextField.delegate = self
        self.numberThreeTextField.delegate = self
        self.numberFourTextField.delegate = self
        
        self.numberOneTextField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        self.numberTwoTextField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        self.numberThreeTextField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        self.numberFourTextField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
    }
    
    private func changeColorResendLabel() {
        if self.isEnableResendLabel {
            self.resendLabel.textColor = UtilHelper.getUIColor(hex: Constant.labelLinkActiveColor)
        } else {
            self.resendLabel.textColor = UtilHelper.getUIColor(hex: Constant.labelLinkInactiveColor)
        }
    }
    
    @objc private func onTapToConfirmView() {
        self.presenter.validationCodeRequest()
    }
    
    @objc private func onTapToBackView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func changeCharacter(textField: UITextField) {
        if textField.text?.utf8.count == 1 {
            switch textField {
            case self.numberOneTextField:
                self.numberTwoTextField.becomeFirstResponder()
            case self.numberTwoTextField:
                self.numberThreeTextField.becomeFirstResponder()
            case self.numberThreeTextField:
                self.numberFourTextField.becomeFirstResponder()
            case self.numberFourTextField:
                let numberOne: String = self.numberOneTextField.text ?? ""
                let numberTwo: String = self.numberTwoTextField.text ?? ""
                let numberThree: String = self.numberThreeTextField.text ?? ""
                let numberFour: String = self.numberFourTextField.text ?? ""
                self.codeOtp = "\(numberOne)\(numberTwo)\(numberThree)\(numberFour)"
                print("OTP - \(codeOtp)")
            default:
                break
            }
        } else if textField.text!.isEmpty {
            self.codeOtp = ""
            switch textField {
            case self.numberFourTextField:
                self.numberThreeTextField.becomeFirstResponder()
            case self.numberThreeTextField:
                self.numberTwoTextField.becomeFirstResponder()
            case self.numberTwoTextField:
                self.numberOneTextField.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    @objc private func onTapResendCodeValidation() {
        if self.isEnableResendLabel{
            self.presenter.resendCodeRequest()
        }
    }
    
    func goToDataRegister(_ state: Bool, message: String) {
        if state {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataRegisterViewController") as! DataRegisterViewController
                vc.numcelular = self.numcelular
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func showMessageFromResendCode(_ state: Bool, message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if state {
                self.timeLeft = 5 * 60
                self.setupTimer()
            }
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
}


extension CodeValidationRegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.utf16.count == 1 && !string.isEmpty {
            return false
        } else {
            return true
        }
    }
    
}
