//
//  PhoneRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class PhoneRegisterPresenter {
    
    private weak var controller: PhoneRegisterViewController?
    
    lazy private var model: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    init(controller: PhoneRegisterViewController) {
        self.controller = controller
    }
    
    func codeRequest() {
        guard let numcelular = self.controller?.numcelular else { return }
        
        if (numcelular.isEmpty) {
            self.controller?.goToCodeValidationRegister(false, message: "El campo no puede estar vacio.")
        } else if (numcelular.count < 9 || !numcelular.hasPrefix("9")) {
            self.controller?.goToCodeValidationRegister(false, message: "Ingrese un número válido.")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.postSolicitudCodigo(numcelular) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationRegister(state, message: objSolicitud.msgResult)
            }
        }
    }
    
}
