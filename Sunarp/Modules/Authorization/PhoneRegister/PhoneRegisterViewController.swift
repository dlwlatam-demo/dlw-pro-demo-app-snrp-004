//
//  PhoneRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 29/07/22.
//

import UIKit

class PhoneRegisterViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var toBackView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var numcelular: String = ""
    var loading: UIAlertController!
    
    private lazy var presenter: PhoneRegisterPresenter = {
       return PhoneRegisterPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        phoneTextField.setPadding(left: CGFloat(16))
        phoneView.borderView()
        toConfirmView.primaryButton()
        toBackView.secondaryButton()
        
        self.phoneTextField.delegate = self
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        let tapBackGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToBackView))
        self.toBackView.addGestureRecognizer(tapBackGesture)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.phoneTextField.resignFirstResponder()
    }
    
    @objc private func onTapToConfirmView() {
        self.numcelular = self.phoneTextField.text ?? ""
        self.presenter.codeRequest()
    }
    
    @objc private func onTapToBackView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func goToCodeValidationRegister(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CodeValidationRegisterViewController") as! CodeValidationRegisterViewController
            vc.numcelular = self.numcelular
            self.navigationController?.pushViewController(vc, animated: true)
            /*
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                    //
                }
            }))
            self.present(alert, animated: true)
             */
        }
    }
        
}

extension PhoneRegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 9
    }
    
}
