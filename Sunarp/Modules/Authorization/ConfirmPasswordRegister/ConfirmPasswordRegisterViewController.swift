
//
//  ConfirmPasswordRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class ConfirmPasswordRegisterViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var hideOrShowImage: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var hideOrShowRepeatImage: UIImageView!
    @IBOutlet weak var passwordRepeatTextField: UITextField!
    
    var maxLength = 0
    
    var loading: UIAlertController!
    var isHidden: Bool = true
    var isHiddenRepeat: Bool = true
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var contrasena: String = ""
    var contrasenaRepeat: String = ""
    
    private lazy var presenter: ConfirmPasswordRegisterPresenter = {
       return ConfirmPasswordRegisterPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        passwordTextField.borderAndPaddingLeftAndRight()
        passwordRepeatTextField.borderAndPaddingLeftAndRight()
        toConfirmView.primaryButton()
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        let tapHideOrShow = UITapGestureRecognizer(target: self, action: #selector(self.onTapHideOrShowPassword))
        self.hideOrShowImage.addGestureRecognizer(tapHideOrShow)
        
        let tapHideOrShowRepeat = UITapGestureRecognizer(target: self, action: #selector(self.onTapHideOrShowPasswordRepeat))
        self.hideOrShowRepeatImage.addGestureRecognizer(tapHideOrShowRepeat)
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.passwordTextField.resignFirstResponder()
        self.passwordRepeatTextField.resignFirstResponder()
    }
    
    @objc private func onTapToConfirmView() {
        
        if  isValidPassword(passwordTextField.text ?? ""){
            print("Entro")
            
            self.contrasena = self.passwordTextField.text ?? ""
            self.contrasenaRepeat = self.passwordRepeatTextField.text ?? ""
            if contrasena != contrasenaRepeat {
                showMessageAlert(message: "Las contraseñas no coinciden")
            }
            self.presenter.registrarUsuario()
        }else{
            print("No Entro")
            showMessageAlert(message: "Verifique su contraseña, debe tener mínimo 8 caracteres y contener por lo menos 1 mayúscula y 1 número")
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    @objc private func onTapHideOrShowPassword() {
        if (isHidden) {
            let text = self.passwordTextField.text
            self.passwordTextField.isSecureTextEntry = false
            self.hideOrShowImage.image = UIImage(systemName: "eye")
            self.passwordTextField.text = ""
            self.passwordTextField.text = text
        } else {
            self.passwordTextField.isSecureTextEntry = true
            self.hideOrShowImage.image = UIImage(systemName: "eye.slash")
        }
        self.isHidden = !self.isHidden
    }
    
    @objc private func onTapHideOrShowPasswordRepeat() {
        if (isHiddenRepeat) {
            let text = self.passwordRepeatTextField.text
            self.passwordRepeatTextField.isSecureTextEntry = false
            self.hideOrShowRepeatImage.image = UIImage(systemName: "eye")
            self.passwordRepeatTextField.text = ""
            self.passwordRepeatTextField.text = text
        } else {
            self.passwordRepeatTextField.isSecureTextEntry = true
            self.hideOrShowRepeatImage.image = UIImage(systemName: "eye.slash")
        }
        self.isHiddenRepeat = !self.isHiddenRepeat
    }
    
    func goToLogin(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let nav = self.navigationController
            
            DispatchQueue.main.async {
                nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
                nav?.pushViewController(vc, animated: false)
            }
            
            /*
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                    
                }
            }))
            self.present(alert, animated: true)
            */
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func isValidPassword(_ password:String) -> Bool {
       if(password.count > 7 && password.count < 17) {
       } else {
           return false
       }
       let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
       let letters = password.components(separatedBy: nonUpperCase)
       let strUpper: String = letters.joined()

       let smallLetterRegEx  = ".*[a-z]+.*"
       let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
       let smallresult = samlltest.evaluate(with: password)

       let numberRegEx  = ".*[0-9]+.*"
       let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
       let numberresult = numbertest.evaluate(with: password)

       let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
       var isSpecial :Bool = false
       if regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, password.count)) != nil {
        print("could not handle special characters")
           isSpecial = true
       }else{
           isSpecial = true
       }
       return (strUpper.count >= 1) && smallresult && numberresult && isSpecial
    }
    
}

extension ConfirmPasswordRegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == passwordTextField) {
            if (count == maxLength) {
                
                if  isValidPassword(passwordTextField.text ?? ""){
                    print("Entro")
                    return true
                }
                
                /*
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                   // self.validateTypeDocument()
                    return false
                }
                */
            } else {
               // self.numDocText.text = ""
                //self.nameText.text = ""
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
    /*
    func isValidPassword(_ password:String) -> Bool {
       if(password.count > 7 && password.count < 17) {
       } else {
           return false
       }
       let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
       let letters = password.components(separatedBy: nonUpperCase)
       let strUpper: String = letters.joined()

       let smallLetterRegEx  = ".*[a-z]+.*"
       let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
       let smallresult = samlltest.evaluate(with: password)

       let numberRegEx  = ".*[0-9]+.*"
       let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
       let numberresult = numbertest.evaluate(with: password)

       let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
       var isSpecial :Bool = false
       if regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, password.count)) != nil {
        print("could not handle special characters")
           isSpecial = true
       }else{
           isSpecial = false
       }
       return (strUpper.count >= 1) && smallresult && numberresult && isSpecial
    }
     */
}
