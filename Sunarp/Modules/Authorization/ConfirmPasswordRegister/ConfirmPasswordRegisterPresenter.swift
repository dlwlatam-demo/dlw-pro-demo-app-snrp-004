
//
//  ConfirmPasswordRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/08/22.
//

import Foundation

class ConfirmPasswordRegisterPresenter {
    
    private weak var controller: ConfirmPasswordRegisterViewController?
    
    lazy private var model: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    init(controller: ConfirmPasswordRegisterViewController) {
        self.controller = controller
    }
    
    func registrarUsuario() {
        
        guard let contrasena = self.controller?.contrasena else { return }
        guard let contrasenaRepeat = self.controller?.contrasenaRepeat else { return }
        
        if (contrasena.isEmpty || contrasenaRepeat.isEmpty) {
            self.controller?.goToLogin(false, message: "El campo no puede estar vacio.")
        } else if (contrasena != contrasenaRepeat) {
            self.controller?.goToLogin(false, message: "Las contraseñas no coinciden.")
        } else {
            self.controller?.loaderView(isVisible: true)
            let email = self.controller?.email ?? ""
            let sexo = self.controller?.gender ?? ""
            let tipoDoc = self.controller?.typeDocument ?? ""
            let nroDoc = self.controller?.numDocument ?? ""
            let nombres = self.controller?.names ?? ""
            let priApe = self.controller?.lastName ?? ""
            let segApe = self.controller?.middleName ?? ""
            let fecNac = self.controller?.dateOfIssue ?? ""
            let nroCelular = self.controller?.numcelular ?? ""
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.model.postRegistrarUsuario(email: email, sexo: sexo, tipoDoc: tipoDoc, nroDoc: nroDoc, nombres: nombres, priApe: priApe, segApe: segApe, fecNac: fecNac, nroCelular: nroCelular, password: contrasena, geoLat: 0, geoLong: 0, appId: Constant.APP_ID, guid: UserPreferencesController.getGuid(), appVersion: appVersion) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.clearGuid()
                }
                self.controller?.goToLogin(state, message: objSolicitud.msgResult)
            }
        }
    }
    
}

