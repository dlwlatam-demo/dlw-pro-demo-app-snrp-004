//
//  LoginPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/08/22.
//

import Foundation

class LoginPresenter {
    
    private weak var controller: LoginViewController?
    
    lazy private var model: LoginModel = {
        let navigation = controller?.navigationController
       return LoginModel(navigationController: navigation!)
    }()
    
    init(controller: LoginViewController) {
        self.controller = controller
    }
    
    func login() {
        
        guard let email = self.controller?.email else { return }
        guard let password = self.controller?.password else { return }
        
        if (email.isEmpty || password.isEmpty) {
            self.controller?.goToHome(false, message: "El campo no puede estar vacio.")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToHome(false, message: "Ingrese un email válido.")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.postLogin(userName: email, password: password) { (objLogin) in
                self.controller?.loaderView(isVisible: false)
                let state = !objLogin.accessToken.isEmpty
                if (objLogin.code == "") {
                    UserPreferencesController.setAccessToken(accessToken: objLogin.accessToken)
                    UserPreferencesController.setRefreshToken(refreshToken: objLogin.refreshToken)
                    UserPreferencesController.setTokenType(tokenType: objLogin.tokenType)
                    UserPreferencesController.setJti(jti: objLogin.jti)
                }
                else {
                    print("objLogin.code = ", objLogin.code)
                    UserPreferencesController.setAccessToken(accessToken: "")
                    UserPreferencesController.setRefreshToken(refreshToken: "")
                    UserPreferencesController.setTokenType(tokenType: "")
                    UserPreferencesController.setJti(jti: "")
                }
                self.controller?.goToHome(state, message: "Email o contraseña incorrectos")
            }
        }
    }
    
   
        
}
