//
//  LoginViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 23/07/22.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LoginViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hideOrShowImage: UIImageView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var recoveryPasswordView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var maxLength = 0
    
    var isHidden: Bool = true
    var email: String = ""
    var password: String = ""
    var loading: UIAlertController!
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var latValue = CLLocationDegrees()
    var longValue = CLLocationDegrees()
    
    private lazy var presenter: LoginPresenter = {
       return LoginPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        addMapa()
        self.maxLength = 9
    }
    
    private func addMapa(){
        let camera = GMSCameraPosition.camera(withLatitude: latValue, longitude: longValue, zoom: 12.0)
             //   let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        
        mapView.camera = camera
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
         // self.view.addSubview(mapView)

         // Creates a marker in the center of the map.
         //marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
         let marker = GMSMarker()
         var markerViewmarkerImage = UIImageView(image: SunarpImage.getImage(named: .vect_posision))
        
         marker.iconView = markerViewmarkerImage
         marker.position = CLLocationCoordinate2D(latitude: latValue, longitude: longValue)
         marker.title = "Lima"
         marker.snippet = "Perú"
        
         marker.map = mapView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func addGestureView(){
        let onTapRecovery = UITapGestureRecognizer(target: self, action: #selector(didAppearRecoveryPasswordView))
        recoveryPasswordView.addGestureRecognizer(onTapRecovery)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapHideOrShow = UITapGestureRecognizer(target: self, action: #selector(self.onTapHideOrShowPassword))
        self.hideOrShowImage.addGestureRecognizer(tapHideOrShow)
        
        let onTapLogin = UITapGestureRecognizer(target: self, action: #selector(didAppearHomeView))
        loginView.addGestureRecognizer(onTapLogin)
    }
    
    @objc private func didAppearRecoveryPasswordView() {
        let storyboard = UIStoryboard(name: "RecoveryPassword", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RecoveryPasswordViewController") as! RecoveryPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func didAppearHomeView() {
        self.email = self.emailTextField.text ?? ""
        self.password = self.passwordTextField.text ?? ""
        self.presenter.login()
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    @objc private func onTapHideOrShowPassword() {
        if (isHidden) {
            let text = self.passwordTextField.text
            self.passwordTextField.isSecureTextEntry = false
            self.hideOrShowImage.image = UIImage(systemName: "eye")
            self.passwordTextField.text = ""
            self.passwordTextField.text = text
        } else {
            self.passwordTextField.isSecureTextEntry = true
            self.hideOrShowImage.image = UIImage(systemName: "eye.slash")
        }
        self.isHidden = !self.isHidden
    }
    
    private func setupDesigner() {        
        headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        
        emailTextField.borderAndPadding()
        passwordTextField.borderAndPaddingLeftAndRight()
        
        loginView.primaryButton()
        
       // self.passwordTextField.delegate = self
    }
    
    func goToHome(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if (state) {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
}


extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == passwordTextField) {
            
           // isValidPassword(passwordTextField.text ?? "")
            
            if  isValidPassword(passwordTextField.text ?? ""){
                return false
            }
            if (count == maxLength) {
                /*
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                   // self.validateTypeDocument()
                    return false
                }
                */
            } else {
               // self.numDocText.text = ""
                //self.nameText.text = ""
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
    
    func isValidPassword(_ password:String) -> Bool {
       if(password.count > 7 && password.count < 17) {
       } else {
           return false
       }
       let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
       let letters = password.components(separatedBy: nonUpperCase)
       let strUpper: String = letters.joined()

       let smallLetterRegEx  = ".*[a-z]+.*"
       let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
       let smallresult = samlltest.evaluate(with: password)

       let numberRegEx  = ".*[0-9]+.*"
       let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
       let numberresult = numbertest.evaluate(with: password)

       let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
       var isSpecial :Bool = false
       if regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, password.count)) != nil {
        print("could not handle special characters")
           isSpecial = true
       }else{
           isSpecial = true
       }
       return (strUpper.count >= 1) && smallresult && numberresult && isSpecial
    }
    
    /*
    func  isValidPassword(password: String) -> Bool {
    var  contNumero = 0
    var  contLetraMay = 0
    var contLetraMin=0
        var i = 0
        for i in 0..<password.count {
         //for (int i = 0; i < password.length(); i++) {
            if (password.substring(i, i+1).matches("[A-Z]")) {
                contLetraMay = contLetraMay + 1;
            } else if (password.substring(i, i+1).matches("[a-z]")) {
                contLetraMin = contLetraMin + 1;
            } else if (password.substring(i, i+1).matches("[0-9]")) {
                contNumero = contNumero + 1;
            }
        }
        if((contLetraMay > 0 || contLetraMin > 0) && contNumero > 0 ){
            return true
        }else{
            return false
        }
    }*/
    
}
