//
//  ConfirmPasswordPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class ConfirmPasswordPresenter {
    
    private weak var controller: ConfirmPasswordViewController?
    
    lazy private var model: RecoveryModel = {
        let navigation = controller?.navigationController
       return RecoveryModel(navigationController: navigation!)
    }()
    
    init(controller: ConfirmPasswordViewController) {
        self.controller = controller
    }
    
    func actualizarContrasena() {
        
        guard let contrasena = self.controller?.contrasena else { return }
        guard let contrasenaRepeat = self.controller?.contrasenaRepeat else { return }
        
        if (contrasena.isEmpty || contrasenaRepeat.isEmpty) {
            self.controller?.goToLogin(false, message: "El campo no puede estar vacio.")
        } else if (contrasena != contrasenaRepeat) {
            self.controller?.goToLogin(false, message: "Las contraseñas no coinciden.")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.putActualizarContrasena(password: contrasena, guid: UserPreferencesController.getGuid()) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToLogin(state, message: objSolicitud.msgResult)
            }
        }
    }    
    
}
