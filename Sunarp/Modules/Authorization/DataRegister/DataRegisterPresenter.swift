//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class DataRegisterPresenter {
    
    private weak var controller: DataRegisterViewController?
    
    lazy private var model: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    init(controller: DataRegisterViewController) {
        self.controller = controller
    }
    
}

extension DataRegisterPresenter: GenericPresenter {
    
    func didLoad() {
        let guid = UserPreferencesController.getGuid()
        self.model.getListaTipoDocumentos(guid: guid) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func validarDni() {
        self.controller?.loaderView(isVisible: true)
        let dni = controller?.numDocument ?? ""
        let fecEmi = controller?.dateOfIssue ?? ""
        let guid = UserPreferencesController.getGuid()
        self.model.postValidarDni(dni: dni, fecEmi: fecEmi, guid: guid) { (jsonValidacionDni) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionDni.msgResult.isEmpty
            self.controller?.loadDatosDni(state, message: jsonValidacionDni.msgResult, jsonValidacionDni: jsonValidacionDni)
        }
    }
    
    func validarCe() {
        self.controller?.loaderView(isVisible: true)
        let ce = controller?.numDocument ?? ""
        let guid = UserPreferencesController.getGuid()
        self.model.postValidarCe(ce: ce, guid: guid) { (jsonValidacionCe) in
            self.controller?.loaderView(isVisible: false)
            let state = jsonValidacionCe.codResult == "1"
            self.controller?.loadDatosCe(state, message: jsonValidacionCe.msgResult, jsonValidacionCe: jsonValidacionCe)
        }
    }
    
    func validateData() {
        let numcelular = self.controller?.numcelular ?? ""
        let numDocument = self.controller?.numDocument ?? ""
        let dateOfIssue = self.controller?.dateOfIssue ?? ""
        let typeDocument = self.controller?.typeDocument ?? ""
        let names = self.controller?.names ?? ""
        let lastName = self.controller?.lastName ?? ""
        let middleName = self.controller?.middleName ?? ""
        let gender = self.controller?.gender ?? ""
        let email = self.controller?.email ?? ""
        let isChecked = self.controller?.isChecked ?? false
        var isValidDocument = self.controller?.isValidDocument ?? false
        
        if (typeDocument != "03" && typeDocument != "09") {
            isValidDocument = true
        }
        
        if (numcelular.isEmpty || numDocument.isEmpty || dateOfIssue.isEmpty || typeDocument.isEmpty || names.isEmpty || lastName.isEmpty || middleName.isEmpty || gender.isEmpty || email.isEmpty) {
            self.controller?.goToConfirmPassword(false, message: "Los campos no pueden estar vacio.")
        } else if (!isValidDocument) {
            self.controller?.goToConfirmPassword(false, message: "Ingrese un documento válido.")
        } else if (!isChecked) {
            self.controller?.goToConfirmPassword(false, message: "Debe de aceptar los términos y condiciones.")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToConfirmPassword(false, message: "Ingrese un email válido.")
        } else {
            self.controller?.goToConfirmPassword(true, message: "")
        }
        
    }
    
}
