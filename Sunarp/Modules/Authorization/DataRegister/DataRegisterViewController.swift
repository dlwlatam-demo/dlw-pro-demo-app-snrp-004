//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class DataRegisterViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var numberDocumentTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    @IBOutlet weak var namesTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var genderTypeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var maxLength = 0
    var tipoDocumentos: [String] = []
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    var generos: [String] = ["MASCULINO", "FEMENINO", "PREFIERO NO DECLARARLO"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var generoPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    private lazy var presenter: DataRegisterPresenter = {
       return DataRegisterPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        presenter.didLoad()
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Registro")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func validateFieldsToEnableButton() {
        
        if !(namesTextField.text?.isEmpty ?? true) && !(lastNameTextField.text?.isEmpty ?? true) && !(numberDocumentTextField.text?.isEmpty ?? true) && !(dateOfIssueTextField.text?.isEmpty ?? true) && !(emailTextField.text?.isEmpty ?? true) && !(genderTypeTextField.text?.isEmpty ?? true) && isChecked {
            toConfirmView.primaryButton()
        } else {
            toConfirmView.primaryDisabledButton()
        }
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkIconAndTitleView.addGestureRecognizer(tapChecked)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        numberDocumentTextField.border()
        numberDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        dateOfIssueTextField.border()
        dateOfIssueTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        namesTextField.border()
        namesTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        lastNameTextField.border()
        lastNameTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        middleNameTextField.border()
        middleNameTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        genderTypeTextField.border()
        genderTypeTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        emailTextField.border()
        emailTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        checkView.borderCheckView()
        //toConfirmView.primaryButton()
        toConfirmView.primaryDisabledButton()
        
        self.numberDocumentTextField.delegate = self
        self.numberDocumentTextField.inputAccessoryView = createToolbarGender()
        self.tipoDocumentoPickerView.tag = 1
        self.generoPickerView.tag = 2
        
        self.generoPickerView.delegate = self
        self.generoPickerView.dataSource = self
        self.genderTypeTextField.inputView = self.generoPickerView
        self.genderTypeTextField.inputAccessoryView = createToolbarGender()
        
        self.namesTextField.delegate = self
        self.lastNameTextField.delegate = self
        self.middleNameTextField.delegate = self
        self.emailTextField.delegate = self
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numberDocumentTextField.resignFirstResponder()
        self.namesTextField.resignFirstResponder()
        self.lastNameTextField.resignFirstResponder()
        self.middleNameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            
        }
        
        self.isChecked = !self.isChecked
        validateFieldsToEnableButton()
        
    }
    
    @objc private func onTapToConfirmView() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        self.names = self.namesTextField.text ?? ""
        self.lastName = self.lastNameTextField.text ?? ""
        self.middleName = self.middleNameTextField.text ?? ""
        self.email = self.emailTextField.text ?? ""
        self.presenter.validateData()
    }
    
    func goToConfirmPassword(_ state: Bool, message: String) {
        if (state) {
            if (state) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPasswordRegisterViewController") as! ConfirmPasswordRegisterViewController
                vc.numcelular = self.numcelular
                vc.numDocument = self.numDocument
                vc.dateOfIssue = self.dateOfIssue
                vc.typeDocument = self.typeDocument
                vc.names = self.names
                vc.lastName = self.lastName
                vc.middleName = self.middleName
                vc.gender = self.gender
                vc.email = self.email
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
    }
     
    func changeTypeDocument(index: Int) {
        self.typeDocument = self.tipoDocumentosEntities[index].tipoDocId
        self.namesTextField.isEnabled = false
        self.lastNameTextField.isEnabled = false
        self.middleNameTextField.isEnabled = false
        self.isValidDocument = false
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            self.maxLength = 8
            self.numberDocumentTextField.keyboardType = .numberPad
            self.dateOfIssueTextField.placeholder = "Fecha Emisión"
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            self.maxLength = 9
            self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numberDocumentTextField.keyboardType = .alphabet
        } else {
            self.maxLength = 20
            self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numberDocumentTextField.keyboardType = .alphabet
            self.namesTextField.isEnabled = true
            self.lastNameTextField.isEnabled = true
            self.middleNameTextField.isEnabled = true
            self.isValidDocument = true
        }
        self.numberDocumentTextField.text = ""
        self.dateOfIssueTextField.text = ""
        self.namesTextField.text = ""
        self.lastNameTextField.text = ""
        self.middleNameTextField.text = ""
        createDatePicker()
    }
    
    func changeTypeGender(index: Int) {
        if (index == 0) {
            self.gender = "M"
        } else if (index == 1) {
            self.gender = "F"
        } else {
            self.gender = "-"
        }
    }
    
    func validateTypeDocument() {
        self.numDocument = self.numberDocumentTextField.text ?? ""
        self.dateOfIssue = self.dateOfIssueTextField.text ?? ""
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            if (!numDocument.isEmpty && !dateOfIssue.isEmpty) {
                self.presenter.validarDni()
            }
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            if (!numDocument.isEmpty) {
                self.presenter.validarCe()
            }
        }
    }
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            self.namesTextField.text = jsonValidacionDni.nombres
            self.lastNameTextField.text = jsonValidacionDni.apellidoPaterno
            self.middleNameTextField.text = jsonValidacionDni.apellidoMaterno
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            self.namesTextField.text = jsonValidacionCe.strNombres
            self.lastNameTextField.text = jsonValidacionCe.strPrimerApellido
            self.middleNameTextField.text = jsonValidacionCe.strSegundoApellido
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            comps.year = -10
            let minDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = .now
            datePicker.minimumDate = minDate
        } else if (self.typeDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            comps.year = -100
            let minDate = calender.date(byAdding: comps, to: .now)
            comps.year = -18
            let maxDate = calender.date(byAdding: comps, to: .now)
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
        }
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateOfIssueTextField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        validateTypeDocument()
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
}

extension DataRegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentos.count
        case 2:
            return self.generos.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentos[row]
        case 2:
            return self.generos[row]
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeDocument(index: row)
            self.typeDocumentTextField.text = tipoDocumentos[row]
            self.typeDocumentTextField.resignFirstResponder()
        case 2:
            self.changeTypeGender(index: row)
            self.genderTypeTextField.text = generos[row]
            self.genderTypeTextField.resignFirstResponder()
        default:
            return
        }
        
    }
}

extension DataRegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == numberDocumentTextField) {
            if (count == maxLength) {
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                    self.validateTypeDocument()
                    return false
                }
            } else {
                self.namesTextField.text = ""
                self.lastNameTextField.text = ""
                self.middleNameTextField.text = ""
                self.isValidDocument = false
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        validateFieldsToEnableButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
}


private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}
