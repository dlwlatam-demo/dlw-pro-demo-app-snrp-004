//
//  MenuDrawerViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 14/09/22.
//

import UIKit

protocol MenuDrawerDelegate: class {
    func menuDrawerLogoutSelected()
}

class MenuDrawerViewController: UIViewController {
    
    @IBOutlet weak var backImage: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var documentNumberLabel: UILabel!
    @IBOutlet weak var perfilButton: UIView!
    @IBOutlet weak var logoutButton: UIView!
    
    let transitionManager = DrawerTransitionManager()

    weak var delegate: MenuDrawerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
    }
    
    init(delegate: MenuDrawerDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        modalPresentationStyle = .custom
        transitioningDelegate = transitionManager
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupDesigner() {
        let usuarioLogin = UserPreferencesController.usuario()
        self.profileImage.backgroundCard()
        
        let str = usuarioLogin.userPhoto
        
        let defaultAttributes = [
            .font: UIFont.systemFont(ofSize: 11, weight: .regular),
            .foregroundColor: UtilHelper.getUIColor(hex: "#323c37")
        ] as [NSAttributedString.Key : Any]
        
        let marketingAttributes = [
            .font: UIFont.systemFont(ofSize: 11, weight: .bold),
            .foregroundColor: UtilHelper.getUIColor(hex: "#323c37")
        ] as [NSAttributedString.Key : Any]
        
        let attributedStringDni = [
            NSAttributedString(string: UserPreferencesController.getTipoDocDesc(),
                               attributes: marketingAttributes),
            ": " + usuarioLogin.nroDoc
        ] as [AttributedStringComponent]
        
        self.profileImage.image = convertBase64StringToImage(imageBase64String: str)
        self.fullNameLabel.text = "\(usuarioLogin.nombres) \(usuarioLogin.priApe)"
        self.documentNumberLabel.attributedText = NSAttributedString(from: attributedStringDni, defaultAttributes: defaultAttributes)
    }
    
    private func addGestureView(){
        
        let tapProfileGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToProfileView))
        self.perfilButton.addGestureRecognizer(tapProfileGesture)
        
        let tapBackGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToBackView))
        self.backImage.addGestureRecognizer(tapBackGesture)
        
        let tapLogoutGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToLogoutView))
        self.logoutButton.addGestureRecognizer(tapLogoutGesture)
    }
    
    @objc private func onTapToProfileView() {
        guard let tabbarController = UIApplication.shared.tabbarController() as? HomeViewController else { return }
        tabbarController.selectedIndex = 1
        dismiss(animated: true)
    }
    
    @objc private func onTapToBackView() {
        dismiss(animated: true)
    }
    
    @objc private func onTapToLogoutView() {
        self.delegate?.menuDrawerLogoutSelected()
        dismiss(animated: true)
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image
    }
}

extension UIApplication {
    
    func tabbarController() -> UIViewController? {
        guard let vcs = self.keyWindow?.rootViewController?.children else { return nil }
        for vc in vcs {
            if  let _ = vc as? HomeViewController {
                return vc
            }
        }
        return nil
    }
}
