//
//  PerfilPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 17/08/22.
//

import Foundation

class InicioPresenter {
    
    private weak var controller: InicioViewController?
    
    lazy private var model: ProfileModel = {
        let navigation = controller?.navigationController
       return ProfileModel(navigationController: navigation!)
    }()
    
    lazy private var loginModel: LoginModel = {
        let navigation = controller?.navigationController
       return LoginModel(navigationController: navigation!)
    }()
    
    init(controller: InicioViewController) {
        self.controller = controller
    }
    
}

extension InicioPresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getListaTipoDocumentos{ (arrayTipoDocumentos) in
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.model.postLogin(appId: Constant.APP_ID, deviceToken: "", deviceType: "", status: "A", deviceId: "1", appVersion: appVersion, ipAddress: UtilHelper.getIPAddress(), deviceLogged: "1", receiveNotifications: "N", deviceOs: Constant.DEVICE_OS)  { (profileEntity) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                   print("email = \(profileEntity.usuarioLogin.email)")
                    if profileEntity.usuarioLogin.email == "" {
                        self.controller?.loaderView(isVisible: false)
                        UserPreferencesController.clearPreference()
                        self.controller?.goToLogin()
                    }
                    else {
                        let tipoDocumento = arrayTipoDocumentos.filter { $0.tipoDocId == profileEntity.usuarioLogin.tipoDoc }
                        UserPreferencesController.setTipoDocDesc(tipoDocDesc: tipoDocumento.first?.nombreAbrev)
                        self.controller?.loaderView(isVisible: false)
                    }
                }
            }
        }
    }
    
    func logout() {
        self.controller?.loaderView(isVisible: true)
        self.loginModel.deleteLogout(jti: UserPreferencesController.getJti()) { (logoutResponse) in
            let state = logoutResponse.resultado == "0" || logoutResponse.resultado == ""
            if (state) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.controller?.loaderView(isVisible: false)
                    UserPreferencesController.clearPreference()
                    self.controller?.goToLogin()
                }
            }
        }

    }
    
}
