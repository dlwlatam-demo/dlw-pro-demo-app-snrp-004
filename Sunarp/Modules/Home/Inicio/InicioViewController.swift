//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift

class InicioViewController: UIViewController {
        
    
    var window: UIWindow?
    var router:Router!
    let disposebag = DisposeBag()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    var loading: UIAlertController!
    private lazy var presenter: InicioPresenter = {
        return InicioPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        //bannerHome()
        addGestureView()
    }
    
    @IBAction func gobtnNotification() {
        
        let storyboard = UIStoryboard(name: "Notification", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    private func addGestureView(){
        
        let tapContactGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToContactoView))
        self.contactoView.addGestureRecognizer(tapContactGesture)
        
        let tapServiceGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToServiceView))
        self.serviciosView.addGestureRecognizer(tapServiceGesture)
        
        let tapalertasGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToAlertView))
        self.alertasView.addGestureRecognizer(tapalertasGesture)
        
        let tapMapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToMapView))
        self.workView.addGestureRecognizer(tapMapGesture)
        
    }
    
    @IBAction func onTapMenu(_ sender: Any) {
        let drawerController = MenuDrawerViewController(delegate: self)
        present(drawerController, animated: true)
    }
    
    @objc private func onTapToMapView() {
        let storyboard = UIStoryboard(name: "Mapa", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapaViewController") as! MapaViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapToServiceView() {
      
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiciosViewController") as! ServiciosViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapToContactoView() {
      
        let storyboard = UIStoryboard(name: "Contacto", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContactoFirstViewController") as! ContactoFirstViewController
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    @objc private func onTapToAlertView() {
      
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegisterViewController") as! AlertRegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    func bannerHome(){
       // self.confirmDialog(title: "",message: "",primaryButton: "",secondaryButton: "X",delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        presenter.willAppear()
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        self.formView.backgroundCard()
        self.serviciosView.borderIconView()
        self.alertasView.borderIconView()
        self.contactoView.borderIconView()
        self.workView.borderIconView()
        lectorQr()
    }
    
    func lectorQr(){
        
        let defaults = UserDefaults.standard
        defaults.set("", forKey: "urlNameKey")
    }
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
}
/*
extension InicioViewController: BannerDialogDelegate{
    func primary(action:String) {
        if action == "CERRAR " {
           // closeMedicalAttention()
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
        }
    }
    
    func secondary() {
    }
}*/

extension InicioViewController: MenuDrawerDelegate {
    
    func menuDrawerLogoutSelected() {
        self.presenter.logout()
    }
    
    func goToLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
    
}
