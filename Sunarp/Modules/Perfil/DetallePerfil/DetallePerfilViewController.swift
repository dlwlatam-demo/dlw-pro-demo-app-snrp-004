//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit

class DetallePerfilViewController: UIViewController {
     
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    @IBOutlet weak var namesTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var genderTypeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var photoImage: UIImageView!
    
    @IBOutlet weak var numeroTextField: UITextField!
    @IBOutlet weak var numeroDNITextField: UITextField!
    
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    let imagePicker = UIImagePickerController()
        
    var generoPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    
    var loading: UIAlertController!
    var isChecked: Bool = false
    var usuarioLoginEntity: UsuarioLoginEntity!
    var arrayTipoDocumentos: [TipoDocumentoEntity]!
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    var photo: String = ""
    
    var generos: [String] = ["MASCULINO", "FEMENINO", "PREFIERO NO DECLARARLO"]
        
    private lazy var presenter: DetallePerfilPresenter = {
       return DetallePerfilPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
        loadDataPerfil()
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Editar Mi Perfil")
        
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapCameraGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapOpenCamera))
        self.checkImage.addGestureRecognizer(tapCameraGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
    }
    
    private func setupDesigner() {
        
        formView.backgroundCard()
        photoImage.backgroundCard()
       
        namesTextField.border()
        namesTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        lastNameTextField.border()
        lastNameTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        dateOfIssueTextField.border()
        dateOfIssueTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        middleNameTextField.border()
        middleNameTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        genderTypeTextField.border()
        genderTypeTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        emailTextField.border()
        emailTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        numeroTextField.border()
        numeroTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        numeroDNITextField.border()
        numeroDNITextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
    
        toConfirmView.primaryButton()
        
        self.generoPickerView.tag = 2
        
        self.generoPickerView.delegate = self
        self.generoPickerView.dataSource = self
        self.genderTypeTextField.inputView = self.generoPickerView
        self.genderTypeTextField.inputAccessoryView = createToolbarGender()
        
        imagePicker.delegate = self
        
        self.numeroTextField.delegate = self
        
//        createDatePicker()
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.namesTextField.resignFirstResponder()
        self.lastNameTextField.resignFirstResponder()
        self.middleNameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
        self.numeroTextField.resignFirstResponder()
        self.numeroDNITextField.resignFirstResponder()
        
        
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
        }
        self.isChecked = !self.isChecked
    }
    
    @objc private func onTapToConfirmView() {
        self.numDocument = self.numeroDNITextField.text ?? ""
        self.names = self.namesTextField.text ?? ""
        self.lastName = self.lastNameTextField.text ?? ""
        self.middleName = self.middleNameTextField.text ?? ""
        self.email = self.emailTextField.text ?? ""
        self.numcelular = self.numeroTextField.text ?? ""
        self.dateOfIssue = formatDateInv(self.dateOfIssueTextField.text ?? "") ?? ""
        
        self.presenter.updateUser()
    }
    
    @objc private func onTapOpenCamera() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func loadDataPerfil() {
        let sexo = usuarioLoginEntity.sexo
        var sexoDescription = ""
        let numeroDocumento = usuarioLoginEntity.nroDoc
        let numeroCelular = usuarioLoginEntity.nroCelular
        let email = usuarioLoginEntity.email
        let fechaNacimientoInic = usuarioLoginEntity.fecNac
        let fechaNacimiento = formatDate(fechaNacimientoInic) ?? ""
        self.gender = sexo
        if (sexo == "M") {
            sexoDescription = "MASCULINO"
        } else if (sexo == "F") {
            sexoDescription = "FEMENINO"
        } else {
            sexoDescription = "PREFIERO NO DECLARARLO"
        }
        
        let tipoDocumento = arrayTipoDocumentos.filter { $0.tipoDocId == usuarioLoginEntity.tipoDoc }
        
        self.typeDocumentTextField.text = tipoDocumento.first?.nombreAbrev
        self.numeroDNITextField.text = numeroDocumento
        self.dateOfIssueTextField.text = fechaNacimiento
        self.namesTextField.text = self.usuarioLoginEntity.nombres
        self.lastNameTextField.text = self.usuarioLoginEntity.priApe
        self.middleNameTextField.text = self.usuarioLoginEntity.segApe
        self.genderTypeTextField.text = sexoDescription
        self.emailTextField.text = email
        self.numeroTextField.text = numeroCelular
                
        let str = usuarioLoginEntity.userPhoto
        self.photoImage.image = convertBase64StringToImage(imageBase64String: str)
        
    }
    
    func formatDate(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy/MM/dd"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "dd/MM/yyyy"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func formatDateInv(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "yyyy-MM-dd"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    
    
    func loadDataPerfilUpd(usuarioLogin: UsuarioLoginEntity, arrayTipoDocumentos: [TipoDocumentoEntity]) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
          
        self.usuarioLoginEntity = usuarioLogin
        self.arrayTipoDocumentos = arrayTipoDocumentos
        
        }
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image
    }
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.resizeImage(600.0, opaque: true).jpegData(compressionQuality: 0.25)?.base64EncodedString() ?? ""
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func goToPerfil(_ state: Bool, message: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func resultSavePhoto(_ state: Bool, message: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (!state) {
                    let str = self.usuarioLoginEntity.userPhoto
                    self.photoImage.image = self.convertBase64StringToImage(imageBase64String: str)
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    func changeTypeGender(index: Int) {
        if (index == 0) {
            self.gender = "M"
        } else if (index == 1) {
            self.gender = "F"
        } else {
            self.gender = "-"
        }
    }
    
    func createDatePicker() {
        let calender = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        
        comps.year = -100
        let minDate = calender.date(byAdding: comps, to: .now)
        comps.year = -18
        let maxDate = calender.date(byAdding: comps, to: .now)
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        
        dateOfIssueTextField.inputView = datePicker
        dateOfIssueTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateOfIssueTextField.text = dateFormatter.string(from: datePicker.date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateOfIssue = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
}

extension DetallePerfilViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            photoImage.contentMode = .scaleAspectFit
            photoImage.image = pickedImage
            savePhoto()
        }
        dismiss(animated: true, completion: nil)
    }
        
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func savePhoto() {
        let image: UIImage = (self.photoImage.image?.resizeImage(600.0, opaque: true))!
        
        self.photo = convertImageToBase64String(img: image)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
            self.presenter.updateUserPhoto()
        }
    }
    
}


extension DetallePerfilViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.generos.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.generos[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.changeTypeGender(index: row)
        self.genderTypeTextField.text = generos[row]
        self.genderTypeTextField.resignFirstResponder()        
    }
}
extension DetallePerfilViewController: UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
}
