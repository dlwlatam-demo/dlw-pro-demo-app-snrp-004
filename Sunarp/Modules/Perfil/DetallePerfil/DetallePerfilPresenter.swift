//
//  DetallePerfilPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/08/22.
//

import Foundation

class DetallePerfilPresenter {
    
    private weak var controller: DetallePerfilViewController?
    
    lazy private var model: ProfileModel = {
        let navigation = controller?.navigationController
       return ProfileModel(navigationController: navigation!)
    }()
    
    init(controller: DetallePerfilViewController) {
        self.controller = controller
    }
    
}

extension DetallePerfilPresenter: GenericPresenter {
        
    func updateUser() {
        self.controller?.loaderView(isVisible: true)
        let sexo = self.controller?.gender ?? ""
        let tipoDoc = self.controller?.usuarioLoginEntity.tipoDoc ?? ""
        let nroDoc = self.controller?.numDocument ?? ""
        let nombres = self.controller?.names ?? ""
        let priApe = self.controller?.lastName ?? ""
        let segApe = self.controller?.middleName ?? ""
        let fecNac = self.controller?.dateOfIssue ?? ""
        let nroCelular = self.controller?.numcelular ?? ""
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        //let rememberToken = UserPreferencesController.getAccessToken()
        let rememberToken = "test"
        let lastConn = UtilHelper.dateNow()
        
        self.model.putRegistro(sexo: sexo, tipoDoc: tipoDoc, nroDoc: nroDoc, nombres: nombres, priApe: priApe, segApe: segApe, fecNac: fecNac, nroCelular: nroCelular, rememberToken: rememberToken, appVersion: appVersion, ipAddress: UtilHelper.getIPAddress(), lastConn: lastConn, geoLat: 0, geoLong: 0) { (objSolicitud) in
            self.controller?.loaderView(isVisible: false)
            let state = objSolicitud.codResult == "1"
            self.controller?.goToPerfil(state, message: objSolicitud.msgResult)
        }
    }
    
    func updateUserPhoto() {
        self.controller?.loaderView(isVisible: true)
        let photo = self.controller?.photo ?? ""
        
        self.model.putRegistroFoto(userPhoto: photo) { (objSolicitud) in
            self.controller?.loaderView(isVisible: false)
            let state = objSolicitud.codResult == "1"
            self.controller?.resultSavePhoto(state, message: objSolicitud.msgResult)
        }
    }
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getListaTipoDocumentos{ (arrayTipoDocumentos) in
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.controller?.loadDataPerfilUpd(usuarioLogin: UserPreferencesController.usuario(), arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
}
