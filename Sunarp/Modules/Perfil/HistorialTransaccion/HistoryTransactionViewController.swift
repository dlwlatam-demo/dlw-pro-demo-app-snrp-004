//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift

class HistoryTransactionViewController: UIViewController {
        
    
    var style: Style = Style.myApp
    let disposebag = DisposeBag()
     
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet var tableView:UITableView!
    
    @IBOutlet weak var totalRegText: UILabel!
    var arrayTipoDocumentos: [TipoDocumentoEntity]!
    var loading: UIAlertController!
    var solicitudes: [SolicitudEntity] = []
    
    private lazy var presenter: HistoryTransactionPresenter = {
        return HistoryTransactionPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        tableView.reloadData()
        registerCell()
        setupNavigationBar()
        presenter.didLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        if (UserPreferencesController.getReloadData()) {
            self.presenter.didLoad()
            UserPreferencesController.setReloadData(reloadData: false)
        }
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Historial de Transacciones")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
        
    private func setupDesigner() {
        tableView.dataSource = self
        
    }
    
    func registerCell() {
        let profileCellNib = UINib(nibName: HistoryTransactionViewCell.reuseIdentifier, bundle: nil)
        tableView.register(profileCellNib, forCellReuseIdentifier: HistoryTransactionViewCell.reuseIdentifier)    
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading?.dismiss(animated: false)
            self.loading = self.loader()
        } else {
//            self.stopLoader(loader: self.loading)
        }
    }
    
    func reloadSolicitud(history: HistoryEntity) {
        self.loaderView(isVisible: false)
        self.solicitudes = history.solicitudes
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if solicitudes.count >= 10 {
            totalRegText.text = "Total de registros: \(solicitudes.count)"
        } else {
            totalRegText.text = ""
        }

    }
}

// MARK: - Table view datasource
extension HistoryTransactionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return solicitudes.count
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTransactionViewCell", for: indexPath) as? HistoryTransactionViewCell else {
            return UITableViewCell()
        }
         
        let solicitud = solicitudes[indexPath.row]
        cell.setup(with: solicitud)
        cell.onTapVerSolicitud = {[weak self] in
            if (solicitud.tipo == "1") {
                let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DocumentoBoletaViewController") as! DocumentoBoletaViewController
                vc.documentKey = solicitud.solicitudKey
                //vc.documentKey = 41080951
                vc.typeDocument = .boleta
                self?.navigationController?.pushViewController(vc, animated: true)
            }  else {
                let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EstadoSolicitudViewController") as! EstadoSolicitudViewController
                vc.solicitudId = solicitud.solicitudKey
                vc.solicitudTipo = solicitud.tipo
                vc.arrayTipoDocumentos = self?.arrayTipoDocumentos
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        return cell
    }
}
