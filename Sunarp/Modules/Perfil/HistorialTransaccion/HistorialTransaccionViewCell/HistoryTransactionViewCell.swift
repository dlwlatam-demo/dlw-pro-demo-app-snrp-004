//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit

final class HistoryTransactionViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "HistoryTransactionViewCell"
    @IBOutlet var titleNameLabel:UILabel!
    @IBOutlet var titleNumTransLabel:UILabel!
    @IBOutlet var titleServiceLabel:UILabel!
    @IBOutlet var titleNumPedidoLabel:UILabel!
    @IBOutlet var dateLabel:UILabel!
    @IBOutlet var priceLabel:UILabel!
    @IBOutlet var viewCell:UIView!
    @IBOutlet var viewPoint:UIView!
    @IBOutlet var verSolicitud: UIView!
    @IBOutlet var verSolicitudTitle: UILabel!
    
    var onTapVerSolicitud: (() -> Void)?
    
    func setup(with solicitud: SolicitudEntity) {
        viewCell.layer.cornerRadius = 10
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        viewPoint.backgroundColor = UIColor.cyan
        
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold12]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular12]
        
        self.titleNameLabel.text = obtainTitleByType(tipo: solicitud.tipo)
        
        let transaccionTitle = NSMutableAttributedString(string: "Nro. de transacción: ", attributes: boldAttrs  as [NSAttributedString.Key : Any])
        let transaccionDescription = NSMutableAttributedString(string: "\(solicitud.transId)", attributes: normalAttrs as [NSAttributedString.Key : Any])
        transaccionTitle.append(transaccionDescription)
        self.titleNumTransLabel.attributedText =  transaccionTitle
        
        let servicioTitle = NSMutableAttributedString(string: "Servicio: ", attributes: boldAttrs  as [NSAttributedString.Key : Any])
        let servicioDescription = NSMutableAttributedString(string: solicitud.strBusq, attributes: normalAttrs as [NSAttributedString.Key : Any])
        servicioTitle.append(servicioDescription)
        self.titleServiceLabel.attributedText =  servicioTitle
        
        let publicidadTitle = NSMutableAttributedString(string: "Número de Publicidad: ", attributes: boldAttrs  as [NSAttributedString.Key : Any])
        let publicidadDescription = NSMutableAttributedString(string: "\(solicitud.aaPubl)-\(solicitud.nuPubl)", attributes: normalAttrs as [NSAttributedString.Key : Any])
        publicidadTitle.append(publicidadDescription)
        self.titleNumPedidoLabel.attributedText =  publicidadTitle
        
        self.dateLabel.text = solicitud.fecHor
        self.priceLabel.text = "S/ \(solicitud.costo).00"
        
        self.verSolicitud.isHidden = !shouldShowButton(tipo: solicitud.tipo)
        self.verSolicitudTitle.text = obtainTitleButtonByType(tipo: solicitud.tipo)
        
        self.viewPoint.backgroundColor = UIColor.colorFromHexString(obtainColorByType(tipo: solicitud.tipo), withAlpha: 1.0)
                
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.verSolicitud.addGestureRecognizer(onTapVerSolicitud)
        
    }
    
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    
    func obtainTitleByType(tipo: String) -> String {
        switch (tipo) {
        case "1": return "Boleta Informativa RPV"
        case "2": return "Búsqueda por Nombre"
        case "3": return "Publicidad Compendiosa"
        default: return ""
        }
    }
    
    func shouldShowButton(tipo: String) -> Bool {
        switch (tipo) {
        case "1": return true
        case "3": return true
        case "2": return false
        default: return false
        }
    }
    
    func obtainTitleButtonByType(tipo: String) -> String {
        switch (tipo) {
        case "1": return "Ver Boleta"
        case "3": return "Ver solicitud"
        default: return ""
        }
    }
    
    func obtainColorByType(tipo: String) -> String {
        switch (tipo) {
        case "1": return "#00a5a5"
        case "2": return "#b2b2b2"
        case "3": return "#ffaf00"
        default: return ""
        }
    }
    
}
