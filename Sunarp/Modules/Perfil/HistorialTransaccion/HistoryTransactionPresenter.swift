//
//  HistoryTransactionPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 26/08/22.
//

import Foundation

class HistoryTransactionPresenter {
    
    private weak var controller: HistoryTransactionViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: HistoryTransactionViewController) {
        self.controller = controller
    }
    
}

extension HistoryTransactionPresenter: GenericPresenter {
 
    func didLoad() {
//        self.controller?.loaderView(isVisible: true)
        self.model.getHistorial { (arraySolicitud) in
            self.controller?.reloadSolicitud(history: arraySolicitud)
        }
    }
    
}
