//
//  LiquidacionSolicitudViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import UIKit
import VisaNetSDK

class LiquidacionSolicitudViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var nombreText: UITextField!
    @IBOutlet weak var apellidoPaternoText: UITextField!
    @IBOutlet weak var apellidoMaternoText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var costoLabel: UILabel!
    @IBOutlet weak var terminoCondicionesCheckView: UIView!
    @IBOutlet weak var terminoCondicionesCheckImage: UIImageView!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var terminoCondicionesLabel: UILabel!
    @IBOutlet weak var labelTitulo: UILabel!
    
    
    var loading: UIAlertController! 
    var arrayTipoDocumentos: [TipoDocumentoEntity]!
    var isChecked: Bool = false
    var nsolMonLiq: String = "0.0"
    var tokenNiubiz: String = ""
    var transactionIdNiubiz: String = ""
    var pinHash: String = ""
    var solicitudId: Int = 0
    var visaKeys: VisaKeysEntity!
    var typeController: PagoExitosoViewController.Controller = .liquidacion
    var codZona: String = ""
    var codOficina: String = ""
    var placa: String = ""
    
    var pagoExitoso:PagoExitosoEntity?
    
    private lazy var presenter: LiquidacionSolicitudPresenter = {
       return LiquidacionSolicitudPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBar()
        setupDesigner()
        addGestureView()
        setValues()
        print("typeController = ", typeController)
    }
        
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Pago de liquidación")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupDesigner() {
        formView.backgroundCard()
        nombreText.borderAndPaddingLeftAndRight()
        apellidoPaternoText.borderAndPaddingLeftAndRight()
        apellidoMaternoText.borderAndPaddingLeftAndRight()
        emailText.borderAndPaddingLeftAndRight()
        
        apellidoPaternoText.delegate = self
        apellidoMaternoText.delegate = self
        nombreText.delegate = self
        
        pagarView.primaryButton()
        terminoCondicionesCheckView.borderCheckView()
        
        self.pagarView.primaryDisabledButton()
        self.isChecked = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func addGestureView(){
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.terminoCondicionesCheckView.addGestureRecognizer(tapChecked)
        
        let tapTerminoCondicionesGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapTerminoCondicionesLabel))
        self.terminoCondicionesLabel.addGestureRecognizer(tapTerminoCondicionesGesture)
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
    }
    
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
    }
    
    
    @objc private func onTapTerminoCondicionesLabel(){
       /* if let url = URL(string: "https://www.sunarp.gob.pe/politicas-privacidad.aspx") {
            UIApplication.shared.open(url)
        }*/
        self.alertTermyConPopupDialog(title: "Estimado(a) usuario(a):",message: "¿?",primaryButton: "No,regresar",secondaryButton: "Si,remover", addColor: SunarpColors.red, delegate: self)
    }
    
    @objc private func onTapPagarView(){
        
        if (self.isChecked && self.nombreText.text != "" && self.apellidoPaternoText.text != "") {
            loaderView(isVisible: true)
            presenter.getVisaKeys()
        }
    }
    
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
        
        self.nombreText.text = usuario.nombres
        self.apellidoPaternoText.text = usuario.priApe
        self.apellidoMaternoText.text = usuario.segApe
        self.emailText.text = usuario.email
        
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2

        let number = NSNumber(value: Float(nsolMonLiq) ?? 0.0)
        let newNsolMonLiq = formatter.string(from: number)!
        
        let costoValue = NSMutableAttributedString(string: "\(newNsolMonLiq)", attributes: boldAttrs as [NSAttributedString.Key : Any])
        
        costoTitle.append(costoValue)
        self.costoLabel.attributedText =  costoTitle
        
        self.labelTitulo.text = "Boleta Vehicular"
        
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.terminoCondicionesCheckImage.image = nil
            self.pagarView.primaryDisabledButton()
        } else {
            self.terminoCondicionesCheckImage.image = UIImage(systemName: "checkmark")
            self.pagarView.primaryButton()
        }
        self.isChecked = !self.isChecked
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        //presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: visaKeys.urlVisanetToken)
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: SunarpWebService.Niubiz.postNiubiz())
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        //self.pinHash = self.visaKeys.pinHash
        //self.presenter.getTransactionId()
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        self.presenter.getTransactionId()
    }
    
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        self.loadNiubizScreen()
    }
    
    func loadNiubizScreen() {
        let usuario = UserPreferencesController.usuario()
        
        Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
        Config.merchantID = self.visaKeys.merchantId
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.type = .prod
        } else {
            Config.CE.type = .dev
        }
                        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        //Config.CE.purchaseNumber = String(describing: self.transactionIdNiubiz)
        Config.CE.purchaseNumber = self.transactionIdNiubiz // "\(Int.random(in:99999...9999999999))"

        
        Config.amount = Double(self.nsolMonLiq) ?? 0.0
        //Config.amount = 6.0
        Config.CE.countable = true
        Config.CE.showAmount = true
        Config.CE.initialAmount = Double(self.nsolMonLiq) ?? 0.0
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        Config.CE.LastNameField.defaultText = apellidoPaternoText.text!
        Config.CE.FirstNameField.defaultText = nombreText.text!
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
                
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
       var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente
 
        
        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
           VisaNet.shared.delegate = self
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: Constant.Localize.información, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: - VISANET DELEGATE SECTION

extension LiquidacionSolicitudViewController : VisaNetDelegate{
    
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
           showAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        print("RESPONSE DATA: \(String(describing: responseData))")
        
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData)  {
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoExitosoViewController") as! PagoExitosoViewController
                vc.transactionData = transactionData
                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.pagoExitosoEntity = self.pagoExitoso
                print("----------------------")
                print(transactionData)
                vc.monto = self.nsolMonLiq
                vc.solicitudId = self.solicitudId
                vc.usuario = "APPSNRPIOS"
                vc.apPaterno = apellidoPaternoText.text ?? ""
                vc.apMaterno = apellidoMaternoText.text ?? ""
                vc.nombre = nombreText.text ?? ""
                vc.controller = typeController
                
                vc.costo = self.nsolMonLiq
                vc.ip = "0.0.0.0"
                vc.codZona = self.codZona
                vc.codOficina = self.codOficina
                vc.placa = self.placa.replacingOccurrences(of: "-", with: "")
                
                vc.codAccion = transactionData.dataMap.actionCode
                vc.codAutoriza = transactionData.dataMap.authorizationCode
                vc.codtienda = transactionData.dataMap.merchant
                vc.concepto = "Boleta Informativa"
                vc.decisionCs = "Accept"
                vc.dscCodAccion = transactionData.dataMap.actionDescription
                vc.dscEci = transactionData.dataMap.eciDescription
                vc.eci = transactionData.dataMap.eci
                vc.estado = transactionData.dataMap.status
                vc.eticket = transactionData.dataMap.idUnico
                vc.fechaYhoraTx = getCurrentDateAndTime()
                vc.idUnico = transactionData.dataMap.idUnico
                vc.idUser = UserPreferencesController.usuario().nroDoc
                vc.impAutorizado = transactionData.dataMap.amount
                vc.nomEmisor = transactionData.dataMap.brandName
                vc.numOrden = transactionData.dataMap.purchaseNumber
                vc.numReferencia = transactionData.dataMap.traceNumber
                vc.oriTarjeta = "D"
                vc.pan = transactionData.dataMap.card
                vc.resCvv2 = transactionData.dataMap.brandHostID
                vc.respuesta = "1"
                vc.reviewTransaction = "false"
                vc.transId = transactionData.dataMap.transactionID
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else if let message = responseData as? String {
                print("Canceled: \(message)")
            } else {
                print("Unknown error")
                //error del comoponente Visa
                //self.goToSegue(VALUES.SEGUE_PAYMENT_ERROR)
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            print("ERROR: \(String(describing: serverError))")
            if let error = responseData as? [String:AnyObject] {
                /*let visaErrorModel = WebTranslator.JSONtoVisaNetErrorEntity(error)
                self.processPayment(visaNetSuccess: nil, visaNetError: visaErrorModel)*/
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
                vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                //error del componente  Visa
                //self.goToSegue(VALUES.SEGUE_PAYMENT_ERROR)
                let vc = storyboard.instantiateViewController(withIdentifier: "PagoRechazadoViewController") as! PagoRechazadoViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


func getCurrentDateAndTime() -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
    let now = Date()
    return dateFormatter.string(from: now)
}
extension LiquidacionSolicitudViewController: TermyCondiDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
          //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
       // print("self.val_aaCont::>>",self.val_aaCont)
        //self.presenter.putDeleteMandato(aaCont: self.val_aaCont, nuCont: self.val_nuCount)
        
        
    }
}


extension LiquidacionSolicitudViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
    
}

extension Date {

    static func -(recent: Date, previous: Date) -> (month: Int?, day: Int?, hour: Int?, minute: Int?, second: Int?) {
        let day = Calendar.current.dateComponents([.day], from: previous, to: recent).day
        let month = Calendar.current.dateComponents([.month], from: previous, to: recent).month
        let hour = Calendar.current.dateComponents([.hour], from: previous, to: recent).hour
        let minute = Calendar.current.dateComponents([.minute], from: previous, to: recent).minute
        let second = Calendar.current.dateComponents([.second], from: previous, to: recent).second

        return (month: month, day: day, hour: hour, minute: minute, second: second)
    }

}
