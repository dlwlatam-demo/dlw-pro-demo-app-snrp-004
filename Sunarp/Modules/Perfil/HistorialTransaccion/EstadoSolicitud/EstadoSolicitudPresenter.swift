//
//  EstadoSolicitudPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 27/08/22.
//

import Foundation

class EstadoSolicitudPresenter {
    
    private weak var controller: EstadoSolicitudViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: EstadoSolicitudViewController) {
        self.controller = controller
    }
    
}

extension EstadoSolicitudPresenter: GenericPresenter {
 
    func didLoad() {
        let id: String = String(controller?.solicitudId ?? 0)
        self.model.getHistorialDetalle(id: id) { (objHistory) in
            self.controller?.loadDataSolicitud(solicitud: objHistory)
        }
    }
    
    func postDesestimiento(solicitudId: String, detalle: String, usuario: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.postDesestimiento(solicitudId: solicitudId, detalle: detalle, usuario: usuario) { (objResponse) in
            self.controller?.loaderView(isVisible: false)
            let state = objResponse.codResult == "1"
            self.controller?.popBackViewController(state, message: objResponse.msgResult)
        }
    }
    
}
