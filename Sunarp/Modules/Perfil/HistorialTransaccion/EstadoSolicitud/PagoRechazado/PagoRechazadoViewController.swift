//
//  PagoRechazadoViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import UIKit

class PagoRechazadoViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var solicitudLabel: UILabel!
    @IBOutlet weak var fechaHoraLabel: UILabel!
    @IBOutlet weak var montoLabel: UILabel!
    @IBOutlet weak var monedaLabel: UILabel!
    @IBOutlet weak var tipoPagoLabel: UILabel!
    @IBOutlet weak var regresarView: UIView!
    @IBOutlet weak var numeroPedidoLabel: UILabel!
    @IBOutlet weak var mensajeErrorLabel: UILabel!
    var guardarSolicitudEntity: GuardarSolicitudEntity?
    var pagoRechazadoEntity: PagoRechazadoEntity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        addGestureView()
        setValues()
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Pago de liquidación")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupDesigner() {
        formView.backgroundCard()
        regresarView.primaryRejectButton()
    }
    
    func setValues() {
        if let item = guardarSolicitudEntity {
            solicitudLabel.text = "\(item.solicitudId)"
            fechaHoraLabel.text = item.tsCrea
        }
        if let item = pagoRechazadoEntity {
            dump(item)
            print(item.data.purchaseNumber)
            numeroPedidoLabel.text = item.data.purchaseNumber
            mensajeErrorLabel.text = "Ha ocurrido un problema con el pago: \(item.data.actionDescription), inténtelo nuevamente."
        }
        
    }
    
    private func addGestureView(){
        
        let tapBackGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapBackView))
        self.regresarView.addGestureRecognizer(tapBackGesture)
        
    }
    
    @objc private func onTapBackView(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

