//
//  PagoExitosoCertificadoLiteralvewController.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 22/10/23.
//

import UIKit

class PagoExitosoCertificadoLiteralvewController: UIViewController {

    @IBOutlet weak var servicioLabel: UILabel!
    @IBOutlet weak var operacionLabel: UILabel!
    @IBOutlet weak var numeroTarjetaLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var numeroPublicidadLabel: UILabel!
    @IBOutlet weak var numeroReciboLabel: UILabel!
    @IBOutlet weak var medioPagoLabel: UILabel!
    @IBOutlet weak var numerSolicitudLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var montoLabel: UILabel!
    @IBOutlet weak var monedaLabel: UILabel!
    @IBOutlet weak var tipoPagoLabel: UILabel!
    @IBOutlet weak var codigoVerificacionLabel: UILabel!
    @IBOutlet weak var finishButton: UIView!
    @IBOutlet weak var numeroPedidoLabel: UILabel!
    
    var paymentProcessEntity: PaymentProcessEntity?
    var transactionData: TransactionData?
    var certificateType:String?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupData()
    }
    
    func setupData(){
        print("setupData")
        
        if let payment = paymentProcessEntity {
            var costoTotal = "0.00"
            if payment.paymentItem.costoTotal != ""  {
                costoTotal = payment.paymentItem.costoTotal
            }
            print(costoTotal)
            if costoTotal != "0.00"  {
                if let transaction = transactionData {
                    operacionLabel.text = transaction.dataMap.actionDescription
                    numeroTarjetaLabel.text = transaction.dataMap.card
                    var name = ""
                    if payment.paymentItem.razSoc != "" {
                        name = payment.paymentItem.razSoc
                    }
                    else {
                        name = "\(payment.paymentItem.nombre) \(payment.paymentItem.apePaterno) \(payment.paymentItem.apeMaterno)"
                    }
                    // let name = "\(transactionData?.dataMap.firstName ?? .empty) \(transactionData?.dataMap.lastName ?? .empty) \(payment.paymentItem.apeMaterno)"
                    nombreLabel.text = name
                    descripcionLabel.text = certificateType
                    numeroPublicidadLabel.text = payment.numeroPublicidad
                    numeroReciboLabel.text = payment.numeroRecibo
                    medioPagoLabel.text = "App Sunarp"
                    numerSolicitudLabel.text = "\(payment.solicitudId)"
                    fechaLabel.text = changeDate(transactionDate: transaction.dataMap.transactionDate)
                    montoLabel.text = "S/ \(transaction.dataMap.amount)"
                    monedaLabel.text = transaction.order.currency
                    tipoPagoLabel.text = transaction.dataMap.brand
                    codigoVerificacionLabel.text = payment.codVerificacion
                    numeroPedidoLabel.text = transaction.dataMap.purchaseNumber
                }
            }
            else {
                // Caso Pago Cero:
                print("pago cero")
                operacionLabel.text = "Aprobado y completado con éxito"
                numeroTarjetaLabel.text = "-"
                
                var name = ""
                if payment.paymentItem.razSoc != "" {
                    name = payment.paymentItem.razSoc
                }
                else {
                    name = "\(payment.paymentItem.nombre) \(payment.paymentItem.apePaterno) \(payment.paymentItem.apeMaterno)"
                }
                nombreLabel.text = name
                descripcionLabel.text = certificateType
                numeroPublicidadLabel.text = payment.numeroPublicidad
                numeroReciboLabel.text = payment.numeroRecibo
                medioPagoLabel.text = "App Sunarp"
                numerSolicitudLabel.text = "\(payment.solicitudId)"
                fechaLabel.text = changeDate(transactionDate: payment.fechaYhoraTx)
                montoLabel.text = "S/ 0.00"
                monedaLabel.text = "Soles"
                tipoPagoLabel.text = "EXONERADO"
                codigoVerificacionLabel.text = payment.codVerificacion
            }
        }
        servicioLabel.text = "Solicitud de certificado"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped))
        finishButton.addGestureRecognizer(tapGesture)
    }
    
    func setupNavigationBar(){
        loadNavigationBar(hideNavigation: false, title: certificateType ?? .empty)
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    
    @objc func goBack() {
        print("goBack")
        if let firstStoryboardViewController = navigationController?.viewControllers.first(where: { $0 is PublicidadCertificadaViewController }) {
            navigationController?.popToViewController(firstStoryboardViewController, animated: true)
        }
        else if let firstStoryboardViewController = navigationController?.viewControllers.first(where: { $0 is CertificadoLiteralPartidaViewController }) {
            navigationController?.popToViewController(firstStoryboardViewController, animated: true)
        }
    }
    
    @objc func viewTapped() {
        if let firstStoryboardViewController = navigationController?.viewControllers.first(where: { $0 is PublicidadCertificadaViewController }) {
            navigationController?.popToViewController(firstStoryboardViewController, animated: true)
        }
        else if let firstStoryboardViewController = navigationController?.viewControllers.first(where: { $0 is CertificadoLiteralPartidaViewController }) {
            navigationController?.popToViewController(firstStoryboardViewController, animated: true)
        }
      }

    func changeDate(transactionDate:String) -> String {
        var newDate = ""
        let dateTx = transactionDate
        if (dateTx.count == 12) {
            let fecha = dateTx.prefix(6)
            let hora = dateTx.suffix(6)
            
            let startFecha = fecha.index(fecha.startIndex, offsetBy: 2)
            let endFecha = fecha.index(fecha.endIndex, offsetBy: -2)
            let rangeFecha = startFecha..<endFecha
            
            let startHora = hora.index(hora.startIndex, offsetBy: 2)
            let endHora = hora.index(hora.endIndex, offsetBy: -2)
            let rangeHora = startHora..<endHora
            
            newDate = "\(fecha.suffix(2))/\(fecha[rangeFecha])/20\(fecha.prefix(2)) \(hora.prefix(2)):\(hora[rangeHora]):\(hora.suffix(2))"
        }
        return newDate
    }

}
