//
//  PagoExitosoPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/09/22.
//

import Foundation

class PagoExitosoPresenter {
    
    private weak var controller: PagoExitosoViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var vehicularModel: ConsultaVehicularModel = {
        let navigation = controller?.navigationController
       return ConsultaVehicularModel(navigationController: navigation!)
    }()
    
    init(controller: PagoExitosoViewController) {
        self.controller = controller
    }
    
}

extension PagoExitosoPresenter: GenericPresenter {
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        
        let solicitudId = self.controller?.solicitudId ?? 0
        let usuario = self.controller?.usuario ?? ""
        let detalle = self.controller?.detalle ?? ""
        let monto = Double(self.controller?.monto ?? "0.0") ?? 0.0
        let terminal = self.controller?.pagoExitosoEntity.terminal ?? ""
        let traceNumber = self.controller?.pagoExitosoEntity.traceNumber ?? ""
        let eciDescription = self.controller?.pagoExitosoEntity.eciDescription ?? ""
        let signature = self.controller?.pagoExitosoEntity.signature ?? ""
        let card = self.controller?.pagoExitosoEntity.card ?? ""
        let merchant = self.controller?.pagoExitosoEntity.merchant ?? ""
        let status = self.controller?.pagoExitosoEntity.status ?? ""
        let actionDescription = self.controller?.pagoExitosoEntity.actionDescription ?? ""
        let idUnico = self.controller?.pagoExitosoEntity.idUnico ?? ""
        let amount = self.controller?.pagoExitosoEntity.amount ?? ""
        let authorizationCode = self.controller?.pagoExitosoEntity.authorizationCode ?? ""
        let transactionDate = self.controller?.pagoExitosoEntity.transactionDate ?? ""
        let actionCode = self.controller?.pagoExitosoEntity.actionCode ?? ""
        let eci = self.controller?.pagoExitosoEntity.eci ?? ""
        let brand = self.controller?.pagoExitosoEntity.brand ?? ""
        let adquirente = self.controller?.pagoExitosoEntity.adquirente ?? ""
        let processCode = self.controller?.pagoExitosoEntity.processCode ?? ""
        let transactionId = self.controller?.pagoExitosoEntity.transactionId ?? ""
        
        self.model.postLiquidacion(
            solicitudId: solicitudId,
            usuario: usuario,
            detalle: detalle,
            monto: monto,
            terminal: terminal,
            traceNumber: traceNumber,
            eciDescription: eciDescription,
            signature: signature,
            card: card,
            merchant: merchant,
            status: status,
            actionDescription: actionDescription,
            idUnico: idUnico,
            amount: amount,
            authorizationCode: authorizationCode,
            transactionDate: transactionDate,
            actionCode: actionCode,
            eci: eci,
            brand: brand,
            adquirente: adquirente,
            processCode: processCode,
            transactionId: transactionId,
            recurrenceStatus: "",
            recurrenceMaxAmount: "") { (liquidacionResponse) in
                self.controller?.loaderView(isVisible: false)
        }
    }
    
    func generarBoleta() {
        self.controller?.loaderView(isVisible: true)
          
        let usuario = self.controller?.usuario ?? ""
        let costo = self.controller?.costo ?? ""
        let ip = self.controller?.ip ?? ""
        let codZona = self.controller?.codZona ?? ""
        let codOficina = self.controller?.codOficina ?? ""
        let placa = self.controller?.placa ?? ""
        let codAccion = self.controller?.codAccion ?? ""
        let codAutoriza = self.controller?.codAutoriza ?? ""
        let codtienda = self.controller?.codtienda ?? ""
        let concepto = self.controller?.concepto ?? ""
        let decisionCs = self.controller?.decisionCs ?? ""
        let dscCodAccion = self.controller?.dscCodAccion ?? ""
        let dscEci = self.controller?.dscEci ?? ""
        let eci = self.controller?.eci ?? ""
        let estado = self.controller?.estado ?? ""
        let eticket = self.controller?.eticket ?? ""
        let fechaYhoraTx = self.controller?.fechaYhoraTx ?? ""
        let idUnico = self.controller?.idUnico ?? ""
        let idUser = self.controller?.idUser ?? ""
        let impAutorizado = self.controller?.impAutorizado ?? ""
        let nomEmisor = self.controller?.nomEmisor ?? ""
        let numOrden = self.controller?.numOrden ?? ""
        let numReferencia = self.controller?.numReferencia ?? ""
        let oriTarjeta = self.controller?.oriTarjeta ?? ""
        let pan = self.controller?.pan ?? ""
        let resCvv2 = self.controller?.resCvv2 ?? ""
        let respuesta = self.controller?.respuesta ?? ""
        let reviewTransaction = self.controller?.reviewTransaction ?? ""
        let transId1 = self.controller?.transId ?? ""
        
        self.vehicularModel.postGenerarBoleta(usuario: usuario, costo: costo, ip: ip, codZona: codZona, codOficina: codOficina, placa: placa, codAccion: codAccion, codAutoriza: codAutoriza, codtienda: codtienda, concepto: concepto, decisionCs: decisionCs, dscCodAccion: dscCodAccion, dscEci: dscEci, eci: eci, estado: estado, eticket: eticket, fechaYhoraTx: fechaYhoraTx, idUnico: idUnico, idUser: idUser, impAutorizado: impAutorizado, nomEmisor: nomEmisor, numOrden: numOrden, numReferencia: numReferencia, oriTarjeta: oriTarjeta, pan: pan, resCvv2: resCvv2, respuesta: respuesta, reviewTransaction: reviewTransaction, transId: transId1) { (objBoleta) in
            self.controller?.initTransId(transId: objBoleta.transId)
                        
        }
        
    }
    func irBoleta() {
        self.controller?.loaderView(isVisible: true)
        
        let usuario = self.controller?.usuario ?? ""
        let costo = self.controller?.costo ?? ""
        let ip = self.controller?.ip ?? ""
        let codZona = self.controller?.codZona ?? ""
        let codOficina = self.controller?.codOficina ?? ""
        let placa = self.controller?.placa ?? ""
        let codAccion = self.controller?.codAccion ?? ""
        let codAutoriza = self.controller?.codAutoriza ?? ""
        let codtienda = self.controller?.codtienda ?? ""
        let concepto = self.controller?.concepto ?? ""
        let decisionCs = self.controller?.decisionCs ?? ""
        let dscCodAccion = self.controller?.dscCodAccion ?? ""
        let dscEci = self.controller?.dscEci ?? ""
        let eci = self.controller?.eci ?? ""
        let estado = self.controller?.estado ?? ""
        let eticket = self.controller?.eticket ?? ""
        let fechaYhoraTx = self.controller?.fechaYhoraTx ?? ""
        let idUnico = self.controller?.idUnico ?? ""
        let idUser = self.controller?.idUser ?? ""
        let impAutorizado = self.controller?.impAutorizado ?? ""
        let nomEmisor = self.controller?.nomEmisor ?? ""
        let numOrden = self.controller?.numOrden ?? ""
        let numReferencia = self.controller?.numReferencia ?? ""
        let oriTarjeta = self.controller?.oriTarjeta ?? ""
        let pan = self.controller?.pan ?? ""
        let resCvv2 = self.controller?.resCvv2 ?? ""
        let respuesta = self.controller?.respuesta ?? ""
        let reviewTransaction = self.controller?.reviewTransaction ?? ""
        let transId = self.controller?.transId ?? ""
        
        self.vehicularModel.postGenerarBoleta(usuario: usuario, costo: costo, ip: ip, codZona: codZona, codOficina: codOficina, placa: placa, codAccion: codAccion, codAutoriza: codAutoriza, codtienda: codtienda, concepto: concepto, decisionCs: decisionCs, dscCodAccion: dscCodAccion, dscEci: dscEci, eci: eci, estado: estado, eticket: eticket, fechaYhoraTx: fechaYhoraTx, idUnico: idUnico, idUser: idUser, impAutorizado: impAutorizado, nomEmisor: nomEmisor, numOrden: numOrden, numReferencia: numReferencia, oriTarjeta: oriTarjeta, pan: pan, resCvv2: resCvv2, respuesta: respuesta, reviewTransaction: reviewTransaction, transId: transId) { objBoleta in
            
            self.controller?.loaderView(isVisible: false)
            let state = !transId.isEmpty
            self.controller?.loadBoletaView(state, message: objBoleta.msgResult, transId: objBoleta.transId)
        }
    }
}
