//
//  PagoExitosoViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import UIKit

class PagoExitosoViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var servicioLabel: UILabel!
    @IBOutlet weak var numeroTarjetaLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var fechaHoraLabel: UILabel!
    @IBOutlet weak var montoLabel: UILabel!
    @IBOutlet weak var monedaLabel: UILabel!
    @IBOutlet weak var tipoTarjetaLabel: UILabel!
    @IBOutlet weak var firstButtonView: UIView!
    @IBOutlet weak var secondButtonView: UIView!
    @IBOutlet weak var firstButtonLabel: UILabel!
    @IBOutlet weak var secondButtonLabel: UILabel!
    @IBOutlet weak var heightForm: NSLayoutConstraint!
    @IBOutlet weak var numeroPedidoLabel: UILabel!
    
    
    var loading: UIAlertController!
    var pagoExitosoEntity: PagoExitosoEntity!
    var transactionData: TransactionData?
    var solicitudId: Int = 0
    var detalle: String = ""
    var monto: String = ""
    var codZonas: [String] = []
    var apPaterno: String = ""
    var apMaterno: String = ""
    var nombre: String = ""
    var razonSocial: String = ""
    var tipoTitular: String = ""
    var derecho: String = ""
    var sociedad: String = ""
    var codAreaRegistral: String = ""
    var desAreaRegistral: String = ""
    var controller: Controller!
    
    var usuario: String = ""
    var costo: String = ""
    var ip: String = ""
    var codZona: String = ""
    var codOficina: String = ""
    var placa: String = ""
    var codAccion: String = ""
    var codAutoriza: String = ""
    var codtienda: String = ""
    var concepto: String = ""
    var decisionCs: String = ""
    var dscCodAccion: String = ""
    var dscEci: String = ""
    var eci: String = ""
    var estado: String = ""
    var eticket: String = ""
    var fechaYhoraTx: String = ""
    var idUnico: String = ""
    var idUser: String = ""
    var impAutorizado: String = ""
    var nomEmisor: String = ""
    var numOrden: String = ""
    var numReferencia: String = ""
    var oriTarjeta: String = ""
    var pan: String = ""
    var resCvv2: String = ""
    var respuesta: String = ""
    var reviewTransaction: String = ""
    var transId: String = ""
    var certificateType:String?
    var cuentaGen: Int = 0
    
    private lazy var presenter: PagoExitosoPresenter = {
       return PagoExitosoPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        addGestureView()
        setValues()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cuentaGen += 1;
        print ("cuentaGen = ", cuentaGen)
        print(self.controller == .consultaVehicular)
        if cuentaGen == 1 {
            if (self.controller == .liquidacion) {
                presenter.willAppear()
            } else if (self.controller == .busquedaNombre ) {
                self.transId = ""
                //self.loaderView(isVisible: true)
                self.firstButtonView.isHidden = false
                //presenter.generarBoleta()
            } else if (self.controller == .consultaVehicular) {
                self.transId = ""
                self.loaderView(isVisible: true)
                self.firstButtonView.isHidden = true
                presenter.generarBoleta()
                
            }
        }
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        if (self.controller == .liquidacion) {
            loadNavigationBar(hideNavigation: false, title: "Pago de liquidación")
        } else if (self.controller == .busquedaNombre) {
            loadNavigationBar(hideNavigation: false, title: "Búsqueda por Nombre")
        } else if (self.controller == .consultaVehicular) {
            loadNavigationBar(hideNavigation: false, title: "Boleta vehicular")
        }
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.onTapFinalizarFromServiceView()
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    private func setupDesigner() {
        formView.backgroundCard()
        self.secondButtonView.isHidden = true
        self.heightForm.constant = 510
        
        if (self.controller == .liquidacion) {
            self.servicioLabel.text = "Boleta informativa"
            self.descripcionLabel.text = "Certificación Registral Vehicular-SPRN"
            self.certificateType = "Boleta informativa"
            self.firstButtonView.primaryButton()
            self.firstButtonLabel.text = "Finalizar"
        } else if (self.controller == .busquedaNombre) {
            self.servicioLabel.text = "Búsqueda de datos por Nombre"
            self.certificateType = "Búsqueda por Nombre"
            self.descripcionLabel.text = "Búsqueda por Nombre - \(self.desAreaRegistral)"
            self.firstButtonView.primaryButton()
            self.firstButtonLabel.text = "Ver datos"
        } else if (self.controller == .consultaVehicular) {
            self.servicioLabel.text = "Boleta informativa"
            self.heightForm.constant = 567
            self.secondButtonView.isHidden = false
            self.firstButtonView.primaryButton()
            self.secondButtonView.secondaryButton()
            self.firstButtonLabel.text = "Ver boleta"
            self.certificateType = "Boleta Informativa"
            self.secondButtonLabel.text = "Terminar"
        }
    }
    
    func setValues() {
        
        if let item = transactionData?.dataMap {
            dump(item)
            self.numeroTarjetaLabel.text = item.card
            if self.controller == .busquedaNombre {
                self.nombreLabel.text = "\(UserPreferencesController.usuario().nombres) \(UserPreferencesController.usuario().priApe) \(UserPreferencesController.usuario().segApe)"
            }
            else {
                self.nombreLabel.text = "\(self.nombre) \(self.apPaterno) \(self.apMaterno)"
            }
            self.descripcionLabel.text = certificateType
            self.fechaHoraLabel.text = self.changeDate()
            self.montoLabel.text = "S/ \(item.amount)"
            self.monedaLabel.text = "Soles"
            
            self.tipoTarjetaLabel.text = (item.cardType == "D") ? "Tarjeta de débito" : "Tarjeta de crébito"
            self.numeroPedidoLabel.text = item.purchaseNumber
        } else
            if let item = pagoExitosoEntity {
                dump(item)
                self.numeroTarjetaLabel.text = item.card
                if self.controller == .busquedaNombre {
                    self.nombreLabel.text = "\(UserPreferencesController.usuario().nombres) \(UserPreferencesController.usuario().priApe) \(UserPreferencesController.usuario().segApe)"
                }
                else {
                    self.nombreLabel.text = "\(self.nombre) \(self.apPaterno) \(self.apMaterno)"
                }
                self.descripcionLabel.text = certificateType
                self.fechaHoraLabel.text = self.changeDate()
                self.montoLabel.text = "S/ \(item.amount)"
                self.monedaLabel.text = "Soles"
                
                self.tipoTarjetaLabel.text = (item.cardType == "D") ? "Tarjeta de débito" : "Tarjeta de crébito"
                self.numeroPedidoLabel.text = item.purchaseNumber
            }
            else {
                print("No tiene data")
            }
        
        
    }
    
    private func addGestureView(){
                
        if (self.controller == .liquidacion) {
            let tapFinalizarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapFinalizarFromHistoryTransactionView))
            self.firstButtonView.addGestureRecognizer(tapFinalizarGesture)
        } else if (self.controller == .busquedaNombre) {
            let tapDatosGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapDatosBusquedaView))
            self.firstButtonView.addGestureRecognizer(tapDatosGesture)
        } else if (self.controller == .consultaVehicular) {
            let tapVerBoletaGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapVerBoletaView))
            self.firstButtonView.addGestureRecognizer(tapVerBoletaGesture)
            let tapFinalizarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapFinalizarFromServiceView))
            self.secondButtonView.addGestureRecognizer(tapFinalizarGesture)
        }
        
    }
    
    @objc private func onTapFinalizarFromServiceView(){
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiciosViewController") as! ServiciosViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
    
    @objc private func onTapFinalizarFromHistoryTransactionView(){
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HistoryTransactionViewController") as! HistoryTransactionViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
    
    @objc private func onTapDatosBusquedaView(){
        let storyboard = UIStoryboard(name: "BusquedaNombre", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DatosBusquedaViewController") as! DatosBusquedaViewController
        vc.pagoExitosoEntity = self.pagoExitosoEntity
        vc.codZonas = self.codZonas
        vc.apPaterno = self.apPaterno
        vc.apMaterno = self.apMaterno
        vc.nombre = self.nombre
        vc.razonSocial = self.razonSocial
        vc.tipoTitular = self.tipoTitular
        vc.derecho = self.derecho
        vc.sociedad = self.sociedad
        vc.codAreaRegistral = self.codAreaRegistral
        vc.desAreaRegistral = self.desAreaRegistral
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
        
    @objc private func onTapVerBoletaView(){
        
        loadBoletaView(_:true, message:"Boleta Informativa", transId: self.transId)

    }
    
    func loaderView(isVisible: Bool) {
        print("loaderView")
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func initTransId(transId: String) {
        self.transId = transId
        print("transId = ", self.transId )
        self.firstButtonView.isHidden = false
        self.loaderView(isVisible: false)
    }
    
    
    
    func changeDate() -> String {
        var newDate = ""
        if (self.transactionData != nil) {
            let dateTx = self.transactionData?.dataMap.transactionDate ?? ""
            if (dateTx.count == 12) {
                let fecha = dateTx.prefix(6)
                let hora = dateTx.suffix(6)
                
                let startFecha = fecha.index(fecha.startIndex, offsetBy: 2)
                let endFecha = fecha.index(fecha.endIndex, offsetBy: -2)
                let rangeFecha = startFecha..<endFecha
                
                let startHora = hora.index(hora.startIndex, offsetBy: 2)
                let endHora = hora.index(hora.endIndex, offsetBy: -2)
                let rangeHora = startHora..<endHora
                
                newDate = "\(fecha.suffix(2))/\(fecha[rangeFecha])/20\(fecha.prefix(2)) \(hora.prefix(2)):\(hora[rangeHora]):\(hora.suffix(2))"
            } else {
                newDate = dateTx
            }
        }
        return newDate
    }
        
    func loadBoletaView(_ state: Bool, message: String, transId: String) {
        
        print("loadBoletaView: ", transId)
        if (state) {
            let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DocumentoBoletaViewController") as! DocumentoBoletaViewController
            vc.documentKey = Int(transId)
            vc.typeDocument = .boleta
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
}

extension PagoExitosoViewController {
    
    enum Controller: String {
        case liquidacion    = "liquidacion"
        case busquedaNombre   = "busquedaNombre"
        case consultaVehicular   = "consultaVehicular"
    }
    
}
