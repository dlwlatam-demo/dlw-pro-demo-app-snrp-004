//
//  AclarationCertificationPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/08/22.
//

import Foundation

class AclarationCertificationPresenter {
    
    private weak var controller: AclarationCertificationViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: AclarationCertificationViewController) {
        self.controller = controller
    }
    
}

extension AclarationCertificationPresenter: GenericPresenter {
 
    func postAclaramiento(solicitudId: String, detalle: String, usuario: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.postAclaramiento(solicitudId: solicitudId, detalle: detalle, usuario: usuario) { (objResponse) in
            self.controller?.loaderView(isVisible: false)
            let state = objResponse.codResult == "1"
            self.controller?.popBackViewController(state, message: objResponse.msgResult)
        }
    }
    
    func postSubsanacion(solicitudId: String, detalle: String, usuario: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.postSubsanacion(solicitudId: solicitudId, detalle: detalle, usuario: usuario) { (objResponse) in
            self.controller?.loaderView(isVisible: false)
            let state = objResponse.codResult == "1"
            self.controller?.popBackViewController(state, message: objResponse.msgResult)
        }
    }
    
}
