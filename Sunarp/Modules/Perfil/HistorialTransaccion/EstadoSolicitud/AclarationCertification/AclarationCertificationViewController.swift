//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class AclarationCertificationViewController: UIViewController {
        
    var loading: UIAlertController!  
    let disposeBag = DisposeBag()
    
    var window: UIWindow?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var formViewAclaration: UIView!
    @IBOutlet weak var formViewDocument: UIView!
         
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var obserTextField: UITextField!
    
    var isAclarimiento: Bool = false
    var solicitudId: String = ""
    var userKeyId: String = ""
    
    private lazy var presenter: AclarationCertificationPresenter = {
        return AclarationCertificationPresenter(controller: self)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        bannerHome()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        if (isAclarimiento) {
            loadNavigationBar(hideNavigation: false, title: "Aclaración de certificado")
        } else {
            loadNavigationBar(hideNavigation: false, title: "Subsanación del Certificado")            
        }
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    func bannerHome(){
       // self.confirmDialog(title: "",message: "",primaryButton: "",secondaryButton: "X",delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        
        self.formView.backgroundCard()
        obserTextField.border()
        obserTextField.setPadding(left: CGFloat(8), right: CGFloat(8))
        
        if (self.isAclarimiento) {
            self.titleLabel.text = "Ingresa la información de solicitud"
        } else {
            self.titleLabel.text = "Añade información"
        }
    }
    
    private func addGestureView(){
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
    }
    
    
    @objc private func onTapToConfirmView() {
        let detalle = self.obserTextField.text ?? ""
        if (self.isAclarimiento) {
            self.presenter.postAclaramiento(solicitudId: self.solicitudId, detalle: detalle, usuario: self.userKeyId)
        } else {
            self.presenter.postSubsanacion(solicitudId: self.solicitudId, detalle: detalle, usuario: self.userKeyId)
        }
    }
        
    func popBackViewController(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for vc in viewControllers {
                        if vc is HistoryTransactionViewController {
                            UserPreferencesController.setReloadData(reloadData: true)
                            self.navigationController!.popToViewController(vc, animated: true)
                        }
                    }
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
        
}


