//
//  DocumentoBoletaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/09/22.
//

import UIKit
import QuickLook

class DocumentoBoletaViewController: UIViewController {
    
    @IBOutlet weak var documentPdfView: UIView!
    @IBOutlet weak var documentPdfLabel: UILabel!
    @IBOutlet weak var documentPdfShareImage: UIImageView!
    
    var loading: UIAlertController!
    var typeDocument: TypeDocumenPdf = .documento
    var documentKey: Int?
    var base64PDF : String = ""
    var fileNamePDF : String = ""
    var previewItem : URL?
    
    private lazy var presenter: DocumentoBoletaPresenter = {
        return DocumentoBoletaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        if (typeDocument == .documento) {
            self.presenter.getDocument()
        } else if (typeDocument == .boleta) {
            self.presenter.getBoleta()
        }
    }
        
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Vista de \(String(describing: self.typeDocument.rawValue))")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadDocument(documento: String, fileName: String) {
        self.documentPdfLabel.text = fileName
        if (!documento.isEmpty) {
            self.base64PDF = documento
            self.fileNamePDF = fileName
            self.viewPDF(newDir: "Documents")
            
            let onTapShare = UITapGestureRecognizer(target: self, action: #selector(documentSharePdf))
            self.documentPdfShareImage.addGestureRecognizer(onTapShare)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.loaderView(isVisible: false)
            }
        }
    }
    
    @objc private func documentSharePdf() {
        if let data = Data(base64Encoded: self.base64PDF) {
            self.setupSharePdf(with: data, name: self.fileNamePDF)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
}

extension DocumentoBoletaViewController{
        
    func viewPDF(newDir: String){
        
        if let data = Data(base64Encoded: self.base64PDF) {
            let pdfView = PDFView(frame: self.documentPdfView.bounds)
            pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.documentPdfView.addSubview(pdfView)
            pdfView.autoScales = true
            if let pdfDocument = PDFDocument(data: data) {
                pdfView.document = pdfDocument
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func setupSharePdf(with pdfData : Data , name: String){
        let fileName = name
        let tmpURL = FileManager.default.temporaryDirectory.appendingPathComponent(fileName)
        do {
            try pdfData.write(to: tmpURL)
            self.sharePDF(url: tmpURL)
        } catch {
            print(error)
        }
    }
    
     func sharePDF(url: URL){
        var files = [Any]()
        files.append(url)
        let activityViewController = UIActivityViewController(activityItems: files, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension DocumentoBoletaViewController : QLPreviewControllerDataSource {

    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem! as QLPreviewItem
    }

}

extension DocumentoBoletaViewController {
    
    enum TypeDocumenPdf: String {
        case documento = "Documento"
        case boleta = "Boleta"
    }
    
}
