//
//  DocumentoBoletaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 8/09/22.
//

import Foundation

class DocumentoBoletaPresenter {
    
    private weak var controller: DocumentoBoletaViewController?
    
    lazy private var model: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    init(controller: DocumentoBoletaViewController) {
        self.controller = controller
    }
    
}

extension DocumentoBoletaPresenter: GenericPresenter {
 
    func getDocument() {
        self.controller?.loaderView(isVisible: true)
        let id: String = String(controller?.documentKey ?? 0)
        self.model.getDocumento(solicitudId: id) { (objDocumento) in
            self.controller?.loadDocument(documento: objDocumento.documento, fileName: objDocumento.filename)
        }
    }
    
    func getBoleta() {
        self.controller?.loaderView(isVisible: true)
        let id: String = String(controller?.documentKey ?? 0)
        let fileName: String = "\(getActualTimeStamp()).pdf"
        print("id = ", id)
        self.model.getBoleta(transId: id) { (objBoleta) in
            self.controller?.loadDocument(documento: objBoleta.boleta, fileName: fileName)
        }
    }
    
    func getActualTimeStamp() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: date).replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ":", with: "").replacingOccurrences(of: " ", with: "")
    }
}
