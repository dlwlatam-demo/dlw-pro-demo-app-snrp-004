//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class EstadoSolicitudViewController: UIViewController {
        
    
    let disposeBag = DisposeBag()
    
    var window: UIWindow?
    
    var loading: UIAlertController!    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
        
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var contentStack: UIStackView!
    
    @IBOutlet weak var contentSubsanacionView: UIView!
    @IBOutlet weak var contentAclaracionView: UIView!
    @IBOutlet weak var contentDocumentoView: UIView!
    @IBOutlet weak var contentDesestimientoView: UIView!
    @IBOutlet weak var contentPagoView: UIView!
    
    @IBOutlet weak var subsanacionView: UIView!
    @IBOutlet weak var aclaracionView: UIView!
    @IBOutlet weak var documentoView: UIView!
    @IBOutlet weak var desestimientoView: UIView!
    @IBOutlet weak var pagoView: UIView!
    
    @IBOutlet weak var numeroSolicitudLabel: UILabel!
    @IBOutlet weak var numeroPublicidadLabel: UILabel!
    @IBOutlet weak var fechaPresentacionLabel: UILabel!
    @IBOutlet weak var tipoCertificadoLabel: UILabel!
    @IBOutlet weak var registroLabel: UILabel!
    @IBOutlet weak var oficinaLabel: UILabel!
    @IBOutlet weak var numeroAsientoLabel: UILabel!
    @IBOutlet weak var numeroPaginaLabel: UILabel!
    
    @IBOutlet weak var solicitanteTitleLabel: UILabel!
    @IBOutlet weak var solicitanteApellidosNombresLabel: UILabel!
    @IBOutlet weak var solicitanteTipoDocumentoLabel: UILabel!
    @IBOutlet weak var solicitanteNumeroDocumentoLabel: UILabel!
    @IBOutlet weak var solicitanteApellidosNombresView: UIView!
    @IBOutlet weak var solicitanteTipoDocumentoView: UIView!
    @IBOutlet weak var solicitanteNumeroDocumentoView: UIView!
    @IBOutlet weak var solicitanteApellidosNombresTitleLabel: UILabel!
    @IBOutlet weak var solicitanteTipoDocumentoTitleLabel: UILabel!
    @IBOutlet weak var solicitanteNumeroDocumentoTitleLabel: UILabel!
    
    @IBOutlet weak var destinatarioTitleLabel: UILabel!
    @IBOutlet weak var destinatarioFormaEnvioLabel: UILabel!
    @IBOutlet weak var destinatarioOficinaRegistralLabel: UILabel!
    @IBOutlet weak var destinatarioApellidosNombresLabel: UILabel!
    @IBOutlet weak var destinatarioDepartamentoLabel: UILabel!
    @IBOutlet weak var destinatarioProvinciaLabel: UILabel!
    @IBOutlet weak var destinatarioDistritoLabel: UILabel!
    @IBOutlet weak var destinatarioDireccionLabel: UILabel!
    @IBOutlet weak var destinatarioCodigoPostalLabel: UILabel!
    @IBOutlet weak var destinatarioFormaEnvioTitleLabel: UILabel!
    @IBOutlet weak var destinatarioFormaEnvioView: UIView!
    @IBOutlet weak var destinatarioOficinaRegistralTitleLabel: UILabel!
    @IBOutlet weak var destinatarioOficinaRegistralView: UIView!
    @IBOutlet weak var destinatarioApellidosNombresTitleLabel: UILabel!
    @IBOutlet weak var destinatarioApellidosNombresView: UIView!
    @IBOutlet weak var destinatarioDepartamentoTitleLabel: UILabel!
    @IBOutlet weak var destinatarioDepartamentoView: UIView!
    @IBOutlet weak var destinatarioProvinciaTitleLabel: UILabel!
    @IBOutlet weak var destinatarioProvinciaView: UIView!
    @IBOutlet weak var destinatarioDistritoTitleLabel: UILabel!
    @IBOutlet weak var destinatarioDistritoView: UIView!
    @IBOutlet weak var destinatarioDireccionTitleLabel: UILabel!
    @IBOutlet weak var destinatarioDireccionView: UIView!
    @IBOutlet weak var destinatarioCodigoPostalTitleLabel: UILabel!
    @IBOutlet weak var destinatarioCodigoPostalView: UIView!
        
    @IBOutlet weak var pagoTitleLabel: UILabel!
    @IBOutlet weak var pagoMontoLabel: UILabel!
    @IBOutlet weak var pagoFechaLabel: UILabel!
    @IBOutlet weak var pagoMayorDerechoLabel: UILabel!
    @IBOutlet weak var pagoFormaPagoLabel: UILabel!
    @IBOutlet weak var pagoMontoTitleLabel: UILabel!
    @IBOutlet weak var pagoMontoView: UIView!
    @IBOutlet weak var pagoFechaTitleLabel: UILabel!
    @IBOutlet weak var pagoFechaView: UIView!
    @IBOutlet weak var pagoMayorDerechoTitleLabel: UILabel!
    @IBOutlet weak var pagoMayorDerechoView: UIView!
    @IBOutlet weak var pagoFormaPagoTitleLabel: UILabel!
    @IBOutlet weak var pagoFormaPagoView: UIView!
    @IBOutlet weak var verOcultarDetalleView: UIView!
    @IBOutlet weak var verOcultaDetalleLabel: UILabel!
    
    @IBOutlet weak var contentInformationHeigaint: NSLayoutConstraint!
    
    @IBOutlet weak var contentInformationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackContentData: UIStackView!
    
    @IBOutlet weak var stackHeader: UIStackView!
    @IBOutlet weak var stackBody: UIStackView!
    @IBOutlet weak var stackBodyDestinatario: UIStackView!
    
    var arrayTipoDocumentos: [TipoDocumentoEntity]!
    var solicitudId: Int = 0
    var solicitudTipo: String = ""
    var solicitudDetail: HistoryDetailEntity!
    
    var isValidDocument: Bool = false
    
    
    private lazy var presenter: EstadoSolicitudPresenter = {
        return EstadoSolicitudPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        bannerHome()
        setupNavigationBar()
        //self.solicitudId = 289730
        if isValidDocument == true {
            loadDataSolicitud(solicitud: solicitudDetail)
        }else{
            presenter.didLoad()
        }
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Estado de la Solicitud")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func bannerHome(){
       // self.confirmDialog(title: "",message: "",primaryButton: "",secondaryButton: "X",delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.subsanacionView.backgroundCard()
        self.aclaracionView.backgroundCard()
        self.documentoView.backgroundCard()
        self.desestimientoView.backgroundCard()
        self.pagoView.backgroundCard()
        
        //self.contentInformationHeightConstraint.constant = CGFloat(340)
        self.stackBody.isHidden = true
        self.stackBodyDestinatario.isHidden = true
       /* self.showOrHiddenSolicitante(true)
        self.showOrHiddenDestinatario(true)
        self.showOrHiddenPago(true)*/
        self.verOcultaDetalleLabel.text = "Ver Detalle"
        
        self.headerView.backgroundColor = UIColor.colorFromHexString(obtainColorByType(tipo: self.solicitudTipo), withAlpha: 1.0)
        
       
    }
    
    private func addGestureView(){

        let tapAclaracionGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToAclarationView))
        self.aclaracionView.addGestureRecognizer(tapAclaracionGesture)
        
        let tapSubsanacionGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToSubsanacionView))
        self.subsanacionView.addGestureRecognizer(tapSubsanacionGesture)
        
        let tapDesisHistoryGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToDesistirView))
        self.desestimientoView.addGestureRecognizer(tapDesisHistoryGesture)
        
        let tapDocumentoGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToDocumentoView))
        self.documentoView.addGestureRecognizer(tapDocumentoGesture)
        
        let tapPagoLiquidacionGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToPagoView))
        self.pagoView.addGestureRecognizer(tapPagoLiquidacionGesture)
        
        let tapExpandedGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapExpandedView))
        self.verOcultarDetalleView.addGestureRecognizer(tapExpandedGesture)
        
    }
    
    
    @objc private func onTapToDesistirView(){
        self.alertPopupDialog(title: "Cuidado",message: "¿Está seguro de desistir de la solicitud?",primaryButton: "Aceptar",secondaryButton: "Cancelar", addColor: SunarpColors.red, delegate: self)
    }
    
    @objc private func onTapToAclarationView() {
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AclarationCertificationViewController") as! AclarationCertificationViewController
        vc.isAclarimiento = true
        vc.solicitudId = solicitudDetail.solicitudId
        vc.userKeyId = solicitudDetail.userKeyId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapToSubsanacionView() {
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AclarationCertificationViewController") as! AclarationCertificationViewController
        vc.isAclarimiento = false
        vc.solicitudId = solicitudDetail.solicitudId
        vc.userKeyId = solicitudDetail.userKeyId
        self.navigationController?.pushViewController(vc, animated: true)
    }
            
    func loadDataSolicitud(solicitud: HistoryDetailEntity) {
        self.solicitudDetail = solicitud
        self.numeroSolicitudLabel.text = solicitud.solicitudId
        self.numeroPublicidadLabel.text = solicitud.certificado.aaPubl + "-" + solicitud.certificado.nuPubl
        self.fechaPresentacionLabel.text = solicitud.pago.fecha
        self.tipoCertificadoLabel.text = solicitud.certificado.tpoCertificado
        self.registroLabel.text = solicitud.certificado.areaRegistral
        self.oficinaLabel.text = solicitud.certificado.oficina
        self.numeroAsientoLabel.text = solicitud.certificado.asiento
        self.numeroPaginaLabel.text = solicitud.certificado.numPaginas
        self.loadButtonsBy(solicitud: solicitud)
        self.titleLabel.text = "ESTADO: \(solicitud.estado.estado)"
        
        self.solicitanteApellidosNombresLabel.text = solicitud.solicitante.nombre
        self.solicitanteTipoDocumentoLabel.text = solicitud.solicitante.tpoDoc
        self.solicitanteNumeroDocumentoLabel.text = solicitud.solicitante.numDoc
        
        self.destinatarioFormaEnvioLabel.text = solicitud.destinatario.tpoEnvioDesc
        self.destinatarioOficinaRegistralLabel.text = solicitud.destinatario.oficina
        self.destinatarioApellidosNombresLabel.text = solicitud.destinatario.nombre
        self.destinatarioDepartamentoLabel.text = solicitud.destinatario.departamento
        self.destinatarioProvinciaLabel.text = solicitud.destinatario.provincia
        self.destinatarioDistritoLabel.text = solicitud.destinatario.distrito
        self.destinatarioDireccionLabel.text = solicitud.destinatario.direccion
        self.destinatarioCodigoPostalLabel.text = solicitud.destinatario.codPostal
        
        self.pagoMontoLabel.text = "S/ \(solicitud.pago.monto)"
        self.pagoFechaLabel.text = solicitud.pago.fecha
        self.pagoMayorDerechoLabel.text = solicitud.pago.mayorDerecho
        self.pagoFormaPagoLabel.text = solicitud.pago.tpoPago
                
    }
    
    @objc private func onTapToDocumentoView() {
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DocumentoBoletaViewController") as! DocumentoBoletaViewController
        vc.documentKey = self.solicitudId
        //vc.documentKey = 289948
        vc.typeDocument = .documento
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapToPagoView() {
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LiquidacionSolicitudViewController") as! LiquidacionSolicitudViewController
        vc.nsolMonLiq = solicitudDetail.nsolMonLiq
        vc.arrayTipoDocumentos = self.arrayTipoDocumentos
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapExpandedView() {
        
        UIView.animate(withDuration: 1, animations: {
            self.stackBody.isHidden = !self.stackBody.isHidden
            let destinatario = self.solicitudDetail.destinatario
            if destinatario.nombre.isEmpty || destinatario.nombre == "" {
                self.stackBodyDestinatario.isHidden = true
            }
            else {
                if !self.stackBody.isHidden {
                    self.stackBodyDestinatario.isHidden = false
                }
            }
           // self.contentInformationHeightConstraint.constant = CGFloat(200)
            //self.stackBody.isHidden = !self.stackBody.isHidden
            
           // self.verOcultaDetalleLabel.text = (!self.stackBody.isHidden = tr)
          if (self.contentInformationHeightConstraint.constant == CGFloat(350)) {
               self.contentInformationHeightConstraint.constant = CGFloat(0)
             //  self.stackContentData.isHidden = false
              /* self.showOrHiddenSolicitante(false)
                self.showOrHiddenDestinatario(false)
                self.showOrHiddenPago(false)*/
               self.verOcultaDetalleLabel.text = "Ver Detalle"
            } else {
              self.contentInformationHeightConstraint.constant = CGFloat(350)
               // self.stackContentData.isHidden = true
               /* self.showOrHiddenSolicitante(true)
                self.showOrHiddenDestinatario(true)
                self.showOrHiddenPago(true)*/
                self.verOcultaDetalleLabel.text = "Ocultar detalle"
             
            }
        })        
    }
    
    func loadButtonsBy(solicitud: HistoryDetailEntity) {
        let usuario = UserPreferencesController.usuario()
        if (!solicitud.userKeyId.isEmpty && usuario.userKeyId == solicitud.userKeyId) {
            self.contentStack.isHidden = false
            if (solicitud.estado.codEstado.elementsEqual("C") && !solicitud.flags.flgAban.elementsEqual("1") && !solicitud.flags.flgDesis.elementsEqual("1")) {
                self.contentDesestimientoView.isHidden = false
            }
            
            if (solicitud.flags.flgObs.elementsEqual("1") && (solicitud.flags.flgDesis.elementsEqual("0") || solicitud.flags.flgDesis.isEmpty)) {
                self.contentSubsanacionView.isHidden = false
            }
            
            if (!Constant.COD_COPIA_LITERAL.contains(solicitud.certificado.certificadoId)) {
                if (solicitud.estado.codEstado.elementsEqual("D") && (solicitud.flags.flgAban.elementsEqual("0") || solicitud.flags.flgAban.isEmpty)) {
                    self.contentAclaracionView.isHidden = false
                }
            }
            
            var montoLiq: Double = 0.0
            
            if (!solicitud.nsolMonLiq.isEmpty) {
                montoLiq = Double(solicitud.nsolMonLiq) ?? 0.0
            }
            
            if (solicitud.flags.flgLiq.elementsEqual("1") && solicitud.estado.codEstado.elementsEqual("1") || montoLiq > 0 && (solicitud.flags.flgDesis.elementsEqual("0") || solicitud.flags.flgAban.isEmpty)) {
                self.contentPagoView.isHidden = false
            }
            
            if (solicitud.estado.codEstado.elementsEqual("D") || solicitud.estado.codEstado.elementsEqual("L")) {
                self.contentDocumentoView.isHidden = false
            }
            
        } else {
            self.contentStack.isHidden = true
        }
    }
    
    func obtainColorByType(tipo: String) -> String {
        switch (tipo) {
        case "1": return "#00a5a5"
        case "2": return "#b2b2b2"
        case "3": return "#ffaf00"
        default: return ""
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
        
    func popBackViewController(_ state: Bool, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for vc in viewControllers {
                        if vc is HistoryTransactionViewController {
                            UserPreferencesController.setReloadData(reloadData: true)
                            self.navigationController!.popToViewController(vc, animated: true)
                        }
                    }
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func showOrHiddenSolicitante(_ isHidden: Bool) {
        self.solicitanteTitleLabel.isHidden = isHidden
        self.solicitanteApellidosNombresLabel.isHidden = isHidden
        self.solicitanteTipoDocumentoLabel.isHidden = isHidden
        self.solicitanteNumeroDocumentoLabel.isHidden = isHidden
        self.solicitanteApellidosNombresView.isHidden = isHidden
        self.solicitanteTipoDocumentoView.isHidden = isHidden
        self.solicitanteNumeroDocumentoView.isHidden = isHidden
        self.solicitanteApellidosNombresTitleLabel.isHidden = isHidden
        self.solicitanteTipoDocumentoTitleLabel.isHidden = isHidden
        self.solicitanteNumeroDocumentoTitleLabel.isHidden = isHidden
    }
    
    func showOrHiddenDestinatario(_ isHidden: Bool) {
        self.destinatarioTitleLabel.isHidden = isHidden
        self.destinatarioFormaEnvioLabel.isHidden = isHidden
        self.destinatarioOficinaRegistralLabel.isHidden = isHidden
        self.destinatarioApellidosNombresLabel.isHidden = isHidden
        self.destinatarioDepartamentoLabel.isHidden = isHidden
        self.destinatarioProvinciaLabel.isHidden = isHidden
        self.destinatarioDistritoLabel.isHidden = isHidden
        self.destinatarioDireccionLabel.isHidden = isHidden
        self.destinatarioCodigoPostalLabel.isHidden = isHidden
        self.destinatarioFormaEnvioTitleLabel.isHidden = isHidden
        self.destinatarioFormaEnvioView.isHidden = isHidden
        self.destinatarioOficinaRegistralTitleLabel.isHidden = isHidden
        self.destinatarioOficinaRegistralView.isHidden = isHidden
        self.destinatarioApellidosNombresTitleLabel.isHidden = isHidden
        self.destinatarioApellidosNombresView.isHidden = isHidden
        self.destinatarioDepartamentoTitleLabel.isHidden = isHidden
        self.destinatarioDepartamentoView.isHidden = isHidden
        self.destinatarioProvinciaTitleLabel.isHidden = isHidden
        self.destinatarioProvinciaView.isHidden = isHidden
        self.destinatarioDistritoTitleLabel.isHidden = isHidden
        self.destinatarioDistritoView.isHidden = isHidden
        self.destinatarioDireccionTitleLabel.isHidden = isHidden
        self.destinatarioDireccionView.isHidden = isHidden
        self.destinatarioCodigoPostalTitleLabel.isHidden = isHidden
        self.destinatarioCodigoPostalView.isHidden = isHidden
    }
    
    func showOrHiddenPago(_ isHidden: Bool) {
        self.pagoTitleLabel.isHidden = isHidden
        self.pagoMontoLabel.isHidden = isHidden
        self.pagoFechaLabel.isHidden = isHidden
        self.pagoMayorDerechoLabel.isHidden = isHidden
        self.pagoFormaPagoLabel.isHidden = isHidden
        self.pagoMontoTitleLabel.isHidden = isHidden
        self.pagoMontoView.isHidden = isHidden
        self.pagoFechaTitleLabel.isHidden = isHidden
        self.pagoFechaView.isHidden = isHidden
        self.pagoMayorDerechoTitleLabel.isHidden = isHidden
        self.pagoMayorDerechoView.isHidden = isHidden
        self.pagoFormaPagoTitleLabel.isHidden = isHidden
        self.pagoFormaPagoView.isHidden = isHidden
    }
    
}


extension EstadoSolicitudViewController: ErrorDesistirDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
            self.presenter.postDesestimiento(solicitudId: self.solicitudDetail.solicitudId, detalle: "", usuario: self.solicitudDetail.userKeyId)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
    }
}
