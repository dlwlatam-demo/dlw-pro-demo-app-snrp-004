//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class PerfilViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var window: UIWindow?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var namesLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var dateOfBirthdayLabel: UILabel!
    @IBOutlet weak var documentNumberLabel: UILabel!
    @IBOutlet weak var numberPhoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var btnNotification: UIButton!
    
    @IBOutlet weak var formViewEditar: UIView!
    @IBOutlet weak var formViewHist: UIView!
    
    var loading: UIAlertController!
    var usuarioLoginEntity: UsuarioLoginEntity!
    var arrayTipoDocumentos: [TipoDocumentoEntity]!
    
    var boolSatus: Bool = false
    
    private lazy var presenter: PerfilPresenter = {
        return PerfilPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        bannerHome()
        
    }
    
    @IBAction func goBtnNotification(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Notification", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    func bannerHome(){
        // self.confirmDialog(title: "",message: "",primaryButton: "",secondaryButton: "X",delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
       // presenter.willAppear()
        presenter.willAppearLogin()
        self.loaderView(isVisible: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            self.loaderView(isVisible: false)
            self.presenter.willAppear()
        }
    }
    
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        self.formView.backgroundCard()
        self.formViewEditar.backgroundCard()
        self.formViewHist.backgroundCard()
        self.photoImage.backgroundCard()
    }
    
    private func addGestureView(){
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.formViewEditar.addGestureRecognizer(tapConfirmGesture)
        let tapConfirmHistoryGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmHistoryView))
        self.formViewHist.addGestureRecognizer(tapConfirmHistoryGesture)
    }
    
    
    @objc private func onTapToConfirmView() {
        
        let storyboard = UIStoryboard(name: "Perfil", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetallePerfilViewController") as! DetallePerfilViewController
        vc.usuarioLoginEntity = self.usuarioLoginEntity
        vc.arrayTipoDocumentos = self.arrayTipoDocumentos
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func onTapToConfirmHistoryView() {
        UserPreferencesController.setReloadData(reloadData: false)
        let storyboard = UIStoryboard(name: "HistoryTransaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HistoryTransactionViewController") as! HistoryTransactionViewController
        vc.arrayTipoDocumentos = self.arrayTipoDocumentos
        self.loading?.dismiss(animated: false)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func loadDataPerfil(usuarioLogin: UsuarioLoginEntity, arrayTipoDocumentos: [TipoDocumentoEntity]) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.9) {
          
        self.usuarioLoginEntity = usuarioLogin
        self.arrayTipoDocumentos = arrayTipoDocumentos
        
        let sexo = usuarioLogin.sexo
        var sexoDescription = ""
        let numeroDocumento = usuarioLogin.nroDoc
        let numeroCelular = usuarioLogin.nroCelular
        let email = usuarioLogin.email
        let fechaNacimientoInicial = usuarioLogin.fecNac
        let fechaNacimiento = self.formatDate(fechaNacimientoInicial) ?? ""
        
        
        
            print("sexo:>>>>:>>",sexo)
        if (sexo == "M") {
            sexoDescription = "Masculino"
        } else if (sexo == "F") {
            sexoDescription = "Femenino"
        } else {
            sexoDescription = "Prefiero no declararlo"
        }
        
        let tipoDocumento = arrayTipoDocumentos.filter { $0.tipoDocId == usuarioLogin.tipoDoc }
        
        let defaultAttributes = [
            .font: UIFont.systemFont(ofSize: 11, weight: .regular),
            .foregroundColor: UtilHelper.getUIColor(hex: "#323c37")
        ] as [NSAttributedString.Key : Any]
        
        let marketingAttributes = [
            .font: UIFont.systemFont(ofSize: 11, weight: .bold),
            .foregroundColor: UtilHelper.getUIColor(hex: "#323c37")
        ] as [NSAttributedString.Key : Any]
        
        let attributedStringSexo = [
            NSAttributedString(string: "Sexo: ",
                               attributes: marketingAttributes),
            sexoDescription
        ] as [AttributedStringComponent]
        
        let attributedStringDni = [
            NSAttributedString(string: tipoDocumento.first?.nombreAbrev ?? "",
                               attributes: marketingAttributes),
            ": " + numeroDocumento
        ] as [AttributedStringComponent]
        
        let attributedStringCelular = [
            NSAttributedString(string: "Nro Celular: ",
                               attributes: marketingAttributes),
            numeroCelular
        ] as [AttributedStringComponent]
        
        let attributedStringEmail = [
            NSAttributedString(string: "Email: ",
                               attributes: marketingAttributes),
            email
        ] as [AttributedStringComponent]
        
        let attributedStringFechaNacimiento = [
            NSAttributedString(string: "Fecha de nacimiento: ",
                               attributes: marketingAttributes),
            fechaNacimiento
        ] as [AttributedStringComponent]
        
        self.namesLabel.text = "\(usuarioLogin.nombres) \(usuarioLogin.priApe)"
        
        self.sexLabel.attributedText = NSAttributedString(from: attributedStringSexo, defaultAttributes: defaultAttributes)
        self.sexLabel.textAlignment = .center
        
        self.dateOfBirthdayLabel.attributedText = NSAttributedString(from: attributedStringFechaNacimiento, defaultAttributes: defaultAttributes)
        self.dateOfBirthdayLabel.textAlignment = .center
        
        self.documentNumberLabel.attributedText = NSAttributedString(from: attributedStringDni, defaultAttributes: defaultAttributes)
        self.documentNumberLabel.textAlignment = .center
        
        self.numberPhoneLabel.attributedText = NSAttributedString(from: attributedStringCelular, defaultAttributes: defaultAttributes)
        self.numberPhoneLabel.textAlignment = .center
        
        self.emailLabel.attributedText = NSAttributedString(from: attributedStringEmail, defaultAttributes: defaultAttributes)
        self.emailLabel.textAlignment = .center
        
          
        let str = usuarioLogin.userPhoto
            self.photoImage.image = self.convertBase64StringToImage(imageBase64String: str)
        
            self.loaderView(isVisible: false)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
            
          let str = usuarioLogin.userPhoto
            self.photoImage.image = self.convertBase64StringToImage(imageBase64String: str)
        
        }
       
    }
    
    func formatDate(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy/MM/dd"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "dd/MM/yyyy"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    @IBAction func onTapMenu(_ sender: Any) {
        let drawerController = MenuDrawerViewController(delegate: self)
        present(drawerController, animated: true)
    }
    
}
/*
extension PerfilViewController {
    
    
}
*/
protocol AttributedStringComponent {
    var text: String { get }
    func getAttributes() -> [NSAttributedString.Key: Any]?
}

// MARK: String extensions

extension String: AttributedStringComponent {
    var text: String { self }
    func getAttributes() -> [NSAttributedString.Key: Any]? { return nil }
}

extension String {
    func toAttributed(with attributes: [NSAttributedString.Key: Any]?) -> NSAttributedString {
        .init(string: self, attributes: attributes)
    }
}

// MARK: NSAttributedString extensions

extension NSAttributedString: AttributedStringComponent {
    var text: String { string }
    
    func getAttributes() -> [Key: Any]? {
        if string.isEmpty { return nil }
        var range = NSRange(location: 0, length: string.count)
        return attributes(at: 0, effectiveRange: &range)
    }
}

extension NSAttributedString {
    
    convenience init?(from attributedStringComponents: [AttributedStringComponent],
                      defaultAttributes: [NSAttributedString.Key: Any],
                      joinedSeparator: String = " ") {
        switch attributedStringComponents.count {
        case 0: return nil
        default:
            var joinedString = ""
            typealias SttributedStringComponentDescriptor = ([NSAttributedString.Key: Any], NSRange)
            let sttributedStringComponents = attributedStringComponents.enumerated().flatMap { (index, component) -> [SttributedStringComponentDescriptor] in
                var components = [SttributedStringComponentDescriptor]()
                if index != 0 {
                    components.append((defaultAttributes,
                                       NSRange(location: joinedString.count, length: joinedSeparator.count)))
                    joinedString += joinedSeparator
                }
                components.append((component.getAttributes() ?? defaultAttributes,
                                   NSRange(location: joinedString.count, length: component.text.count)))
                joinedString += component.text
                return components
            }
            
            let attributedString = NSMutableAttributedString(string: joinedString)
            sttributedStringComponents.forEach { attributedString.addAttributes($0, range: $1) }
            self.init(attributedString: attributedString)
        }
    }
}

extension PerfilViewController: MenuDrawerDelegate {
    
    func menuDrawerLogoutSelected() {
        self.presenter.logout()
    }
    
    func goToLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(vc, animated: false)
        }
    }
    
}
