//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class AlertaPartidasPresenter {
    
    private weak var controller: AlertaPartidasViewController?
    
   
 
    lazy private var model: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
   
    
    init(controller: AlertaPartidasViewController) {
        self.controller = controller
    }
    
}

extension AlertaPartidasPresenter: GenericPresenter {
    
    func getListaPartdas() {
       
     //   self.controller?.loaderView(isVisible: true)
        self.model.getSelectAllPartidaFromUser{ (arrayPartidas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadPartidas(allPartidaResponse: arrayPartidas)
        }
        
    }
    func putDeletePartida(aaCont: String, nuCont: String) {
        self.model.putDeletePartida(aaCont: aaCont, nuCont: nuCont) { (objSolicitud) in
            // self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)
            if (state) {
             //   UserPreferencesController.setGuid(guid: objSolicitud.guid)
            }
            self.controller?.goToCodeValidationUpdatePartida(state, message: objSolicitud.msgResult)
        }
    }
    
    
    
}
