//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class SearchPartidaPresenter {
    
    private weak var controller: SearchPartidaViewController?
    
    lazy private var modelCons: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelAlert: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
 
    init(controller: SearchPartidaViewController) {
        self.controller = controller
    }
    
}

extension SearchPartidaPresenter: GenericPresenter {
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        self.modelDoc.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func didLoadLibroRegistral() {
        let areaRegId = controller?.areaRegId ?? ""
        self.modelDoc.getListaObtenerLibro(areaRegId: areaRegId) { (arrayTipoDocumentos) in
            self.controller?.loadObtenerLibro(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func didiListadoOficina() {
        self.consultaLiteralModel.getListaOficinaRegistral{ (arrayOficinas) in
            self.controller?.loadOficinas(oficinasResponse: arrayOficinas)
        }
    }
    
    
    
    
    func willAppear() {
        self.controller?.loaderView(isVisible: true)
        self.model.getAreasRegistrales{ (arrayArea) in
            self.controller?.loadAreas(areaResponse: arrayArea)
        }
    }
    
    func loadGrupos(codGrupoLibroArea: String) {
        self.controller?.loaderView(isVisible: true)
        self.model.getGrupoLibro(codGrupoLibroArea: codGrupoLibroArea){ (arrayGrupo) in
            self.controller?.loadGrupos(grupoResponse: arrayGrupo)
        }
    }
        
    
    func postSearchPartida(region: String,ofic: String, area: String, partida: String, ficha: String, tomo: String, foja: String, partiSarp: String, placa: String, tipo: String) {
        self.modelAlert.postSearchPartida(region: region,ofic: ofic, area: area, partida: partida, ficha: ficha, tomo: tomo, foja: foja, partiSarp: partiSarp, placa: placa, tipo: tipo) { (objSolicitud) in
            self.controller?.loadSearch(partidas:objSolicitud)
        }
    }
    
        
}
