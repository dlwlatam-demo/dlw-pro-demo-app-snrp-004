//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit

class SearchPartidaViewController: UIViewController {
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var SearchView: UIView!
    
    
    @IBOutlet weak var placaView: UIView!
    @IBOutlet weak var partidaView: UIView!
    @IBOutlet weak var placaText: UITextField!
    @IBOutlet weak var SearchPlacaView: UIView!

    
    var email: String = ""
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    
    var region: String = ""
    var ofic: String = ""
    var area: String = ""
    var partida: String = ""
    var ficha: String = ""
    var tomo: String = ""
    var foja: String = ""
    var partiSarp: String = ""
    var placa: String = ""
    var tipo: String = ""
    
    var maxLength = 0
    
    
    var areaId: String = ""
    var tipoId: String = ""
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoPropiedad: [String] = ["Propiedad Inmueble", "Persona Jurídica","Propiedad Vehicular"]
    var tipoPartidas: [String] = ["Partida", "Ficha", "Tomo", "Partida SARP"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoOperadorPickerView = UIPickerView()
    var tipoPartidaPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    
    var obtenerPartidasEntities: [SearchPartidaEntity] = []
    
    var regPubId: String = ""
    
    
    var oficRegId: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var tipoDocId: String = ""
    var namePropiedad: String = ""
    var areaPartidaId: String = ""
    
    
    var areaRegId: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    var titleBar: String = ""
    
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var precOfic: String = ""
    
    
    var idRgst: Int = 0
    var apPate: String = ""
    var apMate: String = ""
    var nombre: String = ""
    var tiDocu: String = ""
    var noDocu: String = ""
    var direccion: String = ""
    var cnumCel: Int = 0
    var idOperTele: String = ""
    var flEnvioSms: String = ""
    
    
    
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typePropiedadTextField: UITextField!
    @IBOutlet weak var typePartidaTextField: UITextField!
    @IBOutlet weak var numPartidaTextField: UITextField!
    
    @IBOutlet weak var numFolioTextField: UITextField!
    
    private lazy var presenter: SearchPartidaPresenter = {
        return SearchPartidaPresenter(controller: self)
    }()
    
    var indexTypeOficina = 0
    var indexPartida = 0
    var indexPropiedad = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        // setupKeyboard()
        // setValues()
        presenter.didLoad()
        // presenter.didLoadLibroRegistral()
        self.presenter.didiListadoOficina()
        flEnvioSms = "N"
        self.numFolioTextField.isHidden = true
        self.placaView.isHidden = true
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Seleccionar partida")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
        if returnValor == "return"{
            self.navigationController?.popViewController(animated: false)
            
            let defaults = UserDefaults.standard
            defaults.set("return3", forKey: "returnKey")
            
        }
        
        self.presenter.willAppear()
        
        
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        //  self.scrollview.contentInset = contentInsets
        //   self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        //  self.scrollview.contentInset = contentInsets
        // self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.SearchView.primaryButton()
        self.SearchPlacaView.primaryButton()
        
        //  self.SearchView.primaryDisabledButton()
        
        
        typeOficinaTextField.border()
        typeOficinaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoOperadorPickerView.tag = 1
        self.tipoOperadorPickerView.delegate = self
        self.tipoOperadorPickerView.dataSource = self
        self.typeOficinaTextField.inputView = self.tipoOperadorPickerView
        self.typeOficinaTextField.inputAccessoryView = createToolbarOficina()
        
        
        typePropiedadTextField.border()
        typePropiedadTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        self.tipoDocumentoPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typePropiedadTextField.inputView = self.tipoDocumentoPickerView
        self.typePropiedadTextField.inputAccessoryView = createToolbarGender()
        
        typePartidaTextField.border()
        typePartidaTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        self.tipoPartidaPickerView.tag = 3
        self.tipoPartidaPickerView.delegate = self
        self.tipoPartidaPickerView.dataSource = self
        self.typePartidaTextField.inputView = self.tipoPartidaPickerView
        self.typePartidaTextField.inputAccessoryView = createToolbarPartida()
        
        self.numPartidaTextField.borderAndPaddingLeftAndRight()
        self.numFolioTextField.borderAndPaddingLeftAndRight()
        self.placaText.borderAndPaddingLeftAndRight()
        self.placaText.delegate = self
        self.numPartidaTextField.delegate = self
        self.typePartidaTextField.delegate = self
        self.typePropiedadTextField.delegate = self
        self.typeOficinaTextField.delegate = self
        self.numFolioTextField.delegate = self
        
    }
    
    func createToolbarPartida() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedPartida))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    func createToolbarOficina() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedOficina))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    @objc func donePressedPartida() {
        self.changeTypePartida(index: indexPartida)
        self.typePartidaTextField.text = tipoPartidas[indexPartida]
        self.typePartidaTextField.resignFirstResponder()
    }
    
    @objc func donePressedOficina() {
        self.changeTypeOficina(index: indexTypeOficina)
        self.typeOficinaTextField.text = oficinas[indexTypeOficina].Nombre
        self.typeOficinaTextField.resignFirstResponder()
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchView))
        self.SearchView.addGestureRecognizer(tapPagarGesture)
        
        let tapPlacaGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchPlacaView))
        self.SearchPlacaView.addGestureRecognizer(tapPlacaGesture)
    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            flEnvioSms = "S"
        }
        self.isChecked = !self.isChecked
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
    }
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
        self.obtenerLibroEntities = arrayTipoDocumentos
    }
    
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
            typeDocument = "03"
        }
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numPartidaTextField.resignFirstResponder()
    }
    
    @objc private func onTapSearchView(){
        if (areaPartidaId=="Partida") {
            presenter.postSearchPartida(region: self.regPubId,ofic: self.oficRegId, area: areaId, partida: self.numPartidaTextField.text ?? "", ficha: ficha, tomo: tomo, foja: foja, partiSarp: partiSarp, placa: placa, tipo: tipoId)
            
        }else if (areaPartidaId=="Ficha") {
            presenter.postSearchPartida(region: self.regPubId,ofic: self.oficRegId, area: areaId, partida: "", ficha: self.numPartidaTextField.text ?? "", tomo: tomo, foja: foja, partiSarp: partiSarp, placa: placa, tipo: tipoId)
            
        }else if (areaPartidaId=="Tomo") {
            presenter.postSearchPartida(region: self.regPubId,ofic: self.oficRegId, area: areaId, partida:"", ficha:"", tomo: self.numPartidaTextField.text ?? "", foja: self.numFolioTextField.text ?? "", partiSarp: partiSarp, placa: placa, tipo: tipoId)
            
        }else if (areaPartidaId=="Partida SARP") {
            presenter.postSearchPartida(region: self.regPubId,ofic: self.oficRegId, area: areaId, partida:"", ficha:"", tomo: tomo, foja: foja, partiSarp: self.numPartidaTextField.text ?? "", placa: placa, tipo: tipoId)
        }
        
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressedPropiedad))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        return toolbar
    }
    
    @objc func donePressedPropiedad() {
        self.changeTypeDocument(index: indexPropiedad)
        self.typePropiedadTextField.text = tipoPropiedad[indexPropiedad]
        self.typePropiedadTextField.resignFirstResponder()
    }
    
    @objc private func onTapSearchPlacaView(){
        self.placaText.autocapitalizationType = .allCharacters
        placaText.text = placaText.text?.uppercased()
        tipoId = "V"
        presenter.postSearchPartida(region: self.regPubId,ofic: self.oficRegId, area: areaId, partida:"", ficha:"", tomo: tomo, foja: foja, partiSarp:"", placa: self.placaText.text ?? "", tipo: tipoId)
    }
    
    func goToCodeValidationUpdateUser(_ state: Bool, message: String) {
        if (state){
            goBack()
        }else{
            showMessageAlert(message: message)
        }
    }
    func loadSearch(partidas: [SearchPartidaEntity]) {
        
        if !partidas.isEmpty {
            self.obtenerPartidasEntities = partidas
            let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddPartidaViewController") as! AddPartidaViewController
            vc.obtenerPartidasEntities = partidas
            vc.regPubId = regPubId
            vc.oficRegId = oficRegId
            vc.areaId = areaId
            vc.numPlacaInput = self.placaText.text ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            showMessageAlert(message: "No se encontraron datos.")
        }
    }

    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func changeTypeOficina(index: Int) {
        
        self.regPubId = self.oficinas[index].RegPubId
        self.oficRegId = self.oficinas[index].OficRegId
        
        self.SearchView.primaryButton()
        self.isChecked = true
        
    }
    
    
    func changeTypeDocument(index: Int) {
        self.namePropiedad = self.tipoPropiedad[index]
        
        if (namePropiedad=="Propiedad Inmueble") {
            self.placaView.isHidden = true
            self.partidaView.isHidden = false
            areaId = "21000"
        }else if (namePropiedad=="Persona Jurídica") {
            self.placaView.isHidden = true
            self.partidaView.isHidden = false
            areaId = "22000"
        }else if (namePropiedad=="Propiedad Vehicular") {
            
            self.placaView.isHidden = false
            self.partidaView.isHidden = true
            areaId = "24000"
            
            self.placaText.autocapitalizationType = .allCharacters
        }
        
    }
    func changeTypePartida(index: Int) {
        dump(self.tipoPartidas)
        self.areaPartidaId = self.tipoPartidas[index]
        
        
        if (areaPartidaId=="Partida") {
            tipoId = "P"
            self.numFolioTextField.isHidden = true
            self.numPartidaTextField.placeholder = "Partida"
        }else if (areaPartidaId=="Ficha") {
            tipoId = "F"
            self.numFolioTextField.isHidden = true
            self.numPartidaTextField.placeholder = "Ficha"
        }else if (areaPartidaId=="Tomo") {
            tipoId = "T"
            self.numPartidaTextField.placeholder = "Tomo"
            self.numFolioTextField.isHidden = false
            self.numFolioTextField.borderAndPaddingLeftAndRight()
            
        }else if (areaPartidaId=="Partida SARP") {
            tipoId = "S"
            self.numPartidaTextField.placeholder = "Partida SARP"
            self.numFolioTextField.isHidden = true
        }
        
    }
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
    }
    
}

extension SearchPartidaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.oficinas.count
        case 2:
            return  self.tipoPropiedad.count
        case 3:
            return  self.tipoPartidas.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.oficinas[row].Nombre
        case 2:
            return self.tipoPropiedad[row]
        case 3:
            return  self.tipoPartidas[row]
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            indexTypeOficina = row
        case 2:
            indexPropiedad = row
        case 3:
            indexPartida = row
        default:
            return
        }
    }
}



extension SearchPartidaViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
}
