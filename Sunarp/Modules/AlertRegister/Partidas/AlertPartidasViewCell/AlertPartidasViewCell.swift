//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class AlertPartidasViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "AlertPartidasViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    
    
    
    
    @IBOutlet var numeroPartidaLabel:UILabel!
    @IBOutlet var areaLabel:UILabel!
    @IBOutlet var regionLabel:UILabel!
    @IBOutlet var fechaLabel:UILabel!
    @IBOutlet var estadoLabel:UILabel!
    @IBOutlet var estadoAPLabel:UILabel!
    
    
    @IBOutlet weak var listaNotificationView: UIView!
    @IBOutlet weak var eliminarView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    var onTapEliminar: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    var selectItem: AllPartidaFromUserEntity?
    
 
 
    func setup(with solicitud: AllPartidaFromUserEntity) {
        selectItem
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
        
      print("ofiiicina:>>",solicitud.nuPart)
      //  if solicitud.placa == "" {
     //       numeroPartidaLabel?.text = solicitud.nuPart
     //   }
        numeroPartidaLabel?.text = solicitud.nuPart
        areaLabel?.text = solicitud.area
        regionLabel?.text = "\(solicitud.region) - \(solicitud.sede)"
        fechaLabel?.text = solicitud.fecReg
        
        estadoLabel?.text = solicitud.estado
        estadoAPLabel?.text = solicitud.estadoAP
        /*
        if (solicitud.estado == "A"){
            estadoLabel?.text = "Activa"
        }else{
            estadoLabel?.text = "No aplica"
        }
        if (solicitud.estadoAP == "A"){
            estadoAPLabel?.text = "Activa"
        }else{
            estadoAPLabel?.text = "No aplica"
        }
        */
        
        
        /*   partidaLabel?.text = solicitud.numPartida
        fichaLabel?.text = solicitud.fichaId
        tomoLabel?.text = solicitud.tomoId
        folioLabel?.text = solicitud.fojaId
        direccionLabel?.text = solicitud.direccionPredio
        areaRegistralLabel?.text = solicitud.areaRegisDescripcion
        resgitroDeLabel?.text = solicitud.libroDescripcion
       */
     
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.listaNotificationView.addGestureRecognizer(onTapVerSolicitud)
        
        
        let onTapVerSolicitudElim = UITapGestureRecognizer(target: self, action: #selector(onTapEliminarView))
        self.eliminarView.addGestureRecognizer(onTapVerSolicitudElim)
        
        
    }
    
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    @objc private func onTapEliminarView(){
        self.onTapEliminar?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        //self.listaNotificationView.primaryButton()
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
