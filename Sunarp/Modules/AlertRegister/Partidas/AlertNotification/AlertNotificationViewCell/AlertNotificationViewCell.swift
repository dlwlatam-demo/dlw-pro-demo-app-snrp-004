//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class AlertNotificationViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "AlertNotificationViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    
    
    
    
    @IBOutlet var fechaLabel:UILabel!
    @IBOutlet var msjLabel:UILabel!
    
    @IBOutlet var nameZonaLabel:UILabel!
    @IBOutlet var activaLabel:UILabel!
    @IBOutlet var alertaPublicaLabel:UILabel!
    
    
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var eliminarView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: AlertNotificationEntity) {
        
      print("messageE:>>",solicitud.message)
      
        msjLabel?.text = solicitud.message.replacingOccurrences(of: "- PTD.EML ", with: "- Partidas")
        msjLabel?.text = solicitud.message.replacingOccurrences(of: "- PUB.EML ", with: "- Publicidad")
        msjLabel?.text = solicitud.message.replacingOccurrences(of: "- PTD.SMS ", with: "- Partidas")
        msjLabel?.text = solicitud.message.replacingOccurrences(of: "- PUB.SMS ", with: "- Publicidad")
        fechaLabel?.text = solicitud.feNoti
    
    }
    
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
       // viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        //self.addButtonView.primaryButton()
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
