//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit

class AlertNotificationViewController: UIViewController,UITableViewDataSource {
    
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typeSolicitudTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    @IBOutlet weak var insertNumTextField: UITextField!
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    var obtenerNotificationsEntities: [AlertNotificationEntity] = []
    var obtenerNotificationsMSJEntities: [AlertNotificationEntity] = []
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    
    var certificadoId: String = ""
    
    
    var valoficinaOrigen: String = ""
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    var allPartidas: [AllPartidaFromUserEntity] = []
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    var listaValidaPartidaLiteralEntity: [validaPartidaLiteralEntity] = []
    
    private lazy var presenter: AlertNotificationPresenter = {
       return AlertNotificationPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    
    
    var areaId: String = ""
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var titleCerti: String = ""
    
    
    var codGrupoLibroArea: String = ""
    
    
    var coRegi: String = ""
    var coSede: String = ""
    var coArea: String = ""
    var nuPart: String = ""
    var numPlaca: String = ""
    var nuPartSarp: String = ""
    var coLibr: String = ""
    
    
    var aaCont: String = ""
    var nuCont: String = ""
    var tipo: String = ""
    var source: String = ""
    var estado: String = ""
    var estadoAP: String = ""
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDesigner()
        addGestureView()
        
        registerCell()
        
       // self.presenter.listaService()
        
        self.tableSearchView.reloadData()
        
        
        setupNavigationBar()
        getListaNotifications()
        
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Notificaciones")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func getListaNotifications() {
        if self.estado == "Activa"{
            //Alertas por Correo electrónico: tipo=EML, source: PTD
            self.presenter.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: "EML", source: "PTD")
            
            //Alertas por SMS: tipo=SMS, source: PTD
            self.presenter.getListaNotiMSJfications(aaCont: aaCont, nuCont: nuCont, tipo: "SMS", source: "PTD")
            //Alertas por Correo electrónico: tipo=EML, source: PUB
            self.presenter.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: "EML", source: "PUB")
            
            //Alertas por SMS: tipo=SMS, source: PUB
            self.presenter.getListaNotiMSJfications(aaCont: aaCont, nuCont: nuCont, tipo: "SMS", source: "PUB")
        }
        /*
        if self.estadoAP == "Activa"{

        }*/
        if self.estado == "mandato"{
            //Alertas por Correo electrónico: tipo=EML, source: PUB
            self.presenter.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: "EML", source: "MAN")
            
            //Alertas por SMS: tipo=SMS, source: PUB
            self.presenter.getListaNotiMSJfications(aaCont: aaCont, nuCont: nuCont, tipo: "SMS", source: "MAN")
        }
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  
        /*
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        */
        
        //self.tipo = "T"
    }
    
    private func setupDesigner() {
       
        formView.backgroundCard()
     
        //obtiene lista registro juridico
        //self.presenter.validarRegistroJuridico()
     
        
     
          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: AlertNotificationViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: AlertNotificationViewCell.reuseIdentifier)
        
    }
    
   
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
    }
    

    @objc private func onTapToConfirmView() {
        
       // self.numPart = self.insertNumTextField.text ?? ""
       // self.presenter.validateData()
    }
    
    func loadAddNotification(notification: [AlertNotificationEntity]?) {
        
        if let data = notification, !data.isEmpty {
            data.forEach {
                var temp = $0
                
                temp.message = temp.message.replacingOccurrences(of: "PTD.EML", with: "Partidas")
                temp.message = temp.message.replacingOccurrences(of: "PUB.EML", with: "Publicidad")
                temp.message = temp.message.replacingOccurrences(of: "PTD.SMS", with: "Partidas")
                temp.message = temp.message.replacingOccurrences(of: "PUB.SMS", with: "Publicidad")
                dump(temp)
                self.obtenerNotificationsEntities.append(temp)
            }
            
            self.tableSearchView.reloadData()
        }
    }
    
    func loadAddMSJNotification(notification: [AlertNotificationEntity]?) {

        if let data = notification, !data.isEmpty {
                data.forEach {
                    var temp = $0
                    temp.message = temp.message.replacingOccurrences(of: "PTD.EML", with: "Partidas")
                    temp.message = temp.message.replacingOccurrences(of: "PUB.EML", with: "Publicidad")
                    temp.message = temp.message.replacingOccurrences(of: "PTD.SMS", with: "Partidas")
                    temp.message = temp.message.replacingOccurrences(of: "PUB.SMS", with: "Publicidad")
                    dump(temp)
                    self.obtenerNotificationsMSJEntities.append(temp)
                }
                
            self.tableSearchView.reloadData()
        }
    }
    
    func loadAddPartida(partidas: RegistroEntity) {
        print("codResult::>>",partidas.codResult)
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateOfIssueTextField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
        //    self.stopLoader(loader: self.loading)
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("couuntNotif:>>",self.obtenerNotificationsEntities.count)
            //return self.obtenerNotificationsEntities.count;
        
        switch section{
          case 0:
            return self.obtenerNotificationsEntities.count;
          case 1:
            return self.obtenerNotificationsMSJEntities.count;
          default:
              return 0
          }
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /*
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
         */
          return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("areaRegId__::>>",areaRegId)
        //if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: AlertNotificationViewCell.reuseIdentifier, for: indexPath) as? AlertNotificationViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
             //cell.item = self.listaPropiedadEntities[indexPath.row]
    
            
        
              cell.onTapVerSolicitud = {[weak self] in
                  print("self!.regPubId:>>",self!.regPubId)
             }
        
           switch indexPath.section{
            case 0:
               let listaPro = obtenerNotificationsEntities[indexPath.row]
               print("obtenerNotificationsEntitiesS:>>",listaPro)
               cell.setup(with: listaPro)
            case 1:
               let listaPro = obtenerNotificationsMSJEntities[indexPath.row]
               print("obtenerNotificationsEntitiesS:>>",listaPro)
               cell.setup(with: listaPro)
            default:
               cell.textLabel!.text="unknown"
            }
           return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Background color
        //view.tintColor = UIColor.gray
        UITableViewHeaderFooterView.appearance().tintColor = UIColor("#2FB8A5")
        //UITableViewHeaderFooterView.appearance().textLabel?.setfon
        // For Header Text Color
      //  UITableViewHeaderFooterView.textLabel?.textColor = .white
        
            if section == 0 {
                return "ALERTAS POR EMAIL"
            } else {
                return "ALERTAS POR SMS"
            }
    }
}


/*
// MARK: - Table view datasource
extension CertificadoLiteralPartidaDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        print("couunt:>>",self.listaValidaPartidaLiteralEntity.count)
            return self.listaValidaPartidaLiteralEntity.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          /*  if indexPath.section == 0 {
                print("section1")
            } else {
             */
        print("areaRegId__::>>",areaRegId)
        if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaPlacaViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
             //cell.item = self.listaPropiedadEntities[indexPath.row]
    
             let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
             print("listaValidaPartidaLiteralEntity:>>",listaPro)
             cell.setup(with: listaPro)
              cell.onTapVerSolicitud = {[weak self] in
             let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                  
                  vc.codLibro = listaPro.codLibro
                  vc.numPartida = listaPro.numPartida
                  vc.fichaId = listaPro.fichaId
                  vc.tomoId = listaPro.tomoId
                  vc.fojaId = listaPro.fojaId
                 // vc.ofiSARP = listaPro.ofiSARP
                  vc.ofiSARP = "-"
                  vc.coServicio = self?.coServ
                  vc.coTipoRegis = self!.coTipoRgst
                  vc.codZona = listaPro.codZona
                  vc.codOficina = listaPro.codOficina
                  
              self?.navigationController?.pushViewController(vc, animated: true)
             }
             return cell
            
        }else{
          
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            //cell.item = self.listaPropiedadEntities[indexPath.row]
    
            let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
            print("listaValidaPartidaLiteralEntity:>>",listaPro)
            cell.setup(with: listaPro)
            
             cell.onTapVerSolicitud = {[weak self] in
            let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailImagenConsultaTiveViewController") as! DetailImagenConsultaTiveViewController
             vc.tipo = listaPro.tipo
             vc.anioTitulo = listaPro.anioTitulo
             vc.numeroTitulo = listaPro.numeroTitulo
             vc.numeroPlaca = listaPro.numeroPlaca
             vc.codigoVerificacion = listaPro.codigoVerificacion
             vc.codigoZona = listaPro.codigoZona
             vc.codigoOficina = listaPro.codigoOficina
             self?.navigationController?.pushViewController(vc, animated: true)
            }
    
            return cell
        }
    }
}
*/
// MARK: - Table view delegate
extension AlertNotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}

