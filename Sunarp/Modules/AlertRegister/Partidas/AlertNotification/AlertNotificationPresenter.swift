//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class AlertNotificationPresenter {
    
    private weak var controller: AlertNotificationViewController?
    
   
 
    lazy private var model: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
   
    
    init(controller: AlertNotificationViewController) {
        self.controller = controller
    }
    
}

extension AlertNotificationPresenter: GenericPresenter {
    
    func getListaNotifications(aaCont: String, nuCont: String, tipo: String, source: String) {
       
        self.model.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: tipo, source: source){ (arrayNotifications) in
            if let data = arrayNotifications  {
                self.controller?.loadAddNotification(notification:data)
            }
        }
    }
    
    func getListaNotiMSJfications(aaCont: String, nuCont: String, tipo: String, source: String) {
        self.model.getListaNotifications(aaCont: aaCont, nuCont: nuCont, tipo: tipo, source: source){ (arrayNotifications) in
            if let data = arrayNotifications {
                self.controller?.loadAddMSJNotification(notification: data)
            }
        }
    }
    
    func postAlertAddPartida(coRegi: String, coSede: String, coArea: String, nuPart: String, numPlaca: String, nuPartSarp: String, coLibr: String) {
        self.model.postAlertAddPartida(coRegi: coRegi, coSede: coSede, coArea: coArea, nuPart: nuPart, numPlaca: numPlaca, nuPartSarp: nuPartSarp, coLibr: coLibr) { (objSolicitud) in
            self.controller?.loadAddPartida(partidas:objSolicitud)
        }
    }
    
    
}
