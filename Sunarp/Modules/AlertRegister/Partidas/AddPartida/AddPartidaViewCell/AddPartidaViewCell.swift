//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class AddPartidaViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "AddPartidaViewCell"
  //  @IBOutlet var profileImage:UIImageView!
  //  @IBOutlet var titleAddresLabel:UILabel!
 //   @IBOutlet var titleDistLabel:UILabel!
   // @IBOutlet var titleEmail:UILabel!
     @IBOutlet var viewCell:UIView!
    
    
    
    
    
    @IBOutlet var numeroPartidaLabel:UILabel!
    @IBOutlet var nameRegistroLabel:UILabel!
    
    @IBOutlet var nameZonaLabel:UILabel!
    @IBOutlet var fechaLabel:UILabel!
    @IBOutlet var activaLabel:UILabel!
    @IBOutlet var alertaPublicaLabel:UILabel!
    
    
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var eliminarView: UIView!
    
    var onTapVerSolicitud: (() -> Void)?
    
    
    var latValue = String()
    var longValue = String()
    var valType = String()
    var item: [ConsultaPropiedadListEntity] = []
    
 
 
    func setup(with solicitud: SearchPartidaEntity) {
        
      //  print("itttemm:", solicitud.titular)
        //titleNameLabel?.text = solicitud.titular
        
       
        
      print("ofiiicina:>>",solicitud.descripcion)
        nameRegistroLabel?.text = solicitud.descripcion
        
        if solicitud.placa == ""{
          numeroPartidaLabel?.text =  "Partida: \(solicitud.numPartida)"
        }else{
            numeroPartidaLabel?.text =  "Placa: \(solicitud.placa)"
        }
        
        /*   partidaLabel?.text = solicitud.numPartida
        fichaLabel?.text = solicitud.fichaId
        tomoLabel?.text = solicitud.tomoId
        folioLabel?.text = solicitud.fojaId
        direccionLabel?.text = solicitud.direccionPredio
        areaRegistralLabel?.text = solicitud.areaRegisDescripcion
        resgitroDeLabel?.text = solicitud.libroDescripcion
       */
     
        let onTapVerSolicitud = UITapGestureRecognizer(target: self, action: #selector(onTapVerSolicitudView))
        self.addButtonView.addGestureRecognizer(onTapVerSolicitud)
        
    }
    
    @objc private func onTapVerSolicitudView(){
        self.onTapVerSolicitud?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
   
        viewCell.backgroundColor = UIColor.white
        viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        //self.addButtonView.primaryButton()
        
        //print("itttemm:", item[0].titular)
        //titleLabel.textColor = 
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
