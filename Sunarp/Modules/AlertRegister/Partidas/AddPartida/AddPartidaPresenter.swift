//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class AddPartidaPresenter {
    
    private weak var controller: AddPartidaViewController?
    
   
 
    lazy private var model: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
   
    
    init(controller: AddPartidaViewController) {
        self.controller = controller
    }
    
}

extension AddPartidaPresenter: GenericPresenter {
    
    func getListaPartdas() {
       
     //   self.controller?.loaderView(isVisible: true)
        self.model.getSelectAllPartidaFromUser{ (arrayPartidas) in
          //  self.controller?.loaderView(isVisible: false)
         //   self.controller?.loadPartidas(allPartidaResponse: arrayPartidas)
        }
        
    }
    
        
    
    func postAlertAddPartida(coRegi: String, coSede: String, coArea: String, nuPart: String, numPlaca: String, nuPartSarp: String, coLibr: String) {
        self.model.postAlertAddPartida(coRegi: coRegi, coSede: coSede, coArea: coArea, nuPart: nuPart, numPlaca: numPlaca, nuPartSarp: nuPartSarp, coLibr: coLibr) { (objSolicitud) in
            
            self.controller?.loadAddPartida(partidas:objSolicitud)
        }
    }
    
    
}
