//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import WebKit

class TerminosCondicionesViewController: UIViewController {
        
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var toScanerView: UIView!
    @IBOutlet weak var viewWebDetail: UIView!
    @IBOutlet weak var btnAceptarView: UIView!
    
    @IBOutlet weak var webViewCons: WKWebView!
    
    var loading: UIAlertController!
    var urlQr:String = "https://www.sunarp.gob.pe/alertaregistral/app_ar_terms.html"
   // var urlQr:String = "https://pe.computrabajo.com/trabajo-de-uipath-en-lima-en-lima"

    var tipo: String = ""
   // private lazy var presenter: QrPresenter = {
   //     return QrPresenter(controller: self)
   // }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Términos")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        print("urlQr:>>",urlQr)
        viewWebDetail.isHidden = false
        let myURL = URL(string: urlQr)
           let myRequest = URLRequest(url: myURL!)
           webViewCons.load(myRequest)
        
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
        if returnValor == "return1"{
            self.navigationController?.popViewController(animated: false)
            
            let defaults = UserDefaults.standard
            defaults.set("return2", forKey: "returnKey")
            
        }
    }
    
    private func setupDesigner() {
    
  
        btnAceptarView.primaryButton()
       // viewWebDetail.isHidden = true
        
    }
    
    private func addGestureView(){
      
        let btnAceptarGesture = UITapGestureRecognizer(target: self, action: #selector(self.btnAceptarConfirmView))
        self.btnAceptarView.addGestureRecognizer(btnAceptarGesture)
    
    }
    @objc private func btnAceptarConfirmView() {
      
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegistralEmailViewController") as! AlertRegistralEmailViewController
        print("tipo:>>",tipo)
        vc.tipo = tipo
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    
    @IBAction func btnClose() {
        
        viewWebDetail.isHidden = true
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }

    
}


extension TerminosCondicionesViewController: QrReaderViewControllerDelegate{
    func capture(action:String) {
      
        viewWebDetail.isHidden = false
        let myURL = URL(string: action)
           let myRequest = URLRequest(url: myURL!)
           webViewCons.load(myRequest)
    }
    
    
}


