//
//  CodeValidationViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 24/07/22.
//

import UIKit

class AlertRegCodeValidationViewController: UIViewController {

    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var numberOneTextField: UITextField!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var timer: Timer?
    var timeLeft = 30
    var isEnableResendLabel = false
    var codeOtp: String = ""
    var email: String = ""
    var loading: UIAlertController!
    var guid: String = ""
    var tipo: String = ""
    
    var backNotService: Bool = false
    
    private lazy var presenter: AlertRegCodeValidationPresenter = {
       return AlertRegCodeValidationPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         setupDesigner()
         setupTimer()
         addGestureView()
         setupNavigationBar()
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        if tipo == "register"{
            self.navigationController?.navigationBar.isHidden = false
            loadNavigationBar(hideNavigation: false, title: "Registro de Usuario")
            addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
            titleLabel.text = "Registro"
        }else{
            self.navigationController?.navigationBar.isHidden = false
            loadNavigationBar(hideNavigation: false, title: "Alerta Usuario")
            addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
            titleLabel.text = "Establece Tu Contraseña"
        }
        
        numberOneTextField.delegate = self
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
        if returnValor == "return"{
            self.navigationController?.popViewController(animated: false)
            
            
        }
    }
    
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        let tapResendGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapResendCodeValidation))
        self.resendLabel.addGestureRecognizer(tapResendGesture)
        self.resendView.addGestureRecognizer(tapResendGesture)
    }
    
    private func setupTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
        
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numberOneTextField.resignFirstResponder()
    }
    
    @objc func onTimerFires() {
        self.timeLeft -= 1
        self.isEnableResendLabel = false
        if (self.timeLeft < 10) {
            self.countLabel.text = "00:0\(timeLeft)"
        } else if timeLeft < 60 {
            self.countLabel.text = "00:\(timeLeft)"
        } else {
            let minute = 60
            let minutesLeft = timeLeft / minute
            let secondsLeft = timeLeft % minute
            if secondsLeft < 10 {
                self.countLabel.text = "0\(minutesLeft):0\(secondsLeft)"
            } else {
                self.countLabel.text = "0\(minutesLeft):\(secondsLeft)"
            }
        }
        if (self.timeLeft <= 0) {
            self.isEnableResendLabel = true
            self.timer?.invalidate()
            self.timer = nil
        }
        self.changeColorResendLabel()
    }
        
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        
        numberOneTextField.border()
        
        toConfirmView.primaryButton()
        
        //self.numberOneTextField.delegate = self
        
       // self.numberOneTextField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
    }
    
    private func changeColorResendLabel() {
        if self.isEnableResendLabel {
            self.resendLabel.textColor = UtilHelper.getUIColor(hex: Constant.labelLinkActiveColor)
        } else {
            self.resendLabel.textColor = UtilHelper.getUIColor(hex: Constant.labelLinkInactiveColor)
        }
    }
    
    @objc private func onTapToConfirmView() {
        self.presenter.validationCodeEmailRequest()
    }
        
    func goToCodeValidationCode(_ state: Bool, message: String, codResult: String) {
        if (state) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegConfirmPasswordViewController") as? AlertRegConfirmPasswordViewController {
                    vc.guid = self.guid
                    vc.tipo = self.tipo
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }else{
            showMessageAlert(message: message)
        }
    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @objc private func onTapResendCodeValidation() {
        if self.isEnableResendLabel{
            self.presenter.resendCodeRequest()
        }
    }
    
    func goToUpdatePassword(_ state: Bool, message: String) {
        if state {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPasswordViewController") as! ConfirmPasswordViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }

    }
    
    func showMessageFromResendCode(_ state: Bool, message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if state {
                self.timeLeft = 5 * 60
                self.setupTimer()
            }
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
}


extension AlertRegCodeValidationViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 8
    }
}
