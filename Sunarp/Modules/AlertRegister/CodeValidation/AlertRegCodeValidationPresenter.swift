//
//  CodeValidationPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class AlertRegCodeValidationPresenter {
    
    private weak var controller: AlertRegCodeValidationViewController?
    
    lazy private var model: RecoveryModel = {
        let navigation = controller?.navigationController
        return RecoveryModel(navigationController: navigation!)
    }()
    
    init(controller: AlertRegCodeValidationViewController) {
        self.controller = controller
    }
    
    func validationCodeRequest() {
        guard let codeOtp = self.controller?.codeOtp else { return }
        
        if (codeOtp.isEmpty) {
            self.controller?.goToUpdatePassword(false, message: "El campo no puede estar vacío.")
            
        } else {
            self.controller?.loaderView(isVisible: true)
            let guid = UserPreferencesController.getGuid()
            self.model.postValidacionCodigo(codigoValidacion: codeOtp, guid: guid) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                    self.controller?.goToUpdatePassword(state, message: objSolicitud.msgResult)
                } else {
                    self.controller?.goToUpdatePassword(false, message: "Código Erróneo - Volver a intentar.")
                    
                }
                
            }
        }
    }
    
    func resendCodeRequest() {
        
        guard let numcelular = self.controller?.email else { return }
        
        if (!numcelular.isEmpty) {
            self.controller?.loaderView(isVisible: true)
            self.model.postSolicitudCodigo(numcelular) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.showMessageFromResendCode(state, message: objSolicitud.msgResult)
            }
        }
    }
    
    //Alert
    
    func validationCodeEmailRequest() {
        
        guard let guid = self.controller?.guid else { return }
        guard let coNoti = self.controller?.numberOneTextField.text else { return }
        
        print("emailLlll:>>",guid)
        
        self.controller?.loaderView(isVisible: true)
        self.model.getAlertActivateUserById(guid,coNoti:coNoti) { (objSolicitud) in
            self.controller?.loaderView(isVisible: false)
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)
            if (state) {
                UserPreferencesController.setGuid(guid: objSolicitud.guid)
            }
            self.controller?.goToCodeValidationCode(state, message: objSolicitud.msgResult,codResult:objSolicitud.codResult)
        }
    }
    
}
