//
//  RecoveryPasswordViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 23/07/22.
//

import UIKit

class AlertRegistralEmailViewController: UIViewController {

    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    var email: String = ""
    var appVersion: String = ""
    var guid: String = ""
    var loading: UIAlertController!
    var tipo: String = ""
    
    var backNotService: Bool = false
    
    private lazy var presenter: AlertRegistralEmailPresenter = {
       return AlertRegistralEmailPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        
        if tipo == "register"{
            self.navigationController?.navigationBar.isHidden = false
            loadNavigationBar(hideNavigation: false, title: "Registro de Usuario")
            addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
            titleLabel.text = "Registro"
        }else{
            
                self.navigationController?.navigationBar.isHidden = false
                loadNavigationBar(hideNavigation: false, title: "Alerta Registral")
                addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
            titleLabel.text = "Establece Tu Contraseña"
        }
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
        if returnValor == "return"{
            self.navigationController?.popViewController(animated: false)
            
            let defaults = UserDefaults.standard
            defaults.set("return1", forKey: "returnKey")
            
        }
    }
        
    private func setupDesigner() {
        self.headerView.backgroundColorGradientHeader()        
        formView.backgroundCard()
        emailTextField.borderAndPadding()
        sendView.primaryButton()
    }
    
    private func addGestureView(){        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapSendGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSendView))
        self.sendView.addGestureRecognizer(tapSendGesture)
    }
    func goToCodeValidationCreateRegister(_ state: Bool, message: String, codResult: String, guid: String) {
        if (state) {
            
            if codResult == "1"{
                
                self.guid = guid
                self.presenter.codeRequestSendValidationCode()
            }
          }else{
            showMessageAlert(message: message)
        }
    }
    func goToCodeValidationCode(_ state: Bool, message: String, codResult: String, guid: String) {
        if (state) {
            let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegCodeValidationViewController") as! AlertRegCodeValidationViewController
            vc.guid = guid
            vc.tipo = tipo
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            showMessageAlert(message: message)
        }
    }
    
    func goToCodeValidationRegister(_ state: Bool, message: String, codResult: String, guid: String) {
       
        print("codResult:Z>Z>>",codResult)
        print("message:Z>Z>>",message)
       // if (state) {
       
            self.appVersion = ""
            if codResult == "0"{
                // “El correo ingresado ya tiene una cuenta asociada.”
                showMessageAlert(message: message)
            }else if codResult == "1"{
            
                self.guid = guid
                self.presenter.codeRequestSendValidationCode()
            
            }else if codResult == "2"{
            
                self.guid = guid
                self.presenter.codeRequestRegister()
            }else{
            showMessageAlert(message: message)
            }
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { action in
                if (state) {
                  /*  let vc = self.storyboard?.instantiateViewController(withIdentifier: "CodeValidationViewController") as! CodeValidationViewController
                    vc.email = self.email
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    */
                    let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegCodeValidationViewController") as! AlertRegCodeValidationViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }))
            self.present(alert, animated: true)
        }
        */
        
    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.emailTextField.resignFirstResponder()
    }
    
    @objc private func onTapSendView() {
        
            self.email = self.emailTextField.text ?? ""
        
        if tipo == "register"{
            self.presenter.codeRequest()
        }else{
            self.presenter.codeRecoveryPassRequest()
        }
        /*
          let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
          let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegCodeValidationViewController") as! AlertRegCodeValidationViewController
          self.navigationController?.pushViewController(vc, animated: true)
         */
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
}
