//
//  RecoveryPasswordPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class AlertRegistralEmailPresenter {
    
    private weak var controller: AlertRegistralEmailViewController?
    
    lazy private var model: RecoveryModel = {
        let navigation = controller?.navigationController
        return RecoveryModel(navigationController: navigation!)
    }()
    
    init(controller: AlertRegistralEmailViewController) {
        self.controller = controller
    }
    
    func codeRequest() {
        
        guard let email = self.controller?.email else { return }
        
        print("emailLlll:>>",email)
        if (email.isEmpty) {
            self.controller?.goToCodeValidationRegister(false, message: "El campo no puede estar vacio.",codResult:"", guid: "")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToCodeValidationRegister(false, message: "Ingrese un email válido.",codResult:"", guid: "")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.getAlertRegSolicitudCodigo(email) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                print("state:>>",state)
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationRegister(state, message: objSolicitud.msgResult,codResult:objSolicitud.codResult, guid: objSolicitud.guid)
            }
        }
    }
    //recuperarPass
    func codeRecoveryPassRequest() {
        
        guard let email = self.controller?.email else { return }
        
        print("emailLlll:>>",email)
        if (email.isEmpty) {
            self.controller?.goToCodeValidationRegister(false, message: "El campo no puede estar vacio.",codResult:"", guid: "")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToCodeValidationRegister(false, message: "Ingrese un email válido.",codResult:"", guid: "")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.getLoadUserByEmailChnPwd(email) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                print("state:>>",state)
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationRegister(state, message: objSolicitud.msgResult,codResult:objSolicitud.codResult, guid: objSolicitud.guid)
            }
        }
    }
    func codeRequestRegister() {
        
        guard let email = self.controller?.email else { return }
        guard let guid = self.controller?.guid else { return }
        guard let appVersion = self.controller?.appVersion else { return }
        
                                     
        print("emailLlll:>>",email)
       
            self.controller?.loaderView(isVisible: true)
            self.model.postAlertRegValidacionCodigo(email: email,appVersion: appVersion, guid: guid) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                print("state:>>",state)
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationCreateRegister(state, message: objSolicitud.msgResult,codResult:objSolicitud.codResult, guid: objSolicitud.guid)
            }
    }
    //sendValidationCodeByMail
    func codeRequestSendValidationCode() {
        
        guard let guid = self.controller?.guid else { return }
       
        print("emailLlll:>>",guid)
       
            self.controller?.loaderView(isVisible: true)
            self.model.getAlertSendValidationCode(guid) { (objSolicitud) in
                self.controller?.loaderView(isVisible: false)
                let state = objSolicitud.codResult == "1"
                print("state:>>",state)
                if (state) {
                    UserPreferencesController.setGuid(guid: objSolicitud.guid)
                }
                self.controller?.goToCodeValidationCode(state, message: objSolicitud.msgResult,codResult:objSolicitud.codResult, guid: objSolicitud.guid)
            }
    }
    
}
