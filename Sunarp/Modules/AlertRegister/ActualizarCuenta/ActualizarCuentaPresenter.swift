//
//  ConfirmPasswordPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 9/08/22.
//

import Foundation

class ActualizarCuentaPresenter {
    
    private weak var controller: ActualizarCuentaViewController?
    
    
    lazy private var model: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
    init(controller: ActualizarCuentaViewController) {
        self.controller = controller
    }
    
    func loadUserRequest() {
       // self.controller?.loaderView(isVisible: true)
        self.model.getLoadUser() { (objSolicitud) in
       //     self.controller?.loaderView(isVisible: false)
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)

            self.controller?.goToCodeValidationRegister(state, message: objSolicitud.msgResult,objSolicitud:objSolicitud)
        }
    }
    
    
    func putCancelUser(idRgst: Int) {
        self.model.putCancelUser(idRgst: idRgst) { (objSolicitud) in
            // self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)
            self.controller?.goEliminarCuenta(state, message: objSolicitud.msgResult)
        }
    }
    
   
    
}
