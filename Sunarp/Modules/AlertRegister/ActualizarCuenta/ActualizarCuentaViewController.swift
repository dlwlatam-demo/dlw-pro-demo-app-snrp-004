//
//  ConfirmPasswordViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 29/07/22.
//

import UIKit

class ActualizarCuentaViewController: UIViewController {
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var toCancelView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var hideOrShowImage: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var hideOrShowRepeatImage: UIImageView!
    @IBOutlet weak var passwordRepeatTextField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tipoDocumentLabel: UILabel!
    @IBOutlet weak var nroDocumentLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var typeOperadorTextField: UITextField!
    @IBOutlet weak var numCelText: UITextField!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var mensajeLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var isHidden: Bool = true
    var isHiddenRepeat: Bool = true
    var contrasena: String = ""
    var contrasenaRepeat: String = ""
    var loading: UIAlertController!
    
    var guid: String = ""
    var tipo: String = ""
    
    var descriptionDoc: String = ""
    var idRgst: Int = 0
    var tiDocu: String = ""
    var idOperTele: String = ""
    var cnumCel:  String = ""
    var flEnvioSms: String = ""
    
    
    private lazy var presenter: ActualizarCuentaPresenter = {
       return ActualizarCuentaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        
            self.navigationController?.navigationBar.isHidden = false
            loadNavigationBar(hideNavigation: false, title: "Cuenta")
            addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
     
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        self.presenter.loadUserRequest()
    }
    
    private func setupDesigner() {
      
        formView.backgroundCard()
        toConfirmView.primaryButton()
        toCancelView.primaryRejectButton()
        
    }
  
    private func addGestureView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        let tapConfirmCancelGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToCancelCuentaView))
        self.toCancelView.addGestureRecognizer(tapConfirmCancelGesture)
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
 
    }
    
    @objc private func onTapToConfirmView() {
        
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailActualizaCuentaViewController") as! DetailActualizaCuentaViewController
        vc.email = self.emailLabel.text ?? ""
        
        
        vc.noDocu = self.nroDocumentLabel.text ?? ""
        vc.nombre = self.nombreLabel.text ?? ""
        vc.apPate = self.firstNameLabel.text ?? ""
        vc.apMate = self.secondNameLabel.text ?? ""
        vc.direccion = self.direccionLabel.text ?? ""
        vc.idRgst = self.idRgst
        vc.tiDocu = self.tiDocu
        vc.descriptionDoc = descriptionDoc
        vc.idOperTele = self.idOperTele
        vc.cnumCel = self.cnumCel
        vc.flEnvioSms = self.flEnvioSms
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onTapToCancelCuentaView() {
      self.alertCuentaPopupDialog(title: "Cuidado",message: "¿Está seguro que desea eliminar su cuenta de Alerta Registral?",primaryButton: "No",secondaryButton: "Si", addColor: SunarpColors.red, delegate: self)
    }
    
    func goEliminarCuenta(_ state: Bool, message: String) {
        if (state){
            
            let defaults = UserDefaults.standard
            defaults.set("return", forKey: "returnKey")
            self.navigationController?.popViewController(animated: false)
            
        }else{
            showMessageAlert(message: message)
        }
    }
    
    func goToCodeValidationRegister(_ state: Bool, message: String, objSolicitud:LoadUserEntity) {
        
        idRgst = objSolicitud.idRgst
        self.emailLabel.text = objSolicitud.email
        
        if (objSolicitud.tiDocu == "09"){
            descriptionDoc = "DOCUMENTO NACIONAL DE IDENTIDAD"
        } else if (objSolicitud.tiDocu == "03"){
            descriptionDoc = "CARNET DE EXTRANJERIA"
        }else if (objSolicitud.tiDocu == "50"){
            descriptionDoc = "DOCUMENTO DE IDENTIDAD EXTRANJERO"
        }else if (objSolicitud.tiDocu == "05"){
            descriptionDoc = "REGISTRO UNICO DEL CONTRIBUYENTE"
        }else if (objSolicitud.tiDocu == "08"){
            descriptionDoc = "PASAPORTE"
        }else if (objSolicitud.tiDocu == "51"){
            descriptionDoc = "PERMISO TEMPORAL DE PERMANENCIA"
        }else if (objSolicitud.tiDocu == "00"){
            descriptionDoc = "SIN DOCUMENTO"
        }else if (objSolicitud.tiDocu == "90"){
            descriptionDoc = "PARTIDA REGISTRAL"
        }else if (objSolicitud.tiDocu == "91"){
            descriptionDoc = "FICHA REGISTRAL"
        }else if (objSolicitud.tiDocu == "92"){
            descriptionDoc = "TOMO/FOLIO REGISTRAL"
        }else if (objSolicitud.tiDocu == "93"){
            descriptionDoc = "CEDULA DE IDENTIDAD PERSONAL"
        }else if (objSolicitud.tiDocu == "94"){
            descriptionDoc = "DOI"
        }
        self.tiDocu = objSolicitud.tiDocu
        self.idOperTele = String(objSolicitud.idOperTele)
        print("objSolicitudDD:>>",objSolicitud)
        print("objSolicitud.tiDocu:>>",objSolicitud.tiDocu)
        print("objSolicitud.idOperTele:>>",objSolicitud.idOperTele)
        print("objSolicitud.cnumCelLL:>>",objSolicitud.cnumCel)
        print("objSolicitud.celNumber:>>",objSolicitud.celNumber)
        print("objSolicitud.flEnvioSms:>>",objSolicitud.flEnvioSms)
        print("objSolicitud.noDocu:>>",objSolicitud.noDocu)
        print("objSolicitud.coChnPwd:>D>",objSolicitud.coChnPwd)
        
        self.cnumCel = objSolicitud.cnumCel
        self.flEnvioSms = objSolicitud.flEnvioSms
        
        self.tipoDocumentLabel.text = descriptionDoc
        self.nroDocumentLabel.text = objSolicitud.noDocu
        self.nombreLabel.text = objSolicitud.nombre
        self.firstNameLabel.text = objSolicitud.apPate
        self.secondNameLabel.text = objSolicitud.apMate
        self.direccionLabel.text = objSolicitud.direccion
        print("paso 1 ", objSolicitud.idOperTele)
        let textOperador = "Operador: "
        if objSolicitud.idOperTele == 1 {
            self.typeOperadorTextField.text = textOperador + "Claro"
        }else if self.idOperTele == "2" {
            self.typeOperadorTextField.text = textOperador + "Movistar"
        }else if self.idOperTele == "3" {
            self.typeOperadorTextField.text = textOperador + "Entel"
        }else if self.idOperTele == "4" {
            self.typeOperadorTextField.text = textOperador + "Bitel"
        }
        self.numCelText.text = "Número: " + self.cnumCel
        print("paso 2 ", objSolicitud.flEnvioSms)
        if (objSolicitud.flEnvioSms == "S") {
            self.mensajeLabel.text = "¿Desea recibir alertas por mensaje de texto?"
            self.checkImage.image = UIImage(systemName: "checkmark")
             self.typeOperadorTextField.isHidden = false
             self.numCelText.isHidden = false
         }else{
             self.mensajeLabel.text = "¿Desea recibir alertas por mensaje de texto?          No"
             self.checkImage.image = nil
             self.typeOperadorTextField.isHidden = true
             self.numCelText.isHidden = true
         }
        print("paso 3 ", self.numCelText.text ?? "")

    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
           self.stopLoader(loader: self.loading)
        }
    }
}

extension ActualizarCuentaViewController: AlertaCuentaDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
          //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
        self.presenter.putCancelUser(idRgst: self.idRgst)
    }
}
