//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class DetailActualizaCuentaPresenter {
    
    private weak var controller: DetailActualizaCuentaViewController?
    
    lazy private var modelAlert: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    
    init(controller: DetailActualizaCuentaViewController) {
        self.controller = controller
    }
    
}

extension DetailActualizaCuentaPresenter: GenericPresenter {
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        self.modelDoc.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func willAppear() {

    }
    
    func putUpdateUser(idRgst: Int, apPate: String, apMate: String, nombre: String, tiDocu: String, noDocu: String, direccion: String, cnumCel: Int, idOperTele: String, flEnvioSms: String) {
        self.modelAlert.putUpdateUser(idRgst: idRgst, apPate: apPate, apMate: apMate, nombre: nombre, tiDocu: tiDocu, noDocu: noDocu, direccion: direccion, cnumCel: cnumCel, idOperTele: idOperTele, flEnvioSms: flEnvioSms) { (objSolicitud) in
            
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)
            self.controller?.goToCodeValidationUpdateUser(state, message: objSolicitud.msgResult)
        }
    }
    
    
        
}
