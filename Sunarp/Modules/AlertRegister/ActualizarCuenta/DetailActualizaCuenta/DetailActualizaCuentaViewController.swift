//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class DetailActualizaCuentaViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var grabarCuentaView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var toCancelView: UIView!
    
    @IBOutlet weak var numDocText: UITextField!
    
    @IBOutlet weak var nameRazonSocialText: UITextField!
    
    @IBOutlet weak var numCelText: UITextField!
    @IBOutlet weak var nroDocumentoText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var secondNameText: UITextField!
    @IBOutlet weak var direccionText: UITextField!
    
    
    @IBOutlet weak var imagIconView: UIImageView!
    
    @IBOutlet weak var emailLabel: UILabel!
    var email: String = ""
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var maxLength = 0
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoOperadores: [String] = ["Claro", "Movistar","Entel", "Bitel"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoOperadorPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    
    var regPubId: String = ""
    
    
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var tipoDocId: String = ""
    
    
    var areaRegId: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    var titleBar: String = ""
    
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var precOfic: String = ""
    
    var indexTypeDocument = 0
    var idRgst: Int = 0
    var apPate: String = ""
    var apMate: String = ""
    var nombre: String = ""
    var tiDocu: String = ""
    var noDocu: String = ""
    var direccion: String = ""
    var cnumCel:  String = ""
    var idOperTele: String = ""
    var flEnvioSms: String = ""
    
    var descriptionDoc: String = ""
    
    
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    @IBOutlet weak var typeOperadorTextField: UITextField!
    @IBOutlet weak var typeDocumentTextField: UITextField!
    
    private lazy var presenter: DetailActualizaCuentaPresenter = {
       return DetailActualizaCuentaPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        presenter.didLoad()
        
        self.nroDocumentoText.keyboardType = .numberPad
        self.nameText.keyboardType = .alphabet
        self.firstNameText.keyboardType = .alphabet
        self.secondNameText.keyboardType = .alphabet
        self.direccionText.keyboardType = .alphabet
        /*
        self.nroDocumentoText.valueType = .onlyNumbers
        self.nameText.valueType = .alphaNumericWithSpace
        self.firstNameText.valueType = .alphaNumericWithSpace
        self.secondNameText.valueType = .alphaNumericWithSpace
        self.direccionText.valueType = .alphaNumericWithSpace
         */
        self.nroDocumentoText.delegate = self
        self.nameText.delegate = self
        self.firstNameText.delegate = self
        self.secondNameText.delegate = self
        self.direccionText.delegate = self
        
        self.nroDocumentoText.maxLength = 20
        self.nameText.maxLength = 50
        self.firstNameText.maxLength = 50
        self.secondNameText.maxLength = 50
        self.direccionText.maxLength = 50
        
        self.nameText.text = self.nombre
        self.firstNameText.text = self.apPate
        self.secondNameText.text = self.apMate
        self.tipoDocId = self.tiDocu
        self.nroDocumentoText.text = self.noDocu
        self.direccionText.text = self.direccion
        print("cnumCel::CC>>>",self.cnumCel)
        print("idOperTele::>>>",String(self.idOperTele))
        self.numCelText.text = self.cnumCel
        
        
        
        self.typeDocumentTextField.text = self.descriptionDoc
        if self.idOperTele == "1" {
            self.typeOperadorTextField.text = "Claro"
        }else if self.idOperTele == "2" {
            self.typeOperadorTextField.text = "Movistar"
        }else if self.idOperTele == "3" {
            self.typeOperadorTextField.text = "Entel"
        }else if self.idOperTele == "4" {
            self.typeOperadorTextField.text = "Bitel"
        }
        
        if self.cnumCel == ""{
            self.grabarCuentaView.primaryDisabledButton()
        }else{
            self.grabarCuentaView.primaryButton()
        }
        
        if (flEnvioSms == "S") {
           self.checkImage.image = UIImage(systemName: "checkmark")
            self.typeOperadorTextField.isHidden = false
            self.numCelText.isHidden = false
            self.imagIconView.isHidden = false
        }else{
            self.typeOperadorTextField.isHidden = true
            self.numCelText.isHidden = true
            self.imagIconView.isHidden = true
        }
    }
    
    func createToolbarTypeDocument() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneTypeDocument))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelTypeDocument))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func doneTypeDocument() {
        self.changeTypeDocument(index: indexTypeDocument)
        self.typeDocumentTextField.text = tipoDocumentosEntities[indexTypeDocument].descripcion
        self.typeDocumentTextField.resignFirstResponder()
    }
    
    @objc func cancelTypeDocument() {
        self.view.endEditing(true)
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Detalle Cuenta")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        print("test")
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.grabarCuentaView.primaryButton()
        self.grabarCuentaView.primaryDisabledButton()
        
        
        toCancelView.primaryRejectButton()
        
        typeOperadorTextField.border()
        typeOperadorTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoOperadorPickerView.tag = 1
        self.tipoOperadorPickerView.delegate = self
        self.tipoOperadorPickerView.dataSource = self
        self.typeOperadorTextField.inputView = self.tipoOperadorPickerView
        
        
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        self.tipoDocumentoPickerView.tag = 2
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        self.typeDocumentTextField.inputAccessoryView = self.createToolbarTypeDocument()
        
        self.numCelText.borderAndPaddingLeftAndRight()
        self.nroDocumentoText.borderAndPaddingLeftAndRight()
        self.nameText.borderAndPaddingLeftAndRight()
        self.firstNameText.borderAndPaddingLeftAndRight()
        self.secondNameText.borderAndPaddingLeftAndRight()
        self.direccionText.borderAndPaddingLeftAndRight()
        
        checkView.borderCheckView()
        
        self.isChecked = false
        //self.participantePickerView.tag = 2
        self.emailLabel.text = email
        /*
        let amount = String(format: "S/ %.2f", (montoCalc as NSString).doubleValue)
                        
        let costoTitle = NSMutableAttributedString(string: "Costo total: ", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: boldAttrs as [NSAttributedString.Key : Any])
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
         */
     //   self.numDocText.delegate = self
       // self.numDocText.inputAccessoryView = createToolbarGender()
        
    }
    
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapgrabarCuentaView))
        self.grabarCuentaView.addGestureRecognizer(tapPagarGesture)
        
       
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkIconAndTitleView.addGestureRecognizer(tapChecked)
        
        let tapConfirmCancelGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToCancelCuentaView))
        self.toCancelView.addGestureRecognizer(tapConfirmCancelGesture)
        
    }
    @objc private func onTapToCancelCuentaView() {
        goBack()
    }
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            self.typeOperadorTextField.isHidden = true
            self.numCelText.isHidden = true
            self.imagIconView.isHidden = true
            flEnvioSms = "N"
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
              flEnvioSms = "S"
            self.typeOperadorTextField.isHidden = false
            self.numCelText.isHidden = false
            self.imagIconView.isHidden = false
        }
        self.isChecked = !self.isChecked
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
        //self.tipoDocumentoPickerView.delegate = self
        //self.tipoDocumentoPickerView.dataSource = self
        
       // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
    }
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
          self.obtenerLibroEntities = arrayTipoDocumentos
        //  for tipoDocumento in arrayTipoDocumentos {
       //       self.tipoDocumentos.append(tipoDocumento.descripcion)
       //   }
                          // self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
     }

                     
    
 

    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    func setValues() {
        let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
        let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
        let usuario = UserPreferencesController.usuario()
        
        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
            //self.areaRegistralText.text = "DNI"
              typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
            //self.areaRegistralText.text = "CE"
              typeDocument = "03"
        } else {
        }
      
        
        print("certificadoId::",certificadoId)
        print("codArea::",codArea)
        print("codLibro::",codLibro)
        print("valoficinaOrigen::",valoficinaOrigen)
        print("refNumPart::",refNumPart)
        print("numPartida::",numPartida)
        print("numeroPlaca::",numeroPlaca)
        print("usuario.tipo::",usuario.tipo)
        print(" usuario.priApe::", usuario.priApe)
        print("usuario.segApe::",usuario.segApe)
        print("usuario.nombres::",usuario.nombres)
        print("usuario.tipoDoc::",usuario.tipoDoc)
        print("usuario.nroDoc::",usuario.nroDoc)
        print("usuario.email::",usuario.email)
        print("montoCalc::",montoCalc)
        print("countCantPag::",countCantPag)
        print("countCantPagExo::",countCantPagExo)
        print("nuAsieSelectSARP::",nuAsieSelectSARP)
        print("imPagiSIR::",imPagiSIR)
        print("nuSecu::",nuSecu)
         
        print("tipoDoc:>>",usuario.tipoDoc)
        
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.numCelText.resignFirstResponder()
        self.nroDocumentoText.resignFirstResponder()
        self.nameText.resignFirstResponder()
        self.firstNameText.resignFirstResponder()
        self.secondNameText.resignFirstResponder()
        self.direccionText.resignFirstResponder()
        
        
    }
    
    @objc private func onTapgrabarCuentaView(){
        //if (self.isChecked) {
           // loaderView(isVisible: true)
           // presenter.getVisaKeys()
          
        
        
            print("idRgst::",idRgst)
            print("self.firstNameText.text::",self.firstNameText.text ?? "")
            print("self.secondNameText.text ??::",self.secondNameText.text ?? "")
            print("self.nameText.text ??::",self.nameText.text ?? "")
            print("uself.nroDocumentoText.text:",self.nroDocumentoText.text ?? "")
            print(" self.numCelText.text::", self.numCelText.text ?? "")
            print("flEnvioSms::",flEnvioSms)
            print("self.tipoDocId::",self.tipoDocId)
        
        
        presenter.putUpdateUser(idRgst: idRgst, apPate: self.firstNameText.text ?? "", apMate: self.secondNameText.text ?? "", nombre: self.nameText.text ?? "", tiDocu: self.tipoDocId, noDocu: self.nroDocumentoText.text ?? "", direccion: self.direccionText.text ?? "", cnumCel: Int(self.numCelText.text ?? "") ?? 0, idOperTele: self.idOperTele, flEnvioSms: self.flEnvioSms)
       // }
    }
    func goToCodeValidationUpdateUser(_ state: Bool, message: String) {
        if (state){
            goBack()
        }else{
            showMessageAlert(message: message)
        }
    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    
    func changeTypeOficina(index: Int) {
        
        let nameOperador = self.tipoOperadores[index]
     
        if nameOperador == "Claro" {
            self.idOperTele = "1"
        }else if nameOperador == "Movistar" {
            self.idOperTele = "2"
        }else if nameOperador == "Entel" {
            self.idOperTele = "3"
        }else if nameOperador == "Bitel" {
            self.idOperTele = "4"
        }
        
        self.grabarCuentaView.primaryButton()
        self.isChecked = true
        
    }
    
    
    func changeTypeDocument(index: Int) {
        self.tipoDocId = self.tipoDocumentosEntities[index].tipoDocId
       
        print("self.typeNameDocument:---:>>>",self.tipoDocId)
        
    }
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    
    
    func loadDatosDni(_ state: Bool, message: String, jsonValidacionDni: ValidacionDniEntity) {
        if (state) {
            self.isValidDocument = true
            self.grabarCuentaView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
    func loadDatosCe(_ state: Bool, message: String, jsonValidacionCe: ValidacionCeEntity) {
        if (state) {
            self.isValidDocument = true
            self.grabarCuentaView.primaryButton()
        } else {
            self.isValidDocument = false
            showMessageAlert(message: message)
        }
    }
    
}

extension DetailActualizaCuentaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel

        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        switch pickerView.tag {
        case 1:
            label.text = self.tipoOperadores[row]
            
        case 2:
            label.text = self.tipoDocumentosEntities[row].descripcion
            
        default:
            label.text = "Sin datos"
        }
        
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()

        return label
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.tipoOperadores.count
        case 2:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.tipoOperadores[row]
        case 2:
            //return self.tipoCertificado[row]
            
            return self.tipoDocumentosEntities[row].descripcion
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            self.changeTypeOficina(index: row)
            self.typeOperadorTextField.text = tipoOperadores[row]
            self.typeOperadorTextField.resignFirstResponder()
            
        case 2:
            indexTypeDocument = row
        default:
            return
        }
        
    }
    

}


extension DetailActualizaCuentaViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, !text.isEmpty else {
            // Si no hay texto, llenar con ceros y actualizar el campo de texto
            textField.text = ""
            return
        }
        textField.text = text.uppercased()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            // Si no hay texto, llenar con ceros y actualizar el campo de texto
            textField.text = ""
            return true
        }
        
        textField.text = text.uppercased()
        
        // Ocultar el teclado
        textField.resignFirstResponder()
        
        return true
    }
    
    
}
