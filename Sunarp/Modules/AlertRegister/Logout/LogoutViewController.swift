//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import WebKit

class LogoutViewController: UIViewController {
        
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var serviciosView: UIView!
    @IBOutlet weak var alertasView: UIView!
    @IBOutlet weak var contactoView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var toScanerView: UIView!
    @IBOutlet weak var viewWebDetail: UIView!
    @IBOutlet weak var btnAceptarView: UIView!
    
    @IBOutlet weak var webViewCons: WKWebView!
    
    var loading: UIAlertController!
    var urlQr:String = "https://www.sunarp.gob.pe/alertaregistral/app_ar_terms.html"
    
    var tipo: String = ""
   // private lazy var presenter: QrPresenter = {
   //     return QrPresenter(controller: self)
   // }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goBack()
      //  setupDesigner()
      //  addGestureView()
        //setupNavigationBar()
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Términos")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
       
    }
    
    private func setupDesigner() {
    
  
       // btnAceptarView.primaryButton()
       // viewWebDetail.isHidden = true
        
    }
    
    private func addGestureView(){
      
        let btnAceptarGesture = UITapGestureRecognizer(target: self, action: #selector(self.btnAceptarConfirmView))
        self.btnAceptarView.addGestureRecognizer(btnAceptarGesture)
    
    }
    @objc private func btnAceptarConfirmView() {
      
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegistralEmailViewController") as! AlertRegistralEmailViewController
        print("tipo:>>",tipo)
        vc.tipo = tipo
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    
    @IBAction func btnClose() {
        
        viewWebDetail.isHidden = true
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }

    
}




