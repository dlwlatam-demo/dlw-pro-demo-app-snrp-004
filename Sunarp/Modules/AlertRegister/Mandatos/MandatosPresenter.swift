//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation
import UIKit

class MandatosPresenter {
    
    private weak var controller: MandatosViewController?
    
   
 
    lazy private var model: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
   
    
    init(controller: MandatosViewController) {
        self.controller = controller
    }
    
}

extension MandatosPresenter: GenericPresenter {
    
    func getListaPartdas() {
       
     //   self.controller?.loaderView(isVisible: true)
        self.model.getSelectAllMandatoFromUser{ (arrayPartidas) in
          //  self.controller?.loaderView(isVisible: false)
            self.controller?.loadPartidas(allPartidaResponse: arrayPartidas)
        }
        
    }
    func putDeleteMandato(aaCont: String, nuCont: String) {
        self.model.putDeleteMandato(aaCont: aaCont, nuCont: nuCont) { (objSolicitud) in
            // self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            let state = objSolicitud.codResult == "1"
            print("state:>>",state)
            if (state) {
             //   UserPreferencesController.setGuid(guid: objSolicitud.guid)
            }
            self.controller?.goToCodeValidationUpdatePartida(state, message: objSolicitud.msgResult)
        }
    }
    
    
    
}
