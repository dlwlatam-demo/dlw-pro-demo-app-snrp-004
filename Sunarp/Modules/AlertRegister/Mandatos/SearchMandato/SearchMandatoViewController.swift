//
//  PagarBusquedaViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation
import UIKit
import VisaNetSDK

class SearchMandatoViewController: UIViewController {
        
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var SearchView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    
    @IBOutlet weak var placaView: UIView!
    @IBOutlet weak var partidaView: UIView!
    @IBOutlet weak var placaText: UITextField!
    @IBOutlet weak var SearchPlacaView: UIView!
    
    var maxLength = 0
    
    @IBOutlet weak var nameRazonSocialText: UITextField!
    
    @IBOutlet weak var numCelText: UITextField!
    @IBOutlet weak var nroDocumentoText: UITextField!
    
    @IBOutlet weak var numDocText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var secondNameText: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    var email: String = ""
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var region: String = ""
    var ofic: String = ""
    var area: String = ""
    var partida: String = ""
    var ficha: String = ""
    var tomo: String = ""
    var foja: String = ""
    var partiSarp: String = ""
    var placa: String = ""
    var tipo: String = ""
    
    
    
    var areaId: String = ""
    var tipoId: String = ""
    
    var isValidDocument: Bool = false
    var certificadoId: String = ""
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoPropiedad: [String] = ["Propiedad Inmueble", "Persona Jurídica","Propiedad Vehicular"]
    var tipoPartidas: [String] = ["Partida", "Ficha", "Tomo", "Partida SARP"]
    var tipoDocumentoPickerView = UIPickerView()
    var tipoOperadorPickerView = UIPickerView()
    var tipoPartidaPickerView = UIPickerView()
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    
    var obtenerLibroEntities: [ObtenerLibroEntity] = []
    
    
    var obtenerPartidasEntities: [SearchPartidaEntity] = []
    
    var regPubId: String = ""
    
    
    var oficRegId: String = ""
    var valoficinaOrigen: String = ""
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var tipoDocId: String = ""
    var namePropiedad: String = ""
    var areaPartidaId: String = ""
    
    
    var areaRegId: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var numDocument: String = ""
    var dateOfIssue: String = ""
    
    var titleBar: String = ""
    
    
    var tipPerPN: String = ""
    var apePatPN: String = ""
    var apeMatPN: String = ""
    var nombPN: String = ""
    var razSocPN: String = ""
    var tipoDocPN: String = ""
    var numDocPN: String = ""
    var precOfic: String = ""
    
    
    var idRgst: Int = 0
    var apPate: String = ""
    var apMate: String = ""
    var nombre: String = ""
    var tiDocu: String = ""
    var noDocu: String = ""
    var direccion: String = ""
    var cnumCel: Int = 0
    var idOperTele: String = ""
    var flEnvioSms: String = ""
    var indexTypeDocument = 0
    
    
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    @IBOutlet weak var typeDocumentTextField: UITextField!
    @IBOutlet weak var typePropiedadTextField: UITextField!
    @IBOutlet weak var typePartidaTextField: UITextField!
    @IBOutlet weak var numPartidaTextField: UITextField!
    
    @IBOutlet weak var numFolioTextField: UITextField!
    
    private lazy var presenter: SearchMandatoPresenter = {
       return SearchMandatoPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        presenter.didLoad()
        flEnvioSms = "N"
       
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Mandatos")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
       // self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)

    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
     
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.SearchView.primaryButton()
        
      //  self.SearchView.primaryDisabledButton()
        
        
        typeDocumentTextField.border()
        typeDocumentTextField.setPadding(left: CGFloat(8), right: CGFloat(24))
        
        
        self.tipoDocumentoPickerView.tag = 1
        self.tipoDocumentoPickerView.delegate = self
        self.tipoDocumentoPickerView.dataSource = self
        self.typeDocumentTextField.inputView = self.tipoDocumentoPickerView
        self.typeDocumentTextField.inputAccessoryView = self.createToolbarTypeDocument()
     
        self.numDocText.borderAndPaddingLeftAndRight()
        self.nameText.borderAndPaddingLeftAndRight()
        self.firstNameText.borderAndPaddingLeftAndRight()
        self.secondNameText.borderAndPaddingLeftAndRight()
        self.nameText.maxLength = 50
        self.firstNameText.maxLength = 50
        self.secondNameText.maxLength = 50

        self.nameText.delegate = self
        self.firstNameText.delegate = self
        self.secondNameText.delegate = self
        self.numDocText.delegate = self
    }
    
    func createToolbarTypeDocument() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(doneTypeDocument))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelTypeDocument))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
    }
    
    @objc func doneTypeDocument() {
        self.changeTypeDocument(index: indexTypeDocument)
        self.typeDocumentTextField.text = tipoDocumentosEntities[indexTypeDocument].descripcion
        self.typeDocumentTextField.resignFirstResponder()
    }
    
    @objc func cancelTypeDocument() {
        self.view.endEditing(true)
    }
  
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)

        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapSearchView))
        self.SearchView.addGestureRecognizer(tapPagarGesture)

    }
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
              flEnvioSms = "S"
        }
        self.isChecked = !self.isChecked
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        for tipoDocumento in arrayTipoDocumentos {
            self.tipoDocumentos.append(tipoDocumento.descripcion)
        }
    }
    func loadObtenerLibro(arrayTipoDocumentos: [ObtenerLibroEntity]) {
          self.obtenerLibroEntities = arrayTipoDocumentos
     }

                     
    
 

    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    
    func setValues() {
        let usuario = UserPreferencesController.usuario()

        print("self.tipoPer ::>>",self.tipoPer)
        self.tipoPer = "N"
        if (usuario.tipoDoc == "09") {
              typeDocument = "09"
        } else if (usuario.tipoDoc == "03") {
              typeDocument = "03"
        } else {
        }
        
       
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {

        self.numDocText.resignFirstResponder()
        self.nameText.resignFirstResponder()
        self.firstNameText.resignFirstResponder()
        self.secondNameText.resignFirstResponder()
    
        
    }
    
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    
    @objc private func onTapSearchView(){
        presenter.postAddMandatoPartida(apPatePersNatu: self.firstNameText.text ?? "", apMatePersNatu: self.secondNameText.text ?? "", noPersNatu: self.nameText.text ?? "", tiDocuIden: tipoId, nuDocu: self.numDocText.text ?? "")
    }
    
   
    func goToCodeValidationUpdateUser(_ state: Bool, message: String) {
        if (state){
            goBack()
        }else{
            showMessageAlert(message: message)
        }
    }
    func loadSearch(partidas: [SearchPartidaEntity]) {
        
        self.obtenerPartidasEntities = partidas
        print("solicitudTT::-->>",partidas)
        goBack()
        
    }
    func loadAddMandato(mandato: RegistroEntity) {
        print("codResult::>>",mandato.codResult)
        
        goBack()
            
    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadAreas(areaResponse: AreaResponse) {
        self.areas = areaResponse.areas
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
    func loadGrupos(grupoResponse: GrupoResponse) {
        self.grupos = grupoResponse.grupos
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loaderView(isVisible: false)
        }
    }
    
  
    
    func changeTypeOficina(index: Int) {
        
        self.regPubId = self.oficinas[index].RegPubId
        self.oficRegId = self.oficinas[index].OficRegId
     
        self.SearchView.primaryButton()
        self.isChecked = true
        
    }

    
    func changeTypeDocument(index: Int) {
       // self.typeDocument = self.tipoDocumentosEntities[index].tipoDocId
        self.tipoDocId = self.tipoDocumentosEntities[index].tipoDocId
        print(tipoDocId, " = ", self.tipoDocumentosEntities[index].descripcion)
        self.isValidDocument = false
        if (self.tipoDocId == Constant.TYPE_DOCUMENT_CODE_DNI) {
            self.maxLength = 8
            self.numDocText.keyboardType = .numberPad
            //self.dateOfIssueTextField.placeholder = "Fecha Emisión"
        } else if (self.tipoDocId == Constant.TYPE_DOCUMENT_CODE_CE) {
            self.maxLength = 9
           //7 self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numDocText.keyboardType = .alphabet
        } else {
            self.maxLength = 20
            //self.dateOfIssueTextField.placeholder = "Fecha Nacimiento"
            self.numDocText.keyboardType = .alphabet
            
            self.isValidDocument = true
        }
        self.numDocText.text = ""
        self.nameText.text = ""
        self.firstNameText.text = ""
        self.secondNameText.text = ""
        
    }
    
    
    func loadOficinas(oficinasResponse: [OficinaRegistralEntity]) {
        self.oficinas = oficinasResponse
        //self.loaderView(isVisible: false)
       
    }
    
   
}

extension SearchMandatoViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            indexTypeDocument = row
            return self.tipoDocumentosEntities[row].nombreAbrev
            
        default:
            return "Sin datos"
        }
    }

}



extension SearchMandatoViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if (textField == numDocText) {
            if (count == maxLength) {
                let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                if (string.rangeOfCharacter(from: invalidCharacters) == nil) {
                    let mergedString = (textField.text! as NSString) .replacingCharacters(in: range, with: string)
                    textField.text = mergedString
                   // self.validateTypeDocument()
                    return false
                }
            } else {
               // self.numDocText.text = ""
                self.nameText.text = ""
                self.firstNameText.text = ""
                self.secondNameText.text = ""
                self.isValidDocument = false
            }
            return count < maxLength
        } else {
            return count <= 30
        }
        
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
    }
}

