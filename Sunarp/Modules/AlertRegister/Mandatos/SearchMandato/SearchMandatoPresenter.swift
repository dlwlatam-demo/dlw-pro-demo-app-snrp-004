//
//  PagarBusquedaPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 22/09/22.
//

import Foundation

class SearchMandatoPresenter {
    
    private weak var controller: SearchMandatoViewController?
    
    lazy private var modelCons: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var model: ServiceModel = {
        let navigation = controller?.navigationController
       return ServiceModel(navigationController: navigation!)
    }()
    
    lazy private var modelAlert: AlertRegisterModel = {
        let navigation = controller?.navigationController
       return AlertRegisterModel(navigationController: navigation!)
    }()
    
    
    lazy private var modelDoc: RegisterModel = {
        let navigation = controller?.navigationController
       return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
       return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
       return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
 
    init(controller: SearchMandatoViewController) {
        self.controller = controller
    }
    
}

extension SearchMandatoPresenter: GenericPresenter {
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        //let guid = UserPreferencesController.getGuid()
        self.modelDoc.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
   
    
    func postAddMandatoPartida(apPatePersNatu: String, apMatePersNatu: String, noPersNatu: String, tiDocuIden: String, nuDocu: String) {
        self.controller?.loaderView(isVisible: true)
        self.modelAlert.postAddMandatoPartida(apPatePersNatu: apPatePersNatu, apMatePersNatu: apMatePersNatu, noPersNatu: noPersNatu, tiDocuIden: tiDocuIden, nuDocu: nuDocu) { (objSolicitud) in
            
            self.controller?.loaderView(isVisible: false)
            self.controller?.loadAddMandato(mandato:objSolicitud)
        }
    }
    
    
        
}
