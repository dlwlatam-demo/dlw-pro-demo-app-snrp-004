//
//  DataRegisterViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import Foundation
import UIKit

class MandatosViewController: UIViewController,UITableViewDataSource {
    
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var typeOficinaTextField: UITextField!
    @IBOutlet weak var typeSolicitudTextField: UITextField!
    @IBOutlet weak var dateOfIssueTextField: UITextField!
    
    @IBOutlet weak var insertNumTextField: UITextField!
    
    @IBOutlet weak var checkIconAndTitleView: UIView!
    
    
    @IBOutlet weak var typeRadioButtonLabel: UILabel!
    
    var isChecked: Bool = false
    var isValidDocument: Bool = false
    var numcelular: String = ""
    var numDocument: String = ""
    var dateOfIssue: String = ""
    var typeDocument: String = ""
    var names: String = ""
    var lastName: String = ""
    var middleName: String = ""
    var gender: String = ""
    var email: String = ""
    
    var certificadoId: String = ""
    
    
    var valoficinaOrigen: String = ""
    
    var selectOficina: String = ""
    var selectSolicitud: String = ""
    
    
    var val_aaCont: String = ""
    var val_nuCount: String = ""
    
    var maxLength = 0
    //var tipoDocumentos: [String] = []
    var tipoRegistroJuridico: [String] = []
    var registroJuridicoEntities: [RegistroJuridicoEntity] = []
    var tipoCertificado: [String] = []
    var tipoCertificadoEntities: [TipoCertificadoEntity] = []
    
    var oficinas: [OficinaRegistralEntity] = []
    
    var servicesLista: [ServicioSunarEntity] = []
    
    var allPartidas: [AllMandatoFromUserEntity] = []
    
    //var allPartidas: AllMandatoFromUserEntity!
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    
    var tipoDocumentoPickerView = UIPickerView()
    var tipoCertiPickerView = UIPickerView()
    var datePicker = UIDatePicker()
    var loading: UIAlertController!
    
    
    var tipo: String = ""
    var source: String = ""
    var contadorMandatos = 0
    
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    //areaRegId
    
    @IBOutlet var tableSearchView:UITableView!
    var listaEntity: [TiveEntity] = []
    var listaValidaPartidaLiteralEntity: [validaPartidaLiteralEntity] = []
    
    private lazy var presenter: MandatosPresenter = {
       return MandatosPresenter(controller: self)
    }()
    
    
    var codigoZona: String = ""
    var codigoOficina: String = ""
    
    
    
    var regPubId: String = ""
    var oficRegId: String = ""
    var areaRegId: String = ""
    var codGrupo: String = ""
    var tipoPartidaFicha: String = ""
    var numPart: String = ""
    var coServ: String = ""
    var coTipoRgst: String = ""
    
    var titleCerti: String = ""
    
    
    var codGrupoLibroArea: String = ""
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        
        registerCell()
        
        self.presenter.getListaPartdas()
       // self.presenter.listaService()
        
        self.tableSearchView.reloadData()
        
        
        setupNavigationBar()
       
        
    }
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Mandatos")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            //self.insertNumTextField.resignFirstResponder()
            self.presenter.getListaPartdas()
            self.tableSearchView.reloadData()
        }
        
        
        
    }
    
    private func addGestureView(){
     /*
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
       // self.formView.addGestureRecognizer(tapGesture)
        self.formView.addGestureRecognizer(tapGesture)
        
  */
        
        let tapConfirmGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.toConfirmView.addGestureRecognizer(tapConfirmGesture)
        
        
        //self.tipo = "T"
    }
    
    private func setupDesigner() {
       
        //formView.backgroundCard()
        //obtiene lista registro juridico
        //self.presenter.validarRegistroJuridico()
     
          toConfirmView.primaryButton()
          tableSearchView.delegate = self
          tableSearchView.dataSource = self
        
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: MandatosViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: MandatosViewCell.reuseIdentifier)
        
    }
    
   
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
    }
    

    @objc private func onTapToConfirmView() {
       // self.numPart = self.insertNumTextField.text ?? ""
       // self.presenter.validateData()
        print("Contador de Mandatos")
        print(contadorMandatos)
        if contadorMandatos < 5 {
            let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchMandatoViewController") as! SearchMandatoViewController
            //vc.email = self.emailLabel.text ?? ""
            //vc.idRgst = idRgst
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            showToast(message: "No se pueden agregar más de 5 registros")
        }
    }
    
   
    func showToast(message: String) {
        let toastWidth: CGFloat = 300
        let toastHeight: CGFloat = 50
        
        let toastView = ToastView(frame: CGRect(x: (view.frame.width - toastWidth) / 2, y: view.frame.height - 100, width: toastWidth, height: toastHeight))
        view.addSubview(toastView)
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: 10, width: toastWidth - 20, height: toastHeight - 20))
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.font = UIFont.systemFont(ofSize: 12) // Cambia el tamaño de la fuente según tus preferencias
        toastLabel.text = message
        toastLabel.numberOfLines = 2
        toastView.addSubview(toastLabel)
        
        UIView.animate(withDuration: 3.0, animations: {
            toastView.alpha = 0
        }) { (completed) in
            toastView.removeFromSuperview()
        }
    }

   
    func loadPartidas(allPartidaResponse: [AllMandatoFromUserEntity]) {
        
        print("allPartidaResponse::>>",allPartidaResponse)
        self.allPartidas = allPartidaResponse
        contadorMandatos = allPartidas.count
        //self.loaderView(isVisible: false)
        
        self.loaderView(isVisible: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            //self.insertNumTextField.resignFirstResponder()
            
            self.loaderView(isVisible: false)
            
            self.tableSearchView.reloadData()
           /*
            if (self.allPartidas.isEmpty) {
                let alert = UIAlertController(title: "SUNARP", message: "No se encontrarón coincidencias", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
                self.present(alert, animated: true)
            } else {
             
            }
            */
        }
        
        self.tableSearchView.reloadData()
        //self.insertNumTextField.resignFirstResponder()
        self.loaderView(isVisible: false)
       
    }
   
    
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: Constant.Localize.aceptar,
                                         style: .plain, target: nil, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        
        return toolbar
        
    }
        
    func createToolbarGender() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        
        return toolbar
        
    }
    func goToCodeValidationUpdatePartida(_ state: Bool, message: String) {
        
        
        self.presenter.getListaPartdas()
        self.tableSearchView.reloadData()
        if (state){
            self.tableSearchView.reloadData()
           // goBack()
        }else{
            showMessageAlert(message: message)
        }
    }
    
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateOfIssueTextField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
        
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
        //    self.stopLoader(loader: self.loading)
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print("couunt:>>",self.allPartidas.count)
            return self.allPartidas.count;
        //return 1;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /*
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
         */
          return 195
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("areaRegId__::>>",areaRegId)
        //if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: MandatosViewCell.reuseIdentifier, for: indexPath) as? MandatosViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
        
             let listaPro = allPartidas[indexPath.row]
             print("listaValidaPartidaLiteralEntity:>>",listaPro)
         cell.setup(with: listaPro)
        
            cell.onTapVerSolicitud = {[weak self] in
                  
                let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AlertNotificationViewController") as! AlertNotificationViewController
                 
                vc.aaCont = listaPro.aaCont
                vc.nuCont = listaPro.nuCont
                vc.estado = "mandato"
               // vc.estadoAP = listaPro.estadoAP
                self?.navigationController?.pushViewController(vc, animated: true)
             }
             
        
            cell.onTapEliminar = {[weak self] in
              
               
                self?.val_aaCont = listaPro.aaCont
                self?.val_nuCount = listaPro.nuCont
                
                self?.alertCuentaPopupDialog(title: "Estimado(a) usuario(a):",message: "¿Está seguro que desea remover el mandato?",primaryButton: "No,regresar",secondaryButton: "Si,remover", addColor: SunarpColors.red, delegate: self!)
                
                
                self!.tableSearchView.reloadData()
                
                
            }
  
         
             return cell
            
        
    }
}


/*
// MARK: - Table view datasource
extension CertificadoLiteralPartidaDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        print("couunt:>>",self.listaValidaPartidaLiteralEntity.count)
            return self.listaValidaPartidaLiteralEntity.count;
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if areaRegId == "24000"{
            return 265
        }else{
            return 365
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          /*  if indexPath.section == 0 {
                print("section1")
            } else {
             */
        print("areaRegId__::>>",areaRegId)
        if areaRegId == "24000"{
             guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaPlacaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaPlacaViewCell else {
                return UITableViewCell()
             }
             cell.selectionStyle = .none
             cell.separatorInset = .zero
             //cell.item = self.listaPropiedadEntities[indexPath.row]
    
             let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
             print("listaValidaPartidaLiteralEntity:>>",listaPro)
             cell.setup(with: listaPro)
              cell.onTapVerSolicitud = {[weak self] in
             let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "CertiCalculoLiteralDetailPatidaViewController") as! CertiCalculoLiteralDetailPatidaViewController
                  
                  vc.codLibro = listaPro.codLibro
                  vc.numPartida = listaPro.numPartida
                  vc.fichaId = listaPro.fichaId
                  vc.tomoId = listaPro.tomoId
                  vc.fojaId = listaPro.fojaId
                 // vc.ofiSARP = listaPro.ofiSARP
                  vc.ofiSARP = "-"
                  vc.coServicio = self?.coServ
                  vc.coTipoRegis = self!.coTipoRgst
                  vc.codZona = listaPro.codZona
                  vc.codOficina = listaPro.codOficina
                  
              self?.navigationController?.pushViewController(vc, animated: true)
             }
             return cell
            
        }else{
          
            guard let cell = self.tableSearchView.dequeueReusableCell(withIdentifier: CertificadoLiteralDetailPartidaViewCell.reuseIdentifier, for: indexPath) as? CertificadoLiteralDetailPartidaViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            //cell.item = self.listaPropiedadEntities[indexPath.row]
    
            let listaPro = listaValidaPartidaLiteralEntity[indexPath.row]
            print("listaValidaPartidaLiteralEntity:>>",listaPro)
            cell.setup(with: listaPro)
            
             cell.onTapVerSolicitud = {[weak self] in
            let storyboard = UIStoryboard(name: "ConsultaTivet", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailImagenConsultaTiveViewController") as! DetailImagenConsultaTiveViewController
             vc.tipo = listaPro.tipo
             vc.anioTitulo = listaPro.anioTitulo
             vc.numeroTitulo = listaPro.numeroTitulo
             vc.numeroPlaca = listaPro.numeroPlaca
             vc.codigoVerificacion = listaPro.codigoVerificacion
             vc.codigoZona = listaPro.codigoZona
             vc.codigoOficina = listaPro.codigoOficina
             self?.navigationController?.pushViewController(vc, animated: true)
            }
    
            return cell
        }
    }
}
*/
// MARK: - Table view delegate
extension MandatosViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
 

}

extension MandatosViewController: AlertaCuentaDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
          //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
           // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
        print("self.val_aaCont::>>",self.val_aaCont)
        print("self.val_nuCount::>>",self.val_nuCount)
        self.presenter.putDeleteMandato(aaCont: self.val_aaCont, nuCont: self.val_nuCount)
        
        
    }
}


