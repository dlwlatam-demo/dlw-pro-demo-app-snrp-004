//
//  AlertaMandatosView.swift
//  Sunarp
//
//  Created by Enrique Alata Vences on 13/10/23.
//

import Foundation
import UIKit

class ToastView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
