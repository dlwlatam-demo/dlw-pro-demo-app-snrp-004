//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class ListAlertRegistralViewController: UIViewController {
        
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    
    @IBOutlet var uiScroll:UIScrollView!
    @IBOutlet var viwBack:UIView!
    
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var workView: UIView!
    
    @IBOutlet weak var formViewContacto: UIView!
    @IBOutlet weak var formViewConsulta: UIView!
    @IBOutlet weak var formViewVisit: UIView!
    
    
    @IBOutlet weak var formViewColorCouenta: UIView!
    @IBOutlet weak var formViewColorPartidas: UIView!
    @IBOutlet weak var formViewColorMandatos: UIView!
    
    @IBOutlet weak var toConfirmView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = true
        loadNavigationBar(hideNavigation: true, title: "Alerta Regitral")
        //addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
       
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
        if returnValor == "return"{
            self.navigationController?.popViewController(animated: false)
            
            let defaults = UserDefaults.standard
            defaults.set("return3", forKey: "returnKey")
            
        }
    }
    
    private func setupDesigner() {
        
        self.formViewContacto.backgroundCard()
        self.formViewConsulta.backgroundCard()
        self.formViewVisit.backgroundCard()
        
       
        self.formViewColorCouenta.backgroundColor = SunarpColors.cyanLight
        self.formViewColorPartidas.backgroundColor = SunarpColors.blueLight
        self.formViewColorMandatos.backgroundColor = SunarpColors.orangeLight
        
      //  self.formViewEditar.backgroundCard()
       
    }
    @IBAction func cerrarSesion(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func addGestureView(){
        
     
        let tapadContactGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToContacView))
        self.formViewContacto.addGestureRecognizer(tapadContactGesture)
        
       
        
        let tapConConsulGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConsultafirmView))
        self.formViewConsulta.addGestureRecognizer(tapConConsulGesture)
        
        
        let tapVisitLinkGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmView))
        self.formViewVisit.addGestureRecognizer(tapVisitLinkGesture)
        
      
        
    }
   
    
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }
    //dialNumber(number: "+921111111222")
    @objc private func onTapToConsultafirmView() {
      
        
        
          let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
          let vc = storyboard.instantiateViewController(withIdentifier: "AlertaPartidasViewController") as! AlertaPartidasViewController
          self.navigationController?.pushViewController(vc, animated: true)
        
        //dialogMessage()
       
    }
    @objc private func onTapToConfirmView() {
        
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MandatosViewController") as! MandatosViewController
        self.navigationController?.pushViewController(vc, animated: true)
        /*
        guard let url = URL(string: "https://www.sunarp.gob.pe/") else {
             return
        }
        if UIApplication.shared.canOpenURL(url) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
         */
    }
        
    @objc private func onTapToContacView() {
      
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ActualizarCuentaViewController") as! ActualizarCuentaViewController
        self.navigationController?.pushViewController(vc, animated: true)
       //dialNumber(number: "+012083100")
    }
    
 
    
    
    
  

}


