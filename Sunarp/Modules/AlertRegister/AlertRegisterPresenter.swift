//
//  LoginPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 10/08/22.
//

import Foundation

class AlertRegisterPresenter {
    
    private weak var controller: AlertRegisterViewController?
    
    lazy private var model: LoginModel = {
        let navigation = controller?.navigationController
       return LoginModel(navigationController: navigation!)
    }()
    
    init(controller: AlertRegisterViewController) {
        self.controller = controller
    }
    
    func login() {
        
        guard let email = self.controller?.email else { return }
        guard let password = self.controller?.password else { return }
        
        if (email.isEmpty || password.isEmpty) {
            self.controller?.goToHome(false, message: "El campo no puede estar vacío.")
        } else if (!UtilHelper.isValidEmailAddress(emailAddress: email)) {
            self.controller?.goToHome(false, message: "Ingrese un email válido.")
        } else {
            self.controller?.loaderView(isVisible: true)
            self.model.postAlertLogin(email: email, password: password) { (objLogin) in
                self.controller?.loaderView(isVisible: false)
                
                print("objLogin.code::>>:>",objLogin.code)
                print("objLogin.description::>>:>",objLogin.description)
                if objLogin.code == "200"{
                    let state = true
                    self.controller?.goToHome(state, message: "Email o contraseña incorrectos")
                }else{
                    if (email == "") {
                        let state = false
                       //let state = !objLogin.accessToken.isEmpty
                       /* if (state) {
                           UserPreferencesController.setAccessToken(accessToken: objLogin.accessToken)
                           UserPreferencesController.setRefreshToken(refreshToken: objLogin.refreshToken)
                           UserPreferencesController.setTokenType(tokenType: objLogin.tokenType)
                           UserPreferencesController.setJti(jti: objLogin.jti)
                      }*/
                          self.controller?.goToHome(state, message: "Email o contraseña incorrectos")
                    }else{
                        if objLogin.code == "401"{
                          let state = false
                          self.controller?.goToHome(state, message: "Email o contraseña incorrectos")
                        }else  if objLogin.code == "500"{
                            let state = false
                            self.controller?.goToHome(state, message: "Error al iniciar sesión")
                        }else  if objLogin.code == "400"{
                              let state = false
                              self.controller?.goToHome(state, message: "Error al iniciar sesión")
                        }else{
                            if objLogin.description == ""{
                                let state = true
                                self.controller?.goToHome(state, message: "Error al iniciar sesión")
                            }else{
                                let state = false
                                self.controller?.goToHome(state, message: "Error al iniciar sesión")
                            }
                        
                        }
                    }
                 
                }
            }
        }
    }
        
}
