//
//  LoginViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 23/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class AlertRegisterViewController: UIViewController {

    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hideOrShowImage: UIImageView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var recoveryPasswordView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var secondaryButton: UIButton!
    
    var isHidden: Bool = true
    var email: String = ""
    var password: String = ""
    var loading: UIAlertController!
    var tipo: String = ""
    
    private lazy var presenter: AlertRegisterPresenter = {
       return AlertRegisterPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        setupNavigationBar()
        
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Alerta Registral")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let defaults = UserDefaults.standard
        let returnValor = defaults.string(forKey: "returnKey")!
        print("returnValor:>>",returnValor)
    }
    
    private func addGestureView(){
        let onTapRecovery = UITapGestureRecognizer(target: self, action: #selector(didAppearRecoveryPasswordView))
        recoveryPasswordView.addGestureRecognizer(onTapRecovery)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapHideOrShow = UITapGestureRecognizer(target: self, action: #selector(self.onTapHideOrShowPassword))
        self.hideOrShowImage.addGestureRecognizer(tapHideOrShow)
        
        let onTapLogin = UITapGestureRecognizer(target: self, action: #selector(didAppearHomeView))
        loginView.addGestureRecognizer(onTapLogin)
        
        
        let onTapSecondary = UITapGestureRecognizer(target: self, action: #selector(RegisterView))
        secondaryButton.addGestureRecognizer(onTapSecondary)
    }
    //TerminosCondicionesViewController
    @objc private func RegisterView() {
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TerminosCondicionesViewController") as! TerminosCondicionesViewController
        vc.tipo = "register"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func didAppearRecoveryPasswordView() {
        let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertRegistralEmailViewController") as! AlertRegistralEmailViewController
        vc.tipo = "recovery"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func didAppearHomeView() {
        self.email = self.emailTextField.text ?? ""
        self.password = self.passwordTextField.text ?? ""
        
        self.presenter.login()
       
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    @objc private func onTapHideOrShowPassword() {
        if (isHidden) {
            let text = self.passwordTextField.text
            self.passwordTextField.isSecureTextEntry = false
            self.hideOrShowImage.image = UIImage(systemName: "eye")
            self.passwordTextField.text = ""
            self.passwordTextField.text = text
        } else {
            self.passwordTextField.isSecureTextEntry = true
            self.hideOrShowImage.image = UIImage(systemName: "eye.slash")
        }
        self.isHidden = !self.isHidden
    }
    
    private func setupDesigner() {        
        headerView.backgroundColorGradientHeader()
        
        formView.backgroundCard()
        
        emailTextField.borderAndPadding()
        passwordTextField.borderAndPaddingLeftAndRight()
        
        loginView.primaryButton()
        
        secondaryButton.titleLabel?.font = SunarpFont.bold12
        secondaryButton.setTitleColor(SunarpColors.greenLight, for: UIControl.State.normal)
        secondaryButton.addShadowViewCustom(cornerRadius: 5)
        secondaryButton.borderView()
        secondaryButton.layer.borderColor = SunarpColors.greenLight.cgColor
        
        let controlStates: Array<UIControl.State> = [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved]
        for controlState in controlStates {
            secondaryButton.setTitle(NSLocalizedString("Regístrate ahora", comment: ""), for: controlState)
        }
        
        let defaults = UserDefaults.standard
        defaults.set("returnInicio", forKey: "returnKey")
        
    }
    
    func goToHome(_ state: Bool, message: String) {
        if (state) {
            let storyboard = UIStoryboard(name: "AlertRegister", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ListAlertRegistralViewController") as! ListAlertRegistralViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            showMessageAlert(message: message)
        }        
    }
    func showMessageAlert(message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
}
