//
//  DataRegisterPresenter.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 7/08/22.
//

import Foundation

class MapaPresenter {
    
    private weak var controller: MapaViewController?
    
    lazy private var model: MapaModel = {
        let navigation = controller?.navigationController
       return MapaModel(navigationController: navigation!)
    }()
    lazy private var modelProfil: ProfileModel = {
        let navigation = controller?.navigationController
       return ProfileModel(navigationController: navigation!)
    }()
    init(controller: MapaViewController) {
        self.controller = controller
    }
    
}

extension MapaPresenter: GenericPresenter {
    
    func didLoad() {
       // self.controller?.loaderView(isVisible: true)
        let guid = UserPreferencesController.getGuid()
        
        self.modelProfil.getListaTipoDocumentos{ (arrayTipoDocumentos) in
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            self.modelProfil.postLogin(appId: Constant.APP_ID, deviceToken: "", deviceType: "", status: "A", deviceId: "1", appVersion: appVersion, ipAddress: UtilHelper.getIPAddress(), deviceLogged: "1", receiveNotifications: "N", deviceOs: Constant.DEVICE_OS) { (profileEntity) in
              //  self.controller?.loadDataPerfil(profileEntity: profileEntity, arrayTipoDocumentos: arrayTipoDocumentos)
            }
        }
        
        self.model.getListaMapa(guid: guid) { (jsonValidacion) in
           // self.controller?.loaderView(isVisible: true)
            let state = jsonValidacion.codResult == "1"
            self.controller?.loadDatosMapa(state, message: jsonValidacion.msgResult, jsonValidacion: jsonValidacion)
            
        }
    }
    
 
}
