//
//  ConfirmDialog.swift
//  App
//
//  Created by femer on 1/18/21.
//  Copyright © 2021 Raul Quispe. All rights reserved.
//

import UIKit

protocol MarketWindowDelegate {
    func primary(action:[MapaOfficeEntity],index:Int)
    func secondary()
    func showShareSheet(item:MapaOfficeEntity)
    func showRouteMap(item:MapaOfficeEntity)
} 

class MarketWindow: UIViewController {
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var dialogHeaderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var secondaryButton: UIButton!
    @IBOutlet weak var primaryButton: UIButton!
  
    @IBOutlet weak var imgShowRoute: UIView!
    
    var delegate:MarketWindowDelegate?
    var titleText = ""
    var messageText = ""
    var primaryText = ""
    var secondaryText = ""
    var distanKmR = ""
    var officeText = ""
    var typeText = ""
    var distanText = ""
    
    var index:Int!
    
    var arrayEntity: [MapaOfficeEntity] = []
    
    var addColor:UIColor = .clear
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogView.layer.cornerRadius = 8
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        
        
        let attributedString = NSMutableAttributedString(string: titleText)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 8 // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        /*
         let labelFont = messageLabel.font!
         attributedString.addAttribute(NSAttributedString.Key.font, value: labelFont, range: NSMakeRange(0, attributedString.length))
         
         // *** Set Attributed String to your label ***
         messageLabel.attributedText = attributedString
         
         */
        
        print("titleText = ", titleText)
        titleLabel.text = titleText
        messageLabel.text = messageText
        
        
        let controlStates: Array<UIControl.State> = [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved]
        for controlState in controlStates {
            primaryButton.setTitle(NSLocalizedString(primaryText, comment: ""), for: controlState)
            secondaryButton.setTitle(NSLocalizedString(secondaryText, comment: ""), for: controlState)
        }
        
        let tapadShowRoute = UITapGestureRecognizer(target: self, action: #selector(self.onTapShowRoute))
        self.imgShowRoute.addGestureRecognizer(tapadShowRoute)
        
    }
    
   
    @objc private func onTapShowRoute() {
    
        if let kilometro = Double(distanKmR), kilometro  > 10.0 {
            showAlertKilometros()
        }else{
            delegate?.showRouteMap(item: arrayEntity[index])
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showAlertKilometros(){
        let alert = UIAlertController(title: "Información", message: "La distancia es mayor a 10 Km", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func share(_ sender: Any) {
        showShareSheet()
    }
    
    @IBAction func secondary(_ sender: Any) {
        delegate?.secondary()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func primary(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.primary(action: arrayEntity,index:index)
    }
    
    func showShareSheet() {
        let latitude = arrayEntity[index].lat
        let longitude = arrayEntity[index].lon
        let locationURL = URL(string: "http://maps.apple.com/?ll=\(latitude),\(longitude)")
        
        if UIApplication.shared.canOpenURL(locationURL!) {
            let activityViewController = UIActivityViewController(activityItems: [locationURL!], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Aplicación de mapas no encontrada",
                                                    message: "Instale una aplicación de mapas para compartir la ubicación.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
