//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import CoreLocation
import SearchTextField
import HSSearchable

class MapaViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var toConfirmView: UIView!
    @IBOutlet weak var imgcCleanRoutes: UIView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var imgIcono: UIImageView!
    @IBOutlet weak var formBusquedaView: UIView!
    @IBOutlet weak var formBusquedaIntView: UIView!
    @IBOutlet weak var formBusquedaSearchView: UIView!
    //@IBOutlet weak var busquedaTxtFiel: SearchTextField!
    
    
    var flagButton = Bool()
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var latValue = CLLocationDegrees()
    var longValue = CLLocationDegrees()
    var markerArray = Array<Any>()
    var i : Int = 0
    var origin = String()
    var destination = String()
    var previousLat = CLLocationDegrees()
    var previousLong = CLLocationDegrees()
    var officinasEntities: [MapaOfficeEntity] = []
    var loading: UIAlertController!
    var distanKmR = ""
    private lazy var presenter: MapaPresenter = {
        return MapaPresenter(controller: self)
    }()
    
    @IBOutlet var tableView:UITableView!
    var polyline: GMSPolyline?
    //MARK: 10 km de radio a la ubicación actual
    let radiusOfKilometers = 10.0
    
    // let subjectsDict = ["Spanish": ["Oficina Registral"], "Math":["Oficina Receptora"], "Science": ["Sede Central"], "Science": ["Tribunal Registral"], "Science": ["Zona Registral"], "Science": ["Oficina Otras"]]
    let subjectArray = ["Spanish", "Math", "Science"]
    let array = ["Oficina Registral","Oficina Receptora","Sede Central","Tribunal Registral","Zona Registral","Oficina Otras","Todos"]
    
    //MARK: iVars
    var usersData = SearchableWrapper()
    var users: [UserDM] {
        return self.usersData.dataArray as! [UserDM]
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var searchbar: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        
        setupNavigationBar()
        
        flagButton = true
        getCurrentLocation()
        
        
        //imgIcono.image = SunarpImage.iconMapDark
        tableView.isHidden = true
        tableView.reloadData()
        tableView.separatorColor = UIColor.clear
        // initializeYourMap()
        //mapiperu2019
        registerCell()
        self.presenter.didLoad()
        
        //let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        
        let camera = GMSCameraPosition.camera(withLatitude: latValue, longitude: longValue, zoom: 12.0)
        //   let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        
        mapView.camera = camera
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        // self.view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        //marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        let marker = GMSMarker()
        var markerViewmarkerImage = UIImageView(image: SunarpImage.getImage(named: .vect_posision))
        
        marker.iconView = markerViewmarkerImage
        marker.position = CLLocationCoordinate2D(latitude: latValue, longitude: longValue)
        marker.title = "Lima"
        marker.snippet = "Perú"
        marker.map = mapView
    }
    
    @IBAction private func myTargetFunction(_ sender: Any) {
        if sender is UITextField {
            // get textField here
            //formTableView.isHidden = false
        }
    }
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Nuestras Oficinas")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - search
    // 1 - Configure a simple search text view
    /* fileprivate func configureSimpleSearchTextField() {
     // Start visible even without user's interaction as soon as created - Default: false
     busquedaTxtFiel.startVisibleWithoutInteraction = false
     
     // Set data source
     let countries = localPlace()
     busquedaTxtFiel.filterStrings(countries)
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    // Hide keyboard when touching the screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    private func setupDesigner() {
        //  self.formView.backgroundCard()
        //  self.formViewEditar.backgroundCard()
        tableView.delegate = self
        tableView.dataSource = self
        
        self.imgcCleanRoutes.backgroundMapIcon()
        self.imgView.backgroundMapIcon()
        self.formBusquedaView.backgroundCard()
        self.formBusquedaIntView.backgroundCard()
        
        imgcCleanRoutes.isHidden = true
    }
    
    func registerCell() {
        let profileCellNib = UINib(nibName: MapaViewCell.reuseIdentifier, bundle: nil)
        tableView.register(profileCellNib, forCellReuseIdentifier: MapaViewCell.reuseIdentifier)
    }
    
    private func addGestureView(){
        let tapadContactGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToTableView))
        self.imgView.addGestureRecognizer(tapadContactGesture)
        
        let tapadBusqGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToBusqView))
        self.formBusquedaView.addGestureRecognizer(tapadBusqGesture)
        
        let tapadCleanRoutes = UITapGestureRecognizer(target: self, action: #selector(self.onTapClearRoute))
        self.imgcCleanRoutes.addGestureRecognizer(tapadCleanRoutes)
    }
    
    @objc private func onTapClearRoute() {
        polyline?.map = nil
        polyline = nil
        imgcCleanRoutes.isHidden = true
    }
    
    @objc private func onTapToBusqView() {
        
        let storyboard = UIStoryboard(name: "Mapa", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapaSearchViewController") as! MapaSearchViewController
        vc.array = users
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        /*
         let camera = GMSCameraPosition.camera(withLatitude: latValue, longitude: longValue, zoom: 15.0)
         //   let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
         
         mapView.camera = camera
         mapView.delegate = self
         mapView.isMyLocationEnabled = true
         mapView.settings.myLocationButton = true
         // self.view.addSubview(mapView)
         
         // Creates a marker in the center of the map.
         //marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
         let marker = GMSMarker()
         marker.position = CLLocationCoordinate2D(latitude: latValue, longitude: longValue)
         marker.title = "Lima"
         marker.snippet = "Perú"
         marker.map = mapView
         */
    }
    
    @objc private func onTapToTableView() {
        
        if flagButton == true{
            flagButton = false
            imgIcono.image =  SunarpImage.getImage(named: .iconMapWhite)
            imgView.backgroundColor = SunarpColors.greenLight
            tableView.isHidden = false
        }else{
            flagButton = true
            imgIcono.image =  SunarpImage.getImage(named: .iconMapDark)
            imgView.backgroundColor = SunarpColors.white
            tableView.isHidden = true
        }
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    
    func loadDatosMapa(_ state: Bool, message: String, jsonValidacion: MapaEntity) {
        
        
        officinasEntities = jsonValidacion.dataMap
        
        var array : Array<UserDM> = []
        var countVal : Int = 0
        for item in jsonValidacion.dataMap {
            let latitude    = NSNumber(value:Double(item.lat) ?? 0.0)
            //let latitude    = (item as AnyObject).value(forKey: "lat")
            let longitude   = NSNumber(value:Double(item.lon) ?? 0.0)
            let titleString = item.office
            let titleAdreesString = item.address
            let titleTypeString = item.type
            
            let Punto1 = CLLocation(latitude: latValue, longitude: longValue)
            let Punto2 = CLLocation(latitude: latitude as! CLLocationDegrees , longitude: longitude as! CLLocationDegrees)
            
            let DistanciaEnMetros = Punto1.distance(from: Punto2)
            
            let distanKm = Double(DistanciaEnMetros/1000)
            let distanKmR = distanKm.redondear(numeroDeDecimales: 2)
            // print("..ADistanciaEnMetros.>>",DistanciaEnMetros)
            
            drawMarker(item:item,indexSearch:countVal,type:titleTypeString,title: titleString, lat: latitude as! CLLocationDegrees , long: longitude as! CLLocationDegrees,distance:String(distanKmR))
            // drawPath(title: titleString, lat: latitude as! CLLocationDegrees , long: longitude as! CLLocationDegrees)
            
            
            
            
            // print("..distanKmm.>>",distanKm.redondear(numeroDeDecimales: 2))
            let user1 = UserDM(office: titleString, addres: titleAdreesString, type: titleTypeString,lat:item.lat,lon:item.lon,indexSearch:countVal, distance: String(distanKm.redondear(numeroDeDecimales: 2)),item:officinasEntities)
            
            //array : Array<UserDM> = [user1]
            array.append(user1)
            countVal = countVal + 1
        }
        
        
        //    print("..AArray.>>",array)
        self.usersData.serverArray = array;
        
        
        //loaderView(isVisible: false)
        if (state) {
            print(jsonValidacion)
            // self.isValidDocument = true
            //self.namesTextField.text = jsonValidacion.nombres
        } else {
            //self.isValidDocument = false
            // showMessageAlert(message: message)
        }
    }
    
    func getCurrentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //locationManager.requestWhenInUseAuthorization()
        requestLocationAuthorization()
    }
    
    func startLocationUpdates(){
        let lat = locationManager.location?.coordinate.latitude
        let long = locationManager.location?.coordinate.longitude
        if let lattitude = lat, let longitude = long {
            latValue = lattitude
            longValue = longitude
        }
    }
    
    fileprivate func localPlace() -> [String] {
        /*  if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
         do {
         let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
         let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [[String:String]]
         
         var countryNames = [String]()
         for country in jsonResult {
         countryNames.append(country["office"]!)
         }
         
         return countryNames
         } catch {
         print("Error parsing jSON: \(error)")
         return []
         }
         }
         
         return []
         */
        
        
        var latLongArray : NSArray?
        if let path = Bundle.main.path(forResource: "stations", ofType: "plist"){
            latLongArray = NSArray(contentsOfFile : path)
        }
        var countryNames = [String]()
        var countryLatitude = [String]()
        var countryLongitude = [String]()
        if let items = latLongArray {
            for item in items {
                countryNames.append((item as AnyObject).value(forKey: "title") as! String)
                // countryNames.append((item as AnyObject).value(forKey: "lat") as! String)
                //  countryNames.append((item as AnyObject).value(forKey: "long") as! String)
                // let latitude    = (item as AnyObject).value(forKey: "lat")
                // let longitude   = (item as AnyObject).value(forKey: "long")
                //  let titleString = (item as AnyObject).value(forKey: "title")
            }
            return countryNames
        }
        return []
        
    }
    
    
    func drawMarker(item:MapaOfficeEntity,indexSearch:Int ,type: String,title: String, lat: CLLocationDegrees , long: CLLocationDegrees,distance: String) {
        
        
        let image = UIImage(systemName: "record.circle")
        
        let markerView = UIImageView(image: image)
        
        switch(type){
        case TypePoint.oficinaRegistral:
            markerView.tintColor = SunarpColors.cyanMapOfiReg
        case TypePoint.oficinaReceptora:
            markerView.tintColor = SunarpColors.yellowMapOfiRec
        case TypePoint.sedeCentral:
            markerView.tintColor = SunarpColors.redMapOfiCen
        case TypePoint.tribunalRegistral:
            markerView.tintColor = SunarpColors.darMapTriReg
        case TypePoint.zonaRegistral:
            markerView.tintColor = SunarpColors.greenMapZonReg
        case TypePoint.oficinaOtras:
            markerView.tintColor = SunarpColors.purpulMapOfiOther
        default:
            markerView.tintColor = SunarpColors.greyBlack
        }
        
        
        let currentLocation = CLLocation(latitude: latValue, longitude: longValue)
        // Coordenadas que deseas mostrar en el mapa
        let desiredCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        // Calcular la distancia entre la ubicación actual y las coordenadas deseadas (en metros)
        let distanceInMeters = currentLocation.distance(from: CLLocation(latitude: desiredCoordinates.latitude, longitude: desiredCoordinates.longitude))
        
        // Convertir la distancia de metros a kilómetros
        let distanceInKilometers = distanceInMeters / 1000.0
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = title
        marker.userData = distance
        marker.iconView = markerView
        if distanceInMeters < 1000 {
            marker.snippet = "\(type) a  \(String(distanceInMeters.redondear(numeroDeDecimales: 2))) m"
        }
        else {
            marker.snippet = "\(type) a \(distance) km"
        }
        
        
        marker.accessibilityLabel = "\(indexSearch)"
        marker.map = mapView
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let index:Int = Int(marker.accessibilityLabel!)!
        
        let kilometro = getMarkerData(from: marker)
        self.confirmPopupMap(index:index,arrayEntity:self.officinasEntities,title: marker.title!,message: marker.snippet!,primaryButton: "",secondaryButton: "",addColor: SunarpColors.red,delegate: self,distancia: kilometro ?? "")
        
        return nil
    }
    
    func getMarkerData(from marker: GMSMarker) -> String? {
            return marker.userData as? String
        }
    
    func drawPath(title: String, lat: CLLocationDegrees , long: CLLocationDegrees) {
        
        
        let url = URL(string: "https://maps.google.com")
        if (UIApplication.shared.canOpenURL(url!))
        {
            let originLat:String = String(locationManager.location!.coordinate.latitude)
            let originLong:String = String(locationManager.location!.coordinate.longitude)
            let destLat:String = String(lat)
            let destLong:String = String(long)
            
            let openUrlString:String = String(format:"https://www.google.com/maps/dir/?api=1&origin=%@,%@&destination=%@,%@&travelmode=driving",originLat,originLong,destLat,destLong)
            
            print("openUrlString is : \(openUrlString)")
            
            
            let openUrl = URL(string: openUrlString)
            UIApplication.shared.open(openUrl!, options: [:]) { (res:Bool) in
                
            }
        }
        
    }
}

// MARK: - Table view datasource
extension MapaViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        // return self.dateDataScheduleModel?.rows.count ?? 0
        return array.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "ServicesTableView", for: indexPath) as? ServicesTableView
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MapaViewCell", for: indexPath) as? MapaViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        //cell.subjectList = array
        cell.configure(title: array[indexPath.row])
        return cell
    }
}

// MARK: - Table view delegate
extension MapaViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableView {
            
            let listMap = self.array[indexPath.row]
            var filtered : [MapaOfficeEntity] = []
            
            if(listMap == Constant.Localize.allPoints){
                filtered = officinasEntities
            }else {
                filtered = officinasEntities.filter { $0.type == listMap  }
            }
            
            mapView.clear()
            flagButton = true
            imgIcono.image =  SunarpImage.getImage(named: .iconMapDark)
            imgView.backgroundColor = SunarpColors.white
            self.tableView.isHidden = true
            
            var array : Array<UserDM> = []
            var countVal : Int = 0
            for item in filtered {
                let latitude    = NSNumber(value:Double(item.lat) ?? 0.0)
                //let latitude    = (item as AnyObject).value(forKey: "lat")
                let longitude   = NSNumber(value:Double(item.lon) ?? 0.0)
                let titleString = item.office
                let titleAdreesString = item.address
                let titleTypeString = item.type
                
                let Punto1 = CLLocation(latitude: latValue, longitude: longValue)
                let Punto2 = CLLocation(latitude: latitude as! CLLocationDegrees , longitude: longitude as! CLLocationDegrees)
                
                let DistanciaEnMetros = Punto1.distance(from: Punto2)
                
                let distanKm = Double(DistanciaEnMetros/1000)
                distanKmR = distanKm.redondear(numeroDeDecimales: 2)
                print("..ADistanciaEnMetros.>>",DistanciaEnMetros)
                
                drawMarker(item:item,indexSearch:countVal,type:titleTypeString,title: titleString, lat: latitude as! CLLocationDegrees , long: longitude as! CLLocationDegrees,distance:String(distanKmR))
                
                countVal = countVal + 1
                
            }
        }
    }
    
}

extension MapaViewController: MarketWindowDelegate, ShowDetailMapProtocol{
    func showDetail(items: [MapaOfficeEntity], index: Int) {
        let storyboard = UIStoryboard(name: "Mapa", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "MapaDetailViewController") as? MapaDetailViewController {
            vc.arrayEntity = items
            vc.index = index
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showRouteMap(item: MapaOfficeEntity) {
        drawRoute(item: item)
    }
    
    func showShareSheet(item: MapaOfficeEntity) {
        
    }
    
    func primary(action:[MapaOfficeEntity],index:Int) {
        let storyboard = UIStoryboard(name: "Mapa", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "MapaDetailViewController") as? MapaDetailViewController {
            vc.arrayEntity = action
            vc.index = index
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func secondary() {
    }
}

extension MapaViewController: PointsDrawingGoogle{
    func showRoutePointsDrawing(item: MapaOfficeEntity) {
        drawRoute(item: item)
    }
    
    func drawRoute(item: MapaOfficeEntity) {
        let origin = "\(item.lat),\(item.lon)"
        let destination = "\(latValue),\(longValue)"
        let apiKey = Constant.Localize.apiKeyGooleMaps
        
        let url = "\(Constant.Localize.urlGoogleDirections)"+"origin=\(origin)&destination=\(destination)&key=\(apiKey)"
        
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default)
            .validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if let json = value as? [String: Any],
                       let routes = json["routes"] as? [[String: Any]],
                       let route = routes.first,
                       let overviewPolyline = route["overview_polyline"] as? [String: Any],
                       let points = overviewPolyline["points"] as? String {
                        
                        // Decodificar las coordenadas de la ruta desde el string codificado
                        if let path = GMSPath(fromEncodedPath: points) {
                            // Crear una polilínea y agregarla al mapa
                            self.polyline = GMSPolyline(path: path)
                            self.polyline?.strokeColor = UIColor.red
                            self.polyline?.strokeWidth = 3.0
                            self.polyline?.map = self.mapView
                            self.imgcCleanRoutes.isHidden = false
                        }
                    }
                case .failure(let error):
                    print("Error al obtener la ruta: \(error)")
                    break
                }
            }
    }
    
}

extension Double {
    func redondear(numeroDeDecimales: Int) -> String {
        let formateador = NumberFormatter()
        formateador.maximumFractionDigits = numeroDeDecimales
        formateador.roundingMode = .down
        return formateador.string(from: NSNumber(value: self)) ?? ""
    }
}


enum TypePoint {
    static let oficinaRegistral = "Oficina Registral"
    static let oficinaReceptora = "Oficina Receptora"
    static let sedeCentral = "Sede Central"
    static let tribunalRegistral = "Tribunal Registral"
    static let zonaRegistral = "Zona Registral"
    static let oficinaOtras = "Oficina Otras"
}


extension MapaViewController {
    
    func requestLocationAuthorization() {
        let authorizationStatus: CLAuthorizationStatus
        let manager = CLLocationManager()
        
        if #available(iOS 14, *) {
            authorizationStatus = manager.authorizationStatus
        } else {
            authorizationStatus = CLLocationManager.authorizationStatus()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch authorizationStatus {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted, .denied:
                showLocationPermissionAlert()
            case .authorizedAlways, .authorizedWhenInUse:
                startLocationUpdates()
            @unknown default:
                break
            }
        } else {
            showLocationServicesDisabledAlert()
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            startLocationUpdates()
        case .restricted, .denied:
            showLocationPermissionAlert()
        case .notDetermined:
            // The user has not yet made a decision regarding location services. Handle it accordingly.
            break
        @unknown default:
            break
        }
    }
}


extension MapaViewController {
    
    func showLocationPermissionAlert() {
        let alert = UIAlertController(title: "Permiso de ubicación", message: "Debe habilitar los servicios de ubicación para la aplicación para poder usar el mapa.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Ajustes", style: .default) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showLocationServicesDisabledAlert() {
        let alert = UIAlertController(title: "Servicios de ubicación deshabilitados", message: "Los servicios de ubicación están actualmente deshabilitados en su dispositivo. Habilítelos en Configuración para usar el mapa.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    // Other alert functions...
    
}
