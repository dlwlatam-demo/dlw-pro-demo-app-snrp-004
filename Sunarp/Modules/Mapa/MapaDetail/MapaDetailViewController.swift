//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//


import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import CoreLocation
import SearchTextField
import HSSearchable

protocol PointsDrawingGoogle {
    func showRoutePointsDrawing(item: MapaOfficeEntity)
}

class MapaDetailViewController: UIViewController, CLLocationManagerDelegate {

    let disposeBag = DisposeBag()
    var window: UIWindow?
    
    var delegate: PointsDrawingGoogle?

    @IBOutlet weak var formView: UIView!
    
    @IBOutlet weak var formViewCompartir: UIView!
    @IBOutlet weak var formViewLlamar: UIView!
    @IBOutlet weak var formViewRuta: UIView!
    
    @IBOutlet weak var stackRuta: UIStackView!
    @IBOutlet weak var stackShare: UIStackView!
    @IBOutlet weak var stackCall: UIStackView!
    
    
    let locationManager = CLLocationManager()
    var latValue = CLLocationDegrees()
    var longValue = CLLocationDegrees()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var DirecLabel: UILabel!
    @IBOutlet weak var horarioLabel: UILabel!
    @IBOutlet weak var phoneAnexLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var titleDistLabel: UILabel!
    var index:Int!
    var distance:String = ""
    var distanKmR = ""
    var usersData = SearchableWrapper()
    var users: [UserDM] {
        return self.usersData.dataArray as! [UserDM]
    }
    
    //var array : Array<MapaOfficeEntity> = []
    //[MapaOfficeEntity]
    var arrayEntity: [MapaOfficeEntity] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
       
        setupNavigationBar()
        
        print("nAarrayEntity:",arrayEntity)
        print("indexJJJty:",index)
        
        titleLabel.text = arrayEntity[index].office
        subtitleLabel.text = arrayEntity[index].self.type
        DirecLabel.text = arrayEntity[index].self.address
        var valSchedule:String = ""
        var valScheduleSecond:String  = ""
        var valScheduleTree:String  = ""
        print("schedules")
        dump(arrayEntity[index].self.arrayAttention[0])
        let numHorarios = arrayEntity[index].self.arrayAttention[0].schedules.count
        var strHorarios:String = ""
        for i in 0...(numHorarios - 1) {
            valSchedule = (arrayEntity[index].self.arrayAttention[0].schedules[i] as? String)!
            strHorarios += "\(valSchedule)\n\n"
        }
        horarioLabel.text = strHorarios
        
        phoneAnexLabel.text = arrayEntity[index].self.arrayPhone[0].annex
        phoneLabel.text = arrayEntity[index].self.arrayPhone[0].phone
        
        
        //map distance
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization() // Call the authorizationStatus() class

        if CLLocationManager.locationServicesEnabled() { // get my current locations lat, lng
            
            let lat = locationManager.location?.coordinate.latitude
            let long = locationManager.location?.coordinate.longitude
            if let lattitude = lat  {
                if let longitude = long {
                    latValue = lattitude
                    longValue = longitude
                } else {
                    
                }
            } else {
                
                print("problem to find lat and lng")
            }
            
        }
        
        let latitude    = NSNumber(value:Double(arrayEntity[index].lat) ?? 0.0)
        //let latitude    = (item as AnyObject).value(forKey: "lat")
        let longitude   = NSNumber(value:Double(arrayEntity[index].lon) ?? 0.0)
        
        let Punto1 = CLLocation(latitude: latValue, longitude: longValue)
        let Punto2 = CLLocation(latitude: latitude as! CLLocationDegrees , longitude: longitude as! CLLocationDegrees)

        let DistanciaEnMetros = Punto1.distance(from: Punto2)
        
        let distanKm = Double(DistanciaEnMetros/1000)
        distanKmR = distanKm.redondear(numeroDeDecimales: 2)
        print("..ADistanciaEnMetros.>>",DistanciaEnMetros)
        print("distanKmR ",distanKmR)
        //distance:String(distanKmR)
        if DistanciaEnMetros < 1000 {
            titleDistLabel?.text =   "\(String(DistanciaEnMetros.redondear(numeroDeDecimales: 2))) m"
        }
        else {
            titleDistLabel?.text =   "\(String(distanKmR)) km"
        }
        
    }

    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Oficina registral")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
        
    }
    
    // MARK: - Actions
    @IBAction func goBack() {
        
       // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupDesigner() {
        
        
    
        self.formView.backgroundCard()
        
        
  
      //  self.formViewEditar.backgroundCard()
       
    }
    
    private func addGestureView(){
    
        let tapCompartirGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToCompartirView))
        self.stackShare.addGestureRecognizer(tapCompartirGesture)
        
        
        let tapLlamarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToLlamarView))
        self.stackCall.addGestureRecognizer(tapLlamarGesture)
        
        
        let tapRutaGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapToRutaView))
        self.stackRuta.addGestureRecognizer(tapRutaGesture)
    }
    
    
    @objc private func onTapToCompartirView() {
        let latitude = arrayEntity[index].lat
        let longitude = arrayEntity[index].lon
        let locationURL = URL(string: "http://maps.apple.com/?ll=\(latitude),\(longitude)")
        
        if UIApplication.shared.canOpenURL(locationURL!) {
            let activityViewController = UIActivityViewController(activityItems: [locationURL!], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Aplicación de mapas no encontrada",
                                                    message: "Instale una aplicación de mapas para compartir la ubicación.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc private func onTapToLlamarView() {
  
       let phone = arrayEntity[index].self.arrayPhone[0].phone
        if !phone.isEmpty {
            let replace1 = phone.replacingOccurrences(of: "(01)", with: "01")
            let phoneNumber = phone.replacingOccurrences(of: "-", with: "")
            
            if let phoneURL = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(phoneURL) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(phoneURL)
                } else {
                    UIApplication.shared.openURL(phoneURL)
                }
            } else {
                let alertController = UIAlertController(title: "Error", message: "Las llamadas telefónicas no son compatibles con este dispositivo.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc private func onTapToRutaView() {
        if !distanKmR.isEmpty {
            if let kilometro = Double(distanKmR), kilometro  > 10.0 {
                showAlertKilometros()
            }else {
                delegate?.showRoutePointsDrawing(item: arrayEntity[index])
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func showAlertKilometros(){
        let alert = UIAlertController(title: "Información", message: "La distancia es mayor a 10 Km", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func onTapToConfirmView() {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailConsultaVehicularViewController") as! DetailConsultaVehicularViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
     
}



