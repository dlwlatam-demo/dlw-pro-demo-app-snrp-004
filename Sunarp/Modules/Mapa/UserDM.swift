//
//  UserDM.swift
//  HSSearchable
//
//  Created by Hitendra iDev on 28/08/17.
//  Copyright © 2017 Hitendra iDev. All rights reserved.
//

import Foundation
import HSSearchable

struct UserDM {

    var office: String
    var addres: String
    var type: String
    var lat: String
    var lon: String
    var indexSearch: Int
    
    var distance: String
    var item: [MapaOfficeEntity]
}

extension UserDM: SearchableData {
    /*
     this will only search from the name
     */
//    var searchValue: String{
//        return self.name
//    }
    
    /*titleText
     this will search from the name and city both
     */
    var searchValue: String{
        return self.office + " " + self.addres + " " + self.type + " " + self.lat + " " + self.lon + " " + self.distance
    }
}
