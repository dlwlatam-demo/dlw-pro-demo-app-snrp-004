//
//  InicioViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 30/07/22.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import CoreLocation
import SearchTextField
import HSSearchable

protocol ShowDetailMapProtocol {
    func showDetail(items: [MapaOfficeEntity], index: Int)
}

class MapaSearchViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    
    let disposeBag = DisposeBag()
    var window: UIWindow?
    var delegate:ShowDetailMapProtocol?
    
    @IBOutlet weak var formTableView: UIView!
    @IBOutlet weak var busquedaTxtFiel: UITextField!
    
    let locationManager = CLLocationManager()
    var latValue = CLLocationDegrees()
    var longValue = CLLocationDegrees()
    var markerArray = Array<Any>()
    var i : Int = 0
    var origin = String()
    var destination = String()
    var previousLat = CLLocationDegrees()
    var previousLong = CLLocationDegrees()
    var officinasEntities: [MapaOfficeEntity] = []
    
    var loading: UIAlertController!
    
    private lazy var presenter: MapaSearchPresenter = {
        return MapaSearchPresenter(controller: self)
    }()
    
    
    
    @IBOutlet var tableView:UITableView!
    @IBOutlet var tableSearchView:UITableView!
    
    //let array = ["Oficina Registral","Oficina Receptora","Oficina Central","Tribunal Registral","Zona Registral","Oficina Otras"]
    
    //MARK: iVars
    var usersData = SearchableWrapper()
    var users: [UserDM] {
        return self.usersData.dataArray as! [UserDM]
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var searchbar: UISearchBar!
    
    var array : Array<UserDM> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigner()
        addGestureView()
        
        setupNavigationBar()
        
        
        
        // formTableView.isHidden = true
        
        
        //mapiperu2019
        registerCell()
        //self.presenter.didLoad()
        
        
        // 1 - Configure a simple search text field
        
       // print("..AArray.>>",array)
        self.usersData.serverArray = array;
        self.tableSearchView.reloadData()
        
        //set anyone delegate of two based on your requiredments
        self.searchbar.delegate = self.usersData
        tableSearchView.tableHeaderView = self.searchbar
        //self.usersData.customDelegate = self;
        
        self.usersData.searchingCallBack = { isSearching, searchText in
            self.tableSearchView.reloadData()
        }
        // self.loadDummyData()
    }
    
    
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Nuestras Oficinas")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        // router.pop(sender: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - search
    // 1 - Configure a simple search text view
    /* fileprivate func configureSimpleSearchTextField() {
     // Start visible even without user's interaction as soon as created - Default: false
     busquedaTxtFiel.startVisibleWithoutInteraction = false
     
     // Set data source
     let countries = localPlace()
     busquedaTxtFiel.filterStrings(countries)
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    // Hide keyboard when touching the screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    private func setupDesigner() {
        //  self.formView.backgroundCard()
        //  self.formViewEditar.backgroundCard()
        tableSearchView.delegate = self
        tableSearchView.dataSource = self
    }
    func registerCell() {
        //Celda Foto perfil
        let profileCellSearchNib = UINib(nibName: MapaSearchViewCell.reuseIdentifier, bundle: nil)
        tableSearchView.register(profileCellSearchNib, forCellReuseIdentifier: MapaSearchViewCell.reuseIdentifier)
    }
    private func addGestureView(){
        
    }
    
    @objc private func onTapToBusqView() {
        
        
    }
    
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            self.stopLoader(loader: self.loading)
        }
    }
    func loadDatosMapa(_ state: Bool, message: String, jsonValidacion: MapaEntity) {
        
        
        officinasEntities = jsonValidacion.dataMap
        
        var array : Array<UserDM> = []
        for item in jsonValidacion.dataMap {
            let latitude    = NSNumber(value:Double(item.lat) ?? 0.0)
            //let latitude    = (item as AnyObject).value(forKey: "lat")
            let longitude   = NSNumber(value:Double(item.lon) ?? 0.0)
            let titleString = item.office
            let titleAdreesString = item.address
            let titleTypeString = item.type
            
            //  let user1 = UserDM(office: titleString, addres: titleAdreesString, type: titleTypeString,lat:item.lat,lon:item.lon)
            
            //array : Array<UserDM> = [user1]
            // array.append(user1)
        }
        
      //  print("..AArray.>>",array)
        self.usersData.serverArray = array;
        self.tableSearchView.reloadData()
        
        loaderView(isVisible: false)
        if (state) {
           // print(jsonValidacion)
            // self.isValidDocument = true
            //self.namesTextField.text = jsonValidacion.nombres
        } else {
            //self.isValidDocument = false
            // showMessageAlert(message: message)
        }
    }
    
    
    
}



// MARK: - Table view datasource
extension MapaSearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  print("num:::",self.dateDataScheduleModel?.rows.count as Any)
        return self.users.count;
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MapaSearchViewCell", for: indexPath) as? MapaSearchViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        cell.user = self.users[indexPath.row]
        return cell
    }
}

// MARK: - Table view delegate
extension MapaSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let listMap = self.users[indexPath.row]
        navigationController?.popViewController(animated: false)
        delegate?.showDetail(items: listMap.item, index: listMap.indexSearch)
        
       /* let storyboard = UIStoryboard(name: "Mapa", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "MapaDetailViewController") as? MapaDetailViewController {
            vc.arrayEntity = listMap.item
            vc.distance = listMap.distance
            vc.index = listMap.indexSearch
            self.navigationController?.pushViewController(vc, animated: true)
        }*/
        
    }
}

extension MapaSearchViewController: MarketWindowDelegate{
    func showRouteMap(item: MapaOfficeEntity) {
        print("showRouteMap from search")
    }
    
    func showShareSheet(item: MapaOfficeEntity) {
        
    }
    
    func primary(action: [MapaOfficeEntity],index:Int) {
        
    }
    
    func secondary() {
    }
}
