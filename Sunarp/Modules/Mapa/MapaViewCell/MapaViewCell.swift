//
//  ProfileViewCell.swift
//  App
//
//  Created by SmartDoctor on 6/14/20.
//  Copyright © 2020 SmartDoctor. All rights reserved.
//

import UIKit
//import SDWebImage

final class MapaViewCell: UITableViewCell {
    private var style = Style.myApp
    static let reuseIdentifier = "MapaViewCell"
    //  @IBOutlet var profileImage:UIImageView!
    @IBOutlet var titleNameLabel:UILabel!
    // @IBOutlet var titleEmail:UILabel!
    @IBOutlet var viewCell:UIView!
    @IBOutlet weak var viewPointCell: UIImageView!
    private var title: String = ""
    
    func configure(title: String) {
        self.title = title
        titleNameLabel?.text = self.title
        configurePoint(value: title)
        

       /* if self.title == "Oficina Registral"{
            viewPointCell.backgroundColor = SunarpColors.cyanMapOfiReg
        }else if self.title == "Oficina Receptora"{
            viewPointCell.backgroundColor = SunarpColors.yellowMapOfiRec
        }else if self.title == "Sede Central"{
            viewPointCell.backgroundColor = SunarpColors.redMapOfiCen
        }else if self.title == "Tribunal Registral"{
            viewPointCell.backgroundColor = SunarpColors.darMapTriReg
        }else if self.title == "Zona Registral"{
            viewPointCell.backgroundColor = SunarpColors.greenMapZonReg
        }else if self.title == "Oficina Otras"{
            viewPointCell.backgroundColor = SunarpColors.purpulMapOfiOther
            
        }*/
    }
    
    func configurePoint(value: String) {
        switch(value){
        case TypePoint.oficinaRegistral:
            viewPointCell.tintColor = SunarpColors.cyanMapOfiReg
        case TypePoint.oficinaReceptora:
            viewPointCell.tintColor = SunarpColors.yellowMapOfiRec
        case TypePoint.sedeCentral:
            viewPointCell.tintColor = SunarpColors.redMapOfiCen
        case TypePoint.tribunalRegistral:
            viewPointCell.tintColor = SunarpColors.darMapTriReg
        case TypePoint.zonaRegistral:
            viewPointCell.tintColor = SunarpColors.greenMapZonReg
        case TypePoint.oficinaOtras:
            viewPointCell.tintColor = SunarpColors.purpulMapOfiOther
        default:
            viewPointCell.tintColor = SunarpColors.greyBlack
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //style.apply(textStyle: .title, to: self.titleNameLabel)
        
        
        
        viewPointCell.layer.cornerRadius = 6
        viewCell.backgroundColor = UIColor.white
        // viewPointCell.backgroundColor = UIColor.green
        //viewCell.addShadowViewCustom(cornerRadius: 10.0)
        
        
        //titleLabel.textColor =
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /*
     func loadWithData(info:DoctorProperties){
     
     let fileName = info.photo!
     let fileArray = fileName.components(separatedBy: "/")
     let finalFileName = fileArray.last
     self.profileImage.sd_setImage(with: URL(string: info.photo!), placeholderImage: UIImage(named: String(finalFileName!)))
     self.titleNameLabel.text = "\(String(info.first_name!)) \n \(String(info.last_name!))"
     //self.titleEmail.text = "\(String(info.bank_email!))"
     }
     
     func loadWithDataFicha(info:MedicalAttentionSDProCompleted){
     
     let fileName = info.patient_photo!
     let fileArray = fileName.components(separatedBy: "/")
     let finalFileName = fileArray.last
     self.profileImage.sd_setImage(with: URL(string: info.patient_photo!), placeholderImage: UIImage(named: String(finalFileName!)))
     self.titleNameLabel.text = "\(String(info.patient_name!))"
     self.titleEmail.text = ""
     //self.titleEmail.text = "\(String(info.patient_name!))"
     }
     */
}


