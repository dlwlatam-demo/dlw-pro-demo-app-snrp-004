//
//  PaymentBuilder.swift
//  Sunarp
//
//  Created by Marin Espinoza Ramirez on 29/10/23.
//

import Foundation

enum PaymentBuilder {
    
    static func paymentRegistroPerNat(item: TransactionData,
                                      certificadoId:String,
                                      paymentItem:PaymentItem,
                                      guardarSolicitudEntity: GuardarSolicitudEntity) -> PaymentProcessRequest {
        
        let payment = PaymentProcessRequest()
        payment.solicitudId = guardarSolicitudEntity.solicitudId
        payment.costoTotal = item.dataMap.amount
        payment.ip = "10.129.6.1"
        payment.usrId = "APPSNRPIOS"
        payment.idUser = paymentItem.usrId
        payment.idUnico = item.dataMap.transactionID
        payment.pan = item.dataMap.card
        payment.dscCodAccion = item.dataMap.actionDescription
        payment.codAutoriza = item.dataMap.authorizationCode
        payment.codtienda = item.dataMap.merchant
        payment.numOrden = item.order.purchaseNumber
        payment.codAccion = "000"
        payment.fechaYhoraTx = getCurrentDateAndTime()
        payment.nomEmisor = item.dataMap.brand
        payment.oriTarjeta = item.dataMap.cardType
        payment.transId = item.order.transactionID
        payment.eticket =  item.order.transactionID
        payment.codCerti = certificadoId
        payment.cantPaginas = "0"
        payment.cantPaginasExon = "0"
        payment.paginasSolicitadas = "0"
        payment.totalPaginasPartidaFicha = "0"
       
        let visaNetRequest = payment.visanetResponse
        visaNetRequest.codAccion = "000"
        visaNetRequest.codAutoriza = item.dataMap.authorizationCode
        visaNetRequest.codtienda = item.dataMap.merchant
        visaNetRequest.dscCodAccion = item.dataMap.actionDescription
        visaNetRequest.dscEci = item.dataMap.eciDescription
        visaNetRequest.eci = item.dataMap.eci
        visaNetRequest.estado = item.dataMap.status
        visaNetRequest.eticket = item.order.transactionID
        visaNetRequest.fechaYhoraTx = getCurrentDateAndTime()
        visaNetRequest.idUnico =  item.dataMap.transactionID
        visaNetRequest.idUser = paymentItem.usrId
        visaNetRequest.impAutorizado = item.dataMap.amount
        visaNetRequest.nomEmisor =  item.dataMap.brand
        visaNetRequest.numOrden = item.order.purchaseNumber
        visaNetRequest.numReferencia = item.dataMap.traceNumber
        visaNetRequest.oriTarjeta = item.dataMap.cardType
        visaNetRequest.pan =  item.dataMap.card
        visaNetRequest.transId =  item.order.transactionID
        
        payment.visanetResponse = visaNetRequest
        payment.email = item.dataMap.email
        
        return payment
    }
    
    static func getCurrentDateAndTime() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let now = Date()
        return dateFormatter.string(from: now)
    }
}
