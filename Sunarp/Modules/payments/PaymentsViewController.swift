import Foundation
import UIKit
import VisaNetSDK

class PaymentsViewController: UIViewController {
    
    var paymentItem : PaymentItem = PaymentItem()
    var areaRegId : String = ""
    var certificadoId : String = ""
    var titleBar : String = ""
    var precOfic : String = ""
    var tipoNumero : String = ""
    
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var areaRegistralText: UITextField!
    @IBOutlet weak var nombreText: SDCTextField!
    @IBOutlet weak var apellidoPaternoText: SDCTextField!
    @IBOutlet weak var apellidoMaternoText: SDCTextField!
    @IBOutlet weak var costoTotalLabel: UILabel!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var pagarView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var nameRazonSocialText: SDCTextField!
    @IBOutlet weak var numDocText: SDCTextField!
    
    var style: Style = Style.myApp
    var zonas: [ZonaEntity] = []
    var areas: [AreaEntity] = []
    var grupos: [GrupoEntity] = []
    var loading: UIAlertController!
    var isChecked: Bool = false
    var areaRegistralPickerView = UIPickerView()
    var participantePickerView = UIPickerView()
    let boldAttrs = [NSAttributedString.Key.font :  SunarpFont.bold14]
    let normalAttrs = [NSAttributedString.Key.font : SunarpFont.regular14]
    let normalAttrs1: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: SunarpFont.regular10]
    var visaKeys: VisaKeysEntity!
    var tokenNiubiz: String = ""
    var nsolMonLiq: String = "0.0"
    
    
    var tipoPer: String = ""
    var refNumPart: String = ""
    var codArea: String = ""
    
    var countCantPag: String = ""
    var countCantPagExo: String = ""
    
    var tipoDocumentos: [String] = ["DNI", "CE"]
    var tipoDocumentosEntities: [TipoDocumentoEntity] = []
    var typeNameDocument: String = ""
    var typeDocument: String = ""
    
    var montoCalc: String = "0.0"
    
    var numeroPlaca: String = ""
    
    var codLibro: String = ""
    var numPartida: String = ""
    var fichaId: String = ""
    var tomoId: String = ""
    var fojaId: String = ""
    var ofiSARP: String = ""
    var coServicio: String = ""
    var coTipoRegis: String = ""
    
    var imPagiSIR: String = ""
    var nuAsieSelectSARP: String = ""
    var nuSecu: String = ""
    
    var valoficinaOrigen: String = ""
    var formaEnvio: String = "V"
    
    var titleText: String = ""
    var pinHash: String = ""
    var transactionIdNiubiz: String = ""
    var tipoCertificadoEntity:TipoCertificadoEntity?
    
    var codigoGla: String = ""
    
    @IBOutlet weak var toPersonNatuView: UIView!
    @IBOutlet weak var toPersonJuriView: UIView!
    
    @IBOutlet weak var opeDetail: UILabel!
    @IBOutlet weak var toConfirmViewAnio: UIView!
    @IBOutlet weak var toConfirmViewNum: UIView!
    @IBOutlet weak var imgRadioButtonAnio: UIImageView!
    @IBOutlet weak var imgRadioButtonNum: UIImageView!
    var guardarSolicitudEntity:GuardarSolicitudEntity?
    var pagoExitoso:PagoExitosoEntity?
    private lazy var presenter: PaymentsPresenter = {
        return PaymentsPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDesigner()
        setupGestures()
        setupKeyboard()
        setValues()
        presenter.didLoad()
    }
    
    
    // MARK: - Setup
    func setupNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        loadNavigationBar(hideNavigation: false, title: "Detalle de la solicitud")
        addNavigationLeftOption(target: self, selector: #selector(goBack), icon: SunarpImage.getImage(named: .iconBackWhite))
    }
    // MARK: - Actions
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        //self.presenter.willAppear()
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func setZonaList(zonaList: [ZonaEntity]) {
        for item in zonaList {
            if (item.select) {
                self.zonas.append(item)
            }
        }
    }
    
    private func setupDesigner() {
        self.formView.backgroundCard()
        self.areaRegistralText.borderAndPaddingLeftAndRight()
        self.nombreText.borderAndPaddingLeftAndRight()
        self.apellidoPaternoText.borderAndPaddingLeftAndRight()
        self.apellidoMaternoText.borderAndPaddingLeftAndRight()
        self.pagarView.primaryButton()
        self.checkView.borderCheckView()
        self.pagarView.primaryDisabledButton()
        
        self.nameRazonSocialText.borderAndPaddingLeftAndRight()
        self.numDocText.borderAndPaddingLeftAndRight()
        
        self.isChecked = false
        self.areaRegistralPickerView.tag = 1
        self.participantePickerView.tag = 2
        
        self.areaRegistralPickerView.delegate = self
        self.areaRegistralPickerView.dataSource = self
        self.areaRegistralText.inputView = self.areaRegistralPickerView
        self.areaRegistralText.inputAccessoryView = self.createToolbar()
      
        
        let item = paymentItem
        montoCalc = item.costoTotal
        opeDetail.text = titleBar
        // var precOfic = item.costoServicio
        self.nsolMonLiq = String(format: "%.2f", item.costoTotal)
        
        
        let textColor = UIColor.lightGray
        let amount = String(format: "S/ %.2f", (montoCalc as NSString).doubleValue)
        
        let costoTitle = NSMutableAttributedString(string: "", attributes: normalAttrs  as [NSAttributedString.Key : Any])
        let costoValue = NSMutableAttributedString(string: amount, attributes: normalAttrs1)
        costoValue.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSRange(location: 0, length: costoValue.length))
        let opeDetailAttributes = NSMutableAttributedString(string: titleBar, attributes: normalAttrs1)
        opeDetailAttributes.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSRange(location: 0, length: opeDetailAttributes.length))
        
        costoTitle.append(costoValue)
        self.costoTotalLabel.attributedText =  costoTitle
        self.opeDetail.attributedText = opeDetailAttributes
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardByTouchOutsideTextfield (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapChecked = UITapGestureRecognizer(target: self, action: #selector(self.onTapChecked))
        self.checkView.addGestureRecognizer(tapChecked)
        
        let tapTerminoCondicionesGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapTerminoCondicionesLabel))
        self.terminosLabel.addGestureRecognizer(tapTerminoCondicionesGesture)
        
        let tapPagarGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapPagarView))
        self.pagarView.addGestureRecognizer(tapPagarGesture)
        
        let tapConfirmGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewAnio))
        self.toConfirmViewAnio.addGestureRecognizer(tapConfirmGesture1)
        
        let tapConfirmGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.onTapToConfirmViewNum))
        self.toConfirmViewNum.addGestureRecognizer(tapConfirmGesture2)
        
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image  = SunarpImage.getImage(named: .iconRadioButtonGray)
        //self.tipo = "T"
    }
    
    func loadTipoDocumentos(arrayTipoDocumentos: [TipoDocumentoEntity]) {
        self.tipoDocumentosEntities = arrayTipoDocumentos
        self.tipoDocumentos = arrayTipoDocumentos.map{$0.nombreAbrev}
        for (indice, valor) in tipoDocumentosEntities.enumerated() {
            if tipoPer == "N" && valor.nombreAbrev == "DNI" {
                self.areaRegistralText.text = tipoDocumentosEntities[indice].nombreAbrev
                self.typeDocument = tipoDocumentosEntities[indice].tipoDocId
            }
            
            if tipoPer == "J" && valor.nombreAbrev == "R.U.C." {
                self.areaRegistralText.text = tipoDocumentosEntities[indice].nombreAbrev
                self.typeDocument = tipoDocumentosEntities[indice].tipoDocId
            }
        }
    }
    
    @objc private func onTapToConfirmViewAnio() {
        self.tipoPer = "N"
        self.typeDocument = "09"
        self.numDocText.maxLengths = 8
        
        presenter.didLoad()
        clearTextField()
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        toPersonNatuView.isHidden = false
        toPersonJuriView.isHidden = true
    }
    
    @objc private func onTapToConfirmViewNum() {
       
        presenter.didLoadJur()
        clearTextField()
        imgRadioButtonAnio.image = SunarpImage.getImage(named: .iconRadioButtonGray)
        imgRadioButtonNum.image = SunarpImage.getImage(named: .iconRadioButtonGreen)
        toPersonNatuView.isHidden = true
        toPersonJuriView.isHidden = false
        
        self.tipoPer = "J"
        self.typeDocument = "05"
        self.numDocText.maxLengths = 11
    }
    
    func clearTextField() {
        nombreText.text = .empty
        apellidoMaternoText.text = .empty
        apellidoPaternoText.text = .empty
        nameRazonSocialText.text = .empty
        numDocText.text = .empty
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelButton = UIBarButtonItem(title: Constant.Localize.cancelar,
                                           style: .plain, target: nil, action: #selector(cancelPressed))
        
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton], animated: true)
        return toolbar
    }
    
    func setValues() {
        
        let usuario = UserPreferencesController.usuario()
        
        self.nombreText.text          = usuario.nombres
        self.apellidoPaternoText.text = usuario.priApe
        self.apellidoMaternoText.text = usuario.segApe
        self.nameRazonSocialText.text = usuario.nombres
        self.numDocText.text          = usuario.nroDoc
        
        self.tipoPer = "N"
        self.typeDocument = "09"
        numDocText.delegate = self
        nameRazonSocialText.delegate = self
        apellidoPaternoText.delegate = self
        apellidoMaternoText.delegate = self
        nombreText.delegate = self
        
        numDocText.maxLengths = 8
        numDocText.valueType = .onlyNumbers
        
        nameRazonSocialText.valueType = .razonSocial
        apellidoPaternoText.valueType = .alphaNumericWithSpace
        apellidoMaternoText.valueType = .alphaNumericWithSpace
        nombreText.valueType = .alphaNumericWithSpace
    }
    
    @objc func dismissKeyboardByTouchOutsideTextfield (_ sender: UITapGestureRecognizer) {
        self.nombreText.resignFirstResponder()
        self.apellidoPaternoText.resignFirstResponder()
        self.apellidoMaternoText.resignFirstResponder()
        self.numDocText.resignFirstResponder()
    }
    
    @objc private func onTapTerminoCondicionesLabel(){
        self.alertTermyConPopupDialog(title: "Estimado(a) usuario(a):",message: "¿Está seguro que desea remover el mandato?",primaryButton: "No,regresar",secondaryButton: "Si,remover", addColor: SunarpColors.red, delegate: self)
    }
    
    @objc private func onTapPagarView(){
        if (self.isChecked) {
            self.pagarView.primaryDisabledButton()
            print("apellidoMaternoText"," "+self.apellidoMaternoText.text!)
            print("apellidoPaternoText "," "+self.apellidoPaternoText.text!)
            paymentItem.nombre = self.nombreText.text ?? .empty
            paymentItem.apeMaterno = self.apellidoMaternoText.text ?? .empty
            paymentItem.apePaterno = self.apellidoPaternoText.text ?? .empty
            let razonSocial = self.nameRazonSocialText.text ?? .empty
            paymentItem.razSoc =  tipoPer == "J" ? razonSocial : .empty
            paymentItem.tpoPersona = tipoPer
            paymentItem.tpoDoc = typeDocument
            paymentItem.numDoc = numDocText.text ?? .empty
            print("certificadoId"," "+certificadoId)
            print(paymentItem.costoTotal)
            if  paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_NEGATIVO_PREDIOS || certificadoId == Constant.CERTIFICADO_NEGATIVO_PREDIOS_B || certificadoId == Constant.CERTIFICADO_POSITIVO_PREDIOS || certificadoId == Constant.CERTIFICADO_POSITIVO_PREDIOS_B || certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR || certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B   || certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR || certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B ) {
                
                
                presenter.saveRequestPropertyRegister(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, tpoPersona: paymentItem.tpoPersona,
                                                      apePaterno: paymentItem.apePaterno,
                                                      apeMaterno: paymentItem.apeMaterno,
                                                      nombre: paymentItem.nombre,
                                                      razSoc: paymentItem.razSoc,
                                                      tpoDoc: paymentItem.tpoDoc,
                                                      numDoc: paymentItem.numDoc,
                                                      email: paymentItem.email,
                                                      tipPerPN: paymentItem.tipPerPN,
                                                      apePatPN: paymentItem.apePatPN,
                                                      apeMatPN: paymentItem.apeMatPN,
                                                      nombPN: paymentItem.nombPN,
                                                      razSocPN: paymentItem.razSocPN,
                                                      tipoDocPN: paymentItem.tipoDocPN,
                                                      numDocPN: paymentItem.numDocPN,
                                                      costoServicio: paymentItem.costoServicio,
                                                      costoTotal: paymentItem.costoTotal,
                                                      usrId: paymentItem.usrId)
            } else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_DE_CARGAS_Y_GRAVAMENES ) {
                
                print("CERTIFICADO_DE_CARGAS_Y_GRAVAMENES")
                presenter.postDetalleAsientosPublicaCargaSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, codLibro: paymentItem.codLibro, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart:  paymentItem.refNumPart, partida: paymentItem.partida, tomo: paymentItem.tomo, folio: paymentItem.folio, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, costoServicio: paymentItem.costoServicio, costoTotal: paymentItem.costoTotal, ipRemote: paymentItem.ipRemote, sessionId: paymentItem.sessionId, usrId: paymentItem.usrId)
                
            }else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES || certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B || certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES || certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B) {
                
                if (certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES || certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B)  {
                    
                    
                    presenter.postDetalleAsientosPublicaCGASaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, partida: paymentItem.partida, matricula: paymentItem.matricula, expediente: paymentItem.expediente, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, costoServicio: paymentItem.costoServicio, ipRemote: paymentItem.ipRemote, sessionId: paymentItem.sessionId, usrId: paymentItem.usrId)
                    
                    
                } else if (certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES || certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B)  {
                    
                    presenter.postDetalleAsientosPublicaCGEPSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, partida: paymentItem.partida, matricula: paymentItem.matricula, nomEmbarcacion: paymentItem.nomEmbarcacion, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, costoServicio: paymentItem.costoServicio, ipRemote: paymentItem.ipRemote, sessionId: paymentItem.sessionId, usrId: paymentItem.usrId)
                }
            }else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ || certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO) {
                
                presenter.postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, codLibro: paymentItem.codLibro, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, refNumPartMP: paymentItem.refNumPartMP, partida: paymentItem.partida, ficha: paymentItem.ficha, tomo: paymentItem.tomo, folio: paymentItem.folio, placa: paymentItem.placa, matricula: paymentItem.matricula, nomEmbarcacion: paymentItem.nomEmbarcacion, expediente: paymentItem.expediente, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, asiento: paymentItem.asiento,tipPerVP: paymentItem.tipPerVP,apePateVP: paymentItem.apePateVP,apeMateVP:  paymentItem.apeMateVP,nombVP:  paymentItem.nombVP,razSocVP: paymentItem.razSocVP,cargoApoderado:paymentItem.cargoApoderado,datoAdic: paymentItem.datoAdic,costoServicio: paymentItem.costoServicio, usrId: paymentItem.usrId)
                
            }else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA) {
                print("vigencia de poder persona juridica")
                presenter.postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, codLibro: paymentItem.codLibro, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, refNumPartMP: paymentItem.refNumPartMP, partida: paymentItem.partida, ficha: paymentItem.ficha, tomo: paymentItem.tomo, folio: paymentItem.folio, placa: paymentItem.placa, matricula: paymentItem.matricula, nomEmbarcacion: paymentItem.nomEmbarcacion, expediente: paymentItem.expediente, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, asiento: paymentItem.asiento,tipPerVP: paymentItem.tipPerVP,apePateVP: paymentItem.apePateVP,apeMateVP:  paymentItem.apeMateVP,nombVP:  paymentItem.nombVP,razSocVP: paymentItem.razSocVP,cargoApoderado:paymentItem.cargoApoderado,datoAdic: paymentItem.datoAdic,costoServicio: paymentItem.costoServicio, usrId: paymentItem.usrId)
                
            }else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA || certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA) {
                
                presenter.postDetalleAsientosPublicaSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, tipPerPN: paymentItem.tipPerPN, apePatPN: paymentItem.apePatPN,apeMatPN: paymentItem.apeMatPN,nombPN: paymentItem.nombPN,razSocPN: paymentItem.razSocPN,tipoDocPN: paymentItem.tipoDocPN,numDocPN: paymentItem.numDocPN,costoServicio: paymentItem.costoServicio, costoTotal: paymentItem.costoTotal, usrId: paymentItem.usrId)
                
            }else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_REGISTRAL_VEHICULAR ||
                      certificadoId == Constant.CERTIFICADO_REGISTRAL_VEHICULAR_B) {
                
                presenter.postDetalleAsientosPublicaVehiSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, partida: paymentItem.partida,  placa: paymentItem.placa , tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, costoServicio: paymentItem.costoServicio,  ipRemote: paymentItem.ipRemote, sessionId: paymentItem.sessionId, usrId: paymentItem.usrId)
                
            } else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA ||
                       certificadoId == Constant.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_UNION_DE_HECHO ||
                       certificadoId == Constant.CERTIFICADO_POSITIVO_UNION_DE_HECHO ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS ||
                       certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B ||
                       certificadoId == Constant.CERTIFICADO_POSITIVO_DE_TESTAMENTOS ||
                       certificadoId == Constant.CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B) {
                
                presenter.postDetalleAsientosPublicaSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, tipPerPN: paymentItem.tipPerPN, apePatPN: paymentItem.apePatPN,apeMatPN: paymentItem.apeMatPN,nombPN: paymentItem.nombPN,razSocPN: paymentItem.razSocPN,tipoDocPN: paymentItem.tipoDocPN,numDocPN: paymentItem.numDocPN,costoServicio: paymentItem.costoServicio, costoTotal: paymentItem.costoTotal, usrId: paymentItem.usrId)
                
                
            }
            else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_VIGENCIA_DESIGNACION_APOYO ||
                     certificadoId == Constant.CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR ||
                     certificadoId == Constant.CERTIFICADO_VIGENCIA_PODER ||
                     certificadoId == Constant.CERTIFICADO_VIGENCIA_PODER_B) {
                print("postCertiVigeDesigApoyoSaveSolicitud: ", paymentItem.partida)
                presenter.postCertiVigeDesigApoyoSaveSolicitud(codCerti: paymentItem.codCerti, codArea: paymentItem.codArea, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, refNumPartMP: paymentItem.refNumPartMP, partida: paymentItem.partida, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email, asiento: paymentItem.asiento, numPartidaMP: paymentItem.numPartidaMP, numAsientoMP: paymentItem.numAsientoMP, costoServicio: paymentItem.costoServicio, costoTotal: paymentItem.costoTotal, usrId: paymentItem.usrId, lstOtorgantes: paymentItem.lstOtorgantes, lstApoyos: paymentItem.lstApoyos, lstCuradores: paymentItem.lstCuradores, lstPoderdantes: paymentItem
                    .lstPoderdantes, lstApoderados: paymentItem.lstApoderados, tomo: paymentItem.tomo, folio: paymentItem.folio)
                
                
            } else if paymentItem.costoTotal != "0.00" && (certificadoId == Constant.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_A ||
                      certificadoId == Constant.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B ) {
                presenter.postDetalleAsientosPublicaInmobiliarioSaveSolicitud(codCerti: certificadoId,
                                                                              codArea: areaRegId,
                                                                              codLibro: codLibro,
                                                                              oficinaOrigen: paymentItem.oficinaOrigen,
                                                                              refNumPart: refNumPart,
                                                                              refNumPartMP: paymentItem.refNumPartMP,
                                                                              partida: paymentItem.partida,
                                                                              ficha: paymentItem.ficha,
                                                                              tpoPersona: paymentItem.tpoPersona,
                                                                              apePaterno: paymentItem.apePaterno,
                                                                              apeMaterno: paymentItem.apeMaterno,
                                                                              nombre: paymentItem.nombre,
                                                                              razSoc: paymentItem.razSoc,
                                                                              tpoDoc: paymentItem.tpoDoc,
                                                                              numDoc: paymentItem.numDoc,
                                                                              email: paymentItem.email,
                                                                              costoServicio: paymentItem.costoServicio,
                                                                              ipRemote: paymentItem.ipRemote,
                                                                              sessionId: paymentItem.sessionId,
                                                                              usrId: paymentItem.usrId)
            }
            else if(certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PI ||
                    certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PJ ||
                    certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PN ||
                    certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_BM) {
                
                var nuPaginasSolicitadas = Int(countCantPag )
                var nuPaginasSolicitadas2 = Int(countCantPagExo )
                nuPaginasSolicitadas! += nuPaginasSolicitadas2!
                var paginas: String = String(nuPaginasSolicitadas!)
                paymentItem.paginasSolicitadas = paginas
                print(paginas)
            
                presenter.postDetalleAsientosLiteralSaveSolicitud(codCerti: certificadoId, codArea: paymentItem.codArea, codLibro: codLibro, oficinaOrigen: paymentItem.oficinaOrigen, refNumPart: paymentItem.refNumPart, partida: paymentItem.partida,  placa: numeroPlaca, tpoPersona: paymentItem.tpoPersona, apePaterno: paymentItem.apePaterno, apeMaterno: paymentItem.apeMaterno, nombre: paymentItem.nombre, razSoc: paymentItem.razSoc, tpoDoc: paymentItem.tpoDoc, numDoc: paymentItem.numDoc, email: paymentItem.email,costoServicio: paymentItem.costoServicio, cantPaginas: countCantPag, cantPaginasExon: countCantPagExo, paginasSolicitadas: paginas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecu, totalPaginasPartidaFicha: paymentItem.totalPaginasPartidaFicha, ipRemote: "", sessionId: "", usrId: paymentItem.usrId)
                
            }
            
        }
    }
    
    
    @objc private func onTapChecked() {
        if (isChecked) {
            self.checkImage.image = nil
            self.pagarView.primaryDisabledButton()
        } else {
            self.checkImage.image = UIImage(systemName: "checkmark")
            self.pagarView.primaryButton()
        }
        self.isChecked = !self.isChecked
    }
    
    @objc func cancelPressed() {
        self.view.endEditing(true)
    }
    
    func loaderView(isVisible: Bool) {
        if (isVisible) {
            self.loading = self.loader()
        } else {
            //self.stopLoader(loader: self.loading)
        }
    }
    
    func pagarSolicitud(saveProcess: GuardarSolicitudEntity) {
        guardarSolicitudEntity = saveProcess
        presenter.getVisaKeys()
    }
    
    func pagarSolicitudDA(saveProcess: GuardarSolicitudEntity, lstOtorgantes: String, lstApoyos: String, lstCuradores: String, lstPoderdantes: String, lstApoderados: String) {
        guardarSolicitudEntity = saveProcess
        guardarSolicitudEntity?.lstOtorgantes = lstOtorgantes
        guardarSolicitudEntity?.lstApoyos = lstApoyos
        guardarSolicitudEntity?.lstCuradores = lstCuradores
        guardarSolicitudEntity?.lstPoderdantes = lstPoderdantes
        guardarSolicitudEntity?.lstApoderados = lstApoderados
        print("pagarSolicitudDA")
        presenter.getVisaKeys()
    }
    
    func changeTypeDocument(index: Int) {
        self.typeNameDocument = self.tipoDocumentosEntities[index].tipoDocId
        self.numDocText.text = .empty
        
        if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_DNI) {
            self.numDocText.keyboardType = .numberPad
            self.numDocText.maxLengths = 8
            self.numDocText.valueType = .onlyNumbers
        } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_CE) {
            self.numDocText.maxLengths = 9
            self.numDocText.valueType = .alphaNumeric
            self.numDocText.keyboardType = .alphabet
        } else if (self.typeNameDocument == Constant.TYPE_DOCUMENT_CODE_RUC) {
            self.numDocText.maxLengths = 11
            self.numDocText.valueType = .onlyNumbers
            self.numDocText.keyboardType = .numberPad
        } else if (self.typeNameDocument == "00" || self.typeNameDocument == "90" || self.typeNameDocument == "91") || self.typeNameDocument == "92" {
            self.numDocText.maxLengths = 10
            self.numDocText.valueType = .alphaNumeric
            self.numDocText.keyboardType = .alphabet
        }
    }
    
    func loadVisaKeyController(visaKeys: VisaKeysEntity) {
        self.visaKeys = visaKeys
        presenter.getTokenNiubiz(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlVisa: SunarpWebService.Niubiz.postNiubiz())
    }
    
    func loadNiubizController(token: String) {
        self.tokenNiubiz = token
        print("loadNiubizController")
        self.presenter.getNiubizPinHash(token: token, merchant: self.visaKeys.merchantId)
    }
    
    func loadNiubizPinHashController(pinHash: NiubizPinHashEntity) {
        self.pinHash = pinHash.pinHash
        print("loadNiubizPinHashController")
        let urlString = "\(String(visaKeys.urlVisanetCounter))\(String(visaKeys.merchantId))/nextCounter"
        self.presenter.getTransactionId(userName: visaKeys.accesskeyId, password: visaKeys.secretAccessKey, urlString: urlString)
    }
    
    func loadNiubizTransactionId(transactionId: String) {
        self.transactionIdNiubiz = transactionId
        print("transactionIdNiubiz set: " + transactionIdNiubiz)
        self.loadNiubizScreen()
    }
    
    func loadNiubizScreen() {
        print("loadNiubizScreen")
        
        let usuario = UserPreferencesController.usuario()
        //Config.CE.endPointDevURL = String(self.visaKeys.urlVisanet)
        Config.CE.endPointDevURL = String(SunarpWebService.niubizURL.dropLast())
        print(Config.CE.endPointDevURL)
        Config.merchantID = self.visaKeys.merchantId
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.CE.type = .prod
        } else {
            Config.CE.type = .dev
        }
        
        Config.CE.dataChannel = .mobile
        Config.securityToken = self.tokenNiubiz
        Config.CE.purchaseNumber = transactionIdNiubiz //\(Int.random(in:99999...9999999999))"
        let paymentService = paymentItem.costoTotal.replacingOccurrences(of: " ", with: "")
        
        Config.amount = Double(paymentService) ?? 0.0
        Config.CE.countable = true
        Config.CE.showAmount = true
        Config.CE.initialAmount = Double(paymentService) ?? 0.0
        Config.CE.LastNameField.defaultText = paymentItem.apePaterno
        Config.CE.FirstNameField.defaultText = paymentItem.nombre
        Config.CE.EmailField.defaultText = usuario.email
        Config.CE.Header.type = .logo
        Config.CE.Header.logoImage = UIImage(named: "logo")
        
        let tipoDocumento = UserPreferencesController.getTipoDocDesc()
        
        //MDDs obligatorios agregados referente a http://mdd.evirtuales.com codigo : 40009
        var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = usuario.email //self.visaKeys.mdd4 // MDD4 -> Email del cliente
        merchantDefineData["MDD21"] = self.visaKeys.mdd21 // MDD21 -> Cliente frecuente
        merchantDefineData["MDD32"] = usuario.nroDoc //self.visaKeys.mdd32 // MDD32 -> Código o ID del cliente
        merchantDefineData["MDD63"] = tipoDocumento // MDD63 -> Tipo de documento del beneficiario
        merchantDefineData["MDD75"] = self.visaKeys.mdd75 // -> Tipo de registro del cliente
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let startDate = dateFormatter.date(from: usuario.createdAt)!
        let currentDate = Date()
        let interval = currentDate - startDate
        
        merchantDefineData["MDD77"] = interval.day // self.visaKeys.mdd77 // -> Días desde registro del cliente
        
        Config.CE.Antifraud.merchantDefineData = merchantDefineData
        
        if (ConfigurationEnvironment.environment == .release) {
            Config.PINSHA256PROD = pinHash
        }else{
            Config.PINSHA256DEV = pinHash
        }
        //loaderView(isVisible: false)
        /*  DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
         VisaNet.shared.delegate = self
         }*/
        loaderView(isVisible: false)
        print("presentVisaPaymentForm")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let success = VisaNet.shared.presentVisaPaymentForm(viewController: self) {
                VisaNet.shared.delegate = self
                print("presenta correctamente")
            }else{
                print("No presenta")
            }
        }
    }
    
    
}

extension PaymentsViewController : VisaNetDelegate {
    
    func getTransactionData(responseData: Any?) -> TransactionData? {
        if let response = responseData as? [String:AnyObject] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    if let transactionData = try? JSONDecoder().decode(TransactionData.self, from: jsonData){
                        self.pagoExitoso = PagoExitosoEntity(json: response)
                        return transactionData
                    }
                }
            }
        }else {
           showAlert(message: "Ocurrio un error al procesar el pago.")
        }
        return nil
    }
    
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        if serverError == nil {
            if let transactionData = getTransactionData(responseData: responseData) {
                if (certificadoId == Constant.CERTIFICADO_NEGATIVO_PREDIOS ||
                    certificadoId == Constant.CERTIFICADO_NEGATIVO_PREDIOS_B ||
                    certificadoId == Constant.CERTIFICADO_POSITIVO_PREDIOS ||
                    certificadoId == Constant.CERTIFICADO_POSITIVO_PREDIOS_B ||
                    certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR ||
                    certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B ||
                    certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR ||
                    certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B ) {
                    paymentProcess(item: transactionData)
                } else if (certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA ||
                           certificadoId == Constant.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA) {
                    certificadoPersonaJuridica(responseData: responseData)
                } else if (certificadoId == Constant.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA ||
                           certificadoId == Constant.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_UNION_DE_HECHO ||
                           certificadoId == Constant.CERTIFICADO_POSITIVO_UNION_DE_HECHO ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS ||
                           certificadoId == Constant.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B ||
                           certificadoId == Constant.CERTIFICADO_POSITIVO_DE_TESTAMENTOS ||
                           certificadoId == Constant.CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B) {
                    paymentRegistroPerNat(responseData: responseData)
                } else if (certificadoId == Constant.CERTIFICADO_DE_CARGAS_Y_GRAVAMENES ||
                           certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA ||
                           certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ ||
                           certificadoId == Constant.CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO ||
                           certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES ||
                           certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B ||
                           certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES ||
                           certificadoId == Constant.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B ||
                           certificadoId == Constant.CERTIFICADO_REGISTRAL_VEHICULAR ||
                           certificadoId == Constant.CERTIFICADO_REGISTRAL_VEHICULAR_B) {
                    
                    
                    paymentProcess(item: transactionData)
                }
                else if (certificadoId == Constant.CERTIFICADO_VIGENCIA_DESIGNACION_APOYO ||
                         certificadoId == Constant.CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR ||
                         certificadoId == Constant.CERTIFICADO_VIGENCIA_PODER ||
                         certificadoId == Constant.CERTIFICADO_VIGENCIA_PODER_B ||
                         certificadoId == Constant.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_A ||
                         certificadoId == Constant.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B) {
                    
                    
                    paymentProcess(item: transactionData)
                }
                else if(certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PI ||
                        certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PJ ||
                        certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_PN ||
                        certificadoId == Constant.CERTIFICADO_COPIA_LITERAL_BM) {
                    paymentProcess(item: transactionData)
                }
                else {
                    print("OCURREIO UN ERROR???? : === \(certificadoId)")
                }
            }
        } else {
            dump(responseData)
            print("transactionIdNiubiz = " + self.transactionIdNiubiz)
            errorPayment(responseData: responseData)
        }
    }
    
    private func errorPayment(responseData: Any?) {
        if let error = responseData as? [String:AnyObject] {
            let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoRechazadoViewController) as! PagoRechazadoViewController
            vc.pagoRechazadoEntity = PagoRechazadoEntity(json: error)
            vc.pagoRechazadoEntity.data.purchaseNumber = self.transactionIdNiubiz
            vc.guardarSolicitudEntity = guardarSolicitudEntity
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func certificadoPersonaJuridica(responseData: Any?) {
        
        if let item = getTransactionData(responseData: responseData) {
            
            self.pagarView.primaryDisabledButton()
            self.pagarView.isUserInteractionEnabled = false
            
            let payment = PaymentProcessRequest()
            payment.solicitudId = guardarSolicitudEntity?.solicitudId ?? 0
            
            payment.costoTotal = item.dataMap.amount
            payment.ip = "10.129.6.1"
            payment.usrId = "APPSNRPIOS"
            payment.idUser = paymentItem.numDoc
            payment.idUnico = item.dataMap.transactionID
            payment.pan = item.dataMap.card
            payment.dscCodAccion = item.dataMap.actionDescription
            payment.codAutoriza = item.dataMap.authorizationCode
            payment.codtienda = item.dataMap.merchant
            payment.numOrden = item.order.purchaseNumber
            payment.codAccion = "000"
            payment.fechaYhoraTx = getCurrentDateAndTime()
            payment.nomEmisor = item.dataMap.brand
            payment.oriTarjeta = item.dataMap.cardType
            payment.transId = item.order.transactionID
            payment.eticket =  item.order.transactionID
            payment.codCerti = certificadoId
            payment.cantPaginas = "0"
            payment.cantPaginasExon = "0"
            payment.paginasSolicitadas = "0"
            payment.totalPaginasPartidaFicha = "0"
            
            let visaNetRequest = payment.visanetResponse
            visaNetRequest.codAccion = "000"
            visaNetRequest.codAutoriza = item.dataMap.authorizationCode
            visaNetRequest.codtienda = item.dataMap.merchant
            visaNetRequest.dscCodAccion = item.dataMap.actionDescription
            visaNetRequest.dscEci = item.dataMap.eciDescription
            visaNetRequest.eci = item.dataMap.eci
            visaNetRequest.estado = item.dataMap.status
            visaNetRequest.eticket = item.order.transactionID
            visaNetRequest.fechaYhoraTx = getCurrentDateAndTime()
            visaNetRequest.idUnico =  item.dataMap.transactionID
            visaNetRequest.idUser = paymentItem.numDoc
            visaNetRequest.impAutorizado = item.dataMap.amount
            visaNetRequest.nomEmisor =  item.dataMap.brand
            visaNetRequest.numOrden = item.order.purchaseNumber
            visaNetRequest.numReferencia = item.dataMap.traceNumber
            visaNetRequest.oriTarjeta = item.dataMap.cardType
            visaNetRequest.pan =  item.dataMap.card
            visaNetRequest.transId =  item.order.transactionID
            
            payment.visanetResponse = visaNetRequest
            payment.email = item.dataMap.email
            
            dump(payment)
            // self.presenter.paymentRequest(paymentInformation: payment,transactionData: item)
            self.presenter.paymentRequest(paymentInformation: payment,transactionData: item)
            
        }
    }
    
    private func paymentRegistroPerNat(responseData: Any?){
        self.pagarView.primaryDisabledButton()
        self.pagarView.isUserInteractionEnabled = false
        if let item = getTransactionData(responseData: responseData),
           let guardarSolicitudEntity = guardarSolicitudEntity {
            
            let payment = PaymentBuilder.paymentRegistroPerNat(item: item,
                                                               certificadoId: certificadoId,
                                                               paymentItem: paymentItem,
                                                               guardarSolicitudEntity: guardarSolicitudEntity)
            presenter.paymentRequest(paymentInformation: payment,transactionData: item)
        }
    }
    
    
    func loadSaveAsiento(solicitud: GuardarSolicitudEntity, costoServicio: String, codLibro: String , cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, totalPaginasPartidaFicha: String) {

    
        
        guardarSolicitudEntity = solicitud
        guardarSolicitudEntity?.codLibro = codLibro
        guardarSolicitudEntity?.cantPaginas = cantPaginas
        guardarSolicitudEntity?.cantPaginasExon = cantPaginasExon
        guardarSolicitudEntity?.paginasSolicitadas = paginasSolicitadas
        guardarSolicitudEntity?.nuAsieSelectSARP = nuAsieSelectSARP
        guardarSolicitudEntity?.imPagiSIR = imPagiSIR
        guardarSolicitudEntity?.nuSecuSIR = nuSecuSIR
        guardarSolicitudEntity?.totalPaginasPartidaFicha = totalPaginasPartidaFicha
        dump(guardarSolicitudEntity)
        print("loadSaveAsiento")
        
        if costoServicio != "0.00" {
            presenter.getVisaKeys()
        }
        else {
            paymentZeroProcess()
        }
        
        
    }
    
    private func paymentZeroProcess(){
        
        self.pagarView.primaryDisabledButton()
        self.pagarView.isUserInteractionEnabled = false
        let payment = PaymentProcessRequest()
        payment.solicitudId = guardarSolicitudEntity?.solicitudId ?? 0
        
        payment.costoTotal = "0"
        payment.ip = "10.129.6.1"
        payment.usrId = "APPSNRPIOS"
        payment.idUser = paymentItem.numDoc
        payment.idUnico = ""
        payment.pan = ""
        payment.dscCodAccion = ""
        payment.codAutoriza = ""
        payment.codtienda = "EXONERADO"
        payment.numOrden = ""
        payment.codAccion = "000"
        payment.fechaYhoraTx = ""
        payment.nomEmisor = ""
        payment.oriTarjeta = ""
        payment.transId = ""
        payment.eticket =  ""
        payment.codCerti = certificadoId
        payment.formaEnvio = "V"
        payment.lstApoyos = ""
        payment.lstCuradores = ""
        payment.lstApoderados = ""
        payment.lstOtorgantes = ""
        payment.lstPoderdantes = ""
        
        payment.codLibro = paymentItem.codLibro
        payment.cantPaginas = paymentItem.cantPaginas
        payment.cantPaginasExon = paymentItem.cantPaginasExon
        payment.paginasSolicitadas = paymentItem.paginasSolicitadas
        payment.nuAsieSelectSARP = paymentItem.nuAsieSelectSARP
        payment.imPagiSIR = paymentItem.imPagiSIR
        payment.nuSecuSIR = paymentItem.nuSecuSIR
        payment.totalPaginasPartidaFicha = paymentItem.totalPaginasPartidaFicha
        payment.coServ = coServicio
        payment.coTipoRgst = coTipoRegis
        payment.codigoGla = codigoGla

        let visaNetRequest = payment.visanetResponse
        visaNetRequest.codAccion = "000"
        visaNetRequest.codAutoriza = ""
        visaNetRequest.codtienda = "EXONERADO"
        visaNetRequest.dscCodAccion = ""
        visaNetRequest.dscEci = ""
        visaNetRequest.eci = ""
        visaNetRequest.estado = ""
        visaNetRequest.eticket = ""
        visaNetRequest.fechaYhoraTx = ""
        visaNetRequest.idUnico =  ""
        visaNetRequest.idUser = paymentItem.numDoc
        visaNetRequest.impAutorizado = "0"
        visaNetRequest.nomEmisor =  ""
        visaNetRequest.numOrden = ""
        visaNetRequest.numReferencia = ""
        visaNetRequest.oriTarjeta = ""
        visaNetRequest.pan =  ""
        visaNetRequest.transId =  ""
        
        payment.visanetResponse = visaNetRequest
        payment.email = paymentItem.email
        
        presenter.paymentRequestZero(paymentInformation: payment )
    }
    
    
    private func paymentProcess(item: TransactionData){
        
        self.pagarView.primaryDisabledButton()
        self.pagarView.isUserInteractionEnabled = false
        let payment = PaymentProcessRequest()
        payment.solicitudId = guardarSolicitudEntity?.solicitudId ?? 0
        
        payment.costoTotal = item.dataMap.amount
        payment.ip = "10.129.6.1"
        payment.usrId = "APPSNRPIOS"
        payment.idUser = paymentItem.numDoc
        payment.idUnico = item.dataMap.transactionID
        payment.pan = item.dataMap.card
        payment.dscCodAccion = item.dataMap.actionDescription
        payment.codAutoriza = item.dataMap.authorizationCode
        payment.codtienda = item.dataMap.merchant
        payment.numOrden = item.order.purchaseNumber
        payment.codAccion = "000"
        payment.fechaYhoraTx = getCurrentDateAndTime()
        payment.nomEmisor = item.dataMap.brand
        payment.oriTarjeta = item.dataMap.cardType
        payment.transId = item.order.transactionID
        payment.eticket =  item.order.transactionID
        payment.codCerti = certificadoId
        payment.cantPaginas = "0"
        payment.cantPaginasExon = "0"
        payment.paginasSolicitadas = "0"
        payment.totalPaginasPartidaFicha = "0"
        payment.formaEnvio = "V"
        payment.lstApoyos = paymentItem.lstApoyos
        payment.lstCuradores = paymentItem.lstCuradores
        payment.lstApoderados = paymentItem.lstApoderados
        payment.lstOtorgantes = paymentItem.lstOtorgantes
        payment.lstPoderdantes = paymentItem.lstPoderdantes
        
        payment.codLibro = paymentItem.codLibro
        payment.cantPaginas = paymentItem.cantPaginas
        payment.cantPaginasExon = paymentItem.cantPaginasExon
        payment.paginasSolicitadas = paymentItem.paginasSolicitadas
        payment.nuAsieSelectSARP = paymentItem.nuAsieSelectSARP
        payment.imPagiSIR = paymentItem.imPagiSIR
        payment.nuSecuSIR = paymentItem.nuSecuSIR
        payment.totalPaginasPartidaFicha = paymentItem.totalPaginasPartidaFicha
        payment.coServ = coServicio
        payment.coTipoRgst = coTipoRegis
        payment.codigoGla = codigoGla

        let visaNetRequest = payment.visanetResponse
        visaNetRequest.codAccion = "000"
        visaNetRequest.codAutoriza = item.dataMap.authorizationCode
        visaNetRequest.codtienda = item.dataMap.merchant
        visaNetRequest.dscCodAccion = item.dataMap.actionDescription
        visaNetRequest.dscEci = item.dataMap.eciDescription
        visaNetRequest.eci = item.dataMap.eci
        visaNetRequest.estado = item.dataMap.status
        visaNetRequest.eticket = item.order.transactionID
        visaNetRequest.fechaYhoraTx = getCurrentDateAndTime()
        visaNetRequest.idUnico =  item.dataMap.transactionID
        visaNetRequest.idUser = paymentItem.numDoc
        visaNetRequest.impAutorizado = item.dataMap.amount
        visaNetRequest.nomEmisor =  item.dataMap.brand
        visaNetRequest.numOrden = item.order.purchaseNumber
        visaNetRequest.numReferencia = item.dataMap.traceNumber
        visaNetRequest.oriTarjeta = item.dataMap.cardType
        visaNetRequest.pan =  item.dataMap.card
        visaNetRequest.transId =  item.order.transactionID
        
        payment.visanetResponse = visaNetRequest
        payment.email = item.dataMap.email
        presenter.paymentRequest(paymentInformation: payment,transactionData: item)
    }
    
    func paymentProcessResult(item: PaymentProcessEntity?, transactionData: TransactionData) {
        self.pagarView.primaryButton()
        self.pagarView.isUserInteractionEnabled = true
     //   loaderView(isVisible: false)
        if let item = item {
            if let pagoExitoso = pagoExitoso {
                let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoCertificadoLiteralvewController) as! PagoExitosoCertificadoLiteralvewController
                dump(item)
                vc.paymentProcessEntity = item
                vc.paymentProcessEntity?.paymentItem = paymentItem
                vc.transactionData = transactionData
                print("transactionIdNiubiz Exito = " + self.transactionIdNiubiz)

                vc.transactionData?.dataMap.purchaseNumber = self.transactionIdNiubiz
                vc.certificateType = opeDetail.text ?? .empty
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            showAlert(message: "Pagar solictud no esta disponible")
        }
        
    }
    
    
    func paymentProcessResultZero(item: PaymentProcessEntity?) {
        print("zero 003")
        dump(paymentItem)
        self.pagarView.primaryButton()
        self.pagarView.isUserInteractionEnabled = true
        let storyboard = UIStoryboard(name: "PagoLiquidacion", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.Identifier.pagoExitosoCertificadoLiteralvewController) as! PagoExitosoCertificadoLiteralvewController
        vc.paymentProcessEntity = item
        vc.paymentProcessEntity?.paymentItem = paymentItem
        vc.certificateType = opeDetail.text ?? .empty
        // vc.paymentProcessEntity?.fechaYhoraTx = vc.paymentProcessEntity?.tsCrea ?? ""
        dump(vc.paymentProcessEntity)

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: Constant.Localize.información, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getCurrentDateAndTime() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let now = Date()
        return dateFormatter.string(from: now)
    }
    
    
    private func obtainCodesOfZone() -> [String] {
        var codes: [String] = []
        
        for zona in self.zonas {
            codes.append(zona.regPubId)
        }
        return codes
    }
}

extension PaymentsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return  self.tipoDocumentosEntities.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return self.tipoDocumentosEntities[row].nombreAbrev
        default:
            return "Sin datos"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 1:
            
            self.changeTypeDocument(index: row)
            self.areaRegistralText.text = tipoDocumentosEntities[row].nombreAbrev
            self.areaRegistralText.resignFirstResponder()
            
            
        default:
            return
        }
        
    }
}

extension PaymentsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        textField.text =  textField.text?.uppercased()
        validarCampos()
    }
    
    func validarCampos(){
    
        self.pagarView.primaryDisabledButton()
        guard let numDoc = numDocText.text, !numDoc.isEmpty else {
            return
        }
        
        if tipoPer == "N" {
            guard let nombre = nombreText.text, !nombre.isEmpty else {
                return
            }
            
            guard let apellidoPaterno = apellidoPaternoText.text, !apellidoPaterno.isEmpty else {
                return
            }
        }
        
        if(tipoPer == "J") {
            guard let nameRazonSocial = nameRazonSocialText.text, !nameRazonSocial.isEmpty else {
                return
            }
        }
        
        if (isChecked == false ){
            return
        }
        self.pagarView.primaryButton()
    }
}

extension PaymentsViewController: TermyCondiDialogDelegate{
    
    func primary(action:String) {
        if action == "Aceptar" {
            //  self.presenter.putCancelUser(idRgst: self.idRgst)
        } else if action == "REINTENTAR" {
            // callButton.sendActions(for: .touchUpInside)
            print("*** coge cualquier opcion")
        }
    }
    
    func secondary() {
        // print("self.val_aaCont::>>",self.val_aaCont)
        //self.presenter.putDeleteMandato(aaCont: self.val_aaCont, nuCont: self.val_nuCount)
        
        
    }
}


