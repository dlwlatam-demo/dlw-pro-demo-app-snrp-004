//
//  PaymentsPresenter.swift
//  Sunarp
//
//  Created by Segundo Acosta on 11/01/23.
//


import Foundation

class PaymentsPresenter{
    
    private weak var controller: PaymentsViewController?
    
    lazy private var modelCons: RegisterModel = {
        let navigation = controller?.navigationController
        return RegisterModel(navigationController: navigation!)
    }()
    
    lazy private var historyModel: HistoryModel = {
        let navigation = controller?.navigationController
       return HistoryModel(navigationController: navigation!)
    }()
    
    
    lazy private var consultaLiteralModel: ConsultaLiteralModel = {
        let navigation = controller?.navigationController
        return ConsultaLiteralModel(navigationController: navigation!)
    }()
    
    lazy private var consultaPublicaModel: ConsultaPublicaModel = {
        let navigation = controller?.navigationController
        return ConsultaPublicaModel(navigationController: navigation!)
    }()
    
    init(controller: PaymentsViewController) {
        self.controller = controller
    }
}

extension PaymentsPresenter: GenericPresenter{
    
    func didLoad() {
        let tipoPer = controller?.tipoPer ?? ""
        //let guid = UserPreferencesController.getGuid()
        self.modelCons.getListaTipoDocumentosInt(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }

    func didLoadJur() {
        let tipoPer = controller?.tipoPer ?? ""
        //let guid = UserPreferencesController.getGuid()
        self.modelCons.getListaTipoDocumentosJur(guid: tipoPer) { (arrayTipoDocumentos) in
            self.controller?.loadTipoDocumentos(arrayTipoDocumentos: arrayTipoDocumentos)
        }
    }
    
    func saveRequestPropertyRegister(codCerti: String, codArea: String,  oficinaOrigen: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, tipPerPN: String , apePatPN: String , apeMatPN: String , nombPN: String , razSocPN: String , tipoDocPN: String , numDocPN: String , costoServicio: String , costoTotal: String , usrId: String) {
        
        //self.controller?.loaderView(isVisible: true)
        self.consultaPublicaModel.postDetalleAsientosPublicaSaveSolicitud(codCerti: codCerti, codArea: codArea,  oficinaOrigen: oficinaOrigen, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, tipPerPN: tipPerPN , apePatPN: apePatPN , apeMatPN: apeMatPN , nombPN: nombPN , razSocPN: razSocPN , tipoDocPN: tipoDocPN , numDocPN: numDocPN , costoServicio: costoServicio , costoTotal: costoTotal , usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
        }
    }
    
    func postDetalleAsientosPublicaCargaSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, tomo: String, folio: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, costoTotal: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosPublicaCargaSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, tomo: tomo, folio: folio, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio,costoTotal: costoTotal, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (saveRequestPropertyRegister) in
            
            //self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
        }
    }
    
    
    func postDetalleAsientosPublicaCGASaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, matricula: String,expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosPublicaCGASaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, matricula: matricula,expediente: expediente, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
            //self.controller?.loadSaveAsiento(solicitud: saveRequestPropertyRegister)
        }
    }
    
    
    func postDetalleAsientosPublicaCGEPSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, matricula: String,nomEmbarcacion: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosPublicaCGEPSaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, matricula: matricula,nomEmbarcacion: nomEmbarcacion, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
            //self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
    
    
    func postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tomo: String, folio: String, placa: String, matricula: String, nomEmbarcacion: String, expediente: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String,tipPerVP: String,apePateVP: String,apeMateVP: String,nombVP: String,razSocVP: String,cargoApoderado: String,datoAdic: String,costoServicio: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosPublicaCertiVigPJSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, ficha: ficha, tomo: tomo, folio: folio, placa: placa, matricula: matricula, nomEmbarcacion: nomEmbarcacion, expediente: expediente, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, asiento: asiento,tipPerVP: tipPerVP,apePateVP: apePateVP,apeMateVP: apeMateVP,nombVP: nombVP,razSocVP: razSocVP,cargoApoderado: cargoApoderado,datoAdic: datoAdic,costoServicio: costoServicio, usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
            //self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
    
    func postDetalleAsientosPublicaSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, tipPerPN: String, apePatPN: String, apeMatPN: String, nombPN: String, razSocPN: String, tipoDocPN: String, numDocPN: String, costoServicio: String, costoTotal: String, usrId: String) {
        //self.controller?.loaderView(isVisible: true)
        self.consultaPublicaModel.postDetalleAsientosPublicaSaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, tipPerPN: tipPerPN, apePatPN: apePatPN, apeMatPN: apeMatPN, nombPN: nombPN, razSocPN: razSocPN, tipoDocPN: tipoDocPN, numDocPN: numDocPN, costoServicio: costoServicio, costoTotal: costoTotal, usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
            //self.controller?.loadSaveAsiento(solicitud: niubizResponse)
        }
    }
    
    func postDetalleAsientosPublicaInmobiliarioSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, ficha: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String) {
        //self.controller?.loaderView(isVisible: true)
        self.consultaPublicaModel.postDetalleAsientosPublicaInmobiliarioSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, ficha: ficha, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
        }
    }
    
    
    func postDetalleAsientosPublicaVehiSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, ipRemote: String, sessionId: String, usrId: String) {
            self.consultaPublicaModel.postDetalleAsientosPublicaVehiSaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (saveRequestPropertyRegister) in
                
                
                self.controller?.pagarSolicitud(saveProcess: saveRequestPropertyRegister)
                //self.controller?.loadSaveAsiento(solicitud: niubizResponse)
            }
    }
    
    func postCertiVigeDesigApoyoSaveSolicitud(codCerti: String, codArea: String, oficinaOrigen: String, refNumPart: String, refNumPartMP: String, partida: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, asiento: String, numPartidaMP: String, numAsientoMP: String,  costoServicio: String, costoTotal: String, usrId: String, lstOtorgantes: String, lstApoyos: String, lstCuradores: String, lstPoderdantes: String, lstApoderados: String, tomo: String, folio: String) {
        //self.controller?.loaderView(isVisible: true)
        self.consultaPublicaModel.postCertiVigeDesigApoyoSaveSolicitud(codCerti: codCerti, codArea: codArea, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, refNumPartMP: refNumPartMP, partida: partida, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, asiento: asiento, numPartidaMP: numPartidaMP, numAsientoMP: numAsientoMP,  costoServicio: costoServicio, costoTotal: costoTotal, usrId: usrId, tomo: tomo, folio: folio) { (saveRequestPropertyRegister) in
            
            self.controller?.pagarSolicitudDA(saveProcess: saveRequestPropertyRegister, lstOtorgantes: lstOtorgantes, lstApoyos: lstApoyos, lstCuradores: lstCuradores, lstPoderdantes: lstPoderdantes, lstApoderados: lstApoderados)
        }
    }
    
    func postDetalleAsientosLiteralSaveSolicitud(codCerti: String, codArea: String, codLibro: String, oficinaOrigen: String, refNumPart: String, partida: String, placa: String, tpoPersona: String, apePaterno: String, apeMaterno: String, nombre: String, razSoc: String, tpoDoc: String, numDoc: String, email: String, costoServicio: String, cantPaginas: String, cantPaginasExon: String, paginasSolicitadas: String, nuAsieSelectSARP: String, imPagiSIR: String, nuSecuSIR: String, totalPaginasPartidaFicha: String, ipRemote: String, sessionId: String, usrId: String) {
        self.consultaPublicaModel.postDetalleAsientosLiteralSaveSolicitud(codCerti: codCerti, codArea: codArea, codLibro: codLibro, oficinaOrigen: oficinaOrigen, refNumPart: refNumPart, partida: partida, placa: placa, tpoPersona: tpoPersona, apePaterno: apePaterno, apeMaterno: apeMaterno, nombre: nombre, razSoc: razSoc, tpoDoc: tpoDoc, numDoc: numDoc, email: email, costoServicio: costoServicio, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, ipRemote: ipRemote, sessionId: sessionId, usrId: usrId) { (niubizResponse) in
            
             self.controller?.loadSaveAsiento(solicitud: niubizResponse, costoServicio: costoServicio, codLibro: codLibro, cantPaginas: cantPaginas, cantPaginasExon: cantPaginasExon, paginasSolicitadas: paginasSolicitadas, nuAsieSelectSARP: nuAsieSelectSARP, imPagiSIR: imPagiSIR, nuSecuSIR: nuSecuSIR, totalPaginasPartidaFicha: totalPaginasPartidaFicha )
        }
    }
    
    func paymentRequest(paymentInformation: PaymentProcessRequest,transactionData: TransactionData){
       // self.controller?.loaderView(isVisible: true)
        self.consultaPublicaModel.postPaymentRequest(itemProcess: paymentInformation){ (saveRequestPropertyRegister) in
            self.controller?.paymentProcessResult(item: saveRequestPropertyRegister, transactionData: transactionData)
        }
    }
    
    
    func paymentRequestZero(paymentInformation: PaymentProcessRequest){
        print("zero 001")
        dump(paymentInformation)
        self.consultaPublicaModel.postPaymentRequest(itemProcess: paymentInformation){ (saveRequestPropertyRegister) in
            print("zero 002")
            dump(saveRequestPropertyRegister)
            
            self.controller?.paymentProcessResultZero(item: saveRequestPropertyRegister)
        }
    }
    
    func getVisaKeys() {
        var instancia = Constant.VISA_KEYS_INSTANCIA_DEBUG
        var accessAppKey = Constant.VISA_KEYS_ACCESS_DEBUG
        
        if (ConfigurationEnvironment.environment.rawValue == ConfigurationEnvironment.Environment.release.rawValue) {
            instancia = Constant.VISA_KEYS_INSTANCIA_RELEASE
            accessAppKey = Constant.VISA_KEYS_ACCESS_RELEASE
        }
        self.historyModel.postVisaKeys(instancia: instancia, accessAppKey: accessAppKey) { (visaKeysResponse) in
            self.controller?.loadVisaKeyController(visaKeys: visaKeysResponse)
        }
    }
    
    func getTokenNiubiz(userName: String, password: String, urlVisa: String) {
        self.historyModel.postNiubiz(userName: userName, password: password, urlVisa: urlVisa) { (niubizResponse) in
            self.controller?.loadNiubizController(token: niubizResponse)
        }
    }
    
    func getNiubizPinHash(token: String, merchant: String) {
        self.historyModel.postNiubizPinHash(token: token, merchant: merchant) { (niubizPinHashResponse) in
            self.controller?.loadNiubizPinHashController(pinHash: niubizPinHashResponse)
        }
    }
    
    func getTransactionId(userName: String, password: String, urlString: String) {
        historyModel.getTransactionId() {(niubizResponse) in
            self.controller?.loadNiubizTransactionId(transactionId: niubizResponse)
        }
        
    }

    
}

