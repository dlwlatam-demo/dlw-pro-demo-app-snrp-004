//
//  PaymentItem.swift
//  Sunarp
//
//  Created by Segundo Acosta on 12/01/23.
//

import Foundation


class PaymentItem{
    
    var codCerti:String = ""
    var codArea:String = ""
    var oficinaOrigen:String = ""
    var tpoPersona:String = ""
    var apePaterno:String = ""
    var apeMaterno:String = ""
    var nombre:String = ""
    var razSoc:String = ""
    var tpoDoc:String = ""
    var numDoc:String = ""
    var email:String = ""
    var tipPerPN:String = ""
    var apePatPN:String = ""
    var apeMatPN:String = ""
    var nombPN:String = ""
    var razSocPN:String = ""
    var tipoDocPN:String = ""
    var numDocPN:String = ""
    var costoServicio:String = ""
    var costoTotal:String = ""
    var usrId:String = ""
    var codLibro:String = ""
    var refNumPart:String = ""
    var partida:String = ""
    var tomo:String = ""
    var folio:String = ""
    var ipRemote:String = ""
    var sessionId:String = ""
    var matricula:String = ""
    var expediente:String = ""
    var nomEmbarcacion:String = ""
    var refNumPartMP:String = ""
    var ficha:String = ""
    var placa:String = ""
    var asiento:String = ""
    var tipPerVP:String = ""
    var apePateVP:String = ""
    var apeMateVP:String = ""
    var nombVP:String = ""
    var razSocVP:String = ""
    var cargoApoderado:String = ""
    var datoAdic:String = ""
    var numPartidaMP:String = ""
    var numAsientoMP:String = ""
    
    var lstOtorgantes:String = ""
    var lstApoyos: String = ""
    var lstCuradores: String = ""
    var lstPoderdantes: String = ""
    var lstApoderados: String = ""
    
    var cantPaginas: String = ""
    var cantPaginasExon: String = ""
    var paginasSolicitadas: String = ""
    var nuAsieSelectSARP: String = ""
    var imPagiSIR: String = ""
    var nuSecuSIR: String = ""
    var totalPaginasPartidaFicha: String = ""
    
}
