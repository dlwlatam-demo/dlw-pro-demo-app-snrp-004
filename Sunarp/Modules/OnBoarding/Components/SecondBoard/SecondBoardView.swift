//
//  SecondBoardView.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/07/22.
//

import UIKit

class SecondBoardView: UIView {
    
    @IBOutlet var viewContent: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInit()
    }
    
    private func setupInit() {
        viewContent = loadNib()
        viewContent.frame = bounds
        viewContent.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                                        UIView.AutoresizingMask.flexibleHeight]
        
        addSubview(viewContent)
    }
    
    private func loadNib() -> UIView { let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
}
