//
//  OnBoardingViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 21/07/22.
//

import UIKit

class OnBoardingViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipLabel: UILabel!
    
    var scrollWidth: CGFloat! = 0.0
    var scrollHeight: CGFloat! = 0.0
    var currentPage = CGFloat(0)
    
    override func viewDidLayoutSubviews() {
        scrollWidth = scrollView.frame.size.width
        scrollHeight = scrollView.frame.size.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        setupDesign()
        
        self.scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        frame.origin.x = scrollWidth * CGFloat(0)
        frame.size = CGSize(width: scrollWidth, height: scrollHeight)
        
        let firstBoardView = FirstBoardView.init(frame: frame)
        scrollView.addSubview(firstBoardView)
        
        frame.origin.x = scrollWidth * CGFloat(1)
        frame.size = CGSize(width: scrollWidth, height: scrollHeight)
        
        let secondBoardView = SecondBoardView.init(frame: frame)
        scrollView.addSubview(secondBoardView)
        
        frame.origin.x = scrollWidth * CGFloat(2)
        frame.size = CGSize(width: scrollWidth, height: scrollHeight)
        
        let thirdBoardView = ThirdBoardView.init(frame: frame)
        scrollView.addSubview(thirdBoardView)
        
        scrollView.contentSize = CGSize(width: scrollWidth * CGFloat(3), height: scrollHeight)
        self.scrollView.contentSize.height = 1.0
        
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        pageControl.pageIndicatorTintColor = UtilHelper.getUIColor(hex: "#dfedbe")
        pageControl.currentPageIndicatorTintColor = UtilHelper.getUIColor(hex: "#8ec321")
        
    }
    
    //indicator
    @IBAction func pageChanged(_ sender: Any) {
        scrollView!.scrollRectToVisible(CGRect(x: scrollWidth * CGFloat ((pageControl?.currentPage)!), y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setIndiactorForCurrentPage()
    }
    
    func setIndiactorForCurrentPage()  {
        let page = (scrollView?.contentOffset.x)!/scrollWidth
        if (page == currentPage && page > 1) {
            loadWelcomeView()
        } else {
            pageControl?.currentPage = Int(page)
            currentPage = page
        }
    }
    
    private func setupDesign() {
        self.view.backgroundColorGradient()
        self.addGestureView()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    private func addGestureView(){
        let onTapSkip = UITapGestureRecognizer(target: self, action: #selector(didAppearWelcomeView))
        skipLabel.addGestureRecognizer(onTapSkip)
    }
    
    @objc private func didAppearWelcomeView() {
        loadWelcomeView()
    }
    
    private func loadWelcomeView() {
        UserPreferencesController.setFirstLaunch(isFirstLaunch: false)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
