//
//  Configuration.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/07/22.
//

import Foundation

protocol EnvironmentInfo {
    
    var userURL: String {get set}
    var parameterURL: String {get set}
    var securityURL: String {get set}
    var profileURL: String {get set}
    var solicitudURL: String {get set}
    var niubizURL: String {get set}
    var niubizPinURL: String {get set}
    var consultaSunarpURL: String {get set}
    var consultaPropiedadURL: String {get set}
    var consultaPropiedadPJ: String {get set}
    var consultaVehicularURL: String {get set}
    var consultaTiveURL: String {get set}
    var userAlertRegURL: String {get set}
    var userAlertPartidaRegURL: String {get set}
    var userAlertMandatoRegURL: String {get set}
    
}

struct ConfigurationEnvironment {
    
    static var environment: Environment {
        
        let value = Bundle.main.infoDictionary?["Environment"] as? String ?? ""
        let environment = Environment(rawValue: value) ?? .mock
        
        return environment
    }
    
}

extension ConfigurationEnvironment {
    
    enum Environment: String {
        case mock = "Mock"
        case develop = "Develop"
        case release = "Release"
        
        var info: EnvironmentInfo {
            
            switch self {
                case .mock: return Mock()
                case .develop: return Develop()
                case .release: return Release()
            }
        }
    }
}

extension ConfigurationEnvironment {

    private struct Mock: EnvironmentInfo {
        var consultaVehicularURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var consultaSunarpURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var consultaPropiedadURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var userURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var parameterURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var securityURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var profileURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var solicitudURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var consultaPropiedadPJ: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var niubizURL: String = "https://apitestenv.vnforapps.com/api.security/v1/security"
        var niubizPinURL: String = "https://apitestenv.vnforapps.com/api.certificate/v1/query/"
        var consultaTiveURL: String = "https://6dabe09d-9b28-41db-a4f3-50e04809a9fa.mock.pstmn.io/"
        var userAlertRegURL: String = "https://app-alerta-mto-user-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/"
        var userAlertPartidaRegURL: String = "https://app-alerta-partida-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/"
        var userAlertMandatoRegURL: String = "https://app-alerta-mandato-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/"
        
        
    }
    
    private struct Develop: EnvironmentInfo {
        var consultaVehicularURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaSunarpURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaPropiedadURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var parameterURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var securityURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var profileURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var solicitudURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaPropiedadPJ: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var niubizURL: String = "https://apitestenv.vnforapps.com/"
        var niubizPinURL: String = "https://apitestenv.vnforapps.com/"
        var consultaTiveURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertPartidaRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertMandatoRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        
    }


    private struct Release: EnvironmentInfo {
        var consultaVehicularURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaSunarpURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaPropiedadURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var parameterURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var securityURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var profileURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var solicitudURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var consultaPropiedadPJ: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var niubizURL: String = "https://apitestenv.vnforapps.com/"
        var niubizPinURL: String = "https://apitestenv.vnforapps.com/"
        var consultaTiveURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertPartidaRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
        var userAlertMandatoRegURL: String = "https://api-gateway-nonprod.sunarp.gob.pe:9443/"
    }
}
