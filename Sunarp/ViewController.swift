//
//  ViewController.swift
//  Sunarp
//
//  Created by Joel Chuco Marrufo on 20/07/22.
//

import UIKit

class ViewController: UIViewController {

    private lazy var presenter: ViewControllerPresenter = {
       return ViewControllerPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColorGradient()
        self.presenter.validateVersionApp()
    }
    
    func nextView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000)) {
            if (UserPreferencesController.isFirstLaunch()) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnBoardingViewController") as! OnBoardingViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                if (UserPreferencesController.getAccessToken().isEmpty) {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func showAlertMessage() {
            let message = "La versión actual de esta aplicación ya no es compatible. Te invitamos a descargar una nueva version."
            let alert = UIAlertController(title: "Actualizar Aplicación", message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) -> Void in
                self.openStoreApp()
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertError() {
            let message = "Ocurrio un error"
            let alert = UIAlertController(title: "SUNARP", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
    }
    
    func openStoreApp(){
        if let url = URL(string: "itms-apps://apple.com/app/id1045750816") {
            UIApplication.shared.open(url)
        }
    }
}

